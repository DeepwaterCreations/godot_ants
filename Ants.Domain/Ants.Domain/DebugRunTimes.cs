﻿using Prototypist.TaskChain;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Ants.Domain
{
    public class DebugRunTimes
    {
        private class Entry {
            public string runtime;
            public string @event;
            public string thread;
            public string currentTime;
            public string bar;
            public string frame;
            public string runId;
        } 

        private static ConcurrentLinkedList<Entry> times = new ConcurrentLinkedList<Entry> ();

        public static void Add(string @event, long time, int? frame= null, Guid? runId = null) {

            var entry = new Entry{
                runtime =new TimeSpan( time).TotalMilliseconds.ToString("f3"),
                @event = @event.ToString(),
                thread = System.Environment.CurrentManagedThreadId.ToString(),
                currentTime = DateTime.Now.ToString("yyyy MM dd HH:mm:ss:fff") ,
                bar = ToBar((int)Math.Ceiling(new TimeSpan(time).TotalMilliseconds*10)),
            };

            if (frame != null) {
                entry.frame = frame.Value.ToString();
            }
            if (runId != null)
            {
                entry.runId = runId.Value.ToString();
            }

            times.Add(entry);
        }

        private static string ToBar(int v)
        {
            return new string(new char[v].Select(x => '|').ToArray());
        }

        public static string Header() {
            return string.Join(", ", new string[] {
                nameof(Entry.runtime),
                nameof(Entry.@event),
                nameof(Entry.thread),
                nameof(Entry.currentTime),
                nameof(Entry.bar),
                nameof(Entry.frame),
                nameof(Entry.runId)
            });
        }

        public static IEnumerable<string> ToCsv() {

            var localTimes = times;
            times = new ConcurrentLinkedList<Entry>();

            foreach (var dict in localTimes) {
                yield return string.Join(", ", new string[] {
                dict.runtime,
                dict.@event,
                dict.thread,
                dict.currentTime,
                dict.bar,
                dict.frame,
                dict.runId,
            });
            }
        }
    }
}
