﻿using Ants.Domain;
using System.Drawing;
using System.Runtime.Intrinsics;

public enum AntType
{
    Basic,
    EyebalMaxious,
    Grenadier,
    Flag,
    Gather,
    MobileCover,
    HoneyPot,
    Healer,
    Arty,
    BasicNarrow,
    BasicSlow,
    BasicFast,
    AmbushPreditor,
    ArcShooter,
    WebLine,
}



public static class AntFactory
{
    public const int AmbushPreditorRadius = 300;
    public const double AssumedHp = 50;

    public static Dictionary<AntType, (Key key, int pageIndex)> antKeys = new Dictionary<AntType, (Key key, int pageIndex)>
    {
        [AntType.Flag] = (Key.Key1, 0),
        [AntType.Gather] = (Key.Q, 0),
        [AntType.AmbushPreditor] = (Key.A, 0),
        [AntType.Grenadier] = (Key.Z, 0),

        [AntType.Basic] = (Key.Key1, 1),
        [AntType.BasicNarrow] = (Key.Q, 1),
        [AntType.BasicFast] = (Key.A, 1),
        [AntType.BasicSlow] = (Key.Z, 1),

        [AntType.ArcShooter] = (Key.Key1,2),
        [AntType.Healer] = (Key.Q, 2),
        [AntType.Arty] = (Key.A, 2),
        [AntType.MobileCover] = (Key.Z, 2),

        [AntType.HoneyPot] = (Key.Q, 3),
        [AntType.EyebalMaxious] = (Key.A, 3),
        [AntType.WebLine] = (Key.Z, 3),
    };

    //public static Dictionary<Key, AntAction> antKeys = new Dictionary<Key, AntAction>
    //{
    //    // basics
    //    [Key.Key1] = AntAction.Make(new EggKeyAction(AntType.Basic), "basic"),
    //    [Key.Key2] = AntAction.Make(new EggKeyAction(AntType.BasicNarrow), "narrow"),
    //    [Key.Key3] = AntAction.Make(new EggKeyAction(AntType.BasicFast), "fast"),
    //    [Key.Key4] = AntAction.Make(new EggKeyAction(AntType.BasicSlow), "slow"),
    //    [Key.Key5] = AntAction.Make(new EggKeyAction(AntType.ArcShooter), "arc"),

    //    // support units
    //    [Key.Q] = AntAction.Make(new EggKeyAction(AntType.EyebalMaxious), "eyeballMaxus"), // W smoke
    //    [Key.E] = AntAction.Make(new EggKeyAction(AntType.Grenadier), "grenadier"), // E trhow, R push off
    //    [Key.T] = AntAction.Make(new EggKeyAction(AntType.Healer), "healer"), 
    //    [Key.A] = AntAction.Make(new EggKeyAction(AntType.Arty), "arty"), // S shoot
    //    [Key.D] = AntAction.Make(new EggKeyAction(AntType.MobileCover), "mobile cover"),
    //    [Key.F] = AntAction.Make(new EggKeyAction(AntType.AmbushPreditor), "spider"), // G trp, Z sprint

    //    // eco
    //    [Key.X] = AntAction.Make(new EggKeyAction(AntType.Flag), "flag"),
    //    [Key.C] = AntAction.Make(new EggKeyAction(AntType.Gather), "gatherer"),
    //    [Key.F] = AntAction.Make(new EggKeyAction(AntType.HoneyPot), "honeypot"),
    //};

    public static AntState Make(Guid id, AntType type, int side, Vector128<double> globalPosition)
    {

        switch (type)
        {
            case AntType.Flag:
                {
                    var maxHp = 100;
                    return new AntState(
                        maxHp,
                        new AntState.Immutable(
                            id,
                            side,
                            maxHp,
                            lineOfSightFar: 0,
                            lineOfSight: 0,
                            lineOfSightDetection: 0,
                            antKeys.ToDictionary(x=>x.Value, x=> AntAction.Make(new EggKeyAction(x.Key), x.Key.ToString())),
                            MudMode(type),
                            type),
                        globalPosition,
                        radius: AntSize(type))
                    {
                        legs = new LegsState(speed: 3),
                        shield = new ShieldState(
                                new ShieldState.Immutable(shieldedSpeedMultiplier: 1),
                                new BaseShieldState.BaseImmutable(
                                    frontArmor: 30,
                                    backArmor: 1,
                                    armorAngleRads: ToRads(40.0))),
                    };
                }
            case AntType.Grenadier:
                {
                    var maxHp = 25;
                    return new AntState
                        (
                            maxHp,
                            new AntState.Immutable(
                                id,
                                side,
                                maxHp,
                                lineOfSightFar: 500,
                                lineOfSight: 500,
                                lineOfSightDetection: 500,
                                new Dictionary<(Key key, int pageIndex), AntAction>() {
                                    {(Key.Key1,0),  AntAction.Make(new GrenadeNeedsClick(), "throw grendade")},
                                    {(Key.Q,0),  AntAction.Make(new ShockwaveNeedsClick(), "push off")}
                                },
                                MudMode(type),
                                type),
                            globalPosition,
                            radius: AntSize(type)
                        )
                    {
                        legs = new LegsState(speed: 4),
                        grenadeLauncher = new GrenadeLauncherState(0, new GrenadeLauncherState.Immuntable(
                            speed: 5,
                            maxRange: 300,
                            reloadTime: 100,
                            inner_speed: 10,
                            inner_maxRange: 50,
                            inner_damage: 4,
                            inner_ignoreCovoerFor: 100,
                            inner_pierce: 10,
                            inner_stun: 20,
                            inner_knockBack: 10
                            )),
                        shockwaverState = new ShockwaverState(600)
                    };
                }
            case AntType.Basic:
                {
                    var maxHp = 100;
                    return new AntState
                        (
                            maxHp,
                            new AntState.Immutable(
                                id,
                                side,
                                maxHp,
                                lineOfSightFar: 500,
                                lineOfSight: 500,
                                lineOfSightDetection: 500,
                                new Dictionary<(Key key, int pageIndex), AntAction>(),
                                MudMode(type),
                                type),
                            globalPosition,
                            radius: AntSize(type)
                        )
                    {
                        legs = new LegsState(speed: 2),
                        gun = new GunState(new GunState.Immutable(
                                pierce: 10,
                                damage: 6,
                                shotSpeed: 20,
                                scatterAngleRad: ToRads(3),
                                relaodTimeTick: 15,
                                canMoveAndShoot: true,
                                canMoveAndRelaod: true,
                                range: 1500,
                                rangeScatter: 100,
                                setUpTimeTicks: 0,
                                shots: 1,
                                ignoreCoverFor: 500,
                                ignoreCoverForScatter: 200,
                                autoShoot: true,
                                stun: 0,
                                knockBack: 1
                                )),
                        shield = new ShieldState(
                                new ShieldState.Immutable(shieldedSpeedMultiplier: 1),
                                new BaseShieldState.BaseImmutable(
                                    frontArmor: 30,
                                    backArmor: 1,
                                    armorAngleRads: ToRads(40.0))),
                    };
                }
            case AntType.EyebalMaxious:
                {
                    var maxHp = 40;
                    return new AntState
                        (
                            maxHp,
                            new AntState.Immutable(
                                id,
                                side,
                                maxHp,
                                lineOfSightFar: 1500,
                                lineOfSight: 1000,
                                lineOfSightDetection: 500,
                                new Dictionary<(Key key, int pageIndex), AntAction>()
                                {
                                    [(Key.Key1,0)] = AntAction.Make(new SmokeNeedsClick(), "place smoke")
                                },
                                MudMode(type),
                                type),
                            globalPosition,
                            radius: AntSize(type)
                        )
                    {
                        legs = new LegsState(speed: 3),
                        eyeballMaxious = new EyeballMaxiousState(Pathing.eyeball_lenght, new List<Vector128<double>>()),
                        smokePlacerState = new SmokePlacerState(null)
                    };
                }
            case AntType.Gather:
                {
                    var maxHp = 25;
                    return new AntState(
                        maxHp,
                        new AntState.Immutable(
                            id,
                            side,
                            maxHp,
                            lineOfSightFar: 0,
                            lineOfSight: 0,
                            lineOfSightDetection: 0,
                            new Dictionary<(Key key, int pageIndex), AntAction>(),
                            MudMode(type),
                            type),
                        globalPosition,
                        radius: AntSize(type))
                    {
                        legs = new LegsState(speed: 3),
                        resouceGatherer = new ResourceGathererState(),
                        honeyPotState = new HoneyPotState(0,0, 2)
                    };
                }
            case AntType.MobileCover:
                {
                    var maxHp = 100;
                    return new AntState
                        (
                            maxHp,
                            new AntState.Immutable(
                                id,
                                side,
                                maxHp,
                                lineOfSightFar: 0,
                                lineOfSight: 0,
                                lineOfSightDetection: 0,
                                new Dictionary<(Key key, int pageIndex), AntAction>(),
                                MudMode(type),
                                type),
                            globalPosition,
                            radius: AntSize(type)
                        )
                    {
                        legs = new LegsState(speed: 2),
                        shield = new ShieldState(
                                new ShieldState.Immutable(shieldedSpeedMultiplier: 1),
                                new BaseShieldState.BaseImmutable(
                                    frontArmor: 40,
                                    backArmor: 1,
                                    armorAngleRads: ToRads(60.0))),
                    };
                }
            case AntType.HoneyPot:
                {
                    var maxHp = 250;
                    return new AntState(
                        maxHp,
                        new AntState.Immutable(
                            id,
                            side,
                            maxHp,
                            lineOfSightFar: 0,
                            lineOfSight: 0,
                            lineOfSightDetection: 0,
                            new Dictionary<(Key key, int pageIndex), AntAction>(),
                            MudMode(type),
                            type),
                        globalPosition,
                        radius: AntSize(type))
                    {
                        legs = new LegsState(speed: 3),
                        honeyPotState = new HoneyPotState(0, 0, 3),
                    };
                }
            case AntType.Healer:
                {
                    var maxHp = 30;
                    return new AntState
                        (
                            maxHp,
                            new AntState.Immutable(
                                id,
                                side,
                                maxHp,
                                lineOfSightFar: 0,
                                lineOfSight: 0,
                                lineOfSightDetection: 0,
                                new Dictionary<(Key key, int pageIndex), AntAction>(),
                                MudMode(type),
                                type),
                            globalPosition,
                            radius: AntSize(type)
                        )
                    {
                        legs = new LegsState(speed: 2),
                        healGun = new HealGunState(new HealGunState.Immutable(
                                healing: 1,
                                shotSpeed: 20,
                                scatterAngleRad: ToRads(1),
                                relaodTimeTick: 1,//15,
                                canMoveAndShoot: true,
                                canMoveAndRelaod: true,
                                range: 400,
                                rangeScatter: 0,
                                setUpTimeTicks: 0,
                                shots: 1,
                                ignoreCoverFor: 400,
                                ignoreCoverForScatter: 0
                                )),
                    };
                }
            case AntType.Arty:
                {
                    var maxHp = 30;
                    return new AntState
                        (
                            maxHp,
                            new AntState.Immutable(
                                id,
                                side,
                                maxHp,
                                lineOfSightFar: 0,
                                lineOfSight: 0,
                                lineOfSightDetection: 0,
                                new Dictionary<(Key key, int pageIndex), AntAction>
                                {
                                    [(Key.Key1, 0)] = AntAction.Make(new ArtyNeedsClick(), "fire artillery")
                                },
                                MudMode(type),
                                type),
                            globalPosition,
                            radius: AntSize(type)
                        )
                    {
                        legs = new LegsState(speed: 2),
                        artyState = new ArtyState(0, new ArtyState.Immutable(
                                damage: 3,
                                speed: 10,
                                ignoreCoverFor: 1000,
                                pierce: 1,
                                stun: 0,
                                subShot_runForDistance: 150,
                                subShot_speed: 5,
                                runForTicks: FramesPerSecond.framesPerSecond*30,
                                subShot_knockBack: 0
                                ),
                                null),
                    };
                }
            case AntType.BasicNarrow:
                {
                    var maxHp = 100;
                    return new AntState
                        (
                            maxHp,
                            new AntState.Immutable(
                                id,
                                side,
                                maxHp,
                                lineOfSightFar: 500,
                                lineOfSight: 500,
                                lineOfSightDetection: 500,
                                new Dictionary<(Key key, int pageIndex), AntAction>(),
                                MudMode(type),
                                type),
                            globalPosition,
                            radius: AntSize(type)
                        )
                    {
                        legs = new LegsState(speed: 2),
                        gun = new GunState(new GunState.Immutable(
                                pierce: 20,
                                damage: 12,
                                shotSpeed: 20,
                                scatterAngleRad: ToRads(3),
                                relaodTimeTick: 10,
                                canMoveAndShoot: true,
                                canMoveAndRelaod: true,
                                range: 1500,
                                rangeScatter: 100,
                                setUpTimeTicks: 0,
                                shots: 1,
                                ignoreCoverFor: 500,
                                ignoreCoverForScatter: 200,
                                autoShoot: true,
                                stun: 0,
                                knockBack: 1
                                )),
                        shield = new ShieldState(
                                new ShieldState.Immutable(shieldedSpeedMultiplier: 1),
                                new BaseShieldState.BaseImmutable(
                                    frontArmor: 30,
                                    backArmor: 1,
                                    armorAngleRads: ToRads(10.0))),
                    };
                }
            case AntType.BasicSlow:
                {
                    var maxHp = 100;
                    return new AntState
                        (
                            maxHp,
                            new AntState.Immutable(
                                id,
                                side,
                                maxHp,
                                lineOfSightFar: 500,
                                lineOfSight: 500,
                                lineOfSightDetection: 500,
                                new Dictionary<(Key key, int pageIndex), AntAction>(),
                                MudMode(type),
                                type),
                            globalPosition,
                            radius: AntSize(type)
                        )
                    {
                        legs = new LegsState(speed: 1),
                        gun = new GunState(new GunState.Immutable(
                                pierce: 10,
                                damage: 6,
                                shotSpeed: 20,
                                scatterAngleRad: ToRads(5),
                                relaodTimeTick: 5,
                                canMoveAndShoot: true,
                                canMoveAndRelaod: true,
                                range: 500,
                                rangeScatter: 50,
                                setUpTimeTicks: 0,
                                shots: 1,
                                ignoreCoverFor: 500,
                                ignoreCoverForScatter: 200,
                                autoShoot: true,
                                stun: 0,
                                knockBack: 1
                                )),
                        shield = new ShieldState(
                                new ShieldState.Immutable(shieldedSpeedMultiplier: 1),
                                new BaseShieldState.BaseImmutable(
                                    frontArmor: 30,
                                    backArmor: 1,
                                    armorAngleRads: ToRads(120.0))),
                    };
                }
            case AntType.BasicFast:
                {
                    var maxHp = 50;
                    return new AntState
                        (
                            maxHp,
                            new AntState.Immutable(
                                id,
                                side,
                                maxHp,
                                lineOfSightFar: 500,
                                lineOfSight: 500,
                                lineOfSightDetection: 500,
                                new Dictionary<(Key key, int pageIndex), AntAction>(),
                                MudMode(type),
                                type),
                            globalPosition,
                            radius: AntSize(type)
                        )
                    {
                        legs = new LegsState(speed: 4),
                        gun = new GunState(new GunState.Immutable(
                                pierce: 10,
                                damage: 6,
                                shotSpeed: 20,
                                scatterAngleRad: ToRads(3),
                                relaodTimeTick: 15,
                                canMoveAndShoot: true,
                                canMoveAndRelaod: true,
                                range: 1500,
                                rangeScatter: 100,
                                setUpTimeTicks: 0,
                                shots: 1,
                                ignoreCoverFor: 500,
                                ignoreCoverForScatter: 200,
                                autoShoot: true,
                                stun: 0,
                                knockBack: 1
                                )),
                        shield = new ShieldState(
                                new ShieldState.Immutable(shieldedSpeedMultiplier: 1),
                                new BaseShieldState.BaseImmutable(
                                    frontArmor: 30,
                                    backArmor: 1,
                                    armorAngleRads: ToRads(40.0))),
                    };
                }
            case AntType.AmbushPreditor:
                {
                    var maxHp = 40;
                    return new AntState
                        (
                            maxHp,
                            new AntState.Immutable(
                                id,
                                side,
                                maxHp,
                                lineOfSightFar: 500,
                                lineOfSight: 500,
                                lineOfSightDetection: 500,
                                new Dictionary<(Key key, int pageIndex), AntAction>() {
                                    [(Key.Key1,0)] = AntAction.Make(new ZapperNeedsClick(), "place trap"),
                                    [(Key.Q,0)] = AntAction.Make(new SprintNeedsClick(), "sprint")
                                },
                                MudMode(type),
                                type),
                            globalPosition,
                            radius: AntSize(type)
                        )
                    {
                        legs = new LegsState(speed: 1),
                        sprint = new SprintState(new SprintState.Immutable(FramesPerSecond.framesPerSecond*5 , 5, .2)),
                        zapperState = new ZapperState(
                            new ZapperState.Immutable(
                                setUpTime: FramesPerSecond.framesPerSecond * 5,
                                reloadTime: 15,
                                range: 300
                                ),
                            0
                            ),
                        shield = new SheildWhileImobile(
                                new BaseShieldState.BaseImmutable(
                                    frontArmor: 20,
                                    backArmor: 1,
                                    armorAngleRads: ToRads(40.0))),
                    };
                }
            case AntType.ArcShooter:
                {
                    var maxHp = 50;
                    return new AntState
                        (
                            maxHp,
                            new AntState.Immutable(
                                id,
                                side,
                                maxHp,
                                lineOfSightFar: 500,
                                lineOfSight: 500,
                                lineOfSightDetection: 500,
                                new Dictionary<(Key key, int pageIndex), AntAction>(),
                                MudMode(type),
                                type),
                            globalPosition,
                            radius: AntSize(type)
                        )
                    {
                        legs = new LegsState(speed: 2),
                        arcGun = new ArcGunState(new ArcGunState.Immutable(
                                pierce: 10,
                                damage: .1,
                                shotSpeed: 20,
                                scatterAngleRad: ToRads(3),// if I put this to 0, it stops shooting, why? TODO
                                relaodTimeTick: 15,
                                canMoveAndShoot: true,
                                canMoveAndRelaod: true,
                                range: 3000,
                                rangeScatter: 100,
                                setUpTimeTicks: 0,
                                shots: 1,
                                ignoreCoverFor: 500,
                                ignoreCoverForScatter: 200,
                                autoShoot: true,
                                stun: 0,
                                knockBack: 1,
                                radius: 500)),
                    };
                }
            case AntType.WebLine:
                {
                    var maxHp = 50;
                    return new AntState
                        (
                            maxHp,
                            new AntState.Immutable(
                                id,
                                side,
                                maxHp,
                                lineOfSightFar: 500,
                                lineOfSight: 500,
                                lineOfSightDetection: 500,
                                new Dictionary<(Key key, int pageIndex), AntAction>(),
                                MudMode(type),
                                type),
                            globalPosition,
                            radius: AntSize(type)
                        )
                    {
                        legs = new LegsState(speed: 2),
                        zapperGun = new ZapperGunState(new ZapperGunState.Immutable(
                              shotSpeed: 40,
                                shotRange: 600,
                                shotRangeScatter: 50,
                                shotStun: 20,
                                shotScatterAngleRad: ToRads(3),
                                ignoreCoverFor: 500,
                                ignoreCoverForScatter: 50,
                                shotKnockBack: 0,
                                reload: 15), null, 0, false, null),
                    };
                }
            default:
                throw new Exception($"unexpect AntType {type}");
        }
    }

    public static double ToRads(double v)
    {
        return (v * Math.Tau) / 360;
    }

    public static double AntSize(AntType type)
    {
        switch (type)
        {
            case AntType.Grenadier:
                return Pathing.unitSize_Medium;
            case AntType.Flag:
                return Pathing.unitSize_Medium;
            case AntType.Basic:
                return Pathing.unitSize_Medium;
            case AntType.EyebalMaxious:
                return Pathing.unitSize_Small;
            case AntType.Gather:
                return Pathing.unitSize_Medium;
            case AntType.MobileCover:
                return Pathing.unitSize_Large;
            case AntType.HoneyPot:
                return Pathing.unitSize_Large;
            case AntType.Healer:
                return Pathing.unitSize_Medium;
            case AntType.Arty:
                return Pathing.unitSize_Large;
            case AntType.BasicNarrow:
                return Pathing.unitSize_Medium;
            case AntType.BasicFast:
                return Pathing.unitSize_Medium;
            case AntType.BasicSlow:
                return Pathing.unitSize_Medium;
            case AntType.AmbushPreditor:
                return Pathing.unitSize_Small;
            case AntType.ArcShooter:
                return Pathing.unitSize_Medium;
            case AntType.WebLine:
                return Pathing.unitSize_Small;
            default:
                throw new NotImplementedException($"size of type {type} not defined");
        }
    }

    public static string Model(AntType antType, double size)
    {
        return antType switch
        {
            AntType.Grenadier => "res://Models/Test.tscn",
            AntType.Flag => "res://Models/Flag.tscn",
            AntType.Basic => "res://Models/Medium.tscn",
            AntType.EyebalMaxious => "res://Models/Light.tscn",
            AntType.Gather => "res://Models/Medium.tscn",
            AntType.MobileCover => "res://Models/Heavy.tscn",
            AntType.HoneyPot => "res://Models/Unpacker_Large.tscn",
            AntType.Healer => "res://Models/Flag.tscn",
            AntType.Arty => "res://Models/Heavy.tscn",
            AntType.BasicNarrow => "res://Models/Medium_Skinny.tscn",
            AntType.BasicFast => "res://Models/Medium_Small.tscn",
            AntType.BasicSlow => "res://Models/Medium_Fat.tscn",
            AntType.AmbushPreditor => "res://Models/Medium_Fat.tscn",
            AntType.ArcShooter => "res://Models/Medium_Skinny.tscn",
            AntType.WebLine => "res://Models/Flag.tscn",
            _ => throw new NotImplementedException($"unexpected type {antType}"),
        };
    }

    public static string? SheildModel(AntType antType)
    {
        return antType switch
        {
            AntType.Grenadier => null,
            AntType.Flag => null,
            AntType.Basic => "res://Models/Shield.tscn",
            AntType.EyebalMaxious => null,
            AntType.Gather => null,
            AntType.MobileCover => "res://Models/Shield.tscn",
            AntType.HoneyPot => null,
            AntType.Healer => null,
            AntType.Arty => null,
            AntType.BasicNarrow => null,
            AntType.BasicFast => null,
            AntType.BasicSlow => null,
            AntType.AmbushPreditor => null,
            AntType.ArcShooter => null,
            AntType.WebLine => null,
            _ => throw new NotImplementedException($"unexpected type {antType}"),
        };
    }

    public static string? GunModel(AntType antType)
    {
        return antType switch
        {
            AntType.Grenadier => null,
            AntType.Flag => null,
            AntType.Basic => "res://Models/Gun.tscn",
            AntType.EyebalMaxious => null,
            AntType.Gather => null,
            AntType.MobileCover => null,
            AntType.HoneyPot => null,
            AntType.Healer => "res://Models/Gun.tscn",
            AntType.Arty => null,
            AntType.BasicNarrow => "res://Models/Gun.tscn",
            AntType.BasicFast => "res://Models/Gun.tscn",
            AntType.BasicSlow => "res://Models/Gun.tscn",
            AntType.AmbushPreditor => null,
            AntType.ArcShooter => null,
            AntType.WebLine => null,
            _ => throw new NotImplementedException($"unexpected type {antType}"),
        };
    }

    public static int ActionPriory(AntType antType) {
        return antType switch
        {
            AntType.Grenadier => 1,
            AntType.Flag => 2,
            AntType.Basic => 3,
            AntType.EyebalMaxious => 4,
            AntType.Arty => 5,
            AntType.AmbushPreditor => 6,
            AntType.HoneyPot => 7,
            AntType.Healer => 8,
            AntType.Gather => 9,
            AntType.BasicNarrow => 10,
            AntType.BasicFast => 11,
            AntType.BasicSlow => 12,
            AntType.MobileCover => 13,
            AntType.ArcShooter => 14,
            AntType.WebLine => 15,
            _ => throw new NotImplementedException($"unexpected type {antType}"),
        };
    }

    //public static double Population(AntState ant)
    //{
    //    return ant.immutable.type switch
    //    {
    //        AntType.Unpacker_Old => Population(ant.unpacker!.immutable.toAdd),
    //        _ => 1,
    //    };
    //}

    //public static double Population(AntType type)
    //{
    //    return type switch
    //    {
    //        AntType.Unpacker_Old => throw new Exception("an unpackers population depends on what it unpacks into"),
    //        _ => 1,
    //    };
    //}

    public static AntState MakeUnpacker(int side, Guid id, Vector128<double> globalPosition, int buildTime, AntType type, Guid unpackerId)
    {
        throw new NotImplementedException();
        //var maxHp = 50;
        //return new AntState(
        //    maxHp,
        //    new AntState.Immutable(
        //        unpackerId,
        //        side,
        //        maxHp,
        //        lineOfSightFar: 0,
        //        lineOfSight: 0,
        //        lineOfSightDetection: 0,
        //        new Dictionary<Key, IKeyAction>(),
        //        new Dictionary<Key, IPendingClickOrderState>(),
        //        MudMode(type),
        //        AntType.Unpacker_Old),
        //    globalPosition,
        //    radius: AntSize(type))
        //{
        //    unpacker = new UnpackerState(0, new UnpackerState.Immutable(buildTime, type, id)),
        //    legs = new LegsState(4),
        //};
    }

    public static Pathing.Mode MudMode(AntType type)
    {
        switch (type)
        {
            case AntType.Grenadier:
                return Pathing.Mode.SlowMud;
            case AntType.Flag:
                return Pathing.Mode.SlowMud;
            case AntType.Basic:
                return Pathing.Mode.SlowMud;
            case AntType.EyebalMaxious:
                return Pathing.Mode.SlowMud;
            case AntType.Gather:
                return Pathing.Mode.SlowMud;
            case AntType.MobileCover:
                return Pathing.Mode.ImpassableMud;
            case AntType.HoneyPot:
                return Pathing.Mode.ImpassableMud;
            case AntType.Healer:
                return Pathing.Mode.ImpassableMud;
            case AntType.Arty:
                return Pathing.Mode.ImpassableMud;
            case AntType.BasicNarrow:
                return Pathing.Mode.SlowMud;
            case AntType.BasicFast:
                return Pathing.Mode.SlowMud;
            case AntType.BasicSlow:
                return Pathing.Mode.SlowMud;
            case AntType.AmbushPreditor:
                return Pathing.Mode.IgnoreMud;
            case AntType.ArcShooter:
                return Pathing.Mode.SlowMud;
            case AntType.WebLine:
                return Pathing.Mode.IgnoreMud;
            default:
                throw new NotImplementedException($"mode of type {type} not defined");
        }
    }

    internal static int Cost(AntState antState)
    {
        if (antState.unpacker != null)
        {
            return Cost(antState.unpacker.immutable.toAdd);
        }
        return Cost(antState.immutable.type);
    }

    internal static int Cost(AntType type)
    {
        return type switch
        {
            AntType.Grenadier => 100,
            AntType.Flag => 100,
            AntType.Basic => 100,
            AntType.EyebalMaxious => 100,
            AntType.Gather => 0,
            AntType.MobileCover => 100,
            AntType.HoneyPot => 50,
            AntType.Healer => 100,
            AntType.Arty => 100,
            AntType.BasicNarrow => 100,
            AntType.BasicFast => 100,
            AntType.BasicSlow => 100,
            AntType.AmbushPreditor => 100,
            AntType.ArcShooter => 100,
            AntType.WebLine => 0,
            _ => throw new NotImplementedException($"cost of type {type} not defined")
        };
    }

    internal static int GathererCost(AntType type)
    {
        return type switch
        {
            AntType.Grenadier => 0,
            AntType.Flag => 0,
            AntType.Basic => 0,
            AntType.EyebalMaxious => 0,
            AntType.Gather => 100,
            AntType.MobileCover => 0,
            AntType.HoneyPot => 0,
            AntType.Healer => 0,
            AntType.Arty => 0,
            AntType.BasicNarrow => 0,
            AntType.BasicFast => 0,
            AntType.BasicSlow => 0,
            AntType.AmbushPreditor => 0,
            AntType.ArcShooter => 0,
            AntType.WebLine => 0,
            _ => throw new NotImplementedException($"cost of type {type} not defined")
        };
    }

    internal static int Population(AntType type)
    {
        return type switch
        {
            AntType.Grenadier => 1,
            AntType.Flag => 1,
            AntType.Basic => 1,
            AntType.EyebalMaxious => 1,
            AntType.Gather => 0,
            AntType.MobileCover => 1,
            AntType.HoneyPot => 0,
            AntType.Healer => 1,
            AntType.Arty => 1,
            AntType.BasicNarrow => 1,
            AntType.BasicFast => 1,
            AntType.BasicSlow => 1,
            AntType.AmbushPreditor => 1,
            AntType.ArcShooter => 1,
            AntType.WebLine => 0,
            _ => throw new NotImplementedException($"cost of type {type} not defined")
        };
    }
}