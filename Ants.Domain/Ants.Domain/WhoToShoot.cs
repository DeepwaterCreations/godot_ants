﻿using Ants.Domain.Infrastructure;
using Ants.Domain.State;
using Prototypist.Toolbox;
using Prototypist.Toolbox.Object;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Numerics;
using System.Reflection.Metadata;
using System.Runtime.InteropServices;
using System.Runtime.Intrinsics;
using System.Runtime.Intrinsics.X86;
using static System.Net.Mime.MediaTypeNames;
using static WhoToShoot;
public static class WhoToShoot
{

    private class Node
    {
        public (int x, int y) position;
        public readonly LinkedList<Node> nexts = new LinkedList<Node>();

        public Node((int x, int y) position)
        {
            this.position = position;
        }

        public override bool Equals(object? obj)
        {
            return obj is Node node &&
                   position.Equals(node.position);
        }

        public override int GetHashCode()
        {
            return position.GetHashCode();
        }
    }

    // preformance, we do this a lot
    private static readonly (double damage, Vector128<double> shootAt) earlyExit = (0, Vector128.Create(0.0, 0.0));

    /// <returns>null if there is no point in shooting</returns>
    public static (Vector128<double> point, Vector128<double> targetVelocity, bool addScatter)? ShootAt(Target[] targets, IBlocksShots[] rocks, CoverState[] cover, Vector128<double> shoortFrom, GunState gunState, int tick, Guid shooterId, double shooterRaidus)
    {
        var segments = BuildSegments(targets, rocks, cover, shoortFrom, shooterRaidus);

        var segmentsList = segments.AsEnumerable().OrderBy(x => x.angle1).ToList();

        var angles = BuildAngles(gunState.immutable.scatterAngleRad, segmentsList, gunState.immutable.shotSpeed);

        if (!angles.Any())
        {
            return null;
        }

        var bestTracker = new BestTracker<(Vector128<double> point, Vector128<double> velocity, bool addScatter)>();

        var first = angles.First();

        var angle0 = first.angle;
        var distance0 = first.distance;
        var velocity0 = first.targetSpeed;
        var shootAt0 = first.target;

        var (firstUpperAngle, firstLowerAngle1) = first.addScatter ? (Segment2.FixAngle(first.angle + gunState.immutable.scatterAngleRad), first.angle) : (first.angle,Segment2.FixAngle(first.angle - gunState.immutable.scatterAngleRad));
        var (nextAdd, targetedSegments) = BuildTargetedSegments(segmentsList, firstUpperAngle, firstLowerAngle1);

        double firstValue = ValueFromSegments(
            shoortFrom, 
            gunState.immutable.scatterAngleRad, 
            gunState.immutable.range, 
            gunState.immutable.rangeScatter, 
            gunState.immutable.ignoreCoverFor, 
            gunState.immutable.ignoreCoverForScatter, 
            gunState.immutable.pierce,
            targetedSegments, 
            firstLowerAngle1, 
            firstUpperAngle, 
            tick,
            shooterId);
        {
            if (distance0 == 0)
            {
                throw new Exception("shooting at something on top of you?");
            }
            //var shootAt = Avx.Add(shoortFrom, Vector128.Create(distance0 * Math.Cos(angle0), distance0 * Math.Sin(angle0)));

            bestTracker.PossibleBest(firstValue, (shootAt0, velocity0, first.addScatter));
        }

        foreach (var (angle, distance, targetVelocity, shootAt, addScatter, time) in angles.Skip(1))
        {
            double upperAngle, lowerAngle;
            // mutates targetedSegments
            nextAdd = UpdateTargetSegments(gunState.immutable.scatterAngleRad, segmentsList, nextAdd, targetedSegments, angle,
                addScatter, out upperAngle, out lowerAngle);

            double value = ValueFromSegments(
                shoortFrom,
                gunState.immutable.scatterAngleRad,
                gunState.immutable.range,
                gunState.immutable.rangeScatter,
                gunState.immutable.ignoreCoverFor,
                gunState.immutable.ignoreCoverForScatter,
                gunState.immutable.pierce,
                targetedSegments,
                lowerAngle,
                upperAngle,
                tick,
                shooterId);

            if (distance == 0)
            {
                throw new Exception("shooting at something on top of you?");
            }
            //var shootAt = Avx.Add(shoortFrom, Vector128.Create(distance * Math.Cos(angle), distance * Math.Sin(angle)));
            bestTracker.PossibleBest(value, (shootAt, targetVelocity, addScatter));
        }

        return bestTracker.Winner();
    }

    public static (Vector128<double> point, Vector128<double> direction, Vector128<double> velocity, bool addScatter, double time)? ArcShootAt(Target[] targets, IBlocksShots[] rocks, CoverState[] cover, Vector128<double> shoortFrom, ArcGunState gunState, int tick, Guid shooterId)
    {
        var segments = BuildSegmentsArc(targets, rocks, cover, shoortFrom, gunState.immutable.radius, gunState.immutable.shotSpeed);

        var segmentsList = segments.AsEnumerable().OrderBy(x => x.angle1).ToList();

        var angles = BuildAngles(gunState.immutable.scatterAngleRad, segmentsList, gunState.immutable.shotSpeed);

        if (!angles.Any())
        {
            return null;
        }

        var tracker = new BestTracker<(Vector128<double> direction, Vector128<double> velocity, double distance, Vector128<double> shootAt, bool addScatter, double time)>();

        var first = angles.First();
        var angle0 = first.angle;
        var distance0 = first.distance;
        var velocity0 = first.targetSpeed;
        var shootAt0 = first.target;
        var time0 = first.time;

        var (firstUpperAngle, firstLowerAngle) = first.addScatter ? (Segment2.FixAngle(first.angle + gunState.immutable.scatterAngleRad), first.angle) : (first.angle, Segment2.FixAngle(first.angle - gunState.immutable.scatterAngleRad));
        var (nextAdd, targetedSegments) = BuildTargetedSegments(segmentsList, firstUpperAngle, firstLowerAngle);

        double firstValue = ArcValueFromSegments(
            shoortFrom,
            gunState.immutable.scatterAngleRad,
            gunState.immutable.range,
            gunState.immutable.rangeScatter,
            gunState.immutable.ignoreCoverFor,
            gunState.immutable.ignoreCoverForScatter,
            gunState.immutable.pierce,
            targetedSegments,
            firstLowerAngle,
            firstUpperAngle,
            tick,
            shooterId,
            gunState.immutable.radius);

        {
            if (distance0 == 0)
            {
                throw new Exception("shooting at something on top of you?");
            }
            var direction = Vector128.Create(Math.Cos(angle0), Math.Sin(angle0));
            tracker.PossibleBest(firstValue, (direction, velocity0, distance0, shootAt0, first.addScatter, time0));
        }

        foreach (var (angle, distance, targetVelocity, shootAt, addScatter, time) in angles.Skip(1))
        {
            double upperAngle, lowerAngle;
            // mutates targetedSegments
            nextAdd = UpdateTargetSegments(gunState.immutable.scatterAngleRad, segmentsList, nextAdd, targetedSegments, angle, addScatter, out upperAngle, out lowerAngle);

            double value = ArcValueFromSegments(
                shoortFrom,
                gunState.immutable.scatterAngleRad,
                gunState.immutable.range,
                gunState.immutable.rangeScatter,
                gunState.immutable.ignoreCoverFor,
                gunState.immutable.ignoreCoverForScatter,
                gunState.immutable.pierce,
                targetedSegments,
                lowerAngle,
                upperAngle,
                tick,
                shooterId,
                gunState.immutable.radius);

            if (distance == 0)
            {
                throw new Exception("shooting at something on top of you?");
            }
            var direction = Vector128.Create(Math.Cos(angle), Math.Sin(angle));
            tracker.PossibleBest(value, (direction, targetVelocity, distance, shootAt, addScatter, time));

        }

        var winner = tracker.Winner();

        if (winner != null)
        {
            // from where you are shooting from to the center to the shot


            //Vector128<double> at = PositionFromDirection(shoortFrom, gunState.immutable.radius, winner.Value.direction, winner.Value.distance);
            return (winner.Value.shootAt, winner.Value.direction, winner.Value.velocity, winner.Value.addScatter, winner.Value.time);
        }
        return null;
    }

    private static Vector128<double> PositionFromDirection(Vector128<double> shoortFrom, double radius, Vector128<double> direction, double distance)
    {
        var fromToCenter = direction.Rotated(Math.Tau / 4.0).ToLength(radius);
        var centerToTarget = Avx.Multiply(fromToCenter, Vector128.Create(-1.0, -1.0)).Rotated(distance / radius);
        var at = Avx.Add(Avx.Add(shoortFrom, fromToCenter), centerToTarget);
        return at;
    }

    //private static Vector128<double> DirectionFromPosition(Vector128<double> shoortFrom, double radius, Vector128<double> at, double time)
    //{

    //}


    public static (Vector128<double> position, Vector128<double> velocity, bool addScatter)? HealAt(Target[] targets, IBlocksShots[] rocks, CoverState[] cover, Vector128<double> shoortFrom, HealGunState healGunState, int tick, double shooterRadius)
    {
        var segments = BuildSegments(targets, rocks, cover, shoortFrom, shooterRadius);

        var segmentsList = segments.AsEnumerable().OrderBy(x => x.angle1).ToList();

        var angles = BuildAngles(healGunState.immutable.scatterAngleRad, segmentsList, healGunState.immutable.shotSpeed);

        if (!angles.Any())
        {
            return null;
        }

        var tracker = new BestTracker<(Vector128<double> point,  Vector128<double> velocity, bool addScatter)>();

        var first = angles.First();
        var angle0 = angles.First().angle;
        var distance0 = angles.First().distance;
        var velocity0 = angles.First().targetSpeed;
        var shootAt0 = angles.First().target;

        var (firstUpperAngle, firstLowerAngle) = first.addScatter ? (Segment2.FixAngle(first.angle + healGunState.immutable.scatterAngleRad), first.angle) : (first.angle, Segment2.FixAngle(first.angle - healGunState.immutable.scatterAngleRad));
        var (nextAdd, targetedSegments) = BuildTargetedSegments(segmentsList, firstUpperAngle, firstLowerAngle);

        double firstValue = HealingValueFromSegments(shoortFrom, healGunState, targetedSegments, firstLowerAngle, firstUpperAngle, tick);

        {
            if (distance0 == 0)
            {
                throw new Exception("shooting at something on top of you?");
            }
            //var shootAt = Avx.Add(shoortFrom, Vector128.Create(distance0 * Math.Cos(angle0), distance0 * Math.Sin(angle0)));
            tracker.PossibleBest(firstValue, (shootAt0, velocity0, first.addScatter));
        }

        foreach (var (angle, distance, targetVelocity, shootAt, addScatter, time) in angles.Skip(1))
        {
            double upperAngle, lowerAngle;
            nextAdd = UpdateTargetSegments(healGunState.immutable.scatterAngleRad, segmentsList, nextAdd, targetedSegments, angle, addScatter, out upperAngle, out lowerAngle);

            double value = HealingValueFromSegments(shoortFrom, healGunState, targetedSegments, lowerAngle, upperAngle, tick);

            if (distance == 0)
            {
                throw new Exception("shooting at something on top of you?");
            }
            //var shootAt = Avx.Add(shoortFrom, Vector128.Create(distance * Math.Cos(angle), distance * Math.Sin(angle)));
            tracker.PossibleBest(value, (shootAt, targetVelocity, addScatter));
        }

        return tracker.Winner();
    }

    private static int UpdateTargetSegments(double scatterAngleRad, List<Segment2> segmentsList, int nextAdd, List<Segment2> targetedSegments, double angle, bool addScatter, out double upperAngle, out double lowerAngle)
    {
        (upperAngle, lowerAngle) = addScatter ? (Segment2.FixAngle(angle + scatterAngleRad), angle) : (angle, Segment2.FixAngle(angle - scatterAngleRad));
        while (targetedSegments.Any())
        {
            var first = targetedSegments.First();
            if (Segment2.Between(lowerAngle, first.angle2, upperAngle) || Segment2.Between(first.angle1, lowerAngle, first.angle2))
            {
                break;
            }
            targetedSegments.RemoveAt(0);
        }

        var endAt = targetedSegments.Any() ? segmentsList.IndexOf(targetedSegments.First()) : nextAdd - 1;
        endAt = endAt < 0 ? endAt + segmentsList.Count : endAt;
        for (var i = nextAdd; i != endAt; i = (i + 1) % segmentsList.Count)
        {
            var segment = segmentsList[i];
            if (Segment2.Between(lowerAngle, segment.angle1, upperAngle) || Segment2.Between(segment.angle1, upperAngle, segment.angle2))
            {
                targetedSegments.Add(segment);
            }
            else
            {
                if (segment.angle1 > upperAngle)
                {
                    nextAdd = i;
                    break;
                }
            }
        }

        return nextAdd;
    }

    private static (int, List<Segment2>) BuildTargetedSegments(List<Segment2> segmentsList, double upperAngle, double lowerAngle)
    {

        var targetedSegments = new List<Segment2>();
        int nextAdd = 0;
        foreach (var (segment, i) in segmentsList.Select((x, i) => (x, i)))
        {
            if (Segment2.Between(lowerAngle, segment.angle1, upperAngle)
                || Segment2.Between(segment.angle1, upperAngle, segment.angle2))
            {
                targetedSegments.Add(segment);
            }
            else
            {
                if (segment.angle1 > upperAngle)
                {
                    nextAdd = i % segmentsList.Count;
                    break;
                }
            }
        }


        return (nextAdd, targetedSegments);
    }

    private static Vector128<double> ThingSpeed(Thing? thing) {

        if (thing == null) {
            return Vector128.Create(0.0, 0.0);
        }

        return thing.SwitchReturns(
            x => x.target.SwitchReturns(
                y => y.velocity,
                _ => Vector128.Create(0.0, 0.0)),
            _ => Vector128.Create(0.0, 0.0), // this is a little wierd, something this is a bad guy which does move, but we don't want to track it...
            x => Vector128.Create(0.0, 0.0));
    }


    public static IEnumerable<(double angle1, double angle2, double distance, Thing thing, Vector128<double> position1, Vector128<double> position2)> Group(List<Segment2> segmentsList) {

        var res = new List<(double angle1, double angle2, double distance, Thing thing, Vector128<double> position1, Vector128<double> position2)>();

        Segment2? first = null;
        Segment2? last = null;
        
        foreach (var item in segmentsList)
        {
            if (item.thing.Is1(out var target))
            {
                if (first == null)
                {
                    first = item;
                }
                else if (EqualTragets(first.thing.Is1OrThrow().target, target.target))
                {
                    last = item;
                }
                else if (last != null)
                {
                    res.Add((first.angle1, last.angle2, first.distanceOrMaxValueForCover, first.thing, first.position1, last.position2));
                    first = null;
                    last = null;
                }
                else // (last == null)
                {
                    res.Add((first.angle1, first.angle2, first.distanceOrMaxValueForCover, first.thing, first.position1, first.position2));
                    first = null;
                    last = null;
                }
            }
            else {
                if (first == null)
                {
                    // do nothing
                }
                else if (last != null)
                {
                    res.Add((first.angle1, last.angle2, first.distanceOrMaxValueForCover, first.thing, first.position1, last.position2)); 
                    first = null;
                    last = null;
                }
                else // (last == null)
                {
                    res.Add((first.angle1, first.angle2, first.distanceOrMaxValueForCover, first.thing, first.position1, first.position2));
                    first = null;
                    last = null;
                }
            }
        }

        if (first == null) { 
            // do nothing
        }
        else if (last != null)
        {
            res.Add((first.angle1, last.angle2, first.distanceOrMaxValueForCover, first.thing, first.position1, last.position2));
        }
        else // (last == null)
        {
            res.Add((first.angle1, first.angle2, first.distanceOrMaxValueForCover, first.thing, first.position1, first.position2));
        }

        return res;

    }

    private static bool EqualTragets(IOrType<AntState, ZapperSetupState> target1, IOrType<AntState, ZapperSetupState> target2)
    {
        return target1.SwitchReturns(antState1 => target2.SwitchReturns(antState2 => antState1.immutable.id == antState2.immutable.id, _ => false),
            zapperSetup1 => target2.SwitchReturns(_ => false, zapperSetup2 => zapperSetup1.immutable.createdBy == zapperSetup2.immutable.createdBy && zapperSetup1.immutable.createdOnTick == zapperSetup2.immutable.createdOnTick));
    }

    private static (double angle, double distance, Vector128<double> targetSpeed, Vector128<double> target, bool addScatter, double time)[] BuildAngles(double scatterAngleRad, List<Segment2> segmentsList, double shotSpeed)
    {
        return Group(segmentsList).SelectMany(x => {

            return new[]{
                (angle: x.angle1, x.distance, ThingSpeed(x.thing), x.position1, true, x.distance/shotSpeed),
                (angle: x.angle2, x.distance, ThingSpeed(x.thing), x.position2, false, x.distance/shotSpeed)};
        
            }
            )
            .OrderBy(x => x.angle)
            .ToArray();
    }

    private static SegmentsRewrite2 BuildSegments(Target[] targets, IBlocksShots[] rocks, CoverState[] cover, Vector128<double> shoortFrom, double shooterRadius)
    {
        var segments = new SegmentsRewrite2();

        foreach (var item in targets.Select(x => Thing.Make(x)).Union(rocks.Select(x => Thing.Make(x))).Union(cover.Select(x => Thing.Make(x)))
            .OrderBy(x => x.SwitchReturns(
                x => Avx2.Subtract(x.projectedPosition, shoortFrom).LengthSquared(),
                x => Avx2.Subtract(x.Position, shoortFrom).LengthSquared(),
                x => Avx2.Subtract(x.position, shoortFrom).LengthSquared())))
        {
            var (opposite, vector) = item.SwitchReturns(
                target =>
                {
                    var vector = Avx2.Subtract(target.projectedPosition, shoortFrom);
                    var oppisite = target.target.SwitchReturns(x => x.physicsImmunatbleInner.radius, x => ZapperSetupState.Radius);
                    return (oppisite, vector);

                },
                rock =>
                {
                    var vector = Avx2.Subtract(rock.Position, shoortFrom);
                    var oppisite = rock.Radius;
                    return (oppisite, vector);
                },
                cover =>
                {
                    var vector = Avx2.Subtract(cover.position, shoortFrom);
                    var oppisite = cover.physicsImmunatbleInner.radius;
                    return (oppisite, vector);
                });


            var hypotonose = vector.Length();

            if (vector.Length() == 0) {
                Debugger.Launch();
                continue;
            }

            // this can happen when we project the target to move inside the shooter
            // really they will collide so it wouldn't get closer than their combined radius
            if (hypotonose < opposite) {
                hypotonose = opposite+ shooterRadius;
            }

            var angleNearThis = Math.Asin(opposite / hypotonose);
            var angleNearItem = (Math.PI / 2f) - angleNearThis;

            // what happens when vector = <0,0>?
            var oneSide = Avx2.Add(vector, Avx2.Multiply(vector, Vector128.Create(-1.0, -1.0)).ToLength(opposite).Rotated(angleNearItem));
            var otherSide = Avx2.Add(vector, Avx2.Multiply(vector, Vector128.Create(-1.0, -1.0)).ToLength(opposite).Rotated(-angleNearItem));

            var newSegments = Segment2.Make(oneSide.Length(), oneSide.Angle(), otherSide.Angle(), item, Avx.Add(shoortFrom, oneSide), Avx.Add(shoortFrom, otherSide)).ToList();

            segments.AddRange(newSegments.ToList());
        }

        return segments;
    }


   
    private static SegmentsRewrite2 BuildSegmentsArc(Target[] targets, IBlocksShots[] rocks, CoverState[] cover, Vector128<double> shoortFrom, double arcRadius, double shotSpeed)
    {
        var segments = new SegmentsRewrite2();

        segments.AddRange(
        targets.Select(x => Thing.Make(x))
        .Union(rocks.Select(x => Thing.Make(x)))
        .Union(cover.Select(x => Thing.Make(x)))
        .SelectMany(item =>
        {
            var list = new List<Segment2>();

            item.Switch(
                target =>
                {
                    var vector = Avx2.Subtract(target.projectedPosition, shoortFrom);
                    var radius = target.target.SwitchReturns(x => x.physicsImmunatbleInner.radius, x => ZapperSetupState.Radius);

                    list.AddRange(ArcSegmentsOutgoing(arcRadius, item, radius, vector, shoortFrom));
                    list.AddRange(ArcSegmentsReturns(arcRadius, item, radius, vector, shoortFrom));
                },
                rock =>
                {
                    var vector = Avx2.Subtract(rock.Position, shoortFrom);
                    var radius = rock.Radius;

                    list.AddRange(ArcSegmentsOutgoing(arcRadius, item, radius, vector, shoortFrom));
                    list.AddRange(ArcSegmentsReturns(arcRadius, item, radius, vector, shoortFrom));
                    // TODO some of these are friendly ants?which move?
                    // so annoyingly we should probably project their position
                    // ..no friendly ants don't block shots...
                },
                cover =>
                {
                    var vector = Avx2.Subtract(cover.position, shoortFrom);
                    var radius = cover.physicsImmunatbleInner.radius;

                    list.AddRange(ArcSegmentsOutgoing(arcRadius, item, radius, vector, shoortFrom));
                    list.AddRange(ArcSegmentsReturns(arcRadius, item, radius, vector,  shoortFrom));
                });

            

            return list;

        })
        .OrderBy(x => x.distanceOrMaxValueForCover));

        return segments;
    }

    // {C4E2AD4F-28F5-4DAD-A1C4-AE3931FE3270}
    // these two share a lot of code
    // but I think the math is a lot harder to read if I extract the shared code
    // see whoToShoot.png
    // vector is "known"
    // radius is "target radius"
    // arcRadius is "shot radius"
    private static IEnumerable<Segment2> ArcSegmentsOutgoing(double arcRadius, Thing item,  double radius, Vector128<double> vector,  Vector128<double> shoortFrom)
    {

        var vectorLenth = vector.Length();

        // it's out of range
        if (vectorLenth - radius >= arcRadius * 2)
        {
            return Array.Empty<Segment2>();
        }

        // it's not allowed to be closer to us that it's radius
        if (vectorLenth < radius) {
            vectorLenth = radius;
        }

        // find angle to hit one side
        var oneSideDirection = DirectionOutgoingNotNormalized(vectorLenth, arcRadius, arcRadius + radius, vector);
        var oneSideAngle = oneSideDirection.Angle();

        var arc = LawOfConsines(arcRadius, arcRadius + radius, vectorLenth);
        Debug.Assert(arc <= Math.PI);
        var distance1 = arc * arcRadius;

        var position1 = PositionFromDirection(shoortFrom, arcRadius, oneSideDirection, distance1);

        // find the angle to hit the other side
        var side = arcRadius - radius;
        if (vectorLenth > side + arcRadius) {
            // we can't actually get shots around both sides
            // means the SSS defines an invalid triangle
            // vectorLenth >= arcRadius + arcRadius - radius
            // in this case we should hit as much as we can
            // c = vectorLenth - arcRadius
            side = vectorLenth - arcRadius;
        }
        var otherSideDirection = DirectionOutgoingNotNormalized(vectorLenth, arcRadius, side, vector);
        var otherSideAngle = otherSideDirection.Angle();
        var arc2 = LawOfConsines(arcRadius, side, vectorLenth);
        Debug.Assert(arc2 <= Math.PI);
        var distance2 = arc2 * arcRadius;

        var position2 = PositionFromDirection(shoortFrom, arcRadius, otherSideDirection, distance2);

        // find the distance
        // this time the we find the angle in the center

        return Segment2.Make(distance1, otherSideAngle, oneSideAngle, item, position2, position1);
    }


    public static Vector128<double> DirectionNotNormalized(double arcRadius, Vector128<double> from, Vector128<double> to, double shotSpeed, double time)
    {
        var fullTime = arcRadius * Math.Tau / shotSpeed;

        var vector = Avx.Subtract(to, from);

        var vectorLenth = vector.Length();

        // it should be in range
        Debug.Assert(vectorLenth <= arcRadius * 2);

        if (time < fullTime / 2)
        {
            // find angle to hit one side
            return DirectionOutgoingNotNormalized(vectorLenth, arcRadius, arcRadius, vector);
        }
        else {

            return DirectionReturnsNotNormalized(vectorLenth, arcRadius, arcRadius, vector);
        }
    }

    private static Vector128<double> DirectionOutgoingNotNormalized(double vectorLenth, double arcRadius, double arcRadiusModified, Vector128<double> vector)
    {
        var theta1 = LawOfConsines(vectorLenth, arcRadius, arcRadiusModified);
        return vector.Rotated(-(Math.PI / 2.0) + theta1);
    }

    private static Vector128<double> DirectionReturnsNotNormalized(double vectorLenth, double arcRadius, double arcRadiusModified, Vector128<double> vector)
    {
        var theta1 = LawOfConsines(vectorLenth, arcRadius, arcRadiusModified);
        return vector.Rotated(-(Math.PI / 2.0) - theta1);
    }

    // {C4E2AD4F-28F5-4DAD-A1C4-AE3931FE3270}
    // these two share a lot of code
    // but I think the math is a lot harder to read if I extract the shared code
    // see whoToShoot.png
    // vector is "known"
    // radius is "target radius"
    // arcRadius is "shot radius"
    private static IEnumerable<Segment2> ArcSegmentsReturns(double arcRadius, Thing item, double radius, Vector128<double> vector, Vector128<double> shoortFrom)
    {

        var vectorLenth = vector.Length();

        // it's out of range
        if (vectorLenth - radius >= arcRadius * 2)
        {
            return Array.Empty<Segment2>();
        }

        // it's not allowed to be closer to us that it's radius
        if (vectorLenth < radius)
        {
            vectorLenth = radius;
        }

        // find angle to hit one side
        var oneSideDirection = DirectionReturnsNotNormalized(vectorLenth, arcRadius, arcRadius + radius, vector);
        var oneSideAngle = oneSideDirection.Angle();

        var arc = Math.Tau - LawOfConsines(arcRadius, arcRadius + radius, vectorLenth);
        Debug.Assert(arc >= Math.PI);
        var distance1 = arc * arcRadius;

        var position1 = PositionFromDirection(shoortFrom, arcRadius, oneSideDirection, distance1);

        // find the angle to hit the other side
        var side = arcRadius - radius;
        if (vectorLenth > side + arcRadius)
        {
            // we can't actually get shots around both sides
            // means the SSS defines an invalid triangle
            // vectorLenth >= arcRadius + arcRadius - radius
            // in this case we should hit as much as we can
            // c = vectorLenth - arcRadius
            side = vectorLenth - arcRadius;
        }
        // Math.min because sometimes we can't actually get shots around both sides
        // means the SSS defines an invalid triangle
        // vectorLenth >= arcRadius + arcRadius - radius
        // in this case we should hit as much as we can
        // c = vectorLenth - arcRadius
        var otherSideDirection = DirectionReturnsNotNormalized(vectorLenth, arcRadius, side, vector);
        var otherSideAngle = otherSideDirection.Angle();

        var arc2 = Math.Tau - LawOfConsines(arcRadius, arcRadius + radius, vectorLenth);
        Debug.Assert(arc2 >= Math.PI);
        var distance2 = arc2 * arcRadius;

        var position2 = PositionFromDirection(shoortFrom, arcRadius, otherSideDirection, distance2);

        return Segment2.Make(distance1, oneSideAngle, otherSideAngle, item, position1, position2);
    }

    private static double LawOfConsines(double a, double b, double c)
    {
        if (((a * a) + (b * b) - (c * c)) / (2 * a * b) < -1 || ((a * a) + (b * b) - (c * c)) / (2 * a * b) > 1) {
            // sigh sometimes computers just can't do math
            Log.WriteLine($"(a * a + b * b - c * c) / (2 * a * b) is {((a * a) + (b * b) - (c * c)) / (2 * a * b)} should be between 1 and -1");
        }

        if (((a * a) + (b * b) - (c * c)) / (2 * a * b) < -1.1 || ((a * a) + (b * b) - (c * c)) / (2 * a * b) > 1.1)
        {
            // this would get us NaN without the max/min
            throw new Exception($"(a * a + b * b - c * c) / (2 * a * b) is {((a * a) + (b * b) - (c * c)) / (2 * a * b)} should be between 1 and -1");
        }

        return Math.Acos(Math.Max(-1,Math.Min(1, (double)(((a * a) + (b * b) - (c * c)) / (2 * a * b)))));
    }

    // similar to {3FA1B615-6E34-4879-B3E4-3B98A1883CD4}
    public static double HealingValueFromSegments(Vector128<double> shoortFrom, HealGunState gunState, List<Segment2> segments, double from, double to, int tick)
    {
        var value = 0.0;

        foreach (var segment in segments)
        {
            if (segment.thing.Is1(out var target))
            {
                var overlapStart = Math.Max(from, segment.angle1);
                var overlapEnd = Math.Min(to, segment.angle2);
                if (overlapEnd < overlapStart)
                {
                    continue;
                }

                var oddsToHit = (overlapEnd - overlapStart) / gunState.immutable.scatterAngleRad;
                // TODO you probably don't need to calculate these over and over
                var oddsToReach = GunState.OddsToReach(segment.distanceOrMaxValueForCover, gunState.immutable.range, gunState.immutable.rangeScatter);
                var oddsToPassCover = StateAdvancer.OddsToPassCover(segment.covers);
                var percentHealing = oddsToHit * oddsToReach * oddsToPassCover;

                value += ValuOfDamage(target, percentHealing);
            }
        }

        return value;
    }

    // similar to {3FA1B615-6E34-4879-B3E4-3B98A1883CD4}
    private static double ValueFromSegments(
        Vector128<double> shoortFrom, 
        double scatterAngleRad, 
        double range, 
        double rangeScatter, 
        double ignoreCoverFor, 
        double ignoreCoverForScatter,  
        double pierce,
        List<Segment2> segments, 
        double from, 
        double to, 
        int tick, 
        Guid shooterId)
    {
        var value = 0.0;

        foreach (var segment in segments)
        {
            if (segment.thing.Is1(out var target))
            {
                var overlapStart = Math.Max(from, segment.angle1);
                var overlapEnd = Math.Min(to, segment.angle2);
                if (overlapEnd < overlapStart)
                {
                    continue;
                }

                var oddsToHit = (overlapEnd - overlapStart) / scatterAngleRad;
                // TODO you probably don't need to calculate these over and over
                var oddsToReach = GunState.OddsToReach(segment.distanceOrMaxValueForCover, range, rangeScatter);
                var oddsToPassCover = StateAdvancer.OddsToPassCover(segment.covers);
                var damageOnHit =
                    target.target.SwitchReturns(
                        ant =>
                            !target.fullyVisible ? BaseShieldState.HypitheticalDamageArmorPeirce(SimulationSafeRandom.GetDouble(15, 40, ant.immutable.id, tick, shooterId), pierce) :
                            ant.shield != null ? ant.shield.HypitheticalDamage(Avx.Subtract(target.projectedPosition, shoortFrom), pierce, 1.0)
                                                 : 1.0,
                        zapperSetUp => 1.0);

                var percentDamage = oddsToHit * oddsToReach * oddsToPassCover * damageOnHit;

                value += ValuOfDamage(target, percentDamage);
            }
        }

        return value;
    }

    private static double ValuOfDamage(Target target, double percentDamage)
    {
        var targetHp = target.fullyVisible ? target.target.SwitchReturns(x => x.hp, x => x.hp) : AntFactory.AssumedHp;
        var valuePerHp = (1.0 / (1.0 + targetHp));
        return valuePerHp * percentDamage;
    }

    // similar to {3FA1B615-6E34-4879-B3E4-3B98A1883CD4}
    private static double ArcValueFromSegments(
        Vector128<double> shoortFrom,
        double scatterAngleRad,
        double range,
        double rangeScatter,
        double ignoreCoverFor,
        double ignoreCoverForScatter,
        double pierce,
        List<Segment2> segments,
        double from,
        double to,
        int tick,
        Guid shooterId,
        double radius)
        {
        var value = 0.0;

        foreach (var segment in segments)
        {
            if (segment.thing.Is1(out var target))
            {
                var overlapStart = Math.Max(from, segment.angle1);
                var overlapEnd = Math.Min(to, segment.angle2);
                if (overlapEnd < overlapStart)
                {
                    continue;
                }

                var oddsToHit = (overlapEnd - overlapStart) / scatterAngleRad;
                // TODO you probably don't need to calculate these over and over
                var oddsToReach = GunState.OddsToReach(segment.distanceOrMaxValueForCover, range, rangeScatter);
                var oddsToPassCover = StateAdvancer.OddsToPassCover(segment.covers);

                var middle = overlapEnd + overlapStart / 2.0;
                var angle = segment.distanceOrMaxValueForCover / radius;
                var direcitonOfMotion = Vector128.Create(1.0, 0.0).Rotated(angle + middle);

                var damageOnHit =
                    target.target.SwitchReturns(
                        ant =>
                            !target.fullyVisible ? BaseShieldState.HypitheticalDamageArmorPeirce(SimulationSafeRandom.GetDouble(15, 40, ant.immutable.id, tick, shooterId), pierce) :
                            ant.shield != null ? ant.shield.HypitheticalDamage(direcitonOfMotion, pierce, 1.0)
                                                 : 1.0,
                        zapperSetUp => 1.0);

                var percentDamage = oddsToHit * oddsToReach * oddsToPassCover * damageOnHit;

                value += ValuOfDamage(target, percentDamage);
            }
        }

        return value;
    }

    public class Thing<T> : Thing, IIsDefinately<T>
    {
        public Thing(T value)
        {
            this.Value = value ?? throw new ArgumentNullException(nameof(value));
        }

        [NotNull]
        public T Value { get; }

        public override object Representative() => Value;
    }

    public abstract class Thing : OrType<
        Target,
        IBlocksShots,
        CoverState
        >
    {
        public static Thing Make(Target target)
        {
            return new Thing<Target>(target);
        }

        public static Thing Make(IBlocksShots rock)
        {
            return new Thing<IBlocksShots>(rock);
        }

        public static Thing Make(CoverState cover)
        {
            return new Thing<CoverState>(cover);
        }
    }


    public static (Vector128<double> shootAt, double oddsToHit/*approx*/) BestSpotToShootAtAndOddsToHit((Vector128<double> shootAt, double furthestCover)[] hittables, Vector128<double> from, double scatterAngleRad, double ignoreCoverFor, double ignoreCoverForScatter)
    {
        var dbArray = hittables.Select(target =>
        {
            var toCenter = Avx.Subtract(target.shootAt, from);
            // we don't actually have the arc
            // but this should be pretty close

            // arc = angle * radius
            // arc/radius = angle
            var anglePerPoint = MapExtensions.MAP_GRID_SIZE * 2 / toCenter.Length();/*the 2 comes from {4C8DD833-3772-439F-8F44-6A0969CDEF05}*/
            //var oddsForEachPoint = anglePerPoint / scatterAngleRad;

            return (target.shootAt, oddsToHit: hittables
                .Select(y =>
                {
                    var angleToCenter = Math.Abs(toCenter.AngleTo(Avx.Subtract(y.shootAt, from)));
                    var angleToTop = Math.Min(scatterAngleRad / 2.0, angleToCenter + (anglePerPoint / 2.0));
                    var angleToBottom = Math.Max(-scatterAngleRad / 2.0, angleToCenter - (anglePerPoint / 2.0));
                    var angle = angleToTop - angleToBottom;
                    if (angle < 0)
                    {
                        angle = 0;
                    }

                    var oddsToHitAngle = angle / scatterAngleRad;
                    var oddsToPassCover = GunState.OddsToPassCoverAtRange(y.furthestCover, ignoreCoverFor, ignoreCoverForScatter);

                    return oddsToHitAngle * oddsToPassCover;
                })
                .Sum());
        }).ToArray();
        var winner = dbArray.OrderByDescending(x => x.oddsToHit).ThenBy(x => Avx.Subtract(x.shootAt, from).Length()).First();

        return winner;
    }


    public static HashSet<(int x, int y)> CouldHit(Vector128<double> shootFrom, double radius, Vector128<double> projectedPosition)
    {
        // TODO
        // thinking about the edge might be trouble...
        // like... it's kind of hard to konw the odds that you will hit
        // some edge tiles obscure other
        // it might be better to look at a line through the center of the target
        //
        //           00                           .0        
        //          0000                         ..00       
        // shooter 000000 --------------------- ...000 target
        //          0000                         ..00       
        //           00                           .0        
        // 

        // it would be slightly more accurate to curve a little but 🤷‍


        var perpendicular = Avx.Subtract(projectedPosition, shootFrom).NormalizedCopy().Rotated(Math.PI / 2.0);

        var oneEnd = Avx.Add(projectedPosition, Avx.Multiply(perpendicular, (-radius).AsVector()));

        var set = new HashSet<(int x, int y)>() { oneEnd.ToMapPosition() };
        var at = oneEnd;
        // {4C8DD833-3772-439F-8F44-6A0969CDEF05}
        // we step at twice grid size
        // this is counter intuetive
        // but if we step at grid size we get targets like so
        //   ..
        //  #...
        // .##...
        //  .##.
        //   .#
        // they obscure each other
        // being able hit 2 means different things depening on how they are layed out
        // this is very important to BestSpotToShootAtAndOddsToHit
        // because it assumes 
        for (int i = 0; i <= 2 * (radius / (MapExtensions.MAP_GRID_SIZE * 2)); i++)
        {
            at = Avx.Add(at, Avx.Multiply(perpendicular, (MapExtensions.MAP_GRID_SIZE * 2).AsVector()));
            // new Vector128<double>(at.X + (perpendicular.X * (MapExtensions.MAP_GRID_SIZE * 2)), at.Y + (perpendicular.Y * (MapExtensions.MAP_GRID_SIZE * 2)));
            set.Add(at.ToMapPosition());
        }

        return set;
    }

    /// <summary>
    /// approximate
    /// </summary>
    public static double AnglePerCouldHit(Vector128<double> shootFrom, Vector128<double> projectedPosition)
    {
        // {4C8DD833-3772-439F-8F44-6A0969CDEF05}
        return (MapExtensions.MAP_GRID_SIZE * MapExtensions.MAP_GRID_SIZE * 4) / Avx.Subtract(projectedPosition, shootFrom).LengthSquared(); // we square both sides
    }

    private class BestTracker<T>
        where T:struct // it's a value tuple
    {


        double? bestValue = null;
        List<T> ties = new ();
        T? bestCenter2 = null;


        public void PossibleBest(double value, T tuple) {
            if (value <= 0) {
                return;
            }

            // somethings the math doesn't quiet math
            // and we end up with two segments with almost the same value
            if (bestValue == null || (bestValue < value && Math.Abs(bestValue.Value - value) >= .0001))
            {
                bestValue = value;
                bestCenter2 = null;
                ties = new List<T> { tuple };
            }
            else if (bestValue != null && Math.Abs(bestValue.Value - value) < .0001 && ties.Any())
            {
                ties.Add(tuple);
            }
            // if several in a row tie for th lead, we shoot at the middle
            else if (bestValue > value && ties.Any())
            {
                bestCenter2 = ties[(int)Math.Floor((ties.Count - 1) / 2.0)];
                ties = new List<T>();
            }
        }

        public T? Winner() {
            if (bestCenter2 == null && ties.Any())
            {
                bestCenter2 = ties[(int)Math.Floor((ties.Count - 1) / 2.0)];
            }
            if (bestCenter2 != null)
            {
                return bestCenter2;
            }
            return null;
        }
    }
}