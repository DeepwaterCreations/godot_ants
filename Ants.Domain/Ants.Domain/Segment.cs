﻿using Microsoft.VisualBasic;
using Prototypist.Toolbox;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Intrinsics;
using System.Runtime.Intrinsics.X86;
using static System.Net.Mime.MediaTypeNames;
public class Segment2
{

    public readonly double distanceOrMaxValueForCover;
    // we assume angle 1 < angle 2
    // if angle 
    public readonly double angle1;
    public readonly double angle2;
    // used to track what the segments represents
    public readonly WhoToShoot.Thing thing;

    public Vector128<double> position1;
    public Vector128<double> position2;

    public HashSet<double> covers;

    public Segment2(double distanceOrMaxValueForCover, double angle1, double angle2, WhoToShoot.Thing thing, HashSet<double> covers, Vector128<double> position1, Vector128<double> position2)
    {
        if (thing == null) 
        {
            var db = 0;
        }

        if (angle2 > Math.Tau) {
            throw new Exception("angle2 > Math.Tau");
        }
        if (angle1 > Math.Tau)
        {
            throw new Exception("angle1 > Math.Tau");
        }
        if (angle2 < 0)
        {
            throw new Exception("angle2 < 0");
        }
        if (angle1 < 0)
        {
            throw new Exception("angle1 < 0");
        }
        if (angle1 > angle2) 
        {
            throw new Exception("angle1 > angle2");
        }

        this.distanceOrMaxValueForCover = distanceOrMaxValueForCover;
        this.angle1 = angle1;
        this.angle2 = angle2;
        this.thing = thing ?? throw new ArgumentNullException(nameof(thing));
        this.covers = covers;
        this.position1 = position1;
        this.position2 = position2;
    }

    public static double FixAngle(double angle) {
        while (angle > Math.Tau)
        {
            angle -= Math.Tau;
        }
        while (angle < 0)
        {
            angle += Math.Tau;
        }
        return angle;
    }

    public static IEnumerable<Segment2> Make(double distance, double angle1, double angle2,WhoToShoot.Thing thing, Vector128<double> position1, Vector128<double> position2)
    {
        if (thing == null) {
            throw new Exception("why is thing null??");
        }

        if (double.IsNaN(angle1))
        {
            throw new Exception($"angle {angle1}");
        }
        if (double.IsNaN(angle2))
        {
            throw new Exception($"angle {angle2}");
        }
        angle1 = FixAngle(angle1);
        angle2 = FixAngle(angle2);

        var (distanceOrMaxValueForCover, covers) = thing.Is3(out var _) ? (double.MaxValue, new HashSet<double> { distance}) : (distance, new HashSet<double>());


        if (angle1 == angle2)
        {

        }
        else if (angle1 < angle2)
        {
            yield return new Segment2(distanceOrMaxValueForCover, angle1, angle2, thing, covers, position1, position2);
        }
        else
        {
            var totalAngle = (Math.Tau - angle1) + angle2;
            var part2 = ((Math.Tau - angle1) / totalAngle);
            var center = Avx.Add(Avx.Multiply(position1, Vector128.Create(1 - part2, 1 - part2)), Avx.Multiply(position2, Vector128.Create(part2, part2)));
            yield return new Segment2(distanceOrMaxValueForCover, angle1, Math.Tau, thing, covers, position1, center);
            yield return new Segment2(distanceOrMaxValueForCover, 0, angle2, thing, covers, center, position2);
        }
    }

    public static Vector128<double> Center(Segment2 segment, double centerAngle)
    {
        return Center(segment.angle1, segment.angle2, centerAngle, segment.position1, segment.position2);
    }
    public static Vector128<double> Center(double angle1, double angle2, double centerAngle, Vector128<double> position1, Vector128<double> position2) {

        Debug.Assert(angle1 <= angle2);
        Debug.Assert(centerAngle <= angle2);
        Debug.Assert(angle1 <= centerAngle);
        var part2 = (centerAngle - angle1) / (angle2 - angle1);
        return Avx.Add(Avx.Multiply(position1, Vector128.Create(1-part2, 1-part2)), Avx.Multiply(position2, Vector128.Create(part2, part2)));
    } 

    public static bool Between(double angle1, double angle2, double angle3)
    {
        var adjustBy = Math.Tau - angle1;
        angle2 = FixAngle(angle2 + adjustBy);
        angle3 = FixAngle(angle3 + adjustBy);
        return angle2 <= angle3;
    }

    public static Segment2 MakeOrThrow(double angle1, double angle2, double distance, WhoToShoot.Thing thing, Vector128<double> position1, Vector128<double> position2)
    {
        if (angle2 > Math.Tau)
        {
            throw new Exception($"the agnle2 is too big {angle2}");
        }
        if (angle1 > Math.Tau)
        {
            throw new Exception($"the agnle1 is too big {angle1}");
        }
        if (angle2 < 0)
        {
            throw new Exception($"the agnle2 is too small {angle2}");
        }
        if (angle1 < 0)
        {
            throw new Exception($"the agnle1 is too small {angle1}");
        }
        if (angle1 == angle2)
        {
            throw new Exception($"the agnles are the same {angle2}");
        }
        if (angle1 > angle2)
        {
            throw new Exception($"the agnles1 ({angle1}) is bigger tyhan angle2 ({angle2})");
        }
        return new Segment2(distance, angle1, angle2, thing, null, position1, position1);
    }

    public bool Contains(double angle)
    {
        return angle1 <= angle && angle < angle2;
    }

    public static bool FirstWins(Segment2 first, Segment2 second)
    {
        if (first.thing == null)
        {
            return false;
        }

        if (second.thing == null)
        {
            return true;
        }

        var firstWins = first.thing.SwitchReturns(
            first_target => second.thing.SwitchReturns(
                second_target => first.distanceOrMaxValueForCover < second.distanceOrMaxValueForCover,
                second_rock => first.distanceOrMaxValueForCover < second.distanceOrMaxValueForCover,
                second_cover => true),
            first_rock => second.thing.SwitchReturns(
                second_target => first.distanceOrMaxValueForCover < second.distanceOrMaxValueForCover,
                second_rock => first.distanceOrMaxValueForCover < second.distanceOrMaxValueForCover,
                second_cover => true),
            first_cover => second.thing.SwitchReturns(
                second_target => false,
                second_rock => false,
                second_cover => first.distanceOrMaxValueForCover > second.distanceOrMaxValueForCover));

        return firstWins;
        //if (firstWins) {
        //    if (first.thing.Is3(out var first_cover) &&) { 
            
        //    }
        //}
    }

    //public static Segment2 CloserSolidFutherCover(Segment2 a, Segment2 b)
    //{
    //    if (a.thing == null) {
    //        return b;
    //    }

    //    if (b.thing == null)
    //    {
    //        return a;
    //    }

    //    return a.thing.SwitchReturns(
    //        a_target => b.thing.SwitchReturns(
    //            b_target => a.distanceOrMaxValueForCover < b.distanceOrMaxValueForCover ? a : b,
    //            b_rock => a.distanceOrMaxValueForCover < b.distanceOrMaxValueForCover ? a : b,
    //            b_cover => a),
    //        a_rock => b.thing.SwitchReturns(
    //            b_target => a.distanceOrMaxValueForCover < b.distanceOrMaxValueForCover ? a : b,
    //            b_rock => a.distanceOrMaxValueForCover < b.distanceOrMaxValueForCover ? a : b,
    //            b_cover => new Segment2(a.distanceOrMaxValueForCover, a.angle1, a.angle2, a.thing , a.distanceOrMaxValueForCover, a.position1, a.position2)),
    //        a_cover => b.thing.SwitchReturns(
    //            b_target => new Segment2(b.distanceOrMaxValueForCover, b.angle1, b.angle2, b.thing, b.distanceOrMaxValueForCover, b.position1, b.position2),
    //            b_rock => new Segment2(b.distanceOrMaxValueForCover, b.angle1, b.angle2, b.thing, b.distanceOrMaxValueForCover, b.position1, b.position2),
    //            b_cover => a.distanceOrMaxValueForCover > b.distanceOrMaxValueForCover ? a : b));
    //}

    //public bool Combine(Segment2 other, [MaybeNullWhen(false)] out List<Segment2> res)
    //{
    //    var (leading, following) = angle1 < other.angle1 ? (this, other) : (other, this);

    //    // no overlap, we are done
    //    if (!leading.Contains(following.angle1))
    //    {
    //        res = null;
    //        return false;
    //    }

    //    var result = new List<Segment2>(); // allocation on a hot path

    //    var combined = CloserSolidFutherCover(leading, following);

    //    if (combined.thing == leading.thing)
    //    {
    //        result.Add(combined);
    //        if (!leading.Contains(following.angle2))
    //        {
    //            AddIfNotEmpty(following.distanceOrMaxValueForCover, leading.angle2, following.angle2, following.thing, following.coverAtRange, Center(following, leading.angle2), following.position2);
    //        }
    //        res = result;
    //        return true;
    //    }
    //    else
    //    {
    //        AddIfNotEmpty(leading.distanceOrMaxValueForCover, leading.angle1, following.angle1, leading.thing, leading.coverAtRange, Center(leading, following.angle1), following.position1);
    //        AddIfNotEmpty(following.distanceOrMaxValueForCover, following.angle1, following.angle2, following.thing, combined.coverAtRange, following.position1, following.position2);
    //        if (leading.Contains(following.angle2))
    //        {
    //            AddIfNotEmpty(leading.distanceOrMaxValueForCover, following.angle2, leading.angle2, leading.thing, leading.coverAtRange, Center(leading, following.angle2), leading.position2);
    //        }
    //        res = result;
    //        return true;
    //    }

    //    void AddIfNotEmpty(double distance, double angle1, double angle2, WhoToShoot.Thing? thing, double? coverAtRange, Vector128<double> position1, Vector128<double> position2)
    //    {
    //        if (angle1 != angle2)
    //        {
    //            result.Add(new Segment2(distance, angle1, angle2, thing, coverAtRange, position1, position2));
    //        }
    //    }
    //}


    //public static List<Segment2> AddToSegments(List<Segment2> segments, List<Segment2> newSegments)
    //{
    //    while (newSegments.Any())
    //    {
    //        var newSegment = newSegments.First();
    //        newSegments.RemoveAt(0);

    //        var nextSegments = new List<Segment2>();
    //        var confliced = false;

    //        // TODO I think I need a specail data structure
    //        // where I can look up what overlaps
    //        foreach (var segment in segments)
    //        {
    //            if (!confliced && segment.Combine(newSegment, out var toAdds))
    //            {
    //                foreach (var toAdd in toAdds)
    //                {
    //                    newSegments.Add(toAdd);
    //                }
    //                confliced = true;
    //            }
    //            else
    //            {
    //                nextSegments.Add(segment);
    //            }
    //        }
    //        if (!confliced)
    //        {
    //            nextSegments.Add(newSegment);
    //        }
    //        segments = nextSegments;
    //    }

    //    return segments;
    //}
}

public class Segment
{

    public readonly double distance;
    // we assume angle 1 < angle 2
    // if angle 
    public readonly double angle1;
    public readonly double angle2;

    private Segment(double distance, double angle1, double angle2)
    {
        this.distance = distance;
        this.angle1 = angle1;
        this.angle2 = angle2;
    }

    public static IEnumerable<Segment> Make(double angle1, double angle2, double distance)
    {
        if (double.IsNaN(angle1))
        {
            throw new Exception($"angle {angle1}");
        }
        if (double.IsNaN(angle2))
        {
            throw new Exception($"angle {angle2}");
        }

        while (angle1 > Math.Tau)
        {
            angle1 -= Math.Tau;
        }
        while (angle2 > Math.Tau)
        {
            angle2 -= Math.Tau;
        }
        while (angle1 < 0)
        {
            angle1 += Math.Tau;
        }
        while (angle2 < 0)
        {
            angle2 += Math.Tau;
        }
        if (angle1 == angle2)
        {

        }
        else if (angle1 < angle2)
        {
            yield return new Segment(distance, angle1, angle2);
        }
        else
        {
            yield return new Segment(distance, angle1, Math.Tau);
            yield return new Segment(distance, 0, angle2);
        }
    }

    public static Segment MakeOrThrow(double angle1, double angle2, double distance)
    {
        // these could be debug.Assert
        if (angle2 > Math.Tau)
        {
            throw new Exception($"the agnle2 is too big {angle2}");
        }
        if (angle1 > Math.Tau)
        {
            throw new Exception($"the agnle1 is too big {angle1}");
        }
        if (angle2 < 0)
        {
            throw new Exception($"the agnle2 is too small {angle2}");
        }
        if (angle1 < 0)
        {
            throw new Exception($"the agnle1 is too small {angle1}");
        }
        if (angle1 == angle2)
        {
            throw new Exception($"the agnles are the same {angle2}");
        }
        if (angle1 > angle2)
        {
            throw new Exception($"the agnles1 ({angle1}) is bigger tyhan angle2 ({angle2})");
        }
        return new Segment(distance, angle1, angle2);
    }

    public bool Contains(double angle)
    {
        return angle1 <= angle && angle < angle2;
    }

    public bool Combine(Segment other, out List<Segment> res)
    {
        var (leading, following) = angle1 < other.angle1 ? (this, other) : (other, this);

        // no overlap, we are done
        if (!leading.Contains(following.angle1))
        {
            res = null;
            return false;
        }

        var result = new List<Segment>(); // allocation on a hot path

        if (following.distance == leading.distance)
        {
            AddIfNotEmpty(following.distance, Math.Min(leading.angle1, following.angle1), Math.Max(leading.angle2, following.angle2));
            res = result;
            return true;
        }
        else if (following.distance > leading.distance)
        {
            result.Add(leading);
            if (!leading.Contains(following.angle2))
            {
                AddIfNotEmpty(following.distance, leading.angle2, following.angle2);
            }
            res = result;
            return true;
        }
        else
        {
            AddIfNotEmpty(leading.distance, leading.angle1, following.angle1);
            AddIfNotEmpty(following.distance, following.angle1, following.angle2);
            if (leading.Contains(following.angle2))
            {
                AddIfNotEmpty(leading.distance, following.angle2, leading.angle2);
            }
            res = result;
            return true;
        }

        void AddIfNotEmpty(double distance, double angle1, double angle2)
        {
            if (angle1 != angle2)
            {
                result.Add(new Segment(distance, angle1, angle2));
            }
        }
    }


    //public static List<Segment> AddToSegments(List<Segment> segments, List<Segment> newSegments)
    //{
    //    while (newSegments.Any())
    //    {
    //        var newSegment = newSegments.First();
    //        newSegments.RemoveAt(0);

    //        var nextSegments = new List<Segment>();
    //        var confliced = false;
    //        foreach (var segment in segments)
    //        {
    //            if (!confliced && segment.Combine(newSegment, out var toAdds))
    //            {
    //                foreach (var toAdd in toAdds)
    //                {
    //                    newSegments.Add(toAdd);
    //                }
    //                confliced = true;
    //            }
    //            else
    //            {
    //                nextSegments.Add(segment);
    //            }
    //        }
    //        if (!confliced)
    //        {
    //            nextSegments.Add(newSegment);
    //        }
    //        segments = nextSegments;
    //    }

    //    return segments;
    //}
}

public class Segments {



    private const int degrees = 360;
    private readonly Segment[] backing = new Segment[degrees];


    public void AddRange(IEnumerable<Segment> segments) {
        foreach (var segment in segments)
        {
            Add(segment);
        }
    }

    public void Add(Segment toAdd) {

        var endAt = Math.Min((int)Math.Ceiling(toAdd.angle2 * degrees / Math.Tau), backing.Length-1);
        for (int i = (int)Math.Floor(toAdd.angle1 * degrees / Math.Tau); i <= endAt; i++)
        {
            var current = backing[i];
            if (current == null || current.distance > toAdd.distance)
            {
                backing[i] = toAdd;
            }
            else
            {
                i = (int)Math.Ceiling(current.angle2 * degrees / Math.Tau);
            }
        }
    }

    public IEnumerable<Segment> AsEnumerable() {
        (double angle1, double angle2, double distance) ? last = null;

        for (int i = 0; i < degrees; i++)
        {
            var next = backing[i];

            if (last == null)
            {
                if (next != null)
                {
                    last = (next.angle1, next.angle2, next.distance);
                }
            }
            else {
                if (next == null)
                {
                    yield return Segment.MakeOrThrow(last.Value.angle1, last.Value.angle2, last.Value.distance);
                    last = null;
                }
                else {
                    if (last.Value.angle2 < next.angle1)
                    {
                        // co exist
                        yield return Segment.MakeOrThrow(last.Value.angle1, last.Value.angle2, last.Value.distance);
                        last = (next.angle1, next.angle2, next.distance);
                    }
                    else {
                        if (last.Value.distance == next.distance)
                        {
                            // merge
                            last = (last.Value.angle1, next.angle2, next.distance);
                        }
                        else if (last.Value.distance < next.distance)
                        {
                            // last takes a bite out of next
                            yield return Segment.MakeOrThrow(last.Value.angle1, last.Value.angle2, last.Value.distance);
                            last = (last.Value.angle2, next.angle2, next.distance);
                        }
                        else //last.Value.distance > next.distance 
                        {
                            // next takes a bit out of last
                            yield return Segment.MakeOrThrow(last.Value.angle1, next.angle1, last.Value.distance);
                            last = (next.angle1, next.angle2, next.distance);
                        }
                    }
                }
            }
        }

        if (last != null) {
            yield return Segment.MakeOrThrow(last.Value.angle1, last.Value.angle2, last.Value.distance);
        }
    }
}

public class Segments2
{
    private const int degrees = 360;
    private readonly Segment2[] backing = new Segment2[degrees];


    public void AddRange(IEnumerable<Segment2> segments)
    {
        foreach (var segment in segments)
        {
            Add(segment);
        }
    }

    public void Add(Segment2 toAdd)
    {
        var endAt = Math.Min( (int)Math.Ceiling(toAdd.angle2 * degrees / Math.Tau) , backing.Length-1);
        for (int i = (int)Math.Floor(toAdd.angle1 * degrees / Math.Tau); i <= endAt; i++)
        {
            var current = backing[i];
            if (current == null || Segment2.FirstWins(toAdd, current))
            {
                backing[i] = toAdd;
            }
            else
            {
                i = (int)Math.Ceiling(current.angle2 * degrees / Math.Tau);
            }
        }
    }

    public IEnumerable<Segment2> AsEnumerable()
    {
        Segment2? last = null;

        for (int i = 0; i < degrees; i++)
        {
            var next = backing[i];

            if (last == null)
            {
                if (next != null)
                {
                    last = next;
                }
            }
            else
            {
                if (next == null)
                {
                    yield return last;
                    last = null;
                }
                else
                {
                    if (last.angle2 < next.angle1)
                    {
                        // co exist
                        yield return last;
                        last = next;
                    }
                    else
                    {
                        if (Segment2.FirstWins(last, next))
                        {
                            // last takes a bite out of next
                            yield return last;
                            last = new Segment2(next.distanceOrMaxValueForCover, last.angle2, next.angle2, next.thing, next.covers, Segment2.Center(next, last.angle2), next.position2);
                        }
                        else //last.Value.distance > next.distance 
                        {
                            // next takes a bit out of last
                            yield return new Segment2(last.distanceOrMaxValueForCover, last.angle1, next.angle1,  last.thing, last.covers, last.position1, Segment2.Center(last, next.angle1));
                            last = next;
                        }
                    }
                }
            }
        }

        if (last != null)
        {
            yield return last;
        }
    }
}

// this one is used for LOS
public class SegmentsRewrite {

    public readonly List<Segment>[] backing;
    private const int degrees = 36;

    public SegmentsRewrite() {
        backing = new int[degrees].Select(x => new List<Segment>()).ToArray();
    }
 
    public void AddRange(IEnumerable<Segment> segments)
    {
        foreach (var segment in segments)
        {
            Add(segment);
        }
    }

    public void Add(Segment toAdd)
    {

        var endAt = Math.Min((int)Math.Floor(toAdd.angle2 * degrees / Math.Tau), backing.Length - 1);
        for (int i = (int)Math.Floor(toAdd.angle1 * degrees / Math.Tau); i <= endAt; i++)
        {
            backing[i] = MergeIn(backing[i], Segment.MakeOrThrow(
                Math.Max(i * Math.Tau/ degrees, toAdd.angle1),
                Math.Min((i +1) * Math.Tau / degrees, toAdd.angle2), 
                toAdd.distance));
        }
    }

    public IEnumerable<Segment> AsEnumerable()
    {
        for (int i = 0; i < backing.Length; i++)
        {
            foreach (var item in backing[i])
            {
                yield return item;
            }
        }
    }

    private List<Segment> MergeIn(List<Segment> segments, Segment toAdd)
    {

        var enumerator = ((IEnumerable<Segment>)segments).GetEnumerator();
        var res = new List<Segment>();
        while (enumerator.MoveNext())
        {
            var existingSegment = enumerator.Current;

            // we don't overlap toAdd yet
            if (toAdd.angle1 > existingSegment.angle2)
            {
                res.Add(existingSegment);
                continue;
            }

            // existing starts after toAdd ends
            if (existingSegment.angle1 > toAdd.angle2)
            {
                res.Add(toAdd);
                res.Add(existingSegment);
                return RestOfExisting(enumerator, res);
            }

            if (toAdd.distance == existingSegment.distance)
            {
                // merge 
                if (existingSegment.angle2 >= toAdd.angle2)
                {
                    res.Add(Segment.MakeOrThrow(Math.Min(existingSegment.angle1, toAdd.angle1), existingSegment.angle2, toAdd.distance));
                    return RestOfExisting(enumerator, res);
                }
                else
                {
                    // we cover the existing
                }
            }
            else if (toAdd.distance < existingSegment.distance)
            {
                // existing starts before we do
                if (existingSegment.angle1 < toAdd.angle1) {
                    res.Add(Segment.MakeOrThrow(existingSegment.angle1, toAdd.angle1, existingSegment.distance));
                }

                // existing end after toAdd ends
                if (existingSegment.angle2 > toAdd.angle2)
                {
                    res.Add(toAdd);
                    res.Add(Segment.MakeOrThrow(toAdd.angle2, existingSegment.angle2, existingSegment.distance));
                    return RestOfExisting(enumerator, res);
                }
                else
                {
                    // we cover the existing
                }
            }
            else
            {
                // if there is any thing of toAdd that is before existingSegment, add that
                if (toAdd.angle1 < existingSegment.angle1)
                {
                    res.Add(Segment.MakeOrThrow(toAdd.angle1, existingSegment.angle1, toAdd.distance));
                }

                // the existing wins
                res.Add(existingSegment);

                // if there is nothing of toAdd after existing we are done
                if (existingSegment.angle2 >= toAdd.angle2)
                {
                    return RestOfExisting(enumerator, res);
                }

                // overwise contine with what is left
                toAdd = Segment.MakeOrThrow(existingSegment.angle2, toAdd.angle2, toAdd.distance);
            }
        }

        res.Add(toAdd);

        return res;
    }

    private static List<Segment> RestOfExisting(IEnumerator<Segment> enumerator, List<Segment> res)
    {
        while (enumerator.MoveNext())
        {
            res.Add(enumerator.Current);
        }
        return res;
    }
}


public class SegmentsRewrite2
{

    public readonly List<Segment2>[] backing;
    private const int degrees = 36;

    public SegmentsRewrite2()
    {
        backing = new int[degrees].Select(x => new List<Segment2>()).ToArray();
    }

    public void AddRange(IEnumerable<Segment2> segments)
    {
        foreach (var segment in segments)
        {
            Add(segment);
        }
    }

    public void Add(Segment2 toAdd)
    {

        var endAt = Math.Min((int)Math.Floor(toAdd.angle2 * degrees / Math.Tau), backing.Length - 1);
        for (int i = (int)Math.Floor(toAdd.angle1 * degrees / Math.Tau); i <= endAt; i++)
        {
            var anlge1 = Math.Max(i * Math.Tau / degrees, toAdd.angle1);
            var anlge2 = Math.Min((i + 1) * Math.Tau / degrees, toAdd.angle2);

            backing[i] = MergeIn(backing[i], new Segment2(
                toAdd.distanceOrMaxValueForCover,
                anlge1,
                anlge2,
                toAdd.thing,
                toAdd.covers,
                Segment2.Center(toAdd, anlge1),
                Segment2.Center(toAdd, anlge2)));
        }
    }

    public IEnumerable<Segment2> AsEnumerable()
    {
        for (int i = 0; i < backing.Length; i++)
        {
            foreach (var item in backing[i])
            {
                yield return item;
            }
        }
    }

    private List<Segment2> MergeIn(List<Segment2> segments, Segment2 toAdd)
    {

        var enumerator = ((IEnumerable<Segment2>)segments).GetEnumerator();
        var res = new List<Segment2>();
        while (enumerator.MoveNext())
        {
            var existingSegment = enumerator.Current;

            // we don't overlap toAdd yet
            if (toAdd.angle1 > existingSegment.angle2)
            {
                res.Add(existingSegment);
                continue;
            }

            // existing starts after toAdd ends
            if (existingSegment.angle1 > toAdd.angle2)
            {
                res.Add(toAdd);
                res.Add(existingSegment);
                return RestOfExisting(enumerator, res);
            }

            // find who starts
            var (leading, following) = toAdd.angle1 < existingSegment.angle1 ? (toAdd, existingSegment) : (existingSegment, toAdd);

            // TODO 
            // cover has distance of double.MaxValue

            var closerDis = Math.Min(leading.distanceOrMaxValueForCover, following.distanceOrMaxValueForCover);
            var combinedCovers = leading.covers.Union(following.covers).Where(x => x < closerDis).ToHashSet();

            // are we going to combine where the segments overlap?
            if (combinedCovers.Except(leading.covers).Any() && combinedCovers.Except(following.covers).Any())
            {
                //     ---following----?????
                // -------leading------
                // or
                // -------leading------
                //     ---following----?????
                if (leading.angle1 < following.angle1)
                {
                    res.Add(new Segment2(leading.distanceOrMaxValueForCover, leading.angle1, following.angle1, leading.thing, leading.covers, leading.position1, Segment2.Center(leading, following.angle1)));
                }

                var closer = leading.distanceOrMaxValueForCover < following.distanceOrMaxValueForCover ? leading : following;

                //      ------following------
                // ?????-----leading----- 
                // or
                // ?????-----leading----- 
                //      ------following------
                if (leading.angle2 < following.angle2)
                {
                    res.Add(new Segment2(closer.distanceOrMaxValueForCover, following.angle1, leading.angle2, closer.thing, combinedCovers, Segment2.Center(closer, following.angle1), Segment2.Center(closer, leading.angle2)));
                    if (following == existingSegment)
                    {
                        res.Add(new Segment2(following.distanceOrMaxValueForCover, leading.angle2, following.angle2, following.thing, following.covers, Segment2.Center(following, leading.angle2), following.position2));
                        return RestOfExisting(enumerator, res);
                    }
                    else
                    {
                        toAdd = new Segment2(following.distanceOrMaxValueForCover, leading.angle2, following.angle2, following.thing, following.covers, Segment2.Center(following, leading.angle2), following.position2);
                    }
                }
                //      --following--
                // ?????-------leading------- 
                // or 
                // ?????-------leading------- 
                //      --following--
                else
                {
                    res.Add(new Segment2(closer.distanceOrMaxValueForCover, following.angle1, following.angle2, closer.thing, combinedCovers, Segment2.Center(closer, following.angle1), Segment2.Center(closer, following.angle2)));
                    if (following == existingSegment)
                    {
                        toAdd = new Segment2(leading.distanceOrMaxValueForCover, following.angle2, leading.angle2, leading.thing, leading.covers, Segment2.Center(leading, following.angle2), leading.position2);
                    }
                    else
                    {
                        res.Add(new Segment2(leading.distanceOrMaxValueForCover, following.angle2, leading.angle2, leading.thing, leading.covers, Segment2.Center(leading, following.angle2), leading.position2));
                        return RestOfExisting(enumerator, res);
                    }
                }
            }
            // is the starting segment the winning segment?
            else if (leading.distanceOrMaxValueForCover <= following.distanceOrMaxValueForCover) // leading.distance == following.distance is sort of an odd case, it shouldn't really happen
            {
                // does the leading end after the following segment
                //      --following--
                // ?????-------leading------- 
                if (leading.angle2 >= following.angle2)
                {
                    if (leading == existingSegment)
                    {
                        // the existing segment covers the added segment and gains nothign from it
                        res.Add(existingSegment);
                        return RestOfExisting(enumerator, res);
                    }
                    else
                    {
                        // toAdd covers the existing, we keep going to see if it covers others
                    }
                }
                //      ----following-----
                // ?????--leading-- 
                else
                {
                    if (leading == toAdd)
                    {
                        res.Add(toAdd);
                        res.Add(new Segment2(existingSegment.distanceOrMaxValueForCover, toAdd.angle2, existingSegment.angle2, existingSegment.thing, existingSegment.covers, Segment2.Center(existingSegment, toAdd.angle2), existingSegment.position2));
                        return RestOfExisting(enumerator, res);
                    }
                    else
                    {
                        res.Add(existingSegment);
                        toAdd = new Segment2(toAdd.distanceOrMaxValueForCover, existingSegment.angle2, toAdd.angle2, toAdd.thing, toAdd.covers, Segment2.Center(toAdd, existingSegment.angle2), toAdd.position2);
                    }
                }
            }
            // the following segment is the winner
            else 
            {
                if (leading.angle1 < following.angle1)
                {
                    res.Add(new Segment2(leading.distanceOrMaxValueForCover, leading.angle1, following.angle1, leading.thing, leading.covers, leading.position1, Segment2.Center(leading, following.angle1)));
                }

                // ?????-------leading------- 
                //      --following--
                if (leading.angle2 >= following.angle2)
                {
                    res.Add(following);

                    if (leading == toAdd)
                    {
                        toAdd = new Segment2(leading.distanceOrMaxValueForCover, following.angle2, leading.angle2, leading.thing, leading.covers, Segment2.Center(leading, following.angle2), leading.position2);
                    }
                    else
                    {
                        res.Add(new Segment2(leading.distanceOrMaxValueForCover, following.angle2, leading.angle2, leading.thing, leading.covers, Segment2.Center(leading, following.angle2), leading.position2));
                        return RestOfExisting(enumerator, res);
                    }
                }
                // ?????--leading--
                //      ---following---
                else {
                    if (following == toAdd)
                    {
                        // carry on
                    }
                    else {
                        res.Add(following);
                        return RestOfExisting(enumerator, res);
                    }
                }
            }


            //if (Segment2.FirstWins(toAdd, existingSegment))
            //{
            //    // existing starts before we do
            //    if (existingSegment.angle1 < toAdd.angle1)
            //    {
            //        res.Add(new Segment2(existingSegment.distanceOrMaxValueForCover, existingSegment.angle1, toAdd.angle1,  existingSegment.thing, existingSegment.covers, existingSegment.position1 ,Segment2.Center(existingSegment,toAdd.angle1)));
            //    }

            //    // existing end after toAdd ends
            //    if (existingSegment.angle2 > toAdd.angle2)
            //    {
            //        res.Add(toAdd);
            //        res.Add(new Segment2(existingSegment.distanceOrMaxValueForCover, toAdd.angle2, existingSegment.angle2,  existingSegment.thing, existingSegment.coverAtRange, Segment2.Center(existingSegment, toAdd.angle2), existingSegment.position2));
            //        return RestOfExisting(enumerator, res);
            //    }
            //    else
            //    {
            //        // we cover the existing
            //    }
            //}
            //else
            //{
            //    // if there is any thing of toAdd that is before existingSegment, add that
            //    if (toAdd.angle1 < existingSegment.angle1)
            //    {
            //        res.Add(new Segment2(toAdd.distanceOrMaxValueForCover, toAdd.angle1, existingSegment.angle1,  toAdd.thing, toAdd.covers, toAdd.position1, Segment2.Center(toAdd, existingSegment.angle1)));
            //    }

            //    // the existing wins
            //    res.Add(existingSegment);

            //    // if there is nothing of toAdd after existing we are done
            //    if (existingSegment.angle2 >= toAdd.angle2)
            //    {
            //        return RestOfExisting(enumerator, res);
            //    }

            //    // overwise contine with what is left
            //    toAdd = new Segment2(toAdd.distanceOrMaxValueForCover, existingSegment.angle2, toAdd.angle2,  toAdd.thing, toAdd.covers, Segment2.Center(toAdd, existingSegment.angle2), toAdd.position2);
            //}
        }

        res.Add(toAdd);

        return res;
    }

    private static List<Segment2> RestOfExisting(IEnumerator<Segment2> enumerator, List<Segment2> res)
    {
        while (enumerator.MoveNext())
        {
            res.Add(enumerator.Current);
        }
        return res;
    }
}