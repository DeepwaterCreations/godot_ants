﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Ants.V2
{
    public class TypedSerializer
    {

        public static string Serialize(object o) => Serialize(o, new Dictionary<object, int>(), 0);

            public static string Serialize(object o, Dictionary<object, int> alreadySeen,  int tabs) {

            if (o == null)
            {
                return "null" + System.Environment.NewLine;
            }

            if (o is int || o is float || o is double || o is decimal || o is short || o is long || o is uint || o is ushort || o is ulong || o is bool || o is string || o is Guid) {
                return o.ToString() + System.Environment.NewLine;
            }

            if (alreadySeen.TryGetValue(o, out var id)) { 
                return "..." + id + System.Environment.NewLine;
            }
            id = o.GetHashCode();
            alreadySeen.Add(o, id);

            var type = o.GetType();
            var res = type.Name + " " + id;
            res += System.Environment.NewLine;
            res += Tabs(tabs) + "{";
            res += System.Environment.NewLine;
            foreach (var item in type.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance))
            {
                //if (item.Name.StartsWith("_")) {
                //    continue;
                //}
                res += Tabs(tabs+1) + item.Name + " = " + Serialize(item.GetValue(o), alreadySeen, tabs +1);
            }

            if (o is System.Collections.IEnumerable enumerable) {
                var i = 0;
                foreach (var thing in enumerable)
                {
                    res += Tabs(tabs+1) + i.ToString() + " = " + Serialize(thing, alreadySeen, tabs + 1);
                    i++;
                }   
            }

            res += Tabs(tabs) + "}" + System.Environment.NewLine;

            return res;

        }

        private static string Tabs(int tabs)
        {
            var res = "";
            for(int i = 0; i < tabs; i++)
            {
                res += '\t';
            }
            return res;
        }
    }
}
