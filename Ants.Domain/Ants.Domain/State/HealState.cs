﻿using Ants.Domain.Infrastructure;
using Ants.Domain.State;
using System.Diagnostics;
using System.Runtime.Intrinsics.X86;

public class HealState : IStateHash
{
    public double energy;

    public readonly Immutable immutable;

    public class Immutable : IStateHash
    {
        public readonly double radius;
        public readonly double healUnder;
        public readonly double maxRate;
        public readonly double maxEnergy;
        public readonly double energyRestoreRate;

        public Immutable(double radius, double healUnder, double maxRate, double maxEnergy, double energyRestoreRate)
        {
            this.radius = radius;
            this.healUnder = healUnder;
            this.maxRate = maxRate;
            this.maxEnergy = maxEnergy;
            this.energyRestoreRate = energyRestoreRate;
        }

        public IEnumerable<(string, object?)> HashData()
        {
            yield return (nameof(radius), radius);
            yield return (nameof(healUnder), healUnder);
            yield return (nameof(maxRate), maxRate);
            yield return (nameof(maxEnergy), maxEnergy);
            yield return (nameof(energyRestoreRate), energyRestoreRate);
        }

        //public override int GetHashCode()
        //{
        //    return this.Hash();
        //}

        //public override bool Equals(object? obj)
        //{
        //    return obj is Immutable immutable &&
        //           radius == immutable.radius &&
        //           healUnder == immutable.healUnder &&
        //           maxRate == immutable.maxRate &&
        //           maxEnergy == immutable.maxEnergy &&
        //           energyRestoreRate == immutable.energyRestoreRate;
        //}
    }

    public HealState(double energy, Immutable immutable)
    {
        this.energy = energy;
        this.immutable = immutable;
    }

    public void Update(SimulationState simulationState, AntState self) 
    {
        energy = Math.Max(energy + immutable.energyRestoreRate, immutable.maxEnergy);

        foreach (var item in simulationState.ants.Values
            .Where(x=> 
                self.immutable.side == x.immutable.side &&
                x.hp < immutable.healUnder &&
                Avx.Subtract(x.position, self.position).LengthSquared() < immutable.radius * immutable.radius)
            .OrderBy(x => x.hp))
        {
            var toHeal = Math.Min(simulationState.money.GetMainMoney(self.immutable.side), Math.Min(immutable.maxRate, Math.Min(energy, immutable.healUnder - item.hp)));
            energy -= toHeal;
            item.hp += toHeal;
            bool healed = simulationState.money.TrySpendMainMoney(toHeal, self.immutable.side);
            Debug.Assert(healed);
        }
    }

    public IEnumerable<(string, object?)> HashData()
    {
        yield return (nameof(energy), energy);
        yield return (nameof(immutable), immutable);
    }

    public override int GetHashCode()
    {
        return this.Hash();
    }

    public HealState Copy() {
        return new HealState(energy, this.immutable);
    }

    public override bool Equals(object? obj)
    {
        return obj is HealState state &&
               energy == state.energy &&
               EqualityComparer<Immutable>.Default.Equals(immutable, state.immutable);
    }
}