﻿using Ants.Domain;
using Ants.Domain.State.Orders._12___Arty;
using System.Runtime.Intrinsics;

public class ArtyState: IStateHash
{

    //public const int costToCast = FramesPerSecond.framesPerSecond * 30;
    //public const int maxEnergy = costToCast * 2;

    public const int reloadTime = FramesPerSecond.framesPerSecond * 10;


    public readonly Immutable immutable;

    public int reloadDue;
    public Vector128<double>? target; 

    // old
    //public int energy;

    public class Immutable: IStateHash {
        internal readonly double damage;
        internal readonly double ignoreCoverFor;
        internal readonly double pierce;
        internal readonly int stun;
        internal readonly double subShot_runForDistance;
        internal readonly double subShot_speed;
        public readonly int runForTicks;
        public readonly double speed;
        public readonly double subShot_knockBack;

        public Immutable(double damage, double ignoreCoverFor, double pierce, int stun, double subShot_runForDistance, double subShot_speed, int runForTicks, double speed, double subShot_knockBack)
        {
            this.damage = damage;
            this.ignoreCoverFor = ignoreCoverFor;
            this.pierce = pierce;
            this.stun = stun;
            this.subShot_runForDistance = subShot_runForDistance;
            this.subShot_speed = subShot_speed;
            this.runForTicks = runForTicks;
            this.speed = speed;
            this.subShot_knockBack = subShot_knockBack;
        }

        public IEnumerable<(string, object?)> HashData()
        {
            yield return (nameof(damage), damage);
            yield return (nameof(ignoreCoverFor), ignoreCoverFor);
            yield return (nameof(pierce), pierce);
            yield return (nameof(stun), stun);
            yield return (nameof(subShot_runForDistance), subShot_runForDistance);
            yield return (nameof(subShot_speed), subShot_speed);
            yield return (nameof(runForTicks), runForTicks);
            yield return (nameof(speed), speed);
            yield return (nameof(subShot_knockBack), subShot_knockBack);
        }
        //public override int GetHashCode() => this.Hash();
    }

    public ArtyState(int reloadDue, Immutable immutable, Vector128<double>? target)
    {
        this.reloadDue = reloadDue;
        this.immutable = immutable;
        this.target = target;
    }

    public void Update(AntState ant)
    {
        //if (!ant.IsStuned()) {
        //    energy = Math.Min(energy + 1, maxEnergy);
        //}

        if (target != null && ant.legs != null && ant.legs.IsMoving()) {
            target = null;
        }

        if (!ant.IsStuned() && (ant.legs == null || !ant.legs.IsMoving()) && reloadDue > 0)
        {
            reloadDue = reloadDue - 1;
        }
    }

    public ArtyState Copy()
    {
        return new ArtyState(reloadDue, immutable, target);
    }

    //public override int GetHashCode() => this.Hash();
    
    public IEnumerable<(string, object?)> HashData()
    {

        yield return (nameof(reloadDue), reloadDue);
        yield return (nameof(immutable), immutable);
        yield return (nameof(target), target);
    }

    internal ArtyOrder Shoot(Vector128<double> mousePosition)
    {
        return new ArtyOrder(mousePosition);
    }
}