﻿using Ants.Domain.Infrastructure;
using Ants.Domain.State;
using System.Runtime.Intrinsics;

public class SheildWhileImobile : BaseShieldState
{
    public SheildWhileImobile(BaseImmutable immutable) : this(Vector128.Create(1.0,0.0), new Queue<(int tick, Vector128<double> from, double perce, double damagePreArmor)>(), immutable, true, AntState.warmUpFor, 3) { }

    public SheildWhileImobile(Vector128<double> facingUnit, Queue<(int tick, Vector128<double> from, double perce, double damagePreArmor)> lastHits, BaseImmutable immutable, bool isShielded, int exposedFor, double exposedAmount) : base(facingUnit, lastHits, immutable, isShielded, exposedFor, exposedAmount)
    {
    }

    //public override bool Equals(object? obj)
    //{
    //    return obj is SheildWhileImobile imobile &&
    //           facingUnit.Equals(imobile.facingUnit) &&
    //           lastHits.SequenceEqual(imobile.lastHits) &&
    //           isShielded == imobile.isShielded &&
    //           EqualityComparer<BaseImmutable>.Default.Equals(baseImmutable, imobile.baseImmutable);
    //}

    //public override int GetHashCode()
    //{
    //    return this.Hash();
    //    //return HashCode.Combine(facingUnit, lastHits.UncheckedSum(x => x.GetHashCode()), isShielded, baseImmutable);
    //}

    public override IEnumerable<(string, object?)> HashData()
    {
        yield return (nameof(facingUnit), facingUnit);
        yield return (nameof(lastHits), lastHits);
        yield return (nameof(isShielded), isShielded);
        yield return (nameof(baseImmutable), baseImmutable);
        yield return (nameof(exposedFor), exposedFor);
        yield return (nameof(exposedAmount), exposedAmount);
    }

    public override void Update(int tick, AntState owner)
    {
        base.Update(tick, owner);
        isShielded = owner.legs == null || !owner.legs.IsMoving();
    }

    internal override SheildWhileImobile Copy()
    {
        return new SheildWhileImobile(facingUnit, lastHits.ToQueue(), baseImmutable, isShielded, exposedFor, exposedAmount);
    }
}
