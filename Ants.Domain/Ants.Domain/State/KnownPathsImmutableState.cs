﻿using Ants.Domain.Infrastructure;
using Ants.Domain.State;
using Prototypist.Toolbox.IEnumerable;
using System.Runtime.Intrinsics;
using static Pathing;

public class KnownPathsImmutableState: IReadOnlyStateHash
{
    public Dictionary<(Mode,double), Paths> knownPaths;

    // we were actually spending a lot of time on the hash
    // currently this is immutable so we save the value
    public int? ReadOnlyHash { get; set;}

    public KnownPathsImmutableState(Paths[] knownPaths)
    {
        this.knownPaths = knownPaths?.ToDictionary(x=>(x.mode,x.avoidBy), x=>x) ??  throw new ArgumentNullException(nameof(knownPaths));

    }

    //public override bool Equals(object? obj)
    //{
    //    return obj is KnownPathsImmutableState state &&
    //           knownPaths.SetEqual(state.knownPaths);
    //}

    //public override int GetHashCode()
    //{
    //    return ReadOnlyHash.Value;
    //    //return knownPaths.UncheckedSum(x=>x.Key.GetHashCode() + x.Value.GetHashCode());
    //}

    public IEnumerable<(string, object?)> HashData()
    {
        yield return (nameof(knownPaths), knownPaths);
    }
}
