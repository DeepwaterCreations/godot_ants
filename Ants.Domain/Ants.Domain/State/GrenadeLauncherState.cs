﻿using System.Runtime.Intrinsics;

public class GrenadeLauncherState: IStateHash
{
    public int reloadDue;
    public Immuntable immuntable;

    public GrenadeLauncherState(int reloadDue, Immuntable immuntable)
    {
        this.reloadDue = reloadDue;
        this.immuntable = immuntable ?? throw new ArgumentNullException(nameof(immuntable));
    }
    //public override int GetHashCode()
    //{
    //    return this.Hash();
    //}

    public IEnumerable<(string, object?)> HashData()
    {
        yield return (nameof(reloadDue), reloadDue);
        yield return (nameof(immuntable), immuntable);
    }

    public void Update(AntState ant) {
        if (reloadDue > 0 && !ant.IsStuned()) { 
            reloadDue--;
        }
    }

    internal GrenadeLauncherState Copy()
    {
        return new GrenadeLauncherState(reloadDue, immuntable);
    }

    public class Immuntable : IStateHash
    {
        public readonly double speed;
        public readonly double maxRange;
        public readonly int reloadTime;


        public readonly double inner_speed;
        public readonly double inner_runForDistance;
        public readonly double inner_damage;
        public readonly double inner_ignoreCovoerFor;
        public readonly double inner_pierce;
        public readonly int inner_stun;
        public readonly int inner_knockBack;

        public Immuntable(double speed, double maxRange, int reloadTime, double inner_speed, double inner_maxRange,  double inner_damage, double inner_ignoreCovoerFor, double inner_pierce, int inner_stun, int inner_knockBack)
        {
            this.speed = speed;
            this.maxRange = maxRange;
            this.reloadTime = reloadTime;
            this.inner_speed = inner_speed;
            this.inner_runForDistance = inner_maxRange;
            this.inner_damage = inner_damage;
            this.inner_ignoreCovoerFor = inner_ignoreCovoerFor;
            this.inner_pierce = inner_pierce;
            this.inner_stun = inner_stun;
            this.inner_knockBack = inner_knockBack;
        }

        //public override bool Equals(object? obj)
        //{
        //    return obj is Immuntable immuntable &&
        //           speed == immuntable.speed &&
        //           maxRange == immuntable.maxRange &&
        //           reloadTime == immuntable.reloadTime &&
        //           inner_speed == immuntable.inner_speed &&
        //           inner_runForDistance == immuntable.inner_runForDistance &&
        //           inner_damage == immuntable.inner_damage &&
        //           inner_ignoreCovoerFor == immuntable.inner_ignoreCovoerFor &&
        //           inner_endingPierce == immuntable.inner_endingPierce &&
        //           inner_pierceAt200 == immuntable.inner_pierceAt200 &&
        //           inner_stun == immuntable.inner_stun;
        //}

        //public override int GetHashCode()
        //{
        //    return this.Hash();
        //}

        public IEnumerable<(string, object?)> HashData()
        {
            yield return (nameof(speed), speed);
            yield return (nameof(maxRange), maxRange);
            yield return (nameof(reloadTime), reloadTime);
            yield return (nameof(inner_speed), inner_speed);
            yield return (nameof(inner_runForDistance), inner_runForDistance);
            yield return (nameof(inner_damage), inner_speed);
            yield return (nameof(inner_ignoreCovoerFor), inner_ignoreCovoerFor);
            yield return (nameof(inner_pierce), inner_pierce);
            yield return (nameof(inner_stun), inner_stun);
            yield return (nameof(inner_knockBack), inner_knockBack);
        }
    }
}