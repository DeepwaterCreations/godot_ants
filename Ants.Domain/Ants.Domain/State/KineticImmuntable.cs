﻿public abstract class KineticImmuntable : PhysicsSubjectStateImmutable
{
    public readonly double mass = 1;
    public readonly double friction = .7;

    public KineticImmuntable(Mappable mappable) : base(mappable)
    {
    }
}
