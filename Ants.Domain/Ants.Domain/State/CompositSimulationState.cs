﻿namespace Ants.Domain.State
{
    public class CompositSimulationState : IStateHash {
        public readonly SimulationState simulationState;
        public readonly TargetState2 targetState;

        public CompositSimulationState(SimulationState simulationState, TargetState2 targetState)
        {
            this.simulationState = simulationState ?? throw new ArgumentNullException(nameof(simulationState));
            this.targetState = targetState ?? throw new ArgumentNullException(nameof(targetState));
        }

        //public override int GetHashCode()
        //{
        //    return this.Hash();
        //}

        public IEnumerable<(string, object?)> HashData()
        {
            yield return (nameof(simulationState), simulationState);
            yield return (nameof(targetState), targetState);
        }
    }
}