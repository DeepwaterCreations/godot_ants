﻿public class SprintState: IStateHash
{
    //private const double energyGain = .04;
    public bool sprinting = false;
    public double energy = 0;

    // immutable
    public class Immutable: IStateHash
    {
        public readonly double energyGain = .04;
        public readonly double maxEnergy = 120;
        public readonly double multiplySpeed = 2; // {8D52AA25-04FD-4A01-80B6-DFDFD32836CB} maybe this should be sprint speed? - how would that stack with shield

        public Immutable(double maxEnergy, double multiplySpeed, double energyGain)
        {
            this.maxEnergy = maxEnergy;
            this.multiplySpeed = multiplySpeed;
            this.energyGain = energyGain;
        }

        //public override bool Equals(object? obj)
        //{
        //    return obj is Immutable immutable &&
        //           maxEnergy == immutable.maxEnergy &&
        //           multiplySpeed == immutable.multiplySpeed;
        //}

        //public override int GetHashCode()
        //{
        //    return this.Hash();
        //    //return HashCode.Combine(maxEnergy, multiplySpeed);
        //}

        public IEnumerable<(string, object?)> HashData()
        {
            yield return (nameof(maxEnergy), maxEnergy);
            yield return (nameof(multiplySpeed), multiplySpeed);
            yield return (nameof(energyGain), energyGain);
        }
    }
    public readonly Immutable immutable;

    public SprintState(Immutable immutable) : this(false, -immutable.energyGain * AntState.warmUpFor, immutable) { }

    public SprintState(bool sprinting, double energy, Immutable immutable)
    {
        this.sprinting = sprinting;
        this.energy = energy;
        this.immutable = immutable ?? throw new ArgumentNullException(nameof(immutable));
    }

    public void Update(AntState ant)
    {
        if (ant.legs == null || !ant.legs.IsMoving()) {
            sprinting = false;
        }

        if (!sprinting && !ant.IsStuned()) {
            energy = Math.Min(immutable.maxEnergy, energy+ immutable.energyGain);
        }
    }

    public bool TryModifyForce(double forceMagnitude, out double resultForceMagnitude)
    {
        resultForceMagnitude = sprinting ? immutable.multiplySpeed * forceMagnitude : forceMagnitude;
        return sprinting;
    }

    internal SprintState Copy()
    {
        return new SprintState(sprinting, energy, immutable);
    }

    //public override bool Equals(object? obj)
    //{
    //    return obj is SprintState state &&
    //           sprinting == state.sprinting &&
    //           energy == state.energy &&
    //           EqualityComparer<Immutable>.Default.Equals(immutable, state.immutable);
    //}

    //public override int GetHashCode()
    //{
    //    return this.Hash();
    //    //return HashCode.Combine(sprinting, energy, immutable);
    //}

    public IEnumerable<(string, object?)> HashData()
    {
        yield return (nameof(sprinting), sprinting);
        yield return (nameof(energy), energy);
        yield return (nameof(immutable), immutable);
    }
}
