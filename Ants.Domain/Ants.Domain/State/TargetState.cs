﻿using System.Runtime.Intrinsics;

namespace Ants.Domain.State
{
    public class TargetState :IStateHash {
        public const int runEvery = 10;

        public readonly IReadOnlyDictionary<Guid, Guid> targeting;

        public TargetState(Dictionary<Guid, Guid> targeting)
        {
            this.targeting = targeting ?? throw new ArgumentNullException(nameof(targeting));
        }

        public static TargetState NothingTargeted()
        {
            return new TargetState(new Dictionary<Guid, Guid>());
        }

        //public override int GetHashCode()
        //{
        //    return this.Hash();
        //}

        public IEnumerable<(string, object?)> HashData()
        {
            yield return (nameof(targeting), targeting);
        }
    }

    public class PositionAndVelocity : IStateHash
    {
        public readonly Vector128<double> position; 
        public readonly Vector128<double> velocity;
        public readonly bool addScatter;

        public PositionAndVelocity(Vector128<double> position, Vector128<double> velocity, bool addScatter)
        {
            this.position = position;
            this.velocity = velocity;
            this.addScatter = addScatter;
        }

        public IEnumerable<(string, object?)> HashData()
        {
            yield return (nameof(position), position);
            yield return (nameof(velocity), velocity);
            yield return (nameof(addScatter), addScatter);
        }
    }

    public class PositionVelocityAndDirection : IStateHash
    {
        public readonly Vector128<double> position;
        public readonly Vector128<double> velocity;
        public readonly Vector128<double> direction;
        public readonly bool addScatter;
        public readonly double time;

        public PositionVelocityAndDirection(Vector128<double> position, Vector128<double> velocity, Vector128<double> direction, bool addScatter, double time)
        {
            this.position = position;
            this.velocity = velocity;
            this.direction = direction;
            this.addScatter = addScatter;
            this.time = time;
        }

        public IEnumerable<(string, object?)> HashData()
        {
            yield return (nameof(position), position);
            yield return (nameof(velocity), velocity);
            yield return (nameof(direction), direction);
            yield return (nameof(addScatter), addScatter);
            yield return (nameof(time), time);
        }
    }

    public class TargetState2 : IStateHash
    {
        public const int runEvery = 10;

        // value is a position
        public readonly IReadOnlyDictionary<Guid, PositionAndVelocity> targeting;
        // value is direction
        public readonly IReadOnlyDictionary<Guid, PositionVelocityAndDirection> arcTargeting;
        public readonly int at;

        public TargetState2(Dictionary<Guid, PositionAndVelocity> targeting, IReadOnlyDictionary<Guid, PositionVelocityAndDirection> arcTargeting, int at)
        {
            this.targeting = targeting ?? throw new ArgumentNullException(nameof(targeting));
            this.arcTargeting = arcTargeting ?? throw new ArgumentNullException(nameof(arcTargeting));
            this.at = at;
        }

        public static TargetState2 NothingTargeted(int at)
        {
            return new TargetState2(new Dictionary<Guid, PositionAndVelocity>(), new Dictionary<Guid, PositionVelocityAndDirection>(), at);
        }

        //public override int GetHashCode()
        //{
        //    return this.Hash();
        //}

        public IEnumerable<(string, object?)> HashData()
        {
            yield return (nameof(targeting), targeting);
            yield return (nameof(arcTargeting), arcTargeting);
            yield return (nameof(at), at);
        }
    }
}