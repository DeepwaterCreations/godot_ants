﻿public abstract class PhysicsSubjectStateImmutable: IStateHash
{
    public readonly Mappable mappable;

    public PhysicsSubjectStateImmutable(Mappable mappable)
    {
        this.mappable = mappable ?? throw new ArgumentNullException(nameof(mappable));
    }

    //public override bool Equals(object? obj)
    //{
    //    return obj is PhysicsSubjectStateImmutable immutable &&
    //           EqualityComparer<Mappable>.Default.Equals(mappable, immutable.mappable);
    //}

    //public override int GetHashCode()
    //{
    //    return this.Hash();
    //    //return HashCode.Combine(mappable);
    //}

    public abstract IEnumerable<(string, object?)> HashData();
}
