﻿using Ants.Domain.Infrastructure;
using Ants.Domain.State;
using System.Drawing;
using System.Runtime.Intrinsics.X86;

public class ControlState : IStateHash
{
    //public double[] control;
    public double Pool { get; private set; }

    public ControlState(/*double[] control,*/ double pool)
    {
        //this.control = control ?? throw new ArgumentNullException(nameof(control));
        this.Pool = pool;
    }

    public void AddToPool(double amount) {
        Pool += amount * 0.95;
    }

    //public override bool Equals(object? obj)
    //{
    //    return obj is ControlState state && state.Pool == Pool;
    //}

    //public override int GetHashCode()
    //{
    //    return this.Hash();
    //    //return (int)control.UncheckedSum();
    //}

    public IEnumerable<(string, object?)> HashData()
    {
        yield return (nameof(Pool), Pool);
    }

    public void Update(SimulationState state)
    {

        if (state.ants.Count() == 0) {
            return;
        }

        var toSpend = Math.Sqrt(Pool)*.003;


        foreach (var point in state.controlPoints)
        {
            var closestAnt = state.ants.OrderBy(x => Avx.Subtract(x.Value.position, point.position).Length()).First();

            var closestAntLength = Avx.Subtract(closestAnt.Value.position, point.position).Length();

            if (closestAntLength > 500) {
                point.side = 3; // 3 isn't really right, but it'll work for the 2 of us
                continue;
            }

            var add = toSpend / (double)state.controlPoints.Length;
            state.money.GainMainMoney(add, closestAnt.Value.immutable.side, add);
           
            point.side = closestAnt.Value.immutable.side;
        }

        Pool -= toSpend;
    }

    internal ControlState Copy()
    {
        return new ControlState(Pool);
    }
}
