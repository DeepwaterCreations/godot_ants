﻿using Ants.Domain.Infrastructure;
using Ants.Domain.State;
using System.Diagnostics;
using System.Runtime.Intrinsics;

public class ShieldState : BaseShieldState
{
    public enum ShieldAction { 
        enabling,
        disabling,
        none
    }

    private ShieldAction shieldAction = ShieldAction.none;
    private int progress;
    public const int enabled = 120;

    // immutable
    public class Immutable: IStateHash
    {
        public readonly double shieldedSpeedMultiplier = 1; // {8D52AA25-04FD-4A01-80B6-DFDFD32836CB} 
        public readonly int enableRate = 2;
        public readonly int disableRate = -1;

        public Immutable(double shieldedSpeedMultiplier)
        {
            this.shieldedSpeedMultiplier = shieldedSpeedMultiplier;
        }

        //public override bool Equals(object? obj)
        //{
        //    return obj is Immutable immutable &&
        //           shieldedSpeedMultiplier == immutable.shieldedSpeedMultiplier &&
        //           enableRate == immutable.enableRate &&
        //           disableRate == immutable.disableRate;
        //}

        //public override int GetHashCode()
        //{
        //    return this.Hash();
        //    //return HashCode.Combine(shieldedMultiplier, enableRate, disableRate);
        //}

        public IEnumerable<(string, object?)> HashData()
        {
            yield return (nameof(shieldedSpeedMultiplier), shieldedSpeedMultiplier);
            yield return (nameof(enableRate), enableRate);
            yield return (nameof(disableRate), disableRate);
        }
    }
    public readonly Immutable immutable;

    public ShieldState(Immutable immutable, BaseImmutable baseImmutable): this (ShieldAction.none, enabled, immutable, Vector128.Create(1.0,0.0), new Queue<(int tick, Vector128<double> from, double perce, double damagePreArmor)>(), baseImmutable, true, AntState.warmUpFor, 3)
    { 
    }


    public ShieldState(
        ShieldAction shieldAction, 
        int progress, 
        Immutable immutable,
        Vector128<double> facingUnit, 
        Queue<(int tick, Vector128<double> from, double perce, double damagePreArmor)> lastHits,
        BaseImmutable baseImmutable, 
        bool isShielded,
        int exposedFor,
        double exposedAmount
        ) : base(facingUnit, lastHits, baseImmutable, isShielded, exposedFor, exposedAmount)
    {
        this.shieldAction = shieldAction;
        this.progress = progress;
        this.immutable = immutable ?? throw new ArgumentNullException(nameof(immutable));
    }

    public override void Update(int tick, AntState ant) {
        base.Update(tick, ant);

        if (shieldAction == ShieldAction.enabling) {
            if (progress < enabled)
            {
                progress = Math.Min(enabled, progress + immutable.enableRate);
            }
            else
            {
                isShielded = true;
                shieldAction = ShieldAction.none;
            }
        }
        else if (shieldAction == ShieldAction.disabling)
        {
            if (progress > 0)
            {
                progress = Math.Max(0, progress + immutable.disableRate);
            }
            else
            {
                isShielded = false;
                shieldAction = ShieldAction.none;
            }
        }
    }

    public double ShieldEnbaledPercent() {
        switch (shieldAction)
        {
            case ShieldAction.enabling:
                return  progress / (double)enabled;
            case ShieldAction.disabling:
                return progress / (double)enabled;
            case ShieldAction.none:
                return isShielded ? 1.0 : 0.0;
            default:
                throw new NotImplementedException($"unexpected state {shieldAction}");
        }
    }

    public void Disable() {
        shieldAction = ShieldAction.disabling;
    }

    public void Enable()
    {
        shieldAction = ShieldAction.enabling;
    }

    public bool TryFriction(double friction, out double actualFriction)
    {
        if (!isShielded) {
            actualFriction = friction;
            return false;
        }

        // {0C8D3F28-3764-4548-B199-BA19133778DA}
        var old = friction / (1 - friction); 
        var target = old * immutable.shieldedSpeedMultiplier;   
        actualFriction = 1 / ((1 / target) + 1);
        Debug.Assert(actualFriction > 0);
        return true;
    }

    internal override ShieldState Copy()
    {
        return new ShieldState(shieldAction, progress, immutable, facingUnit, lastHits.ToQueue(), baseImmutable, isShielded, exposedFor, exposedAmount);
    }

    //public override bool Equals(object? obj)
    //{
    //    return obj is ShieldState state &&
    //           facingUnit.Equals(state.facingUnit) &&
    //           lastHits.SequenceEqual(state.lastHits) &&
    //           isShielded == state.isShielded &&
    //           shieldAction == state.shieldAction &&
    //           progress == state.progress &&
    //           EqualityComparer<Immutable>.Default.Equals(immutable, state.immutable);
    //}

    //public override int GetHashCode()
    //{
    //    return this.Hash();
    //    //return HashCode.Combine(
    //    //    facingUnit, 
    //    //    lastHits.UncheckedSum(x=>x.GetHashCode()),
    //    //    isShielded, 
    //    //    shieldAction,
    //    //    progress, 
    //    //    immutable);
    //}

    public override IEnumerable<(string, object?)> HashData()
    {
        yield return (nameof(facingUnit), facingUnit);
        yield return (nameof(lastHits), lastHits);
        yield return (nameof(isShielded), isShielded);
        yield return (nameof(shieldAction), shieldAction);
        yield return (nameof(progress), progress);
        yield return (nameof(immutable), immutable);
        yield return (nameof(exposedAmount), exposedAmount);
        yield return (nameof(exposedFor), exposedFor);
    }
}
