﻿using Ants.Domain.Infrastructure;
using System.Runtime.Intrinsics;
using System.Runtime.Intrinsics.X86;
using static BulletState.Immutable;

public class ArcBulletState : PhysicsSubjectState2 {

    public readonly Immutable immutable;
    private double distanceTravelled;
    public readonly HashSet<Vector128<double>> coverHit;

    public ArcBulletState(Immutable immutable, ImmunatblePhysicsState immunatblePhysics, Vector128<double> position, Vector128<double> velocity, double distanceTravelled, HashSet<Vector128<double>> coverHit) : base(immunatblePhysics, position, velocity)
    {
        this.immutable = immutable;
        this.distanceTravelled = distanceTravelled;
        this.coverHit = coverHit;
    }

    public override IEnumerable<(string, object?)> HashData()
    {
        foreach (var item in base.HashData())
        {
            yield return item;
        }
        yield return (nameof(immutable), immutable);
        yield return (nameof(distanceTravelled), distanceTravelled);
        yield return (nameof(coverHit), coverHit);
    }

    public double GetDistanceTravelled() { 
        return distanceTravelled;
    }

    internal ArcBulletState Copy()
    {
        return new ArcBulletState(immutable, physicsImmunatble, position, velocity, distanceTravelled, coverHit.ToHashSet());
    }

    internal bool Update()
    {
        var velocity = Velocity(position, immutable.center, immutable.speed);

        this.velocity = velocity;

        distanceTravelled += immutable.speed;

        return distanceTravelled > immutable.runFor;
    }

    private static Vector128<double> Velocity(Vector128<double> position, Vector128<double> center, double speed)
    {
        var fromCenter = Avx.Subtract(position, center);
        var radius = fromCenter.Length();
        // theta * r = arc
        // theta = arc/r
        var angle = speed / radius;
        var nextFromCenter = fromCenter.Rotated(angle);
        var velocity = Avx.Subtract(nextFromCenter, fromCenter);
        return velocity;
    }

    public class Immutable : IComparable<ArcBulletState.Immutable>, IStateHash
    {
        public class Id : IStateHash, IComparable<ArcBulletState.Immutable.Id>
        {
            /// <summary>
            /// used to unique identify the shot
            /// we hold an id instead of a reference because references are constantly throw away and rebuilt
            /// </summary>
            public readonly Guid shooterId;
            /// <summary>
            /// used to unique identify the shot
            /// </summary>  
            public readonly int shotOnFrame;
            /// <summary>
            /// used to unique identify the shot
            /// 0 if this is the first shot the shooter shot on that frame
            /// 1 if this is the second and so on
            /// </summary>
            public readonly int shotIndex;


            public Id(Guid shooterId, int shotOnFrame, int shotIndex)
            {
                this.shooterId = shooterId;
                this.shotOnFrame = shotOnFrame;
                this.shotIndex = shotIndex;
            }

            public int CompareTo(Id? other)
            {
                if (other == null) return 1;

                if (shooterId.CompareTo(other.shooterId) != 0)
                {
                    return shooterId.CompareTo(other.shooterId);
                }
                if (shotOnFrame.CompareTo(other.shotOnFrame) != 0)
                {
                    return shotOnFrame.CompareTo(other.shotOnFrame);
                }

                if (shotIndex.CompareTo(other.shotIndex) != 0)
                {
                    return shotIndex.CompareTo(other.shotIndex);
                }
                return 0;
            }

            public override bool Equals(object? obj)
            {
                return obj is Id id &&
                       shooterId.Equals(id.shooterId) &&
                       shotOnFrame == id.shotOnFrame &&
                       shotIndex == id.shotIndex
                       ;
            }

            public override int GetHashCode()
            {
                return this.Hash();
                //return HashCode.Combine(shooterId, shotOnFrame, shotIndex);
            }

            public IEnumerable<(string, object?)> HashData()
            {
                yield return (nameof(shooterId), shooterId);
                yield return (nameof(shotOnFrame), shotOnFrame);
                yield return (nameof(shotIndex), shotIndex);
            }

            public override string ToString()
            {
                return $"({shooterId}, {shotOnFrame}, {shotIndex})";
            }
        }

        public readonly Id id;

        public readonly double runFor;
        public readonly double speed;
        public readonly int side;

        public readonly double damage;
        public readonly double pierce = 10;
        public readonly double ignoreCoverFor;

        public readonly int stun;// ticks

        public readonly bool damagesTeam;

        public readonly Vector128<double> center;

        public readonly double knockBack;

        public Immutable(Id id, double runFor, double speed, int side, double damage, double pierce, double ignoreCoverFor, int stun, bool damagesTeam, Vector128<double> center, double knockBack)
        {
            this.id = id ?? throw new ArgumentNullException(nameof(id));
            this.runFor = runFor;
            this.speed = speed;
            this.side = side;
            this.damage = damage;
            this.pierce = pierce;
            this.ignoreCoverFor = ignoreCoverFor;
            this.stun = stun;
            this.damagesTeam = damagesTeam;
            this.center = center;
            this.knockBack = knockBack;
        }

        public int CompareTo(Immutable? other)
        {
            if (other == null) return 1;

            return id.CompareTo(other.id);
        }
        public IEnumerable<(string, object?)> HashData()
        {
            yield return (nameof(id), id);
            yield return (nameof(speed), speed);
            yield return (nameof(side), side);
            yield return (nameof(damage), damage);
            yield return (nameof(pierce), pierce);
            yield return (nameof(stun), stun);
            yield return (nameof(damagesTeam), damagesTeam);
            yield return (nameof(center), center);
            yield return (nameof(knockBack), knockBack);
        }
    }


    public static ArcBulletState MakeBullet(Vector128<double> position, Immutable immutable)
    {

        return new ArcBulletState(
            immutable,
            new ImmunatblePhysicsState(1),// TODO is this always "1"? To really support, we'll have to update who to shoot to support avoid by
            position,
            Velocity(position, immutable.center, immutable.speed),
            0,
            new HashSet<Vector128<double>>()
            );
    }
}

public class BulletState : PhysicsSubjectState2
{


    // immutable 
    public class Immutable : IComparable<BulletState.Immutable>, IStateHash
    {
        public class Id:IStateHash , IComparable<BulletState.Immutable.Id>
        {
            /// <summary>
            /// used to unique identify the shot
            /// we hold an id instead of a reference because references are constantly throw away and rebuilt
            /// </summary>
            public readonly Guid shooterId;
            /// <summary>
            /// used to unique identify the shot
            /// </summary>  
            public readonly int shotOnFrame;
            /// <summary>
            /// used to unique identify the shot
            /// 0 if this is the first shot the shooter shot on that frame
            /// 1 if this is the second and so on
            /// </summary>
            public readonly int shotIndex;


            public Id(Guid shooterId, int shotOnFrame, int shotIndex)
            {
                this.shooterId = shooterId;
                this.shotOnFrame = shotOnFrame;
                this.shotIndex = shotIndex;
            }

            public int CompareTo(Id? other)
            {
                if (other == null) return 1;

                if (shooterId.CompareTo(other.shooterId) != 0)
                {
                    return shooterId.CompareTo(other.shooterId);
                }
                if (shotOnFrame.CompareTo(other.shotOnFrame) != 0)
                {
                    return shotOnFrame.CompareTo(other.shotOnFrame);
                }

                if (shotIndex.CompareTo(other.shotIndex) != 0)
                {
                    return shotIndex.CompareTo(other.shotIndex);
                }
                return 0;
            }

            public override bool Equals(object? obj)
            {
                return obj is Id id &&
                       shooterId.Equals(id.shooterId) &&
                       shotOnFrame == id.shotOnFrame &&
                       shotIndex == id.shotIndex
                       ;
            }

            public override int GetHashCode()
            {
                return this.Hash();
                //return HashCode.Combine(shooterId, shotOnFrame, shotIndex);
            }

            public IEnumerable<(string, object?)> HashData()
            {
                yield return (nameof(shooterId), shooterId);
                yield return (nameof(shotOnFrame), shotOnFrame);
                yield return (nameof(shotIndex), shotIndex);
            }

            public override string ToString()
            {
                return $"({shooterId}, {shotOnFrame}, {shotIndex})";
            }
        }

        public readonly Id id;

        public readonly double runFor;
        public readonly double speed;
        public readonly Vector128<double> direction;
        public readonly int side;

        public readonly Vector128<double> startedAt;
        public readonly double damage; 
        public readonly double pierce = 10;
        public readonly double ignoreCoverFor; // why is this still a thing? {A48990DF-ABDE-48ED-895B-16A49CC38ED8}

        public readonly int stun;// ticks

        public readonly bool damagesTeam;
        public readonly bool damagesShooter;

        public readonly double knockBack;

        public Immutable(Guid shooterId, int frame, int shooterShotOnFrame,
            double runFor, double speed, Vector128<double> direction, int side, 
            Vector128<double> startedAt, double damage,
            double ignoreCoverFor, double pierce, int stun, bool damagesTeam, double knockBack, bool damagesShooter)
        {
            this.id = new Id(shooterId, frame, shooterShotOnFrame);
            this.runFor = runFor;
            this.speed = speed;
            this.direction = direction;
            this.side = side;
            this.startedAt = startedAt;
            this.damage = damage;
            this.pierce = pierce;
            this.ignoreCoverFor = ignoreCoverFor;
            this.stun = stun;
            this.damagesTeam = damagesTeam;
            this.knockBack = knockBack;
            this.damagesShooter = damagesShooter;
        }

        public int CompareTo(Immutable? other)
        {
            if (other == null) return 1;

            return id.CompareTo(other.id);
        }

        //public override bool Equals(object? obj)
        //{
        //    return obj is Immutable immutable &&
        //           id.Equals(immutable.id) &&
        //           runFor == immutable.runFor &&
        //           speed == immutable.speed &&
        //           direction.Equals(immutable.direction) &&
        //           side == immutable.side &&
        //           startedAt.Equals(immutable.startedAt) &&
        //           startingDamge == immutable.startingDamge &&
        //           endingPierce == immutable.endingPierce &&
        //           pierceAt200 == immutable.pierceAt200 &&
        //           ignoreCoverFor == immutable.ignoreCoverFor &&
        //           stun == immutable.stun &&
        //           damagesTeam== immutable.damagesTeam;
        //}

        //public override int GetHashCode()
        //{
        //    return this.Hash();
        //    //return HashCode.Combine(id, runFor, speed, direction, side, startedAt, startingDamge, startingPierce);
        //}

        public IEnumerable<(string, object?)> HashData()
        {
            yield return (nameof(id), id);
            yield return (nameof(runFor), runFor);
            yield return (nameof(speed), speed);
            yield return (nameof(direction), direction);
            yield return (nameof(side), side);
            yield return (nameof(startedAt), startedAt);
            yield return (nameof(damage), damage);
            yield return (nameof(pierce), pierce);
            yield return (nameof(ignoreCoverFor), ignoreCoverFor);
            yield return (nameof(stun), stun);
            yield return (nameof(damagesTeam), damagesTeam);
            yield return (nameof(damagesShooter), damagesShooter);
            yield return (nameof(knockBack), knockBack);
        }
    }
    public readonly Immutable immutable;

    public readonly HashSet<Vector128<double>> coverHit;

    private BulletState(Vector128<double> position, Immutable immutable, ImmunatblePhysicsState physicsImmunatble, HashSet<Vector128<double>> coverHit): base(physicsImmunatble, position, Avx.Multiply(immutable.direction, immutable.speed.AsVector()))
    {
        this.immutable = immutable;
        this.coverHit = coverHit;
    }

    public static BulletState MakeBullet(Vector128<double> position, Immutable immutable) {

        return new BulletState(
            position, 
            immutable, 
            new ImmunatblePhysicsState(1),// TODO is this always "1"? To really support, we'll have to update who to shoot to support avoid by
            new HashSet<Vector128<double>>()
            );
    }

    //public (int x, int y)[] ReletivePositions() => new[] { (0, 0) };
    //public (int x, int y)[] ReletiveTopPositions() => new[] { (0, 0) };
    //public (int x, int y)[] ReletiveBottomPositions() => new[] { (0, 0) };
    //public (int x, int y)[] ReletiveLeftPositions() => new[] { (0, 0) };
    //public (int x, int y)[] ReletiveRightPositions() => new[] { (0, 0) };
    //public (int x, int y)[] ReletiveEdgePositions() => new[] { (0, 0) };
    //public (int x, int y)[] ReletiveBottomRightPositions() => new[] { (0, 0) };
    //public (int x, int y)[] ReletiveTopLeftPositions() => new[] { (0, 0) };
    //public (int x, int y)[] ReletiveTopRightPositions() => new[] { (0, 0) };
    //public (int x, int y)[] ReletiveBottomLeftPositions() => new[] { (0, 0) };


    //public static double DamageAtRange(double range, double ranFor, double startingDamage) {
    //    return startingDamage;
    //}

    public static double PierceAtRange(double ranFor, double PierceAt200/*includes radius {8307C005-B1E7-4CB9-ADE8-5A4AD347B85C}*/,  double endingPierce)
    {
        // at0/(1 + 200.0) = (PierceAt100 - endingPierce)
        // at0 = (PierceAt200 - endingPierce)*(201.0)

        var at0 = (PierceAt200 - endingPierce) * (201.0);
        return (at0 / (1 + ranFor)) + endingPierce;
    }

    //return true if killed
    public bool Update()
    {
        //damage = ((immutable.runFor - Avx.Subtract(GlobalPosition, immutable.startedAt.ToGlobalVector()).Length()) / immutable.runFor) * immutable.startingDamge;//Math.Max(startingDamge/10, damage - startingDamge * delta/2.0);

            //((immutable.runFor - Avx.Subtract(GlobalPosition, immutable.startedAt.ToGlobalVector()).Length()) / immutable.runFor) * immutable.startingPierce;

        return Avx.Subtract(position, immutable.startedAt).Length() > immutable.runFor;
    }

    internal BulletState Copy()
    {
        return new BulletState(position, immutable, physicsImmunatble, coverHit.ToHashSet());
    }

    public double CurrentPierce() {
        return immutable.pierce;//PierceAtRange(Avx.Subtract(position, immutable.startedAt).Length(), immutable.pierceAt200, immutable.endingPierce);
    }

    //public Immutable SortHash() => immutable;

    public MapEntry ToMapEntry() => MapEntry.Make(immutable.id);

    //public override bool Equals(object? obj)
    //{
    //    return obj is BulletState state &&
    //           position.Equals(state.position) &&
    //           velocity.Equals(state.velocity) &&
    //           EqualityComparer<Immutable>.Default.Equals(immutable, state.immutable);
    //}

    //public override int GetHashCode()
    //{
    //    return this.Hash();
    //}

    public override IEnumerable<(string, object?)> HashData()
    {
        foreach (var item in base.HashData())
        {
            yield return item;
        }
        yield return (nameof(immutable), immutable);
        yield return (nameof(coverHit), coverHit);
    }

    //public (Guid shooterId, int shotOnFrame, int shotIndex) GetThing()
    //{
    //    return (immutable.shooterId, immutable.shotOnFrame, immutable.shotIndex);
    //}
}
