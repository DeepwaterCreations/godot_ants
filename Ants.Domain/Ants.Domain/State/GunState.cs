﻿using Ants.Domain.Infrastructure;
using Ants.Domain.State;
using System;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.InteropServices;
using System.Runtime.Intrinsics;
using System.Runtime.Intrinsics.X86;

public class ArcGunState : IStateHash
{
    /// <summary>
    /// in ticks
    /// </summary>
    public uint reloadDue;

    public class Immutable : IStateHash
    {
        public readonly double pierce = 10;
        //public readonly double endingPierce = 10;
        // includes radius
        // {8307C005-B1E7-4CB9-ADE8-5A4AD347B85C}
        //public readonly double pierceAt200 = 30;
        public readonly double damage = 3;
        public readonly double shotSpeed = 10;
        public readonly double scatterAngleRad = 0.1;
        public readonly uint relaodTimeTick = 30;
        public readonly bool canMoveAndShoot = false;
        public readonly bool canMoveAndRelaod = false;
        public readonly double range = 800;
        public readonly double rangeScatter = 50;
        public readonly int setUpTimeTicks = 6;
        public readonly int shots = 1;
        public readonly double ignoreCoverFor = 300;
        public readonly double ignoreCoverForScatter = 100;
        public readonly int stun = 60;
        public readonly double knockBack;
        public readonly double radius;

        public Immutable(
            double pierce,
            double damage,
            double shotSpeed,
            double scatterAngleRad,
            uint relaodTimeTick,
            bool canMoveAndShoot,
            bool canMoveAndRelaod,
            double range,
            double rangeScatter,
            int setUpTimeTicks,
            int shots,
            double ignoreCoverFor,
            double ignoreCoverForScatter,
            bool autoShoot,
            int stun,
            double knockBack,
            double radius)
        {
            this.pierce = pierce;
            this.damage = damage;
            this.shotSpeed = shotSpeed;
            this.scatterAngleRad = scatterAngleRad;
            this.relaodTimeTick = relaodTimeTick;
            this.canMoveAndShoot = canMoveAndShoot;
            this.canMoveAndRelaod = canMoveAndRelaod;
            this.range = range;
            this.rangeScatter = rangeScatter;
            this.setUpTimeTicks = setUpTimeTicks;
            this.shots = shots;
            this.ignoreCoverFor = ignoreCoverFor;
            this.ignoreCoverForScatter = ignoreCoverForScatter;
            this.autoShoot = autoShoot;
            this.stun = stun;
            this.knockBack = knockBack;
            this.radius = radius;
        }

        public bool autoShoot { get; init; } = true;

        public IEnumerable<(string, object?)> HashData()
        {
            yield return (nameof(pierce), pierce);
            yield return (nameof(damage), damage);
            yield return (nameof(shotSpeed), shotSpeed);
            yield return (nameof(scatterAngleRad), scatterAngleRad);
            yield return (nameof(relaodTimeTick), relaodTimeTick);
            yield return (nameof(canMoveAndShoot), canMoveAndShoot);
            yield return (nameof(range), range);
            yield return (nameof(rangeScatter), rangeScatter);
            yield return (nameof(setUpTimeTicks), setUpTimeTicks);
            yield return (nameof(shots), shots);
            yield return (nameof(autoShoot), autoShoot);
            yield return (nameof(stun), stun);
            yield return (nameof(radius), radius);
        }
    }

    public readonly Immutable immutable;

    public ArcGunState(Immutable immutable) : this(AntState.warmUpFor, immutable) { }

    public ArcGunState(uint reloadDue, Immutable immutable)
    {
        this.reloadDue = reloadDue;
        this.immutable = immutable ?? throw new ArgumentNullException(nameof(immutable));
    }

    public void Update(AntState ant)
    {
        if ((immutable.canMoveAndRelaod || ant.legs == null || !ant.legs.IsMoving()) && reloadDue != 0 && !ant.IsStuned())
        {
            reloadDue--;
        }
    }
    internal ArcGunState Copy()
    {
        return new ArcGunState(reloadDue, immutable);
    }
    public IEnumerable<(string, object?)> HashData()
    {
        yield return (nameof(reloadDue), reloadDue);
        yield return (nameof(immutable), immutable);
    }

    public static Vector128<double> GetShotDirection(Vector128<double> from, Vector128<double> shootAt, Guid shooterId, int frame, int shot, double scatterAngleRad)
    {
        var normalizedDirection = Avx.Subtract(shootAt, from).NormalizedCopy();

        // TODO more real scatter
        // bell curve?
        // probably naw, that would make my who is best to shoot at math harder

        var rand = SimulationSafeRandom.GetDouble(-.5, .5, shooterId, frame, shot, Guid.Parse("{0FD2D24E-E488-4542-94DE-AAFDEAEAF01C}"));//nameof(GetShotDirection
        normalizedDirection = normalizedDirection.Rotated(rand * scatterAngleRad);
        return normalizedDirection;
    }

    internal static ArcBulletState[] Shoot(Vector128<double> from, Vector128<double> direction, AntState shooter, int frame, ShotIndexTracker shotIndexTracker, double addScatter)
    {
        shooter.arcGun!.reloadDue = shooter.arcGun.immutable.relaodTimeTick;
        //TestSafeLog.WriteLine("shooter.Gun.reloadDue" + shooter.Gun.reloadDue);

        var res = new ArcBulletState[shooter.arcGun.immutable.shots];

        for (int i = 0; i < res.Length; i++)
        {
            var rand = SimulationSafeRandom.GetDouble(-.5 + addScatter, .5 + addScatter, shooter.immutable.id, frame, i, Guid.Parse("{3759C55C-650F-40BC-A02A-8FD119D762E9}"));
            var center = Avx.Add(direction.Rotated((Math.Tau / 4.0) + (rand * shooter.arcGun.immutable.scatterAngleRad)).ToLength(shooter.arcGun.immutable.radius), from);

            res[i] = ArcBulletState.MakeBullet(
                from,
                new ArcBulletState.Immutable(
                    new ArcBulletState.Immutable.Id(shooter.immutable.id, frame, shotIndexTracker.Get(shooter.immutable.id)),
                    GunState.GetRunFor(shooter.immutable.id, frame, i, shooter.arcGun.immutable.range, shooter.arcGun.immutable.rangeScatter),
                    shooter.arcGun.immutable.shotSpeed,
                    shooter.immutable.side,
                    shooter.arcGun.immutable.damage,
                    shooter.arcGun.immutable.pierce,
                    GunState.GetIgnoreCoverFor(shooter.immutable.id, frame, i, shooter.arcGun.immutable.ignoreCoverFor, shooter.arcGun.immutable.ignoreCoverForScatter),
                    shooter.arcGun.immutable.stun,
                    false,
                    center,
                    shooter.arcGun.immutable.knockBack));
        }
        return res;
    }

}

public class GunState : IStateHash
{
    /// <summary>
    /// in ticks
    /// </summary>
    public uint reloadDue;

    // immutable 
    public class Immutable : IStateHash
    {
        public readonly double pierce = 10;
        //public readonly double endingPierce = 10;
        // includes radius
        // {8307C005-B1E7-4CB9-ADE8-5A4AD347B85C}
        //public readonly double pierceAt200 = 30;
        public readonly double damage = 3;
        public readonly double shotSpeed = 10;
        public readonly double scatterAngleRad = 0.1;
        public readonly uint relaodTimeTick = 30;
        public readonly bool canMoveAndShoot = false;
        public readonly bool canMoveAndRelaod = false;
        public readonly double range = 800;
        public readonly double rangeScatter = 50;
        public readonly int setUpTimeTicks = 6;
        public readonly int shots = 1;
        public readonly double ignoreCoverFor = 300;
        public readonly double ignoreCoverForScatter = 100;
        public readonly int stun = 60;
        public readonly double knockBack;

        public Immutable(
            double pierce,
            double damage,
            double shotSpeed,
            double scatterAngleRad,
            uint relaodTimeTick,
            bool canMoveAndShoot,
            bool canMoveAndRelaod,
            double range,
            double rangeScatter,
            int setUpTimeTicks,
            int shots,
            double ignoreCoverFor,
            double ignoreCoverForScatter,
            bool autoShoot,
            int stun,
            double knockBack)
        {
            this.pierce = pierce;
            this.damage = damage;
            this.shotSpeed = shotSpeed;
            this.scatterAngleRad = scatterAngleRad;
            this.relaodTimeTick = relaodTimeTick;
            this.canMoveAndShoot = canMoveAndShoot;
            this.canMoveAndRelaod = canMoveAndRelaod;
            this.range = range;
            this.rangeScatter = rangeScatter;
            this.setUpTimeTicks = setUpTimeTicks;
            this.shots = shots;
            this.ignoreCoverFor = ignoreCoverFor;
            this.ignoreCoverForScatter = ignoreCoverForScatter;
            this.autoShoot = autoShoot;
            this.stun = stun;
            this.knockBack = knockBack;
        }

        public bool autoShoot { get; init; } = true;

        //public override bool Equals(object? obj)
        //{
        //    return obj is Immutable immutable &&
        //           endingPierce == immutable.endingPierce &&
        //           pierceAt200 == immutable.pierceAt200 &&
        //           damage == immutable.damage &&
        //           shotSpeed == immutable.shotSpeed &&
        //           scatterAngleRad == immutable.scatterAngleRad &&
        //           relaodTimeTick == immutable.relaodTimeTick &&
        //           canMoveAndShoot == immutable.canMoveAndShoot &&
        //           canMoveAndRelaod == immutable.canMoveAndRelaod &&
        //           range == immutable.range &&
        //           rangeScatter == immutable.rangeScatter &&
        //           setUpTimeTicks == immutable.setUpTimeTicks &&
        //           shots == immutable.shots &&
        //           autoShoot == immutable.autoShoot &&
        //           stun == immutable.stun;
        //}

        //public override int GetHashCode()
        //{
        //    return this.Hash();
        //    //HashCode hash = new HashCode();
        //    //hash.Add(perce);
        //    //hash.Add(damage);
        //    //hash.Add(shotSpeed);
        //    //hash.Add(scatterAngleRad);
        //    //hash.Add(relaodTimeTick);
        //    //hash.Add(canMoveAndShoot);
        //    //hash.Add(canMoveAndRelaod);
        //    //hash.Add(range);
        //    //hash.Add(rangeScatter);
        //    //hash.Add(setUpTimeTicks);
        //    //hash.Add(shots);
        //    //hash.Add(autoShoot);
        //    //return hash.ToHashCode();
        //}

        public IEnumerable<(string, object?)> HashData()
        {
            yield return (nameof(pierce), pierce);
            yield return (nameof(damage), damage);
            yield return (nameof(shotSpeed), shotSpeed);
            yield return (nameof(scatterAngleRad), scatterAngleRad);
            yield return (nameof(relaodTimeTick), relaodTimeTick);
            yield return (nameof(canMoveAndShoot), canMoveAndShoot);
            yield return (nameof(range), range);
            yield return (nameof(rangeScatter), rangeScatter);
            yield return (nameof(setUpTimeTicks), setUpTimeTicks);
            yield return (nameof(shots), shots);
            yield return (nameof(autoShoot), autoShoot);
            yield return (nameof(stun), stun);
        }
    }
    public readonly Immutable immutable;

    // sub-states
    //public readonly MultiShotImmutableState multiShot;

    public GunState(Immutable immutable) : this(AntState.warmUpFor, immutable) { }

    public GunState(uint reloadDue, Immutable immutable/* MultiShotImmutableState multiShot*/)
    {
        this.reloadDue = reloadDue;
        this.immutable = immutable ?? throw new ArgumentNullException(nameof(immutable));
        //this.multiShot = multiShot ?? throw new ArgumentNullException(nameof(multiShot));
    }

    public void Update(AntState ant)
    {
        if ((immutable.canMoveAndRelaod || ant.legs == null || !ant.legs.IsMoving()) && reloadDue != 0 && !ant.IsStuned())
        {
            reloadDue--;
        }
    }


    public static double OddsToPassCoverAtRange(double targetRange, double ignoreCoverFor, double ignoreCoverForScatter)
    {
        return 1.0 - (1.0 / (1 + Math.Pow(Math.E, -((targetRange - ignoreCoverFor) / ignoreCoverForScatter))));
    }

    public double OddsToReach(double targetRange)
    {
        return OddsToReach(targetRange, immutable.range, immutable.rangeScatter);
    }

    public static double OddsToReach(double targetRange, double range, double rangeScatter)
    {
        return 1.0 - 1.0 / (1 + Math.Pow(Math.E, -((targetRange - range) / rangeScatter)));
    }

    // dup
    // {7BF2FFDA-B876-4929-82DA-D68C4374ADD7}
    public double GetRunFor(Guid shooerId, int frame, int shot)
    {
        return GetRunFor(shooerId, frame, shot, immutable.range, immutable.rangeScatter);
    }

    public static double GetRunFor(Guid shooerId, int frame, int shot, double range, double rangeScatter)
    {
        // y = 1 - 1/(1+ e^(-((x-range)/rangeScatter)))
        // y - 1 = -1/(1+ e^(-(x-range)/rangeScatter))
        // 1 - y = 1/(1+ e^(-(x-range)/rangeScatter))
        // 1/(1-y) = 1+ e^(-(x-range)/rangeScatter)
        // (1/(1-y)) - 1 = e^(-(x-range)/rangeScatter)
        // ln(1/(1-y) - 1) = -(x-range)/rangeScatter
        // ln(1/(1-y) - 1)*rangeScatter = -(x-range)/
        // -ln(1/(1-y) - 1)*rangeScatter = x - range
        // -ln(1/(1-y) - 1)*rangeScatter + range = x
        var rand = SimulationSafeRandom.GetDouble(0, 1, shooerId, frame, shot, Guid.Parse("{D52E4265-5E76-4A0C-B618-0FD4EF8F5F37}"));//nameof(GetRunFor)
        return -((double)Math.Log((1.0 / rand) - 1.0) * rangeScatter) + range;

        //return Math.Min(targetDistance + (shotSpeed/60.0), -((double)Math.Log((1.0 / (1.0 - TestSafeGD.RandfRange(0,1)))- 1.0) * rangeScatter) + Math.Min(targetDistance, range));
    }

    // this just copies that ^
    // dup
    // {D14503FA-4B89-4788-8A6C-35CEA6F976CF}
    // but it does have a different event id D52E4265-5E76-4A0C-B618-0FD4EF8F5F37 vs A21DB99F-6012-450C-B5B7-C4CEBF6D54DA
    private double GetIgnoreCoverFor(Guid shooerId, int frame, int shot)
    {
        return GetIgnoreCoverFor(shooerId, frame, shot, immutable.ignoreCoverFor, immutable.ignoreCoverForScatter);
    }

    public static double GetIgnoreCoverFor(Guid shooerId, int frame, int shot, double ignoreCoverFor, double ignoreCoverForScatter)
    {
        var rand = SimulationSafeRandom.GetDouble(0, 1, shooerId, frame, shot, Guid.Parse("{A21DB99F-6012-450C-B5B7-C4CEBF6D54DA}"));//nameof(GetRunFor)
        return -((double)Math.Log((1.0 / rand) - 1.0) * ignoreCoverForScatter) + ignoreCoverFor;
    }

    public Vector128<double> GetShotDirection(Vector128<double> from, Vector128<double> shootAt, Guid shooterId, int frame, int shot, double addScatter)
    {
        return GetShotDirection(from, shootAt, shooterId, frame, shot, immutable.scatterAngleRad, addScatter);
    }

    public static Vector128<double> GetShotDirection(Vector128<double> from, Vector128<double> shootAt, Guid shooterId, int frame, int shot, double scatterAngleRad, double addScatter)
    {
        var normalizedDirection = Avx.Subtract(shootAt, from).NormalizedCopy();

        // TODO more real scatter
        // bell curve?
        // probably naw, that would make my who is best to shoot at math harder
  
        var rand = SimulationSafeRandom.GetDouble(-.5 + addScatter, .5 + addScatter, shooterId, frame, shot, Guid.Parse("{0FD2D24E-E488-4542-94DE-AAFDEAEAF01C}"));//nameof(GetShotDirection
        normalizedDirection = normalizedDirection.Rotated(rand * scatterAngleRad);
        return normalizedDirection;
    }

    /// <summary>
    /// doesn't take into account target velocity
    /// </summary>
    public static IEnumerable<(Vector128<double> at, double time)> SimpleTargetArc(Vector128<double> shootingFrom, Vector128<double> targetStart, double arcRadius, double shotSpeed)
    {
        var startingDistance = Avx.Subtract(targetStart, shootingFrom).Length();
        var fullTime = arcRadius * Math.Tau / shotSpeed;
        var guess = (Math.Asin(Math.Min(1, startingDistance / (2 * arcRadius))) * 2 * arcRadius) / shotSpeed;
        yield return (targetStart, guess);
        var guess2 = fullTime - guess;
        yield return (targetStart, guess2);
    }

    public static bool TryTargetArc(Vector128<double> shootingFrom, Vector128<double> targetStart, Vector128<double> targetVelocity, double arcRadius, double shotSpeed, double guess, [NotNullWhen(true)] out (Vector128<double> at, double time)? result) 
    {

        var function = TargetDistanceMinusArcDistance(shootingFrom, targetStart, targetVelocity, arcRadius, shotSpeed);
        var derivative = TargetDistanceMinusArcDistanceDerivative(shootingFrom, targetStart, targetVelocity, arcRadius, shotSpeed);

        if (!NewtonsMethod(function, derivative, guess, 5, out var time))
        {
            result = default;
            return false;
        }
        if (double.IsNaN(time))
        {
            throw new Exception($"time is NaN");
        }
        result = (Avx.Add(targetStart, Avx.Multiply(targetVelocity, time.AsVector())), time);
        return true;
    }

    public static IEnumerable<(Vector128<double> at, double time)> TryTargetArc(Vector128<double> shootingFrom, Vector128<double> targetStart, Vector128<double> targetVelocity, double arcRadius, double shotSpeed)
    {
        var function = TargetDistanceMinusArcDistance(shootingFrom, targetStart, targetVelocity, arcRadius, shotSpeed);
        var derivative = TargetDistanceMinusArcDistanceDerivative(shootingFrom, targetStart, targetVelocity, arcRadius, shotSpeed);

        // startingDistance = 2 * arcRadius * sin(shotSpeed * time / arcRadius)
        // startingDistance/ (2 * arcRadius) = sin(shotSpeed * time / arcRadius)
        // startingDistance/ (2 * arcRadius) = sin(shotSpeed * time / arcRadius)
        // arcsin(startingDistance/ (2 * arcRadius)) = shotSpeed * time /arcRadius
        // arcsin(startingDistance/ (2 * arcRadius)) * arcRadius = shotSpeed * time
        // (arcsin(startingDistance/ (2 * arcRadius)) * arcRadius) / shotSpeed = time
        var startingDistance = Avx.Subtract(targetStart, shootingFrom).Length();
        var guess = (Math.Asin(Math.Min(1, startingDistance / (2 * arcRadius))) * 2 * arcRadius) / shotSpeed;


        var fullTime = arcRadius * Math.Tau / shotSpeed;
        // I think this is true but if it is not, I'd like to know
        Debug.Assert(guess <= fullTime * .5);
        if (!NewtonsMethod(function, derivative, guess, 5, out var time1))
        {
            yield break;
        }
        if (double.IsNaN(time1))
        {
            throw new Exception($"time1 is NaN");
        }
        if (time1 >= 0 && time1 <= fullTime)
        {
            yield return (Avx.Add(targetStart, Avx.Multiply(targetVelocity, time1.AsVector())), time1);
        }


        // we try to find a second position with a second guess
        // if that guess doesn't work we try the 3/4th of the way around
        if (time1 < fullTime * .5)
        {
            if (NewtonsMethod(function, derivative, fullTime - time1, 5, out var time2))
            {
                if (double.IsNaN(time2))
                {
                    throw new Exception($"time2 is NaN");
                }
                if (time2 >= fullTime * .5 && time2 <= fullTime)
                {
                    yield return (Avx.Add(targetStart, Avx.Multiply(targetVelocity, time2.AsVector())), time2);
                }
                else
                {
                    if (NewtonsMethod(function, derivative, fullTime * .75, 5, out var time3))
                    {
                        if (double.IsNaN(time3))
                        {
                            throw new Exception($"time3 is NaN");
                        }
                        if (time2 >= fullTime * .5 && time2 <= fullTime)
                        {
                            yield return (Avx.Add(targetStart, Avx.Multiply(targetVelocity, time3.AsVector())), time3);
                        }
                    }
                }
            }
        }
        else
        {
            if (NewtonsMethod(function, derivative, fullTime * .25, 5, out var time4))
            {
                if (double.IsNaN(time4))
                {
                    throw new Exception($"time4 is NaN");
                }
                if (time4 >= 0 && time4 < fullTime * .5)
                {
                    yield return (Avx.Add(targetStart, Avx.Multiply(targetVelocity, time4.AsVector())), time4);
                }
            }
        }
    }

    // TODO take into account the velocity of the shooter - might cause {89F9BCD3-7FDD-4AAB-A2A1-1543F31D7916}
    // velocit of the shooter
    private static Func<double, double> TargetDistanceMinusArcDistance(Vector128<double> shootingFrom, Vector128<double> targetStart, Vector128<double> targetVelocity, double arcRadius, double shotSpeed)
    {
        return (time) => Avx.Subtract(Avx.Add(targetStart, Avx.Multiply(targetVelocity, time.AsVector())), shootingFrom).Length() - (2 * arcRadius * Math.Sin(shotSpeed * time / (2*arcRadius)));
    }

    // TODO take into account the velocity of the shooter - might cause {89F9BCD3-7FDD-4AAB-A2A1-1543F31D7916}
    private static Func<double, double> TargetDistanceMinusArcDistanceDerivative(Vector128<double> shootingFrom, Vector128<double> targetStart, Vector128<double> targetVelocity, double arcRadius, double shotSpeed)
    {
        var x0 = Avx.Subtract(targetStart, shootingFrom).X();
        var y0 = Avx.Subtract(targetStart, shootingFrom).Y();
        var dx = targetVelocity.X();
        var dy = targetVelocity.Y();
        // ((x0 + dx*t)*(x0 + dx*t) + (y0 + dy*t)*(y0 + dy*t) )^ 1/2
        // ((a + b*x)*(a + b*x) + (c + d*x)*(c + d*x) )^ 1/2
        // google the derivative...
        // (b*(b*x + a) + d*(d*x + c))/ sqrt((a + b*x)*(a + b*x) + (c + d*x)*(c + d*x)); 
        // (dx*(dx*t + x0) + dy*(dy*t + y0))/ sqrt((x0 + dx*t)*(x0 + dx*t) + (y0 + dy*t)*(y0 + dy*t)); 
        return (time) => (((dx * ((dx * time) + x0)) + (dy * ((dy * time) + y0))) / Math.Sqrt(((x0 + (dx * time)) * (x0 + (dx * time))) + ((y0 + (dy * time)) * (y0 + (dy * time)))))
                         - (shotSpeed  * Math.Cos(shotSpeed * time / (2*arcRadius)));
    }

    private static bool NewtonsMethod(Func<double, double> function, Func<double, double> derivative, double guess, int rounds, out double res)
    {
        // D + Dv*t = 2*R*sin(shotSpeed *t / R);
        // D + Dv*t - 2*R*sin(shotSpeed *t / R) = 0;
        // derivative: Dv - 2*R*(shotSpeed/R)*cos(shotSpeed *t / R) 
        var at = guess;
        for (int round = 0; round < rounds; round++)
        {
            var currentValue = function(at);
            var currentDerivative = derivative(at);

            if (Math.Abs(currentValue) < .01)
            {
                res = at;
                return true;
            }

            if (currentDerivative != 0)
            {
                // currentValue + currentDerivative*x = 0
                var x = -currentValue / currentDerivative;
                at += x;
            }
            else
            {
                // idk, try something
                at += .1;
            }
        }
        res = -1;
        return false;
    }

    // TODO this does have multiple solutions are multople times

    // TODO take into account the velocity of the shooter - might cause {89F9BCD3-7FDD-4AAB-A2A1-1543F31D7916}
    public static bool TryTarget(Vector128<double> shootingFrom, Vector128<double> targetStart, Vector128<double> targetVelocity, double spotSpeed, out Vector128<double> shootAt, out double time)
    {
        // x1^2 + y1^2 = sportSpeed -- x1, y1 are the component of our shot
        var x0 = Avx.Subtract(targetStart, shootingFrom).X();
        var y0 = Avx.Subtract(targetStart, shootingFrom).Y();
        var x2 = targetVelocity.X();
        var y2 = targetVelocity.Y();
        // x1*t = x2*t + x0 
        // y1*t = y2*t + y0 
        // x1 = x2 + x0/t -- divide by t
        // y1 = y2 + y0/t -- divide by t
        // (x2 + x0/t)^2 + (y2 + y0/t)^2 = sportSpeed -- substitute
        // (x2^2 + y2^2) + (2*x2*x0 + 2*y2*y0) * (1/t) + (x0^2 +y0^2) * (1/t^2) = sportSpeed -- expand
        var a = (x0 * x0) + (y0 * y0);
        var b = (2 * x2 * x0) + (2 * y2 * y0);
        var c = (x2 * x2) + (y2 * y2) - (spotSpeed * spotSpeed);

        var underSqrt = (b * b) - (4 * a * c);

        if (underSqrt < 0)
        {
            shootAt = default;
            time = default;
            return false;
        }

        double t;
        var tInverse1 = (-b - Math.Sqrt(underSqrt)) / (2 * a);
        var tInverse2 = (-b + Math.Sqrt(underSqrt)) / (2 * a);

        if (tInverse1 > 0)
        {
            if (tInverse2 > 0)
            {
                t = Math.Min(1 / tInverse1, 1 / tInverse2);
            }
            else
            {
                t = 1 / tInverse1;
            }
        }
        else
        {
            if (tInverse2 > 0)
            {
                t = 1 / tInverse2;
            }
            else
            {
                shootAt = default;
                time = default;
                return false;
            }
        }

        // now solve for x1, y1
        var x1 = x2 + (x0 / t);
        var y1 = y2 + (y0 / t);

        shootAt = Avx.Add(shootingFrom, Vector128.Create(x1 * t, y1 * t));
        time = t;
        return true;
    }

    internal static BulletState[] Shoot(Vector128<double> from, Vector128<double> at, AntState shooter, int frame, ShotIndexTracker shotIndexTracker, double addScatter)
    {
        shooter.gun!.reloadDue = shooter.gun.immutable.relaodTimeTick;
        //TestSafeLog.WriteLine("shooter.Gun.reloadDue" + shooter.Gun.reloadDue);

        var res = new BulletState[shooter.gun.immutable.shots];

        for (int i = 0; i < res.Length; i++)
        {
            var direction = shooter.gun.GetShotDirection(from, at, shooter.immutable.id, frame, i, addScatter);

            res[i] = BulletState.MakeBullet(
            from,
            new BulletState.Immutable(
                shooter.immutable.id,
                frame,
                shotIndexTracker.Get(shooter.immutable.id),
                shooter.gun.GetRunFor(shooter.immutable.id, frame, i),
                shooter.gun.immutable.shotSpeed,
                direction,
                shooter.immutable.side,
                from,
                shooter.gun.immutable.damage,
                shooter.gun.GetIgnoreCoverFor(shooter.immutable.id, frame, i),
                shooter.gun.immutable.pierce,
                shooter.gun.immutable.stun,
                false,
                shooter.gun.immutable.knockBack,
                false));
        }
        return res;
    }

    internal GunState Copy()
    {
        return new GunState(reloadDue, immutable);
    }

    //public override bool Equals(object? obj)
    //{
    //    return obj is GunState state &&
    //           reloadDue == state.reloadDue &&
    //           EqualityComparer<Immutable>.Default.Equals(immutable, state.immutable);
    //}

    //public override int GetHashCode()
    //{
    //    return this.Hash();
    //    //return HashCode.Combine(reloadDue, favoriteTargetId, immutable);
    //}

    public IEnumerable<(string, object?)> HashData()
    {
        yield return (nameof(reloadDue), reloadDue);
        yield return (nameof(immutable), immutable);
    }
}
