﻿
/////////////////////////

public class UnpackerState: IStateHash
{
    public int unpackedAcount;// in ticks

    // immutable 
    public class Immutable: IStateHash
    {
        public readonly int buildTime; // in ticks
        public readonly AntType toAdd;
        public readonly Guid toAddId;

        public Immutable(int buildTime, AntType toAdd, Guid toAddId)
        {
            this.buildTime = buildTime;
            this.toAdd = toAdd;
            this.toAddId = toAddId;
        }

        //public override int GetHashCode()
        //{
        //    return this.Hash();
        //    //return buildTime + toAdd.GetHashCode() + toAddId.GetHashCode();
        //}

        public IEnumerable<(string, object?)> HashData()
        {
            yield return (nameof(buildTime), buildTime);
            yield return (nameof(toAdd), toAdd);
            yield return (nameof(toAddId), toAddId);
        }
    }

    public readonly Immutable immutable;

    public UnpackerState(int unpackedAmount, Immutable immutable)
    {
        this.unpackedAcount = unpackedAmount;
        this.immutable = immutable ?? throw new ArgumentNullException(nameof(immutable));
    }

    public void Update() {
        unpackedAcount++;
    }

    // returns the current ant if we haven't hatched yet
    // when we hatch it returns the new ant
    public AntState UnpackedToOrSelf(AntState self) {
        if (unpackedAcount > immutable.buildTime) {
            throw new Exception("what's up with that??");
        }
        if (unpackedAcount == immutable.buildTime) {
            var res = AntFactory.Make(immutable.toAddId, immutable.toAdd, self.immutable.side, self.position);
            return res;
        }
        return self;
    }

    internal UnpackerState Copy()
    {
        return new UnpackerState(unpackedAcount, immutable);
    }

    //public override int GetHashCode()
    //{
    //    return this.Hash();
    //    //return unpackedAcount + immutable.GetHashCode();
    //}

    public IEnumerable<(string, object?)> HashData()
    {
        yield return (nameof(unpackedAcount), unpackedAcount);
        yield return (nameof(immutable), immutable);
    }
}
