﻿using System.Runtime.Intrinsics;

public abstract class PhysicsSubjectState : IStateHash
{
    protected PhysicsSubjectState(GlobalPosionInt globalPositionInt)
    {
        GlobalPositionInt = globalPositionInt ?? throw new ArgumentNullException(nameof(globalPositionInt));
    }

    public GlobalPosionInt GlobalPositionInt { get; set; }
    public Vector128<double> GlobalPosition
    {
        get => GlobalPositionInt.ToGlobalVector();
    }

    public abstract IEnumerable<(string, object?)> HashData();
}


public abstract class PhysicsSubjectState<TImmutable> : PhysicsSubjectState, IMappable
    where TImmutable : PhysicsSubjectStateImmutable
{

    public readonly TImmutable immutable;

    public PhysicsSubjectState(TImmutable immutable, GlobalPosionInt globalPositionInt) : base(globalPositionInt)
    {
        this.immutable = immutable;
    }

    public (int x, int y)[] ReletivePositions() => immutable.mappable.ReletivePositions();
    public (int x, int y)[] ReletiveTopPositions() => immutable.mappable.ReletiveTopPositions();
    public (int x, int y)[] ReletiveBottomPositions() => immutable.mappable.ReletiveBottomPositions();
    public (int x, int y)[] ReletiveLeftPositions() => immutable.mappable.ReletiveLeftPositions();
    public (int x, int y)[] ReletiveRightPositions() => immutable.mappable.ReletiveRightPositions();
    public (int x, int y)[] ReletiveEdgePositions() => immutable.mappable.ReletiveEdgePositions();
    public abstract MapEntry ToMapEntry();
    //public (int x, int y)[] ReletiveBottomRightPositions() => mappable.ReletiveBottomRightPositions();
    //public (int x, int y)[] ReletiveTopLeftPositions() => mappable.ReletiveTopLeftPositions();
    //public (int x, int y)[] ReletiveTopRightPositions() => mappable.ReletiveTopRightPositions();
    //public (int x, int y)[] ReletiveBottomLeftPositions() => mappable.ReletiveBottomLeftPositions();
}