﻿using Prototypist.Toolbox.Dictionary;
using System.Linq;
using System.Runtime.Intrinsics;
using static RockState;

public class ShotIndexTracker 
{
    private readonly Dictionary<Guid, int> track = new Dictionary<Guid, int>();

    public int Get(Guid id) {
        if (!track.ContainsKey(id)) {
            track[id] = 0;
        }
        return ++track[id];
    }
}

public class CoverState: PhysicsSubjectStateInner2
{


    public CoverState(ImmunatblePhysicsStateInner immunatble, Vector128<double> position): base(immunatble, position)
    {
    }

    //public override int GetHashCode()
    //{
    //    return this.Hash();
    //}

    public override IEnumerable<(string, object?)> HashData()
    {
        yield return (nameof(physicsImmunatbleInner), physicsImmunatbleInner);
        yield return (nameof(position), position);
    }
}

public class RockState : PhysicsSubjectStateInner2, IBlocksShots
{
    public double Radius => physicsImmunatbleInner.radius;
    public Vector128<double> Position => position;
    //public double Radius => immutable.mappable.radius;

    //public class RockStateImmutable : PhysicsSubjectStateImmutable
    //{
    //    public RockStateImmutable(Mappable mappable) : base(mappable)
    //    {
    //    }

    //    public override IEnumerable<(string, object?)> HashData()
    //    {
    //        yield return (nameof(mappable), mappable);
    //    }
    //}

    public RockState(ImmunatblePhysicsStateInner immunatble, Vector128<double> position) : base(immunatble, position)
    {
    }

    //public override MapEntry ToMapEntry() => MapEntry.Make(this);

    //public override bool Equals(object? obj)
    //{
    //    return obj is RockState state &&
    //           position.Equals(state.position) &&
    //           physicsImmunatbleInner.Equals(state.physicsImmunatbleInner);
    //}

    //public override int GetHashCode()
    //{
    //    return this.Hash();
    //    //return HashCode.Combine(GlobalPositionInt, immutable, Radius);
    //}

    public override IEnumerable<(string, object?)> HashData()
    {
        yield return (nameof(physicsImmunatbleInner), physicsImmunatbleInner);
        yield return (nameof(position), position);
    }
}
