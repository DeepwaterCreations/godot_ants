﻿using System.Runtime.Intrinsics;

namespace Ants.Domain.State
{
    public class ResourceState : IStateHash
    {
        public const double decayRate = .05;

        public double available;
        public readonly Immutable immutable;

        public ResourceState(double available, Immutable immutable)
        {
            this.available = available;
            this.immutable = immutable ?? throw new ArgumentNullException(nameof(immutable));
        }

        // ya know guids hold a lot of info
        // I could probably make a guid out of some stuff
        // yeah... that's going to be fun to debug
        // when they collide because I wrote a sloppy something 
        
        public class Immutable : IStateHash {
            public readonly double gatherRate;
            public readonly Vector128<double> position;
            public readonly Id id;

            public class Id : IComparable<Id>, IStateHash{ 
                public readonly int createdOnFrame;
                public readonly Guid createdBy;
                private readonly bool onKill;

                public Id(int createdOnFrame, Guid createdBy, bool onKill)
                {
                    this.createdOnFrame = createdOnFrame;
                    this.createdBy = createdBy;
                    this.onKill = onKill;
                }

                public int CompareTo(Id? other)
                {
                    if (other == null ) return 1;

                    var created = other.createdOnFrame.CompareTo(createdOnFrame);

                    if (created != 0) return created;

                    var killed = other.onKill.CompareTo(onKill);

                    if (killed != 0) return created;

                    return other.createdBy.CompareTo(createdBy);
                }

                public IEnumerable<(string, object?)> HashData()
                {
                    yield return (nameof(createdOnFrame), createdOnFrame);
                    yield return (nameof(createdBy), createdBy);
                    yield return (nameof(onKill), onKill);
                }
            }

            public Immutable(double gatherRate, Vector128<double> position, Id id)
            {
                this.gatherRate = gatherRate;
                this.position = position;
                this.id = id ?? throw new ArgumentNullException(nameof(id));
            }

            public IEnumerable<(string, object?)> HashData()
            {
                yield return (nameof(gatherRate), gatherRate);
                yield return (nameof(position), position);
                yield return (nameof(id), id);
            }

            //public override int GetHashCode()
            //{
            //    return this.Hash();
            //}

            //public override bool Equals(object? obj)
            //{
            //    return obj is Immutable immutable &&
            //           gatherRate == immutable.gatherRate &&
            //           position.Equals(immutable.position);
            //}
        }

        public bool Update() {
            available = Math.Max(0, available - decayRate);
            return available == 0;
        }

        public IEnumerable<(string, object?)> HashData()
        {
            yield return (nameof(available), available);
            yield return (nameof(immutable), immutable);
        }

        //public override int GetHashCode()
        //{
        //    return this.Hash();
        //}

        //public override bool Equals(object? obj)
        //{
        //    return obj is ResourceState state &&
        //           available == state.available &&
        //           immutable.Equals(state.immutable);
        //}

        public ResourceState Copy() => new ResourceState(available, immutable);
    }
}