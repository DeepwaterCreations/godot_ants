﻿using Ants.Domain.Infrastructure;
using Ants.Domain.State;
using System.Runtime.Intrinsics;
using System.Runtime.Intrinsics.X86;

public class StationTrailState : IStateHash
{
    public void Update(SimulationState state, Vector128<double> currentPosition) {
        if (state.Stations.Any() && state.Stations.Where(x => Avx.Subtract(x.position, currentPosition).Length() < TrainPathing.StationDistance * .9).Any())
        {
            return;
        }
        state.Stations.Add(new StationState(currentPosition));
    }

    public IEnumerable<(string, object?)> HashData()
    {
        yield break;
    }

    internal StationTrailState Copy()
    {
        return this;
    }
}
