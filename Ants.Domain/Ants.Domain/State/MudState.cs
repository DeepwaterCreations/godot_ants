﻿using System.Runtime.Intrinsics;

namespace Ants.Domain.State
{
    public class MudState: PhysicsSubjectStateInner2, IStateHash
    {

        public MudState(double radius, Vector128<double> position): base (new ImmunatblePhysicsState(radius), position)
        {
        }

        //public override int GetHashCode()
        //{
        //    return this.Hash();
        //}

        public override IEnumerable<(string, object?)> HashData()
        {
            yield return (nameof(physicsImmunatbleInner), physicsImmunatbleInner);
            yield return (nameof(position), position);
        }
    }
}