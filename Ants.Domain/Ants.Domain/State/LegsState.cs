﻿public class LegsState: IStateHash
{
    public int stoppedFor; // in ticks

    // immutable
    /// <summary>
    /// in units per frame
    /// </summary>
    public readonly double Speed;

    public LegsState(double speed): this(0, speed)
    {
    }

    public LegsState(int stoppedFor, double speed)
    {
        this.stoppedFor = stoppedFor;
        Speed = speed;
    }

    // it would be nice to have acceleration as well as speed 

    public void Update()
    {
        stoppedFor++;
    }

    public bool IsMoving(int stoppedFor = 1)
    {
        return this.stoppedFor <= stoppedFor;
    }

    internal LegsState Copy()
    {
        return new LegsState(stoppedFor, Speed);
    }

    //public override bool Equals(object? obj)
    //{
    //    return obj is LegsState state &&
    //           stoppedFor == state.stoppedFor &&
    //           Speed == state.Speed;
    //}

    //public override int GetHashCode()
    //{
    //    return this.Hash();
    //    //return stoppedFor + (int)Speed;

    //    // this seems to make bad hashcodes, but the unit test passes
    //    // maybe it's mono thing
    //    //return HashCode.Combine(stoppedFor, Speed);
    //}

    public IEnumerable<(string, object?)> HashData()
    {
        yield return (nameof(stoppedFor), stoppedFor);
        yield return (nameof(Speed), Speed);
    }
}
