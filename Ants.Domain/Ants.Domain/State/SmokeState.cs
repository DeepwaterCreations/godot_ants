﻿using System.Runtime.Intrinsics;

namespace Ants.Domain.State
{
    public class SmokeState : IStateHash
    {
        public readonly Vector128<double> position;
        public readonly double radius;
        public readonly Guid id;

        public SmokeState(Vector128<double> position, Guid id)
        {
            this.position = position;
            this.radius = Pathing.unitSize_Large;
            this.id = id;
        }

        //public override bool Equals(object? obj)
        //{
        //    return obj is SmokeState state &&
        //           position.Equals(state.position) &&
        //           radius == state.radius &&
        //           id == state.id;
        //}

        //public override int GetHashCode()
        //{
        //    return this.Hash();
        //}

        public IEnumerable<(string, object?)> HashData()
        {
            yield return (nameof(position), position);
            yield return (nameof(radius), radius);
            yield return (nameof(id), id);
        }
    }
}