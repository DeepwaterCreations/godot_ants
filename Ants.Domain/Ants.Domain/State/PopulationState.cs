﻿namespace Ants.Domain.State
{
    public class PopulationState : IStateHash
    {
        public double allowedPopulation;
        public double[] pendingPopulation;

        public PopulationState(double allowedPopulation, double[] pendingPopulation)
        {
            this.allowedPopulation = allowedPopulation;
            this.pendingPopulation = pendingPopulation;
        }

        public IEnumerable<(string, object?)> HashData()
        {
            yield return (nameof(allowedPopulation), allowedPopulation);
        }

        internal PopulationState Copy()
        {
            return new PopulationState(allowedPopulation, pendingPopulation.ToArray());
        }

        internal void Update()
        {
            allowedPopulation += .0002;
        }
    }
}