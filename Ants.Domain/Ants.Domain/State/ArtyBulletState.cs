﻿using Ants.Domain;
using Ants.Domain.Infrastructure;
using Ants.Domain.State;
using System.Diagnostics;
using System.Runtime.Intrinsics;
using System.Runtime.Intrinsics.X86;

public class ArtyBulletState : PhysicsSubjectState2
{


    // immutable 
    public class Immutable : IComparable<ArtyBulletState.Immutable>, IStateHash
    {
        public class Id : IStateHash, IComparable<ArtyBulletState.Immutable.Id>
        {
            /// <summary>
            /// used to unique identify the shot
            /// we hold an id instead of a reference because references are constantly throw away and rebuilt
            /// </summary>
            public readonly Guid shooterId;
            /// <summary>
            /// used to unique identify the shot
            /// </summary>  
            public readonly int shotOnFrame;
            /// <summary>
            /// used to unique identify the shot
            /// 0 if this is the first shot the shooter shot on that frame
            /// 1 if this is the second and so on
            /// </summary>
            public readonly int shotIndex;


            public Id(Guid shooterId, int shotOnFrame, int shotIndex)
            {
                this.shooterId = shooterId;
                this.shotOnFrame = shotOnFrame;
                this.shotIndex = shotIndex;
            }

            public int CompareTo(Id? other)
            {
                if (other == null) return 1;

                if (shooterId.CompareTo(other.shooterId) != 0)
                {
                    return shooterId.CompareTo(other.shooterId);
                }
                if (shotOnFrame.CompareTo(other.shotOnFrame) != 0)
                {
                    return shotOnFrame.CompareTo(other.shotOnFrame);
                }

                if (shotIndex.CompareTo(other.shotIndex) != 0)
                {
                    return shotIndex.CompareTo(other.shotIndex);
                }
                return 0;
            }

            public override bool Equals(object? obj)
            {
                return obj is Id id &&
                       shooterId.Equals(id.shooterId) &&
                       shotOnFrame == id.shotOnFrame &&
                       shotIndex == id.shotIndex
                       ;
            }

            public override int GetHashCode()
            {
                return this.Hash();
                //return HashCode.Combine(shooterId, shotOnFrame, shotIndex);
            }

            public IEnumerable<(string, object?)> HashData()
            {
                yield return (nameof(shooterId), shooterId);
                yield return (nameof(shotOnFrame), shotOnFrame);
                yield return (nameof(shotIndex), shotIndex);
            }

            public override string ToString()
            {
                return $"({shooterId}, {shotOnFrame}, {shotIndex})";
            }
        }

        public readonly Id id;

        public readonly int runForTicks;
        public readonly double speed;
        public readonly Vector128<double> direction;
        public readonly int side;

        public readonly double subShot_runForDistance;
        public readonly double subShot_damage;
        public readonly double subShot_speed;
        public readonly double pierce = 10;
        public readonly double ignoreCoverFor;

        public readonly int stun;// ticks
        public readonly double knockBack;

        public readonly bool damagesTeam;

        public Immutable(Guid shooterId, int frame, int shooterShotOnFrame,
            int runForTicks, double speed, Vector128<double> direction, int side, double startingDamge,
            double ignoreCoverFor, double pierce, int stun, bool damagesTeam,
            double subShot_runForDistance, double subShot_speed, double knockBack)
        {
            this.id = new Id(shooterId, frame, shooterShotOnFrame);
            this.runForTicks = runForTicks;
            this.speed = speed;
            this.direction = direction;
            this.side = side;
            this.subShot_damage = startingDamge;
            this.pierce = pierce;
            this.ignoreCoverFor = ignoreCoverFor;
            this.stun = stun;
            this.damagesTeam = damagesTeam;
            this.subShot_runForDistance = subShot_runForDistance;
            this.subShot_speed = subShot_speed;            this.knockBack = knockBack;

        }

        public int CompareTo(Immutable? other)
        {
            if (other == null) return 1;

            return id.CompareTo(other.id);
        }

        //public override bool Equals(object? obj)
        //{
        //    return obj is Immutable immutable &&
        //           id.Equals(immutable.id) &&
        //           runForTicks == immutable.runForTicks &&
        //           speed == immutable.speed &&
        //           direction.Equals(immutable.direction) &&
        //           side == immutable.side &&
        //           subShot_damage == immutable.subShot_damage &&
        //           endingPierce == immutable.endingPierce &&
        //           pierceAt200 == immutable.pierceAt200 &&
        //           ignoreCoverFor == immutable.ignoreCoverFor &&
        //           stun == immutable.stun &&
        //           damagesTeam == immutable.damagesTeam;
        //}

        //public override int GetHashCode()
        //{
        //    return this.Hash();
        //    //return HashCode.Combine(id, runFor, speed, direction, side, startedAt, startingDamge, startingPierce);
        //}

        public IEnumerable<(string, object?)> HashData()
        {
            yield return (nameof(id), id);
            yield return (nameof(runForTicks), runForTicks);
            yield return (nameof(speed), speed);
            yield return (nameof(direction), direction);
            yield return (nameof(side), side);
            yield return (nameof(subShot_damage), subShot_damage);
            yield return (nameof(pierce), pierce);
            yield return (nameof(ignoreCoverFor), ignoreCoverFor);
            yield return (nameof(stun), stun);
            yield return (nameof(damagesTeam), damagesTeam);
            yield return (nameof(subShot_runForDistance), subShot_runForDistance);
            yield return (nameof(subShot_speed), subShot_speed);
            yield return (nameof(knockBack), knockBack);
        }
    }
    public readonly Immutable immutable;
    public int createSubBullestIn;
    public const int createSubBulletsEvery = FramesPerSecond.framesPerSecond / 4;
    public readonly HashSet<Vector128<double>> coverHit;

    private ArtyBulletState(Vector128<double> position, Immutable immutable, ImmunatblePhysicsState physicsImmunatble, Vector128<double> velocity, int createSubBullestIn, HashSet<Vector128<double>> coverHit) : base(physicsImmunatble, position, velocity)
    {
        this.immutable = immutable;
        this.createSubBullestIn = createSubBullestIn;
        this.coverHit = coverHit;
    }

    public static ArtyBulletState MakeBullet(Vector128<double> position, Immutable immutable)
    {

        return new ArtyBulletState(
            position,
            immutable,
            new ImmunatblePhysicsState(1),// TODO is this always "1"? To really support, we'll have to update who to shoot to support avoid by
            Avx.Multiply(immutable.direction, immutable.speed.AsVector()),
            ArtyBulletState.createSubBulletsEvery,
            new HashSet<Vector128<double>>()
            );
    }


    public double GetDistanceTravelled(int tick) {
        return (tick - immutable.id.shotOnFrame) * immutable.speed;
    }
   
    //return true if killed
    public bool Update(int tick, SimulationState simulationState, ShotIndexTracker shotIndexTracker)
    {
        createSubBullestIn--;
        if (createSubBullestIn == 0) {
            var bullet1 = BulletState.MakeBullet(
                position,
                new BulletState.Immutable(
                    immutable.id.shooterId,
                    tick,
                     shotIndexTracker.Get(immutable.id.shooterId),
                    immutable.subShot_runForDistance, // TODO randomize?
                    immutable.subShot_speed,
                    velocity.NormalizedCopy().Rotated(Math.Tau/ 20.0),
                    immutable.side,
                    position,
                    immutable.subShot_damage,
                    immutable.ignoreCoverFor, // TODO randomize?
                    immutable.pierce,
                    immutable.stun,
                    true,
                    immutable.knockBack,
                    false));
            var added1 = simulationState.bullets.TryAdd(bullet1.immutable.id, bullet1);
            Debug.Assert(added1);
            var bullet2 = BulletState.MakeBullet(
                position,
                new BulletState.Immutable(
                    immutable.id.shooterId,
                    tick,
                     shotIndexTracker.Get(immutable.id.shooterId),
                    immutable.subShot_runForDistance, // TODO randomize?
                    immutable.subShot_speed,
                    velocity.NormalizedCopy().Rotated(-Math.Tau / 20.0),
                    immutable.side,
                    position,
                    immutable.subShot_damage,
                    immutable.ignoreCoverFor, // TODO randomize?
                    immutable.pierce,
                    immutable.stun,
                    true,
                    immutable.knockBack,
                    false));
            var added2 = simulationState.bullets.TryAdd(bullet2.immutable.id, bullet2);
            Debug.Assert(added2);
            createSubBullestIn = createSubBulletsEvery;
        }

        return tick - immutable.id.shotOnFrame > immutable.runForTicks;
    }

    internal ArtyBulletState Copy()
    {
        return new ArtyBulletState(position, immutable, physicsImmunatble, velocity, createSubBullestIn, coverHit.ToHashSet());
    }

    //public override bool Equals(object? obj)
    //{
    //    return obj is ArtyBulletState state &&
    //           createSubBullestIn == state.createSubBullestIn &&
    //           position.Equals(state.position) &&
    //           velocity.Equals(state.velocity) &&
    //           EqualityComparer<Immutable>.Default.Equals(immutable, state.immutable);
    //}

    //public override int GetHashCode()
    //{
    //    return this.Hash();
    //}

    public override IEnumerable<(string, object?)> HashData()
    {
        foreach (var item in base.HashData())
        {
            yield return item;
        }

        yield return (nameof(immutable), immutable);
        yield return (nameof(createSubBullestIn), createSubBullestIn);
        yield return (nameof(coverHit), coverHit);
    }
}