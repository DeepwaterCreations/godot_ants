﻿using System.Runtime.Intrinsics;

public class GrenadeOrder : IOrderState
{
    internal Vector128<double> shootAt;

    public GrenadeOrder(Vector128<double> shootAt)
    {
        this.shootAt = shootAt;
    }

    public void Cancel(AntState actor)
    {
    }

    public IOrderState CopyIfMutable()
    {
        return this;
    }

    public override bool Equals(object? obj)
    {
        return obj is GrenadeOrder order &&
               shootAt.Equals(order.shootAt);
    }

    public override int GetHashCode()
    {
        return this.Hash();
        //return HashCode.Combine(shootAt);
    }

    public IEnumerable<(string, object?)> HashData()
    {
        yield return (nameof(shootAt), shootAt);
    }
}