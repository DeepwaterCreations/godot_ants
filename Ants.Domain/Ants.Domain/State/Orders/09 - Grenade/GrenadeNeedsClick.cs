﻿using Ants.Domain.State;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Intrinsics;

public class GrenadeNeedsClick : IPendingClickOrderState
{

    public override int GetHashCode()
    {
        //return nameof(GrenadeNeedsClick).GetHashCode();
        return Guid.Parse("{0DDD049F-5526-4387-A609-C60263431909}").GetHashCode();
    }

    public override bool Equals(object? obj)
    {
        return obj is GrenadeNeedsClick;
    }

    public bool TryOrder(Vector128<double> mousePosition, SimulationState state, AntState ant, ITrainPathing game, ImmutableSimulationState immutableSimulationState, AntState[] alsoOrdered, [MaybeNullWhen(false)] out IPendingOrderState order)
    {
        order = new GrenadePendingOrder(mousePosition);
        return true;
    }
}