﻿using Ants.Domain.Infrastructure;
using Ants.Domain.State;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Intrinsics;

public class MovePendingImmutableState : IPendingOrderState
{
    public readonly Vector128<double>[] path;
    private readonly double withIn;

    public MovePendingImmutableState(Vector128<double>[] path, double withIn)
    {
        this.path = path;
        this.withIn = withIn;
    }

    //public IOrderState Order() => new MoveOrderState(path);

    public bool TryOrder(SimulationState state, AntState ant, [MaybeNullWhen(false)] out IOrderState order)
    {
        //var avoidables = state.rocks.Select(x => (x.GlobalPosition, x.Radius)).ToArray();
        //if (Pathing.GetPath3(state.paths.knownPaths[(ant.immutable.mudMode, ant.immutable.mappable.radius)], ant.GlobalPosition, path, out var res))
        //{
        //    if (!res.Any()) {
        //        order = default;
        //        return false;
        //    }

        //    order = new MoveOrderState(res);
        //    return true;
        //}

        order = new MoveOrderState(path, withIn);
        return true;
    }

    public byte[] Serialize()
    { 
        var list = new List<byte>();
        list.Add(typeByte);
        list.AddRange(BitConverter.GetBytes(path.Length));
        foreach (var point in path)
        {
            list.AddRange(BitConverter.GetBytes(point.X()));
            list.AddRange(BitConverter.GetBytes(point.Y()));
        }
        list.AddRange(BitConverter.GetBytes(withIn));
        return list.ToArray();
    }

    public static MovePendingImmutableState Deserialize(byte[] bytes)
    {
        var at = 1;// skip type byte
        var length = BitConverter.ToInt32(bytes, at);
        at += 4;
        var array = new Vector128<double>[length];
        for (int i = 0; i < length; i++)
        {
            var x = BitConverter.ToDouble(bytes, at);
            at += 8;
            var y = BitConverter.ToDouble(bytes, at);
            at += 8;
            array[i] = Vector128.Create(x, y);
        }
        var withIn = BitConverter.ToDouble(bytes, at);
        at += 8;
        return new MovePendingImmutableState(array, withIn);
    }

    public override bool Equals(object? obj)
    {
        return obj is MovePendingImmutableState state &&
               path.SequenceEqual(state.path);
    }

    public override int GetHashCode()
    {
        return this.Hash();
        //return HashCode.Combine(destination, nameof(MovePendingImmutableState));
    }

    public IEnumerable<(string, object?)> HashData()
    {
        yield return (nameof(path), path);
    }

    public const byte typeByte = 5;
}
