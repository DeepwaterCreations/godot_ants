﻿using Ants.Domain.Infrastructure;
using Ants.Domain.State;
using Prototypist.Toolbox.Object;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Runtime.Intrinsics;
using System.Runtime.Intrinsics.X86;

public interface IMoveOrder {
    public Vector128<double> GetDestination();
}

public class MoveOrderState : IOrderState, IMoveOrder
{
    public IReadOnlyList<Vector128<double>> destinations;
    private readonly double withIn;

    public MoveOrderState(IReadOnlyList<Vector128<double>> destinations, double withIn)
    {
        if (!destinations.Any()) {
            throw new Exception("destinations shouldn't be empty!");
        }
        this.destinations = destinations ?? throw new ArgumentNullException(nameof(destinations));
        this.withIn = withIn;
    }

    public bool Act(AntState actor, Map2<MudState> mudMap, IEnumerable<(Vector128<double> globalPosition, double radius)> avoidables, Map2<AntState> antsMap)
    {
        if (actor.legs != null)
        {
            var pressure = 0;
            
            while (Avx.Subtract(destinations.First(), actor.position).Length() < actor.physicsImmunatbleInner.radius / 10.0
                // this might be a slighly expensive 
                // TODO - maybe I should time it?
                || (destinations.Count > 1 && Pathing.CanReach(actor.position, destinations.ElementAt(1), avoidables, actor.physicsImmunatbleInner.radius, out var _, out var _))
                )
            {
                destinations = destinations.Skip(1).ToArray();
                if (!destinations.Any())
                {
                    //sprite.Playing = false;
                    return true;
                }

                pressure++;
                if (pressure > 1000) {
                    Log.WriteLine("MoveOrderState.Act high pressure");
                    Debugger.Launch();
                }
            }

            foreach (var antHit in antsMap.Hit(actor.position, actor.Radius))
            {
                if (antHit.immutable.side == actor.immutable.side && antHit.immutable.id != actor.immutable.id) {
                    if (antHit.activeOrder is IMoveOrder moveOrder) {
                        if (Avx.Subtract(actor.position, GetDestination()).LengthSquared() <= withIn * withIn)
                        {
                            return true;
                        }
                    }
                }
            }


            var concreteDestination = destinations.First();
            Vector128<double> distance = Avx.Subtract(concreteDestination, actor.position);
            //var normalizedDirection = distance.Normalized();

            actor.RequestVelocity(distance, mudMap);

            //sprite.Rotation = (double)(Math.Atan2(normalizedDirection.y, normalizedDirection.x) + Math.PI / 2.0);
            //sprite.Playing = true;
            //if (sprite.Material is ShaderMaterial shaderMaterial)
            //{
            //    shaderMaterial.SetShaderParameter("plusX", MathF.Cos(-sprite.Rotation));
            //    shaderMaterial.SetShaderParameter("plusY", MathF.Sin(-sprite.Rotation));//
            //}

            return false;
        }
        return true;
    }

    public void Cancel(AntState actor)
    {
    }

    public IOrderState CopyIfMutable()
    {
        return CopyTyped();
    }

    public MoveOrderState CopyTyped()
    {
        return new MoveOrderState(destinations.ToArray(), withIn);
    }

    public override bool Equals(object? obj)
    {
        return obj is MoveOrderState state &&
               destinations.SequenceEqual(state.destinations);
    }

    public Vector128<double> GetDestination() => destinations.Last();

    public override int GetHashCode()
    {
        return this.Hash();
    }

    public IEnumerable<(string, object?)> HashData()
    {
        yield return (nameof(destinations), destinations);
    }
}
