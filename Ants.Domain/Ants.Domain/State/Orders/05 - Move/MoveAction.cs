﻿using Ants.Domain.State;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Runtime.Intrinsics;

public class MoveAction : IPendingClickOrderState, IStateHash
{
    public bool TryOrder(Vector128<double> mousePosition, SimulationState state, AntState ant, ITrainPathing game,ImmutableSimulationState immutableSimulationState, AntState[] alsoOrdered, [MaybeNullWhen(false)] out IPendingOrderState order)
    {
        var avoidables = immutableSimulationState.rocks.Select(x => (x.position, x.physicsImmunatbleInner.radius)).ToArray();
        if (Pathing.GetPath3(immutableSimulationState.paths.knownPaths[(ant.immutable.mudMode, ant.physicsImmunatbleInner.radius)], ant.position, mousePosition, out var res))
        {
            if (!res.Any())
            {
                order = default;
                return false;
            }

            double within = WithIn(ant, alsoOrdered);

            order = new MovePendingImmutableState(res.ToArray(), within);
            return true;
        }
        order = default;
        return false;


    }

    public static double WithIn(AntState ant, AntState[] alsoOrdered)
    {
        return alsoOrdered.Length == 1 ? ant.physicsImmunatble.radius * 1.12  // 1.118 is the sqrt(5/4) which is the radius we need if three ants are stuck in a triangle with the destination at the middle, we round up a little
                                       : Math.Sqrt(alsoOrdered.Sum(x => x.physicsImmunatble.radius * x.physicsImmunatble.radius));
    }

    public override int GetHashCode()
    {
        return this.Hash();
        //return 1596694235;
        //return nameof(MoveAction).GetHashCode();
    }

    public override bool Equals(object? obj)
    {
        return obj is MoveAction;
    }

    public IEnumerable<(string, object?)> HashData()
    {
        yield break;
    }

    public override string ToString() => $"move";
}


public class TrainMoveAction : IPendingClickOrderState, IStateHash
{
    public bool TryOrder(Vector128<double> mousePosition, SimulationState state, AntState ant, ITrainPathing game, ImmutableSimulationState immutableSimulationState, AntState[] alsoOrdered, [MaybeNullWhen(false)] out IPendingOrderState order)
    {
        var avoidables = immutableSimulationState.rocks.Select(x => (x.position, x.physicsImmunatbleInner.radius)).ToArray();
        if (TrainPathing.GetPath(game.TrainPathingHolder(ant.immutable.mudMode, ant.physicsImmunatbleInner.radius), ant.position, mousePosition, out var res))
        {
            if (!res.Any())
            {
                order = default;
                return false;
            }

            var withIn = MoveAction.WithIn(ant, alsoOrdered);

            // I guess it's ok to return a normal move, since we already calculated the paths
            order = new MovePendingImmutableState(res.ToArray(), withIn);
            return true;
        }
        order = default;
        return false;


    }

    public override int GetHashCode()
    {
        return this.Hash();
        //return 1596694235;
        //return nameof(MoveAction).GetHashCode();
    }

    public override bool Equals(object? obj)
    {
        return obj is TrainMoveAction;
    }

    public IEnumerable<(string, object?)> HashData()
    {
        yield break;
    }

    public override string ToString() => $"train move";
}
