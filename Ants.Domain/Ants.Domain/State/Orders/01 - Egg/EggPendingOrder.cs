﻿using Ants.Domain.Infrastructure;
using Ants.Domain.State;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Intrinsics;

public class EggPendingOrder : IPendingOrderState
{
    //private readonly int cost;
    //private readonly double population;
    private readonly AntType unpackInto;
    private readonly Guid id;
    //private readonly Guid unpackerId;
    private readonly Vector128<double> moveTo;
    private readonly Vector128<double> placeTowards;

    public EggPendingOrder(/*int cost,*/ AntType unpackInto, Guid id, /*Guid unpackerId,*/ /*double population,*/ Vector128<double> moveTo, Vector128<double> placeTowards)
    {
        this.unpackInto = unpackInto;
        //this.cost = cost;
        this.id = id;
        //this.unpackerId = unpackerId;
        //this.population = population;
        this.moveTo = moveTo;
        this.placeTowards = placeTowards;
    }

    public const byte typeByte = 1;

    public byte[] Serialize()
    {
        var list = new List<byte>();

        list.Add(typeByte);
        //list.AddRange(BitConverter.GetBytes(cost));
        list.AddRange(BitConverter.GetBytes((int)unpackInto));
        list.AddRange(id.ToByteArray());
        //list.AddRange(unpackerId.ToByteArray());
        //list.AddRange(BitConverter.GetBytes(population));
        list.AddRange(BitConverter.GetBytes(moveTo.X()));
        list.AddRange(BitConverter.GetBytes(moveTo.Y()));
        list.AddRange(BitConverter.GetBytes(placeTowards.X()));
        list.AddRange(BitConverter.GetBytes(placeTowards.Y()));


        return list.ToArray();
    }

    public static EggPendingOrder Deserialize(byte[] bytes) {

        var at = 1; // skip the type byte
        var unpackInto = (AntType)BitConverter.ToInt32(bytes, at);
        at += 4;

        var id = new Guid(bytes.AsSpan(at, 16));
        at += 16;
        //var unpackerId = new Guid(bytes.AsSpan(at, 16));
        //at += 16;

        var moveToX = BitConverter.ToDouble(bytes, at);
        at += 8;
        var moveToY = BitConverter.ToDouble(bytes, at);
        at += 8;

        var placeTowardsX = BitConverter.ToDouble(bytes, at);
        at += 8;
        var placeTowardsY = BitConverter.ToDouble(bytes, at);
        at += 8;

        return new EggPendingOrder(
            //cost: BitConverter.ToInt32(bytes, 1),
            unpackInto,
            id,
            //unpackerId,
            //population: BitConverter.ToDouble(bytes, 41),
            moveTo: Vector128.Create(moveToX, moveToY),
            placeTowards: Vector128.Create(placeTowardsX, placeTowardsY)
            );
    }

    public bool TryOrder(SimulationState state, AntState ant, [MaybeNullWhen(false)] out IOrderState order)
    {
        // maybe I need a combined tracker for all the resources
        // and a combined API to try them all
        if (state.money.GetMainMoney(ant.immutable.side) >=  AntFactory.Cost(unpackInto)  &&
            state.populationState.allowedPopulation >= state.CompletAndPendingPopulation(ant.immutable.side) + AntFactory.Population(unpackInto) &&
            state.money.GetGathererMoney(ant.immutable.side) >= AntFactory.GathererCost(unpackInto)
            ) {
            state.money.AssertSpendMainMoney(AntFactory.Cost(unpackInto), ant.immutable.side);
            state.populationState.pendingPopulation[ant.immutable.side] += AntFactory.Population(unpackInto);
            state.money.AssertSpendGathererMoney(AntFactory.GathererCost(unpackInto), ant.immutable.side);
            order = new MakeUnitOrderImmutableState(id, unpackInto,  /*unpackerId,*/ moveTo, placeTowards);
            return true;
        }

        Log.WriteLine("EggKeyAction no money");
        order = default;
        return false;
    }

    public override bool Equals(object? obj)
    {
        return obj is EggPendingOrder order &&
               //cost == order.cost &&
               unpackInto == order.unpackInto &&
               id.Equals(order.id) &&
               //unpackerId.Equals(order.unpackerId) &&
               moveTo.Equals(order.moveTo) &&
               placeTowards.Equals(order.placeTowards);
    }

    public override int GetHashCode()
    {
        return this.Hash();
        //return HashCode.Combine(cost, unpackInto, id, buildTime, unpackerId);
    }

    public IEnumerable<(string, object?)> HashData()
    {
        //yield return (nameof(cost), cost);
        yield return (nameof(unpackInto), unpackInto);
        yield return (nameof(id), id);
        //yield return (nameof(unpackerId), unpackerId);
        yield return (nameof(moveTo), moveTo);
        yield return (nameof(placeTowards), placeTowards);
    }
}
