﻿using Ants.Domain.State;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Intrinsics;
using System.Runtime.Intrinsics.X86;

public class EggKeyAction: IPendingClickOrderState
{

    public readonly AntType unpackInto;

    public EggKeyAction(AntType unpackInto)
    {
        this.unpackInto = unpackInto;
    }

    public bool TryOrder(Vector128<double> mousePosition, SimulationState state, AntState ant, ITrainPathing game, ImmutableSimulationState immutableState, AntState[] alsoOrdered, [MaybeNullWhen(false)] out IPendingOrderState order)
    {
        var avoidables = immutableState.rocks.Select(x => (x.position, x.physicsImmunatbleInner.radius)).ToArray();
        if (Pathing.GetPath3(immutableState.paths.knownPaths[(ant.immutable.mudMode, ant.physicsImmunatbleInner.radius)], ant.position, mousePosition, out var res))
        {
            order = new EggPendingOrder(unpackInto, Guid.NewGuid(), /*Guid.NewGuid(),*/  mousePosition, res.Any()? res.First(): Avx.Add(ant.position, Vector128.Create(1000.0,0.0)));
            return true;
        }
        order = default;
        return false;
    }

    public override int GetHashCode()
    {
        return unpackInto.GetHashCode();
    }

    public override bool Equals(object? obj)
    {
        return obj is EggKeyAction action &&
               unpackInto == action.unpackInto;
    }

    public override string? ToString() => $"place egg {unpackInto}, cost: ${AntFactory.Cost(unpackInto)}, gather cost: ${AntFactory.GathererCost(unpackInto)}, population: ${AntFactory.Population(unpackInto)}";
}
