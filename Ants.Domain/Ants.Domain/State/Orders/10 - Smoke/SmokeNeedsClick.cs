﻿using Ants.Domain.State;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Intrinsics;

public class SmokeNeedsClick : IPendingClickOrderState
{

    public override int GetHashCode()
    {
        //return nameof(SmokeNeedsClick).GetHashCode();
        return Guid.Parse("{8CBEDEB0-55A5-4251-BE2E-FD90424365E1}").GetHashCode();
    }

    public override bool Equals(object? obj)
    {
        return obj is SmokeNeedsClick;
    }

    public bool TryOrder(Vector128<double> mousePosition, SimulationState state, AntState ant, ITrainPathing game, ImmutableSimulationState immutableSimulationState, AntState[] alsoOrdered, [MaybeNullWhen(false)] out IPendingOrderState order)
    {
        order = new SmokeOrder(mousePosition, Guid.NewGuid());
        return true;
    }
}
