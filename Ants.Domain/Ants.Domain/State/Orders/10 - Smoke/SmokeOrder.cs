﻿using Ants.Domain.Infrastructure;
using Ants.Domain.State;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Intrinsics;

public class SmokeOrder : IPendingOrderState, IOrderState
{
    public readonly Vector128<double> mousePosition;
    public readonly Guid smokeId;

    public SmokeOrder(Vector128<double> mousePosition, Guid smokeId)
    {
        this.mousePosition = mousePosition;
        this.smokeId = smokeId;
    }

    public bool TryOrder(SimulationState state, AntState ant, [MaybeNullWhen(false)] out IOrderState order)
    {
        order = this;
        return true;
    }

    public void Cancel(AntState actor)
    {
    }

    public IOrderState CopyIfMutable()
    {
        return this;
    }

    public byte[] Serialize()
    {
        var list = new List<byte>
        {
            typeByte
        };
        list.AddRange(BitConverter.GetBytes(mousePosition.X()));
        list.AddRange(BitConverter.GetBytes(mousePosition.Y()));
        list.AddRange(smokeId.ToByteArray());

        return list.ToArray();
    }

    public static SmokeOrder Deserialize(byte[] bytes)
    {
        return new SmokeOrder(
            Vector128.Create(
                    BitConverter.ToDouble(bytes, 1),
                    BitConverter.ToDouble(bytes, 9)
                ),
            new Guid(bytes.AsSpan(17)));
    }

    public override bool Equals(object? obj)
    {
        return obj is SmokeOrder order &&
               mousePosition.Equals(order.mousePosition);
    }

    public override int GetHashCode()
    {
        return this.Hash();
        //return HashCode.Combine(mousePosition);
    }

    public IEnumerable<(string, object?)> HashData()
    {
        yield return (nameof(mousePosition), mousePosition);
        yield return (nameof(smokeId), smokeId);
    }

    public static byte typeByte = 10;
}

