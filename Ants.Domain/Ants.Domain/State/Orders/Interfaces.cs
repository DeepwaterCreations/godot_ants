﻿using Ants.Domain;
using Ants.Domain.State;
using Prototypist.Toolbox;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Runtime.Intrinsics;
using System.Text;
using System.Threading.Tasks;
using static Pathing;
using static WhoToShoot;

/// <summary>
/// needs to implement GetHashCode
/// it is used in the state hash
/// </summary>

public interface IClickAction : IStateHash
{
    IPendingOrderState Order(Vector128<double> mousePosition);
}


/// <summary>
/// needs to implement GetHashCode
/// it is used in the state hash
/// </summary>
public interface IKeyAction
{
    IPendingOrderState GetPendingOrder();
}

/// <summary>
/// needs to implement GetHashCode
/// it is used in the state hash
/// </summary>
public interface IKeyClickAction
{
    IPendingClickOrderState GetPendingClickOrder();
}

public interface IOrderState : IStateHash
{
    IOrderState CopyIfMutable();
    void Cancel(AntState actor);
}

public interface ITrainPathing
{
    TrainPaths TrainPathingHolder(Mode mode, double radius);
}

public interface IPendingClickOrderState
{
    bool TryOrder(Vector128<double> mousePosition, SimulationState state, AntState ant, ITrainPathing game, ImmutableSimulationState immutableState, AntState[] alsoOrdered, [MaybeNullWhen(false)] out IPendingOrderState order);
}

public interface IPendingOrderState : IStateHash
{
    bool TryOrder(SimulationState state, AntState ant, [MaybeNullWhen(false)] out IOrderState order);

    //IOrderState Order();

    public byte[] Serialize();
}




public abstract class AntAction : OrType<IPendingClickOrderState, IKeyAction> //, MenuAction
{
    public readonly string description;

    // inner so you don't make an InnerAntAction<int>
    private class InnerAntAction<T> : AntAction, IIsDefinately<T>
    {
        public InnerAntAction(T value, string description) :base(description)
        {
            this.Value = value ?? throw new ArgumentNullException(nameof(value));
        }

        [NotNull]
        public T Value { get; }

        public override object Representative() => Value;
    }

    private AntAction(string description) {
        this.description = description;
    }

    public static AntAction Make(IPendingClickOrderState clickAction, string description)
    {
        return new InnerAntAction<IPendingClickOrderState>(clickAction, description);
    }

    public static AntAction Make(IKeyAction keyAction, string description)
    {
        return new InnerAntAction<IKeyAction>(keyAction, description);
    }

    //public static AntAction Make(MenuAction menuAction, string description)
    //{
    //    return new InnerAntAction<MenuAction>(menuAction, description);
    //}
}

//public class MenuAction 
//{
//    public readonly Dictionary<Ants.Domain.Key, AntAction> backing;

//    public MenuAction(Dictionary<Key, AntAction> backing)
//    {
//        this.backing = backing ?? throw new ArgumentNullException(nameof(backing));
//    }
//}
