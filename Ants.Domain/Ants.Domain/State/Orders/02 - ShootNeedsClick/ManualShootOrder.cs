﻿using System.Runtime.Intrinsics;

public class ManualShootOrder : IOrderState
{
    internal Vector128<double> shootAt;

    public ManualShootOrder(Vector128<double> shootAt)
    {
        this.shootAt = shootAt;
    }

    public void Cancel(AntState actor)
    {
    }

    public IOrderState CopyIfMutable()
    {
        return this;
    }

    public override bool Equals(object? obj)
    {
        return obj is ManualShootOrder order &&
               shootAt.Equals(order.shootAt);
    }

    public override int GetHashCode()
    {
        return this.Hash();
        //return HashCode.Combine(shootAt);
    }

    public IEnumerable<(string, object?)> HashData()
    {
        yield return (nameof(shootAt), shootAt);
    }
}
