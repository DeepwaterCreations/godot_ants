﻿using Ants.Domain.State;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Intrinsics;

public class ShootNeedsClick : IPendingClickOrderState
{

    public override int GetHashCode()
    {
        return nameof(ShootNeedsClick).GetHashCode();
    }

    public override bool Equals(object? obj)
    {
        return obj is ShootNeedsClick;
    }

    public bool TryOrder(Vector128<double> mousePosition, SimulationState state, AntState ant, ITrainPathing game,ImmutableSimulationState immutableSimulationState, AntState[] alsoOrdered,  [MaybeNullWhen(false)] out IPendingOrderState order)
    {
        order = new ShootPendingOrder(mousePosition);
        return true;
    }
}
