﻿using Ants.Domain.Infrastructure;
using Ants.Domain.State;
using Microsoft.VisualBasic;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Intrinsics;

public class ShootPendingOrder : IPendingOrderState
{
    private Vector128<double> mousePosition;

    public ShootPendingOrder(Vector128<double> mousePosition)
    {
        this.mousePosition = mousePosition;
    }

    public bool TryOrder(SimulationState state, AntState ant, [MaybeNullWhen(false)] out IOrderState order)
    {
        order =  new ManualShootOrder(mousePosition);
        return true;
    }

    public byte[] Serialize()
    {
        var list = new List<byte>();

        list.Add(typeByte);
        list.AddRange(BitConverter.GetBytes(mousePosition.X()));
        list.AddRange(BitConverter.GetBytes(mousePosition.Y()));

        return list.ToArray();
    }

    public static ShootPendingOrder Deserialize(byte[] bytes) {
        return new ShootPendingOrder(
            Vector128.Create(
                    BitConverter.ToDouble(bytes, 1),
                    BitConverter.ToDouble(bytes, 9)
                ));
    }

    public override bool Equals(object? obj)
    {
        return obj is ShootPendingOrder order &&
               mousePosition.Equals(order.mousePosition);
    }

    public override int GetHashCode()
    {
        return this.Hash();
        //return HashCode.Combine(mousePosition);
    }

    public IEnumerable<(string, object?)> HashData()
    {
        yield return (nameof(mousePosition), mousePosition);
    }

    public const byte typeByte = 2;
}