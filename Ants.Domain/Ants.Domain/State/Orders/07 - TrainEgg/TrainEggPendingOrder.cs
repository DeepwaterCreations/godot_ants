﻿using Ants.Domain.State;
using System.Diagnostics.CodeAnalysis;

public class TrainEggPendingOrder : IPendingOrderState
{
    //private readonly int cost;
    private readonly AntType unpackInto;
    private readonly Guid id;
    private readonly int buildTime;
    private readonly Guid unpackerId;

    public TrainEggPendingOrder(/*int cost,*/ AntType unpackInto, Guid id, int buildTime, Guid unpackerId)
    {
        this.unpackInto = unpackInto;
        //this.cost = cost;
        this.id = id;
        this.buildTime = buildTime;
        this.unpackerId = unpackerId;
    }

    public static byte typeByte = 7;

    public byte[] Serialize()
    {
        var list = new List<byte>();

        list.Add(typeByte);
        //list.AddRange(BitConverter.GetBytes(cost));
        list.AddRange(BitConverter.GetBytes((int)unpackInto));
        list.AddRange(id.ToByteArray());
        list.AddRange(BitConverter.GetBytes(buildTime));
        list.AddRange(unpackerId.ToByteArray());

        return list.ToArray();
    }

    public static TrainEggPendingOrder Deserialize(byte[] bytes)
    {

        return new TrainEggPendingOrder(
            // skip the type byte
            //cost: BitConverter.ToInt32(bytes, 1),
            unpackInto: (AntType)BitConverter.ToInt32(bytes, 5),
            id: new Guid(bytes.AsSpan(9, 16)),
            buildTime: BitConverter.ToInt32(bytes, 25),
            unpackerId: new Guid(bytes.AsSpan(29, 16))
            );
    }

    public bool TryOrder(SimulationState state, AntState ant, [MaybeNullWhen(false)] out IOrderState order)
    {
        if (state.money.GetMainMoney(ant.immutable.side) >= AntFactory.Cost(unpackInto) &&
            state.populationState.allowedPopulation >= state.CompletAndPendingPopulation(ant.immutable.side) + AntFactory.Population(unpackInto) &&
            state.money.GetGathererMoney(ant.immutable.side) >= AntFactory.GathererCost(unpackInto))
        {
            state.money.AssertSpendMainMoney(AntFactory.Cost(unpackInto), ant.immutable.side);
            state.money.AssertSpendGathererMoney(AntFactory.GathererCost(unpackInto), ant.immutable.side);
            state.populationState.pendingPopulation[ant.immutable.side] += AntFactory.Population(unpackInto);
            order = new TrainMakeUnitOrderImmutableState(id, unpackInto, buildTime, unpackerId);
            return true;
        }

        Log.WriteLine("EggKeyAction no money");
        order = default;
        return false;
    }

    public override bool Equals(object? obj)
    {
        return obj is TrainEggPendingOrder order &&
               //cost == order.cost &&
               unpackInto == order.unpackInto &&
               id.Equals(order.id) &&
               buildTime == order.buildTime &&
               unpackerId.Equals(order.unpackerId);
    }

    public override int GetHashCode()
    {
        return this.Hash();
        //return HashCode.Combine(cost, unpackInto, id, buildTime, unpackerId);
    }

    public IEnumerable<(string, object?)> HashData()
    {
        //yield return (nameof(cost), cost);
        yield return (nameof(unpackInto), unpackInto);
        yield return (nameof(id), id);
        yield return (nameof(buildTime), buildTime);
        yield return (nameof(unpackerId), unpackerId);
    }
}
