﻿public class TrainEggKeyAction : IKeyAction
{
    //private readonly int cost;
    private readonly AntType unpackInto;
    private readonly int buildTime;

    public TrainEggKeyAction(AntType unpackInto, int buildTimeFrames)
    {
        //this.cost = AntFactory.Cost(unpackInto); ;
        this.unpackInto = unpackInto;
        this.buildTime = buildTimeFrames;
    }

    public IPendingOrderState GetPendingOrder()
    {
        return new TrainEggPendingOrder(unpackInto, Guid.NewGuid(), buildTime, Guid.NewGuid());
    }

    public override int GetHashCode()
    {
        return unpackInto.GetHashCode() + buildTime;
    }

    public override bool Equals(object? obj)
    {
        return obj is TrainEggKeyAction action &&
               //cost == action.cost &&
               unpackInto == action.unpackInto &&
               buildTime == action.buildTime;
    }

    public override string? ToString() => $"place egg {unpackInto} ${AntFactory.Cost(unpackInto)}";
}
