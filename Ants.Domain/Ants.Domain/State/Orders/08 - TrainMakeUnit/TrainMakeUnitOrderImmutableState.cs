﻿using Ants.Domain.Infrastructure;
using Ants.Domain.State;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Intrinsics;
using System.Runtime.Intrinsics.X86;

public class TrainMakeUnitOrderImmutableState : IOrderState, IPendingOrderState
{
    public Guid Id { get; init; }
    public AntType unit { get; init; }
    //public int refund { get; init; }
    public int buildTime { get; init; }
    public Guid unpackerId { get; init; }

    public const byte typeByte = 8;

    public byte[] Serialize()
    {
        var res = new List<byte>();
        res.Add(typeByte);
        res.AddRange(Id.ToByteArray());
        res.AddRange(BitConverter.GetBytes((int)unit));
        //res.AddRange(BitConverter.GetBytes(refund));
        res.AddRange(BitConverter.GetBytes(buildTime));
        res.AddRange(unpackerId.ToByteArray());

        return res.ToArray();
    }

    public static TrainMakeUnitOrderImmutableState Deserialize(byte[] bytes)
    {
        var at = 1;
        var id = new Guid(bytes.AsSpan(at, 16));
        at += 16;
        var unit = (AntType)BitConverter.ToInt32(bytes, at);
        at += 4;
        //var refund = BitConverter.ToInt32(bytes, at);
        //at += 4;
        var buildTime = BitConverter.ToInt32(bytes, at);
        at += 4;
        var unpackerId = new Guid(bytes.AsSpan(at, 16));
        at += 16;
        return new TrainMakeUnitOrderImmutableState(id, unit, /*refund,*/ buildTime, unpackerId);
    }

    public TrainMakeUnitOrderImmutableState(Guid Id, AntType unit, /*int refund,*/ int buildTime, Guid unpackerId)
    {
        this.unit = unit;
        //this.refund = refund;
        this.Id = Id;
        this.buildTime = buildTime;
        this.unpackerId = unpackerId;
    }

    public bool TryOrder(SimulationState state, AntState ant, [MaybeNullWhen(false)] out IOrderState order)
    {
        order = this;
        return true;
    }

    /// <returns>new unit if added, null othersies </returns>
    public AntState? ActOrRefund(SimulationState state, AntState myAnt, Map2<AntState> map, double maxMoney)
    {
        
        var res = AntFactory.MakeUnpacker(myAnt.immutable.side, Id, Vector128<double>.Zero, buildTime, unit, unpackerId);
        state.populationState.pendingPopulation[myAnt.immutable.side] -= AntFactory.Population(res.immutable.type);

        foreach (var station in state.Stations
            .Where(x => Avx.Subtract(myAnt.position, x.position).Length() < 300)
            .OrderBy(x => Avx.Subtract(myAnt.position, x.position).Length()))
        {
            res.position = station.position;
            if (!map.Hit(res).Any())
            {
                map.Add(res);
                return res;
            }
        } 

        Log.WriteLine($"no where to place train unit");
        state.money.GainMainMoney(AntFactory.Cost(myAnt.immutable.type),myAnt.immutable.side, maxMoney);
        state.money.GainGathererMoney(AntFactory.GathererCost(myAnt.immutable.type), myAnt.immutable.side);
        return null;
    }

    public IOrderState CopyIfMutable()
    {
        return this;
    }

    public void Cancel(AntState actor)
    {
    }

    public override bool Equals(object? obj)
    {
        return obj is TrainMakeUnitOrderImmutableState state &&
               Id.Equals(state.Id) &&
               unit == state.unit &&
               //refund == state.refund &&
               buildTime == state.buildTime &&
               unpackerId.Equals(state.unpackerId);
    }

    public override int GetHashCode()
    {
        return this.Hash();
        //return HashCode.Combine(Id, unit, refund, buildTime, unpackerId);
    }

    public IEnumerable<(string, object?)> HashData()
    {
        yield return (nameof(Id), Id);
        yield return (nameof(unit), unit);
        //yield return (nameof(refund), refund);
        yield return (nameof(buildTime), buildTime);
        yield return (nameof(unpackerId), unpackerId);
    }
}
