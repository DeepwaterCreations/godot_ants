﻿using Ants.Domain.State;
using Prototypist.Toolbox.Object;
using System.Diagnostics.CodeAnalysis;

public class SprintPendingOrder : IPendingOrderState
{
    public readonly MovePendingImmutableState inner;

    public SprintPendingOrder(MovePendingImmutableState inner)
    {
        this.inner = inner;
    }

    public bool TryOrder(SimulationState state, AntState ant, [MaybeNullWhen(false)] out IOrderState order)
    {
        if (inner.TryOrder(state, ant, out var innerOrder))
        {
            order = new SprintOrder(innerOrder.SafeCastTo(out MoveOrderState _));
            return true;
        }
        order = null;
        return false;
    }


    //public IOrderState Order() => new SprintOrder(inner.Order().SafeCastTo(out MoveOrderState _));

    public byte[] Serialize()
    {
        var list = new List<byte>(); 
        list.Add(typeByte);
        list.AddRange(inner.Serialize().Skip(1));

        return list.ToArray();
    }

    public static SprintPendingOrder Deserialize(byte[] bytes) {
        var list = new List<byte>();
        list.Add(MovePendingImmutableState.typeByte);
        list.AddRange(bytes.Skip(1));
        return new SprintPendingOrder(MovePendingImmutableState.Deserialize(list.ToArray()));
    }

    public override bool Equals(object? obj)
    {
        return obj is SprintPendingOrder order &&
               EqualityComparer<MovePendingImmutableState>.Default.Equals(inner, order.inner);
    }

    public override int GetHashCode()
    {
        return this.Hash();
        //return HashCode.Combine(inner);
    }

    public IEnumerable<(string, object?)> HashData()
    {
        yield return (nameof(inner), inner);
    }

    public const byte typeByte = 3;
}
