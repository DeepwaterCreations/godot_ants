﻿using Ants.Domain.State;
using Prototypist.Toolbox.Object;
using System.Collections.Concurrent;
using System.Runtime.Intrinsics;

public class SprintOrder : IOrderState, IMoveOrder
{
    private MoveOrderState moveOrderState;

    public SprintOrder(MoveOrderState moveOrderState)
    {
        this.moveOrderState = moveOrderState;
    }

    public void Cancel(AntState actor)
    {
        actor.sprint.sprinting = false;
    }

    public IOrderState CopyIfMutable()
    {
        return new SprintOrder(moveOrderState.CopyIfMutable().SafeCastTo(out MoveOrderState _));
    }

    public override bool Equals(object? obj)
    {
        return obj is SprintOrder order &&
               EqualityComparer<MoveOrderState>.Default.Equals(moveOrderState, order.moveOrderState);
    }

    public Vector128<double> GetDestination()
    {
        return ((IMoveOrder)moveOrderState).GetDestination();
    }

    public override int GetHashCode()
    {
        return this.Hash();
        //return HashCode.Combine(moveOrderState) + nameof(SprintOrder).GetHashCode();
    }

    public IEnumerable<(string, object?)> HashData()
    {
        yield return (nameof(moveOrderState), moveOrderState);
    }

    internal bool Act(AntState ant, Map2<MudState> terrainMap, IEnumerable<(Vector128<double> globalPosition, double radius)> avoidables, Map2<AntState> antsMap)
    {
        if (ant.sprint.sprinting)
        {
            ant.sprint!.energy = Math.Max(ant.sprint.energy - 1, 0);
            if (ant.sprint.energy <= 0)
            {
                ant.sprint.sprinting = false;
            }
        }
        else if (ant.sprint.energy >= 1) {
            ant.sprint.sprinting = true;
        }
        var res = moveOrderState.Act(ant, terrainMap, avoidables, antsMap);

        if (res) {
            ant.sprint.sprinting = false;
        }

        return res;
    }
}
