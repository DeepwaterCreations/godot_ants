﻿using Ants.Domain.Infrastructure;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Runtime.Intrinsics;
using System.Runtime.Intrinsics.X86;
using System.Text;
using System.Threading.Tasks;

namespace Ants.Domain.State.Orders._13___Zapper
{
    public class ZapperOrder : IPendingOrderState, IOrderState
    {
        public static byte typeByte = 13;
        public readonly Vector128<double> mousePosition;
        public readonly Guid webLineId;

        public ZapperOrder(Vector128<double> position, Guid webLineId)
        {
            this.mousePosition = position;
            this.webLineId = webLineId;
        }

        public void Cancel(AntState actor)
        {
        }

        public IOrderState CopyIfMutable()
        {
            return this;
        }

        public override bool Equals(object? obj)
        {
            return obj is ZapperOrder order &&
                   mousePosition.Equals(order.mousePosition) &&
                   webLineId.Equals(order.webLineId);
        }

        public override int GetHashCode()
        {
            return this.Hash();
        }

        public IEnumerable<(string, object?)> HashData()
        {
            yield return (nameof(mousePosition), mousePosition);
            yield return (nameof(webLineId), webLineId);
        }

        public byte[] Serialize()
        {
            var list = new List<byte>
            {
                typeByte
            };
            list.AddRange(BitConverter.GetBytes(mousePosition.X()));
            list.AddRange(BitConverter.GetBytes(mousePosition.Y()));
            list.AddRange(webLineId.ToByteArray());

            return list.ToArray();
        }
        public static ZapperOrder Deserialize(byte[] bytes)
        {
            return new ZapperOrder(
                Vector128.Create(
                        BitConverter.ToDouble(bytes, 1),
                        BitConverter.ToDouble(bytes, 9)
                    ),
                    new Guid(bytes.AsSpan(17)));
        }

        public bool TryOrder(SimulationState state, AntState ant, [MaybeNullWhen(false)] out IOrderState order)
        {
            Debug.Assert(ant.zapperState != null);
            if (ant.zapperState.energy < ant.zapperState.MaxEnergy()) {
                order = null;
                return false;
            }

            ant.zapperState.energy = 0;
            order = this;
            return true;
        }
    }
}