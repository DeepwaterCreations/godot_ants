﻿using Ants.Domain.Infrastructure;
using Ants.Domain.State;
using Ants.Domain.State.Orders._13___Zapper;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Intrinsics;
using System.Runtime.Intrinsics.X86;

public class ZapperNeedsClick : IPendingClickOrderState
{

    public override int GetHashCode()
    {
        // Guid.Parse("{0A6E16DF-3040-498F-A675-36A00501D3A0}").GetHashCode();
        return 1124356668;
    }

    public override bool Equals(object? obj)
    {
        return obj is ZapperNeedsClick;
    }

    public bool TryOrder(Vector128<double> mousePosition, SimulationState state, AntState ant, ITrainPathing game, ImmutableSimulationState immutableSimulationState, AntState[] alsoOrdered, [MaybeNullWhen(false)] out IPendingOrderState order)
    {
        // shouldn't this just go in the direction you click?
        Debug.Assert(ant.zapperState != null);
        order = new ZapperOrder(mousePosition, Guid.NewGuid());
        return true;
    }
}