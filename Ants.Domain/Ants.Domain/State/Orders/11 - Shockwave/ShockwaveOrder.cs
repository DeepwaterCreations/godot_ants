﻿using Ants.Domain.Infrastructure;
using Ants.Domain.State;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Intrinsics;

public class ShockwaveOrder : IPendingOrderState, IOrderState
{
    public readonly Vector128<double> mousePosition;
    public readonly Guid smokeId;

    public ShockwaveOrder(Vector128<double> mousePosition, Guid smokeId)
    {
        this.mousePosition = mousePosition;
        this.smokeId = smokeId;
    }

    public bool TryOrder(SimulationState state, AntState ant, [MaybeNullWhen(false)] out IOrderState order)
    {
        Debug.Assert(ant.shockwaverState != null);
        if (ant.shockwaverState.energy >= ShockwaverState.costToCast) {
            ant.shockwaverState.energy -= ShockwaverState.costToCast;
            order = this;
            return true;
        }
        order = default;
        return false;
    }

    public void Cancel(AntState actor)
    {
    }

    public IOrderState CopyIfMutable()
    {
        return this;
    }

    public byte[] Serialize()
    {
        var list = new List<byte>
        {
            typeByte
        };
        list.AddRange(BitConverter.GetBytes(mousePosition.X()));
        list.AddRange(BitConverter.GetBytes(mousePosition.Y()));
        list.AddRange(smokeId.ToByteArray());

        return list.ToArray();
    }

    public static ShockwaveOrder Deserialize(byte[] bytes)
    {
        return new ShockwaveOrder(
            Vector128.Create(
                    BitConverter.ToDouble(bytes, 1),
                    BitConverter.ToDouble(bytes, 9)
                ),
            new Guid(bytes.AsSpan(17)));
    }

    public override bool Equals(object? obj)
    {
        return obj is ShockwaveOrder order &&
               mousePosition.Equals(order.mousePosition);
    }

    public override int GetHashCode()
    {
        return this.Hash();
        //return HashCode.Combine(mousePosition);
    }

    public IEnumerable<(string, object?)> HashData()
    {
        yield return (nameof(mousePosition), mousePosition);
        yield return (nameof(smokeId), smokeId);
    }

    public static byte typeByte = 11;
}

