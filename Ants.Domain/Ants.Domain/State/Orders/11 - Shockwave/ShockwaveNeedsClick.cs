﻿using Ants.Domain.State;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Intrinsics;

public class ShockwaveNeedsClick : IPendingClickOrderState
{

    public override int GetHashCode()
    {
        return Guid.Parse("{1B78D7D9-8529-4149-BD34-92DDE2058030}").GetHashCode();
    }

    public override bool Equals(object? obj)
    {
        return obj is ShockwaveNeedsClick;
    }

    public bool TryOrder(Vector128<double> mousePosition, SimulationState state, AntState ant, ITrainPathing game, ImmutableSimulationState immutableSimulationState, AntState[] alsoOrdered, [MaybeNullWhen(false)] out IPendingOrderState order)
    {
        Debug.Assert(ant.shockwaverState != null);
        order = ant.shockwaverState.Shockwave(mousePosition);
        return true;
    }
}