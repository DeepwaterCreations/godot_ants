﻿using Ants.Domain.Infrastructure;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Runtime.Intrinsics;
using System.Text;
using System.Threading.Tasks;

namespace Ants.Domain.State.Orders._12___Arty
{
    public class ArtyOrder : IPendingOrderState, IOrderState
    {
        public static byte typeByte = 12;
        public readonly Vector128<double> mousePosition;

        public ArtyOrder(Vector128<double> position)
        {
            this.mousePosition = position;
        }

        public void Cancel(AntState actor)
        {
        }

        public IOrderState CopyIfMutable()
        {
            return this;
        }

        public override bool Equals(object? obj)
        {
            return obj is ArtyOrder order &&
                   mousePosition.Equals(order.mousePosition);
        }

        public override int GetHashCode()
        {
            return this.Hash();
        }

        public IEnumerable<(string, object?)> HashData()
        {
            yield return (nameof(mousePosition), mousePosition);
        }

        public byte[] Serialize()
        {
            var list = new List<byte>
            {
                typeByte
            };
            list.AddRange(BitConverter.GetBytes(mousePosition.X()));
            list.AddRange(BitConverter.GetBytes(mousePosition.Y()));

            return list.ToArray();
        }
        public static ArtyOrder Deserialize(byte[] bytes)
        {
            return new ArtyOrder(
                Vector128.Create(
                        BitConverter.ToDouble(bytes, 1),
                        BitConverter.ToDouble(bytes, 9)
                    ));
        }

        public bool TryOrder(SimulationState state, AntState ant, [MaybeNullWhen(false)] out IOrderState order)
        {
            Debug.Assert(ant.artyState != null);
            order = this;
            return true;
        }
    }
}