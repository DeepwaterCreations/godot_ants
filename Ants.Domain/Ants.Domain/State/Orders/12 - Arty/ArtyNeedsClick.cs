﻿using Ants.Domain.State;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Intrinsics;

public class ArtyNeedsClick : IPendingClickOrderState
{

    public override int GetHashCode()
    {
        return Guid.Parse("{ABEF113A-3296-438D-A452-19EA7FBCD0E1}").GetHashCode();
    }

    public override bool Equals(object? obj)
    {
        return obj is ArtyNeedsClick;
    }

    public bool TryOrder(Vector128<double> mousePosition, SimulationState state, AntState ant, ITrainPathing game, ImmutableSimulationState immutableSimulationState, AntState[] alsoOrdered, [MaybeNullWhen(false)] out IPendingOrderState order)
    {
        Debug.Assert(ant.artyState != null);
        order = ant.artyState.Shoot(mousePosition);
        return true;
    }
}