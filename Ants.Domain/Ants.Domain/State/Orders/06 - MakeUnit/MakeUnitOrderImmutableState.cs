﻿using Ants.Domain.Infrastructure;
using Ants.Domain.State;
using System;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Intrinsics;
using System.Runtime.Intrinsics.X86;
using static System.Collections.Specialized.BitVector32;
// you pay upfront
// and then get a refund if the unit isn't placed
public class MakeUnitOrderImmutableState : IOrderState, IPendingOrderState
{
    public Guid Id { get; init; }
    public AntType unit { get; init; }
    //public int refund { get; init; }
    //public Guid unpackerId { get; init; }
    //public double populationRefund { get; init; }
    public const byte typeByte = 6;
    private readonly Vector128<double> moveTo;
    private readonly Vector128<double> placeToward;

    public byte[] Serialize()
    {
        var res = new List<byte>();
        res.Add(typeByte);
        res.AddRange(Id.ToByteArray());
        res.AddRange(BitConverter.GetBytes((int)unit));
        //res.AddRange(BitConverter.GetBytes(refund));
        //res.AddRange(unpackerId.ToByteArray());
        //res.AddRange(BitConverter.GetBytes(populationRefund));
        res.AddRange(BitConverter.GetBytes(moveTo.X()));
        res.AddRange(BitConverter.GetBytes(moveTo.Y()));
        res.AddRange(BitConverter.GetBytes(placeToward.X()));
        res.AddRange(BitConverter.GetBytes(placeToward.Y()));

        return res.ToArray();
    }

    public static MakeUnitOrderImmutableState Deserialize(byte[] bytes)
    {
        var at = 1;
        var id = new Guid(bytes.AsSpan(at, 16));
        at += 16;
        var unit = (AntType)BitConverter.ToInt32(bytes, at);
        at += 4;
        //var refund = BitConverter.ToInt32(bytes, at);
        //at += 4;
        var unpackerId = new Guid(bytes.AsSpan(at, 16));
        at += 16;
        //var populationRefund = BitConverter.ToInt32(bytes, at);
        //at += 8;
        var moveToX = BitConverter.ToDouble(bytes, at);
        at += 8;
        var moveToY = BitConverter.ToDouble(bytes, at);
        at += 8;
        var placeTowardsX = BitConverter.ToDouble(bytes, at);
        at += 8;
        var placeTowardsY = BitConverter.ToDouble(bytes, at);
        at += 8;
        return new MakeUnitOrderImmutableState(id, unit, /*refund,  unpackerId, populationRefund,*/ Vector128.Create(moveToX, moveToY), Vector128.Create(placeTowardsX, placeTowardsY));
    }

    public MakeUnitOrderImmutableState(Guid Id, AntType unit, /*int refund,  Guid unpackerId, double populationRefund,*/ Vector128<double> moveTo, Vector128<double> placeToward)
    {
        this.unit = unit;
        //this.refund = refund;
        this.Id = Id;
        //this.unpackerId = unpackerId;
        //this.populationRefund = populationRefund;
        this.moveTo = moveTo;
        this.placeToward = placeToward;

    }

    public bool TryOrder(SimulationState state, AntState ant, [MaybeNullWhen(false)] out IOrderState order)
    {
        order = this;
        return true;
    }

    /// <returns>new unit if added, null othersies </returns>
    public AntState? ActOrRefund(
        SimulationState state, 
        AntState placer, 
        Map2<AntState> antMap, 
        ImmutableSimulationState immutableSimulationState, 
        double maxMoney, 
        int side, 
        Action<Guid, IPendingOrderState, bool> enqueueOrder /*this is a bit of a gross hack, this method is on domain but game is in Ants and domain doesn't have access. Maybe I should move game to domain.*/) 
    {
        // 
        var stopWatch = Stopwatch.StartNew();
        var antsToAvoid = state.ants.Select(x => (x.Value.position, x.Value.physicsImmunatbleInner.radius)).ToArray();
        var res = AntFactory.Make(Id, unit, placer.immutable.side, Vector128<double>.Zero);
        var avoidables = res.immutable.mudMode == Pathing.Mode.IgnoreMud ? immutableSimulationState.rocks.Select(x=>(x.position, x.physicsImmunatbleInner.radius)).Union(antsToAvoid).OrderBy(x=>x.position.X()).ThenBy(x => x.position.Y()).ThenBy(x=>x.radius).ToArray() 
                                                                         : immutableSimulationState.rocks.Select(x => (x.position, x.physicsImmunatbleInner.radius)).Union(antsToAvoid).Union(immutableSimulationState.mud.Select(x => (x.position, x.physicsImmunatbleInner.radius))).OrderBy(x => x.position.X()).ThenBy(x => x.position.Y()).ThenBy(x => x.radius).ToArray();
        var placesTried = 0;

        state.populationState.pendingPopulation[placer.immutable.side] -= AntFactory.Population(res.immutable.type);

        ;// AntFactory.MakeUnpacker(myAnt.immutable.side, Id, Vector128<double>.Zero, buildTime, unit, unpackerId);
        var startAt = AntFactory.AntSize(placer.immutable.type) + res.physicsImmunatbleInner.radius + Pathing.padding;
        var localTowards = Avx.Subtract(placeToward, placer.position).ToLength(startAt);
        var startAnlge = Math.Atan2(localTowards.X(), localTowards.Y());


        foreach (var place in Spiral(
                maxDistance: 300, 
                radius: res.physicsImmunatbleInner.radius, 
                padding: Pathing.padding, 
                startAt: startAt,
                startAnlge: startAnlge)
            .Select(x => Avx.Add(x, placer.position)))
        {
            placesTried++;
            Log.WriteLine($"place {place}");
            if (Pathing.TryAdjustTarget(place, avoidables, res.physicsImmunatbleInner.radius, res.physicsImmunatbleInner.radius, out var adjustedPlace)) {

                res.position = adjustedPlace;

                // ...should this be in try adjust target
                if (res.immutable.mudMode == Pathing.Mode.ImpassableMud && IsInMud(immutableSimulationState.mudMap, res)) 
                {
                    Log.WriteLine("don't place in the mud"); 
                    continue;
                }
                if (!immutableSimulationState.rockMap.Hit(res).Any() && !antMap.Hit(res).Any())
                {
                    antMap.Add(res);
                    stopWatch.Stop();
                    Log.WriteLine($"time to place {stopWatch.Elapsed.TotalMilliseconds}ms placesTried: {placesTried}, placed at: {res.position}, id: {res.immutable.id}");
                    state.control.AddToPool(AntFactory.Cost(unit));

                    // this is too slow to do in the simulation
                    if (res.immutable.side == side)
                    {
                        Task.Run(() =>
                        {
                            if (new MoveAction().TryOrder(moveTo, state, res, null /*todo get this here*/, immutableSimulationState, new[] { res } , out var order))
                            {
                                enqueueOrder(res.immutable.id, order, false);
                            }
                            else
                            {
                                Log.WriteLine("cant execute order!");
                            }
                        }).ContinueWith(x =>
                        {
                            if (x.IsFaulted)
                            {
                                Log.WriteLine($"order failed {x.Exception}");
                            }
                        });
                    }

                    return res;
                }
                else 
                {
                    Log.WriteLine("could not add to map");
                }
            }
        }

        stopWatch.Stop();
        Log.WriteLine($"no where to place unit {stopWatch.Elapsed.TotalMilliseconds}ms placesTried {placesTried}");
        state.money.GainMainMoney(AntFactory.Cost(unit), placer.immutable.side, maxMoney);
        state.money.GainGathererMoney(AntFactory.GathererCost(unit), placer.immutable.side);
        return null;
    }

    public bool IsInMud(Map2<MudState> mudMap, PhysicsSubjectStateInner2 couldBeInMud)
    {
        return mudMap.Hit(couldBeInMud).Any();
    }
    
    public IEnumerable<Vector128<double>> Spiral(double maxDistance, double radius, double padding, double startAt, double startAnlge) 
    {
        //var angle = 0.0;
        var distance = startAt;

        while (distance < maxDistance)
        {
            var arcAngle = (((radius * 2.0) + padding) / distance);
            var steps = Math.Floor(Math.Tau / arcAngle);
            var angleAdd = Math.Tau / steps;

            for (int i = 0; i < steps; i++)
            {
                var angle = (angleAdd * i) - (startAnlge - Math.Tau/4.0);
                yield return Vector128.Create(distance, 0).Rotated(angle);
            }
            distance += (radius * 2.0) + padding;
        }

        //while (distance < maxDistance) {
        //    angle += ((radius * 2.0) + padding) / distance;
        //    if (angle > Math.PI * 2) {
        //        angle =- Math.PI * 2;
        //        distance += (radius * 2.0) + padding;
        //    }
        //    yield return Vector128.Create(distance, 0).Rotated(angle);
        //}
    }


    public IEnumerable<Vector128<double>> HoneyComb(int maxLevel, double combSize)
    {

        var at = Vector128.Create( 0.0, 0.0);
        for (int level = 1; level <= maxLevel; level++)
        {
            at = Avx.Add(at, Avx.Multiply( Vector128.Create(0.0, -1.0 ), (combSize * 2).AsVector()));

            for (int i = 0; i < level; i++)
            {
                at = Avx.Add(at, Avx.Multiply(Vector128.Create(0.0, -1.0 ).Rotated((2 / 6.0) * (Math.PI * 2)), (combSize * 2).AsVector()));
                yield return Vector128.Create(at.X(), at.Y() );
            }
            for (int i = 0; i < level; i++)
            {
                at = Avx.Add(at, Avx.Multiply(Vector128.Create(0.0, 1.0 ), (combSize * 2).AsVector()));
                yield return Vector128.Create(at.X(), at.Y() );
            }
            for (int i = 0; i < level; i++)
            {
                at = Avx.Add(at, Avx.Multiply(Vector128.Create(0.0, -1.0).Rotated((-2 / 6.0) * (Math.PI * 2)), (combSize * 2).AsVector()));
                yield return Vector128.Create(at.X(), at.Y());
            }
            for (int i = 0; i < level; i++)
            {
                at = Avx.Add(at, Avx.Multiply(Vector128.Create(0.0, -1.0).Rotated((-1 / 6.0) * (Math.PI * 2)), (combSize * 2).AsVector()));
                yield return Vector128.Create(at.X(), at.Y());
            }
            for (int i = 0; i < level; i++)
            {
                at = Avx.Add(at, Avx.Multiply(Vector128.Create(0.0, -1.0), (combSize * 2).AsVector()));
                yield return Vector128.Create(at.X(), at.Y());
            }
            for (int i = 0; i < level; i++)
            {
                at = Avx.Add(at, Avx.Multiply(Vector128.Create(0.0, -1.0 ).Rotated((1 / 6.0) * (Math.PI * 2)), (combSize * 2).AsVector()));
                yield return Vector128.Create(at.X(), at.Y() );
            }
        }
    }

    public IOrderState CopyIfMutable()
    {
        return this;
    }

    public void Cancel(AntState actor)
    {
    }

    public override bool Equals(object? obj)
    {
        return obj is MakeUnitOrderImmutableState state &&
               Id.Equals(state.Id) &&
               unit == state.unit &&
               //unpackerId.Equals(state.unpackerId) &&
               moveTo.Equals(state.moveTo) &&
               placeToward.Equals(state.placeToward);
    }

    public override int GetHashCode()
    {
        return this.Hash();
        //return HashCode.Combine(Id, unit, refund, buildTime, unpackerId);
    }

    public IEnumerable<(string, object?)> HashData()
    {
        yield return (nameof(Id), Id);
        yield return (nameof(unit), unit);
        //yield return (nameof(unpackerId), unpackerId);
        yield return (nameof(moveTo), moveTo);
        yield return (nameof(placeToward), placeToward);
    }
}
