﻿using Ants.Domain.State;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Intrinsics;

public class MoveWithShiledUpNeedsClick : IPendingClickOrderState
{
    public bool TryOrder(Vector128<double> mousePosition, SimulationState state, AntState ant, ITrainPathing game, ImmutableSimulationState immutableSimulationState, AntState[] alsoOrdered, [MaybeNullWhen(false)] out IPendingOrderState order) {
        var avoidables = immutableSimulationState.rocks.Select(x => (x.position, x.physicsImmunatbleInner.radius)).ToArray();
        if (Pathing.GetPath3(immutableSimulationState.paths.knownPaths[(ant.immutable.mudMode, ant.physicsImmunatbleInner.radius)], ant.position, mousePosition, out var res))
        {
            if (!res.Any())
            {
                order = default;
                return false;
            }

            var withIn = MoveAction.WithIn(ant, alsoOrdered);

            order =new PendingMoveWithShiledUpOrderImmutableState(new MovePendingImmutableState(res.ToArray(), withIn));
            return true;
        }
        order = default;
        return false;
    }
    public override int GetHashCode()
    {
        return 36663670;
        //return nameof(MoveWithShiledUpNeedsClick).GetHashCode();
    }

    public override bool Equals(object? obj)
    {
        return obj is MoveWithShiledUpNeedsClick;
    }

    public override string ToString() => "move without shield";
}
