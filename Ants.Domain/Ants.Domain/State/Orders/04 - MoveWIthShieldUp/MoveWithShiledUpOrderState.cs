﻿using Ants.Domain.State;
using Prototypist.Toolbox.Object;
using System.Collections.Concurrent;
using System.Runtime.Intrinsics;
//public class SprintClickAction : IKeyClickAction
//{
//    public IPendingClickOrderState GetPendingClickOrder() => new MoveWithShiledUpNeedsClick();
//}

//public class MoveWithShiledUpClickAction : IKeyClickAction
//{
//    public IPendingClickOrderState GetPendingClickOrder() => new MoveWithShiledUpNeedsClick();
//}

// is this mutable or no? the order can change so I think it's mutable 
public class MoveWithShiledUpOrderState : IOrderState, IMoveOrder
{
    public MoveOrderState activeOrder;

    public MoveWithShiledUpOrderState(MoveOrderState activeOrder)
    {
        this.activeOrder = activeOrder ?? throw new ArgumentNullException(nameof(activeOrder));
    }

    public bool Act(AntState actor, Map2<MudState> mudMap, IEnumerable<(Vector128<double> globalPosition, double radius)> avoidables, Map2<AntState> antsMap)
    {
        if (actor.shield.SafeIs(out ShieldState shieldState))
        {
            if (activeOrder.Act(actor,mudMap, avoidables, antsMap))
            {
                shieldState.Enable();
                return true;
            }
            else {
                shieldState.Disable();
            }
            return false;
        }
        return true;
    }

    public void Cancel(AntState actor)
    {
        if (actor.shield.SafeIs(out ShieldState shieldState))
        {
            shieldState.Enable();
        }
    }

    public IOrderState CopyIfMutable()
    {
        return new MoveWithShiledUpOrderState(activeOrder.CopyTyped());
    }

    public override bool Equals(object? obj)
    {
        return obj is MoveWithShiledUpOrderState state &&
               EqualityComparer<MoveOrderState>.Default.Equals(activeOrder, state.activeOrder);
    }

    public Vector128<double> GetDestination()
    {
        return ((IMoveOrder)activeOrder).GetDestination();
    }

    public override int GetHashCode()
    {
        return this.Hash();
        //return activeOrder.GetHashCode() + nameof(MoveWithShiledUpOrderState).GetHashCode();
    }

    public IEnumerable<(string, object?)> HashData()
    {
        yield return (nameof(activeOrder), activeOrder);
    }
}
