﻿using Ants.Domain.State;
using Prototypist.Toolbox.Object;
using System.Diagnostics.CodeAnalysis;

public class PendingMoveWithShiledUpOrderImmutableState : IPendingOrderState
{
    public readonly MovePendingImmutableState inner;

    public PendingMoveWithShiledUpOrderImmutableState(MovePendingImmutableState to)
    {
        this.inner = to;
    }

    public bool TryOrder(SimulationState state, AntState ant, [MaybeNullWhen(false)] out IOrderState order)
    {
        //order = new MoveWithShiledUpOrderState(inner.Order().SafeCastTo(out MoveOrderState _));
        //return true;

        if (inner.TryOrder(state, ant, out var innerOrder))
        {
            order = new MoveWithShiledUpOrderState(innerOrder.SafeCastTo(out MoveOrderState _));
            return true;
        }
        order = null;
        return false;
    }

    //public IOrderState Order() => new MoveWithShiledUpOrderState(inner.Order().SafeCastTo(out MoveOrderState _));

    public byte[] Serialize()
    {
        var list = new List<byte>();
        list.Add(typeByte);
        list.AddRange(inner.Serialize().Skip(1));

        return list.ToArray();
    }

    public static PendingMoveWithShiledUpOrderImmutableState Deserialize(byte[] bytes)
    {
        var list = new List<byte>();
        list.Add(MovePendingImmutableState.typeByte);
        list.AddRange(bytes.Skip(1));
        return new PendingMoveWithShiledUpOrderImmutableState(MovePendingImmutableState.Deserialize(list.ToArray()));
    }

    public override bool Equals(object? obj)
    {
        return obj is PendingMoveWithShiledUpOrderImmutableState state &&
               EqualityComparer<MovePendingImmutableState>.Default.Equals(inner, state.inner);
    }

    public override int GetHashCode()
    {
        return HashCode.Combine(inner) + nameof(PendingMoveWithShiledUpOrderImmutableState).GetHashCode();
    }

    public IEnumerable<(string, object?)> HashData()
    {
        yield return (nameof(inner), inner);
    }

    public const byte typeByte = 4;
}
