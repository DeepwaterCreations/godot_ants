﻿

using System.Runtime.Intrinsics;

namespace Ants.Domain.State
{
    public class ZapperSetupState : PhysicsSubjectStateInner2, IBlocksShots
    {
        public const double Radius = 5;
        public const double MaxHp = 5;

        // doubles as Id
        public class Immutable: IStateHash
        {
            public readonly Guid createdBy;
            public readonly int createdOnTick;
            public readonly Guid webLineId;

            public Immutable(Guid createdBy, int createdOnTick, Guid webLineId)
            {
                this.createdBy = createdBy;
                this.createdOnTick = createdOnTick;
                this.webLineId = webLineId;
            }

            public double Radius => ZapperSetupState.Radius;


            public IEnumerable<(string, object?)> HashData()
            {
                yield return (nameof(createdBy), createdBy);
                yield return(nameof(createdOnTick), createdOnTick);
                yield return (nameof(webLineId), webLineId);
            }
        }

        public readonly Immutable immutable;
        public double hp;

        double IBlocksShots.Radius => physicsImmunatbleInner.radius;
        // a little werid because position is immutable
        public Vector128<double> Position => position;

        public ZapperSetupState(Immutable immutable, double hp, Vector128<double> position) :base(new ImmunatblePhysicsStateInner(Radius), position)
        {
            this.immutable = immutable;
            this.hp = hp;
        }

        public override IEnumerable<(string, object?)> HashData()
        {
            yield return (nameof(immutable), immutable);
            yield return (nameof(hp), hp);
        }

        internal ZapperSetupState Copy()
        {
            return new ZapperSetupState(immutable, hp, position);
        }

        internal Guid SortHash()
        {
            return immutable.createdBy;
        }
    }
}