﻿using System.Runtime.Intrinsics;
using static BulletState.Immutable;

public class ControlPointState: IStateHash
{
    // for drawing
    public int side;

    //immutable
    public readonly Vector128<double> position;

    public ControlPointState(int side, Vector128<double> position)
    {
        this.side = side;
        this.position = position;
    }

    internal ControlPointState Copy()
    {
        return this;
    }

    //public override bool Equals(object? obj)
    //{
    //    return obj is ControlPointState state &&
    //           side == state.side &&
    //           position.Equals(state.position);
    //}

    //public override int GetHashCode()
    //{
    //    return this.Hash();
    //    //return HashCode.Combine(side, position);
    //}

    public IEnumerable<(string, object?)> HashData()
    {
        yield return (nameof(side), side);
        yield return (nameof(position), position);
    }
}
