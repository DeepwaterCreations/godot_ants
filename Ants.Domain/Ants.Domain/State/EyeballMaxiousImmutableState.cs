﻿using Ants.Domain.Infrastructure;
using Ants.Domain.State;
using Prototypist.Toolbox;
using Prototypist.Toolbox.IEnumerable;
using System.Runtime.Intrinsics;
using System.Runtime.Intrinsics.X86;
// really favorite target
//public class ShootOrderImmutableState : IOrderState, IPendingOrderState
//{
//    public void Cancel(AntState actor)
//    {
//    }

//    public IOrderState CopyIfMutable()
//    {
//        return this;
//    }

//    public override bool Equals(object? obj)
//    {
//        return base.Equals(obj);
//    }

//    public override int GetHashCode()
//    {
//        return base.GetHashCode();
//    }

//    public byte[] Serialize()
//    {
//        throw new NotImplementedException();
//    }

//    public override string? ToString()
//    {
//        return base.ToString();
//    }

//    public bool TryOrder(State state, AntState ant, [MaybeNullWhen(false)] out IOrderState order)
//    {
//        throw new NotImplementedException();
//    }
//}

// this should probably just be a setting on gun...
//public class MultiShotImmutableState 
//{
//    public readonly int shots;

//    internal MultiShotImmutableState Copy()
//    {
//        return this;
//    }
//}

public class EyeballMaxiousState : IStateHash
{ 
    public double eyeballsOffset = 150;

    private const int eyes = 4;

    public EyeballMaxiousState(double eyeballsOffset, IReadOnlyList<Vector128<double>> eyePositions)
    {
        this.eyeballsOffset = eyeballsOffset;
        EyePositions = eyePositions ?? throw new ArgumentNullException(nameof(eyePositions));
    }

    public IReadOnlyList<Vector128<double>> EyePositions { get; private set; }

    public static void UpdateEyePositions(EyeballMaxiousState self, Map2<RockState> map, AntState ant) 
    {
        var list = new List<Vector128<double>>();
        for (double f = 0; f < 1; f += 1.0 / eyes)
        {
            var target = Avx.Add( ant.position, Vector128.Create(self.eyeballsOffset * Math.Sin(2 * Math.PI * f), self.eyeballsOffset * Math.Cos(2 * Math.PI * f)));
            map.CanReach(ant.position, target, out var reached);
            list.Add(reached);
        }

        self.EyePositions = list;
    }

    internal EyeballMaxiousState Copy()
    {
        return new EyeballMaxiousState(eyeballsOffset, EyePositions.ToArray());
    }

    //public override bool Equals(object? obj)
    //{
    //    return obj is EyeballMaxiousState state &&
    //           eyeballsOffset == state.eyeballsOffset &&
    //           EyePositions.SetEqual(state.EyePositions);
    //}

    //public override int GetHashCode()
    //{
    //    return this.Hash();
    //    //return HashCode.Combine(eyeballsOffset, EyePositions.UncheckedSum(x=>x.GetHashCode()));
    //}

    public IEnumerable<(string, object?)> HashData()
    {
        yield return (nameof(eyeballsOffset), eyeballsOffset);
        yield return (nameof(EyePositions), EyePositions);
    }
}
