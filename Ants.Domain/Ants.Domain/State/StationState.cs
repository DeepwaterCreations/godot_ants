﻿using System.Runtime.Intrinsics;

namespace Ants.Domain.State
{
    public class StationState : IStateHash
    {
        public readonly Vector128<double> position;

        public StationState(Vector128<double> position)
        {
            this.position = position;
        }

        //public override bool Equals(object? obj)
        //{
        //    return obj is StationState state &&
        //           position.Equals(state.position);
        //}

        //public override int GetHashCode()
        //{
        //    return this.Hash();
        //}

        public IEnumerable<(string, object?)> HashData()
        {
            yield return (nameof(position), position);
        }
    }
}