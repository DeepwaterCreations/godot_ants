﻿
// this is actually just a marker
// it doesn't do anything
// would be cood to visually show how full they are
using Ants.Domain;
using Ants.Domain.Infrastructure;
using Ants.Domain.State;
using System.Runtime.CompilerServices;
using System.Runtime.Intrinsics;
using System.Runtime.Intrinsics.X86;

public class HoneyPotState : IStateHash
{
    public double lost;
    public int lastDamage;

    // immutable - no point in making an object for one property
    public readonly double storagePerHp;
    

    public HoneyPotState(double lost, int lastDamage, double storagePerHp)
    {
        this.lost = lost;
        this.lastDamage = lastDamage;
        this.storagePerHp = storagePerHp;
    }

    public IEnumerable<(string, object?)> HashData() {
        yield return (nameof(lost), lost);
        yield return (nameof(lastDamage), lastDamage);
        yield return (nameof(storagePerHp), storagePerHp);
    }
    //public override int GetHashCode() => this.Hash();
    //public override bool Equals(object? obj) { 
    //    return obj is HoneyPotState other && other.lost == lost && other.lastDamage == lastDamage;
    //}

    internal void Lost(double lost, int tick)
    {
        this.lost += lost;
        this.lastDamage = tick;
    }

    public ResourceState? Update(int tick, Guid antId, Vector128<double> antPosition)
    {
        if (lost > 0) {
            if (tick - lastDamage >= FramesPerSecond.framesPerSecond * 2.0 || lost > ResourceSpawnerState.startingResource) 
            {
                var gatherRate = SimulationSafeRandom.GetDouble(ResourceSpawnerState.minGatherRate, ResourceSpawnerState.maxGatherRate, Guid.Parse("{ADF6A892-E3F0-4312-8649-083EA853E605}"), tick, antId);
                var angle = SimulationSafeRandom.GetDouble(0, Math.Tau, Guid.Parse("{4A34733D-D49B-467D-B125-6910810B2281}"), tick, antId);
                var radius = SimulationSafeRandom.GetDouble(0, 40, Guid.Parse("{2423B90C-305D-40D2-8EA3-01F862BAB003}"), tick, antId);

                var (y, x) = Math.SinCos(angle);
                var resoucePosition = Avx.Add(antPosition, Vector128.Create(x * radius * 2.0, y * radius * 2.0));

                var res = new ResourceState(lost, new ResourceState.Immutable(gatherRate, resoucePosition, new ResourceState.Immutable.Id(tick, antId, false)));
                lost = 0;
                return res;
            }
        }
        return null;
    }

    internal HoneyPotState Copy()
    {
        return new HoneyPotState(lost, lastDamage, storagePerHp);
    }
}