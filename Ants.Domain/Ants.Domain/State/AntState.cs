﻿using Ants.Domain;
using Ants.Domain.Infrastructure;
using Ants.Domain.State;
using Microsoft.VisualBasic;
using Prototypist.Toolbox.Object;
using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Runtime.Intrinsics;
using System.Runtime.Intrinsics.X86;
using static BulletState.Immutable;

public interface IStateHash
{
    IEnumerable<(string, object?)> HashData();
}

public interface IReadOnlyStateHash : IStateHash
{
    int? ReadOnlyHash { get; set; }
}

public static class DebugPrint
{

    public static string ToDebugString(this IStateHash? o) => ToDebugString(o, 0, new Dictionary<object, int>());
    public static string ToDebugString(this object? o, int indent, Dictionary<object, int> alreadySeen)
    {

        if (o == null)
        {
            return "null" + Environment.NewLine;
        }

        if (alreadySeen.TryGetValue(o, out var id))
        {
            return "..." + id + System.Environment.NewLine;
        }
        id = o.Hash();
        alreadySeen.Add(o, id);

        if (o is Vector128<double> vect)
        {
            return "<" + vect.X() + ", " + vect.Y() + "> " + VectHash(vect) + Environment.NewLine;
        }

        if (o is ITuple tuple)
        {
            var res = @$"{o.GetType().Name} {id}" + Environment.NewLine;
            res += Indent(indent) + "{" + Environment.NewLine;
            for (int i = 0; i < tuple.Length; i++)
            {
                res += $"{Indent(indent + 1)}Item{i} = {ToDebugString(tuple[i], indent + 1, alreadySeen)}";
            }
            res += Indent(indent) + "}" + Environment.NewLine;
            return res;
        }

        if (o is IStateHash stateHash)
        {
            var res = @$"{o.GetType().Name} {id}" + Environment.NewLine;
            res += Indent(indent) + "{" + Environment.NewLine;
            foreach (var (key, value) in stateHash.HashData())
            {
                res += $"{Indent(indent + 1)}{key} = {ToDebugString(value, indent + 1, alreadySeen)}";
            }
            res += Indent(indent) + "}" + Environment.NewLine;
            return res;
        }

        var args = o.GetType().GetGenericArguments();
        if (args.Count() == 2)
        {
            var dictType = typeof(IEnumerable<>).MakeGenericType(new[] { typeof(KeyValuePair<,>).MakeGenericType(args) });
            if (o.GetType().IsAssignableTo(dictType))
            {
                dynamic dynamicO = (dynamic)o;
                return DebugDict(dynamicO, indent, alreadySeen);
            }
        }

        if (args.Count() == 1)
        {
            var listType = typeof(IEnumerable<>).MakeGenericType(args);
            if (o.GetType().IsAssignableTo(listType))
            {
                dynamic dynamicO = (dynamic)o;
                return DebugList(dynamicO, indent, alreadySeen);
            }
        }

        var elementType = o.GetType().GetElementType();
        if (elementType != null)
        {
            return DebugList((dynamic)o, indent, alreadySeen);
        }

        return (o.ToString() ?? "null") + " " + id + Environment.NewLine;
    }

    public static string Indent(int indent)
    {
        var res = "";
        for (int i = 0; i < indent; i++)
        {
            res += '\t';
        }
        return res;
    }

    public static string DebugDict<TKey, TValue>(IEnumerable<KeyValuePair<TKey, TValue>> dict, int indent, Dictionary<object, int> alreadySeen)
    {
        var res = "dict " + DictHash(dict) + Environment.NewLine;
        res += Indent(indent) + "{" + Environment.NewLine;
        var items = new List<(string, int)>();
        foreach (var item in dict)
        {
            items.Add((
                Indent(indent + 1) + "key" + " = " + item.Key.ToDebugString(indent + 1, alreadySeen) +
                Indent(indent + 1) + "value" + " = " + item.Value.ToDebugString(indent + 1, alreadySeen), alreadySeen[item.Key]));
        }
        foreach (var item in items.OrderBy(x => x.Item2))
        {
            res += item.Item1;
        }
        res += Indent(indent) + "}" + Environment.NewLine;
        return res;
    }

    // I shouldn't need this, but it seems I do
    public static int GetConsistantStringHashCode(string s)
    {
        unchecked
        {
            var res = 0;
            foreach (var c in s)
            {
                res += c;
            }
            return res;
        }
    }

    public static string DebugList<TValue>(IEnumerable<TValue> list, int indent, Dictionary<object, int> alreadySeen)
    {
        var res = "list " + ListHash(list) + Environment.NewLine;
        res += Indent(indent) + "{" + Environment.NewLine;
        var i = 0;
        foreach (var item in list)
        {
            res += Indent(indent + 1) + "item" + i + item.ToDebugString(indent + 1, alreadySeen);
        }
        res += Indent(indent) + "}" + Environment.NewLine;
        return res;
    }

    //public static int InitHash(this IReadOnlyStateHash stateHash)
    //{
    //    return stateHash.HashData().UncheckedSum(x => x.Item2.Hash(stateHash));
    //}

    public static int Hash(this IStateHash stateHash)
    {
        return stateHash.HashData().UncheckedSum(x => x.Item2.Hash());
    }

    // this needs to be a lot smart
    // about lists and hashsets and dicts
    // and vectors
    // and deep 
    //public static bool HashEquals<T>(this T self, object? other)
    //    where T:IStateHash
    //{
    //    if (other == null) {
    //        return self == null;
    //    }
    //    if (!other.SafeIs(out T otherStateHash)) {
    //        return false;
    //    }
    //    return self.HashData().Zip(otherStateHash.HashData(), (x, y) => x.Equals(y)).All(x => x);
    //}

    // these don't hash nicely
    private static int VectHash(this Vector128<double> vect)
    {
        unchecked
        {
            return (vect.X() + vect.Y()).GetHashCode();
        }
    }

    private static int Hash(this object? o)
    {
        if (o == null)
        {
            return 0;
        }

        if (o is Vector128<double> vect)
        {
            return VectHash(vect);
        }

        if (o is ITuple tuple)
        {
            unchecked
            {
                var res = 0;
                for (int i = 0; i < tuple.Length; i++)
                {
                    res += tuple[i].Hash();
                }
                return res;
            }
        }

        if (o is IReadOnlyStateHash readOnlyStateHash)
        {
            if (readOnlyStateHash.ReadOnlyHash != null)
            {
                return readOnlyStateHash.ReadOnlyHash.Value;
            }
            else {
                var value = readOnlyStateHash.HashData().UncheckedSum(x => x.Item2.Hash());
                readOnlyStateHash.ReadOnlyHash = value;
                return value;
            }
        }

        if (o is IStateHash stateHash)
        {
            return stateHash.HashData().UncheckedSum(x => x.Item2.Hash());
        }

        var args = o.GetType().GetGenericArguments();
        if (args.Count() == 2)
        {
            var dictType = typeof(IEnumerable<>).MakeGenericType(new[] { typeof(KeyValuePair<,>).MakeGenericType(args) });
            if (o.GetType().IsAssignableTo(dictType))
            {
                dynamic dynamicO = (dynamic)o;
                return DictHash(dynamicO);
            }
        }

        if (args.Count() == 1)
        {
            var listType = typeof(IEnumerable<>).MakeGenericType(args);
            if (o.GetType().IsAssignableTo(listType))
            {
                dynamic dynamicO = (dynamic)o;
                return ListHash(dynamicO);
            }
        }

        var elementType = o.GetType().GetElementType();
        if (elementType != null)
        {
            return ListHash((dynamic)o);
        }

        return o.GetHashCode();
    }

    public static int DictHash<TKey, TValue>(this IEnumerable<KeyValuePair<TKey, TValue>> dict)
    {
        unchecked
        {
            var res = 0;
            foreach (var item in dict)
            {
                res += item.Key.Hash();
                res += item.Value.Hash();
            }
            return res;
        }
    }

    public static int ListHash<TValue>(this IEnumerable<TValue> list)
    {
        unchecked
        {
            var res = 0;
            foreach (var item in list)
            {
                res += item.Hash();
            }
            return res;
        }
    }
}



public class AntState : PhysicsSubjectState2, IStateHash, IBlocksShots
{
    public const int warmUpFor = 180;

    public double hp;
    public IOrderState? activeOrder;
    private int stun = 0; // in ticks
    public Queue<IPendingOrderState> pendingOrders = new Queue<IPendingOrderState>();
    public int boostFor = 0;
    public double boostAmount = 0;

    //public bool getsOutOfTheWay;
    //public bool takeDamageAsLife;

    // immutable 
    public class Immutable : IStateHash
    {
        public readonly Guid id;
        public readonly int side;// bool? - probs not
        public readonly double maxHp = 30;
        public readonly double lineOfSight = 600;
        public readonly double lineOfSightDetection = 300;
        public readonly double lineOfSightFar = 1200;

        public readonly Dictionary<(Key key, int pageIndex), AntAction> actions;
        public bool stealth;

        public readonly Pathing.Mode mudMode;
        // a lot of stuff here actually dirives from type
        public readonly AntType type;

        public Immutable(Guid id, int side, double maxHp, double lineOfSightFar, double lineOfSight, double lineOfSightDetection, Dictionary<(Key key, int pageIndex), AntAction> actions, Pathing.Mode mudMode, AntType type)
        {
            this.id = id;
            this.side = side;
            this.maxHp = maxHp;
            this.lineOfSightFar = lineOfSightFar;
            this.lineOfSight = lineOfSight;
            this.lineOfSightDetection = lineOfSightDetection;
            this.actions = actions ?? throw new ArgumentNullException(nameof(actions));
            this.mudMode = mudMode;
            this.type = type;
        }

        public IEnumerable<(string, object?)> HashData()
        {
            yield return (nameof(id), id);
            yield return (nameof(side), side);
            yield return (nameof(maxHp), maxHp);
            yield return (nameof(lineOfSight), lineOfSight);
            yield return (nameof(lineOfSightDetection), lineOfSightDetection);
            yield return (nameof(lineOfSightFar), lineOfSightFar);
            yield return (nameof(actions), actions);
            yield return (nameof(actions), actions);
            yield return (nameof(mudMode), mudMode);
            yield return (nameof(type), type);
            yield return (nameof(stealth), stealth);
        }

        //        public string DebugStateHash(int indent) =>
        //@$"{nameof(Immutable)} {GetHashCode()}
        //{IStateHash.Indent(indent + 1)}{nameof(id)} = {id} {id.GetHashCode()}
        //{IStateHash.Indent(indent + 1)}{nameof(side)} = {side}
        //{IStateHash.Indent(indent + 1)}{nameof(maxHp)} = {(int)maxHp}
        //{IStateHash.Indent(indent + 1)}{nameof(lineOfSight)} = {(int)lineOfSight}
        //{IStateHash.Indent(indent + 1)}{nameof(keyClickActions)} = {keyClickActions.ToDebugString(indent)}
        //{IStateHash.Indent(indent + 1)}{nameof(keyActions)} = {keyActions.ToDebugString(indent)}
        //{IStateHash.Indent(indent + 1)}{nameof(defultAction)} = {defultAction.DebugStateHash(indent)}
        //";

        //public override int GetHashCode() => this.Hash();
    }

    // sub states
    public GunState? gun { get; init; }
    public LegsState? legs { get; init; }
    public SprintState? sprint { get; init; }
    public BaseShieldState? shield { get; init; }
    public EyeballMaxiousState? eyeballMaxious { get; init; }
    public UnpackerState? unpacker { get; init; }
    public StationTrailState? stationTrail { get; init; }
    public HealState? heal { get; init; }
    public GrenadeLauncherState? grenadeLauncher { get; init; }
    public ResourceGathererState? resouceGatherer { get; init; }
    public SmokePlacerState? smokePlacerState { get; init; }
    public HoneyPotState? honeyPotState { get; init; }
    public ShockwaverState? shockwaverState { get; init; }
    public HealGunState? healGun { get; init; }
    public ArtyState? artyState { get; init; }
    public ZapperState? zapperState { get; init; }
    public ArcGunState? arcGun { get; init; }
    public ZapperGunState? zapperGun { get; init; }

    public readonly Immutable immutable;

    // TODO other components get states too, even if they are empty

    // ui only 
    // I need to think about how this is going to work
    // I think we read these for UI effects 
    // like sparks and explosion sounds
    //private List<(int, double)> damageTakenRecently = new List<(int, double)>();

    public AntState(
        double hp,
        Immutable immutable,
        Vector128<double> position,
        double radius) : this(hp, null, new Queue<IPendingOrderState>(), immutable, /*new List<(int, double)>(),*/ position, Vector128.Create(0.0, 0.0)/*, true*/, 0, new ImmunatblePhysicsState(radius), AntState.warmUpFor, 1)
    {
    }

    private AntState(
        double hp,
        IOrderState? activeOrder,
        Queue<IPendingOrderState> pendingOrders,
        Immutable immutable,
        //List<(int, double)> damageTakenRecently,
        Vector128<double> position,
        Vector128<double> velocity,
        int stun,
        ImmunatblePhysicsState physicsImmunatble,
        int boostFor,
        double boostAmount) : base(physicsImmunatble, position, velocity)
    {
        this.hp = hp;
        this.activeOrder = activeOrder;
        this.pendingOrders = pendingOrders ?? throw new ArgumentNullException(nameof(pendingOrders));
        this.immutable = immutable ?? throw new ArgumentNullException(nameof(immutable));
        //this.damageTakenRecently = damageTakenRecently ?? throw new ArgumentNullException(nameof(damageTakenRecently));
        //this.takeDamageAsLife = takeDamageAsLife;
        this.stun = stun;
        this.boostAmount = boostAmount;
        this.boostFor = boostFor;
    }

    public IEnumerable<Vector128<double>> SeesFrom()
    {
        return eyeballMaxious?.EyePositions ?? new[] { position };
    }

    public IReadOnlyList<Vector128<double>> PointsToSee()
    {
        return PointsToSee(position, physicsImmunatble.radius);
    }

    public static IReadOnlyList<Vector128<double>> PointsToSee(Vector128<double> position, double radius)
    {
        var res = new List<Vector128<double>>();
        for (int i = 0; i < 6; i++)
        {
            res.Add(
                Avx.Add(
                    position,
                    Avx.Multiply(
                        Vector128.Create(1.0, 0.0),
                        radius.AsVector())
                    .Rotated(Math.PI * 2 * i / 6.0)));
        }
        return res;
    }

    internal void Heal(HealBulletState healBullet)
    {
        hp = Math.Min(immutable.maxHp, hp + healBullet.immutable.startingHealing);
    }

    // returns true if the ant was killed
    public (bool, double) Hit(
        double damage, 
        double pierce, 
        Vector128<double> velocity, 
        double knockBack, 
        int stun,
        int tick, 
        SimulationState state)
    {

        // I'm thinking glance is out - it just make things more complex
        // the argument for it is it 
        //double glancing = Wall.Glance(bullet.GlobalPosition, bullet.startedAt, GlobalPosition);
        //damage *= glancing;

        damage = Damage(damage, velocity.NormalizedCopy(), pierce, tick);

        if (legs != null && legs.IsMoving())
        {
            ApplyForce(velocity.ToLength(knockBack));
        }
        Stun(stun);



        if (honeyPotState != null)
        {
            var hpLost = Math.Min(damage, hp);
            var hpStorageLost = hpLost * honeyPotState.storagePerHp;
            var money = state.money.GetMainMoney(this.immutable.side);
            var hpStorageBeforeHit = state.HoneyPotHpStorageSum(this.immutable.side);
            var loss = money * hpStorageLost / hpStorageBeforeHit;
            honeyPotState.Lost(loss, tick);
            state.money.AssertSpendMainMoney(loss, this.immutable.side);
        }

        //if (takeDamageAsLife)
        //{
        //    // you get healing at a discount thus the .5
        //    // var dollarPerHp = 0.5 * (AntFactory.Cost(this) / this.immutable.maxHp); 

        //    //var lifeLost = Math.Min(moneyState.money[immutable.side], damage * .5 * dollarPerHp);
        //    //moneyState.money[immutable.side] -= lifeLost;
        //    //control.AddToPool(lifeLost);
        //    hp -= (damage - (lifeLost/ dollarPerHp));
        //}
        //else { 
        hp -= damage;

        //}
        //damageTakenRecently.Add((tick, damage));
        return (hp <= 0, damage);
    }

    public double Damage(double damage, Vector128<double> directionOfMotion, double peirce, int tick)
    {
        if (shield != null)
        {
            var res = shield.ApplyArmor(position, directionOfMotion, peirce, damage, tick);
            //Log.WriteLine($"peirce: {peirce}, damage: {res}");
            return res;
        }
        return damage;
    }

    public void Update(int tick, SimulationState state)
    {
        if (stun > 0)
        {
            stun--;
        }

        if (boostFor > 0)
        {
            boostFor--;
        }

        gun?.Update(this);
        legs?.Update();
        sprint?.Update(this);
        shield?.Update(tick, this);
        unpacker?.Update();
        stationTrail?.Update(state, position);
        grenadeLauncher?.Update(this);
        shockwaverState?.Update(this);
        healGun?.Update(this);
        artyState?.Update(this);
        zapperState?.Update(this);
        arcGun?.Update(this);
        zapperGun?.Update(this);
    }

    public void ApplyFiction(Map2<MudState> mudMap)
    {
        double friction = CurrentFriction(mudMap);

        velocity = Avx.Multiply(velocity, friction.AsVector());
    }

    private const double friction = .7;

    private double CurrentFriction(Map2<MudState> mudMap)
    {
        var friction = AntState.friction;
        if (shield != null && shield.SafeIs(out ShieldState shieldState) && shieldState.TryFriction(friction, out double actualFriction))
        {
            friction = actualFriction;
        }

        if (immutable.mudMode == Pathing.Mode.SlowMud && TryModifyFriction(mudMap, friction, 2, out actualFriction))
        {
            friction = actualFriction;
        }
        // impassable mud isn't totally impassable
        // but is is really really slow
        else if (immutable.mudMode == Pathing.Mode.ImpassableMud && TryModifyFriction(mudMap, friction, 50, out actualFriction))
        {
            friction = actualFriction;
        }

        return friction;
    }

    public bool TryModifyFriction(Map2<MudState> mudMap, double friction, double by, out double actualFriction)
    {
        if (!mudMap.Hit(this).Any())
        {
            actualFriction = friction;
            return false;
        }

        // {0C8D3F28-3764-4548-B199-BA19133778DA}
        // V = a (friction/(1 - friction))
        // where:
        // - V is terminal velocity
        // - a is acceleration
        // - and friction is friction (to be multiplied by velocity as used here)

        var old = friction / (1 - friction); // 2.333 when friction = .7
        var target = old / by;   // 1.166
        // target = actualFriction/(1 - actualFriction)
        // target = 1 / ((1/actualFriction) - 1)
        // target * ((1/actualFriction) - 1) = 1
        // ((1/actualFriction) - 1) = 1/ target
        // (1/actualFriction) = (1/ target) + 1
        // 1 = actualFriction * ((1/ target) + 1)
        // 1 = actualFriction * ((1/ target) + 1)
        // 1 / ((1/ target) + 1) = actualFriction
        actualFriction = 1 / ((1 / target) + 1);
        // let's check
        // (1 / target) = .85719
        // (1 / target) + 1 = 1.85719
        // 1 / ((1 / target) + 1) = .5385
        // .5385/.4615
        // 1.166 ✅
        Debug.Assert(actualFriction >= 0);
        Debug.Assert(actualFriction <= 1);
        return true;
    }

    public bool CanMove()
    {
        return legs != null && !IsStuned();
    }

    public void RequestVelocity(Vector128<double> distance, Map2<MudState> mudMap)
    {
        if (CanMove())
        {
            var speed = legs.Speed;
            var friction = CurrentFriction(mudMap);

            // distance = (velocity + (force/mass))* friction
            // distance/ friction = velocity + (force/mass)
            // (distance/ friction) - velocity = force/mass
            // ((distance/ friction) - velocity) * mass = froce
            var neededForce = Avx.Multiply(Avx.Subtract(Avx.Divide(distance, friction.AsVector()), velocity), physicsImmunatble.mass.AsVector());

            var availableForceMagnitude = ForceForSpeed(speed, this);

            if (sprint != null && sprint.TryModifyForce(availableForceMagnitude, out double resultForceMagnitude))
            {
                availableForceMagnitude = resultForceMagnitude;
            }

            if (boostFor > 0)
            {
                availableForceMagnitude *= boostAmount;
            }

            var force = neededForce.Length() > availableForceMagnitude ?
                Avx.Multiply(neededForce.NormalizedCopy(), availableForceMagnitude.AsVector()) :
                neededForce;

            ApplyForce(force);

            if (force.Length() != 0)
            {
                legs.stoppedFor = 0;
            }
        }
    }

    public void RequestFullSpeed(Vector128<double> direction)
    {
        if (CanMove())
        {
            var availableForceMagnitude = ForceForSpeed(legs.Speed, this);

            ApplyForce(Avx.Multiply(direction.NormalizedCopy(), availableForceMagnitude.AsVector()));
            legs.stoppedFor = 0;
        }
    }

    public static double ForceForSpeed(double speed, AntState parent)
    {
#if DEBUG
        if (friction > 1)
        {
            throw new Exception($"{friction} should not be more than 1");
        }
#endif
        return speed * (1.0 - friction) * parent.physicsImmunatble.mass;
    }

    internal AntState Copy()
    {
        var res = new AntState(
            hp,
            activeOrder?.CopyIfMutable(),
            pendingOrders.ToQueue(),
            immutable,
            //damageTakenRecently.ToList(),
            position,
            velocity,
            //takeDamageAsLife
            stun,
            physicsImmunatble,
            boostFor,
            boostAmount
            )
        {
            gun = gun?.Copy(),
            legs = legs?.Copy(),
            sprint = sprint?.Copy(),
            shield = shield?.Copy(),
            eyeballMaxious = eyeballMaxious?.Copy(),
            unpacker = unpacker?.Copy(),
            stationTrail = stationTrail?.Copy(),
            grenadeLauncher = grenadeLauncher?.Copy(),
            resouceGatherer = resouceGatherer?.Copy(),
            smokePlacerState = smokePlacerState?.Copy(),
            honeyPotState = honeyPotState?.Copy(),
            shockwaverState = shockwaverState?.Copy(),
            healGun = healGun?.Copy(),
            artyState = artyState?.Copy(),
            zapperState = zapperState?.Copy(),
            arcGun = arcGun?.Copy(),
            zapperGun = zapperGun?.Copy(),
        };
        return res;
    }

    internal Guid SortHash()
    {
        return immutable.id;
    }

    //public override MapEntry ToMapEntry() => MapEntry.Make(immutable.id);

    //public override int GetHashCode()
    //{
    //    return this.Hash();
    //    //unchecked
    //    //{
    //    //    return
    //    //        GlobalPositionInt.GetHashCode()
    //    //        + immutable.GetHashCode()
    //    //        + VelocityInt.GetHashCode()
    //    //        + (int)hp
    //    //        + activeOrder?.GetHashCode() ?? 0
    //    //        + pendingOrders.Select(x => x.GetHashCode()).UncheckedSum()
    //    //        + Gun?.GetHashCode() ?? 0
    //    //        + legs?.GetHashCode() ?? 0
    //    //        + sprint?.GetHashCode() ?? 0
    //    //        + shield?.GetHashCode() ?? 0
    //    //        + eyeballMaxious?.GetHashCode() ?? 0
    //    //        + unpacker?.GetHashCode() ?? 0
    //    //        + damageTakenRecently.Select(x => x.Item1 + (int)x.Item2).UncheckedSum();
    //    //}
    //}


    internal bool IsStuned()
    {
        return stun != 0;
    }

    internal int Stun(int newStun) {
        stun = Math.Max(stun, newStun);
        return stun;
    }
    public override IEnumerable<(string, object?)> HashData()
    {
        foreach (var item in base.HashData())
        {
            yield return item;
        }
        yield return (nameof(immutable), immutable);
        yield return (nameof(hp), hp);
        yield return (nameof(activeOrder), activeOrder);
        yield return (nameof(pendingOrders), pendingOrders);
        yield return (nameof(gun), gun);
        yield return (nameof(legs), legs);
        yield return (nameof(sprint), sprint);
        yield return (nameof(shield), shield);
        yield return (nameof(eyeballMaxious), eyeballMaxious);
        yield return (nameof(unpacker), unpacker);
        //yield return (nameof(takeDamageAsLife), takeDamageAsLife);
        yield return (nameof(heal), heal);
        yield return (nameof(stun), stun);
        yield return (nameof(physicsImmunatble), physicsImmunatble);
        yield return (nameof(boostAmount), boostAmount);
        yield return (nameof(boostFor), boostFor);
        yield return (nameof(resouceGatherer), resouceGatherer);
        yield return (nameof(smokePlacerState), smokePlacerState);
        yield return (nameof(honeyPotState), honeyPotState);
        yield return (nameof(shockwaverState), shockwaverState);
        yield return (nameof(healGun), healGun);
        yield return (nameof(artyState), artyState);
        yield return (nameof(zapperState), zapperState);
        yield return (nameof(arcGun), arcGun);
        yield return (nameof(zapperGun), zapperGun);
        //yield return (nameof(damageTakenRecently), damageTakenRecently);
    }

}
