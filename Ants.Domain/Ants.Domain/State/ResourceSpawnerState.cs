﻿using Ants.Domain.Infrastructure;
using System.Runtime.Intrinsics;
using System.Runtime.Intrinsics.X86;
using System.Security.Cryptography.X509Certificates;

namespace Ants.Domain.State
{
    // TODO actually this is immuatable right?
    public class ResourceSpawnerState
    {
        public static readonly double maxRadius = 400;
        public static readonly double maxGatherRate = .25;
        public static readonly double minGatherRate = .01;
        //public readonly double maxResource = 400;
        //public readonly double minResource = 400;
        public readonly Vector128<double> position;
        public readonly Guid id;//

        public const double startingResource = 50;

        public ResourceSpawnerState(Vector128<double> position, Guid id)
        {
            this.position = position;
            this.id = id;
        }

        public ResourceState? Update(int tick) {
            if (tick % 100 != 0) { 
                return null;
            }

            var angle = SimulationSafeRandom.GetDouble(0,Math.Tau, Guid.Parse("{3D0F6E9A-40C4-4178-B963-BCCFAF25B2BE}"), tick, id);
            var radius = SimulationSafeRandom.GetDouble(0, maxRadius, Guid.Parse("{39CBD23F-3C37-4DAB-8927-DE52BBE988B3}"), tick, id);
            var gatherRate = SimulationSafeRandom.GetDouble(minGatherRate, maxGatherRate, Guid.Parse("{506067DE-97D4-40C8-850C-19F5E91487A2}"), tick, id);
            //var resource = SimulationSafeRandom.GetDouble(minResource, maxResource, Guid.Parse("{BBE75E5E-01AD-4D67-AA95-11A4634F7368}"), tick, id);

            var (y,x) = Math.SinCos(angle);
            var resoucePosition = Avx.Add(position, Vector128.Create(x * radius, y * radius));

            return new ResourceState(startingResource, new ResourceState.Immutable(gatherRate, resoucePosition, new ResourceState.Immutable.Id(tick, id, false)));
        }
    }
}