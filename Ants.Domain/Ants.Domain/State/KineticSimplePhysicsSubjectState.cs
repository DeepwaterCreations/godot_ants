﻿using Ants.Domain.Infrastructure;
using System.Runtime.Intrinsics;

public abstract class KineticSimplePhysicsSubjectState<TKineticImmuntable> : PhysicsSubjectState<TKineticImmuntable>
    where TKineticImmuntable : KineticImmuntable
{
    //private Vector128<double> v;


    // velocity doesn't really need to be an int
    // just position...
    public GlobalPosionInt VelocityInt { get; set; }

    public Vector128<double> velocity
    {
        get => VelocityInt.ToGlobalVector(); 
    }

    public KineticSimplePhysicsSubjectState(TKineticImmuntable immuntable, GlobalPosionInt globalPosition, GlobalPosionInt velocity) : base(immuntable, globalPosition)
    {
        this.VelocityInt = velocity;
    }

    public void ApplyForce(GlobalPosionInt force) {

        var res = VelocityInt + (force / immutable.mass);

        VelocityInt = new GlobalPosionInt((int)Math.Round(res.X()), (int)Math.Round(res.Y()));
    }

}
