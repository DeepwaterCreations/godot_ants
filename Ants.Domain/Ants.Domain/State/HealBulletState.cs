﻿using Ants.Domain.Infrastructure;
using System.Runtime.Intrinsics;
using System.Runtime.Intrinsics.X86;

public class HealBulletState : PhysicsSubjectState2
{


    // immutable 
    public class Immutable : IStateHash, IComparable<HealBulletState.Immutable>
    {
        public class Id : IStateHash, IComparable<HealBulletState.Immutable.Id>
        {
            /// <summary>
            /// used to unique identify the shot
            /// we hold an id instead of a reference because references are constantly throw away and rebuilt
            /// </summary>
            public readonly Guid shooterId;
            /// <summary>
            /// used to unique identify the shot
            /// </summary>  
            public readonly int shotOnFrame;
            /// <summary>
            /// used to unique identify the shot
            /// 0 if this is the first shot the shooter shot on that frame
            /// 1 if this is the second and so on
            /// </summary>
            public readonly int shotIndex;


            public Id(Guid shooterId, int shotOnFrame, int shotIndex)
            {
                this.shooterId = shooterId;
                this.shotOnFrame = shotOnFrame;
                this.shotIndex = shotIndex;
            }

            public int CompareTo(Id? other)
            {
                if (other == null) return 1;

                if (shooterId.CompareTo(other.shooterId) != 0)
                {
                    return shooterId.CompareTo(other.shooterId);
                }
                if (shotOnFrame.CompareTo(other.shotOnFrame) != 0)
                {
                    return shotOnFrame.CompareTo(other.shotOnFrame);
                }

                if (shotIndex.CompareTo(other.shotIndex) != 0)
                {
                    return shotIndex.CompareTo(other.shotIndex);
                }
                return 0;
            }

            public override bool Equals(object? obj)
            {
                return obj is Id id &&
                       shooterId.Equals(id.shooterId) &&
                       shotOnFrame == id.shotOnFrame &&
                       shotIndex == id.shotIndex
                       ;
            }

            public override int GetHashCode()
            {
                return this.Hash();
                //return HashCode.Combine(shooterId, shotOnFrame, shotIndex);
            }

            public IEnumerable<(string, object?)> HashData()
            {
                yield return (nameof(shooterId), shooterId);
                yield return (nameof(shotOnFrame), shotOnFrame);
                yield return (nameof(shotIndex), shotIndex);
            }

            public override string ToString()
            {
                return $"({shooterId}, {shotOnFrame}, {shotIndex})";
            }
        }

        public readonly Id id;

        public readonly double runFor;
        public readonly double speed;
        public readonly Vector128<double> direction;
        public readonly int side;

        public readonly Vector128<double> startedAt;
        public readonly double startingHealing;

        public readonly double ignoreCoverFor;

        public Immutable(Guid shooterId, int frame, int shooterShotOnFrame,
            double runFor, double speed, Vector128<double> direction, int side,
            Vector128<double> startedAt, 
            double healing,
            double ignoreCoverFor)
        {
            this.id = new Id(shooterId, frame, shooterShotOnFrame);
            this.runFor = runFor;
            this.speed = speed;
            this.direction = direction;
            this.side = side;
            this.startedAt = startedAt;
            this.startingHealing = healing;
            this.ignoreCoverFor = ignoreCoverFor;
        }

        //public int CompareTo(Immutable? other)
        //{
        //    if (other == null) return 1;

        //    return id.CompareTo(other.id);
        //}

        //public override bool Equals(object? obj)
        //{
        //    return obj is Immutable immutable &&
        //           id.Equals(immutable.id) &&
        //           runFor == immutable.runFor &&
        //           speed == immutable.speed &&
        //           direction.Equals(immutable.direction) &&
        //           side == immutable.side &&
        //           startedAt.Equals(immutable.startedAt) &&
        //           startingHealing == immutable.startingHealing &&
        //           ignoreCoverFor == immutable.ignoreCoverFor;
        //}

        //public override int GetHashCode()
        //{
        //    return this.Hash();
        //}

        public IEnumerable<(string, object?)> HashData()
        {
            yield return (nameof(id), id);
            yield return (nameof(runFor), runFor);
            yield return (nameof(speed), speed);
            yield return (nameof(direction), direction);
            yield return (nameof(side), side);
            yield return (nameof(startedAt), startedAt);
            yield return (nameof(startingHealing), startingHealing);
            yield return (nameof(ignoreCoverFor), ignoreCoverFor);
        }

        public int CompareTo(Immutable? other)
        {
            if (other == null) return 1;

            return id.CompareTo(other.id);
        }
    }
    public readonly Immutable immutable;
    public readonly HashSet<Vector128<double>> coverHit;

    private HealBulletState(Vector128<double> position, Immutable immutable, ImmunatblePhysicsState physicsImmunatble, HashSet<Vector128<double>> coverHit) : base(physicsImmunatble, position, Avx.Multiply(immutable.direction, immutable.speed.AsVector()))
    {
        this.immutable = immutable;
        this.coverHit = coverHit;
    }

    public static HealBulletState MakeBullet(Vector128<double> position, Immutable immutable)
    {

        return new HealBulletState(
            position,
            immutable,
            new ImmunatblePhysicsState(1),// TODO is this always "1"? To really support, we'll have to update who to shoot to support avoid by
            new HashSet<Vector128<double>>()
            );
    }

    //return true if killed
    public bool Update()
    {
        return Avx.Subtract(position, immutable.startedAt).Length() > immutable.runFor;
    }

    internal HealBulletState Copy()
    {
        return new HealBulletState(position, immutable, physicsImmunatble, coverHit.ToHashSet());
    }

    //public override bool Equals(object? obj)
    //{
    //    return obj is HealBulletState state &&
    //           position.Equals(state.position) &&
    //           velocity.Equals(state.velocity) &&
    //           EqualityComparer<Immutable>.Default.Equals(immutable, state.immutable);
    //}

    //public override int GetHashCode()
    //{
    //    return this.Hash();
    //}

    public override IEnumerable<(string, object?)> HashData()
    {
        foreach (var item in base.HashData())
        {
            yield return item;
        }
        yield return (nameof(immutable), immutable);
        yield return (nameof(coverHit), coverHit);
    }

    //public (Guid shooterId, int shotOnFrame, int shotIndex) GetThing()
    //{
    //    return (immutable.shooterId, immutable.shotOnFrame, immutable.shotIndex);
    //}
}
