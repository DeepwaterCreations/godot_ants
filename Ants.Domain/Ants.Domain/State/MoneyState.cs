﻿using Ants.Domain;
using Ants.Domain.Infrastructure;
using System.Diagnostics;

public class MoneyState : IStateHash
{
    private double[] mainMoney;
    private double[] gatherMoney;

    public MoneyState(double[] mainMoney, double[] gatherMoney)
    {
        this.mainMoney = mainMoney ?? throw new ArgumentNullException(nameof(mainMoney));
        this.gatherMoney = gatherMoney ?? throw new ArgumentNullException(nameof(gatherMoney));
    }

    // TODO
    // future me, here is a land mind
    // control is currently populated by the number of players who joined the lobby
    // there could be ants on the maps for like player 4
    // but only like two players in the game
    public static bool logedIt = false;
    private void CheckPlayerMain(int player)
    {
        if (player < mainMoney.Length)
        {
            return;
        }

        if (!logedIt)
        {
            Log.WriteLine($"player {player} could not recieve money");
            logedIt = true;
        }
    }
    private void CheckPlayerGatherer(int player)
    {
        if (player < gatherMoney.Length)
        {
            return;
        }

        if (!logedIt)
        {
            Log.WriteLine($"player {player} could not recieve money");
            logedIt = true;
        }
    }


    public double GetMainMoney(int player) => mainMoney[player];

    public double GainMainMoney(double toGain, int player, double max)
    {
        CheckPlayerMain(player);
        if (max < mainMoney[player] + toGain)
        {
            var gained = max - mainMoney[player];
            mainMoney[player] = max;
            return gained;
        }
        mainMoney[player] += toGain;
        return toGain;
    }

    public bool TrySpendMainMoney(double toSpend, int player)
    {
        CheckPlayerMain(player);
        if (mainMoney[player] >= toSpend)
        {
            mainMoney[player] -= toSpend;
            return true;
        }
        return false;
    }

    public void AssertSpendMainMoney(double toSpend, int player)
    {
        var spent = TrySpendMainMoney(toSpend, player);
        Debug.Assert(spent);
    }

    public double GetGathererMoney(int player) => gatherMoney[player];

    public void GainGathererMoney(double toGain, int player)
    {
        CheckPlayerGatherer(player);
        gatherMoney[player] += toGain;
    }

    public bool TrySpendGathererMoney(double toSpend, int player)
    {
        CheckPlayerGatherer(player);
        if (gatherMoney[player] >= toSpend)
        {
            gatherMoney[player] -= toSpend;
            return true;
        }
        return false;
    }

    public void AssertSpendGathererMoney(double toSpend, int player)
    {
        var spent = TrySpendGathererMoney(toSpend, player);
        Debug.Assert(spent);
    }

    //public override bool Equals(object? obj)
    //{
    //    return obj is MoneyState state &&
    //           mainMoney.SequenceEqual(state.mainMoney);
    //}

    //public override int GetHashCode()
    //{
    //    return this.Hash();
    //    //return (int)money.UncheckedSum();
    //}

    public IEnumerable<(string, object?)> HashData()
    {
        yield return (nameof(mainMoney), mainMoney);
        yield return (nameof(gatherMoney), gatherMoney);
    }

    public void Update()
    {
        if (false)
        {
            for (var i = 0; i < gatherMoney.Length; i++)
            {
                gatherMoney[i] += 100 / ((double)FramesPerSecond.framesPerSecond * 120);
            }
        }
    }

    internal MoneyState Copy()
    {
        return new MoneyState(mainMoney.ToArray(), gatherMoney.ToArray());
    }
}
