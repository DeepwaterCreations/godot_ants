﻿using Ants.Domain.Infrastructure;
using System.Runtime.Intrinsics;
using System.Runtime.Intrinsics.X86;

public class HealGunState : IStateHash
{
    /// <summary>
    /// in ticks
    /// </summary>
    public uint reloadDue;

    // immutable 
    public class Immutable : IStateHash
    {
        public readonly double healing = 3;
        public readonly double shotSpeed = 10;
        public readonly double scatterAngleRad = 0.1;
        public readonly uint relaodTimeTick = 30;
        public readonly bool canMoveAndShoot = false;
        public readonly bool canMoveAndRelaod = false;
        public readonly double range = 800;
        public readonly double rangeScatter = 50;
        public readonly int setUpTimeTicks = 6;
        public readonly int shots = 1;
        public readonly double ignoreCoverFor = 300;
        public readonly double ignoreCoverForScatter = 100;

        public Immutable(
            double healing,
            double shotSpeed,
            double scatterAngleRad,
            uint relaodTimeTick,
            bool canMoveAndShoot,
            bool canMoveAndRelaod,
            double range,
            double rangeScatter,
            int setUpTimeTicks,
            int shots,
            double ignoreCoverFor,
            double ignoreCoverForScatter)
        {
            this.healing = healing;
            this.shotSpeed = shotSpeed;
            this.scatterAngleRad = scatterAngleRad;
            this.relaodTimeTick = relaodTimeTick;
            this.canMoveAndShoot = canMoveAndShoot;
            this.canMoveAndRelaod = canMoveAndRelaod;
            this.range = range;
            this.rangeScatter = rangeScatter;
            this.setUpTimeTicks = setUpTimeTicks;
            this.shots = shots;
            this.ignoreCoverFor = ignoreCoverFor;
            this.ignoreCoverForScatter = ignoreCoverForScatter;
        }

        //public override bool Equals(object? obj)
        //{
        //    return obj is Immutable immutable &&
        //           healing == immutable.healing &&
        //           shotSpeed == immutable.shotSpeed &&
        //           scatterAngleRad == immutable.scatterAngleRad &&
        //           relaodTimeTick == immutable.relaodTimeTick &&
        //           canMoveAndShoot == immutable.canMoveAndShoot &&
        //           canMoveAndRelaod == immutable.canMoveAndRelaod &&
        //           range == immutable.range &&
        //           rangeScatter == immutable.rangeScatter &&
        //           setUpTimeTicks == immutable.setUpTimeTicks &&
        //           shots == immutable.shots;
        //}

        //public override int GetHashCode()
        //{
        //    return this.Hash();
        //}

        public IEnumerable<(string, object?)> HashData()
        {
            yield return (nameof(healing), healing);
            yield return (nameof(shotSpeed), shotSpeed);
            yield return (nameof(scatterAngleRad), scatterAngleRad);
            yield return (nameof(relaodTimeTick), relaodTimeTick);
            yield return (nameof(canMoveAndShoot), canMoveAndShoot);
            yield return (nameof(range), range);
            yield return (nameof(rangeScatter), rangeScatter);
            yield return (nameof(setUpTimeTicks), setUpTimeTicks);
            yield return (nameof(shots), shots);
        }
    }
    public readonly Immutable immutable;

    // sub-states
    //public readonly MultiShotImmutableState multiShot;

    public HealGunState(Immutable immutable) : this(AntState.warmUpFor, immutable) { }

    public HealGunState(uint reloadDue, Immutable immutable/* MultiShotImmutableState multiShot*/)
    {
        this.reloadDue = reloadDue;
        this.immutable = immutable ?? throw new ArgumentNullException(nameof(immutable));
        //this.multiShot = multiShot ?? throw new ArgumentNullException(nameof(multiShot));
    }

    public void Update(AntState ant)
    {
        if ((immutable.canMoveAndRelaod || ant.legs == null || !ant.legs.IsMoving()) && reloadDue != 0 && !ant.IsStuned())
        {
            reloadDue--;
        }
    }


    public static double OddsToPassCoverAtRange(double targetRange, double ignoreCoverFor, double ignoreCoverForScatter)
    {
        return 1.0 - (1.0 / (1 + Math.Pow(Math.E, -((targetRange - ignoreCoverFor) / ignoreCoverForScatter))));
    }

    public double OddsToReach(double targetRange)
    {
        return OddsToReach(targetRange, immutable.range, immutable.rangeScatter);
    }

    public static double OddsToReach(double targetRange, double range, double rangeScatter)
    {
        return 1.0 - 1.0 / (1 + Math.Pow(Math.E, -((targetRange - range) / rangeScatter)));
    }

    // dup
    // {7BF2FFDA-B876-4929-82DA-D68C4374ADD7}
    public double GetRunFor(Guid shooerId, int frame, int shot)
    {

        // y = 1 - 1/(1+ e^(-((x-range)/rangeScatter)))
        // y - 1 = -1/(1+ e^(-(x-range)/rangeScatter))
        // 1 - y = 1/(1+ e^(-(x-range)/rangeScatter))
        // 1/(1-y) = 1+ e^(-(x-range)/rangeScatter)
        // (1/(1-y)) - 1 = e^(-(x-range)/rangeScatter)
        // ln(1/(1-y) - 1) = -(x-range)/rangeScatter
        // ln(1/(1-y) - 1)*rangeScatter = -(x-range)/
        // -ln(1/(1-y) - 1)*rangeScatter = x - range
        // -ln(1/(1-y) - 1)*rangeScatter + range = x

        var rand = SimulationSafeRandom.GetDouble(0, 1, shooerId, frame, shot, Guid.Parse("{D3545A5D-A33E-4630-AC9C-158FBD84695B}"));//nameof(GetRunFor)
        return -((double)Math.Log((1.0 / rand) - 1.0) * immutable.rangeScatter) + immutable.range;
        //return Math.Min(targetDistance + (shotSpeed/60.0), -((double)Math.Log((1.0 / (1.0 - TestSafeGD.RandfRange(0,1)))- 1.0) * rangeScatter) + Math.Min(targetDistance, range));
    }

    // this just copies that ^
    // dup
    // {D14503FA-4B89-4788-8A6C-35CEA6F976CF}
    private double GetIgnoreCoverFor(Guid shooerId, int frame, int shot)
    {
        var rand = SimulationSafeRandom.GetDouble(0, 1, shooerId, frame, shot, Guid.Parse("{C10435BE-F770-4B62-914A-1D10540A1A78}"));//nameof(GetRunFor)
        return -((double)Math.Log((1.0 / rand) - 1.0) * immutable.ignoreCoverForScatter) + immutable.ignoreCoverFor;
    }

    public Vector128<double> GetShotDirection(Vector128<double> from, Vector128<double> shootAt, Guid shooterId, int frame, int shot, double addScatter)
    {
        var normalizedDirection = Avx.Subtract(shootAt, from).NormalizedCopy();

        // TODO more real scatter
        // bell curve?
        // probably naw, that would make my who is best to shoot at math harder
        var rand = SimulationSafeRandom.GetDouble(-.5 + addScatter, .5 + addScatter, shooterId, frame, shot, Guid.Parse("{2DCCEB74-F900-4AE0-AA9E-311D20A68A9C}"));//nameof(GetShotDirection
        normalizedDirection = normalizedDirection.Rotated(rand * immutable.scatterAngleRad);
        return normalizedDirection;
    }


    internal HealBulletState[] Shoot(Vector128<double> from, Vector128<double> at, AntState shooter, int frame, ShotIndexTracker shotIndexTracker, double addScatter)
    {
        shooter.healGun!.reloadDue = shooter.healGun.immutable.relaodTimeTick;
        //TestSafeLog.WriteLine("shooter.Gun.reloadDue" + shooter.Gun.reloadDue);

        var res = new HealBulletState[immutable.shots];

        for (int i = 0; i < res.Length; i++)
        {
            res[i] = HealBulletState.MakeBullet(
            from,
            new HealBulletState.Immutable(
                shooter.immutable.id,
                frame,
                shotIndexTracker.Get(shooter.immutable.id),
                shooter.healGun.GetRunFor(shooter.immutable.id, frame, i),
                shooter.healGun.immutable.shotSpeed,
                shooter.healGun.GetShotDirection(from, at, shooter.immutable.id, frame, i, addScatter),
                shooter.immutable.side,
                from,
                shooter.healGun.immutable.healing,
                shooter.healGun.GetIgnoreCoverFor(shooter.immutable.id, frame, i)));
        }
        return res;
    }

    internal HealGunState Copy()
    {
        return new HealGunState(reloadDue, immutable);
    }

    //public override bool Equals(object? obj)
    //{
    //    return obj is HealGunState state &&
    //           reloadDue == state.reloadDue &&
    //           EqualityComparer<Immutable>.Default.Equals(immutable, state.immutable);
    //}

    //public override int GetHashCode()
    //{
    //    return this.Hash();
    //}

    public IEnumerable<(string, object?)> HashData()
    {
        yield return (nameof(reloadDue), reloadDue);
        yield return (nameof(immutable), immutable);
    }
}
