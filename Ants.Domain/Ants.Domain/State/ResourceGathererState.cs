﻿using Ants.Domain.Infrastructure;
using Ants.Domain.State;
using Prototypist.Toolbox.Object;
using System.Runtime.Intrinsics;
using System.Runtime.Intrinsics.X86;

public class ResourceGathererState: IStateHash
{

    public const double range = 800;

    // this is really non-simulation state
    // it is calculated every frame
    // without it this is read only
    // would be nice to push it out somewhere
    // wouldn't have to copy this
    //public Vector128<double>? collectFrom;

    //public ResourceGathererState(Vector128<double>? collectFrom)
    //{
    //    this.collectFrom = collectFrom;
    //}

    public ResourceGathererState Copy() {
        return this;
        //return new ResourceGathererState(collectFrom);
    }

    //public override bool Equals(object? obj)
    //{
    //    return obj is ResourceGathererState state &&
    //           collectFrom.NullSafeEqual(state.collectFrom);
    //}

    //public override int GetHashCode()
    //{
    //    return this.Hash();
    //}

    public IEnumerable<(string, object?)> HashData()
    {
        yield break;
        //yield return (nameof(collectFrom), collectFrom);
    }

    internal ResourceGathered? Gether(AntState ant, List<ResourceState> resources, IReadOnlyList<(Vector128<double> globalPosition, double radius)> rocks, MoneyState money, double maxMoney)
    {
        if (ant.IsStuned()) 
        {
            //collectFrom = null;
            return null;
        }

        var gatherFrom = resources
            .Where(x => Avx.Subtract(x.immutable.position, ant.position).LengthSquared() < range * range)
            .Where(x => Pathing.IsAPathBetween(ant.position, x.immutable.position, rocks, 0))
            .Select(x => (resource: x, gatherRate: GatherRate(x, ant.position)))
            .OrderByDescending(x => x.gatherRate)
            .ThenBy(x => x.resource.immutable.id)
            .Take(1)
            .ToArray();

        if (!gatherFrom.Any())
        {

            //collectFrom = null;
            return null;

        }

        var target = gatherFrom[0];
        target.resource.available -= target.gatherRate;
        var gained = money.GainMainMoney(target.gatherRate, ant.immutable.side, maxMoney);
        //collectFrom = target.resource.immutable.position;
        return new ResourceGathered(ant.immutable.side, gained, target.resource.immutable.position, ant.position);

    }

    private double GatherRate(ResourceState x, Vector128<double> position)
    {
        var distance = Avx.Subtract(position, x.immutable.position).Length();
        var gatherRate = x.immutable.gatherRate * (range - distance) / range;
        if (x.available < gatherRate)
        {
            return x.available;
        }
        return gatherRate;
    }
}