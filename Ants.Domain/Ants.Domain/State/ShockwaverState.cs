﻿using Ants.Domain;
using System.Diagnostics.CodeAnalysis;
using System.Numerics;
using System.Runtime.Intrinsics;

public class ShockwaverState : IStateHash {

    public const int costToCast = FramesPerSecond.framesPerSecond * 30;
    public const int maxEnergy = costToCast * 2;
    public int energy;

    public ShockwaverState(int energy)
    {
        this.energy = energy;
    }

    public void Update(AntState self) {
        if (!self.IsStuned())
        {
            energy = Math.Min(energy + 1, maxEnergy);
        }
    }

    public ShockwaverState Copy()
    {
        return new ShockwaverState(energy);
    }


    public IEnumerable<(string, object?)> HashData()
    {
        yield return (nameof(energy), energy);
    }

    internal ShockwaveOrder Shockwave(Vector128<double> mousePosition)
    {
        return new ShockwaveOrder(mousePosition, Guid.NewGuid());
    }
}