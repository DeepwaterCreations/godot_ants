﻿using Ants.Domain.Infrastructure;
using System.Diagnostics;
using System.Runtime.Intrinsics;
using System.Runtime.Intrinsics.X86;

public class GrenadeState : IStateHash
{

    public readonly int timeToLive;
    public readonly Vector128<double> from;
    public readonly Vector128<double> velocity;


    public readonly double shot_runForDistance;
    public readonly double shot_speed;
    public readonly int shot_side;
    public readonly double shot_damage;

    public readonly double shot_ignoreCovoerFor;
    public readonly double shot_pierce;
    public readonly int shot_stun;
    public readonly double shot_knockBack;

    // if this starts to drift duplicate
    // just using BulletState.Immutable.Id because it happens to be the same and I'm lazy
    public readonly BulletState.Immutable.Id id;

    public GrenadeState(BulletState.Immutable.Id id, int timeToLive, Vector128<double> from, Vector128<double> velocity, double shot_runForDistance, double shot_speed, int shot_side, double shot_damage, double shot_ignoreCovoerFor, double shot_pierce, int shot_stun, double shot_knockBack)
    {
        this.id = id;
        this.timeToLive = timeToLive;
        this.from = from;
        this.velocity = velocity;
        this.shot_runForDistance = shot_runForDistance;
        this.shot_speed = shot_speed;
        this.shot_side = shot_side;
        this.shot_damage = shot_damage;
        this.shot_ignoreCovoerFor = shot_ignoreCovoerFor;
        this.shot_pierce = shot_pierce;
        this.shot_stun = shot_stun;
        this.shot_knockBack = shot_knockBack;
    }

    //public override bool Equals(object? obj)
    //{
    //    return obj is GrenadeState state &&
    //           id == state.id &&
    //           timeToLive == state.timeToLive &&
    //           from.Equals(state.from) &&
    //           velocity.Equals(state.velocity) &&
    //           shot_runForDistance == state.shot_runForDistance &&
    //           shot_speed == state.shot_speed &&
    //           shot_side == state.shot_side &&
    //           shot_damage == state.shot_damage &&
    //           shot_ignoreCovoerFor == state.shot_ignoreCovoerFor &&
    //           shot_endingPierce == state.shot_endingPierce &&
    //           shot_pierceAt200 == state.shot_pierceAt200 &&
    //           shot_stun == state.shot_stun;
    //}

    //public override int GetHashCode()
    //{
    //    return this.Hash();
    //}

    public Vector128<double> GetPosition(double frame) => Avx.Add(from, Avx.Multiply(velocity, (frame - id.shotOnFrame).AsVector()));
    public double GetZ(double frame) {
        var x = frame - id.shotOnFrame;
        var hightPointAt = timeToLive / 2.0;
        var scaleBy = 50 / (hightPointAt * hightPointAt);
        return scaleBy* ((hightPointAt* hightPointAt) - ((x - hightPointAt) * (x - hightPointAt)));
    }

    public IEnumerable<(string, object?)> HashData()
    {
        yield return (nameof(id), id);
        yield return (nameof(timeToLive), timeToLive);
        yield return (nameof(velocity), velocity);
        yield return (nameof(from), from);
        yield return (nameof(velocity), velocity);
        yield return (nameof(shot_runForDistance), shot_runForDistance);
        yield return (nameof(shot_speed), shot_speed);
        yield return (nameof(shot_side), shot_side);
        yield return (nameof(shot_damage), shot_damage);
        yield return (nameof(shot_ignoreCovoerFor), shot_ignoreCovoerFor);
        yield return (nameof(shot_pierce), shot_pierce);
        yield return (nameof(shot_stun), shot_stun);
        yield return (nameof(shot_knockBack), shot_knockBack);
    }

    internal bool Update(ShotIndexTracker shotIndexTracker, int frame, out List<BulletState> bullets)
    {
        bullets = new List<BulletState>();
        if (frame - id.shotOnFrame != timeToLive)
        {
            return false;
        }

        Debug.Assert(frame - id.shotOnFrame <= timeToLive);
        bullets = Explode(shotIndexTracker, frame, GetPosition(frame), id.shooterId, shot_runForDistance, shot_speed, shot_side, shot_damage, shot_ignoreCovoerFor, shot_pierce, shot_stun, shot_knockBack);
        return true;

    }

    public static List<BulletState> Explode(
        ShotIndexTracker shotIndexTracker, 
        int frame,
        Vector128<double> position,
        Guid shooterId,
        double runForDistance,
        double speed,
        int side,
        double damage,
        double ignoreCovoerFor,
        double pierce,
        int stun,
        double knockBack
        )
    {
        List<BulletState> bullets = new List<BulletState>();
        var by = Math.Tau / 18.0;
        for (double angle = 0; angle < Math.Tau; angle += by)
        {
            // soh cah toa
            var x = Math.Cos(angle);
            var y = Math.Sin(angle);

            bullets.Add(BulletState.MakeBullet(position,
                new BulletState.Immutable(
                    shooterId,
                    frame,
                    shotIndexTracker.Get(shooterId),
                    runForDistance,
                    speed,
                    Vector128.Create(x, y),
                    side,
                    position,
                    damage,
                    ignoreCovoerFor,
                    pierce,
                    stun,
                    true,
                    knockBack,
                    true)));
        }

        return bullets;
    }
}
