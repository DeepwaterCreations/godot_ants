﻿using Ants.Domain.Infrastructure;
using System.Runtime.Intrinsics;
using System.Runtime.Intrinsics.X86;

namespace Ants.Domain.State
{
    public class ShockwaveState : IStateHash
    {
        public class Id : IComparable<Id>, IStateHash
        {
            public readonly Guid shooterId;
            public readonly int tick;
            public readonly int indexOnFrame;

            public Id(Guid shooterId, int tick, int indexOnFrame)
            {
                this.shooterId = shooterId;
                this.tick = tick;
                this.indexOnFrame = indexOnFrame;
            }

            public int CompareTo(Id? other)
            {
                if (other == null) return 1;    

                var shooterIdCompare = shooterId.CompareTo(other.shooterId);
                if (shooterIdCompare == 0) {
                    return shooterIdCompare;
                }

                var tickCompare = tick.CompareTo(other.tick);
                if (tickCompare == 0)
                {
                    return tickCompare;
                }
                var indexOnFrameCompare = indexOnFrame.CompareTo(other.indexOnFrame);
                return indexOnFrameCompare;
            }

            //public override bool Equals(object? obj)
            //{
            //    return obj is Id id &&
            //           shooterId.Equals(id.shooterId) &&
            //           tick == id.tick &&
            //           indexOnFrame == id.indexOnFrame;
            //}

            //public override int GetHashCode()
            //{
            //    return this.Hash();
            //}

            public IEnumerable<(string, object?)> HashData()
            {
                yield return (nameof(shooterId), shooterId);
                yield return (nameof(tick), tick);
                yield return (nameof(indexOnFrame), indexOnFrame);
            }

        }

        internal const double Speed = 15;
        private readonly int createdOnTick;
        private readonly Vector128<double> startAt;
        public readonly Vector128<double> velocity;
        private readonly int timeToLive;
        public readonly Id id;

        public Vector128<double> CurrentPosition(double tick) => Avx.Add(Avx2.Multiply(velocity, (tick - createdOnTick).AsVector()), startAt);

        public ShockwaveState(int createdOnTick, Vector128<double> startAt, Vector128<double> velocity, int timeToLive, Id id)
        {
            this.createdOnTick = createdOnTick;
            this.startAt = startAt;
            this.velocity = velocity;
            this.timeToLive = timeToLive;
            this.id = id;
        }

        internal bool UpdateTrueWHenStillExists(int tick, Map2<RockState> rockMap)
        {
            if (tick - createdOnTick > timeToLive)
            {
                return false;
            }

            var currentPosition = CurrentPosition(tick);
            var hit = rockMap.Hit(currentPosition, 0);

            if (hit.Any())
            {
                return false;
            }

            return true;
        }

        public IEnumerable<(string, object?)> HashData()
        {
            yield return (nameof(createdOnTick), createdOnTick);
            yield return (nameof(startAt), startAt);
            yield return (nameof(velocity), velocity);
            yield return (nameof(timeToLive), timeToLive);
            yield return (nameof(id), id);
        }

        //public override bool Equals(object? obj)
        //{
        //    return obj is ShockwaveState state &&
        //           createdOnTick == state.createdOnTick &&
        //           startAt.Equals(state.startAt) &&
        //           velocity.Equals(state.velocity) &&
        //           timeToLive == state.timeToLive &&
        //           EqualityComparer<Id>.Default.Equals(id, state.id);
        //}

        //public override int GetHashCode()
        //{
        //    return this.Hash();
        //}
    }
}