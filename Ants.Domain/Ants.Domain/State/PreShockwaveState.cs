﻿
using Ants.Domain.Infrastructure;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Intrinsics;
using System.Runtime.Intrinsics.X86;

namespace Ants.Domain.State
{
    public class PreShockwaveState : IStateHash
    {


        public const int timeToLive = 25;

        public const double Speed = ShockwaveState.Speed * 2;

        private readonly int createdOnTick;
        private readonly Vector128<double> startAt;
        private readonly Vector128<double> velocity;
        public readonly ShockwaveState.Id id;

        public PreShockwaveState(int createdOnTick, Vector128<double> startAt, Vector128<double> velocity, ShockwaveState.Id id)
        {
            this.createdOnTick = createdOnTick;
            this.startAt = startAt;
            this.velocity = velocity;
            this.id = id;
        }

        public Vector128<double> CurrentPosition(double tick) {
            return Avx.Add(Avx2.Multiply(velocity, (tick - createdOnTick).AsVector()), startAt);
        }

        internal bool UpdateTrueWhenStillExists(int tick, Map2<RockState> rockMap,  IReadOnlyList<(Vector128<double> globalPosition, double radius)> avoidables, out ShockwaveState? shockwave /*null when the shockwave has run out of gass*/)
        {
            if (tick - createdOnTick > timeToLive) 
            {
                shockwave = null;
                return false;
            }

            // TODO factional movement

            var currentPosition = CurrentPosition(tick);
            var hit = rockMap.Hit(currentPosition, 0);

            if (!hit.Any()) {
                shockwave = null;
                return true;
            }
            var waveVelocity = Avx.Subtract(currentPosition, hit.First().position).ToLength(ShockwaveState.Speed);

            // I think the order of hit will be consistant
            if (Pathing.TryAdjustTarget(currentPosition, avoidables, avoidBy:1, out var res))
            {
                shockwave = new ShockwaveState(
                    tick,
                    res,
                    waveVelocity,
                    timeToLive - (tick - createdOnTick),
                    id);
                return false;
            }
            else {
                Log.WriteLine("Pre shockwave fizzzleds");
                shockwave = null;
                return true;
            }
        }

            public IEnumerable<(string, object?)> HashData()
        {
            yield return (nameof(createdOnTick), createdOnTick);
            yield return (nameof(startAt), startAt);
            yield return (nameof(velocity), velocity);
            yield return (nameof(id), id);
        }

        //public override bool Equals(object? obj)
        //{
        //    return obj is PreShockwaveState state &&
        //           createdOnTick == state.createdOnTick &&
        //           startAt.Equals(state.startAt) &&
        //           velocity.Equals(state.velocity) &&
        //           EqualityComparer<ShockwaveState.Id>.Default.Equals(id, state.id);
        //}

        //public override int GetHashCode()
        //{
        //    return this.Hash();
        //}
    }
}