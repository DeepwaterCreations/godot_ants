﻿

using Ants.Domain;
using System.Runtime.Intrinsics;


public class ZapperGunState : IStateHash {
    

    public class Immutable : IStateHash
    {
        public double shotSpeed;
        public double shotRange;
        public double shotRangeScatter;
        public int shotStun;
        public double shotScatterAngleRad;
        public double ignoreCoverFor;
        public double ignoreCoverForScatter;
        public double shotKnockBack;
        public int reloadTicks;

        public Immutable(double shotSpeed, double shotRange, double shotRangeScatter, int shotStun, double shotScatterAngleRad, double ignoreCoverFor, double ignoreCoverForScatter, double shotKnockBack, int reload)
        {

            this.shotSpeed = shotSpeed;
            this.shotRange = shotRange;
            this.shotRangeScatter = shotRangeScatter;
            this.shotStun = shotStun;
            this.shotScatterAngleRad = shotScatterAngleRad;
            this.ignoreCoverFor = ignoreCoverFor;
            this.ignoreCoverForScatter = ignoreCoverForScatter;
            this.shotKnockBack = shotKnockBack;
            this.reloadTicks = reload;
        }

        public IEnumerable<(string, object?)> HashData()
        {
            yield return (nameof(shotSpeed), shotSpeed);
            yield return (nameof(shotRange), shotRange);
            yield return (nameof(shotRangeScatter), shotRangeScatter);
            yield return (nameof(shotStun), shotStun);
            yield return (nameof(shotScatterAngleRad), shotScatterAngleRad);
            yield return (nameof(ignoreCoverFor), ignoreCoverFor);
            yield return (nameof(ignoreCoverForScatter), ignoreCoverForScatter);
            yield return (nameof(shotKnockBack), shotKnockBack);
            yield return (nameof(reloadTicks), reloadTicks);
        }
    }

    public Immutable immutable;

    public Vector128<double>? target;
    // this is annoying, it is immutable
    // but I can't really pass it in the way I would like...
    // maybe I should build the ant without the component 
    // and then attache the compent?
    // probably would atleast be good to have a getter and setter
    /// <summary>
    /// in practive this is always populated
    /// it should be populated shortly after it is created
    /// </summary>
    public Guid? createdBy;
    public int reloadDue = 0;
    public bool presists;

    public ZapperGunState(Immutable immutable, Vector128<double>? target, int reloadDue, bool presists, Guid? createdBy)
    {
        this.immutable = immutable ?? throw new ArgumentNullException(nameof(immutable));
        this.target = target;
        this.reloadDue = reloadDue;
        this.presists = presists;
        this.createdBy = createdBy;
    }

    public ZapperGunState Copy()
    {
        return new ZapperGunState(immutable, target, reloadDue, presists, createdBy);
    }


    internal static BulletState Shoot(AntState shooter, Vector128<double> at, int frame, ShotIndexTracker shotIndexTracker, double addScatter)
    {
        shooter.zapperGun!.reloadDue = shooter.zapperGun.immutable.reloadTicks;

        return BulletState.MakeBullet(
            shooter.position,
            new BulletState.Immutable(
            shooter.immutable.id,
            frame,
            shotIndexTracker.Get(shooter.immutable.id),
            GunState.GetRunFor(shooter.immutable.id, frame, 0, shooter.zapperGun.immutable.shotRange, shooter.zapperGun.immutable.shotRangeScatter),
            shooter.zapperGun.immutable.shotSpeed,
            GunState.GetShotDirection(shooter.position, at, shooter.immutable.id, frame, 0, shooter.zapperGun.immutable.shotScatterAngleRad, addScatter),
            shooter.immutable.side,
            shooter.position,
            0,
            GunState.GetIgnoreCoverFor(shooter.immutable.id, frame, 0, shooter.zapperGun.immutable.ignoreCoverFor, shooter.zapperGun.immutable.ignoreCoverForScatter),
            0,
            shooter.zapperGun.immutable.shotStun,
            false,
            shooter.zapperGun.immutable.shotKnockBack,
            false));
    }


    public IEnumerable<(string, object?)> HashData()
    {
        yield return (nameof(target), target);
        yield return (nameof(immutable), immutable);
        yield return (nameof(reloadDue), reloadDue);
        yield return (nameof(presists), presists);
        yield return (nameof(createdBy), createdBy);
    }

    internal void Update(AntState antState)
    {
        if (reloadDue > 0 && !antState.IsStuned())
        {
            reloadDue--;
        }
    }
}

public class ZapperState:IStateHash
{
    // TODO
    // these shoudl probably all be in AntType
    // just to see everything together


    //public const int maxEnergy = 60;

    public int MaxEnergy() => this.immutable.setUpTime*2;


    public class Immutable : IStateHash
    {
        public int setUpTime;
        public int reloadTime;
        public double range;

        public Immutable(int setUpTime, int reloadTime, double range)
        {
            this.setUpTime = setUpTime;
            this.reloadTime = reloadTime;
            this.range = range;
        }

        public IEnumerable<(string, object?)> HashData()
        {
            yield return (nameof(setUpTime), setUpTime);
            yield return (nameof(reloadTime), reloadTime);
            yield return (nameof(range), range);
        }
    }

    public Immutable immutable;

    public int energy;

    public ZapperState(Immutable immutable, int energy)
    {
        this.immutable = immutable ?? throw new ArgumentNullException(nameof(immutable));

        this.energy = energy;
    }

    public ZapperState Copy() { 
        return new ZapperState(immutable, energy);
    }

    public IEnumerable<(string, object?)> HashData()
    {
        yield return (nameof(immutable), immutable);
        yield return (nameof(energy), energy);
    }

    internal void Update(AntState antState)
    {
        if (!antState.IsStuned()) {
            energy = Math.Min(energy + 1, MaxEnergy());
        }
    }
}