﻿using Ants.Domain.Infrastructure;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Net.NetworkInformation;
using System.Numerics;
using System.Reflection;
using System.Runtime.Intrinsics;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Ants.Domain.State
{
    public class ImmutableSimulationState
    {
        // maybe keep these ordered? {83711288-CCD7-4631-84F9-9A8975F18873}
        public readonly RockState[] rocks;
        public readonly MudState[] mud;
        public readonly CoverState[] cover;
        public readonly KnownPathsImmutableState paths;
        public readonly Map2<CoverState> coverMap;
        public readonly Map2<RockState> rockMap;
        public readonly Map2<MudState> mudMap;
        public readonly IReadOnlyList<(Vector128<double> position, double radius)> rocksTuple;
        public readonly IReadOnlyList<(Vector128<double> position, double radius)> mudTuple;

        public ImmutableSimulationState(RockState[] rocks, MudState[] mud, CoverState[] cover, KnownPathsImmutableState paths)
        {
            if (cover is null)
            {
                throw new ArgumentNullException(nameof(cover));
            }
            // no two covers should have the same position
            Debug.Assert(cover.Select(x => x.position).Distinct().Count() == cover.Count());
            if (cover.Select(x => x.position).Distinct().Count() != cover.Count())
            {
                var db = cover.GroupBy(x => x.position).Where(x => x.Count() > 1).ToArray();
            }
            this.cover = cover.OrderBy(x => x.position.X()).ThenBy(x => x.position.Y()).ToArray();

            if (rocks is null)
            {
                throw new ArgumentNullException(nameof(rocks));
            }
            // no two rocks should have the same position
            Debug.Assert(rocks.Select(x => x.position).Distinct().Count() == rocks.Count());
            if (rocks.Select(x => x.position).Distinct().Count() != rocks.Count())
            {
                var db = rocks.GroupBy(x => x.position).Where(x => x.Count() > 1).ToArray();
            }
            this.rocks = rocks.OrderBy(x => x.position.X()).ThenBy(x => x.position.Y()).ToArray();


            // no two mud should have the same position
            Debug.Assert(mud.Select(x => x.position).Distinct().Count() == mud.Count());
            if (mud.Select(x => x.position).Distinct().Count() != mud.Count())
            {
                var db = mud.GroupBy(x => x.position).Where(x => x.Count() > 1).ToArray();
            }
            this.mud = mud ?? throw new ArgumentNullException(nameof(mud));
            
            this.paths = paths ?? throw new ArgumentNullException(nameof(paths));


            this.coverMap = Map2<CoverState>.Build(this.cover);// this one is immutable, needs to be ordered to be deterministic
            this.rockMap = Map2<RockState>.Build(this.rocks);// this one is immutable, needs to be ordered to be deterministic
            this.mudMap = Map2<MudState>.Build(this.mud);// this one is immutable, needs to be ordered to be deterministic

            this.rocksTuple = rocks.Select(x => (x.position, x.physicsImmunatbleInner.radius)).ToArray();
            this.mudTuple = mud.Select(x => (x.position, x.physicsImmunatbleInner.radius)).ToArray();
        }
    }

    // TODO this shoudl really be read only and it should be a copy and modify sort of thing
    public class SimulationState: IStateHash
    {
        // maybe simulation state needs an immutable

        // maybe keep these ordered? {83711288-CCD7-4631-84F9-9A8975F18873}
        public ConcurrentDictionary<Guid, AntState> ants;

        public List<GrenadeState> grenadeStates;

        // I'm thinking these don't even belong in the state
        // orders come in with where to go on them
        // train paths aren't in here... 
        // maybe keep these ordered?
        public ConcurrentDictionary<BulletState.Immutable.Id, BulletState> bullets;
        public MoneyState money;
        public ControlState control;
        public ControlPointState[] controlPoints;
        public List<StationState> Stations;
        public PopulationState populationState;
        public ResourceSpawnerState[] resourceSpawners;
        public List<ResourceState> resources;
        public Dictionary<Guid, SmokeState> smokes;
        public List<ShockwaveState> shockwaves;
        //public List<PreShockwaveState> preShockwaves;
        public ConcurrentDictionary<HealBulletState.Immutable.Id, HealBulletState> healBulletStates;
        public ConcurrentDictionary<ArtyBulletState.Immutable.Id, ArtyBulletState> artyBulletStates;
        public List<ZapperSetupState> zapperSetupStates;
        public ConcurrentDictionary<ArcBulletState.Immutable.Id, ArcBulletState> arcBulletStates;
        //public List<SmokeBulletState> smokeBullets;

        public SimulationState(
            ConcurrentDictionary<Guid, AntState> ants,
            ConcurrentDictionary<BulletState.Immutable.Id, BulletState> bullets,
            MoneyState money,
            ControlState control,
            ControlPointState[] controlPoints,
            List<StationState> Stations,
            PopulationState populationState,
            List<GrenadeState> grenadeStates,
            ResourceSpawnerState[] resourceSpawners,
            List<ResourceState> resources,
            Dictionary<Guid, SmokeState> smoke,
            List<ShockwaveState> shockwaves, 
            //List<PreShockwaveState> preShockwaves,
            ConcurrentDictionary<HealBulletState.Immutable.Id, HealBulletState> healBulletStates,
            ConcurrentDictionary<ArtyBulletState.Immutable.Id, ArtyBulletState> artyBulletStates,
            List<ZapperSetupState> zapperSetupStates,
            ConcurrentDictionary<ArcBulletState.Immutable.Id, ArcBulletState> arcBulletStates)
        {
            this.ants = ants ?? throw new ArgumentNullException(nameof(ants));
            this.bullets = bullets ?? throw new ArgumentNullException(nameof(bullets));
            this.money = money ?? throw new ArgumentNullException(nameof(money));
            this.control = control ?? throw new ArgumentNullException(nameof(control));
            this.controlPoints = controlPoints ?? throw new ArgumentNullException(nameof(controlPoints));
            this.Stations = Stations ?? throw new ArgumentNullException(nameof(Stations));
            this.populationState = populationState ?? throw new ArgumentNullException(nameof(populationState));
            this.grenadeStates = grenadeStates ?? throw new ArgumentNullException(nameof(grenadeStates));
            this.resourceSpawners = resourceSpawners ?? throw new ArgumentNullException(nameof(resourceSpawners));
            this.resources = resources ?? throw new ArgumentNullException(nameof(resources));
            this.smokes = smoke ?? throw new ArgumentNullException(nameof(smoke));
            this.shockwaves = shockwaves ?? throw new ArgumentNullException(nameof(shockwaves));
            //this.preShockwaves = preShockwaves ?? throw new ArgumentNullException(nameof(preShockwaves));
            this.healBulletStates = healBulletStates ?? throw new ArgumentNullException(nameof(healBulletStates));
            this.artyBulletStates = artyBulletStates ?? throw new ArgumentNullException(nameof(artyBulletStates));
            this.zapperSetupStates = zapperSetupStates ?? throw new ArgumentNullException(nameof(zapperSetupStates));
            this.arcBulletStates = arcBulletStates ?? throw new ArgumentNullException(nameof(arcBulletStates));
        }

        //public override int GetHashCode()
        //{
        //    return this.Hash();
        //}

        // does not include pending population
        public double CompletePopulation(int side) {
            return ants.Values
            .Where(x => x.immutable.side == side)
            .Select(x => AntFactory.Population(x.immutable.type))
            .Sum();
        }

        public double CompletAndPendingPopulation(int side) {
            return CompletePopulation(side) + populationState.pendingPopulation[side];
        }

        public IEnumerable<(string, object?)> HashData()
        {
            yield return (nameof(ants), ants);
            //yield return (nameof(paths), paths);
            yield return (nameof(bullets), bullets);
            yield return (nameof(money), money);
            yield return (nameof(control), control);
            yield return (nameof(controlPoints), controlPoints);
            yield return (nameof(Stations), Stations);
            yield return (nameof(populationState), populationState);
            yield return (nameof(grenadeStates), grenadeStates);
            yield return (nameof(resources), resources);
            yield return (nameof(smokes), smokes);
            yield return (nameof(shockwaves), shockwaves);
            //yield return (nameof(preShockwaves), preShockwaves);
            yield return (nameof(healBulletStates), healBulletStates);
            yield return (nameof(artyBulletStates), artyBulletStates);
            yield return (nameof(zapperSetupStates), zapperSetupStates);
            yield return (nameof(arcBulletStates), arcBulletStates);
        }

        // I could probably copy less
        // copy on modify
        public SimulationState Copy()
        {
            var nextAnt = new ConcurrentDictionary<Guid, AntState>();

            foreach (var (key, value) in ants)
            {
                nextAnt[key] = value.Copy();
            }

            var nextBullet = new ConcurrentDictionary<BulletState.Immutable.Id, BulletState>();

            foreach (var (key, value) in bullets)
            {
                nextBullet[key] = value.Copy();
            }

            var nextHealBulletStates = new ConcurrentDictionary<HealBulletState.Immutable.Id, HealBulletState>();

            foreach (var (key, value) in healBulletStates)
            {
                nextHealBulletStates[key] = value.Copy();
            }

            var nextArtyBulletStates = new ConcurrentDictionary<ArtyBulletState.Immutable.Id, ArtyBulletState>();

            foreach (var (key, value) in artyBulletStates)
            {
                nextArtyBulletStates[key] = value.Copy();
            }

            var nextArcBullet = new ConcurrentDictionary<ArcBulletState.Immutable.Id, ArcBulletState>();

            foreach (var (key, value) in arcBulletStates)
            {
                nextArcBullet[key] = value.Copy();
            }

            return new SimulationState(
                nextAnt,
                nextBullet,
                money.Copy(),
                control.Copy(),
                controlPoints.ToArray(),//controlPoints are readonly so shallow copy is ok
                Stations.ToList(),//Stations are readonly so shallow copy is ok
                populationState.Copy(),
                grenadeStates.ToList(),
                resourceSpawners,
                resources.Select(x => x.Copy()).ToList(),
                smokes.ToDictionary(x => x.Key, x => x.Value),
                shockwaves.ToList(), //shockwaves are readonly so shallow copy is ok
                //preShockwaves.ToList(), //preShockwaves are readonly so shallow copy is ok
                nextHealBulletStates,
                nextArtyBulletStates,
                zapperSetupStates.Select(x => x.Copy()).ToList(),
                nextArcBullet
                );
        }
    }

    public static class SimulationStateExtensions {
        public static double HoneyPotHpSum(this SimulationState state, int side)
        {
            return state.ants.Values.Where(x => x.honeyPotState != null && x.immutable.side == side).Sum(x => x.hp);
        }
        public static double HoneyPotHpStorageSum(this SimulationState state, int side)
        {
            return state.ants.Values.Where(x => x.honeyPotState != null && x.immutable.side == side).Sum(x => x.hp*x.honeyPotState.storagePerHp);
        }
    }
}