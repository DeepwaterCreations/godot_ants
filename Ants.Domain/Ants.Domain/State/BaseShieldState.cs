﻿using Ants.Domain.Infrastructure;
using System.Diagnostics;
using System.Runtime.Intrinsics;
using System.Runtime.Intrinsics.X86;

public abstract class BaseShieldState: IStateHash
{
    public Vector128<double> facingUnit;
    public readonly Queue<(int tick, Vector128<double> from/* really some distance in the direction it came from */, double perce, double damagePreArmor)> lastHits = new ();
    public bool isShielded = false;
    public int exposedFor = 0;
    public double exposedAmount = 0;

    public class BaseImmutable: IStateHash
    {
        public readonly double frontArmor;
        public readonly double backArmor;
        public readonly double armorAngleRads;

        public BaseImmutable(double frontArmor, double backArmor, double armorAngleRads)
        {
            this.frontArmor = frontArmor;
            this.backArmor = backArmor;
            this.armorAngleRads = armorAngleRads;
        }

        //public override bool Equals(object? obj)
        //{
        //    return obj is BaseImmutable immutable &&
        //           frontArmor == immutable.frontArmor &&
        //           backArmor == immutable.backArmor &&
        //           armorAngleRads == immutable.armorAngleRads;
        //}

        //public override int GetHashCode() => this.Hash();

        public IEnumerable<(string, object?)> HashData()
        {
            yield return (nameof(frontArmor), frontArmor); 
            yield return (nameof(backArmor), backArmor); 
            yield return (nameof(armorAngleRads), armorAngleRads);
        }
    }
    public readonly BaseImmutable baseImmutable;


    protected BaseShieldState(Vector128<double> facingUnit, Queue<(int tick, Vector128<double> from, double perce, double damagePreArmor)> lastHits, BaseImmutable immutable, bool isShielded, int exposedFor, double exposedAmount)
    {
        this.facingUnit = facingUnit;
        this.lastHits = lastHits ?? throw new ArgumentNullException(nameof(lastHits));
        this.baseImmutable = immutable ?? throw new ArgumentNullException(nameof(immutable));
        this.isShielded = isShielded;
        this.exposedAmount = exposedAmount;
        this.exposedFor = exposedFor;
    }

    public double ApplyArmor(Vector128<double> currentPosition, Vector128<double> directionOfMotion, double peirce, double damage, int tick)
    {
        var from = Avx.Subtract(currentPosition, directionOfMotion.ToLength(300));
        lastHits.Enqueue((tick, from, peirce, damage));

        return HypitheticalDamage(/*currentPosition,*/ directionOfMotion, peirce, damage);
    }


    public static double HypitheticalDamageArmorPeirce(double armor, double peirce) {
        return Sigmoidish(peirce - armor);
    }

    public double HypitheticalDamage(/*Vector128<double> currentPosition,*/ Vector128<double> directionOfMotion, double peirce, double damage)
    {
        if (exposedFor > 0) {
            damage *= exposedAmount;
        }

        if (isShielded)
        {
            return damage * ApplyArmor(directionOfMotion, peirce, facingUnit/*, currentPosition*/);
        }

        return damage;
    }

    public double ApplyArmor(Vector128<double> directionOfMotion, double peirce, Vector128<double> facingUnit/*, Vector128<double> currentPosition*/)
    {

        //var affectiveFrontArmor = frontArmor;// destination.Any() ? frontArmorMoving : frontArmor;

        //var incommingAngle = (currentPosition - startedAt).Angle();
        //var facingAngle = facingUnit.Angle();

        var angleBetween = facingUnit.NormalizedCopy().AngleTo(directionOfMotion.Rotated(Math.PI));

        //var armorAngleRadius = (baseImmutable.armorAngleRads * 2 * Math.Pi) / 360.0;

        return ApplyArmor(peirce, angleBetween);

        //var dot = facingUnit.Dot((GlobalPosition - startedAt).Normalized());
        //var percent = (1 + dot) / 2.0;
        //var hitArmor = (frontArmor * (1 - percent)) + (backArmor * percent);

        //var res2 = Wall.Sigmoid(peirce - hitArmor);
        //return res2;
    }

    private double ApplyArmor(double peirce, double angleBetween)
    {

        if (angleBetween < 0) {
            angleBetween = -angleBetween;
        }

        while (angleBetween > Math.PI)
        {
            angleBetween = (2 * Math.PI) - angleBetween;
        }

        var affectiveArmor = baseImmutable.backArmor;
        var halfArmorAngle = baseImmutable.armorAngleRads / 2.0;

        if (baseImmutable.armorAngleRads > angleBetween)
        {
            var percent = (angleBetween / baseImmutable.armorAngleRads) * (angleBetween / baseImmutable.armorAngleRads) * (angleBetween / baseImmutable.armorAngleRads);
            //affectiveArmor = baseImmutable.frontArmor;
            affectiveArmor = (baseImmutable.frontArmor * (1 - percent)) + (baseImmutable.backArmor * percent);
        }
        var res2 = Sigmoidish(peirce - affectiveArmor);
        return res2;
    }

    private const double scale = .5;
    public static double Sigmoidish(double value)
    {
        if (value >= 0)
        {
            // .5 + (.5 - (.5 / (1 + (value*scale))));
            return .5 + (.5 * (1 - (1 / (1 + (value*scale)))));
        }
        return .5 - (.5 * (1 - (1 / (1 - (value* scale)))));

        //return (1.0 / (1.0 + Mathf.Pow(Mathf.E, -value)));
    }

    public virtual void Update(int tick, AntState ant)
    {
        if (exposedFor > 0) {
            exposedFor--;
        }

        while (lastHits.Count() > 5)
        {
            if ((tick - lastHits.Peek().tick) > 5 * 60)
            {
                lastHits.Dequeue();
            }
            else
            {
                break;
            }
        }

        if (lastHits.Any())
        {
            // this has been optimized 
            // to minimize the number of calls to Angle
            // which is apparently slow

            // more optimizatio might be needed
            // maybe don't calc on every frame
            // or don't calc if you don't have to
            // or store part of this calcuation in last hits

            // you could probably use dot..
            
            var damageByAngle = new double[36];
            foreach (var hit in lastHits)
            {
                var angle = Avx.Subtract(hit.from, ant.position).Angle();

                if (angle < 0) {
                    angle = (2 * Math.PI) + angle;
                }

                for (int i = 0; i < damageByAngle.Length; i++)
                {
                    var theta = (Math.PI * 2 * i) / damageByAngle.Length;

                    var agnleDiff = Math.Abs(angle - theta);

                    var damage = ApplyArmor(hit.perce, agnleDiff) * hit.damagePreArmor;

                    //  + Math.FloorToInt(angle/((Math.Pi * 2)/ damageByAngle.Length)) % 
                    damageByAngle[i] += damage;
                }
            }

            var bestI = 0;
            for (int i = 1; i < damageByAngle.Length; i++)
            {
                if (damageByAngle[i] < damageByAngle[bestI]) {
                    bestI = i;
                }
            }

            facingUnit = Vector128.Create(1.0, 0.0).Rotated((Math.PI * 2 * bestI) / damageByAngle.Length);
        }
    }

    internal abstract BaseShieldState Copy();
    public abstract IEnumerable<(string, object?)> HashData();
}
