﻿using Prototypist.Toolbox.Object;

public class SmokePlacerState : IStateHash
{
    public const double range = 400;

    public Guid? mySmokeId;
    public SmokePlacerState(Guid? mySmokeId)
    {
        this.mySmokeId = mySmokeId;
    }

    public SmokePlacerState Copy()
    {
        return new SmokePlacerState(mySmokeId);
    }

    public IEnumerable<(string, object?)> HashData()
    {
        yield return (nameof(mySmokeId), mySmokeId);
    }

    //public override bool Equals(object? obj)
    //{
    //    return obj is SmokePlacerState state &&
    //           mySmokeId.NullSafeEqual(state.mySmokeId);
    //}

    //public override int GetHashCode()
    //{
    //    return this.Hash();
    //}
}