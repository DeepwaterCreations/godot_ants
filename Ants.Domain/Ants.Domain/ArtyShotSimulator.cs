﻿//using Godot;
using System.Runtime.Intrinsics;

public class ArtyShotSimulator
{
    private class TestPhysicsSubjectState : PhysicsSubjectState2
    {
        public TestPhysicsSubjectState(ImmunatblePhysicsState immunatble, Vector128<double> position, Vector128<double> velocity) : base(immunatble, position, velocity)
        {
        }

        //public override int GetHashCode()
        //{
        //    throw new NotImplementedException();
        //}

        public override IEnumerable<(string, object?)> HashData()
        {
            throw new NotImplementedException();
        }
    }

    public static IEnumerable<Vector128<double>> ProjectPath(ImmunatblePhysicsState immunatble, Vector128<double> position, Vector128<double> velocity, Map2<RockState> rockMap, int steps)
    {
        var subject = new TestPhysicsSubjectState(immunatble, position, velocity);

        var ourMap = Map2<PhysicsSubjectState2>.Build(new[] { subject });

        var keyPoints = new List<Vector128<double>>
        {
            subject.position
        };

        var isBounce = false;
        // this has to match the code that actually moves the arty shot
        // {7F17B933-EC40-4E32-8B07-807977B28FD3}
        for (int i = 0; i < steps; i++)
        {
            foreach (var step in ourMap.MoveByLength(subject, 10))
            {

                var hit = rockMap.Hit(subject);

                if (!hit.Any())
                {
                    if (isBounce)
                    {
                        keyPoints.Add(subject.position);
                        isBounce = false;
                    }
                }
                else
                {
                    if (!isBounce)
                    {
                        keyPoints.Add(subject.position);
                        isBounce = true;
                    }

                    foreach (var rockHit in hit)
                    {
                        // bounce
                        subject.BounceOffOf(rockHit, step);
                    }
                }
            }
        }
        keyPoints.Add(subject.position);

        return keyPoints;
    }

}
