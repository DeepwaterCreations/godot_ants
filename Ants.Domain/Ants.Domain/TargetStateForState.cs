﻿//using Godot;
using Ants.Domain.Infrastructure;
using Ants.Domain.State;
using Prototypist.Toolbox;
using Prototypist.Toolbox.Object;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.Intrinsics;
using System.Runtime.Intrinsics.X86;
using static WhoToShoot;

public class TargetStateFromState
{
    public static double lastAdvanceSeconds;


    private class FriendAndFoeBySide {
        private Dictionary<int, List<(AntState ant, bool fullyVisible)>> couldShootAtDictAnt = new();
        private Dictionary<int, List<(ZapperSetupState zapper, bool fullyVisible)>> couldShootAtDictZapper = new();

        private readonly IReadOnlyDictionary<int, HashSet<Guid>> sideSeesUnits;
        private readonly IReadOnlyDictionary<int, HashSet<Guid>> sideSeesUnitsMid;

        private readonly IReadOnlyDictionary<int, HashSet<ZapperSetupState.Immutable>> sideSeesZapperFull;
        private readonly IReadOnlyDictionary<int, HashSet<ZapperSetupState.Immutable>> sideSeesZapperMid;

        public FriendAndFoeBySide(IReadOnlyDictionary<int, HashSet<Guid>> sideSeesUnits, IReadOnlyDictionary<int, HashSet<Guid>> sideSeesUnitsMid, IReadOnlyDictionary<int, HashSet<ZapperSetupState.Immutable>> sideSeesZapperFull, IReadOnlyDictionary<int, HashSet<ZapperSetupState.Immutable>> sideSeesZapperMid)
        {
            this.sideSeesUnits = sideSeesUnits ?? throw new ArgumentNullException(nameof(sideSeesUnits));
            this.sideSeesUnitsMid = sideSeesUnitsMid ?? throw new ArgumentNullException(nameof(sideSeesUnitsMid));
            this.sideSeesZapperFull = sideSeesZapperFull ?? throw new ArgumentNullException(nameof(sideSeesZapperFull));
            this.sideSeesZapperMid = sideSeesZapperMid ?? throw new ArgumentNullException(nameof(sideSeesZapperMid));
        }

        public List<(AntState ant, bool fullyVisible)> GetGuysCouldShootAt(int side, SimulationState fromState) {

  

            if (!couldShootAtDictAnt.TryGetValue(side, out var guysCouldShootAt))
            {
                sideSeesUnits.TryGetValue(side, out var sideSees);
                sideSeesUnitsMid.TryGetValue(side, out var sideSeesMid);
                sideSees = sideSees ?? new HashSet<Guid>();
                sideSeesMid = sideSeesMid ?? new HashSet<Guid>();

                guysCouldShootAt = new List<(AntState ant, bool fullyVisible)>();
                foreach (var ant in fromState.ants.Values)
                {
                    if (ant.immutable.side != side)
                    {
                        if (sideSees.Contains(ant.immutable.id))
                        {
                            guysCouldShootAt.Add((ant, fullyVisible: true));
                        }
                        else if (sideSeesMid.Contains(ant.immutable.id))
                        {
                            guysCouldShootAt.Add((ant, fullyVisible: false));
                        }
                    }
                }
                couldShootAtDictAnt.Add(side, guysCouldShootAt);
            }
            return guysCouldShootAt;
        }

        public List<(ZapperSetupState zapper, bool fullyVisible)> GetZappersCouldShootAt(int side, SimulationState fromState)
        {
            if (!couldShootAtDictZapper.TryGetValue(side, out var zappersCouldShootAt))
            {

                sideSeesZapperFull.TryGetValue(side, out var sideSeesZapperForSide);
                sideSeesZapperForSide = sideSeesZapperForSide ?? new HashSet<ZapperSetupState.Immutable>();
                sideSeesZapperMid.TryGetValue(side, out var sideSeesZapperMidForSide);
                sideSeesZapperMidForSide = sideSeesZapperMidForSide ?? new HashSet<ZapperSetupState.Immutable>();


                zappersCouldShootAt = new List<(ZapperSetupState zapper, bool fullyVisible)>();
                foreach (var zapper in fromState.zapperSetupStates)
                {
                    if (fromState.ants.TryGetValue(zapper.immutable.createdBy, out var relatedAnt))
                    {
                        if (relatedAnt.immutable.side != side)
                        {
                            if (sideSeesZapperForSide.Contains(zapper.immutable))
                            {
                                zappersCouldShootAt.Add((zapper, fullyVisible: true));
                            }
                            else if (sideSeesZapperMidForSide.Contains(zapper.immutable))
                            {
                                zappersCouldShootAt.Add((zapper, fullyVisible: false));
                            }
                        }
                    }
                }

                couldShootAtDictZapper.Add(side, zappersCouldShootAt);
            }

            return zappersCouldShootAt;
        }
    }

    // cap on how many who to shoots we will calculate in one frame
    // maybe make this parallel
    public static TargetState2 FromState(
        SimulationState fromState,
        IReadOnlyDictionary<int, HashSet<Guid>> sideSeesUnits,
        IReadOnlyDictionary<int, HashSet<Guid>> sideSeesUnitsMid,
        ImmutableSimulationState immutableSimulationState,
        int tick,
        IReadOnlyDictionary<int, AntState[]> unitsBySide,
        IReadOnlyDictionary<int, HashSet<ZapperSetupState.Immutable>> sideSeesZapperFull,
        IReadOnlyDictionary<int, HashSet<ZapperSetupState.Immutable>> sideSeesZapperMid)
    {
        //var map = Map.FromState(fromState);

        Stopwatch simulation = new Stopwatch();
        simulation.Start();

        var shooters = fromState.ants.Values.Where(ant =>
            ant.gun != null &&
            ant.gun.immutable.autoShoot &&
            ant.gun.reloadDue <= TargetState.runEvery * 2 &&
            (ant.gun.immutable.canMoveAndShoot || ant.legs == null || !ant.legs.IsMoving())).ToArray();

        var couldShootAt = new FriendAndFoeBySide(sideSeesUnits, sideSeesUnitsMid, sideSeesZapperFull, sideSeesZapperMid);

        var couldShootAtDictAnt = new Dictionary<int, List<(AntState ant, bool fullyVisible)>>();
        var couldShootAtDictZapper = new Dictionary<int, List<(ZapperSetupState zapper, bool fullyVisible)>>();

        var res1 = shooters
            .Select(shooter =>
            {
                var from = shooter.position;

                var guysCouldShootAt = couldShootAt.GetGuysCouldShootAt(shooter.immutable.side, fromState);

                var zappersCouldShootAt = couldShootAt.GetZappersCouldShootAt(shooter.immutable.side, fromState);

                if (guysCouldShootAt.Any() || zappersCouldShootAt.Any())
                {
                    var reasonableRange = shooter.gun/*better have a gun it you are shooting*/!.immutable.range + shooter.gun.immutable.rangeScatter;

                    var targets = guysCouldShootAt
                                .Select(guy => (target: guy, guy.ant.position, time: Avx.Subtract( guy.ant.position, shooter.position).LengthSquared() /(shooter.gun.immutable.shotSpeed* shooter.gun.immutable.shotSpeed)))
                                .Select(x => new Target(OrType.Make<AntState, ZapperSetupState>(x.target.ant), x.position, x.target.Item2))
                                .Union(zappersCouldShootAt.Select(x => new Target(OrType.Make<AntState, ZapperSetupState>(x.zapper), x.zapper.position, x.fullyVisible)))
                                .Where(x => Avx.Subtract(x.projectedPosition, from).LengthSquared() < reasonableRange * reasonableRange)
                                .ToArray();

                    //if (!unitsBySide.TryGetValue(shooter.immutable.side, out var friends))
                    //{
                    //    friends = new AntState[] { };
                    //}

                    var at = WhoToShoot.ShootAt(
                        targets,
                        immutableSimulationState.rocks
                            .Where(x => Avx.Subtract(x.Position, from).LengthSquared() < reasonableRange * reasonableRange && Avx.Subtract(x.Position, from).LengthSquared() != 0)
                            .Select(x => (IBlocksShots)x)
                            //.Union(friends.Except(new[] { shooter }))
                            .ToArray(),
                        immutableSimulationState.cover.Where(x => Avx.Subtract(x.position, from).LengthSquared() < reasonableRange * reasonableRange).ToArray(),
                        from,
                        shooter.gun,
                        tick,
                        shooter.immutable.id,
                        shooter.physicsImmunatbleInner.radius);
                    if (at.HasValue)
                    {
                        return (true, shooter, at.Value.point, at.Value.targetVelocity, at.Value.addScatter);
                    }
                }
                return (false, shooter, Vector128.Create(0.0, 0.0), Vector128.Create(0.0, 0.0), true);
            })
            .Where(x => x.Item1);


        var arcShooters = fromState.ants.Values.Where(ant =>
            ant.arcGun != null &&
            ant.arcGun.immutable.autoShoot &&
            ant.arcGun.reloadDue <= TargetState.runEvery * 2 &&
            (ant.arcGun.immutable.canMoveAndShoot || ant.legs == null || !ant.legs.IsMoving())).ToArray();

        var res2 = arcShooters
            .SelectMany(arcShooter =>
            {
                var from = arcShooter.position;

                var guysCouldShootAt = couldShootAt.GetGuysCouldShootAt(arcShooter.immutable.side, fromState);

                var zappersCouldShootAt = couldShootAt.GetZappersCouldShootAt(arcShooter.immutable.side, fromState);


                if (guysCouldShootAt.Any() || zappersCouldShootAt.Any())
                {
                    var reasonableRange = arcShooter.arcGun/*better have a gun it you are shooting*/!.immutable.radius * 2;

                    var targets = guysCouldShootAt
                                .Select(guy => new Target(OrType.Make<AntState, ZapperSetupState>(guy.ant), guy.ant.position, guy.Item2))
                                .Union(
                                    zappersCouldShootAt
                                    .Select(pair => new Target(OrType.Make<AntState, ZapperSetupState>(pair.zapper), pair.zapper.position, pair.fullyVisible)))
                                .ToArray();

                    //if (!unitsBySide.TryGetValue(arcShooter.immutable.side, out var friends))
                    //{
                    //    friends = new AntState[] { };
                    //}

                    var direction = WhoToShoot.ArcShootAt(
                        targets,
                        immutableSimulationState.rocks
                            .Select(x => (IBlocksShots)x)
                            //.Union(friends.Except(new[] { arcShooter }))
                            .Where(x => Avx.Subtract(x.Position, from).LengthSquared() < (reasonableRange + x.Radius) * (reasonableRange + x.Radius))
                            .ToArray(),
                        immutableSimulationState.cover.Where(x => Avx.Subtract(x.position, from).LengthSquared() < reasonableRange * reasonableRange).ToArray(),
                        from,
                        arcShooter.arcGun,
                        tick,
                        arcShooter.immutable.id);
                    if (direction.HasValue)
                    {
                        return new[] { (shooter:arcShooter, direction.Value.point, direction.Value.direction, direction.Value.velocity, direction.Value.addScatter, direction.Value.time) };
                    }
                }
                return new (AntState shooter, Vector128<double> point, Vector128<double> direction, Vector128<double> velocity, bool addScatter, double time)[0];
            }).ToDictionary(x=>x.shooter.immutable.id, x=>new PositionVelocityAndDirection(x.point, x.velocity, x.direction, x.addScatter, x.time));

        var healers = fromState.ants.Values.Where(shooter =>
                shooter.healGun != null &&
                shooter.healGun.reloadDue <= TargetState.runEvery * 2 &&
                (shooter.healGun.immutable.canMoveAndShoot || shooter.legs == null || !shooter.legs.IsMoving())).ToArray();

        var friendsAndFoesBySide = new Dictionary<int, (Target[] couldHeal, IBlocksShots[] shotBlockers)>();

        var map = fromState.zapperSetupStates.ToDictionary(x => x.immutable, x => x);

        var res3 = healers
            .Select(healer =>
            {
                var from = healer.position;
                var reasonableRange = healer.healGun/*better have a gun it you are shooting*/!.immutable.range + healer.healGun.immutable.rangeScatter;

                Target[] targets;
                IBlocksShots[] shotBlockers;
                if (!friendsAndFoesBySide.TryGetValue(healer.immutable.side, out var friendsAndFoes)) {
                    sideSeesUnits.TryGetValue(healer.immutable.side, out var sideSees);
                    sideSeesUnitsMid.TryGetValue(healer.immutable.side, out var sideSeesMid);
                    sideSees = sideSees ?? new HashSet<Guid>();
                    sideSeesMid = sideSeesMid ?? new HashSet<Guid>();

                    var hurtFreinds = new List<AntState>();
                    var inTheWayFreinds = new List<AntState>();
                    foreach (var friend in unitsBySide[healer.immutable.side])
                    {
                        if (friend.hp < friend.immutable.maxHp)
                        {
                            hurtFreinds.Add(friend);
                        }
                        else
                        {
                            inTheWayFreinds.Add(friend);
                        }
                    }

                    var hurtFriendZappers = new List<ZapperSetupState>();
                    var inTheWayFreindZappers = new List<ZapperSetupState>();
                    foreach (var friendZapper in fromState.zapperSetupStates.Where(x=> fromState.ants.TryGetValue(x.immutable.createdBy, out var ant) && ant.immutable.side == healer.immutable.side))
                    {
                        if (friendZapper.hp < ZapperSetupState.MaxHp)
                        {
                            hurtFriendZappers.Add(friendZapper);
                        }
                        else
                        {
                            inTheWayFreindZappers.Add(friendZapper);
                        }
                    }


                    var baddies = sideSees.Union(sideSeesMid).SelectMany(x => fromState.ants.TryGetValue(x, out var ant) ? new[] { ant} : Array.Empty<AntState>()).Where(x => x.immutable.side != healer.immutable.side).Select(x=> { IBlocksShots y = x;return y; }).ToArray();

                    sideSeesZapperFull.TryGetValue(healer.immutable.side, out var zapperFull);
                    sideSeesZapperMid.TryGetValue(healer.immutable.side, out var zapperMid);
                    zapperFull = zapperFull ?? new HashSet<ZapperSetupState.Immutable>();
                    zapperMid = zapperMid ?? new HashSet<ZapperSetupState.Immutable>();


                    var baddieZapperSetups = zapperFull
                        .Union(zapperMid)
                        .Where(x => fromState.ants.TryGetValue(x.createdBy, out var ant) && ant.immutable.side != healer.immutable.side)
                        .Select(x => { IBlocksShots y = map[x]; return y; }).ToArray();

                    shotBlockers = immutableSimulationState.rocks
                        .Select(x => (IBlocksShots)x)
                        .Union(baddies)
                        .Union(baddieZapperSetups)
                        .Where(x => Avx.Subtract(x.Position, from).LengthSquared() < reasonableRange * reasonableRange && Avx.Subtract(x.Position, from).LengthSquared() != 0)
                        .ToArray();

                    // TODO arc healers?

                    targets = hurtFreinds
                     .Select(ant => (ant: ant, ant.position, time: Avx.Subtract(ant.position, healer.position).LengthSquared() / (healer.healGun.immutable.shotSpeed * healer.healGun.immutable.shotSpeed)))
                        .Select(x => new Target(OrType.Make<AntState, ZapperSetupState>(x.ant), x.position, true))
                        .Union(hurtFriendZappers.Select(x => new Target(OrType.Make<AntState, ZapperSetupState>(x), x.position, true)))
                        .Where(x => Avx.Subtract(x.projectedPosition, from).LengthSquared() < reasonableRange * reasonableRange)
                        .ToArray();

                    friendsAndFoesBySide[healer.immutable.side] = (targets, shotBlockers);
                }else{
                    (targets, shotBlockers) = friendsAndFoes;
                }

                var at = WhoToShoot.HealAt(
                    targets.Where(x=>!x.target.Is1(out var ant) || ant.immutable.id != healer.immutable.id).ToArray(),
                    shotBlockers,
                    immutableSimulationState.cover.Where(x => Avx.Subtract(x.position, from).LengthSquared() < reasonableRange * reasonableRange).ToArray(),
                    from,
                    healer.healGun,
                    tick,
                    healer.physicsImmunatbleInner.radius);
                if (at.HasValue)
                {
                    return (true, healer, at.Value.position, at.Value.velocity, at.Value.addScatter);
                }

                return (false, healer, Vector128.Create(0.0, 0.0), Vector128.Create(0.0, 0.0), true);
            })
            .Where(x => x.Item1);


        lastAdvanceSeconds = (double)simulation.Elapsed.Ticks / (double)TimeSpan.TicksPerSecond;

        return new TargetState2(res1.Union(res3)
            .ToDictionary(x => x.Item2.immutable.id, x => new PositionAndVelocity(x.Item3,x.Item4, x.Item5)), res2, tick);

    }
}

public class Target
{
    public IOrType<AntState, ZapperSetupState> target;
    public Vector128<double> projectedPosition;
    public bool fullyVisible;

    public Target(IOrType<AntState, ZapperSetupState> target, Vector128<double> projectedPosition, bool fullyVisible)
    {
        this.target = target ?? throw new ArgumentNullException(nameof(target));
        this.projectedPosition = projectedPosition;
        this.fullyVisible = fullyVisible;
    }
}

public class ArcTarget
{
    public IOrType<AntState, ZapperSetupState> target;
    public Vector128<double> projectedPosition;
    public bool fullyVisible;

    public ArcTarget(IOrType<AntState, ZapperSetupState> target, Vector128<double> projectedPosition, bool fullyVisible)
    {
        this.target = target ?? throw new ArgumentNullException(nameof(target));
        this.projectedPosition = projectedPosition;
        this.fullyVisible = fullyVisible;
    }
}
