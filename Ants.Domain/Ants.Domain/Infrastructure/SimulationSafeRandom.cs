﻿using Ants.Domain.State;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ants.Domain.Infrastructure
{
    internal class SimulationSafeRandom
    {
        //
        private static byte[] numbers = GetBytes();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="min">inclusive</param>
        /// <param name="max">exclusive</param>
        /// <returns></returns>
        public static int GetInt(int min, int max, params object[] parms)
        {
            

            var index = parms.UncheckedSum(x => x.GetHashCode());

            var number = BitConverter.ToInt32(numbers, Math.Abs(index % (numbers.Length-4)));

            var res = number % (max - min) + min;

            //Log.WriteLine($"rolling {string.Join(", ", parms.Select(x => x.ToString()))} got {res}");

            return res;
        }


        public static double GetDouble(double min, double max, params object[] parms)
        {
            var index = parms.UncheckedSum(x => x.GetHashCode());

            // we can't do the same thing thing we do with int
            // because the numbers in double aren't evenly distributed
            // here is the same number of doubles in every power of ten 
            // even though there are different numbers of number in each power of ten 

            var percentile = BitConverter.ToUInt32(numbers, Math.Abs(index % (numbers.Length-4))) / (double)uint.MaxValue;

            var res = (percentile * (max - min)) + min;

            //Log.WriteLine($"rolling {string.Join(", ", parms.Select(x => x.ToString()))} got {res}");

            return res;
        }

        private static byte[] GetBytes()
        {
            var res = new byte[1000];
            new Random(982634729).NextBytes(res);
            return res;
        }
    }
}
