﻿using System;
using System.Collections.Generic;
using System.Linq;

public static class QuequeExtension
{
    public static Queue<T> ToQueue<T>(this IEnumerable<T> self) {
        var queue = new Queue<T>();
        foreach (var item in self)
        {
            queue.Enqueue(item);
        }
        return queue;
    }
}
