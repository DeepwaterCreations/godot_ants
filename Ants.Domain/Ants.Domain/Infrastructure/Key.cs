﻿// Godot as an enum of the same name
// but this library is independent of godot
// so I duplicated it, it's annoying
namespace Ants.Domain;

public struct Key
{
    // I copied this from a decompiled version of Godot.Key
    // it matches this list https://docs.godotengine.org/en/3.0/classes/class_@globalscope.html?highlight=%40GlobalScope
    // and this list https://learn.microsoft.com/en-us/dotnet/api/system.windows.forms.keys?redirectedfrom=MSDN&view=windowsdesktop-7.0

    public readonly long keyCode;

    //
    // Summary:
    //     Enum value which doesn't correspond to any key. This is used to initialize Godot.Key
    //     properties with a generic state.
    public static Key None = new Key(0L);
    //
    // Summary:
    //     Keycodes with this bit applied are non-printable.
    public static Key Special = new Key(0x400000L);
    //
    // Summary:
    //     Escape key.
    public static Key Escape = new Key(4194305L);
    //
    // Summary:
    //     Tab key.
    public static Key Tab = new Key(4194306L);
    //
    // Summary:
    //     Shift + Tab key.
    public static Key Backtab = new Key(4194307L);
    //
    // Summary:
    //     Backspace key.
    public static Key Backspace = new Key(4194308L);
    //
    // Summary:
    //     Return key (on the main keyboard).
    public static Key Enter = new Key(4194309L);
    //
    // Summary:
    //     Enter key on the numeric keypad.
    public static Key KpEnter = new Key(4194310L);
    //
    // Summary:
    //     Insert key.
    public static Key Insert = new Key(4194311L);
    //
    // Summary:
    //     Delete key.
    public static Key Delete = new Key(4194312L);
    //
    // Summary:
    //     Pause key.
    public static Key Pause = new Key(4194313L);
    //
    // Summary:
    //     Print Screen key.
    public static Key Print = new Key(4194314L);
    //
    // Summary:
    //     System Request key.
    public static Key Sysreq = new Key(4194315L);
    //
    // Summary:
    //     Clear key.
    public static Key Clear = new Key(4194316L);
    //
    // Summary:
    //     Home key.
    public static Key Home = new Key(4194317L);
    //
    // Summary:
    //     End key.
    public static Key End = new Key(4194318L);
    //
    // Summary:
    //     Left arrow key.
    public static Key Left = new Key(4194319L);
    //
    // Summary:
    //     Up arrow key.
    public static Key Up = new Key(4194320L);
    //
    // Summary:
    //     Right arrow key.
    public static Key Right = new Key(4194321L);
    //
    // Summary:
    //     Down arrow key.
    public static Key Down = new Key(4194322L);
    //
    // Summary:
    //     Page Up key.
    public static Key Pageup = new Key(4194323L);
    //
    // Summary:
    //     Page Down key.
    public static Key Pagedown = new Key(4194324L);
    //
    // Summary:
    //     Shift key.
    public static Key Shift = new Key(4194325L);
    //
    // Summary:
    //     Control key.
    public static Key Ctrl = new Key(4194326L);
    //
    // Summary:
    //     Meta key.
    public static Key Meta = new Key(4194327L);
    //
    // Summary:
    //     Alt key.
    public static Key Alt = new Key(4194328L);
    //
    // Summary:
    //     Caps Lock key.
    public static Key Capslock = new Key(4194329L);
    //
    // Summary:
    //     Num Lock key.
    public static Key Numlock = new Key(4194330L);
    //
    // Summary:
    //     Scroll Lock key.
    public static Key Scrolllock = new Key(4194331L);
    //
    // Summary:
    //     F1 key.
    public static Key F1 = new Key(4194332L);
    //
    // Summary:
    //     F2 key.
    public static Key F2 = new Key(4194333L);
    //
    // Summary:
    //     F3 key.
    public static Key F3 = new Key(4194334L);
    //
    // Summary:
    //     F4 key.
    public static Key F4 = new Key(4194335L);
    //
    // Summary:
    //     F5 key.
    public static Key F5 = new Key(4194336L);
    //
    // Summary:
    //     F6 key.
    public static Key F6 = new Key(4194337L);
    //
    // Summary:
    //     F7 key.
    public static Key F7 = new Key(4194338L);
    //
    // Summary:
    //     F8 key.
    public static Key F8 = new Key(4194339L);
    //
    // Summary:
    //     F9 key.
    public static Key F9 = new Key(4194340L);
    //
    // Summary:
    //     F10 key.
    public static Key F10 = new Key(4194341L);
    //
    // Summary:
    //     F11 key.
    public static Key F11 = new Key(4194342L);
    //
    // Summary:
    //     F12 key.
    public static Key F12 = new Key(4194343L);
    //
    // Summary:
    //     F13 key.
    public static Key F13 = new Key(4194344L);
    //
    // Summary:
    //     F14 key.
    public static Key F14 = new Key(4194345L);
    //
    // Summary:
    //     F15 key.
    public static Key F15 = new Key(4194346L);
    //
    // Summary:
    //     F16 key.
    public static Key F16 = new Key(4194347L);
    //
    // Summary:
    //     F17 key.
    public static Key F17 = new Key(4194348L);
    //
    // Summary:
    //     F18 key.
    public static Key F18 = new Key(4194349L);
    //
    // Summary:
    //     F19 key.
    public static Key F19 = new Key(4194350L);
    //
    // Summary:
    //     F20 key.
    public static Key F20 = new Key(4194351L);
    //
    // Summary:
    //     F21 key.
    public static Key F21 = new Key(4194352L);
    //
    // Summary:
    //     F22 key.
    public static Key F22 = new Key(4194353L);
    //
    // Summary:
    //     F23 key.
    public static Key F23 = new Key(4194354L);
    //
    // Summary:
    //     F24 key.
    public static Key F24 = new Key(4194355L);
    //
    // Summary:
    //     F25 key. Only supported on macOS and Linux due to a Windows limitation.
    public static Key F25 = new Key(4194356L);
    //
    // Summary:
    //     F26 key. Only supported on macOS and Linux due to a Windows limitation.
    public static Key F26 = new Key(4194357L);
    //
    // Summary:
    //     F27 key. Only supported on macOS and Linux due to a Windows limitation.
    public static Key F27 = new Key(4194358L);
    //
    // Summary:
    //     F28 key. Only supported on macOS and Linux due to a Windows limitation.
    public static Key F28 = new Key(4194359L);
    //
    // Summary:
    //     F29 key. Only supported on macOS and Linux due to a Windows limitation.
    public static Key F29 = new Key(4194360L);
    //
    // Summary:
    //     F30 key. Only supported on macOS and Linux due to a Windows limitation.
    public static Key F30 = new Key(4194361L);
    //
    // Summary:
    //     F31 key. Only supported on macOS and Linux due to a Windows limitation.
    public static Key F31 = new Key(4194362L);
    //
    // Summary:
    //     F32 key. Only supported on macOS and Linux due to a Windows limitation.
    public static Key F32 = new Key(4194363L);
    //
    // Summary:
    //     F33 key. Only supported on macOS and Linux due to a Windows limitation.
    public static Key F33 = new Key(4194364L);
    //
    // Summary:
    //     F34 key. Only supported on macOS and Linux due to a Windows limitation.
    public static Key F34 = new Key(4194365L);
    //
    // Summary:
    //     F35 key. Only supported on macOS and Linux due to a Windows limitation.
    public static Key F35 = new Key(4194366L);
    //
    // Summary:
    //     Multiply (*) key on the numeric keypad.
    public static Key KpMultiply = new Key(4194433L);
    //
    // Summary:
    //     Divide (/) key on the numeric keypad.
    public static Key KpDivide = new Key(4194434L);
    //
    // Summary:
    //     Subtract (-) key on the numeric keypad.
    public static Key KpSubtract = new Key(4194435L);
    //
    // Summary:
    //     Period (.) key on the numeric keypad.
    public static Key KpPeriod = new Key(4194436L);
    //
    // Summary:
    //     Add (+) key on the numeric keypad.
    public static Key KpAdd = new Key(4194437L);
    //
    // Summary:
    //     Number 0 on the numeric keypad.
    public static Key Kp0 = new Key(4194438L);
    //
    // Summary:
    //     Number 1 on the numeric keypad.
    public static Key Kp1 = new Key(4194439L);
    //
    // Summary:
    //     Number 2 on the numeric keypad.
    public static Key Kp2 = new Key(4194440L);
    //
    // Summary:
    //     Number 3 on the numeric keypad.
    public static Key Kp3 = new Key(4194441L);
    //
    // Summary:
    //     Number 4 on the numeric keypad.
    public static Key Kp4 = new Key(4194442L);
    //
    // Summary:
    //     Number 5 on the numeric keypad.
    public static Key Kp5 = new Key(4194443L);
    //
    // Summary:
    //     Number 6 on the numeric keypad.
    public static Key Kp6 = new Key(4194444L);
    //
    // Summary:
    //     Number 7 on the numeric keypad.
    public static Key Kp7 = new Key(4194445L);
    //
    // Summary:
    //     Number 8 on the numeric keypad.
    public static Key Kp8 = new Key(4194446L);
    //
    // Summary:
    //     Number 9 on the numeric keypad.
    public static Key Kp9 = new Key(4194447L);
    //
    // Summary:
    //     Context menu key.
    public static Key Menu = new Key(4194370L);
    //
    // Summary:
    //     Hyper key. (On Linux/X11 only).
    public static Key Hyper = new Key(4194371L);
    //
    // Summary:
    //     Help key.
    public static Key Help = new Key(4194373L);
    //
    // Summary:
    //     Media back key. Not to be confused with the Back button on an Android device.
    public static Key Back = new Key(4194376L);
    //
    // Summary:
    //     Media forward key.
    public static Key Forward = new Key(4194377L);
    //
    // Summary:
    //     Media stop key.
    public static Key Stop = new Key(4194378L);
    //
    // Summary:
    //     Media refresh key.
    public static Key Refresh = new Key(4194379L);
    //
    // Summary:
    //     Volume down key.
    public static Key Volumedown = new Key(4194380L);
    //
    // Summary:
    //     Mute volume key.
    public static Key Volumemute = new Key(4194381L);
    //
    // Summary:
    //     Volume up key.
    public static Key Volumeup = new Key(4194382L);
    //
    // Summary:
    //     Media play key.
    public static Key Mediaplay = new Key(4194388L);
    //
    // Summary:
    //     Media stop key.
    public static Key Mediastop = new Key(4194389L);
    //
    // Summary:
    //     Previous song key.
    public static Key Mediaprevious = new Key(4194390L);
    //
    // Summary:
    //     Next song key.
    public static Key Medianext = new Key(4194391L);
    //
    // Summary:
    //     Media record key.
    public static Key Mediarecord = new Key(4194392L);
    //
    // Summary:
    //     Home page key.
    public static Key Homepage = new Key(4194393L);
    //
    // Summary:
    //     Favorites key.
    public static Key Favorites = new Key(4194394L);
    //
    // Summary:
    //     Search key.
    public static Key Search = new Key(4194395L);
    //
    // Summary:
    //     Standby key.
    public static Key Standby = new Key(4194396L);
    //
    // Summary:
    //     Open URL / Launch Browser key.
    public static Key Openurl = new Key(4194397L);
    //
    // Summary:
    //     Launch Mail key.
    public static Key Launchmail = new Key(4194398L);
    //
    // Summary:
    //     Launch Media key.
    public static Key Launchmedia = new Key(4194399L);
    //
    // Summary:
    //     Launch Shortcut 0 key.
    public static Key Launch0 = new Key(4194400L);
    //
    // Summary:
    //     Launch Shortcut 1 key.
    public static Key Launch1 = new Key(4194401L);
    //
    // Summary:
    //     Launch Shortcut 2 key.
    public static Key Launch2 = new Key(4194402L);
    //
    // Summary:
    //     Launch Shortcut 3 key.
    public static Key Launch3 = new Key(4194403L);
    //
    // Summary:
    //     Launch Shortcut 4 key.
    public static Key Launch4 = new Key(4194404L);
    //
    // Summary:
    //     Launch Shortcut 5 key.
    public static Key Launch5 = new Key(4194405L);
    //
    // Summary:
    //     Launch Shortcut 6 key.
    public static Key Launch6 = new Key(4194406L);
    //
    // Summary:
    //     Launch Shortcut 7 key.
    public static Key Launch7 = new Key(4194407L);
    //
    // Summary:
    //     Launch Shortcut 8 key.
    public static Key Launch8 = new Key(4194408L);
    //
    // Summary:
    //     Launch Shortcut 9 key.
    public static Key Launch9 = new Key(4194409L);
    //
    // Summary:
    //     Launch Shortcut A key.
    public static Key Launcha = new Key(4194410L);
    //
    // Summary:
    //     Launch Shortcut B key.
    public static Key Launchb = new Key(4194411L);
    //
    // Summary:
    //     Launch Shortcut C key.
    public static Key Launchc = new Key(4194412L);
    //
    // Summary:
    //     Launch Shortcut D key.
    public static Key Launchd = new Key(4194413L);
    //
    // Summary:
    //     Launch Shortcut E key.
    public static Key Launche = new Key(4194414L);
    //
    // Summary:
    //     Launch Shortcut F key.
    public static Key Launchf = new Key(4194415L);
    //
    // Summary:
    //     Unknown key.
    public static Key Unknown = new Key(0x7FFFFFL);
    //
    // Summary:
    //     Space key.
    public static Key Space = new Key(0x20L);
    //
    // Summary:
    //     ! key.
    public static Key Exclam = new Key(33L);
    //
    // Summary:
    //     " key.
    public static Key Quotedbl = new Key(34L);
    //
    // Summary:
    //     # key.
    public static Key Numbersign = new Key(35L);
    //
    // Summary:
    //     $ key.
    public static Key Dollar = new Key(36L);
    //
    // Summary:
    //     % key.
    public static Key Percent = new Key(37L);
    //
    // Summary:
    //     & key.
    public static Key Ampersand = new Key(38L);
    //
    // Summary:
    //     ' key.
    public static Key Apostrophe = new Key(39L);
    //
    // Summary:
    //     ( key.
    public static Key Parenleft = new Key(40L);
    //
    // Summary:
    //     ) key.
    public static Key Parenright = new Key(41L);
    //
    // Summary:
    //     * key.
    public static Key Asterisk = new Key(42L);
    //
    // Summary:
    //     + key.
    public static Key Plus = new Key(43L);
    //
    // Summary:
    //     , key.
    public static Key Comma = new Key(44L);
    //
    // Summary:
    //     - key.
    public static Key Minus = new Key(45L);
    //
    // Summary:
    //     . key.
    public static Key Period = new Key(46L);
    //
    // Summary:
    //     / key.
    public static Key Slash = new Key(47L);
    //
    // Summary:
    //     Number 0 key.
    public static Key Key0 = new Key(48L);
    //
    // Summary:
    //     Number 1 key.
    public static Key Key1 = new Key(49L);
    //
    // Summary:
    //     Number 2 key.
    public static Key Key2 = new Key(50L);
    //
    // Summary:
    //     Number 3 key.
    public static Key Key3 = new Key(51L);
    //
    // Summary:
    //     Number 4 key.
    public static Key Key4 = new Key(52L);
    //
    // Summary:
    //     Number 5 key.
    public static Key Key5 = new Key(53L);
    //
    // Summary:
    //     Number 6 key.
    public static Key Key6 = new Key(54L);
    //
    // Summary:
    //     Number 7 key.
    public static Key Key7 = new Key(55L);
    //
    // Summary:
    //     Number 8 key.
    public static Key Key8 = new Key(56L);
    //
    // Summary:
    //     Number 9 key.
    public static Key Key9 = new Key(57L);
    //
    // Summary:
    //     : key.
    public static Key Colon = new Key(58L);
    //
    // Summary:
    //     ; key.
    public static Key Semicolon = new Key(59L);
    //
    // Summary:
    //     < key.
    public static Key Less = new Key(60L);
    //
    // Summary:
    //     = new Key(key.
    public static Key Equal = new Key(61L);
    //
    // Summary:
    //     > key.
    public static Key Greater = new Key(62L);
    //
    // Summary:
    //     ? key.
    public static Key Question = new Key(0x3FL);
    //
    // Summary:
    //     @ key.
    public static Key At = new Key(0x40L);
    //
    // Summary:
    //     A key.
    public static Key A = new Key(65L);
    //
    // Summary:
    //     B key.
    public static Key B = new Key(66L);
    //
    // Summary:
    //     C key.
    public static Key C = new Key(67L);
    //
    // Summary:
    //     D key.
    public static Key D = new Key(68L);
    //
    // Summary:
    //     E key.
    public static Key E = new Key(69L);
    //
    // Summary:
    //     F key.
    public static Key F = new Key(70L);
    //
    // Summary:
    //     G key.
    public static Key G = new Key(71L);
    //
    // Summary:
    //     H key.
    public static Key H = new Key(72L);
    //
    // Summary:
    //     I key.
    public static Key I = new Key(73L);
    //
    // Summary:
    //     J key.
    public static Key J = new Key(74L);
    //
    // Summary:
    //     K key.
    public static Key K = new Key(75L);
    //
    // Summary:
    //     L key.
    public static Key L = new Key(76L);
    //
    // Summary:
    //     M key.
    public static Key M = new Key(77L);
    //
    // Summary:
    //     N key.
    public static Key N = new Key(78L);
    //
    // Summary:
    //     O key.
    public static Key O = new Key(79L);
    //
    // Summary:
    //     P key.
    public static Key P = new Key(80L);
    //
    // Summary:
    //     Q key.
    public static Key Q = new Key(81L);
    //
    // Summary:
    //     R key.
    public static Key R = new Key(82L);
    //
    // Summary:
    //     S key.
    public static Key S = new Key(83L);
    //
    // Summary:
    //     T key.
    public static Key T = new Key(84L);
    //
    // Summary:
    //     U key.
    public static Key U = new Key(85L);
    //
    // Summary:
    //     V key.
    public static Key V = new Key(86L);
    //
    // Summary:
    //     W key.
    public static Key W = new Key(87L);
    //
    // Summary:
    //     X key.
    public static Key X = new Key(88L);
    //
    // Summary:
    //     Y key.
    public static Key Y = new Key(89L);
    //
    // Summary:
    //     Z key.
    public static Key Z = new Key(90L);
    //
    // Summary:
    //     [ key.
    public static Key Bracketleft = new Key(91L);
    //
    // Summary:
    //     \ key.
    public static Key Backslash = new Key(92L);
    //
    // Summary:
    //     ] key.
    public static Key Bracketright = new Key(93L);
    //
    // Summary:
    //     ^ key.
    public static Key Asciicircum = new Key(94L);
    //
    // Summary:
    //     _ key.
    public static Key Underscore = new Key(95L);
    //
    // Summary:
    //     ` key.
    public static Key Quoteleft = new Key(96L);
    //
    // Summary:
    //     { key.
    public static Key Braceleft = new Key(123L);
    //
    // Summary:
    //     | key.
    public static Key Bar = new Key(124L);
    //
    // Summary:
    //     } key.
    public static Key Braceright = new Key(125L);
    //
    // Summary:
    //     ~ key.
    public static Key Asciitilde = new Key(126L);
    //
    // Summary:
    //     ¥ key.
    public static Key Yen = new Key(165L);
    //
    // Summary:
    //     § key.
    public static Key Section = new Key(167L);
    //
    // Summary:
    //     "Globe" key on Mac / iPad keyboard.
    public static Key Globe = new Key(4194416L);
    //
    // Summary:
    //     "On-screen keyboard" key iPad keyboard.
    public static Key Keyboard = new Key(4194417L);
    //
    // Summary:
    //     英数 key on Mac keyboard.
    public static Key JisEisu = new Key(4194418L);
    //
    // Summary:
    //     かな key on Mac keyboard.
    public static Key JisKana = new Key(4194419L);

    public Key(long keyCode)
    {
        this.keyCode = keyCode;
    }

    public override bool Equals(object? obj)
    {
        return obj is Key key &&
               keyCode == key.keyCode;
    }

    public override int GetHashCode()
    {
        return (int)keyCode;
    }

    public override string? ToString()
    {
        return keyCode.ToString();
    }
}