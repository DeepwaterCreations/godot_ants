﻿using Prototypist.TaskChain;
using System;
using System.Collections.Generic;
using System.Runtime.Intrinsics;
using System.Threading.Tasks;
using System.Threading;

public class TrainPathTicket {
    public TrainPaths value;
    public int ticketToBeat;

    public TrainPathTicket(TrainPaths value, int ticketToBeat)
    {
        this.value = value ?? throw new ArgumentNullException(nameof(value));
        this.ticketToBeat = ticketToBeat;
    }
}

public class TrainPathingHolder
{
    public TrainPathTicket value;
    HashSet<Vector128<double>> last;

    int ticketSource = 0;

    public TrainPathingHolder(TrainPaths trainPaths, HashSet<Vector128<double>> stations)
    {
        value = new TrainPathTicket(trainPaths, ticketSource);
        last = stations;
    }

    public void Update(HashSet<Vector128<double>> stations)
    {
        // tickets ensure train paths submitted later overwrite paths submitted earlier
        var myTicket = Interlocked.Increment(ref ticketSource);

        var outterPressure = 0;

        while (true)
        {
            outterPressure++;
            if (outterPressure > 1000) {
                Log.WriteLine("couldn't update train pathing");
                throw new Exception("couldn't update train pathing");
            }

            var localLast = last;

            if (!localLast.SetEquals(stations))
            {
                if (Interlocked.CompareExchange(ref last, stations, localLast) == localLast)
                {
                    Task.Run(() =>
                    {
                        var myValue = new TrainPathTicket(TrainPathing.UpdatePaths(value.value, stations), myTicket);

                        var innerPressure = 0;

                        while (true)
                        {
                            innerPressure++;
                            if (outterPressure > 1000)
                            {
                                Log.WriteLine("inner couldn't update train pathing");
                                throw new Exception("inner couldn't update train pathing");
                            }

                            var local = value;
                            if (myTicket < value.ticketToBeat)
                            {
                                if (Interlocked.CompareExchange(ref value, myValue, local) == local)
                                {
                                    break;
                                }
                            }
                            else
                            {
                                break;
                            }
                        }
                    }).ContinueWith(x =>
                    {
                        if (x.IsFaulted)
                        {
                            Log.WriteLine("failed to generage train paths");
                        }
                    });
                    break;
                }
            }
            else {
                break;
            }
        }
    }

}
