using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Reflection;
//using System.Numerics;
using System.Runtime.InteropServices;
using System.Runtime.Intrinsics;
using System.Runtime.Intrinsics.X86;
using System.Threading.Tasks;
using Ants.Domain.Infrastructure;
using Ants.Domain.State;
using Prototypist.Toolbox.IEnumerable;
using static System.Net.WebRequestMethods;
using static Pathing;
using File = System.IO.File;


public class PathSegment
{
    public readonly Vector128<double> from;
    public readonly Vector128<double> to;

    public PathSegment(Vector128<double> from, Vector128<double> to)
    {
        this.from = from;
        this.to = to;
    }

    public override bool Equals(object? obj)
    {
        return obj is PathSegment segment &&
               from.Equals(segment.from) &&
               to.Equals(segment.to);
    }

    public override int GetHashCode()
    {
        return HashCode.Combine(from, to);
    }
}

public class TrainPaths : Paths {
    public readonly PathSegment[] pathSegments;

    public TrainPaths(Dictionary<Vector128<double>, Reachable> reachables, Mode mode, IReadOnlyList<(Vector128<double> globalPosition, double radius)> cantEnter, Map2<MudState> mudMap, double avoidBy, PathSegment[] pathSegments) : base(reachables, mode, cantEnter, mudMap, avoidBy)
    {
        this.pathSegments = pathSegments ?? throw new ArgumentNullException(nameof(pathSegments));
    }

    public override IEnumerable<(string, object?)> HashData()
    {
        foreach (var item in base.HashData())
        {
            yield return item;
        }
        yield return (nameof(pathSegments), pathSegments);
    }
}

public class Paths : IReadOnlyStateHash
{
    public readonly Dictionary<Vector128<double>, Reachable> reachables;
    public readonly IReadOnlyList<(Vector128<double> globalPosition, double radius)> cantEnter;
    public readonly Map2<MudState> mudMap;
    public readonly double avoidBy;
    public readonly Mode mode;

    // we were actually spending a lot of time on the hash
    // currently this is immutable so we save the value
    public int? ReadOnlyHash { get; set; }

    public Paths(Dictionary<Vector128<double>, Reachable> reachables, Mode mode, IReadOnlyList<(Vector128<double> globalPosition, double radius)> cantEnter, Map2<MudState> mudMap, double avoidBy)
    {
        this.reachables = reachables ?? throw new ArgumentNullException(nameof(reachables));
        this.mode = mode;
        this.cantEnter = cantEnter ?? throw new ArgumentNullException(nameof(cantEnter));
        this.mudMap = mudMap ?? throw new ArgumentNullException(nameof(mudMap));
        this.avoidBy = avoidBy;
    }

    public virtual IEnumerable<(string, object?)> HashData()
    {
        yield return (nameof(reachables), reachables);
        yield return (nameof(cantEnter), cantEnter);
        yield return (nameof(mudMap), mudMap);
        yield return (nameof(avoidBy), avoidBy);
        yield return (nameof(mode), mode);
    }

    //public override int GetHashCode()
    //    => this.ReadOnlyHash;
}

// I think it would be possible to do some rewrting around OptimiziePath
// you have a dict<(vec from, vec to), list<vec>>
// you'd optimize each path in the dict
// wouldn't that be really slow tho?
// you end up optimizing the same thing over and over

// maybe I could on each OptimiziePath on everything after OptimizeReachables in BuildKnownPaths
// I think that would make OptimiziePath less work on GetPath3
// this is fine for now

public class TrainPathing 
{
    // TODO
    // would be good to adjust path so you can do turns a little easier

    public const int StationDistance = 50;
    public static TrainPaths UpdatePaths(TrainPaths lastPaths, HashSet<Vector128<double>> stations)
    {
        var segments = GetSegments(stations.ToArray(), lastPaths.cantEnter, lastPaths.avoidBy);

        stations = segments.SelectMany(x => new[] { x.from, x.to }).ToHashSet();

        // keep the old paths
        // where all the points are newStations
        var updated = lastPaths.reachables
            .Where(x => stations.Contains(x.Key))
            .ToDictionary(x => x.Key, x => new Reachable(x.Value.distanceTo.Where(pair => pair.Value.ToPoints().All(point => stations.Contains(point))).ToDictionary(x => x.Key, x => x.Value)));

        // add any direct paths
        DirectPaths(lastPaths.mudMap, lastPaths.mode, segments, updated);

        OptimizeReachables(updated, stations.ToArray());

        return new TrainPaths(updated, lastPaths.mode, lastPaths.cantEnter, lastPaths.mudMap, lastPaths.avoidBy, segments.ToArray());
    }

    private static void DirectPaths(Map2<MudState> terrainMap, Mode mode, List<PathSegment> segments, Dictionary<Vector128<double>, Reachable> updated)
    {
        foreach (var segment in segments)
        {
            {
                if (!updated.TryGetValue(segment.from, out var reachables))
                {
                    reachables = new Reachable(new Dictionary<Vector128<double>, Path2>());
                    updated[segment.from] = reachables;
                }
                reachables.distanceTo[segment.to] = new ConcretePath2(new[] { segment.from, segment.to }, terrainMap, mode);
            }
            {
                if (!updated.TryGetValue(segment.to, out var reachables))
                {
                    reachables = new Reachable(new Dictionary<Vector128<double>, Path2>());
                    updated[segment.to] = reachables;
                }
                reachables.distanceTo[segment.from] = new ConcretePath2(new[] { segment.to, segment.from }, terrainMap, mode);
            }
        }
    }

    public static List<PathSegment> GetSegments(Vector128<double>[] stations,
        IReadOnlyList<(Vector128<double> globalPosition, double radius)> cantEnter,
        double avoidBy) 
    {
        var directPaths = new List<PathSegment>();

        for (int i = 0; i < stations.Length; i++)
        {
            var from = stations[i];
            for (int j = i + 1; j < stations.Length; j++)
            {
                var to = stations[j];
                if (Avx.Subtract(to, from).Length() < StationDistance && IsAPathBetween(from, to, cantEnter, avoidBy))
                {
                    directPaths.Add(new PathSegment(from, to));
                }
            }
        }

        var good = new List<PathSegment>();
        while (directPaths.Any()) { 
            var toAdd = directPaths[0];
            directPaths.RemoveAt(0);

            foreach (var goodPathSegment in good)
            {
                var intersection = Intersect(toAdd.from, toAdd.to, goodPathSegment.from, goodPathSegment.to);
                if (intersection != null ) {
                    directPaths.Add(new PathSegment(goodPathSegment.from, intersection.Value.point));
                    directPaths.Add(new PathSegment(intersection.Value.point, goodPathSegment.to));
                    directPaths.Add(new PathSegment(toAdd.from, intersection.Value.point));
                    directPaths.Add(new PathSegment(intersection.Value.point, toAdd.to));
                    good.Remove(goodPathSegment);
                    goto next;
                }
            }

            good.Add(toAdd);
        next:;
        }

        return good;
    }

    //private static void AddDirectionConnections(HashSet<Vector128<double>> stations, double avoidBy, 
    //    Dictionary<Vector128<double>, Reachable> updated,
    //    IReadOnlyList<(Vector128<double> globalPosition, double radius)> cantEnter,
    //    TerrainMap terrainMap,
    //    Mode mode
    //    )
    //{
    //    foreach (var from in stations)
    //    {
    //        if (!updated.TryGetValue(from, out var reachables))
    //        {
    //            reachables = new Reachable(new Dictionary<Vector128<double>, Path2>());
    //            updated[from] = reachables;
    //        }

    //        foreach (var to in stations)
    //        {
    //            if (Avx.Subtract(to, from).Length() < StationDistance && IsAPathBetween(from, to, cantEnter, avoidBy))
    //            {
    //                updated[from].distanceTo[to] = new ConcretePath2(new[] { from, to }, terrainMap, mode);
    //            }
    //        }
    //    }
    //}

    public static TrainPaths BuildKnownPaths(HashSet<Vector128<double>> stations, IReadOnlyList<(Vector128<double> globalPosition, double radius)> avoidables, Map2<MudState> mudMap, Mode mode, double avoidBy) {
         
        var cantPassThrough = mode == Mode.ImpassableMud ? avoidables.Union(mudMap.Backing().Select(x=>(x.position, x.physicsImmunatbleInner.radius))).ToArray() : avoidables;

        var segments = GetSegments(stations.ToArray(), cantPassThrough, avoidBy);

        var res = new Dictionary<Vector128<double>, Reachable>();

        DirectPaths(mudMap, mode, segments, res);

        OptimizeReachables(res, stations.ToArray());

        return new TrainPaths(res, mode, cantPassThrough, mudMap, avoidBy, segments.ToArray());
    }

    public static async Task<Dictionary<(Mode, double), TrainPaths>> BuildKnownPaths(HashSet<Vector128<double>> stations, IReadOnlyList<(Vector128<double> globalPosition, double radius)> avoidables, Map2<MudState> mudMap) {
        var dict = new Dictionary<(Mode, double),Task<TrainPaths>>();
        foreach (var size in new[] { Pathing.unitSize_Small, Pathing.unitSize_Medium, Pathing.unitSize_Large })
        {
            foreach (var mode in new[] { Mode.ImpassableMud, Mode.SlowMud, Mode.IgnoreMud })
            {
                dict[(mode, size)] = Task.Run((Func<TrainPaths>)(() =>
                {
                    return BuildKnownPaths(stations, avoidables, mudMap, mode, size);
                }));
            }
        }

        await Task.WhenAll<TrainPaths>(dict.Values).ConfigureAwait(false);
        Log.WriteLine("finished building train paths");
        return dict.ToDictionary(x=>x.Key, x=>x.Value.Result);
    }

    internal static bool GetPath(TrainPaths paths, Vector128<double> from, Vector128<double> to, [MaybeNullWhen(false)] out List<Vector128<double>> res) { 
        // we need to find what station we can reach
        var nearStart = ReachableStations(paths.pathSegments.SelectMany(x => new[] { x.from, x.to}).ToHashSet(), from, paths.avoidBy, paths.cantEnter);

        if (!nearStart.Any()) {
            res = default;
            return false;
        }

        (double dis, Vector128<double> point)? winner = null;
        foreach (var segment in paths.pathSegments)
        {
            var point = ClosestPointOnSegmentTo(segment.from, segment.to, to);
            var dis = Avx.Subtract(point, to).Length();
            if (dis < 200 && (winner == null || winner.Value.dis < dis))
            {
                winner = (dis, point);
            }
        }
        
        if (winner == null)
        {
            res = default;
            return false;
        }

        var maybeRes = FindBestPath(paths, 
            nearStart.Select(x => new ConcretePath2(new[] { from, x }, paths.mudMap, paths.mode)).ToArray(), 
            nearStart.Select(x => new ConcretePath2(new[] { from, x }, paths.mudMap, paths.mode)).ToArray());

        if (maybeRes == null)
        {
            res = default;
            return false;
        }

        res = maybeRes.ToPoints();
        return true;

    }

    public static HashSet<Vector128<double>> ReachableStations(HashSet<Vector128<double>> stations, Vector128<double> at, double minDistanceTotracks, IReadOnlyList<(Vector128<double> globalPosition, double radius)> cantEnter)
    {
        var near = stations.Where(x => Avx.Subtract(x, at).Length() < StationDistance).ToArray();

        var res = new HashSet<Vector128<double>>();

        for (int i = 0; i < near.Length; i++)
        {
            var from = near[i];
            var distance = Avx.Subtract(from, at).Length();
            for (int j = i + 1; j < near.Length; j++)
            {
                var to = near[j];
                if (IsAPathBetween(from, to, cantEnter, minDistanceTotracks))
                {
                    if (Avx.Subtract(at, Avx.Add(from, Avx.Multiply(Avx.Subtract(to, from).NormalizedCopy(), distance.AsVector()))).Length() < minDistanceTotracks)
                    {
                        res.Add(to);
                        res.Add(from);
                    }
                }
            }
        }
        return res;
    }

}

public partial class Pathing
{
    public const double eyeball_lenght = 50;
    public const double unitSize_Small = 15;
    public const double unitSize_Medium = 30;
    public const double unitSize_Large = 60;

    //   public static Task<Paths> BuildKnownPath(
    //	IReadOnlyList<(Vector128<double> globalPosition, double radius)> avoidables,
    //       IReadOnlyList<(Vector128<double> globalPosition, double radius)> mud,
    //       TerrainMap terrainMap,
    //       Mode mode)  {

    //	return Task.Run(() =>
    //	{
    //		return BuildKnownPaths(avoidables, mud, terrainMap, mode, Pathing.unitSize);
    //	});
    //}

    private const double stepSize = 10.0;

    // this is probably going to go away...
    //public static Dictionary<Vector128<double>, Reachable> knownPaths = null;

    public static bool GetPathWithFristPoint(Paths paths, Vector128<double> from, Vector128<double> to, [MaybeNullWhen(false)] out List<Vector128<double>> res) {
        if (paths == null)
        {
            throw new ArgumentNullException(nameof(paths));
        }

        if (!TryAdjustTarget(to, paths.cantEnter, paths.avoidBy, out var nextTo))
        {
            res = null;
            return false;
        }
        else
        {
            to = nextTo;
        }

        if (!TryAdjustTarget(from, paths.cantEnter, paths.avoidBy, out var nextFrom))
        {
            Log.WriteLine("starting from illegal position");
            res = null;
            return false;
        }

        if (FindPath(paths, nextFrom, to, out var path))
        {
            if (!nextFrom.Equals(from))
            {
                res = new List<Vector128<double>>();
                res.Add(from);
                res.AddRange(path.ToPoints());
            }
            else
            {
                res = path.ToPoints();
            }
            res = OptimizePath(res, paths).ToList();

            return true;
        }
        res = null;
        return false;
    }

    public static bool GetPath3(Paths paths, Vector128<double> from, Vector128<double> to, [MaybeNullWhen(false)] out List<Vector128<double>> res)
    {
        if (GetPathWithFristPoint(paths, from, to, out var innerRes)) {
            res = innerRes.Skip(1).ToList();
            return true;
        }
        res = null;
        return false;
    }

    // path are optimal given the points but the are built around a very limited set of points
    // additional points are generated to help the path curve around rocks
    // we then need to reoptimize using these points
    public static List<Vector128<double>> OptimizePath(List<Vector128<double>> points, Paths paths) 
    { 
        return OptimizePath(points, paths.mudMap, paths.mode, paths.avoidBy, paths.cantEnter);
    }

    public static List<Vector128<double>> OptimizePath(List<Vector128<double>> points,Map2<MudState> mudMap,  Mode mudMode, double avoidBy, IReadOnlyList<(Vector128<double> globalPosition, double radius)> cantEnter)
    {
        var (pointsDict, pointsArray) = DirectPaths(mudMap, mudMode, avoidBy, cantEnter, points.ToArray());

        OptimizeReachables(pointsDict, pointsArray);

        var path = pointsDict[points.First()].distanceTo[points.Last()];

        return path.ToPoints();
    }

    //internal static bool GetPath2(IReadOnlyList<(Vector128<double> globalPosition, double radius)> avoidables, Vector128<double> from, Vector128<double> to, double avoidBy,[MaybeNullWhen(false)] out List<Vector128<double>> res)
    //{
    //    return GetPath3(knownPaths, avoidables, from, to, avoidBy, out res);
    //}

    public abstract class Path2 : IComparable<Path2>, IStateHash
    {

        public abstract Vector128<double> Last();
        public abstract Vector128<double> First();
        // this isn't exactly length
        // it's travel time at speed 1
        public abstract double TravelTime();
        public abstract List<Vector128<double>> ToPoints();
        public int CompareTo(Path2? other)
        {
            if (other == null)
            {
                return 1;
            }

            return TravelTime().CompareTo(other.TravelTime());
        }
        internal CompositPath Combine(Path2 distanceTo)
        {
            return new CompositPath(this, distanceTo);
        }

        internal abstract Path2 Reverse();
        public abstract IEnumerable<(string, object?)> HashData();
    }

    public partial class ConcretePath2 : Path2
    {
        readonly Vector128<double>[] path;
        // as speed 1
        private readonly double travelTime;

        public ConcretePath2(Vector128<double>[] path, double travelTime)
        {
            this.path = path ?? throw new ArgumentNullException(nameof(path));
            this.travelTime = travelTime;
        }

        private static IEnumerable<Vector128<double>> RemoveDups(IEnumerable<Vector128<double>> path) {
            Vector128<double>? last = null;
            foreach (var item in path)
            {
                if (last.HasValue && item.Equals(last.Value)) {
                    continue;
                }
                yield return item;
                last = item;
            }
        }

        public ConcretePath2(Vector128<double>[] path, Map2<MudState> mudMap, Mode mode)
        {
            path = RemoveDups(path).ToArray();

            this.path = path;

            if (mode == Mode.SlowMud)
            {
                var res = 0.0;
                for (int i = 0; i < path.Length - 1; i++)
                {

                    var normal = Avx.Subtract(path[i + 1], path[i]).NormalizedCopy();

                    // two so we don't sameple the same spot twice
                    var length = Avx.Subtract(path[i + 1], path[i]).Length();
                    var steps = Math.Ceiling(length / (MapExtensions.MAP_GRID_SIZE * 2));

                    var stepLength = length / steps;
                    for (int atStep = 0; atStep < steps; atStep++)
                    {
                        var mapPosition = Avx.Add(path[i], Avx.Multiply(normal, (stepLength * atStep).AsVector()));
                        res += mudMap.Hit(mapPosition, 0).Any() ? 2 * stepLength : stepLength;
                    }
                }
                travelTime = res;
            }
            else
            {
                var res = 0.0;
                for (int i = 0; i < path.Length - 1; i++)
                {
                    res += Avx.Subtract(path[i + 1], path[i]).Length();
                }
                travelTime = res;
            }
        }

        //public override bool Equals(object? obj)
        //{
        //    return obj is ConcretePath2 path &&
        //           this.path.SequenceEqual(path.path);
        //}

        public override Vector128<double> First() => path.First();

        //public override int GetHashCode()
        //{
        //    return this.Hash();
        //}

        public override Vector128<double> Last() => path.Last();

        public override double TravelTime() => travelTime;


        public override List<Vector128<double>> ToPoints()
        {
            return path.ToList();
        }

        internal override Path2 Reverse()
        {
            return new ConcretePath2(path.Reverse().ToArray(), travelTime);
        }

        public override IEnumerable<(string, object?)> HashData()
        {
            yield return (nameof(travelTime), travelTime);
            yield return (nameof(path), path);
        }
    }

    public partial class CompositPath : Path2
    {
        private Path2 path1;
        private Path2 path2;
        private double travelTime;

        public CompositPath(Path2 path1, Path2 path2)
        {
            this.path1 = path1 ?? throw new ArgumentNullException(nameof(path1));
            this.path2 = path2 ?? throw new ArgumentNullException(nameof(path2));
            travelTime = path1.TravelTime() + path2.TravelTime();
        }

        public override double TravelTime()
        {
            return travelTime;
        }

        public override List<Vector128<double>> ToPoints()
        {
            var res = new List<Vector128<double>>();
            foreach (var item in path1.ToPoints())
            {
                res.Add(item);
            }
            foreach (var item in path2.ToPoints().Skip(1))
            {
                res.Add(item);
            }
            return res;
        }

        internal void Update(CompositPath shortestPath)
        {
            this.path1 = shortestPath.path1;
            this.path2 = shortestPath.path2;
            travelTime = path1.TravelTime() + path2.TravelTime();
        }

        internal override Path2 Reverse()
        {
            return new ReversePath(this);
        }

        public override Vector128<double> Last() => path2.Last();

        public override Vector128<double> First() => path1.First();

        //public override bool Equals(object? obj)
        //{
        //    return obj is CompositPath path &&
        //           EqualityComparer<Path2>.Default.Equals(path1, path.path1) &&
        //           EqualityComparer<Path2>.Default.Equals(path2, path.path2);
        //}

        //public override int GetHashCode()
        //{
        //    return this.Hash();
        //}


        public override IEnumerable<(string, object?)> HashData()
        {
            yield return (nameof(path1), path1);
            yield return (nameof(path2), path2);
            yield return (nameof(travelTime), travelTime);
        }
    }

    public partial class ReversePath : Path2
    {
        private readonly Path2 shortestPath;

        public ReversePath(Path2 shortestPath)
        {
            this.shortestPath = shortestPath;
        }

        //public override bool Equals(object? obj)
        //{
        //    return obj is ReversePath path &&
        //           EqualityComparer<Path2>.Default.Equals(shortestPath, path.shortestPath);
        //}

        public override Vector128<double> First() => shortestPath.Last();

        //public override int GetHashCode()
        //{
        //    return this.Hash();
        //}

        public override Vector128<double> Last() => shortestPath.First();

        public override double TravelTime() => shortestPath.TravelTime();

        public override List<Vector128<double>> ToPoints()
        {
            var list = shortestPath.ToPoints();
            list.Reverse();
            return list;
        }

        internal override Path2 Reverse()
        {
            return shortestPath;
        }

        public override IEnumerable<(string, object?)> HashData()
        {
            yield return (nameof(shortestPath), shortestPath);
        }
    }

    public partial class Reachable: IStateHash
    {
        public readonly Dictionary<Vector128<double>, Path2> distanceTo;

        public Reachable(Dictionary<Vector128<double>, Path2> distanceTo)
        {
            this.distanceTo = distanceTo ?? throw new ArgumentNullException(nameof(distanceTo));
        }

        //public override bool Equals(object? obj)
        //{
        //    return obj is Reachable reachable &&
        //           distanceTo.SetEqual(reachable.distanceTo);
        //}

        //public override int GetHashCode() => this.Hash();

        public IEnumerable<(string, object?)> HashData()
        {
            yield return (nameof(distanceTo), distanceTo);
        }
    }

    public static bool FindPath(
        Paths paths,
        Vector128<double> from,
        Vector128<double> to,
        [MaybeNullWhen(false)] out Path2 res)
    {
        Path2? bestPath = null;

        // if there is a direction path, take it

        if (IsAPathBetween(from, to, paths.cantEnter, paths.avoidBy))
        {
            bestPath = new ConcretePath2(new[] { from, to }, paths.mudMap, paths.mode);
            // however not if the unit is slowed by mud, the direct path might be over mud while it's quicker to go around
            if (paths.mode != Mode.SlowMud)
            {
                res = bestPath;
                return true;
            }
        }

        var startCanReachs = paths.reachables.Keys
            .Where(key => IsAPathBetween(from, key, paths.cantEnter, paths.avoidBy))
            .Select(key => (Path2)new ConcretePath2(new[] { from, key }, paths.mudMap, paths.mode))
            .ToArray();


        startCanReachs = startCanReachs.Union(
            paths.reachables.Keys.Except(startCanReachs.Select(x => x.Last()))
            .SelectMany(key =>
            {
                if (TryAdjustedPathBetween(from, key, paths.cantEnter, paths.avoidBy, out var adjusted))
                {
                    return new Path2[] { new ConcretePath2(adjusted, paths.mudMap, paths.mode) };
                }
                return new Path2[] { };
            })).ToArray();

        var endCanReachs = paths.reachables.Keys
            .Where(key => IsAPathBetween(key, to, paths.cantEnter, paths.avoidBy))
            .Select(key => (Path2)new ConcretePath2(new[] { key, to }, paths.mudMap, paths.mode))
            .ToArray();

        endCanReachs = endCanReachs.Union(
            paths.reachables.Keys.Except(endCanReachs.Select(x => x.First()))
            .SelectMany(key =>
            {
                if (TryAdjustedPathBetween(key, to, paths.cantEnter, paths.avoidBy, out var adjusted))
                {
                    return new Path2[] { new ConcretePath2(adjusted, paths.mudMap, paths.mode) };
                }
                return new Path2[] { };
            })).ToArray();

        bestPath = FindBestPath(paths, startCanReachs, endCanReachs);

        if (bestPath != null)
        {
            res = bestPath;
            return true;
        }

        res = null;
        return false;
    }

    public static Path2? FindBestPath(Paths paths, Path2[] startCanReachs, Path2[] endCanReachs)
    {
        Path2? bestPath = null;

        foreach (var startCanReach in startCanReachs)
        {
            foreach (var endCanReach in endCanReachs)
            {
                if (startCanReach.Last().Equals(endCanReach.First()))
                {
                    var option = new CompositPath(startCanReach, endCanReach);
                    if (bestPath == null || bestPath.TravelTime() > option.TravelTime())
                    {
                        bestPath = option;
                    }
                }

                if (paths.reachables.TryGetValue(startCanReach.Last(), out var reaches) && reaches.distanceTo.TryGetValue(endCanReach.First(), out var path))
                {
                    var option = new CompositPath(new CompositPath(startCanReach, path), endCanReach);
                    if (bestPath == null || bestPath.TravelTime() > option.TravelTime())
                    {
                        bestPath = option;
                    }
                }
            }
        }

        return bestPath;
    }

    //private static Vector128<double>[] GetClosestPoints(rock avoidable, Vector128<double> closePoint)
    //{
    //	var points = pointsFromSource[avoidable];
    //	var myAngle = (closePoint - avoidable.GlobalPosition).Angle();

    //	var larger = points.Select(x =>
    //	{
    //		var angle = (x - avoidable.GlobalPosition).Angle();

    //		if (angle < myAngle)
    //		{
    //			angle += 2 * Math.PI;
    //		}

    //		return (angle - myAngle, x);

    //	}).OrderBy(x => x.Item1).First().x;

    //	var smaller = points.Select(x =>
    //	{
    //		var angle = (x - avoidable.GlobalPosition).Angle();

    //		if (angle > myAngle)
    //		{
    //			angle -= 2 * Math.PI;
    //		}

    //		return (myAngle - angle, x);

    //	}).OrderBy(x => x.Item1).First().x;

    //       return (smaller == larger ? new[] { smaller } : new[] { smaller, larger });
    //}

    public static byte[] Serialize((double size, Mode mode, Vector128<double>[][] vectorss)[] pathss)
    {
        var res = new List<byte>();
        res.AddRange(BitConverter.GetBytes(pathss.Length));

        foreach (var (size, mode, vectorss) in pathss)
        {
            res.AddRange(BitConverter.GetBytes(size));
            res.AddRange(BitConverter.GetBytes((int)mode));

            res.AddRange(BitConverter.GetBytes(vectorss.Length));

            foreach (var vectors in vectorss)
            {
                //file.Store32((uint)vectors.Length);
                res.AddRange(BitConverter.GetBytes(vectors.Length));
                foreach (var vector in vectors)
                {
                    //file.Storedouble(vector.x);
                    //file.Storedouble(vector.y);
                    res.AddRange(BitConverter.GetBytes(vector.X()));
                    res.AddRange(BitConverter.GetBytes(vector.Y()));
                }
            }
        }
        return res.ToArray();
    }

    public static (double size, Mode mode, Vector128<double>[][] vectorss)[] Deserialize(byte[] bytes)
    {

        var at = 0;
        var length = BitConverter.ToInt32(bytes, at);
        at += 4;
        var res = new (double size, Mode mode, Vector128<double>[][] vectorss)[length];

        for (int pathsIndes = 0; pathsIndes < length; pathsIndes++)
        {
            var size = BitConverter.ToDouble(bytes, at);
            at += 8;
            var mode = (Mode)BitConverter.ToInt32(bytes, at);
            at += 4;

            var vectorssLen = BitConverter.ToInt32(bytes, at);//file.Get32(); //
            at += 4;
            var vectorss = new Vector128<double>[vectorssLen][];

            for (int vectorssIndex = 0; vectorssIndex < vectorssLen; vectorssIndex++)
            {
                var innerArrayLen = BitConverter.ToInt32(bytes, at); //file.Get32();//
                at += 4;
                var innerArray = new Vector128<double>[innerArrayLen];
                for (int innerArrayIndex = 0; innerArrayIndex < innerArrayLen; innerArrayIndex++)
                {
                    var x = BitConverter.ToDouble(bytes, at); //file.Getdouble();// 
                    at += 8;                        //
                    var y = BitConverter.ToDouble(bytes, at); //file.Getdouble();// 
                    at += 8;
                    var vector = Vector128.Create(x, y);
                    // TOOD: if doing other formations, are these (x,y) in absolute terms or relative to the perpendicular?
                    //var vector = Vector128.Create(x, y - 2*Math.Abs(vectorssIndex - vectorssLen/2));
                    innerArray[innerArrayIndex] = vector;
                }
                vectorss[vectorssIndex] = innerArray;
            }

            res[pathsIndes] = (size, mode, vectorss);
        }

        return res.ToArray();
    }

    public enum Mode
    {
        ImpassableMud,
        SlowMud,
        IgnoreMud,
    }

    public static async Task<Paths[]> BuildKnownPaths(IReadOnlyList<(Vector128<double> globalPosition, double radius)> avoidables,
        IReadOnlyList<(Vector128<double> globalPosition, double radius)> mud,
        Map2<MudState> mudMap)
    {

        var filePath = "pathing data/08ABAE2F-B95E-438F-B702-906A5EA4C520.data";

        if (/*false &&*/ File.Exists(filePath))
        {
            return await Task.Run(async () =>
            {
                var fileData = await File.ReadAllBytesAsync(filePath).ConfigureAwait(false);
                var data = Deserialize(fileData);
                var res = new List<Paths>();

                foreach (var pathsData in data)
                {
                    var reachables = new Dictionary<Vector128<double>, Reachable>();
                    foreach (var pathPoints in pathsData.vectorss)
                    {
                        Path2 path = new ConcretePath2(pathPoints, mudMap, pathsData.mode);

                        if (!reachables.ContainsKey(pathPoints.First()))
                        {
                            reachables[pathPoints.First()] = new Reachable(new Dictionary<Vector128<double>, Path2>());
                        }
                        reachables[pathPoints.First()].distanceTo[pathPoints.Last()] = path;
                    }

                    // {7FB07668-D652-43E2-8282-CC36888F82FC}
                    var cantPassThrough = pathsData.mode == Mode.ImpassableMud ? avoidables.Union(mud).ToArray() : avoidables;

                    res.Add(new Paths(reachables, pathsData.mode, cantPassThrough, mudMap, pathsData.size));
                }

                Log.WriteLine($"loaded path from file");
                return res.ToArray();
            }).ConfigureAwait(false);
        }
        else
        {
            var list = new List<Task<Paths>>();
            foreach (var size in new[] { Pathing.unitSize_Small, Pathing.unitSize_Medium, Pathing.unitSize_Large })
            {
                foreach (var mode in new[] { Mode.ImpassableMud, Mode.SlowMud, Mode.IgnoreMud })
                {
                    list.Add(Task.Run(() =>
                    {
                        return BuildKnownPaths(avoidables, mud, mudMap, mode, size);
                    }));
                }
            }

            var pathss = await Task.WhenAll<Paths>(list).ConfigureAwait(false);

            await File.WriteAllBytesAsync(filePath, Serialize(
                pathss.Select(x =>(
                    x.avoidBy, 
                    x.mode, 
                    x.reachables.Values.SelectMany(x => x.distanceTo.Values.Select(y => y.ToPoints().ToArray())).ToArray()))
                .ToArray()))
                .ConfigureAwait(false);

            return pathss;
        }
    }

    public static Paths BuildKnownPaths(
        IReadOnlyList<(Vector128<double> globalPosition, double radius)> avoidables,
        IReadOnlyList<(Vector128<double> globalPosition, double radius)> mud,
        Map2<MudState> mudMap,
        Mode mode,
        double avoidBy)
    {
        var (pointsDict, pointsArray) = DirectPaths(avoidables, mud, mudMap, mode, avoidBy);

        OptimizeReachables(pointsDict, pointsArray);


        // TODO this is dup of here
        // {7FB07668-D652-43E2-8282-CC36888F82FC}
        var cantPassThrough = mode == Mode.ImpassableMud ? avoidables.Union(mud).ToArray() : avoidables;


        return new Paths(pointsDict, mode, cantPassThrough, mudMap, avoidBy);
    }

    //private static void Aaa(Dictionary<Vector128<double>, Reachable> pointsDict, TerrainMap terrainMap, Mode mudMode, double avoidBy, IReadOnlyList<(Vector128<double> globalPosition, double radius)> cantEnter) {
    //    foreach (var reachable in pointsDict.Values)
    //    {
    //        foreach (var path in reachable.distanceTo.Values)
    //        {
    //            var before = path.ToPoints();
    //            var after = OptimizePath(before, terrainMap, mudMode, avoidBy, cantEnter);
    //        }
    //    }
    //}

    public static void OptimizeReachables(Dictionary<Vector128<double>, Reachable> pointsDict, Vector128<double>[] pointsArray)
    {
        var watch = Stopwatch.StartNew();

        var noDirectPath = pointsArray.SelectMany(x => pointsArray.Select(y => (from: x, to: y)))
            .Where(x => x.from.X() > x.to.X() || (x.from.X() == x.to.X() && x.from.Y() > x.to.Y())) // we are only interested in one way 
            .Where(x => !pointsDict[x.from].distanceTo.ContainsKey(x.to))
            .ToArray();

        Log.WriteLine($"direction paths {watch.Elapsed}");

        var pressure = 0;
        var changes = true;
        while (changes)
        {
            int change = 0;
            int total = 0;

            changes = false;
            foreach (var (from, to) in noDirectPath)
            {
                total++;
                var reachableFrom = pointsDict[from];
                var reachableTo = pointsDict[to];

                var fromDistances = reachableFrom.distanceTo.ToArray();
                var toDistances = reachableTo.distanceTo;

                // probably makes this a little faster... 
                // yeah, tested about 25% faster
                if (pointsDict[from].distanceTo.TryGetValue(to, out var current))
                {
                    fromDistances = fromDistances.Where(x => x.Value.TravelTime() < current.TravelTime()).ToArray();
                    toDistances = toDistances.Where(x => x.Value.TravelTime() < current.TravelTime()).ToDictionary(x => x.Key, x => x.Value);
                }

                var options = fromDistances.SelectMany(
                    x => toDistances.TryGetValue(x.Key, out var distanceTo) ? new[] { x.Value.Combine(distanceTo.Reverse()) } : Array.Empty<CompositPath>()).ToArray();

                if (options.Any())
                {
                    var shortestPath = options.Min()!;
                    if (current != null)
                    {
                        if (current.TravelTime() > shortestPath.TravelTime())
                        {
                            change++;
                            changes = true;
                            ((CompositPath)current).Update(shortestPath);
                        }
                    }
                    else
                    {
                        change++;
                        changes = true;
                        pointsDict[from].distanceTo[to] = shortestPath;
                        pointsDict[to].distanceTo[from] = new ReversePath(shortestPath);
                    }
                }
            }
            pressure++;
            Log.WriteLine($"pressure: {pressure} {watch.Elapsed}, {change} of {total}");
        }

        Log.WriteLine($"done {watch.Elapsed}");
    }

    public static (Dictionary<Vector128<double>, Reachable> pointsDict, Vector128<double>[] pointsArray) DirectPaths(
        IReadOnlyList<(Vector128<double> globalPosition, double radius)> avoidables,
        IReadOnlyList<(Vector128<double> globalPosition, double radius)> mud,
        Map2<MudState> mudMap,
        Mode mode,
        double avoidBy)
    {
        // {7FB07668-D652-43E2-8282-CC36888F82FC}
        var cantPassThrough = mode == Mode.ImpassableMud ? avoidables.Union(mud).ToArray() : avoidables;
        var generatePointsFrom = mode == Mode.ImpassableMud || mode == Mode.SlowMud ? avoidables.Union(mud).ToArray() : avoidables;

        var pointsArray = generatePointsFrom.SelectMany(x => GeneratePoints(x, cantPassThrough, avoidBy)).ToArray();
        return DirectPaths(mudMap, mode, avoidBy, cantPassThrough, pointsArray);
    }

    private static (Dictionary<Vector128<double>, Reachable> pointsDict, Vector128<double>[] pointsArray) DirectPaths(Map2<MudState> mudMap, Mode mode, double avoidBy, IReadOnlyList<(Vector128<double> globalPosition, double radius)> cantPassThrough, Vector128<double>[] pointsArray)
    {
        var pointsDict = new Dictionary<Vector128<double>, Reachable>();

        foreach (var from in pointsArray)
        {
            if (!pointsDict.TryGetValue(from, out var reachable))
            {
                reachable = new Reachable(new Dictionary<Vector128<double>, Path2>());
                pointsDict[from] = reachable;
            }

            foreach (var to in pointsArray)
            {
                if (from.Equals(to))
                {
                    continue;
                }

                if (!reachable.distanceTo.ContainsKey(to) && IsAPathBetween(from, to, cantPassThrough, avoidBy))
                {
                    reachable.distanceTo[to] = new ConcretePath2(new[] { from, to }, mudMap, mode);
                }
            }
        }

        foreach (var from in pointsArray)
        {
            var reachable = pointsDict[from];

            foreach (var to in pointsArray)
            {
                if (from.Equals(to))
                {
                    continue;
                }

                if (!reachable.distanceTo.ContainsKey(to) && TryAdjustedPathBetween(from, to, cantPassThrough, avoidBy, out var path))
                {
                    reachable.distanceTo[to] = new ConcretePath2(path, mudMap, mode);
                }
            }
        }

        return (pointsDict, pointsArray);
    }

    private static Vector128<double>[] MakeArcPath(Vector128<double> from, Vector128<double> to, Vector128<double> center)
    {
        var radius = Avx.Subtract(from, center).Length();

        //      var len = (from - to).Length();

        //var angleNearCenter = Math.Asin((len / 2.0) / radius);

        var fromAngle = Avx.Subtract(from, center).Angle();
        var toAngle = Avx.Subtract(to, center).Angle();

        var possibleAngles = new[] {
            (Math.Abs(toAngle - fromAngle), toAngle),
            (Math.Abs((toAngle + (2 * Math.PI)) - fromAngle), toAngle + (2 * Math.PI)),
            (Math.Abs((toAngle - (2 * Math.PI)) - fromAngle), toAngle - (2 * Math.PI)),
        };

        toAngle = possibleAngles.OrderBy(x => x.Item1).First().Item2;

        //var direction = (from - center).Cross((to - center)) > 0 ? 1 : -1;

        //      if (toAngle < fromAngle && direction == 1)
        //{
        //	toAngle += Math.PI * 2;
        //}
        //if (toAngle < fromAngle && direction == -1)
        //{
        //	fromAngle += Math.PI * 2;
        //}

        var arcLen = Math.Abs(toAngle - fromAngle) * radius;
        var steps = Math.Ceiling(arcLen / stepSize);

        if (steps > 50)
        {
            Log.WriteLine($"made a long arc path, steps{steps}");
        }

        var ponts = new List<Vector128<double>> { };

        // we want from to be exact
        ponts.Add(from);

        for (double i = 1; i < steps; i++)
        {
            ponts.Add(Avx.Add(Avx.Subtract(from, center).Rotated((toAngle - fromAngle) * (i / steps)), center));
        }

        // we want to be exact 
        ponts.Add(to);

        return ponts.ToArray();
    }

    //private static IEnumerable<Vector128<double>> MakeArcPathPoints(Vector128<double> from, Vector128<double> to, Vector128<double> globalPosition) {
    //	if ((from - to).Length() < 10) {
    //		yield return from;
    //		yield return to;
    //       }

    //}


    public static Vector128<double> PointReached(Vector128<double> start, Vector128<double> end, IReadOnlyList<(Vector128<double> globalPosition, double radius)> avoidables, double avoidBy)
    {
        var path = Avx.Subtract(end, start);
        var perpendicular = path.NormalizedCopy().Rotated((double)(Math.PI / 2.0));
        var normal = path.NormalizedCopy();
        double? shortestDistanceTravelled = null;
        foreach (var avoidable in avoidables)
        {
            var positionReletiveToStart = Avx.Subtract(avoidable.globalPosition, start);
            var distanceFromPath = positionReletiveToStart.ProjectCopy(perpendicular).Length();
            var distanceAlongPath = positionReletiveToStart.ProjectCopy(normal);

            var distanceTravelled = distanceAlongPath.Length() - (double)Math.Sqrt((avoidable.radius + avoidBy) * (avoidable.radius + avoidBy) - (distanceFromPath * distanceFromPath));

            if (distanceFromPath < (avoidable.radius + avoidBy) && distanceAlongPath.Dot(normal) > 0 && distanceTravelled < path.Length())
            {
                if (shortestDistanceTravelled == null || distanceTravelled < shortestDistanceTravelled)
                {
                    shortestDistanceTravelled = distanceTravelled;
                }
            }
        }
        if (shortestDistanceTravelled != null)
        {
            return Avx.Add(start, Avx.Multiply(normal, ((double)shortestDistanceTravelled).AsVector()));
        }
        return end;
    }

    //public static (Guid id, Vector128<double> pos)[] GroupAdjust(
    //    IReadOnlyList<(Vector128<double> globalPosition, double radius)> avoidables, 
    //    IReadOnlyList<(Guid id, Vector128<double> globalPosition, double radius)> requestedPositions) 
    //{ 
    //    // loop over the requested positions
    //    // if any are in violoation adjust
    //    // but how?
    //}


    /// <summary>
    /// does not intersect if the lines are parallel. This is really about lines crossing and parallel lines don't cross
    /// the end points are included but the start points are exculed. This is used 
    /// </summary>
    public static (Vector128<double> point, double at1, double at2)? Intersect(Vector128<double> start1, Vector128<double> end1, Vector128<double> start2, Vector128<double> end2)
    {
        var v1 = Avx.Subtract(end1, start1);
        var v2 = Avx.Subtract(end2, start2);
        double vx1 = v1.X();
        double vx2 = v2.X();
        double vy1 = v1.Y();
        double vy2 = v2.Y();

        double x1 = start1.X();
        double x2 = start2.X();
        double y1 = start1.Y();
        double y2 = start2.Y();


        // vectors with 0 length can't cross
        if (vx1 == 0 && vy1 == 0)
        {
            return null;
        }
        if (vx2 == 0 && vy2 == 0)
        {
            return null;
        }

        // if the vectors are parallel they don't cross
        // even if they are ontop of each other 
        if (Vector128.Create(vx1, vy1).NormalizedCopy().Equals(Vector128.Create(vx2, vy2).NormalizedCopy()))
        {
            return null;
        }

        if (vx1 != 0)
        {
            var t2 = (y2 - y1 - ((x2 - x1) * vy1 / vx1)) / (-vy2 + (vx2 * vy1 / vx1));
            var t1 = (x2 - x1 + (vx2 * t2)) / vx1;
            if (t2 >= 0 && t2 < 1 && t1 >= 0 && t1 < 1)
            {
                return (Avx.Add(start2, Avx.Multiply(v2, t2.AsVector())), t1, t2);
                //return (new Vector128<double>((double)(start2.X() + (vx2 * t2)), (double)(start2.Y() + (vy2 * t2))), t1, t2);
            }
            else
            {
                return null;
            }
        }

        if (vx2 != 0)
        {
            var t1 = (y1 - y2 - ((x1 - x2) * vy2 / vx2)) / (-vy1 + (vx1 * vy2 / vx2));
            var t2 = (x1 - x2 + (vx1 * t1)) / vx2;
            if (t1 >= 0 && t1 < 1 && t2 >= 0 && t2 < 1)
            {
                return (Avx.Add(start1, Avx.Multiply(v1, t1.AsVector())), t1, t2);
                //return (new Vector128<double>((double)(start1.X + (vx1 * t1)), (double)(start1.Y + (vy1 * t1))), t1, t2);
            }
            else
            {
                return null;
            }
        }

        throw new Exception("if both vectors had a vx of 0, they would be parallel");
    }


    public static Vector128<double> ClosestPointOnSegmentTo(Vector128<double> start, Vector128<double> end, Vector128<double> point)
    {
        var segment = Avx.Subtract(end, start);
        var normalizedSegment = segment.NormalizedCopy();

        var toPoint = Avx.Subtract(point, start);
        var distanceAlongPath = normalizedSegment.Dot(toPoint);

        if (distanceAlongPath < 0) 
        {
            return start;
        }

        if (distanceAlongPath > segment.Length())
        {
            return end;
        }

        return Avx.Add(start, Avx.Multiply(normalizedSegment, distanceAlongPath.AsVector())); 
    }

    //public static bool DoLinesCross(Vector128<double> start1, Vector128<double> end1, Vector128<double> start2, Vector128<double> end2) {
    //	var startingDx = start1.x - start2.x;
    //	var startingDy = start1.y - start2.y;
    //	var deltaDx = (end1.x - start1.x) - (end2.x - start2.x);
    //	var deltaDy = (end1.y - start1.y) - (end2.y - start2.y);
    //	// startingDx + deltaDx*t = 0 
    //	// startingDy + deltaDy*t = 0
    //	// for some t between 0 and 1
    //	// startingDx + deltaDx*t = startingDy + deltaDy*t
    //	// deltaDx*t = startingDy + deltaDy*t - startingDx
    //	// deltaDx*t - deltaDy*t = startingDy - startingDx
    //	// (deltaDx - deltaDy) *t = startingDy - startingDx
    //	// t = startingDy - startingDx / (deltaDx - deltaDy)

    //	// I don't think that's right
    //	// because that would be true anytime dx and dy are equal

    //	if (deltaDx - deltaDy == 0) {
    //		return false;
    //	}

    //	var t = (startingDy - startingDx) / (deltaDx - deltaDy);

    //	return t >= 0 && t <= 1;
    //   }


    // adjusted paths are a solution to unit moving around round objects
    // they generate usally become a curve around the avoidable
    public static bool TryAdjustedPathBetween(Vector128<double> start, Vector128<double> end, IReadOnlyList<(Vector128<double> globalPosition, double radius)> avoidables, double avoidBy, [MaybeNullWhen(false)] out Vector128<double>[] path)
    {

        var proposed = new List<Vector128<double>>();

        var len = Avx.Subtract(end, start).Length();
        var steps = Math.Ceiling(len / stepSize);

        // these should really be pretty short
        // if they are long
        if (steps > 25)
        {
            path = default;
            return false;
        }

        for (int i = 0; i <= steps; i++)
        {
            if (!TryAdjustTarget(Avx.Add(Avx.Multiply((i / steps).AsVector(), end), Avx.Multiply(((steps - i) / steps).AsVector(), start)), avoidables, avoidBy, out var target))
            {
                path = default;
                return false;
            }
            proposed.Add(target);

            if (i != 0)
            {
                if (!IsAPathBetween(proposed[i - 1], proposed[i], avoidables, avoidBy))
                {
                    path = default;
                    return false;
                }
            }
        }

        path = proposed.ToArray();
        return true;
    }

    // old only? I hope
    //  public static bool IsAnArcBetween(Vector128<double> start, Vector128<double> end, rock center, IReadOnlyList<rock> avoidables, double avoidBy, out Vector128<double>[] path) {

    //var r = (start - center.GlobalPosition).Length();

    //path = MakeArcPath(start, end, center.GlobalPosition);

    //      foreach (var avoidable in avoidables.Where(x=>x != center)) 
    //{
    //	if ((avoidable.GlobalPosition - center.GlobalPosition).Length() > r + avoidBy + avoidable.Radius + padding) { 
    //		continue;
    //	}

    //	if ((start - avoidable.GlobalPosition).Length() < avoidBy + avoidable.Radius + padding) {
    //		return false;
    //	}

    //          if ((end - avoidable.GlobalPosition).Length() < avoidBy + avoidable.Radius + padding)
    //          {
    //              return false;
    //          }

    //	for (int i = 0; i < path.Length - 1; i++)
    //	{
    //		if (Intersect(path[i], path[i + 1], center.GlobalPosition, avoidable.GlobalPosition) != null) {
    //			return false;
    //		}
    //	}
    //      }
    //return true;
    //  }


    public static bool IsAPathBetween(Vector128<double> start, Vector128<double> end, IReadOnlyList<(Vector128<double> globalPosition, double radius)> avoidables, double avoidBy)
    {
        if (start.Equals(end)) 
        {
            return true;
        }

        var path = Avx.Subtract(end, start);
        var perpendicular = path.NormalizedCopy().Rotated((double)(Math.PI / 2.0));
        var normal = path.NormalizedCopy();
        foreach (var avoidable in avoidables)
        {
            if (Avx.Subtract(start, avoidable.globalPosition).Length() < avoidable.radius + avoidBy)
            {
                return false;
            }
            if (Avx.Subtract(end, avoidable.globalPosition).Length() < avoidable.radius + avoidBy)
            {
                return false;
            }

            var positionReletiveToStart = Avx.Subtract(avoidable.globalPosition, start);
            var distanceFromPath = positionReletiveToStart.ProjectCopy(perpendicular).Length();
            var distanceAlongPath = positionReletiveToStart.ProjectCopy(normal);

            if (Math.Abs(distanceFromPath) < avoidable.radius + avoidBy && distanceAlongPath.Dot(normal) > 0 && distanceAlongPath.Length() < path.Length())
            {
                return false;
            }
        }
        return true;
    }

    public static bool CanReach(Vector128<double> start, Vector128<double> end, IEnumerable<(Vector128<double> globalPosition, double radius)> avoidables, double avoidBy, out Vector128<double> reached, out Vector128<double>? lastHit)
    {
        var path = Avx.Subtract(end, start);
        var perpendicular = path.NormalizedCopy().Rotated((double)(Math.PI / 2.0));
        var normal = path.NormalizedCopy();

        var distanceTraveledStarted = Avx.Subtract(end, start).Length();
        var distanceTraveled = distanceTraveledStarted;

        lastHit = null;

        foreach (var avoidable in avoidables)
        {
            if (Avx.Subtract(start, avoidable.globalPosition).Length() < avoidable.radius + avoidBy)
            {
                lastHit = avoidable.globalPosition;
                reached = start; 
                return false;
            }

            var positionReletiveToStart = Avx.Subtract(avoidable.globalPosition, start);
            var distanceFromPath = positionReletiveToStart.ProjectCopy(perpendicular).Length();
            var distanceAlongPath = positionReletiveToStart.ProjectCopy(normal);

            var hypotanose = avoidable.radius + avoidBy;

            if (Math.Abs(distanceFromPath) < hypotanose && distanceAlongPath.Dot(normal) > 0)
            {
                var tookOff = Math.Abs(hypotanose * Math.Cos(Math.Asin(distanceFromPath / hypotanose)));

                var proposedGotTo = distanceAlongPath.Length() - tookOff;

                if (proposedGotTo < distanceTraveled)
                {
                    //Log.WriteLine($"got here {gotTo} --> {proposedGotTo}, tookOff {tookOff}, distanceAlongPath {distanceAlongPath}");
                    lastHit = avoidable.globalPosition;
                    distanceTraveled = proposedGotTo;
                }
            }
        }
        reached = Avx.Add(start, path.ToLength(distanceTraveled));
        return distanceTraveled == distanceTraveledStarted;
    }

    public static double padding = MapExtensions.MAP_GRID_SIZE * Math.Sqrt(2);
    private static double lengthPerPoint = 200.0;
    public static Vector128<double>[] GeneratePoints((Vector128<double> globalPosition, double radius) fromAvoidable, IReadOnlyList<(Vector128<double> globalPosition, double radius)> avoidables, double avoidBy)
    {
        var list = new List<Vector128<double>>();
        double numberOfPoints = GetNumberOfPoints(fromAvoidable, avoidBy);

        //double hypotanose = GetHypotanose(fromAvoidable, avoidBy, numberOfPoints);

        for (int i = 0; i < numberOfPoints; i++)
        {
            var angle = Math.PI * 2 * (i / (double)numberOfPoints);
            //list.Add(fromAvoidable.GlobalPosition + new Vector128<double>(0, hypotanose).Rotated(angle));
            list.Add(Avx.Add(fromAvoidable.globalPosition, Vector128.Create(0.0, fromAvoidable.radius + avoidBy + padding).Rotated(angle)));
            //list.Add(fromAvoidable.GlobalPosition + new Vector128<double>(Math.Sin(angle) * avoidBy, Math.Cos(angle) * avoidBy));
        }

        return list.Where(point => avoidables.Except(new[] { fromAvoidable }).All(y => Avx.Subtract(y.globalPosition, point).Length() > avoidBy + y.radius)).ToArray();
    }

    private static double GetNumberOfPoints((Vector128<double> globalPosition, double radius) fromAvoidable, double avoidBy)
    {
        return Math.Max(4, Math.Ceiling((fromAvoidable.radius + avoidBy) * Math.PI * 2 / lengthPerPoint));
    }

    private static double GetHypotanose((Vector128<double> globalPosition, double radius) fromAvoidable, double avoidBy, double numberOfPoints)
    {
        // we are wrapping our circle a polygone 
        // we need the point closest to the polygon to be fromAvoidable.radius + avoidBy + 1 away
        // the line from the center of our circle to that point is the adjecten side of right triangle
        var adjacent = fromAvoidable.radius + avoidBy + 1;
        // the hypotanose leads to the point of the polygone
        // the angle between the adjacent side and the hypotanose is 2pi / (numberOfPoints * 2)
        // cos(2pi / numberOfPoints) = adjacent/hypotanose
        var hypotanose = adjacent / Math.Cos(Math.PI * 2.0 / (numberOfPoints * 2.0));
        return hypotanose;
    }

    public static bool IsInOrbitOf(Vector128<double> vector, (Vector128<double> globalPosition, double radius) avoidable, IReadOnlyList<(Vector128<double> globalPosition, double radius)> avoidables, double avoidBy, [MaybeNullWhen(false)] out Vector128<double>[] close)
    {

        double numberOfPoints = GetNumberOfPoints(avoidable, avoidBy);

        double hypotanose = GetHypotanose(avoidable, avoidBy, numberOfPoints);

        var distance = Avx.Subtract(avoidable.globalPosition, vector).Length();

        if (distance >= avoidable.radius + avoidBy && distance <= hypotanose)
        {
            var angleNewAvoidable = Math.Acos((avoidable.radius + avoidBy) / distance);
            var points = new[] {
                Avx.Multiply(Avx.Subtract(avoidable.globalPosition, vector).NormalizedCopy().Rotated(angleNewAvoidable), (avoidable.radius + avoidBy + padding).AsVector()),
                Avx.Multiply(Avx.Subtract(avoidable.globalPosition, vector).NormalizedCopy().Rotated(-angleNewAvoidable), (avoidable.radius + avoidBy + padding).AsVector()) }
            .Distinct()
            .ToArray();

            points = points
                .Where(point => avoidables.Except(new[] { avoidable }).All(y => Avx.Subtract(y.globalPosition, point).Length() > avoidBy + y.radius))
                .Where(x => IsAPathBetween(x, vector, avoidables, avoidBy))
                .ToArray();
            if (points.Length > 0)
            {
                close = points;
                return true;
            }
        }

        close = null;
        return false;
    }

    //public static bool TryAdjustTarget(Vector128<double> target, IReadOnlyList<(Vector128<double> globalPosition, double radius)> avoidables, double avoidBy, out Vector128<double> res) {
    //	return TryAdjustTarget(target, avoidables, avoidBy, out res);
    //   }

    public static bool TryAdjustTarget(Vector128<double> target, IReadOnlyList<(Vector128<double> globalPosition, double radius)> avoidables, double avoidBy, [MaybeNullWhen(false)] out Vector128<double> res)
        => TryAdjustTarget(target, avoidables, avoidBy, double.MaxValue, out res);

    public static bool TryAdjustTarget(Vector128<double> target, IReadOnlyList<(Vector128<double> globalPosition, double radius)> avoidables, double avoidBy, double maxAdjustment, [MaybeNullWhen(false)] out Vector128<double> res)
    {
        var startingTarget = target;
        var tries = 0;

        while (tries < 100)
        {
            tries++;
            bool moved = false;
            foreach (var avoidable in avoidables)
            {
                var diff = Avx.Subtract(target, avoidable.globalPosition);
                if (diff.Length() < avoidable.radius + avoidBy + padding - 1) // -1 becase we push it out to 'avoidable.radius + avoidBy + padding' but computer math is a bit messed up an the actualy length can be off by a bit
                {
                    target = Avx.Add(avoidable.globalPosition, Avx.Multiply(diff.SafeNormalizedCopy(), (avoidable.radius + avoidBy + padding ).AsVector()));
                    moved = true;
                    //Log.WriteLine($"ADJUST BUG adjuested to: {target}, length: {Avx.Subtract(target, avoidable.globalPosition).Length()}");
                    if (Avx.Subtract(target, startingTarget).Length() > maxAdjustment)
                    {
                        res = default;
                        return false;
                    }
                }
            }

            if (!moved)
            {
                res = target;
                return true;
            }
        }
        Log.WriteLine("100 tries no luck");
        res = default;
        return false;
    }

    public static bool CanPlace(Vector128<double> target, IReadOnlyList<(Vector128<double> globalPosition, double radius)> avoidables, double avoidBy)
    {
        foreach (var avoidable in avoidables)
        {
            var diff = Avx.Subtract(target, avoidable.globalPosition);
            if (diff.Length() < avoidable.radius + avoidBy + Pathing.padding)
            {
                return false;
            }
        }
        return true;
    }

    internal static bool InLine(Vector128<double> start, Vector128<double> possiblyInTheMiddle, Vector128<double> end)
    {
        if (start.Equals(end))
        {
            if (possiblyInTheMiddle.Equals(start))
            {
                return true;
            }
            return false;
        }

        var direction = Avx.Subtract(end, start).NormalizedCopy();

        if (direction.Dot(Avx.Subtract(possiblyInTheMiddle, start).NormalizedCopy()) > .99)
        {
            if (Avx.Subtract(possiblyInTheMiddle, start).Length() <= Avx.Subtract(end, start).Length())
            {
                return true;
            }
        }
        return false;
    }
}
