﻿using Ants.Domain.Infrastructure;
using System.Runtime.Intrinsics;
using System.Runtime.Intrinsics.X86;

public class GlobalPosionInt {

    // we back by a double for preformance
    // not that I've measured
    // means we have more Vector128<double> interacting with Vector128<double>
    private readonly Vector128<double> backing;


    public GlobalPosionInt(int x, int y): this (Vector128.Create( (double)x, (double)y ))
    {
    }

    private GlobalPosionInt(Vector128<double> backing)
    {
#if DEBUG

        if (backing.X() != Math.Round(backing.X())) {
            throw new Exception($"x should be a whole number! x: {backing.X()}");
        }

        if (backing.Y() != Math.Round(backing.Y()))
        {
            throw new Exception($"y should be a whole number! y: {backing.X()}");
        }
#endif

        this.backing = backing;
    }

    public static Vector128<double> scaleVector = Vector128.Create(scale, scale);
    public const double scale = 1000;

    #region operate with GlobalPosionInt
    public static GlobalPosionInt operator +(GlobalPosionInt self, GlobalPosionInt other)
    {
        return new GlobalPosionInt(Avx.Add(self.backing, other.backing));
    }

    public static GlobalPosionInt operator -(GlobalPosionInt self, GlobalPosionInt other)
    {
        return new GlobalPosionInt(Avx.Subtract(self.backing, other.backing));
    }

    public static GlobalPosionInt operator -(GlobalPosionInt self)
    {
        return new GlobalPosionInt(Avx.Subtract(Vector128<double>.Zero ,self.backing));
    }

    public static GlobalPosionInt operator *(GlobalPosionInt self, GlobalPosionInt other)
    {
        return new GlobalPosionInt(Avx.Multiply(self.backing, other.backing));
    }

    #endregion

    #region operate with Vector128<double>

    public static Vector128<double> operator +(GlobalPosionInt self, Vector128<double> other)
    {
        return Avx.Add(self.backing, other);
    }

    public static Vector128<double> operator -(GlobalPosionInt self, Vector128<double> other)
    {
        return Avx.Subtract(self.backing, other);
    }

    public static Vector128<double> operator /(GlobalPosionInt self, Vector128<double> other)
    {
        return Avx.Divide(self.backing, other);
    }
    public static Vector128<double> operator *(GlobalPosionInt self, Vector128<double> other)
    {
        return Avx.Multiply(self.backing, other);
    }

    public static Vector128<double> operator *(Vector128<double> other, GlobalPosionInt self)
    {
        return Avx.Multiply(self.backing, other);
    }

    #endregion

    #region operate with double

    public static Vector128<double> operator /(GlobalPosionInt self, double other)
    {
        return Avx.Divide(self.backing, other.AsVector());
    }

    public static Vector128<double> operator *(GlobalPosionInt self, double other)
    {
        return Avx.Multiply(self.backing, other.AsVector());
        //return new GlobalPosionInt(new Vector128<double>(new[] { Math.Round(res.X()), Math.Round(res.X()) }));
    }

    public static Vector128<double> operator *(double other, GlobalPosionInt self)
    {
        return Avx.Multiply(self.backing, other.AsVector());
        //return new GlobalPosionInt(new Vector128<double>(new[] { Math.Round(res.X()), Math.Round(res.X()) }));
    }

    #endregion

    // operate with int?

    /// <summary>
    /// with ints it really doesn't make sense to normalize to one..
    /// </summary>
    public GlobalPosionInt NormalizedToLen1000()
    {
        var norm = Avx.Multiply(backing.NormalizedCopy(), (1000.0).AsVector());
        return new GlobalPosionInt(norm);
    }

    public double Length()
    {
        return backing.Length();
    }

    public double Dot(GlobalPosionInt other)
    {
        return backing.Dot(other.backing);
    }

    public static GlobalPosionInt FromGlobalVector(Vector128<double> place)
    {
        var res = Avx.Multiply(place, scaleVector);
        return new GlobalPosionInt(Vector128.Create(Math.Round(res.X()), Math.Round(res.Y())));
    }

    public static GlobalPosionInt FromMapPosition((int x, int y) place)
    {
        var res = Avx.Multiply(
                Vector128.Create((double)place.x, (double)place.y), 
                Avx.Multiply(
                    scaleVector, 
                    MapExtensions.MAP_GRID_SIZE_VECTOR));
        return new GlobalPosionInt(Vector128.Create(Math.Round(res.X()), Math.Round(res.Y())));
    }

    // cache?
    internal Vector128<double> ToGlobalVector()
    {
        return Avx.Divide(backing, scaleVector);
    }

    public override bool Equals(object? obj)
    {
        return obj is GlobalPosionInt other && other.backing.Equals(backing);
    }

    public override int GetHashCode()
    {
        unchecked
        {
            return backing.X().GetHashCode() + backing.Y().GetHashCode();
        }
    }

    public GlobalPosionInt Divide(int number) {
        return new GlobalPosionInt(X() / number, Y() / number);
    }

    public override string ToString()
    {
        return $"({backing.X()}, {backing.Y()})";
    }

    public int X() => (int)backing.X();
    public int Y() => (int)backing.Y();
}



public static class GlobalPosionIntExtensions
{

    // (int x, int y) should probably be a type
    // or properties of GlobalPosionInt

    public static GlobalPosionInt ToGlobalPositionInt(this (int x, int y) self)
    {
        return GlobalPosionInt.FromMapPosition(self);
    }

    public static GlobalPosionInt ToGlobalPositionInt(this Vector128<double> self)
    {
        return GlobalPosionInt.FromGlobalVector(self);
    }
}
