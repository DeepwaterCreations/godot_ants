﻿using Ants.Domain.Infrastructure;
using Ants.Domain.State;
using Prototypist.Toolbox;
using Prototypist.Toolbox.Bool;
using Prototypist.Toolbox.Dictionary;
using Prototypist.Toolbox.IEnumerable;
using Prototypist.Toolbox.Object;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Net.NetworkInformation;
using System.Numerics;
using System.Runtime.Intrinsics;
using System.Runtime.Intrinsics.X86;
using System.Xml.Linq;
using static Pathing;

public class MapEntry<T> : MapEntry, IIsDefinately<T>
{
    public MapEntry(T value)
    {
        this.Value = value ?? throw new ArgumentNullException(nameof(value));
    }

    [NotNull]
    public T Value { get; }

    public override object Representative() => Value;
}

public abstract class MapEntry : OrType<
        Guid, // ant id
        BulletState.Immutable.Id, // bullet id 
        RockState // rocks are immunatable
        >
{
    public static MapEntry Make(Guid id)
    {
        return new MapEntry<Guid>(id);
    }

    public static MapEntry Make(BulletState.Immutable.Id id)
    {
        return new MapEntry<BulletState.Immutable.Id>(id);
    }

    public static MapEntry Make(RockState id)
    {
        return new MapEntry<RockState>(id);
    }

    public IOrType<AntState, BulletState, RockState> Convert(Dictionary<Guid, AntState> ants, Dictionary<BulletState.Immutable.Id, BulletState> bullets)
    {
        return SwitchReturns(
                id => OrType.Make<AntState, BulletState, RockState>(ants[id]),
                id => OrType.Make<AntState, BulletState, RockState>(bullets[id]),
                RockState => OrType.Make<AntState, BulletState, RockState>(RockState)
                );
    }
}


//public class Map
//{
//    private readonly ConcurrentDictionary<(int x, int y), MapEntry> antsRocks = new ConcurrentDictionary<(int x, int y), MapEntry>();
//    //private readonly ConcurrentDictionary<(int x, int y), Guid> ants = new ConcurrentDictionary<(int x, int y), Guid>();
//    //private readonly ConcurrentDictionary<(int x, int y), RockState> rocks = new ConcurrentDictionary<(int x, int y), RockState>();
//    private readonly ConcurrentDictionary<(int x, int y), ConcurrentDictionary<BulletState.Immutable.Id, BulletState.Immutable.Id>> bullets = new ConcurrentDictionary<(int x, int y), ConcurrentDictionary<BulletState.Immutable.Id, BulletState.Immutable.Id>>();

//    //private readonly ConcurrentDictionary<(int x, int y), ConcurrentDictionary<MapEntry, MapEntry>> backing = new ConcurrentDictionary<(int x, int y), ConcurrentDictionary<MapEntry, MapEntry>>();

//    public Map()
//    {
//    }

//    //public Map(
//    //    ConcurrentDictionary<(int x, int y), Guid> ants,
//    //    ConcurrentDictionary<(int x, int y), RockState> rocks,
//    //    ConcurrentDictionary<(int x, int y), ConcurrentDictionary<BulletState.Immutable.Id, BulletState.Immutable.Id>> bullets)
//    //{
//    //    this.ants = ants ?? throw new ArgumentNullException(nameof(ants));
//    //    this.rocks = rocks ?? throw new ArgumentNullException(nameof(rocks));
//    //    this.bullets = bullets ?? throw new ArgumentNullException(nameof(bullets));
//    //}

//    public static Map FromState(SimulationState state)
//    {

//        //TestSafeLog.WriteLine("NewMapFromState");
//        Map map = new Map();
//        foreach (var bullet in state.bullets.Values)
//        {
//            map.AddToMap(bullet, bullet.GlobalPositionInt);
//        }
//        foreach (var ant in state.ants.Values)
//        {
//            map.AddToMap(ant, ant.GlobalPositionInt);
//        }
//        // TODO, rocks don't change much
//        // but, I would like to be able to add and remove them at somepoint 
//        // rebuilding a map full of rocks is expensive 
//        // maybe rocks map is it's own thing?
//        // it splits of map
//        // and it tracked in run
//        // but ant placements and rock placement can't overlap
//        // so it would be hard to split off rocks
//        // hmmm
//        foreach (var rock in state.rocks)
//        {
//            map.AddToMap(rock, rock.GlobalPositionInt);
//        }

//        return map;
//    }

//    // probably I should have differnt try gets for different types
//    public bool TryRawGet((int x, int y) key, [MaybeNullWhen(false)] out IEnumerable<MapEntry> entry)
//    {
//        var res = new List<MapEntry>();
//        if (antsRocks.TryGetValue(key, out var ant)) {
//            res.Add(ant);
//        }


//        if (bullets.TryGetValue(key, out var bulletsHit))
//        {
//            foreach (var bullet in bulletsHit)
//            {
//                res.Add(new MapEntry<BulletState.Immutable.Id>(bullet.Key));
//            }
//        }
//        entry = res;
//        return entry.Any();
//    }

//    public void AddToMap(AntState thing, GlobalPosionInt at)
//    {
//        var mapPosition = at.ToMapPosition();
//        var entry = thing.ToMapEntry();
//        foreach (var GlobalPosition in thing.ReletivePositions().Select(x => x.Shift(mapPosition.x, mapPosition.y)))
//        {
//            if (!antsRocks.TryAdd(GlobalPosition, entry)) {
//                Log.WriteLine("tried to add ant, but couldn't");
//            }
//        }
//    }

//    public void AddToMap(RockState thing, GlobalPosionInt at)
//    {
//        var mapPosition = at.ToMapPosition();
//        var entry = thing.ToMapEntry();
//        foreach (var GlobalPosition in thing.ReletivePositions().Select(x => x.Shift(mapPosition.x, mapPosition.y)))
//        {
//            if (!antsRocks.TryAdd(GlobalPosition, entry)) {
//                // rocks do overlap sometimes
//                //Log.WriteLine("tried to add rock, but couldn't");
//            }
//        }
//    }


//    public void AddToMap(BulletState thing, GlobalPosionInt at)
//    {
//        var mapPosition = at.ToMapPosition();
//        foreach (var GlobalPosition in thing.ReletivePositions().Select(x => x.Shift(mapPosition.x, mapPosition.y)))
//        {
//            var list = bullets.GetOrAdd(GlobalPosition, new ConcurrentDictionary<BulletState.Immutable.Id, BulletState.Immutable.Id>());
//            if (!list.TryAdd(thing.immutable.id, thing.immutable.id)) {
//                Log.WriteLine("tried to add bullet, but couldn't");
//            }
//        }
//    }

//    public void AddEdge(AntState subject, (int x, int y)[] edgeRaletivePosition)
//    {
//        var entry = subject.ToMapEntry();

//        foreach (var GlobalPosition in edgeRaletivePosition.Select(x => x.ToMapPosition(subject)))
//        {
//            if (!antsRocks.TryAdd(GlobalPosition, entry)) {
//                Log.WriteLine("tried to add ant, but couldn't");
//            }
//        }
//    }

//    public void AddEdge(RockState subject, (int x, int y)[] edgeRaletivePosition)
//    {
//        var entry = subject.ToMapEntry();

//        foreach (var GlobalPosition in edgeRaletivePosition.Select(x => x.ToMapPosition(subject)))
//        {
//            if (!antsRocks.TryAdd(GlobalPosition, entry))
//            {
//                Log.WriteLine("tried to add rock, but couldn't");
//            }
//        }
//    }

//    public void AddEdge(BulletState subject, (int x, int y)[] edgeRaletivePosition)
//    {
//        foreach (var GlobalPosition in edgeRaletivePosition.Select(x => x.ToMapPosition(subject)))
//        {
//            var dict = bullets.GetOrAdd(GlobalPosition, new ConcurrentDictionary<BulletState.Immutable.Id, BulletState.Immutable.Id>());
//            if (!dict.TryAdd(subject.immutable.id, subject.immutable.id)) 
//            {
//                Log.WriteLine("tried to add bullet, but couldn't");
//            }
//        }
//    }

//    // isn't thread safe
//    // if two thing call it on the same space at the same time
//    // it'll bug
//    public bool TryAddToMap(AntState thing, GlobalPosionInt at)
//    {
//        var mapAt = MapExtensions.ToMapPosition(at);
//        foreach (var GlobalPosition in thing.ReletivePositions().Select(relativePos => (mapAt.x + relativePos.x, mapAt.y + relativePos.y)))
//        {
//            if (antsRocks.TryGetValue(GlobalPosition, out var _)) {
//                return false;
//            }
//        }
//        AddToMap(thing, at);
//        return true;
//    }

//    public void Remove(AntState subject)
//    {
//        var entry = subject.ToMapEntry();
//        foreach (var GlobalPosition in subject.ReletivePositions().Select(x => x.ToMapPosition(subject)))
//        {
//            if (antsRocks.TryRemove(GlobalPosition, out var removed))
//            {
//                if (removed.Representative() is Guid guid)
//                {
//                    if (guid != subject.immutable.id)
//                    {
//                        Log.WriteLine($"id of removed item doesn't match. Expected: {subject.immutable.id}, got: {removed.Representative()}");
//                    }
//                }
//                else {
//                    Log.WriteLine($"type of removed item doesn't match. Expected: {subject.immutable.id}, got: {removed.Representative()}");
//                }
//            }
//            else {
//                Log.WriteLine($"could not remove {subject}");
//            }
//        }
//    }

//    public void Remove(RockState subject)
//    {
//        var entry = subject.ToMapEntry();
//        foreach (var GlobalPosition in subject.ReletivePositions().Select(x => x.ToMapPosition(subject)))
//        {
//            if (antsRocks.TryRemove(GlobalPosition, out var removed))
//            {
//                if (removed.Representative() != subject)
//                {
//                    Log.WriteLine($"id of removed item doesn't match. Expected: {subject}, got: {removed.Representative()}");
//                }
//            }
//            else
//            {
//                Log.WriteLine($"could not remove {subject}");
//            }
//        }
//    }

//    public void Remove(BulletState subject)
//    {
//        foreach (var GlobalPosition in subject.ReletivePositions().Select(x => x.ToMapPosition(subject)))
//        {
//            var list = bullets[GlobalPosition];
//            if (list.TryRemove(subject.immutable.id, out var removed))
//            {
//                if (!subject.immutable.id.Equals(removed)) {
//                    Log.WriteLine($"removed the wrong thing, expected: {subject.immutable.id}, got: {removed}");
//                }
//            }
//            else {
//                Log.WriteLine("could not remove ");
//            }
//            if (list.Count == 0)
//            {
//                bullets.Remove(GlobalPosition, out var _);
//            }
//        }
//    }

//    public void RemoveEdge(AntState subject, (int x, int y)[] edgeRaletivePosition)
//    {

//        var entry = subject.ToMapEntry();
//        foreach (var GlobalPosition in edgeRaletivePosition.Select(x => x.ToMapPosition(subject)))
//        {
//            if (antsRocks.TryRemove(GlobalPosition, out var removed))
//            {
//                if (removed.Representative() is Guid guid)
//                {
//                    if (guid != subject.immutable.id)
//                    {
//                        Log.WriteLine($"id of removed item doesn't match. Expected: {subject.immutable.id}, got: {removed.Representative()}");
//                    }
//                }
//                else
//                {
//                    Log.WriteLine($"type of removed item doesn't match. Expected: {subject.immutable.id}, got: {removed.Representative()}");
//                }
//            }
//            else
//            {
//                Log.WriteLine($"could not remove {subject}");
//            }
//        }
//    }

//    public void RemoveEdge(RockState subject, (int x, int y)[] edgeRaletivePosition)
//    {

//        var entry = subject.ToMapEntry();
//        foreach (var GlobalPosition in edgeRaletivePosition.Select(x => x.ToMapPosition(subject)))
//        {
//            var list = antsRocks[GlobalPosition];
//            if (antsRocks.TryRemove(GlobalPosition, out var removed))
//            {
//                if (removed.Representative() != subject)
//                {
//                    Log.WriteLine($"id of removed item doesn't match. Expected: {subject}, got: {removed.Representative()}");
//                }
//            }
//            else
//            {
//                Log.WriteLine($"could not remove {subject}");
//            }
//        }
//    }

//    //public IEnumerable<(int x, int y, MapEntry value)> Enumerate()
//    //{
//    //    foreach (var (key, dict) in backing)
//    //    {
//    //        foreach (var (value, _) in dict)
//    //        {
//    //            yield return (key.x, key.y, value);
//    //        }
//    //    }
//    //}

//    //public Map Copy()
//    //{
//    //    return new Map(new ConcurrentDictionary<(int x, int y), ConcurrentDictionary<MapEntry, MapEntry>>(backing.ToDictionary(x => x.Key, y => new ConcurrentDictionary<MapEntry, MapEntry>(y.Value))));
//    //}
//}

// steps should probably include there x and y elements
// and if you are hit you should probably be allowed to move a partial step (but not leave your box)

public class StepTime
{
    public readonly double time;
    public readonly GlobalPosionInt step;
    public readonly string debugType;

    public StepTime(double time, double endTime/*I don't really need this but it's nice for debugging*/, GlobalPosionInt step, string debugType)
    {
        if (time < 0 || time > endTime)
        {
            Log.WriteLine($"StepTime constructor time is {time}, endTime is {endTime}");
            Debugger.Launch();
        }
        this.time = time;
        this.step = step;
        this.debugType = debugType;
    }


    public static List<StepTime> BuildSteps(GlobalPosionInt velocity, double now, double endTime, GlobalPosionInt globalPosionInt)
    {
        var timeRemaining = endTime - now;
        if (timeRemaining < 0)
        {
            Log.WriteLine($"time remaining {timeRemaining}");
        }

        var steps = new List<StepTime>();

        if (velocity.X() != 0)
        {
            steps.AddRange(StepsX(velocity.X(), now, endTime,globalPosionInt));
        }

        if (velocity.Y() != 0)
        {
            steps.AddRange(StepsY(velocity.Y(), now, endTime, globalPosionInt));
        }
        steps = steps.OrderBy(x => x.time).ToList();

        return steps; ;
    }

    public static List<StepTime> StepYOrRemainder(int velocityY, double now, double endTime, GlobalPosionInt globalPositionInt)
    {
        if (velocityY == 0)
        {
            return new List<StepTime>();
        }

        var at = globalPositionInt.Y();

        var i = velocityY > 0 ? 1 : -1;
        if (TryMoveToEdgeY(velocityY, i, now, endTime, at, globalPositionInt, "to edge y (StepYOrRemainder)", out var toEdge))
        {
            var res = new List<StepTime>();
            at += toEdge.step.Y();
            if (toEdge.step.Y() != 0)
            {
                res.Add(toEdge);
            }

            if (TryMoveOverEdgeY(velocityY, i, now, endTime, at, globalPositionInt, "over edge y (StepYOrRemainder)", out var overEdge))
            {
                at += overEdge.step.Y();
                if (overEdge.step.Y() != 0)
                {
                    res.Add(overEdge);
                }
            }
            return res;
        }
        else
        {
            var remainderY = (int)(velocityY * (endTime - now));
            return new List<StepTime> { new StepTime(endTime, endTime, new GlobalPosionInt(0, remainderY), "remainder y (StepYOrRemainder)") };
        }
    }

    public static List<StepTime> StepXOrRemainder(int velocityX, double now, double endTime, GlobalPosionInt globalPositionInt)
    {
        if (velocityX == 0)
        {
            return new List<StepTime>();
        }

        var at = globalPositionInt.X();

        var i = velocityX > 0 ? 1 : -1;
        if (TryMoveToEdgeX(velocityX, i, now, endTime, at, globalPositionInt, "to edge x (StepXOrRemainder)", out var toEdge))
        {
            var res = new List<StepTime>();

            at += toEdge.step.X();
            if (toEdge.step.X() != 0)
            {
                res.Add(toEdge);
            }

            if (TryMoveOverEdgeX(velocityX, i, now, endTime, at, globalPositionInt, "over edge x (StepXOrRemainder)", out var overEdge))
            {
                at += overEdge.step.X();
                if (overEdge.step.X() != 0)
                {
                    res.Add(overEdge);
                }
            }

            return res;
        }
        else
        {
            var remainderX = (int)(velocityX * (endTime - now));
            return new List<StepTime> { new StepTime(endTime, endTime, new GlobalPosionInt(remainderX, 0), "remainder x (StepXOrRemainder)") };
        }
    }

    public static List<StepTime> StepsX(int velocityX, double now, double endTime, GlobalPosionInt globalPositionInt)
    {
        if (velocityX == 0 || now == endTime)
        {
            return new List<StepTime>();
        }

        var res = new List<StepTime>();

        var countBy = velocityX > 0 ? 1 : -1;
        var i = 0;
        var at = globalPositionInt.X();

        var pressure = 0;

        while (true)
        {
            i += countBy;
            if (!TryMoveToEdgeX(velocityX, i, now, endTime, at, globalPositionInt, "to edge x", out var toEdge))
            {
                break;
            }

            at += toEdge.step.X();
            if (toEdge.step.X() != 0)
            {
                res.Add(toEdge);
            }

            if (!TryMoveOverEdgeX(velocityX, i, now, endTime, at, globalPositionInt, "over edge x", out var overEdge))
            {
                break;
            }
            at += overEdge.step.X();
            if (overEdge.step.X() != 0)
            {
                res.Add(overEdge);
            }

            pressure++;
            if (pressure > 1000)
            {
                Log.WriteLine("StepTime.StepsX high pressure");
                Debugger.Launch();
            }
        }
        var remainderX = ((int)(velocityX * (endTime - now)) - res.Aggregate(0, (sum, item) => sum + item.step.X()));
        if (remainderX != 0)
        {
            res.Add(new StepTime(endTime, endTime, new GlobalPosionInt(remainderX, 0), "remainder x"));
        }
        return res;
    }
    public static bool TryMoveToEdgeX(int velocityX, int steps, double now, double endTime, int from, GlobalPosionInt globalPosionInt, string stepType, [MaybeNullWhen(false)] out StepTime res)
    {
        var edge = steps > 0 ? globalPosionInt.ToMapPosition().Shift(steps, 0).ToGlobalPositionInt().X() - 1
                             : globalPosionInt.ToMapPosition().Shift(steps + 1, 0).ToGlobalPositionInt().X();
        var toEdge = edge - globalPosionInt.X();
        var edgeAt = now + (toEdge / (double)velocityX);
        if (edgeAt < endTime)
        {
            res = new StepTime(edgeAt, endTime, new GlobalPosionInt(edge - from, 0), stepType);
            return true;
        }
        res = default;
        return false;
    }

    public static bool TryMoveOverEdgeX(int velocityX, int steps, double now, double endTime, int from, GlobalPosionInt globalPosionInt, string stepType, [MaybeNullWhen(false)] out StepTime res)
    {
        var edge = steps > 0 ? globalPosionInt.ToMapPosition().Shift(steps, 0).ToGlobalPositionInt().X()
                             : globalPosionInt.ToMapPosition().Shift(steps + 1, 0).ToGlobalPositionInt().X() - 1;
        var toEdge = edge - globalPosionInt.X();
        var edgeAt = now + (toEdge / (double)velocityX);
        if (edgeAt < endTime)
        {
            res = new StepTime(edgeAt, endTime, new GlobalPosionInt(edge - from, 0), stepType);
            return true;
        }
        res = default;
        return false;
    }

    public static bool TryMoveToEdgeY(int velocityY, int steps, double now, double endTime, int from, GlobalPosionInt globalPosionInt, string stepType, [MaybeNullWhen(false)] out StepTime res)
    {
        var edge = steps > 0 ? globalPosionInt.ToMapPosition().Shift(0, steps).ToGlobalPositionInt().Y() - 1
                             : globalPosionInt.ToMapPosition().Shift(0, steps + 1).ToGlobalPositionInt().Y();
        var toEdge = edge - globalPosionInt.Y();
        var edgeAt = now + (toEdge / (double)velocityY);
        if (edgeAt < endTime)
        {
            res = new StepTime(edgeAt, endTime, new GlobalPosionInt(0, edge - from), stepType);
            return true;
        }
        res = default;
        return false;
    }

    public static bool TryMoveOverEdgeY(int velocityY, int steps, double now, double endTime, int from, GlobalPosionInt globalPosionInt, string stepType, [MaybeNullWhen(false)] out StepTime res)
    {
        var edge = steps > 0 ? globalPosionInt.ToMapPosition().Shift(0, steps).ToGlobalPositionInt().Y()
                             : globalPosionInt.ToMapPosition().Shift(0, steps + 1).ToGlobalPositionInt().Y() - 1;
        var toEdge = edge - globalPosionInt.Y();
        var edgeAt = now + (toEdge / (double)velocityY);
        if (edgeAt < endTime)
        {
            res = new StepTime(edgeAt, endTime, new GlobalPosionInt(0, edge - from), stepType);
            return true;
        }
        res = default;
        return false;
    }


    public static List<StepTime> StepsY(int velocityY, double now, double endTime, GlobalPosionInt globalPosionInt)
    {
        if (velocityY == 0 || now == endTime)
        {
            return new List<StepTime>();
        }

        var res = new List<StepTime>();

        var countBy = velocityY > 0 ? 1 : -1;
        var i = 0;
        var at = globalPosionInt.Y();

        var pressure = 0;

        while (true)
        {
            i += countBy;
            if (!TryMoveToEdgeY(velocityY, i, now, endTime, at, globalPosionInt, "to edge y", out var toEdge))
            {
                break;
            }

            at += toEdge.step.Y();
            if (toEdge.step.Y() != 0)
            {
                res.Add(toEdge);
            }

            if (!TryMoveOverEdgeY(velocityY, i, now, endTime, at, globalPosionInt, "over edge y", out var overEdge))
            {
                break;
            }
            at += overEdge.step.Y();
            if (overEdge.step.Y() != 0)
            {
                res.Add(overEdge);
            }

            pressure++;
            if (pressure > 1000)
            {
                Log.WriteLine("StepTime.StepsY high pressure");
                Debugger.Launch();
            }
        }
        var remainderY = (int)(velocityY * (endTime - now)) - res.Aggregate(0, (sum, item) => sum + item.step.Y());
        if (remainderY != 0)
        {
            res.Add(new StepTime(endTime, endTime, new GlobalPosionInt(0, remainderY), "remainder"));
        }
        return res;
    }
}

public class Move<T>
    where T : IMoveable
{


    public readonly T moveable;
    private readonly double endTime;

    // TODO make this private when you are done debugging it
    public List<StepTime> steps;

    public Move(T ant, GlobalPosionInt velocity, double now, double endTime=1)
    {
        this.moveable = ant;
        this.endTime = endTime;
        steps = StepTime.BuildSteps(velocity, now, endTime, moveable.GlobalPositionInt);
    }

    public Move(T moveable, double endTime, List<StepTime> steps)
    {
        this.moveable = moveable;
        this.endTime = endTime;
        this.steps = steps ?? throw new ArgumentNullException(nameof(steps));
    }

    //public void Init(GlobalPosionInt velocity, double now, double endTime = 1)
    //{
    //    steps = StepTime.BuildSteps(velocity, now, endTime, moveable.GlobalPositionInt);
    //}


    public double NextMoveAt()
    {
        if (steps.Count() == 0)
        {
            throw new Exception("this shouldn't have been called");
        }
        return steps.First().time;
    }

    internal StepTime FirstStep()
    {
        return steps[0];
        //return new Vector128<double>(Math.Sign(xMoves) * MapExtensions.MAP_GRID_SIZE, 0);
    }

    internal void CantMove(StepTime skipped)
    {
        // go ahead and move to the edge
        // recalcuate your steps but from the new poistion
        // and with the full skipped step spend

        if (skipped.step.X() != 0)
        {

            if (StepTime.TryMoveToEdgeX(moveable.VelocityInt.X(), Math.Sign(moveable.VelocityInt.X()), skipped.time, endTime, moveable.GlobalPositionInt.X(), moveable.GlobalPositionInt, "cant move x", out var step))
            {
                moveable.GlobalPositionInt += step.step;
            }

            steps = steps.Where(x => x.step.X() == 0).ToList();

            // take a little time out
            // it's going to try to enter the square again
            if (skipped.time + .1 < endTime)
            {
                steps.AddRange(StepTime.StepsX(moveable.VelocityInt.X(), skipped.time + .1, endTime, moveable.GlobalPositionInt));
            }

            steps = steps.OrderBy(x => x.time).ToList();
        }

        if (skipped.step.Y() != 0)
        {
            if (StepTime.TryMoveToEdgeY(moveable.VelocityInt.Y(), Math.Sign(moveable.VelocityInt.Y()), skipped.time, endTime, moveable.GlobalPositionInt.Y(), moveable.GlobalPositionInt, "cant move y", out var step))
            {
                moveable.GlobalPositionInt += step.step;
            }

            steps = steps.Where(x => x.step.Y() == 0).ToList();

            // take a little time out
            // it's going to try to enter the square again
            if (skipped.time + .1 < endTime)
            {
                steps.AddRange(StepTime.StepsY(moveable.VelocityInt.Y(), skipped.time + .1, endTime, moveable.GlobalPositionInt));
            }

            steps = steps.OrderBy(x => x.time).ToList();
        }
    }

    internal void Applied(StepTime step)
    {
        steps.Remove(step);
    }

    internal bool HasSteps()
    {
        return steps.Any();
    }
}

public static class MapExtensions
{

    public static Vector128<double> MAP_GRID_SIZE_VECTOR = Vector128.Create(MAP_GRID_SIZE, MAP_GRID_SIZE);
    public const double MAP_GRID_SIZE = 5;

    public static (int x, int y) MapPositionDifference((int x, int y) a, (int x, int y) b)
    {
        return (x: a.x - b.x, y: a.y - b.y);
    }

    public static decimal LengthSquaredDecimal(this Vector128<double> self)
    {
        // old comment from when self.x was a f-l-o-a-t (dashes so searching for that word doesn't bring it up)
        // interesting stackoverflow tho so I'll leave around
        // wow, https://stackoverflow.com/questions/8284275/casting-double-to-decimal-loses-precision-in-c-sharp
        // honestly I could probably go back to .Length() nothing should be going fast enough to break this
        return (((decimal)(double)self.X()) * ((decimal)(double)self.X())) + (((decimal)(double)self.Y()) * ((decimal)(double)self.Y()));
    }


    private class MoveComparer : IComparer<(double at, Guid id)>
    {
        public int Compare((double at, Guid id) x, (double at, Guid id) y)
        {
            var firstCompare = x.at.CompareTo(y.at);
            if (firstCompare != 0)
            {
                return firstCompare;
            }

            return x.id.CompareTo(y.id);
        }
    }

    // if I wanted to make this faster,
    // I think I could calculate less up front
    // just get the first move for each actor 
    //public static void MoveAnts(Map map, ConcurrentDictionary<Guid, AntState> ants, ConcurrentDictionary<BulletState.Immutable.Id, BulletState> bullets, int tick, TerrainMap terrainMap, EffectEvents events, EffectEventRequireVerification eventsRequiringVerification, MoneyState money, ControlState control)
    //{


    //    double now = 0;

    //    var moveDict = ants.Values
    //            .Select(x => new Move<AntState>(x, x.VelocityInt, now))
    //            .ToDictionary(x => x.moveable.immutable.id, x => x);

    //    var movers = moveDict.Where(x => x.Value.HasSteps()).Select(x=>x.Value).ToHashSet();

    //    var pressure = 0;
    //    var collidePressure = 0;
    //    var cantMovePressure = 0;

    //    //    Log.WriteLine($"new frame ");


    //    while (movers.Any())
    //    {

    //        pressure++;
    //        if (pressure > 10000)
    //        {
    //            Log.WriteLine("MapExtensions.MoveAnts, high pressure");
    //            Debugger.Launch();
    //        }

    //        var nextMove = movers.OrderBy(x=>x.NextMoveAt()).ThenBy(x=>x.moveable.immutable.id).First();

    //        var step1 = nextMove.FirstStep();
    //        now = step1.time;

    //        CheckAtPosition check = step1.step.X() > 0 ? CheckRightEdgeAtPosition :
    //                                step1.step.X() < 0 ? CheckLeftEdgeAtPosition :
    //                                step1.step.Y() > 0 ? CheckBottomEdgeAtPosition :
    //                                                   CheckTopEdgeAtPosition;

    //        var before = nextMove.moveable.GlobalPositionInt.ToMapPosition();
    //        var after = (step1.step + nextMove.moveable.GlobalPositionInt).ToMapPosition();

    //        if (Math.Abs(before.x - after.x) > 1)
    //        {
    //            Log.WriteLine("x is > 1");
    //        }
    //        if (Math.Abs(before.y - after.y) > 1)
    //        {
    //            Log.WriteLine("y is > 1");
    //        }

    //        // if we didn't leave our square, skip the check
    //        if ((step1.step + nextMove.moveable.GlobalPositionInt).ToMapPosition().Equals(nextMove.moveable.GlobalPositionInt.ToMapPosition()))
    //        {
    //            Move(Array.Empty<MapEntry>(), step1, false);
    //        }
    //        else if (check(map, nextMove.moveable, (nextMove.moveable.GlobalPositionInt + step1.step).ToMapPosition()).ToArray().Assign(out var step1Hit)
    //            .Where(x => x.Is1(out var _) || x.Is3(out var _))
    //            .Any()
    //            .Not())
    //        {
    //            if (nextMove.moveable.immutable.mudMode == Pathing.Mode.ImpassableMud && terrainMap.IsMud((nextMove.moveable.GlobalPositionInt + step1.step).ToMapPosition(), out var mudCenter)) {
    //                MoveAround(now, movers, nextMove.moveable, step1.step.Y() != 0, mudCenter);
    //            }
    //            else {

    //                Move(step1Hit, step1, true);
    //            }
    //        }
    //        else
    //        {
    //            // do step one and hit the thing
    //            var collidedWith = step1Hit.Where(x => x.Is1(out var _) || x.Is3(out var _)).First();

    //            if (!MovingTowardsEachOther(
    //                nextMove.moveable.velocity,
    //                collidedWith.Is1(out var otherAntId) ? ants[otherAntId].velocity : Vector128.Create(0.0, 0.0),
    //                nextMove.moveable.GlobalPosition,
    //                collidedWith.Is1(out var _) ? ants[otherAntId].GlobalPosition : collidedWith.Is3OrThrow().GlobalPosition))
    //            {
    //                cantMovePressure++;
    //                nextMove.CantMove(step1);
    //            }
    //            else if (collidedWith.Is1(out var _))
    //            {
    //                // Log.WriteLine($"collided {nextMove.moveable.immutable.id} by {step1.step} at {step1.time} - {step1.debugType}");
    //                var otherAnt = ants[otherAntId];

    //                if (otherAnt.immutable.side != nextMove.moveable.immutable.side)
    //                {
    //                    // we really hit another ant
    //                    var (v1, v2) = Collide(nextMove.moveable.velocity, otherAnt.velocity, nextMove.moveable.GlobalPosition, otherAnt.GlobalPosition, nextMove.moveable.immutable.mass, otherAnt.immutable.mass);
    //                    collidePressure++;

    //                    nextMove.moveable.VelocityInt = v1.ToGlobalPositionInt();
    //                    otherAnt.VelocityInt = v2.ToGlobalPositionInt();

    //                    movers.Remove(nextMove);
    //                    var newMove = new Move<AntState>(nextMove.moveable, nextMove.moveable.VelocityInt, Math.Min(1, now + .1)/* take a time out when you hit something, units packed together collide a lot and this can actually slow down the game*/);
    //                    movers.Add(newMove);

    //                    var otherMove = movers.Where(x => x.moveable.immutable.id == otherAnt.immutable.id).SingleOrDefault(); ;
    //                    if (otherMove != null)
    //                    {
    //                        movers.Remove(otherMove);
    //                    }
    //                    var otherNewMove = new Move<AntState>(otherAnt, ants[otherAntId].VelocityInt, Math.Min(1, now/*the other can move right away, it already lost some move*/));
    //                    movers.Add(otherNewMove);
    //                }
    //                else {
    //                    // try to move around the other
    //                    MoveAround(now, movers, nextMove.moveable, step1.step.Y() != 0,otherAnt.GlobalPosition);
    //                    // the other tries to get out of the way
    //                    if (otherAnt.velocity.LengthSquared() == 0)
    //                    {
    //                        var awayFromUs = Avx.Subtract(nextMove.moveable.GlobalPosition, otherAnt.GlobalPosition).NormalizedCopy();
    //                        var normalVelcory = nextMove.moveable.VelocityInt.ToGlobalVector().NormalizedCopy();
    //                        var awayAround = Avx.Add(awayFromUs, normalVelcory);
    //                        otherAnt.RequestFullSpeed(Avx.Multiply(awayAround, Vector128.Create(-1.0, -1.0)));
    //                        otherAnt.ApplyFiction(terrainMap);

    //                        // we assume the other wasn't moving
    //                        var otherNewMove = new Move<AntState>(otherAnt, ants[otherAntId].VelocityInt, Math.Min(1, now/*the other can move right away, it already lost some move*/));
    //                        movers.Add(otherNewMove);
    //                    }
    //                    else { 
    //                        // maybe I should do something here?
    //                        // maybe not, they'll hit use when they move
    //                        // and then they'll start sliding around
    //                    }
    //                }
    //            }
    //            else
    //            {
    //                // take your speed and reapply it to move around the rock
    //                MoveAround(now, movers, nextMove.moveable,  step1.step.Y() != 0, collidedWith.Is3OrThrow().GlobalPosition);


    //                // in case I need to hit rocks with physics
    //                // we hit a rock
    //                //moves.Remove((nextMove.NextMoveAt(), nextMove.moveable.immutable.id));
    //                //var v1 = Collide(nextMove.moveable.velocity, nextMove.moveable.GlobalPosition, collidedWith.Is3OrThrow().GlobalPosition);
    //                //nextMove.moveable.VelocityInt = v1.ToGlobalPositionInt();
    //                //nextMove.Init(nextMove.moveable.VelocityInt, Math.Min(1, now + .1));
    //                //if (nextMove.HasSteps())
    //                //{
    //                //    moves.Add((nextMove.NextMoveAt(), nextMove.moveable.immutable.id), nextMove);
    //                //}
    //            }

    //        }


    //        movers = movers.Where(x => x.HasSteps()).ToHashSet();

    //        void Move(
    //            IEnumerable<MapEntry> stepHit,
    //            StepTime step,
    //            bool updateMap)
    //        {
    //            var killed = false;
    //            foreach (var bulletId in stepHit.Select(x => x.Is2OrThrow()))
    //            {
    //                var bullet = bullets[bulletId];
    //                var (antKilled, bulletKilled, damage) = AntBulletCollide(nextMove.moveable, bullet, tick, money, control);
    //                if (bulletKilled)
    //                {
    //                    if (nextMove.moveable.immutable.side == bullet.immutable.side)
    //                    {
    //                        events.shotsHitFriendly.Add(new ShotHitFriendly(bullet.immutable.id, bullet.GlobalPosition));
    //                    }
    //                    else 
    //                    {
    //                        events.shotsHit.Add(new ShotHit(bullet.immutable.id, nextMove.moveable, bullet.GlobalPosition, damage));
    //                    }
    //                    bullets.TryRemove(bulletId, out var _);
    //                    map.Remove(bullet);
    //                }
    //                if (antKilled)
    //                {
    //                    eventsRequiringVerification.antsKilled.Add(new AntKilled(nextMove.moveable.immutable.id, nextMove.moveable.GlobalPosition));
    //                    map.Remove(nextMove.moveable);
    //                    ants.TryRemove(nextMove.moveable.immutable.id, out var _);
    //                    movers.Remove(nextMove);
    //                    killed = true;
    //                    break;
    //                }
    //            }

    //            if (!killed)
    //            {

    //                var (relativePositionsToRemove, relativePositionsToAdd)
    //                    = step1.step.X() > 0 ? (nextMove.moveable.ReletiveLeftPositions(), nextMove.moveable.ReletiveRightPositions()) :
    //                      step1.step.X() < 0 ? (nextMove.moveable.ReletiveRightPositions(), nextMove.moveable.ReletiveLeftPositions()) :
    //                      step1.step.Y() > 0 ? (nextMove.moveable.ReletiveTopPositions(), nextMove.moveable.ReletiveBottomPositions()) :
    //                                         (nextMove.moveable.ReletiveBottomPositions(), nextMove.moveable.ReletiveTopPositions());

    //                // remove and add back
    //                // we could probably save some work
    //                if (updateMap)
    //                {
    //                    map.RemoveEdge(nextMove.moveable, relativePositionsToRemove);
    //                }
    //                //Log.WriteLine($"step {step.step} {step.debugType} {step.time}");
    //                nextMove.moveable.GlobalPositionInt += step.step;
    //                nextMove.Applied(step);
    //                if (updateMap)
    //                {
    //                    map.AddEdge(nextMove.moveable, relativePositionsToAdd);
    //                }
    //            }
    //        }
    //    }

    //    if (collidePressure > 1000)
    //    {
    //        Log.WriteLine($"collidePressure: {collidePressure}");
    //    }

    //    if (cantMovePressure > 1000)
    //    {
    //        Log.WriteLine($"cantMovePressure: {cantMovePressure}");
    //    }

    //    static void MoveAround(double now, HashSet<Move<AntState>> movers, AntState moveable, bool moveY, Vector128<double> collidedWith)
    //    {
    //        var awayFromRock = Avx.Subtract(moveable.GlobalPosition, collidedWith).NormalizedCopy();
    //        var normalVelcory = moveable.VelocityInt.ToGlobalVector().NormalizedCopy();
    //        var awayAround = Avx.Add(awayFromRock, normalVelcory);
    //        var steps = new List<StepTime>();

    //        if (moveY)
    //        {
    //            steps.AddRange(StepTime.StepXOrRemainder((int)moveable.VelocityInt.Length() * Math.Sign(awayAround.X()), now, 1.0, moveable.GlobalPositionInt));
    //            var moved = steps.Sum(x => x.step.X());
    //            var finalTime = steps.Any() ? steps.Last().time : now;
    //            var remaining = moveable.VelocityInt.Length() - Math.Abs(moved);
    //            var finalPosition = steps.Aggregate(moveable.GlobalPositionInt, (pos, x) => pos + x.step);

    //            steps.AddRange(StepTime.StepsY((int)remaining * Math.Sign(awayAround.Y()), finalTime, 1.0, finalPosition));

    //        }
    //        else
    //        {
    //            steps.AddRange(StepTime.StepYOrRemainder((int)moveable.VelocityInt.Length() * Math.Sign(awayAround.Y()), now, 1.0, moveable.GlobalPositionInt));
    //            var moved = steps.Sum(x => x.step.Y());
    //            var finalTime = steps.Any() ? steps.Last().time : now;
    //            var remaining = moveable.VelocityInt.Length() - Math.Abs(moved);
    //            var finalPosition = steps.Aggregate(moveable.GlobalPositionInt, (pos, x) => pos + x.step);

    //            steps.AddRange(StepTime.StepsX((int)remaining * Math.Sign(awayAround.X()), finalTime, 1.0, finalPosition));
    //        }

    //        var toRemove = movers.Where(x => x.moveable.immutable.id == moveable.immutable.id).SingleOrDefault(); ;
    //        if (toRemove != null)
    //        {
    //            movers.Remove(toRemove);
    //        }
    //        var newMove = new Move<AntState>(moveable, 1.0, steps);
    //        if (newMove.HasSteps())
    //        {
    //            movers.Add(newMove);
    //        }
    //    }
    //}

    // I should just pass the whole state
    //public static void MoveBullets(Map map, ConcurrentDictionary<Guid, AntState> ants, ConcurrentDictionary<BulletState.Immutable.Id, BulletState> bullets, int tick, TerrainMap terrainMap, EffectEvents events, EffectEventRequireVerification eventsRequiringVerification, MoneyState money, ControlState control)
    //{
    //    var steps = bullets.Values
    //        .Where(bullet => bullet.immutable.speed != 0)
    //        .SelectMany(bullet => StepTime.BuildSteps(bullet.VelocityInt, 0,1, bullet.GlobalPositionInt).Select(step => (bullet, step)))
    //        .OrderBy(x => x.step.time)
    //        .ThenBy(x => x.bullet.immutable.id.shooterId)
    //        .ThenBy(x => x.bullet.immutable.id.shotOnFrame)
    //        .ThenBy(x => x.bullet.immutable.id.shotIndex)
    //        .ToArray();

    //    foreach (var (bullet, step) in steps)
    //    {
    //        if (!bullets.ContainsKey(bullet.immutable.id))
    //        {
    //            continue;
    //        }


    //        CheckAtPosition check = step.step.X() > 0 ? CheckRightEdgeAtPosition :
    //                                step.step.X() < 0 ? CheckLeftEdgeAtPosition :
    //                                step.step.Y() > 0 ? CheckBottomEdgeAtPosition :
    //                                                   CheckTopEdgeAtPosition;

    //        // if we didn't leave our square, skip the check
    //        if ((step.step + bullet.GlobalPositionInt).ToMapPosition().Equals(bullet.GlobalPositionInt.ToMapPosition()))
    //        {
    //            Move(Array.Empty<MapEntry>(), step);
    //        }
    //        else
    //        {
    //            var atInt = (step.step + bullet.GlobalPositionInt);
    //            if (terrainMap.Cover(atInt.ToMapPosition())) {
    //                var distance = (atInt - bullet.immutable.startedAt).ToGlobalVector().Length();
    //                if (distance > bullet.immutable.ignoreCoverFor) 
    //                {
    //                    // we are dead
    //                    events.shotsHitCover.Add(new ShotHitCover(bullet.immutable.id, bullet.GlobalPosition));
    //                    bullets.TryRemove(bullet.immutable.id, out var _);
    //                    map.Remove(bullet);
    //                    continue;
    //                }
    //            }

    //            var step1Hit = check(map, bullet, (bullet.GlobalPositionInt + step.step).ToMapPosition()).ToArray();

    //            Move(step1Hit, step);
    //        }

    //        void Move(
    //            IEnumerable<MapEntry> stepHit,
    //            StepTime step)
    //        {
    //            foreach (var thing in stepHit)
    //            {
    //                // we are dead
    //                if (thing.Is3(out var rock))
    //                {
    //                    events.shotsHitRock.Add(new ShotHitRock(bullet.immutable.id, bullet.GlobalPosition));
    //                    bullets.TryRemove(bullet.immutable.id, out var _);
    //                    map.Remove(bullet);
    //                    return;
    //                }

    //                if (thing.Is1(out var antId))
    //                {
    //                    var ant = ants[antId];
    //                    var (antKilled, bulletKilled, damage) = AntBulletCollide(ant, bullet, tick, money, control);
    //                    if (antKilled)
    //                    {
    //                        eventsRequiringVerification.antsKilled.Add(new AntKilled(ant.immutable.id, ant.GlobalPosition));
    //                        map.Remove(ant);
    //                        ants.TryRemove(antId, out var _);
    //                    }
    //                    if (bulletKilled)
    //                    {
    //                        if (ant.immutable.side == bullet.immutable.side)
    //                        {
    //                            events.shotsHitFriendly.Add(new ShotHitFriendly(bullet.immutable.id, bullet.GlobalPosition));
    //                        }
    //                        else
    //                        {
    //                            events.shotsHit.Add(new ShotHit(bullet.immutable.id, ant, bullet.GlobalPosition, damage));
    //                        }
    //                        bullets.TryRemove(bullet.immutable.id, out var _);
    //                        map.Remove(bullet);
    //                        return;
    //                    }
    //                }
    //            }

    //            // remove and add back
    //            // we could probably save some work
    //            map.Remove(bullet);
    //            bullet.GlobalPositionInt += step.step;
    //            map.AddToMap(bullet, bullet.GlobalPositionInt);
    //        }
    //    }
    //}

    // dup
    // {60AA4B5C-A478-405A-9067-DF94213B1299}
    public static (bool antKilled, bool bulletKilled, double damage) AntBulletCollide(AntState ant, BulletState bullet, int tick, SimulationState state)
    {
        if (bullet.immutable.id.shooterId == ant.immutable.id) 
        {
            if (!bullet.immutable.damagesShooter)
            {
                return (false, false, 0);
            }
        }
        else if (!bullet.immutable.damagesTeam && ant.immutable.side == bullet.immutable.side)
        {
            return (false, false, 0);
        }

        var hit = ant.Hit( bullet.immutable.damage, bullet.CurrentPierce(), bullet.velocity,bullet.immutable.knockBack, bullet.immutable.stun, tick, state);
        return (hit.Item1, true, hit.Item2);

    }

    // dup
    //{60AA4B5C-A478-405A-9067-DF94213B1299}
    public static (bool antKilled, bool bulletKilled, double damage) AntArcBulletCollide(AntState ant, ArcBulletState bullet, int tick, SimulationState state)
    {
        if (!bullet.immutable.damagesTeam && ant.immutable.side == bullet.immutable.side)
        {
            return (false, false, 0);
        }
        var hit = ant.Hit(bullet.immutable.damage, bullet.immutable.pierce, bullet.velocity, bullet.immutable.knockBack, bullet.immutable.stun, tick, state);
        return (hit.Item1, true, hit.Item2);

    }

    private const double CLOSE = .01;

    // they could already be moving apart
    public static bool MovingTowardsEachOther(Vector128<double> v1, Vector128<double> v2, Vector128<double> position1, Vector128<double> postition2)
    {
        var dis = Avx.Subtract(position1, postition2);

        // I don't think I need to nornalize
        // srt is slow and probably cost accuracy 
        //var normal = dis.NormalizedCopy();

        var s1 = v1.Dot(dis);
        var s2 = v2.Dot(dis);

        if (Math.Abs(s1 - s2) < CLOSE) {
            return MovingTowardsEachOtherDecimal(v1, v2, position1, postition2);
        }

        return s1 - s2 < 0;
    }

    // sometimes doubles produce the wrong outcome
    // or atleast an outcome that disagrees with Collide
    public static bool MovingTowardsEachOtherDecimal(Vector128<double> v1, Vector128<double> v2, Vector128<double> position1, Vector128<double> postition2)
    {
        var dx = (decimal)position1.X() - (decimal)postition2.X();
        var dy = (decimal)position1.Y() - (decimal)postition2.Y();

        var s1 = (decimal)v1.X() * dx + (decimal)v1.Y() * dy;
        var s2 = (decimal)v2.X() * dx + (decimal)v2.Y() * dy;

        return s1 - s2 < 0;
    }

    public static Vector128<double> Collide(Vector128<double> v1, Vector128<double> position1, Vector128<double> postition2)
    {
        var dis = Avx.Subtract(position1, postition2);

        var normal = dis.NormalizedCopy();

        var s1 = v1.Dot(normal);

        return Avx.Subtract(v1, Avx.Multiply((2 * s1).AsVector(), normal));
    }

    public static (Vector128<double> v1f, Vector128<double> v2f) Collide(
        Vector128<double> v1,
        Vector128<double> v2,
        Vector128<double> position1,
        Vector128<double> postition2,
        double m1,
        double m2)
    {
        var dis = Avx.Subtract(position1, postition2);

        var normal = dis.NormalizedCopy();

        var s1 = v1.Dot(normal);
        var s2 = v2.Dot(normal);

        var c1 = (s1 * m1) + (s2 * m2);
        var c2 = (s1 * s1 * m1) + (s2 * s2 * m2);

        var A = (m2 * m2) + (m2 * m1); // this doens't look right, is this right?
        var B = -2 * m2 * c1;
        var C = (c1 * c1) - (c2 * m1);

        double sf2;

        if (A != 0)
        {
            // b^2 - 4ac
            var D = (B * B) - (4 * A * C);


            if (D >= 0)
            {
                var sf2_plus = (-B + Math.Sqrt(D)) / (2 * A);
                var sf2_minus = (-B - Math.Sqrt(D)) / (2 * A);

                if (sf2_minus != s2 && sf2_minus != s2 && sf2_plus != sf2_minus)
                {
                    if (Math.Abs(s2 - sf2_plus) > Math.Abs(s2 - sf2_minus))
                    {
                        if (Math.Abs(s2 - sf2_minus) > CLOSE)
                        {
                            return CollideDecimal(v1, v2, position1, postition2, m1, m2);
                        }
                        sf2 = sf2_plus;
                    }
                    else
                    {
                        if (Math.Abs(s2 - sf2_plus) > CLOSE)
                        {
                            return CollideDecimal(v1, v2, position1, postition2, m1, m2);
                        }
                        sf2 = sf2_minus;
                    }
                }
                else if (sf2_minus != s2)
                {
                    sf2 = sf2_minus;
                }
                else if (sf2_plus != s2)
                {
                    sf2 = sf2_plus;
                }
                else
                {
                    return CollideDecimal(v1, v2, position1, postition2, m1, m2);
                }
            }
            else
            {
                return CollideDecimal(v1, v2, position1, postition2, m1, m2);
            }
        }
        else
        {
            return CollideDecimal(v1, v2, position1, postition2, m1, m2);
        }

        var force = Avx.Multiply(normal, ((double)((sf2 - s2) * m2)).AsVector());
        return (Avx.Subtract(v1, Avx.Divide(force, m1.AsVector())), Avx.Add(v2, Avx.Divide(force, m2.AsVector())));
    }

    public static (Vector128<double> v1f, Vector128<double> v2f) CollideDecimal(
        Vector128<double> v1, 
        Vector128<double> v2, 
        Vector128<double> position1, 
        Vector128<double> postition2, 
        double m1dub, 
        double m2dub)
    {
        var m1 = (decimal)m1dub;
        var m2 = (decimal)m2dub;

        var dx = (decimal)position1.X() - (decimal)postition2.X();
        var dy = (decimal)position1.Y() - (decimal)postition2.Y();
        var length = Sqrt((dx * dx) + (dy * dy));
        var nx = dx / length;
        var ny = dy / length;

        var s1 = (decimal)v1.X() * dx + (decimal)v1.Y() * dy; 
        var s2 = (decimal)v2.X() * dx + (decimal)v2.Y() * dy;

        var c1 = (s1 * m1) + (s2 * m2);
        var c2 = (s1 * s1 * m1) + (s2 * s2 * m2);

        var A = (m2 * m2) + (m2 * m1); // this doens't look right, is this right?
        var B = -2 * m2 * c1;
        var C = (c1 * c1) - (c2 * m1);

        decimal sf2;

        if (A != 0)
        {
            // b^2 - 4ac
            var D = (B * B) - (4 * A * C);

            if (D >= 0)
            {
                var sf2_plus = (-B + Sqrt(D)) / (2 * A);
                var sf2_minus = (-B - Sqrt(D)) / (2 * A);

                if (sf2_minus != s2 && sf2_minus != s2 && sf2_plus != sf2_minus)
                {
                    if (Math.Abs(s2 - sf2_plus) > Math.Abs(s2 - sf2_minus))
                    {
                        sf2 = sf2_plus;
                    }
                    else
                    {
                        sf2 = sf2_minus;
                    }
                }
                else if (sf2_minus != s2)
                {
                    sf2 = sf2_minus;
                }
                else if (sf2_plus != s2)
                {
                    sf2 = sf2_plus;
                }
                else
                {
                    throw new Exception($"we are getting no vfs, v1: {v1}, v2: {v2}, pos: {position1}, pos: {postition2}, m1: {m1},  m2: {m2}");
                }
            }
            else
            {
                throw new Exception($"should not be negative, v1: {v1}, v2: {v2}, pos: {position1}, pos: {postition2}, m1: {m1},  m2: {m2}");
            }
        }
        else
        {
            throw new Exception($"A should not be 0! if A is zero something has 0 mass, v1: {v1}, v2: {v2}, pos: {position1}, pos: {postition2}, m1: {m1},  m2: {m2}");
        }

        var fx = nx * (sf2 - s2) * m2;
        var fy = ny * (sf2 - s2) * m2;
        var res1x = (decimal)v1.X() - (fx / m1);
        var res1y = (decimal)v1.Y() - (fy / m1);
        var res2x = (decimal)v2.X() + (fx / m2);
        var res2y = (decimal)v2.Y() + (fy / m2);

        return (Vector128.Create((double)res1x, (double)res1y), Vector128.Create((double)res2x, (double)res2y));
    }

    // https://stackoverflow.com/questions/4124189/performing-math-operations-on-decimal-datatype-in-c
    // x - a number, from which we need to calculate the square root
    // epsilon - an accuracy of calculation of the root from our number.
    // The result of the calculations will differ from an actual value
    // of the root on less than epslion.
    public static decimal Sqrt(decimal x, decimal epsilon = 0.0M)
    {
        if (x < 0) throw new OverflowException("Cannot calculate square root from a negative number");

        decimal current = (decimal)Math.Sqrt((double)x), previous;
        do
        {
            previous = current;
            if (previous == 0.0M) return 0;
            current = (previous + x / previous) / 2;
        }
        while (Math.Abs(previous - current) > epsilon);
        return current;
    }

    //public delegate IEnumerable<MapEntry> CheckAtPosition(Map map, IMappable subject, (int x, int y) position);

    //public static IEnumerable<MapEntry> CheckTopEdgeAtPosition(Map map, IPositionlessMappable subject, (int x, int y) position)
    //    => CheckEdge(map, subject.ReletiveTopPositions().Select(x => x.Shift(position.x, position.y)));

    //public static IEnumerable<MapEntry> CheckBottomEdgeAtPosition(Map map, IPositionlessMappable subject, (int x, int y) position)
    //    => CheckEdge(map, subject.ReletiveBottomPositions().Select(x => x.Shift(position.x, position.y)));

    //public static IEnumerable<MapEntry> CheckLeftEdgeAtPosition(Map map, IPositionlessMappable subject, (int x, int y) position)
    //    => CheckEdge(map, subject.ReletiveLeftPositions().Select(x => x.Shift(position.x, position.y)));

    //public static IEnumerable<MapEntry> CheckRightEdgeAtPosition(Map map, IPositionlessMappable subject, (int x, int y) position)
    //    => CheckEdge(map, subject.ReletiveRightPositions().Select(x => x.Shift(position.x, position.y)));

    //// things location has not yet been updated
    //public static IEnumerable<MapEntry> CheckEdge(Map map, IEnumerable<(int x, int y)> edge)
    //{
    //    foreach (var item in edge)
    //    {
    //        if (map.TryRawGet(item, out var list))
    //        {
    //            foreach (var entry in list)
    //            {
    //                yield return entry;
    //            }
    //        }
    //    }
    //}


    public static (int x, int y) ToMapPosition(this Vector128<double> globalPosition)
    {
        var res = Avx.Floor(Avx.Divide(globalPosition, MapExtensions.MAP_GRID_SIZE_VECTOR));
        return ((int)res.X(), (int)res.Y());
    }

    public static Vector128<double> ToGlobalPosition(this (int x, int y) globalPosition)
    {
        return Avx.Multiply(Vector128.Create((double)globalPosition.x, (double)globalPosition.y), MapExtensions.MAP_GRID_SIZE_VECTOR);
    }

    // {597A8F3F-CDBA-420F-BD78-386E3F71D9BA}
    //internal static (int x, int y) Next((int x, int y) at, (int x, int y) target)
    //{
    //    var xOp = at.x < target.x ? (at.x + 1, at.y)
    //                                     : (at.x - 1, at.y);
    //    var yOp = at.y < target.y ? (at.x, at.y + 1)
    //                                 : (at.x, at.y - 1);

    //    return LengthSquaredBetween(xOp, target) < LengthSquaredBetween(yOp, target) ? xOp : yOp;

    //}

    internal static IEnumerable<(int x, int y)> PointsBetween(Vector128<double> startAt, Vector128<double> endAt) 
    {
        var difference = Avx.Subtract(endAt, startAt);

        var distance = difference.Length();
        var steps = Math.Ceiling(distance / MapExtensions.MAP_GRID_SIZE);
        var stepLength = distance / steps;

        var step = difference.ToLength(stepLength);

        var at = startAt;

        var last = startAt.ToMapPosition();
        for (int i = 1; i <= steps; i++)
        {
            at = Avx.Add(step, at);
            var mapAt = at.ToMapPosition();
            if (last != mapAt) 
            {
                yield return mapAt; 
            }
            last = mapAt;
        }

    }

    //internal static IEnumerable<(MapEntry hit, Vector128<double> positionBeforeHit)> RayCast(Map map, Vector128<double> globalPosition, Vector128<double> target)
    //{
    //    var last = globalPosition.ToMapPosition();
    //    foreach (var mapPosition in PointsBetween(globalPosition, target))
    //    {
    //        if (map.TryRawGet(mapPosition, out var list))
    //        {
    //            foreach (var item in list)
    //            {
    //                yield return (item, last.ToGlobalPosition());
    //            }
    //        }
    //        last = mapPosition;
    //    }


    //    //var mapAt = ToMapPosition(globalPosition);
    //    //var mapTarget = ToMapPosition(target);
    //    //if (mapTarget == mapAt)
    //    //{
    //    //    yield break;
    //    //}
    //    //var pressure = 0;
    //    //while (true)
    //    //{
    //    //    pressure++;
    //    //    if (pressure > 1000)
    //    //    {
    //    //        Debugger.Launch();
    //    //    }

    //    //    // this isn't good at all, these are probably all broken    
    //    //    // {597A8F3F-CDBA-420F-BD78-386E3F71D9BA}
    //    //    var xOp = mapAt.x < mapTarget.x ? (mapAt.x + 1, mapAt.y)
    //    //                                 : (mapAt.x - 1, mapAt.y);
    //    //    var yOp = mapAt.y < mapTarget.y ? (mapAt.x, mapAt.y + 1)
    //    //                                 : (mapAt.x, mapAt.y - 1);

    //    //    var nextMapAt = LengthSquaredBetween(xOp, mapTarget) < LengthSquaredBetween(yOp, mapTarget) ? xOp : yOp;

    //    //    if (map.TryRawGet(nextMapAt, out var list))
    //    //    {
    //    //        foreach (var item in list)
    //    //        {
    //    //            yield return (item, Vector128.Create((double)mapAt.x, mapAt.y));
    //    //        }
    //    //    }


    //    //    if (LengthSquaredBetween(nextMapAt, mapTarget) == 0)
    //    //    {
    //    //        yield break;
    //    //    }

    //    //    mapAt = nextMapAt;
    //    //}
    //}

    //internal static IEnumerable<(MapEntry hit, Vector128<double> positionBeforeHit)> RayCast(Map map, IMappable self, Vector128<double> target)
    //{

    //    var mapAt = ToMapPosition(self.GlobalPositionInt);
    //    var mapTarget = ToMapPosition(target);
    //    if (mapTarget == mapAt)
    //    {
    //        yield break;
    //    }
    //    var pressure = 0;
    //    while (true)
    //    {
    //        pressure++;
    //        if (pressure > 1000)
    //        {
    //            Debugger.Launch();
    //        }

    //        // this one is good
    //        // {597A8F3F-CDBA-420F-BD78-386E3F71D9BA}

    //        ((int, int), CheckAtPosition) xOp = mapAt.x < mapTarget.x ? ((mapAt.x + 1, mapAt.y), CheckRightEdgeAtPosition)
    //                                                               : ((mapAt.x - 1, mapAt.y), CheckLeftEdgeAtPosition);
    //        ((int, int), CheckAtPosition) yOp = mapAt.y < mapTarget.y ? ((mapAt.x, mapAt.y + 1), CheckBottomEdgeAtPosition)
    //                                                               : ((mapAt.x, mapAt.y - 1), CheckTopEdgeAtPosition);

    //        var nextMapAt = LengthSquaredBetween(xOp.Item1, mapTarget) < LengthSquaredBetween(yOp.Item1, mapTarget) ? xOp : yOp;

    //        foreach (var item in nextMapAt.Item2(map, self, nextMapAt.Item1))
    //        {
    //            yield return (item, Vector128.Create((double)mapAt.x, mapAt.y));
    //        }

    //        if (LengthSquaredBetween(nextMapAt.Item1, mapTarget) == 0)
    //        {
    //            yield break;
    //        }

    //        mapAt = nextMapAt.Item1;
    //    }
    //}

    //internal static bool CanMoveTo(Map map, IMappable self, Vector128<double> target)
    //{
    //    foreach (var (hit, _) in RayCast(map, self.GlobalPositionInt.ToGlobalVector(), target))
    //    {
    //        if (hit.Is3(out RockState _))
    //        {
    //            return false;
    //        }
    //    }
    //    return true;
    //}

    private static double LengthBetween((int, int) item1, (int x, int y) target)
    {
        return Math.Sqrt(
            ((item1.Item1 - target.x) * (item1.Item1 - target.x)) +
            ((item1.Item2 - target.y) * (item1.Item2 - target.y)));
    }
    private static double LengthSquaredBetween((int, int) item1, (int x, int y) target)
    {
        return
            ((item1.Item1 - target.x) * (item1.Item1 - target.x)) +
            ((item1.Item2 - target.y) * (item1.Item2 - target.y));
    }

    /// <summary>
    /// assumes all interior angles are less than 180 degrees 
    /// </summary>
    public static bool IsInShape(Vector128<double>[] pointsClockwise, Vector128<double> point)
    {

        for (int i = 0; i < pointsClockwise.Length; i++)
        {
            var from = pointsClockwise[i];
            var to = pointsClockwise[(i + 1) % pointsClockwise.Length];

            var parameterLine = Avx.Subtract(to, from);
            var pointLine = Avx.Subtract(point, from);

            // counter clockwise is position
            var angle = parameterLine.AngleTo(pointLine);
            if (angle > 0 || angle < -Math.PI)
            {
                return false;
            }
        }

        return true;


    }

    public static (int x, int y) ToMapPosition(this (int x, int y) position, IMappable mappable)
    {
        var center = mappable.GlobalPositionInt.ToMapPosition();

        return (position.x + center.x, position.y + center.y);
    }

    public static (int x, int y) ToMapPosition(this GlobalPosionInt position)
    {
        var res = Avx.Floor(position / Avx.Multiply(GlobalPosionInt.scaleVector, MapExtensions.MAP_GRID_SIZE_VECTOR));

        return ((int)res.X(), (int)res.Y());
    }

    public static Vector128<double> ToGodotPosition(this (int x, int y) position)
    {
        return Avx.Multiply(Vector128.Create((double)position.x, position.y), MAP_GRID_SIZE_VECTOR);
    }

    public static (int x, int y) Shift(this (int x, int y) position, int x, int y)
    {
        return (position.x + x, position.y + y);
    }


    public static (int x, int y) Add(this (int x, int y) a, (int x, int y) b)
    {
        return (a.x + b.x, a.y + b.y);
    }


    public static (int x, int y) Subtract(this (int x, int y) a, (int x, int y) b)
    {
        return (a.x - b.x, a.y - b.y);
    }
}

public interface IMappable : IPositionlessMappable
{
    GlobalPosionInt GlobalPositionInt { get; set; }
}


public interface IPositionlessMappable
{

    MapEntry ToMapEntry();
    (int x, int y)[] ReletivePositions();
    (int x, int y)[] ReletiveTopPositions();
    (int x, int y)[] ReletiveBottomPositions();
    (int x, int y)[] ReletiveLeftPositions();
    (int x, int y)[] ReletiveRightPositions();
    (int x, int y)[] ReletiveEdgePositions();
    //(int x, int y)[] ReletiveBottomRightPositions();
    //(int x, int y)[] ReletiveTopLeftPositions();
    //(int x, int y)[] ReletiveTopRightPositions();
    //(int x, int y)[] ReletiveBottomLeftPositions();
}

public class Mappable: IStateHash
{
    private (int x, int y)[] reletivePositions;

    private (int x, int y)[] reletiveTopPositions;
    private (int x, int y)[] reletiveBottomPositions;

    private (int x, int y)[] reletiveLeftPositions;
    private (int x, int y)[] reletiveRightPositions;

    //private (int x, int y)[] reletiveBottomRightPositions;
    //private (int x, int y)[] reletiveTopLeftPositions;

    //private (int x, int y)[] reletiveTopRightPositions;
    //private (int x, int y)[] reletiveBottomLeftPositions;

    private (int x, int y)[] reletiveEdgePositions;

    // in GlobalPosition units
    public readonly double radius;

    public Mappable(double radius)
    {
        var reletivePositionsList = new List<(int x, int y)>();
        for (double x = -radius; x <= radius; x += MapExtensions.MAP_GRID_SIZE)
        {
            for (double y = -radius; y <= radius; y += MapExtensions.MAP_GRID_SIZE)
            {
                if (Math.Sqrt(((x - MapExtensions.MAP_GRID_SIZE) * (x - MapExtensions.MAP_GRID_SIZE)) + ((y - MapExtensions.MAP_GRID_SIZE) * (y - MapExtensions.MAP_GRID_SIZE))) < radius &&
                    Math.Sqrt(((x + MapExtensions.MAP_GRID_SIZE) * (x + MapExtensions.MAP_GRID_SIZE)) + ((y - MapExtensions.MAP_GRID_SIZE) * (y - MapExtensions.MAP_GRID_SIZE))) < radius &&
                    Math.Sqrt(((x - MapExtensions.MAP_GRID_SIZE) * (x - MapExtensions.MAP_GRID_SIZE)) + ((y + MapExtensions.MAP_GRID_SIZE) * (y + MapExtensions.MAP_GRID_SIZE))) < radius &&
                    Math.Sqrt(((x + MapExtensions.MAP_GRID_SIZE) * (x + MapExtensions.MAP_GRID_SIZE)) + ((y + MapExtensions.MAP_GRID_SIZE) * (y + MapExtensions.MAP_GRID_SIZE))) < radius)
                {
                    reletivePositionsList.Add(Vector128.Create(x, y).ToMapPosition());
                }
            }
        }

        reletivePositions = reletivePositionsList.ToArray();

        // up is -y
        reletiveTopPositions = reletivePositions.GroupBy(point => point.x).Select(column => (column.Key, column.Select(point => point.y).Min())).ToArray();
        reletiveBottomPositions = reletivePositions.GroupBy(point => point.x).Select(column => (column.Key, column.Select(point => point.y).Max())).ToArray();

        reletiveLeftPositions = reletivePositions.GroupBy(point => point.y).Select(row => (row.Select(point => point.x).Min(), row.Key)).ToArray();
        reletiveRightPositions = reletivePositions.GroupBy(point => point.y).Select(row => (row.Select(point => point.x).Max(), row.Key)).ToArray();

        //reletiveBottomRightPositions = reletivePositions.GroupBy(point => point.y - point.x).Select(row => (row.Select(point => point.x).Max(), row.Select(point => point.y).Max())).ToArray();
        //reletiveTopLeftPositions = reletivePositions.GroupBy(point => point.y - point.x).Select(row => (row.Select(point => point.x).Min(), row.Select(point => point.y).Min())).ToArray();

        //reletiveTopRightPositions = reletivePositions.GroupBy(point => point.y + point.x).Select(row => (row.Select(point => point.x).Max(), row.Select(point => point.y).Min())).ToArray();
        //reletiveBottomLeftPositions = reletivePositions.GroupBy(point => point.y + point.x).Select(row => (row.Select(point => point.x).Min(), row.Select(point => point.y).Max())).ToArray();

        reletiveEdgePositions = reletiveTopPositions.Union(reletiveBottomPositions).Distinct().ToArray();
        this.radius = radius;
    }

    public (int x, int y)[] ReletiveBottomPositions() => reletiveBottomPositions;

    public (int x, int y)[] ReletiveLeftPositions() => reletiveLeftPositions;

    public (int x, int y)[] ReletivePositions() => reletivePositions;

    public (int x, int y)[] ReletiveRightPositions() => reletiveRightPositions;

    public (int x, int y)[] ReletiveTopPositions() => reletiveTopPositions;
    public (int x, int y)[] ReletiveEdgePositions() => reletiveEdgePositions;

    //public override bool Equals(object? obj) => this.HashEquals(obj);

    //public override int GetHashCode()=> this.Hash();
    
    public IEnumerable<(string, object?)> HashData()
    {
        yield return (nameof(radius), radius);
    }
    //public (int x, int y)[] ReletiveBottomRightPositions() => reletiveBottomRightPositions;
    //public (int x, int y)[] ReletiveTopLeftPositions() => reletiveTopLeftPositions;
    //public (int x, int y)[] ReletiveTopRightPositions() => reletiveTopRightPositions;
    //public (int x, int y)[] ReletiveBottomLeftPositions() => reletiveBottomLeftPositions;
}