﻿using System;
using System.Numerics;
using System.Runtime.Intrinsics;
using System.Runtime.Intrinsics.X86;

namespace Ants.Domain.Infrastructure;

public static class VectorExtensions
{
    public static Vector128<double> Make(double x, double y)
    {

        var res = Vector128.Create(x, y);
        return res;

    }

    public static double X(this Vector128<double> self)
    {
        return self.GetElement(0);
    }

    public static double Y(this Vector128<double> self)
    {
        return self.GetElement(1);
    }

    public static Vector128<double> AsVector(this double d)
    {
        return Vector128.Create(d, d);
    }
    public static Vector128<double> AsVector(this int i)
    {
        return Vector128.Create(i, (double)i);
    }

    public static Vector128<double> ProjectCopy(this Vector128<double> self, Vector128<double> alongNotNormalized)
    {
        var alongNormalized = alongNotNormalized.NormalizedCopy();
        return Sse2.Multiply(alongNormalized, self.Dot(alongNormalized).AsVector());
    }

    /// <summary>
    /// counterclockwise 
    /// https://stackoverflow.com/questions/14607640/rotating-a-vector-in-3d-space
    /// </summary>
    public static Vector128<double> Rotated(this Vector128<double> self, double rads)
    {
        return Vector128.Create(
            Math.Cos(rads) * self.X() - Math.Sin(rads) * self.Y(),
            Math.Sin(rads) * self.X() + Math.Cos(rads) * self.Y()
        );
    }

    public static double Length(this Vector128<double> self)
    {
        return Math.Sqrt(self.LengthSquared());
    }

    public static double LengthSquared(this Vector128<double> self)
    {
        return self.Dot(self);
    }

    public static Vector128<double> SafeNormalizedCopy(this Vector128<double> self)
    {
        if (self.Length() == 0)
        {
            return Vector128.Create(1.0, 0.0);
        }

        return Sse2.Divide(self, self.Length().AsVector());
    }

    public static Vector128<double> NormalizedCopy(this Vector128<double> self)
    {
#if DEBUG
        if (self.Length() == 0) { 
            throw new DivideByZeroException();
        }
#endif

        return Sse2.Divide(self, self.Length().AsVector());
    }

    public static Vector128<double> ToLength(this Vector128<double> self, double targetLength)
    {
        var currentLength = self.Length();
        return Sse2.Multiply(self, (targetLength/ currentLength).AsVector());
    }

    public static double Dot(this Vector128<double> self, Vector128<double> target)
    {
        var res = Sse2.Multiply(self, target);
        return res.X() + res.Y();
    }

    public static double Cross(this Vector128<double> self, Vector128<double> target)
    {
        return self.X() * target.Y() - self.Y() * target.X();
    }

    public static double AngleTo(this Vector128<double> self, Vector128<double> target)
    {
        return Math.Atan2(self.Cross(target), self.Dot(target));
    }

    public static double Angle(this Vector128<double> self)
    {
        return Math.Atan2(self.Y(), self.X());
    }
}