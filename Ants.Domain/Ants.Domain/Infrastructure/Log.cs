﻿using Ants.Domain;
using Prototypist.TaskChain;
using System;
using System.Threading.Channels;
// you should probably mock?
// or make an interface
public static class Log
{
    public const string Logs = "log";
    public static readonly ConcurrentLinkedList<ILogger> loggers = new ConcurrentLinkedList<ILogger>();

    public static FileLogger? bugLogger = null;

    public static FileLogger? bugLogger2 = null;

    public static void StartBugLogger(int port)
    {
        bugLogger = new FileLogger();
        bugLogger.Start($"bug-{port}").ContinueWith(x =>
        {
            if (x.IsFaulted)
            {
                Log.WriteLine("bug logger IsFaulted");
            }
            if (x.Exception != null)
            {
                Log.WriteLine(x.Exception.ToString());
            }
        });
    }
    public static void StartBugLogger2(int port)
    {
        bugLogger2 = new FileLogger();
        bugLogger2.Start($"bug-2-{port}").ContinueWith(x =>
        {
            if (x.IsFaulted)
            {
                Log.WriteLine("bug logger IsFaulted");
            }
            if (x.Exception != null)
            {
                Log.WriteLine(x.Exception.ToString());
            }
        });
    }
    public static void WriteLine(string v)
    {
        foreach (var logger in loggers)
        {
            logger.WriteLine(v);
        }
    }
}


public interface ILogger{
    void WriteLine(string v);
}

public class ConsoleLogger : ILogger
{
    public void WriteLine(string v)
    {
        Console.WriteLine(v);
    }
}

public class FileLogger : ILogger
{
    private class Entry
    {
        public DateTime timeLocal;
        public string s;
        public int processing = 0;

        public Entry(string s)
        {
            this.s = s ?? throw new ArgumentNullException(nameof(s));
            this.timeLocal = DateTime.Now;
        }
    }

    private readonly Channel<(DateTime time, string text)> channel;

    public FileLogger()
    {
        channel = Channel.CreateUnbounded<(DateTime, string)>();

    }

    public Task Start(string filename)
    {
        return Task.Run(async () => await WriteToFile(filename).ConfigureAwait(false));
    }

    public async Task WriteToFile(string filename)
    {
        Directory.CreateDirectory($"../{Log.Logs}");

        File.Delete($"../{Log.Logs}/{filename}.log");

        while (true)
        {
            try
            {

                await channel.Reader.WaitToReadAsync().ConfigureAwait(false);

                var lines = new List<string>();
                while (channel.Reader.TryRead(out var line))
                {
                    lines.Add($"{line.time}: {line.text}");
                }

                await File.AppendAllLinesAsync($"../{Log.Logs}/{filename}.log", lines).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                var db = 0;
            }
        }
    }

    public void WriteLine(string v)
    {
        channel.Writer.TryWrite((DateTime.UtcNow, v));
    }
}

public class RunTimesLogger
{
    public RunTimesLogger()
    {
    }

    public Task Start(int port)
    {
        return Task.Run(async () => await WriteToFile($"runtimes-{port}").ConfigureAwait(false));
    }

    public async Task WriteToFile(string filename)
    {
        Directory.CreateDirectory($"../{Log.Logs}");

        File.Delete($"../{Log.Logs}/{filename}.log");

        var lastWroteRunTimes = DateTime.UtcNow;

        while (true)
        {
            try
            {
                File.Delete($"../{Log.Logs}/{filename}.csv");
                await File.AppendAllLinesAsync($"../{Log.Logs}/{filename}.csv", new[] { DebugRunTimes.Header() }).ConfigureAwait(false);

                await Task.Delay(60).ConfigureAwait(false);

                // TODO, what the the heck
                // this should probabaly be it's own process
                if (lastWroteRunTimes + TimeSpan.FromSeconds(60) < DateTime.UtcNow)
                {
                    await File.AppendAllLinesAsync($"../{Log.Logs}/{filename}.csv", DebugRunTimes.ToCsv()).ConfigureAwait(false);
                    lastWroteRunTimes = DateTime.UtcNow;
                }
            }
            catch (Exception e)
            {
                var db = 0;
            }
        }
    }
}
