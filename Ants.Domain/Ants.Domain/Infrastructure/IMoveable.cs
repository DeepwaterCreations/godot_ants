﻿public interface IMoveable: IStateHash
{
    GlobalPosionInt GlobalPositionInt { get; set; }
    GlobalPosionInt VelocityInt { get; }
}