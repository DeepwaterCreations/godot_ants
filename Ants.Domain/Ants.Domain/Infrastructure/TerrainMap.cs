﻿using System.Collections.Concurrent;
using System.Diagnostics;
using System.Runtime.Intrinsics;


public class TerrainMap2 { 

}

public class TerrainMap : IReadOnlyStateHash
{

    private readonly Dictionary<(int x, int y), Vector128<double>>  mud;
    private readonly IReadOnlySet<(int x, int y)> cover;

    // this is readonly so we store the hash
    // we spend a lot of time on hash codes
    public int? ReadOnlyHash { get; set; }

    public TerrainMap(Dictionary<(int x, int y), Vector128<double>> mud, IReadOnlySet<(int x, int y)> cover)
    {
        this.mud = mud ?? throw new ArgumentNullException(nameof(mud));
        this.cover = cover ?? throw new ArgumentNullException(nameof(cover));

    }

    public IEnumerable<(string, object?)> HashData()
    {
        yield return (nameof(mud), mud);
        yield return (nameof(cover), cover);
    }

    //public override int GetHashCode() => ReadOnlyHash;

    //private readonly IReadOnlyDictionary<(int x, int y), double> conver = new HashSet<(int x, int y)>();

    public bool TryModifyFriction((int x, int y) value, double friction, double by, out double actualFriction)
    {
        if (!mud.ContainsKey(value))
        {
            actualFriction = friction;
            return false;
        }

        // {0C8D3F28-3764-4548-B199-BA19133778DA}
        // V = a (friction/(1 - friction))
        // where:
        // - V is terminal velocity
        // - a is acceleration
        // - and friction is friction (to be multiplied by velocity as used here)
        
        var old = friction / (1 - friction); // 2.333 when friction = .7
        var target = old / by;   // 1.166
        // target = actualFriction/(1 - actualFriction)
        // target = 1 / ((1/actualFriction) - 1)
        // target * ((1/actualFriction) - 1) = 1
        // ((1/actualFriction) - 1) = 1/ target
        // (1/actualFriction) = (1/ target) + 1
        // 1 = actualFriction * ((1/ target) + 1)
        // 1 = actualFriction * ((1/ target) + 1)
        // 1 / ((1/ target) + 1) = actualFriction
        actualFriction = 1 / ((1 / target) + 1);
        // let's check
        // (1 / target) = .85719
        // (1 / target) + 1 = 1.85719
        // 1 / ((1 / target) + 1) = .5385
        // .5385/.4615
        // 1.166 ✅
        Debug.Assert(actualFriction >= 0);
        Debug.Assert(actualFriction <= 1);
        return true;
    }

    internal bool Cover((int x, int y) mapPosition)
    {
        return cover.Contains(mapPosition);
    }

    internal bool IsMud((int x, int y) mapPosition, out Vector128<double> mudCenter)
    {
        return mud.TryGetValue(mapPosition, out mudCenter);
    }
}