﻿namespace Ants.Domain.Infrastructure
{
    // we have state and immuatable state
    // this is just mutable state
    // naw they are all combined now

    // struct city?

    public static class EnumerableExtensions
    {

        public static double UncheckedSum(this IEnumerable<double> dubs)
        {
            var res = 0.0;
            unchecked
            {
                foreach (var dub in dubs)
                {
                    res += dub;
                }
            }
            return res;
        }
        public static int UncheckedSum(this IEnumerable<int> ints)
        {
            var res = 0;
            unchecked
            {
                foreach (var @int in ints)
                {
                    res += @int;
                }
            }
            return res;
        }
        public static int UncheckedSum<T>(this IEnumerable<T> items, Func<T, int> transform)
        {
            var res = 0;
            unchecked
            {
                foreach (var item in items)
                {
                    res += transform(item);
                }
            }
            return res;
        }
    }
}