﻿using Ants.Domain.Infrastructure;
using Prototypist.Toolbox.Dictionary;
using Prototypist.Toolbox.Object;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Runtime.Intrinsics;
using System.Runtime.Intrinsics.X86;
using System.Xml.Schema;
using static WhoToShoot;


public class Map2<T>
    where T : PhysicsSubjectStateInner2
{
    public const int gridSize = 100;
    private readonly Dictionary<(int, int), List<T>> backing;

    public Map2(Dictionary<(int, int), List<T>> backing)
    {
        this.backing = backing ?? throw new ArgumentNullException(nameof(backing));
    }

    public static Map2<T> Build(IEnumerable<T> things)
    {
        var backing = new Dictionary<(int, int), List<T>>();
        foreach (var thing in things)
        {
            // each thing is added in a square
            // really should be added in a circle
            // would be faster
            Add(backing, thing);
        }
        return new Map2<T>(backing);
    }

    public IEnumerable<T> Backing() => backing.Values.SelectMany(x => x);

    private static void Add(Dictionary<(int, int), List<T>> backing, T thing)
    {
        for (var x = (int)Math.Floor((thing.position.X() - thing.physicsImmunatbleInner.radius) / gridSize); x <= (int)Math.Ceiling((thing.position.X() + thing.physicsImmunatbleInner.radius) / gridSize); x++)
        {
            for (var y = (int)Math.Floor((thing.position.Y() - thing.physicsImmunatbleInner.radius) / gridSize); y <= (int)Math.Ceiling((thing.position.Y() + thing.physicsImmunatbleInner.radius) / gridSize); y++)
            {
                var list = backing.GetOrAdd((x, y), new List<T> { });
                list.Add(thing);
            }
        }
    }

    public IEnumerable<T> Hit(PhysicsSubjectStateInner2 movable) => Hit(movable.position, movable.physicsImmunatbleInner.radius);

    public IEnumerable<T> Hit(Vector128<double> position, double radius)
    {
        var alreadyHit = new HashSet<T>();
        for (var x = (int)Math.Floor((position.X() - radius) / gridSize); x <= (int)Math.Ceiling((position.X() + radius) / gridSize); x++)
        {
            for (var y = (int)Math.Floor((position.Y() - radius) / gridSize); y <= (int)Math.Ceiling((position.Y() + radius) / gridSize); y++)
            {
                if (backing.TryGetValue((x, y), out var list))
                {
                    foreach (var item in list.ToList())
                    {
                        if (!alreadyHit.Contains(item))
                        {
                            if (item.Overlaps(position, radius))
                            {
                                yield return item;
                                alreadyHit.Add(item);
                            }
                        }
                    }
                }
            }
        }
    }

    public void Remove(T thing)
    {
        for (var x = (int)Math.Floor((thing.position.X() - thing.physicsImmunatbleInner.radius) / gridSize); x <= (int)Math.Ceiling((thing.position.X() + thing.physicsImmunatbleInner.radius) / gridSize); x++)
        {
            for (var y = (int)Math.Floor((thing.position.Y() - thing.physicsImmunatbleInner.radius) / gridSize); y <= (int)Math.Ceiling((thing.position.Y() + thing.physicsImmunatbleInner.radius) / gridSize); y++)
            {
                if (backing.TryGetValue((x, y), out var list))
                {
                    list.Remove(thing);
                }
            }
        }
    }
    public void Add(T thing) {
        Add(this.backing, thing);
    }

    public bool CanReach(Vector128<double> from, Vector128<double> to, out Vector128<double> reached) 
    {
        var at = from;
        {
            var atGridVector = Avx.Divide(from, gridSize.AsVector());
            var atGrid = (x: (int)Math.Floor(atGridVector.X()), y: (int)Math.Floor(atGridVector.Y()));
            if (backing.TryGetValue(atGrid, out var list))
            {
                if (!Pathing.CanReach(from, to, list.Select(x=>(x.position, x.physicsImmunatbleInner.radius)).ToArray(), 0, out reached, out var _)) {
                    return false;
                }
            }
        }

        var direction = Avx.Subtract(to, from);

        if (direction.LengthSquared() == 0) {
            reached = to;
            return true;
        }

        var pressure = 0;

        while (true)
        {
            if (pressure++ > 1_000) {
                throw new Exception("pressuer!");
            }

            var XEdge = ((int)Math.Floor(at.X() / gridSize) + Math.Sign(direction.X())) * gridSize;
            var YEdge = ((int)Math.Floor(at.Y() / gridSize) + Math.Sign(direction.Y())) * gridSize;

            var timeToXEdge = direction.X() == 0 ? double.MaxValue : (XEdge - at.X()) / direction.X();
            var timeToYEdge = direction.Y() == 0 ? double.MaxValue : (YEdge - at.Y()) / direction.Y();

            var timeToEnd = Avx.Subtract(to, at).LengthSquared() / direction.LengthSquared();

            if (timeToEnd <= timeToXEdge && timeToEnd <= timeToYEdge)
            {
                reached = to;
                return true;
            }

            if (timeToXEdge < timeToYEdge)
            {
                at = Vector128.Create(XEdge, at.Y());
            }
            else
            {
                at = Vector128.Create(at.X(), YEdge);
            }

            var atGridVector = Avx.Divide(at, gridSize.AsVector());
            var atGrid = (x: (int)Math.Floor(atGridVector.X()), y: (int)Math.Floor(atGridVector.Y()));
            if (backing.TryGetValue(atGrid, out var list))
            {
                if (!Pathing.CanReach(from, to, list.Select(x => (x.position, x.physicsImmunatbleInner.radius)).ToArray(), 0, out reached, out var _))
                {
                    return false;
                }
            }
        }
    }

    public IEnumerable<double> MoveInSteps<TT>(TT subject, int steps)
        where TT : PhysicsSubjectState2, T
    {
        var moveBy = 1.0 / steps;
        for (int i = 0; i < steps; i++)
        {
            Remove(subject);
            subject.position = Avx.Add(Avx.Multiply(subject.velocity, moveBy.AsVector()), subject.position);
            Add(subject);
            yield return moveBy;
        }
    }

    public IEnumerable<double> MoveByLength<TT>(TT subject, double length)
        where TT : PhysicsSubjectState2, T
    {
        var timeRemaing = 1.0;
        var pressue = 0;
        while (timeRemaing > 0)
        {
            Debug.Assert(pressue++ < 1000);

            // radius.. too small? too big?


            var timeToMoveByLength = subject.velocity.LengthSquared() == 0 ? 1 : (length* length) / subject.velocity.LengthSquared();
            timeToMoveByLength = Math.Min(timeRemaing, timeToMoveByLength);

            Remove(subject);

            subject.position = Avx.Add(Avx.Multiply(subject.velocity, timeToMoveByLength.AsVector()), subject.position);
            Add(subject);

            timeRemaing -= timeToMoveByLength;
            yield return timeToMoveByLength;
        }
    }

    public IEnumerable<double> Move<TT>(TT subject)
        where TT: PhysicsSubjectState2, T
    {
        return MoveByLength(subject, subject.physicsImmunatble.radius); 
    }
}

// this is just radius....
public class ImmunatblePhysicsStateInner : IStateHash
{
    public readonly double radius;
    public ImmunatblePhysicsStateInner(double radius)
    {
        this.radius = radius;
    }
    public virtual IEnumerable<(string, object?)> HashData()
    {
        yield return (nameof(radius), radius);
    }
    //public override int GetHashCode() => this.Hash();

    //public override bool Equals(object? obj) => this.HashEquals(obj);
}

public class ImmunatblePhysicsState : ImmunatblePhysicsStateInner
{
    public readonly double mass = 1;

    public ImmunatblePhysicsState(double radius) : base(radius)
    {
    }

    public override IEnumerable<(string, object?)> HashData()
    {
        yield return (nameof(radius), radius);
        yield return (nameof(mass), mass);
    }
    //public override int GetHashCode() => this.Hash();

    //public override bool Equals(object? obj) => this.HashEquals(obj);
}

public interface IBlocksShots
{
    public double Radius { get; }
    public Vector128<double> Position { get; }
}

public abstract class PhysicsSubjectStateInner2 : IStateHash
{
    public readonly ImmunatblePhysicsStateInner physicsImmunatbleInner;
    public Vector128<double> position;

    public PhysicsSubjectStateInner2(ImmunatblePhysicsStateInner immunatble, Vector128<double> position)
    {
        this.physicsImmunatbleInner = immunatble;
        this.position = position;
    }

    internal bool Overlaps(PhysicsSubjectStateInner2 movable) => Overlaps(movable.position, movable.physicsImmunatbleInner.radius);

    internal bool Overlaps(Vector128<double> otherPosition, double otherRadius) => Avx.Subtract(position, otherPosition).LengthSquared() <= ((physicsImmunatbleInner.radius + otherRadius) * (physicsImmunatbleInner.radius + otherRadius));

    public virtual IEnumerable<(string, object?)> HashData() {
        yield return (nameof(physicsImmunatbleInner), physicsImmunatbleInner);
        yield return (nameof(position), position);
    }

    // why did I have this?
    //public override abstract int GetHashCode();
}

public abstract class PhysicsSubjectState2 : PhysicsSubjectStateInner2, IBlocksShots
{

    public Vector128<double> velocity;
    public ImmunatblePhysicsState physicsImmunatble;

    public double Radius => physicsImmunatble.radius;

    public Vector128<double> Position => position;

    public PhysicsSubjectState2(ImmunatblePhysicsState immunatble, Vector128<double> position, Vector128<double> velocity): base(immunatble, position)
    {
        physicsImmunatble = immunatble;
        this.velocity = velocity;
    }

    public void BounceOffOf(PhysicsSubjectStateInner2 other, double step) {
        var fromOther = Avx.Subtract(other.position, position);
        var overlap = (physicsImmunatble.radius + other.physicsImmunatbleInner.radius) - fromOther.Length();
        if (overlap > 0)
        {
            var useMass = other.SafeIs(out PhysicsSubjectState2 physicsSubjectState2) ? Math.Min(physicsImmunatble.mass, physicsSubjectState2.physicsImmunatble.mass) : physicsImmunatble.mass;
            var force = Avx.Multiply(fromOther.ToLength(-overlap), (useMass * step).AsVector());
            ApplyForce(force);
        }
    }

    public void ApplyForce(Vector128<double> force) {
        velocity = Avx.Add(velocity, Avx.Divide(force, physicsImmunatble.mass.AsVector()));
    }

    public override IEnumerable<(string, object?)> HashData()
    {
        foreach (var item in base.HashData()) {
            yield return item;
        }

        yield return (nameof(physicsImmunatbleInner), physicsImmunatbleInner);
        yield return (nameof(position), position);
    }

}
