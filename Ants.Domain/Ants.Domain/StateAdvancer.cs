﻿using Ants.Domain;
using Ants.Domain.Infrastructure;
using Ants.Domain.State;
using Ants.Domain.State.Orders._12___Arty;
using Ants.Domain.State.Orders._13___Zapper;
using Prototypist.Toolbox.Object;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Runtime.Intrinsics;
using System.Runtime.Intrinsics.X86;

public class StateAdvancer
{
    public static double lastAdvanceSeconds;

    public (
        SimulationState state,
        EffectEvents events,
        EffectEventRequireVerification eventsRequiringVerification,
        IReadOnlyList<AntState> pings)
        Advance(
            SimulationState state,
            int tick,
            List<(Guid id, IPendingOrderState order, bool queue)> orders,
            TargetState2 targetState,
            ImmutableSimulationState immutableSimulationState,
            int side,
            Action<Guid, IPendingOrderState, bool> enqueueOrder,
            ConcurrentDictionary<int, HashSet<Guid>> sideSeesUnitFull,
            ConcurrentDictionary<int, HashSet<Guid>> sideSeesUnitMid,
            IReadOnlyDictionary<int, HashSet<ResourceState.Immutable.Id>> sideSeesResources)
    {

        Stopwatch simulation = new Stopwatch();
        simulation.Start();

        var publicPings = new List<AntState>();

        try
        {

            var eventsRequiringVerification = new EffectEventRequireVerification();
            var events = new EffectEvents();
            var shooterIndex = new ShotIndexTracker();

            // do I need this?
            // I think it is just rendering info
            // like who see what bullet
            //var stuffYouCanSee = state.ants.Select(x => x.ToCanBeSeen()).Union(state.bullets.Select(x => x.ToCanBeSeen())).ToArray();

            // update score and money
            state.money.Update();
            if (false)
            {
#pragma warning disable CS0162 // Unreachable code detected
                state.control.Update(state);
#pragma warning restore CS0162 // Unreachable code detected
            }
            state.populationState.Update();

            state.resources = state.resources.Where(x => !x.Update()).ToList();

            foreach (var resourceSpawner in state.resourceSpawners.OrderBy(x => x.id))
            {
                var res = resourceSpawner.Update(tick);
                if (res != null)
                {
                    state.resources.Add(res);
                }
            }

            // honey pots bleed
            foreach (var honeyPotAnt in state.ants.Values.Where(x=>!x.IsStuned()).Where(x => x.honeyPotState != null).OrderBy(x => x.SortHash()))
            {
                var res = honeyPotAnt.honeyPotState.Update(tick, honeyPotAnt.immutable.id, honeyPotAnt.position);
                if (res != null)
                {
                    state.resources.Add(res);
                }
            }

            // check if pre shockwaves hit rocks
            //var nextPreShockWaves = new List<PreShockwaveState>();
            var nextShockWaves = new List<ShockwaveState>();
            //foreach (var preShockwave in state.preShockwaves /*do I need to order these?*/)
            //{
            //    if (!preShockwave.UpdateTrueWhenStillExists(tick, immutableSimulationState.rockMap, immutableSimulationState.rocksTuple, out var shockwave))
            //    {
            //        if (shockwave != null)
            //        {
            //            nextShockWaves.Add(shockwave);
            //        }
            //    }
            //    else
            //    {
            //        nextPreShockWaves.Add(preShockwave);
            //    }
            //}
            foreach (var shockwave in state.shockwaves)
            {
                // do they die if they hit rocks?
                // do the bounce?
                // die for now
                if (shockwave.UpdateTrueWHenStillExists(tick, immutableSimulationState.rockMap))
                {
                    nextShockWaves.Add(shockwave);
                }
            }
            //state.preShockwaves = nextPreShockWaves;
            state.shockwaves = nextShockWaves;

            foreach (var ant in state.ants.Values)
            {
                // this shouldn't depend on anything exteral
                // it's fo update verious clocks and stuff
                // if it depends on external things, the order might matter
                ant.Update(tick, state);
            }

            foreach (var bullet in state.bullets.Values.ToArray())
            {
                // this shouldn't depend on anything exteral
                // it's fo update verious clocks and stuff
                // if it depends on external things, the order might matter
                if (bullet.Update())
                {
                    events.ShotsFizzled.Add(new ShotFizzled(bullet.immutable.id, bullet.position));
                    state.bullets.TryRemove(bullet.immutable.id, out var _);
                }
            }

            // order doesn't matter at the moment
            // might at somepoint
            foreach (var arcBullet in state.arcBulletStates.Values.ToArray())
            {
                if (arcBullet.Update())
                {
                    // TODO
                    //events.ShotsFizzled.Add(new ShotFizzled(bullet.immutable.id, bullet.position));
                    state.arcBulletStates.TryRemove(arcBullet.immutable.id, out var _);
                }
            }

            // this bullet updates the state
            // it creates sub bullets
            // sneaky mutation
            foreach (var artyBullet in state.artyBulletStates.Values.OrderBy(x => x.immutable.id).ToArray())
            {
                if (artyBullet.Update(tick, state, shooterIndex))
                {
                    state.artyBulletStates.TryRemove(artyBullet.immutable.id, out var _);
                }
            }

            foreach (var healBullet in state.healBulletStates.Values.ToArray())
            {
                // this shouldn't depend on anything exteral
                // it's fo update verious clocks and stuff
                // if it depends on external things, the order might matter
                if (healBullet.Update())
                {
                    // maybe later...
                    //events.ShotsFizzled.Add(new ShotFizzled(bullet.immutable.id, bullet.position));
                    state.healBulletStates.TryRemove(healBullet.immutable.id, out var _);
                }
            }

            //var nextSmokeBullets = new List<SmokeBulletState>();
            //foreach (var smokeBullet in state.smokeBullets)
            //{
            //    if (smokeBullet.endFrame == tick)
            //    {
            //        state.smokes.Add(new SmokeState(smokeBullet.endAt, smokeBullet.smokeId));
            //    }
            //    else {
            //        nextSmokeBullets.Add(smokeBullet);
            //    }
            //}

            var nextGrenadeStates = new List<GrenadeState>();
            foreach (var grenade in state.grenadeStates.OrderBy(x => x.id))
            {
                if (grenade.Update(shooterIndex, tick, out var bullets))
                {
                    foreach (var bullet in bullets)
                    {
                        var added = state.bullets.TryAdd(bullet.immutable.id, bullet);
                        Debug.Assert(added);
                    }
                }
                else
                {
                    nextGrenadeStates.Add(grenade);
                }
            }
            state.grenadeStates = nextGrenadeStates;

            foreach (var (id, pendingOrder, queue) in orders)
            {
                if (state.ants.TryGetValue(id, out var ant))
                {
                    if (queue)
                    {
                        ant.pendingOrders.Enqueue(pendingOrder);
                    }
                    else
                    {
                        ant.activeOrder?.Cancel(ant);
                        ant.pendingOrders = new Queue<IPendingOrderState>();
                        //ant.activeOrder = pendingOrder.Order();
                        if (pendingOrder.TryOrder(state, ant, out var orderState))
                        {
                            ant.activeOrder = orderState;
                        }
                        else
                        {
                            Log.WriteLine("failed order");
                            ant.activeOrder = null;
                        }
                    }
                }
                else
                {
                    Log.WriteLine("order for missing unit");
                }
            }

            // update next order
            foreach (var ant in state.ants.Values.Where(x => !x.IsStuned()))
            {
                if (ant.activeOrder == null)
                {
                    if (ant.pendingOrders.TryDequeue(out var next))
                    {
                        //ant.activeOrder = next.Order();
                        if (next.TryOrder(state, ant, out var order))
                        {
                            ant.activeOrder = order;
                        }
                    }
                }
            }

            var antsMap = Map2<AntState>.Build(state.ants.Values.OrderBy(x => x.SortHash())); // needs to be ordered to be deterministic
            var zapperSetUpMap = Map2<ZapperSetupState>.Build(state.zapperSetupStates.OrderBy(x => x.SortHash())); // needs to be ordered to be deterministic
            var bulletsMap = Map2<BulletState>.Build(state.bullets.Values.OrderBy(x => x.immutable));// needs to be ordered to be deterministic
            var healBullestMap = Map2<HealBulletState>.Build(state.healBulletStates.Values.OrderBy(x => x.immutable));// needs to be ordered to be deterministic
            var artyBullestMap = Map2<ArtyBulletState>.Build(state.artyBulletStates.Values.OrderBy(x => x.immutable));// needs to be ordered to be deterministic
            var arcBulletsMap = Map2<ArcBulletState>.Build(state.arcBulletStates.Values.OrderBy(x => x.immutable));// needs to be ordered to be deterministic

            // place eggs (before shooting so you can shoot at stuff that just spawned in)
            foreach (var ant in state.ants.Values.Where(x => !x.IsStuned()).OrderBy(x => x.SortHash()).ToArray()/*we need a reliable order*/)
            {
                if (ant.activeOrder.SafeIs(out MakeUnitOrderImmutableState make))
                {
                    // TODO this is pretty werid
                    // I pass in the maps and I pass in a list of places you can't place
                    // dup {1627C264-4D47-4814-B4A2-F11753011D52}
                    var res = make.ActOrRefund(state, ant, antsMap, immutableSimulationState, MaxMoney(state.ants.Values, ant.immutable.side), side, enqueueOrder);
                    if (res != null)
                    {
                        if (state.ants.TryAdd(res.immutable.id, res))
                        {
                            eventsRequiringVerification.antsCreated.Add(new AntCreatedEvent(res.immutable.side, res.position, res.immutable.type, res.immutable.id));
                        }
                        else
                        {
                            Log.WriteLine("ant could not be added, that was unexpected");
                        }
                    }
                    ant.activeOrder = null;
                }
                if (ant.activeOrder.SafeIs(out TrainMakeUnitOrderImmutableState trainMake))
                {
                    // dup {1627C264-4D47-4814-B4A2-F11753011D52}
                    var res = trainMake.ActOrRefund(state, ant, antsMap, MaxMoney(state.ants.Values, ant.immutable.side));
                    if (res != null)
                    {
                        if (state.ants.TryAdd(res.immutable.id, res))
                        {
                            eventsRequiringVerification.antsCreated.Add(new AntCreatedEvent(res.immutable.side, res.position, res.immutable.type, res.immutable.id));
                        }
                        else
                        {
                            Log.WriteLine("ant could not be added, that was unexpected");
                        }
                    }
                    ant.activeOrder = null;
                }
            }

            var nextZapperSetupStates = new List<ZapperSetupState>();
            foreach (var zapperSetupState in state.zapperSetupStates)
            {
                if (!state.ants.TryGetValue(zapperSetupState.immutable.createdBy, out var ant))
                {
                    continue;
                }

                if (ant.legs != null && ant.legs.IsMoving()) {
                    continue;
                } 

                Debug.Assert(ant.zapperState != null);
                if (tick - zapperSetupState.immutable.createdOnTick == ant.zapperState.immutable.setUpTime)
                {
                    var moveTo = Avx.Add(ant.position, Avx.Subtract(zapperSetupState.position, ant.position).ToLength(ant.physicsImmunatble.radius + AntFactory.AntSize(AntType.WebLine)));

                    // and we create the thing that shoot togards the zapper
                    var make = new MakeUnitOrderImmutableState(zapperSetupState.immutable.webLineId, AntType.WebLine, moveTo, zapperSetupState.position);

                    // dup {1627C264-4D47-4814-B4A2-F11753011D52}
                    var res = make.ActOrRefund(state, ant, antsMap, immutableSimulationState, MaxMoney(state.ants.Values, ant.immutable.side), side, enqueueOrder);
                    if (res != null)
                    {
                        if (state.ants.TryAdd(res.immutable.id, res))
                        {
                            eventsRequiringVerification.antsCreated.Add(new AntCreatedEvent(res.immutable.side, res.position, res.immutable.type, res.immutable.id));
                        }
                        else
                        {
                            Log.WriteLine("ant could not be added, that was unexpected");
                        }
                    }

                    Debug.Assert(res.zapperGun != null);
                    res.zapperGun.target = zapperSetupState.position;
                    res.zapperGun.createdBy = zapperSetupState.immutable.createdBy;
                }
                else 
                {
                    nextZapperSetupStates.Add(zapperSetupState);
                }
            }

            // set up zappers
            foreach (var zapper in state.ants.Values.Where(x => !x.IsStuned()).OrderBy(x => x.SortHash())/*order probably doesn't matter here, but just in case*/)
            {
                if (zapper.activeOrder.SafeIs(out ZapperOrder zapperOrder))
                {
                    Debug.Assert(zapper.zapperState != null);
                    var at = zapperOrder.mousePosition;
                    var sub = Avx.Subtract(at, zapper.Position);
                    if (sub.LengthSquared() > zapper.zapperState.immutable.range * zapper.zapperState.immutable.range) { 
                        at = Avx.Add(sub.ToLength(zapper.zapperState.immutable.range), zapper.Position);
                    }
                    nextZapperSetupStates.Add(new ZapperSetupState(new ZapperSetupState.Immutable(zapper.immutable.id, tick, zapperOrder.webLineId), ZapperSetupState.MaxHp, at));
                    zapper.activeOrder = null;
                }
            }

            state.zapperSetupStates = nextZapperSetupStates;

            // kill abandoned zappers
            foreach (var zapper in state.ants.Values.Where(x => !x.IsStuned()).OrderBy(x => x.SortHash())/*order probably doesn't matter here, but just in case*/) {
                if (zapper.zapperGun != null) {
                    if (zapper.zapperGun.presists) {
                        continue;
                    }

                    if (!state.ants.TryGetValue(zapper.zapperGun.createdBy.Value, out var createdBy))
                    {
                        Log.WriteLine("zapper's creator is dead");
                        state.ants.Remove(zapper.immutable.id, out var _);
                    }
                    else if (createdBy.legs != null && createdBy.legs.IsMoving()) {
                        Log.WriteLine("zapper's creator is moveing");
                        state.ants.Remove(zapper.immutable.id, out var _);
                    }
                }
            }


            foreach (var ant in state.ants.Values.OrderBy(x => x.SortHash()).ToArray()/*we need a reliable order*/)
            {
                if (ant.heal != null)
                {
                    ant.heal.Update(state, ant);
                }
            }

            // you might expect .Where(x => !x.IsStuned()) but it's actually inside gather
            // there's a collect from that we want to set to pull for stunned ants
            // but maybe I should do that hear to be consistant?
            foreach (var ant in state.ants.Values.OrderBy(x => x.SortHash()).ToArray()/*we need a reliable order*/)
            {
                if (ant.resouceGatherer != null)
                {
                    var weSee = sideSeesResources.TryGetValue(ant.immutable.side, out var x) ? x : new HashSet<ResourceState.Immutable.Id>();
                    var gatherEvent = ant.resouceGatherer.Gether(ant, state.resources.Where(x => weSee.Contains(x.immutable.id)).ToList(), immutableSimulationState.rocksTuple, state.money, MaxMoney(state.ants.Values, ant.immutable.side));

                    if (gatherEvent != null)
                    {
                        events.resourceGathereds.Add(gatherEvent);
                    }
                }
            }

            // hatch eggs
            foreach (var item in state.ants.Values.Where(x => x.unpacker != null).OrderBy(x => x.SortHash()).ToArray())
            {
                var unpacksTo = item.unpacker!.UnpackedToOrSelf(item);
                if (unpacksTo != item)
                {
                    antsMap.Remove(item);
                    antsMap.Add(unpacksTo);
            
                    if (!state.ants.TryRemove(item.immutable.id, out var _))
                    {
                        Log.WriteLine("failed to remove");
                    }
                    state.ants[unpacksTo.immutable.id] = unpacksTo;
                    eventsRequiringVerification.antsCreated.Add(new AntCreatedEvent(unpacksTo.immutable.side, unpacksTo.position, unpacksTo.immutable.type, unpacksTo.immutable.id));
                }
            }



            // manual shoot
            foreach (var shooter in state.ants.Values.Where(x => !x.IsStuned()).OrderBy(x => x.SortHash())/*we need a reliable order*/)
            {
                if (shooter.activeOrder.SafeIs(out ManualShootOrder shootOrder))
                {
                    if (shooter.gun!.reloadDue == 0)
                    {
                        Shoot(state, tick, shooter, shootOrder.shootAt, events, shooterIndex, 0);
                    }
                    shooter.activeOrder = null;
                }
            }

            // throw grenades
            foreach (var shooter in state.ants.Values.Where(x => !x.IsStuned()).OrderBy(x => x.SortHash())/*we need a reliable order*/)
            {
                if (shooter.activeOrder.SafeIs(out GrenadeOrder nadeOrder))
                {
                    if (shooter.grenadeLauncher!.reloadDue == 0)
                    {
                        var distance = Avx.Subtract(nadeOrder.shootAt, shooter.position);

                        if (distance.LengthSquared() > (shooter.grenadeLauncher.immuntable.maxRange * shooter.grenadeLauncher.immuntable.maxRange))
                        {
                            distance = distance.ToLength(shooter.grenadeLauncher.immuntable.maxRange);
                        }

                        state.grenadeStates.Add(new GrenadeState(
                                // should I really be using BulletState here not GrenadeBulletState?
                                new BulletState.Immutable.Id(
                                    shooter.immutable.id,
                                    tick,
                                    shooterIndex.Get(shooter.immutable.id)),
                                (int)Math.Ceiling(distance.Length() / (shooter.grenadeLauncher.immuntable.speed)),
                                shooter.position,
                                distance.SafeNormalizedCopy().ToLength(shooter.grenadeLauncher.immuntable.speed),
                                shooter.grenadeLauncher.immuntable.inner_runForDistance,
                                shooter.grenadeLauncher.immuntable.inner_speed,
                                shooter.immutable.side,
                                shooter.grenadeLauncher.immuntable.inner_damage,
                                shooter.grenadeLauncher.immuntable.inner_ignoreCovoerFor,
                                shooter.grenadeLauncher.immuntable.inner_pierce,
                                shooter.grenadeLauncher.immuntable.inner_stun,
                                shooter.grenadeLauncher.immuntable.inner_knockBack
                                ));
                        shooter.grenadeLauncher.reloadDue = shooter.grenadeLauncher.immuntable.reloadTime;
                    }
                    shooter.activeOrder = null;
                }
            }

            // smoke
            foreach (var shooter in state.ants.Values.Where(x => !x.IsStuned()).OrderBy(x => x.SortHash())/*order probably doesn't matter here, but just in case*/)
            {
                if (shooter.activeOrder.SafeIs(out SmokeOrder smokeOrder))
                {
                    var distance = Avx.Subtract(smokeOrder.mousePosition, shooter.position);

                    if (distance.LengthSquared() > (SmokePlacerState.range * SmokePlacerState.range))
                    {
                        distance = distance.ToLength(SmokePlacerState.range);
                    }
                    var position = Avx.Add(shooter.position, distance);

                    if (Pathing.IsAPathBetween(shooter.position, position, immutableSimulationState.rocksTuple, 0))
                    {
                        // remove the old smoke
#pragma warning disable CS8602 // Dereference of a possibly null reference. - I want this to throw
                        if (shooter.smokePlacerState.mySmokeId != null)
                        {
#pragma warning restore CS8602 // Dereference of a possibly null reference.
                            state.smokes.Remove(shooter.smokePlacerState.mySmokeId.Value);
                        }

                        shooter.smokePlacerState.mySmokeId = smokeOrder.smokeId;

                        var added = state.smokes.TryAdd(smokeOrder.smokeId, new SmokeState(position, smokeOrder.smokeId));
                        Debug.Assert(added);
                    }
                    else
                    {
                        Log.WriteLine("failed to place smoke, target position isn't visible");
                    }
                    shooter.activeOrder = null;

                }
            }

            foreach (var ant in state.ants)
            {
                if (ant.Value.immutable.type == AntType.MobileCover && tick % 20 == 0) {
                    Log.WriteLine($"target at: {ant.Value.position}");
                }
            }

            // create shockwaves
            foreach (var shooter in state.ants.Values.Where(x => !x.IsStuned()).OrderBy(x => x.SortHash())/*we need a reliable order*/)
            {
                if (shooter.activeOrder.SafeIs(out ShockwaveOrder shockwaveOrder))
                {
                    var distance = Avx.Subtract(shockwaveOrder.mousePosition, shooter.position);
                    for (int i = -3; i <= 3; i++)
                    {
                        if (!Pathing.CanReach(shooter.position, Avx.Add(shooter.position,
                            Avx.Multiply(distance.Rotated(i / 20.0).ToLength(ShockwaveState.Speed), PreShockwaveState.timeToLive.AsVector())),
                            immutableSimulationState.rocksTuple, 0, out var reached, out var lastHit)) { 
                        
                            var timeSpent = Avx.Subtract(reached, shooter.position).Length()/ ShockwaveState.Speed;

                            state.shockwaves.Add(new ShockwaveState(
                                tick,
                                reached,
                                Avx.Subtract(reached, lastHit.Value).ToLength(ShockwaveState.Speed),
                                (int)Math.Round(PreShockwaveState.timeToLive - timeSpent),
                                new ShockwaveState.Id(shooter.immutable.id, tick, i)));
                        }
                    }
                    shooter.activeOrder = null;

                }
            }

            // fire arty
            // old
            //foreach (var shooter in state.ants.Values.Where(x => !x.IsStuned()).OrderBy(x => x.SortHash())/*we need a reliable order*/)
            //{
            //    if (shooter.activeOrder.SafeIs(out ArtyOrder artyOrder))
            //    {
            //        Debug.Assert(shooter.artyState != null);

            //        var immutable = new ArtyBulletState.Immutable(
            //            shooter.immutable.id,
            //            tick,
            //            shooterIndex.Get(shooter.immutable.id),
            //            shooter.artyState.immutable.runForTicks,
            //            shooter.artyState.immutable.speed,
            //            Avx.Subtract(artyOrder.mousePosition, shooter.position).NormalizedCopy(),
            //            shooter.immutable.side,
            //            shooter.artyState.immutable.damage,
            //            shooter.artyState.immutable.ignoreCoverFor,
            //            shooter.artyState.immutable.pierce,
            //            shooter.artyState.immutable.stun,
            //            true,
            //            shooter.artyState.immutable.subShot_runForDistance,
            //            shooter.artyState.immutable.subShot_speed,
            //            shooter.artyState.immutable.subShot_knockBack);

            //        state.artyBulletStates.TryAdd(immutable.id, ArtyBulletState.MakeBullet(shooter.position, immutable));

            //        publicPings.Add(shooter);

            //        shooter.activeOrder = null;
            //    }
            //}

            foreach (var shooter in state.ants.Values.Where(x => !x.IsStuned()).OrderBy(x => x.SortHash())/*we need a reliable order*/)
            {
                if (shooter.activeOrder.SafeIs(out ArtyOrder artyOrder))
                {
                    Debug.Assert(shooter.artyState != null);

                    shooter.artyState.target = artyOrder.mousePosition;
                }
            }

            // fire arty
            foreach (var shooter in state.ants.Values.Where(x => !x.IsStuned()).OrderBy(x => x.SortHash())/*we need a reliable order*/)
            {
                if (shooter.artyState != null && shooter.artyState.target != null && shooter.artyState.reloadDue == 0)
                {
                    var immutable = new ArtyBulletState.Immutable(
                        shooter.immutable.id,
                        tick,
                        shooterIndex.Get(shooter.immutable.id),
                        shooter.artyState.immutable.runForTicks,
                        shooter.artyState.immutable.speed,
                        Avx.Subtract(shooter.artyState.target.Value, shooter.position).NormalizedCopy(),
                        shooter.immutable.side,
                        shooter.artyState.immutable.damage,
                        shooter.artyState.immutable.ignoreCoverFor,
                        shooter.artyState.immutable.pierce,
                        shooter.artyState.immutable.stun,
                        true,
                        shooter.artyState.immutable.subShot_runForDistance,
                        shooter.artyState.immutable.subShot_speed,
                        shooter.artyState.immutable.subShot_knockBack);

                    state.artyBulletStates.TryAdd(immutable.id, ArtyBulletState.MakeBullet(shooter.position, immutable));

                    publicPings.Add(shooter);

                    shooter.artyState.reloadDue = ArtyState.reloadTime;
                }
            }

            var shooters = state.ants.Values.Where(ant =>
                ant.gun != null &&
                ant.gun.immutable.autoShoot &&
                ant.gun.reloadDue == 0 &&
                !ant.IsStuned() &&
                (ant.gun.immutable.canMoveAndShoot || ant.legs == null || !ant.legs.IsMoving())).ToArray();


            // shoot (this goes before movement so you're shooting with old velocity info)
            // why would I want that?
            //
            // {89F9BCD3-7FDD-4AAB-A2A1-1543F31D7916}
            foreach (var shooter in shooters.OrderBy(x => x.SortHash()))
            {
                if (targetState.targeting.TryGetValue(shooter.immutable.id, out var shootAt))
                {
                    var timeAlreadyPassed = tick - targetState.at;
                    if (GunState.TryTarget(shooter.position, Avx.Add( shootAt.position, Avx.Multiply( shootAt.velocity, Vector128.Create((double)timeAlreadyPassed, (double)timeAlreadyPassed))), shootAt.velocity, shooter.gun.immutable.shotSpeed, out var innerProjectedPosition, out var _)) 
                    {
                        Shoot(state, tick, shooter, innerProjectedPosition, events, shooterIndex, shootAt.addScatter ? .5 : -.5);
                    }
                }
            }


            var arcShooters = state.ants.Values.Where(ant =>
                ant.arcGun != null &&
                ant.arcGun.immutable.autoShoot &&
                ant.arcGun.reloadDue == 0 &&
                !ant.IsStuned() &&
                (ant.arcGun.immutable.canMoveAndShoot || ant.legs == null || !ant.legs.IsMoving())).ToArray();

            foreach (var arcShooter in arcShooters.OrderBy(x => x.SortHash()))
            {
                if (targetState.arcTargeting.TryGetValue(arcShooter.immutable.id, out var shootAt))
                {
                    var timeAlreadyPassed = tick - targetState.at;


                    // would be good to use the direction from targetState as a guess
                    // maybe we could spend less time in newton's method

                    Log.WriteLine($"shootAt.position: {shootAt.position}");
                    if (GunState.TryTargetArc(arcShooter.position, Avx.Add(shootAt.position, Avx.Multiply(shootAt.velocity, Vector128.Create((double)timeAlreadyPassed, (double)timeAlreadyPassed))), shootAt.velocity, arcShooter.arcGun.immutable.radius, arcShooter.arcGun.immutable.shotSpeed, shootAt.time, out var tryArcShoot))
                    {
                        var (at, time) = tryArcShoot.Value;

                        Log.WriteLine($"arc shoot at: {at}, addScatter: {shootAt.addScatter}");
                        var direction = WhoToShoot.DirectionNotNormalized(arcShooter.arcGun.immutable.radius, arcShooter.position, at, arcShooter.arcGun.immutable.shotSpeed, time).NormalizedCopy();
                        ArcShoot(state, tick, arcShooter, direction, events, shooterIndex, shootAt.addScatter ? .5 : -.5);
                    }
                }
                //if (targetState.arcTargeting.TryGetValue(arcShooter.immutable.id, out var direction))
                //{
                //    ArcShoot(state, tick, arcShooter, direction.direction, events, shooterIndex);
                //}
            }

            // zapper shoot
            // I feel like this does a sort of weird targeting end-run
            // it doesn't use targeting
            // but it also shoot everything
            // most of target is figuring out who the best target is
            foreach (var ant in state.ants.Values.Where(x => !x.IsStuned()).OrderBy(x => x.SortHash()).ToArray()/*we need a reliable order*/)
            {
                if (ant.zapperGun != null && ant.zapperGun.target != null && ant.zapperGun.reloadDue == 0)
                {
                    sideSeesUnitMid.TryGetValue(ant.immutable.side, out var seesMid);
                    seesMid = seesMid ?? new HashSet<Guid>();
                    sideSeesUnitFull.TryGetValue(ant.immutable.side, out var seesFull);
                    seesFull = seesFull ?? new HashSet<Guid>();

                    var sees = seesMid.Union(seesFull).ToHashSet();

                    var hit = antsMap.Hit(ant.zapperGun.target.Value, AntFactory.AmbushPreditorRadius);

                    foreach (var target in hit.Where(x=> sees.Contains(x.immutable.id) && ant.immutable.side != x.immutable.side))
                    {
                        if (GunState.TryTarget(ant.position, target.position, target.velocity, ant.zapperGun.immutable.shotSpeed, out var projectedPosition, out var _)) 
                        {
                            var bullet = ZapperGunState.Shoot(ant, projectedPosition, tick, shooterIndex, 0);

                            state.bullets.TryAdd(bullet.immutable.id, bullet);
                            events.shotsFired.Add(new ShotFired(ant.immutable.side, ant.position, bullet.immutable.id));

                            ant.zapperGun.presists = true;
                        }
                    }
                }
            }

            var healers = state.ants.Values.Where(shooter =>
                shooter.healGun != null &&
                shooter.healGun.reloadDue == 0 &&
                !shooter.IsStuned() &&
                (shooter.healGun.immutable.canMoveAndShoot || shooter.legs == null || !shooter.legs.IsMoving())).ToArray();


            // shoot (this goes before movement so you're shooting with old velocity info)
            // why would I want that?
            //
            // TOOD - seems like this should call try target
            // I think units are missing more than they should
            // probably this is why
            // {89F9BCD3-7FDD-4AAB-A2A1-1543F31D7916}
            foreach (var healer in healers.OrderBy(x => x.SortHash()))
            {
                if (targetState.targeting.TryGetValue(healer.immutable.id, out var shootAt))
                {
                    var timeAlreadyPassed = tick - targetState.at;
                    if (GunState.TryTarget(healer.position, Avx.Add(shootAt.position, Avx.Multiply(shootAt.velocity, Vector128.Create((double)timeAlreadyPassed, (double)timeAlreadyPassed))), shootAt.velocity, healer.healGun.immutable.shotSpeed, out var innerProjectedPosition, out var _))
                    {
                        ShootHeals(state, tick, healer, innerProjectedPosition, events, shooterIndex, shootAt.addScatter ? .5 : -.5);
                    }
                }
            }

            foreach (var ant in state.ants.Values)
            {
                ant.ApplyFiction(immutableSimulationState.mudMap);
            }

            // update velocity
            var notMoving = new List<AntState>();
            var doneMoving = new List<AntState>();
            foreach (var ant in state.ants.Values.Where(x => !x.IsStuned()))
            {
                var avoid = immutableSimulationState.rocksTuple;
                if (ant.immutable.mudMode == Pathing.Mode.ImpassableMud)
                {
                    avoid = avoid.Union(immutableSimulationState.mudTuple).ToArray();
                }

                if (ant.activeOrder.SafeIs(out MoveOrderState move))
                {
                    if (move.Act(ant, immutableSimulationState.mudMap, avoid, antsMap))
                    {
                        doneMoving.Add(ant);
                        notMoving.Add(ant);
                    }
                }
                else if (ant.activeOrder.SafeIs(out MoveWithShiledUpOrderState move2))
                {
                    if (move2.Act(ant, immutableSimulationState.mudMap, avoid, antsMap))
                    {
                        doneMoving.Add(ant);
                        notMoving.Add(ant);
                    }
                }
                else if (ant.activeOrder.SafeIs(out SprintOrder move3))
                {
                    if (move3.Act(ant, immutableSimulationState.mudMap, avoid, antsMap))
                    {
                        doneMoving.Add(ant);
                        notMoving.Add(ant);
                    }
                }
                else
                {
                    notMoving.Add(ant);
                }
            }

            // we don't clear the active orders right away
            // like where it says "doneMoving.Add(ant)"
            // because if two are hitting each other
            // we want them both to clear
            // if we cleared them as they came up in the loop
            // then the frist one with clear
            // and the second one would push it back
            foreach (var ant in doneMoving)
            {
                ant.activeOrder = null;
            }

            // update everyone's velocity before with do the not moving
            // this is a little werid
            // it updates ant's velocities
            // but it also dependings on ant velocities
            // so we need to order it
            foreach (var ant in notMoving.OrderBy(x => x.SortHash()))
            {
                var list = new List<Vector128<double>>();

                if (!HasActiveTrap(ant, state.ants, state.zapperSetupStates))
                {
                    foreach (var antHit in antsMap.Hit(ant.position, ant.Radius + 30))
                    {
                        if (antHit.immutable.side == ant.immutable.side)
                        {
                            var fromOther = Avx.Subtract(ant.position, antHit.position);

                            if (antHit.velocity.Dot(fromOther) > 0)
                            {
                                var fromAlongVelocity = fromOther.ProjectCopy(antHit.velocity);

                                var outOfWay = fromAlongVelocity.LengthSquared() == 0 ? fromOther.Rotated(Math.Tau / 4.0).NormalizedCopy()
                                                                                      : Avx.Subtract(fromOther, fromAlongVelocity).NormalizedCopy();

                                var away = fromOther.NormalizedCopy();

                                list.Add(Avx.Add(outOfWay, away).NormalizedCopy());
                            }
                        }
                    }
                }

                if (list.Count > 0)
                {
                    var sum = Vector128<double>.Zero;
                    foreach (var item in list)
                    {
                        sum = Avx.Add(sum, item);
                    }
                    sum = Avx.Divide(sum, list.Count.AsVector());

                    // don't get out of the say into mud
                    if (ant.immutable.mudMode != Pathing.Mode.IgnoreMud && immutableSimulationState.mudMap.Hit(Avx.Add(ant.position, sum), ant.physicsImmunatble.radius).Any())
                    {
                        ant.RequestVelocity(Vector128.Create(0.0, 0.0), immutableSimulationState.mudMap);
                    }
                    else {
                        ant.RequestFullSpeed(sum);
                    }
                }
                else
                {
                    // stop moving
                    // TODO move to where they were last ordered to be?
                    // and then knock back could be always, not just when a unit is moving
                    ant.RequestVelocity(Vector128.Create(0.0, 0.0), immutableSimulationState.mudMap);
                }
            }

            // shockwaves push ants
            // possible other stuff?
            // bullets?
            foreach (var shockwave in state.shockwaves)
            {
                foreach (var hit in antsMap.Hit(shockwave.CurrentPosition(tick), 0))
                {
                    var diff = Avx2.Subtract(shockwave.velocity, hit.velocity.ProjectCopy(shockwave.velocity));
                    if (diff.Dot(shockwave.velocity) >= 0)
                    {
                        var part = 1.0;

                        hit.ApplyForce(Avx2.Multiply(diff, (hit.physicsImmunatble.mass * part).AsVector()));
                        hit.Stun(3);
                    }
                }
            }

            // I think I could do map stuff in parallel
            // I break up the map like
            //
            // 121212121212
            // 343434343434
            // 121212121212
            // 343434343434
            // 
            // do all the 1 in parallel
            // and then all the 2s
            // as long as nothing is moving so fast as to make it from a 1 to a different 1


            Stopwatch stopwatchMove = new Stopwatch();
            stopwatchMove.Start();
            foreach (var bullet in state.bullets.Values.OrderBy(x => x.immutable))
            {
                foreach (var _ in bulletsMap.MoveByLength(bullet, 10))
                {
                    // did they hit an ant?
                    foreach (var antHit in antsMap.Hit(bullet))
                    {
                        var (antKilled, bulletKilled, damage) = MapExtensions.AntBulletCollide(antHit, bullet, tick, state);

                        // TODO move into that method?
                        // {FBD6644C-0130-4B81-A90E-9846F752F60D} dup
                        if (antHit.immutable.side == bullet.immutable.side)
                        {
                            events.shotsHitFriendly.Add(new ShotHitFriendly(bullet.immutable.id, bullet.position));
                        }
                        else
                        {
                            events.shotsHit.Add(new ShotHit(bullet.immutable.id, antHit, bullet.position, damage));
                        }

                        if (antKilled)
                        {
                            KillAnt(state, eventsRequiringVerification, antsMap, antHit.immutable.id, tick);
                        }

                        if (bulletKilled && state.bullets.TryRemove(bullet.immutable.id, out var _))
                        {
                            bulletsMap.Remove(bullet);
                            state.bullets.TryRemove(bullet.immutable.id, out var _);
                            goto noMoreBullet;
                        }
                    }

                    // did they hit a zapper that is setting up?
                    foreach (var setupHit in zapperSetUpMap.Hit(bullet))
                    {
                        if (state.ants.TryGetValue(setupHit.immutable.createdBy, out var owner)) 
                        {
                            if (owner.immutable.side != bullet.immutable.side || bullet.immutable.damagesTeam) 
                            {
                                setupHit.hp -= bullet.immutable.damage;

                                zapperSetUpMap.Remove(setupHit);
                                state.zapperSetupStates.Remove(setupHit);

                                bulletsMap.Remove(bullet);
                                state.bullets.TryRemove(bullet.immutable.id, out var _);
                                goto noMoreBullet;
                            }
                        }
                    }

                    // did they hit cover?
                    foreach (var coverHit in immutableSimulationState.coverMap.Hit(bullet).Where(x=>!bullet.coverHit.Contains(x.position)))
                    {
                        if (Dies(Avx.Subtract(bullet.position, bullet.immutable.startedAt).Length(), Guid.Parse("{533834E3-925E-43DE-A213-13EEC1DCCD60}"), tick, coverHit.position, bullet.immutable.id))
                        {
                            // we are dead
                            events.shotsHitCover.Add(new ShotHitCover(bullet.immutable.id, bullet.position));
                            bulletsMap.Remove(bullet);
                            state.bullets.TryRemove(bullet.immutable.id, out var _);
                            goto noMoreBullet;
                        }
                        bullet.coverHit.Add(coverHit.position);
                    }
                    // did they hit a rock?
                    foreach (var rockHit in immutableSimulationState.rockMap.Hit(bullet))
                    {
                        events.shotsHitRock.Add(new ShotHitRock(bullet.immutable.id, bullet.position));
                        bulletsMap.Remove(bullet);
                        state.bullets.TryRemove(bullet.immutable.id, out var _);
                        goto noMoreBullet;
                    }
                }
            noMoreBullet:;
            }

            foreach (var healBullet in state.healBulletStates.Values.OrderBy(x => x.immutable))
            {
                foreach (var _ in healBullestMap.MoveByLength(healBullet, 10))
                {
                    // did they hit an ant?
                    foreach (var antHit in antsMap.Hit(healBullet))
                    {
                        if (healBullet.immutable.id.shooterId == antHit.immutable.id)
                        {
                            continue;
                        }

                        antHit.Heal(healBullet);

                        healBullestMap.Remove(healBullet);
                        state.healBulletStates.TryRemove(healBullet.immutable.id, out var _);
                        goto noMoreHealBullet;

                    }

                    // did they hit cover?
                    foreach (var coverHit in immutableSimulationState.coverMap.Hit(healBullet).Where(x => !healBullet.coverHit.Contains(x.position)))
                    {
                        if (Dies(Avx.Subtract(healBullet.position, healBullet.immutable.startedAt).Length(), Guid.Parse("{7FF4FB3D-9E99-4EF4-BBEB-3C2F187BC52F}"), tick, coverHit.position, healBullet.immutable.id))
                        {
                            // we are dead

                            // TODO maybe some day
                            // events.shotsHitCover.Add(new ShotHitCover(bullet.immutable.id, bullet.position));
                            healBullestMap.Remove(healBullet);
                            state.healBulletStates.TryRemove(healBullet.immutable.id, out var _);
                            goto noMoreHealBullet;
                        }
                        healBullet.coverHit.Add(coverHit.position);
                    }

                    // did they hit a rock?
                    foreach (var rockHit in immutableSimulationState.rockMap.Hit(healBullet))
                    {
                        // TODO maybe some day
                        //events.shotsHitRock.Add(new ShotHitRock(bullet.immutable.id, healBullet.position));
                        healBullestMap.Remove(healBullet);
                        state.healBulletStates.TryRemove(healBullet.immutable.id, out var _);
                        goto noMoreHealBullet;
                    }
                }
            noMoreHealBullet:;
            }

            // this duplicate the code the shows the user where their shot is going to go
            //{7F17B933-EC40-4E32-8B07-807977B28FD3}
            foreach (var artyBullet in state.artyBulletStates.Values.OrderBy(x => x.immutable))
            {
                foreach (var step in artyBullestMap.MoveByLength(artyBullet, 10))
                {
                    // did they hit an ant?
                    foreach (var antHit in antsMap.Hit(artyBullet))
                    {
                        if (artyBullet.immutable.id.shooterId == antHit.immutable.id)
                        {
                            continue;
                        }
                        Explode(state, artyBullet, shooterIndex, tick, bulletsMap);
                        artyBullestMap.Remove(artyBullet);
                        state.artyBulletStates.TryRemove(artyBullet.immutable.id, out var _);
                        goto noMoreArtyBullet;

                    }

                    // ignore cover for now, I want to rework it
                    // ...
                    foreach (var coverHit in immutableSimulationState.coverMap.Hit(artyBullet).Where(x => !artyBullet.coverHit.Contains(x.position)))
                    {
                        var distance = artyBullet.GetDistanceTravelled(tick);

                        if (Dies(distance, Guid.Parse("{8A9BDD0E-595C-46FF-A4F8-BA160929DE62}"), tick, coverHit.position, artyBullet.immutable.id))
                        {
                            // we are dead
                            // TODO
                            //events.shotsHitCover.Add(new ShotHitCover(bullet.immutable.id, bullet.position));
                            artyBullestMap.Remove(artyBullet);
                            state.artyBulletStates.TryRemove(artyBullet.immutable.id, out var _);
                            goto noMoreArtyBullet;
                        }
                        artyBullet.coverHit.Add(coverHit.position);
                    }

                    // did they hit a rock?
                    foreach (var rockHit in immutableSimulationState.rockMap.Hit(artyBullet))
                    {
                        // bounce
                        artyBullet.BounceOffOf(rockHit, step);
                    }
                }
            noMoreArtyBullet:;
            }

            foreach (var arcBullet in state.arcBulletStates.Values.OrderBy(x=>x.immutable))
            {
                foreach (var _ in arcBulletsMap.MoveByLength(arcBullet, 10))
                {
                    // did they hit an ant?
                    foreach (var antHit in antsMap.Hit(arcBullet))
                    {
                        var (antKilled, bulletKilled, damage) = MapExtensions.AntArcBulletCollide(antHit, arcBullet, tick, state);

                        // TODO move into that method?
                        // {FBD6644C-0130-4B81-A90E-9846F752F60D} dup
                        if (antHit.immutable.side == arcBullet.immutable.side)
                        {
                            // TODO
                            //events.shotsHitFriendly.Add(new ShotHitFriendly(arcBullet.immutable.id, arcBullet.position));
                        }
                        else
                        {
                            // TODO
                            //events.shotsHit.Add(new ShotHit(arcBullet.immutable.id, antHit, arcBullet.position, damage));
                        }

                        if (antKilled)
                        {
                            KillAnt(state, eventsRequiringVerification, antsMap, antHit.immutable.id, tick);
                        }

                        if (bulletKilled && state.arcBulletStates.TryRemove(arcBullet.immutable.id, out var _))
                        {
                            arcBulletsMap.Remove(arcBullet);
                            state.arcBulletStates.TryRemove(arcBullet.immutable.id, out var _);
                            goto noMoreBullet;
                        }
                    }

                    // did they hit a zapper that is setting up?
                    foreach (var setupHit in zapperSetUpMap.Hit(arcBullet))
                    {
                        if (state.ants.TryGetValue(setupHit.immutable.createdBy, out var owner))
                        {
                            if (owner.immutable.side != arcBullet.immutable.side || arcBullet.immutable.damagesTeam)
                            {
                                setupHit.hp -= arcBullet.immutable.damage;

                                zapperSetUpMap.Remove(setupHit);
                                state.zapperSetupStates.Remove(setupHit);

                                arcBulletsMap.Remove(arcBullet);
                                state.arcBulletStates.TryRemove(arcBullet.immutable.id, out var _);
                                goto noMoreBullet;
                            }
                        }
                    }

                    // did they hit cover?
                    foreach (var coverHit in immutableSimulationState.coverMap.Hit(arcBullet).Where(x=>!arcBullet.coverHit.Contains(x.position)))
                    {
                        var distance = arcBullet.GetDistanceTravelled();

                        if (Dies(distance, Guid.Parse("{8E7542FC-A10C-43B1-AA43-0A6C8FD0BF46}"), tick, coverHit.position, arcBullet.immutable.id))
                        {
                            // we are dead
                            // TODO
                            //events.shotsHitCover.Add(new ShotHitCover(bullet.immutable.id, bullet.position));
                            arcBulletsMap.Remove(arcBullet);
                            state.arcBulletStates.TryRemove(arcBullet.immutable.id, out var _);
                            goto noMoreBullet;
                        }
                        arcBullet.coverHit.Add(coverHit.position);
                    }
                    // did they hit a rock?
                    foreach (var rockHit in immutableSimulationState.rockMap.Hit(arcBullet))
                    {
                        // TODO
                        //events.shotsHitRock.Add(new ShotHitRock(arcBullet.immutable.id, arcBullet.position));
                        arcBulletsMap.Remove(arcBullet);
                        state.arcBulletStates.TryRemove(arcBullet.immutable.id, out var _);
                        goto noMoreBullet;
                    }
                }
            noMoreBullet:;
            }
            
            stopwatchMove.Stop();
            DebugRunTimes.Add($"move bullets", stopwatchMove.ElapsedTicks, frame:tick);

            stopwatchMove = new Stopwatch();
            stopwatchMove.Start();
            foreach (var ant in state.ants.Values.OrderBy(x => x.SortHash()))
            {
                foreach (var step in antsMap.MoveByLength(ant, 10))
                {
                    // did they hit an arty bullet
                    foreach (var artyBullet in artyBullestMap.Hit(ant))
                    {
                        if (artyBullet.immutable.id.shooterId == ant.immutable.id)
                        {
                            continue;
                        }

                        Explode(state, artyBullet, shooterIndex, tick, bulletsMap);
                        artyBullestMap.Remove(artyBullet);
                        state.artyBulletStates.TryRemove(artyBullet.immutable.id, out var _);
                    }

                    // did they hit a bullet?
                    // {FBD6644C-0130-4B81-A90E-9846F752F60D} dup
                    foreach (var bulletHit in bulletsMap.Hit(ant))
                    {
                        var (antKilled, bulletKilled, damage) = MapExtensions.AntBulletCollide(ant, bulletHit, tick, state);

                        if (ant.immutable.side == bulletHit.immutable.side)
                        {
                            events.shotsHitFriendly.Add(new ShotHitFriendly(bulletHit.immutable.id, bulletHit.position));
                        }
                        else
                        {
                            events.shotsHit.Add(new ShotHit(bulletHit.immutable.id, ant, bulletHit.position, damage));
                        }

                        if (bulletKilled && state.bullets.TryRemove(bulletHit.immutable.id, out var _))
                        {
                            bulletsMap.Remove(bulletHit);
                        }

                        if (antKilled && KillAnt(state, eventsRequiringVerification, antsMap, ant.immutable.id, tick))
                        {
                            goto noMoreAnt;
                        }
                    }

                    // did they hit an arc bullet
                    // {FBD6644C-0130-4B81-A90E-9846F752F60D} dup
                    foreach (var arcBulletHit in arcBulletsMap.Hit(ant))
                    {
                        var (antKilled, bulletKilled, damage) = MapExtensions.AntArcBulletCollide(ant, arcBulletHit, tick, state);

                        if (ant.immutable.side == arcBulletHit.immutable.side)
                        {
                            // TODO
                            //events.shotsHitFriendly.Add(new ShotHitFriendly(arcBulletHit.immutable.id, arcBulletHit.position));
                        }
                        else
                        {
                            // TODO
                            //events.shotsHit.Add(new ShotHit(arcBulletHit.immutable.id, ant, arcBulletHit.position, damage));
                        }

                        if (bulletKilled && state.arcBulletStates.TryRemove(arcBulletHit.immutable.id, out var _))
                        {
                            arcBulletsMap.Remove(arcBulletHit);
                        }

                        if (antKilled && KillAnt(state, eventsRequiringVerification, antsMap, ant.immutable.id, tick))
                        {
                            goto noMoreAnt;
                        }
                    }

                    // did they hit a heal bullet?
                    foreach (var healBullet in healBullestMap.Hit(ant))
                    {
                        if (healBullet.immutable.id.shooterId == ant.immutable.id)
                        {
                            continue;
                        }

                        ant.Heal(healBullet);
                        state.healBulletStates.TryRemove(healBullet.immutable.id, out var _);
                    }

                    // did they hit an ant?
                    foreach (var antHit in antsMap.Hit(ant))
                    {
                        if (antHit.SortHash().CompareTo(ant.SortHash()) > 0)
                        {
                            // TODO adjust discresionary force to move around
                            // if the other is a friend and not moving they can move out of the way
                            // push apart
                            ant.BounceOffOf(antHit, step);
                            antHit.BounceOffOf(ant, step);
                        }
                    }

                    // did they hit a rock?
                    foreach (var rockHit in immutableSimulationState.rockMap.Hit(ant))
                    {
                        // TODO adjust discresionary force to move around
                        ant.BounceOffOf(rockHit, step);
                    }
                }
            noMoreAnt:;
            }
            stopwatchMove.Stop();
            DebugRunTimes.Add($"move ants", stopwatchMove.ElapsedTicks, frame: tick);


            foreach (var ant in state.ants.Values)
            {
                if (ant.eyeballMaxious != null)
                {
                    EyeballMaxiousState.UpdateEyePositions(ant.eyeballMaxious, immutableSimulationState.rockMap, ant);
                }
            }

            // side see don't really effect anything here
            // which is interesting
            // maybe thye don't need to be here
            // for bullets I think I only actual use what the current player can see

            lastAdvanceSeconds = (double)simulation.Elapsed.Ticks / (double)TimeSpan.TicksPerSecond;
            return (state, events, eventsRequiringVerification, publicPings);
        }
        catch (Exception e)
        {
            Log.WriteLine(e.ToString());
            throw;
        }
    }

    /// <summary>
    /// don't get out of the way if you are setting a trap
    /// </summary>
    private bool HasActiveTrap(AntState ant, ConcurrentDictionary<Guid, AntState> ants, List<ZapperSetupState> zapperSetupStates)
    {
        if (ants.Where(x => x.Value.zapperGun != null && !x.Value.zapperGun.presists && x.Value.zapperGun.createdBy.Value == ant.immutable.id).Any()){
            return true;
        }

        if (zapperSetupStates.Any(x => x.immutable.createdBy == ant.immutable.id)) {
            return true;
        }

        return false;
    }

    private static void Explode(SimulationState state, ArtyBulletState artyBullet, ShotIndexTracker shotIndexTracker, int frame, Map2<BulletState> bulletMap)
    {
        state.artyBulletStates.TryRemove(artyBullet.immutable.id, out var _);

        var bullets = GrenadeState.Explode(
            shotIndexTracker,
            frame,
            artyBullet.position,
            artyBullet.immutable.id.shooterId,
            artyBullet.immutable.subShot_runForDistance,
            artyBullet.immutable.subShot_speed,
            artyBullet.immutable.side,
            artyBullet.immutable.subShot_damage,
            artyBullet.immutable.ignoreCoverFor,
            artyBullet.immutable.pierce,
            0,
            artyBullet.immutable.knockBack
            );
        foreach (var bullet in bullets)
        {
            state.bullets.TryAdd(bullet.immutable.id, bullet);
            bulletMap.Add(bullet);
            // TODO probably fire an event?
            // is it the same one that gets fired when you fire a normal bullet?
        }
    }

    public static double MaxMoney(IEnumerable<AntState> ants, int side)
    {
        return ants.Where(ant => ant.immutable.side == side).Select(ant => (ant.honeyPotState?.storagePerHp ?? 0) * ant.immutable.maxHp).Sum();
    }

    //private static int HoneyPotCount(IEnumerable<AntState> ants, int side)
    //{
    //    return ants.Where(x => x.honeyPotState != null && x.immutable.side == side).Count();
    //}

    private static bool KillAnt(SimulationState state, EffectEventRequireVerification eventsRequiringVerification, Map2<AntState> antsMap, Guid antId, int tick)
    {
        if (state.ants.TryRemove(antId, out var antHit))
        {
            eventsRequiringVerification.antsKilled.Add(new AntKilled(antHit.immutable.id, antHit.position));
            if (antHit.smokePlacerState != null && antHit.smokePlacerState.mySmokeId != null)
            {
                var remvoed = state.smokes.Remove(antHit.smokePlacerState.mySmokeId.Value);
                Debug.Assert(remvoed);
            }
            if (antHit.honeyPotState != null)
            {
                var gatherRate = SimulationSafeRandom.GetDouble(ResourceSpawnerState.minGatherRate, ResourceSpawnerState.maxGatherRate, Guid.Parse("{E3F50D9F-B331-45EE-9575-8B1DE34E0AF3}"), tick, antId);
                state.resources.Add(new ResourceState(antHit.honeyPotState.lost, new ResourceState.Immutable(gatherRate, antHit.position, new ResourceState.Immutable.Id(tick, antId, true))));
            }
            antsMap.Remove(antHit);
            return true;
        }
        return false;
    }

    // this is a preformance thing
    // it doesn't totally sort things 
    // it also doesn't really help with preformance
    private static IEnumerable<T> SortOfSortByDesending<T>(IEnumerable<T> self, Func<T, double> sorter)
    {

        var sortedList = new SortedList<double, List<T>>();
        bool odd = true;
        foreach (var item in self)
        {
            var index = -sorter(item);
            if (sortedList.TryGetValue(index, out var list))
            {
                list.Add(item);
            }
            else
            {
                sortedList.Add(index, new List<T> { item });
            }
            if (!odd)
            {
                var leader = sortedList.First();
                sortedList.RemoveAt(0);
                foreach (var result in leader.Value)
                {
                    yield return result;
                }
            }
            odd = !odd;
        }
        foreach (var list in sortedList)
        {
            foreach (var res in list.Value)
            {
                yield return res;
            }
        }

    }

    private static void ShootHeals(SimulationState state, int tick, AntState shooter, Vector128<double> target, EffectEvents effectEvents, ShotIndexTracker shotIndexTracker, double addScatter)
    {
        var healBullets = shooter.healGun!.Shoot(shooter.position, target, shooter, tick, shotIndexTracker, addScatter);

        foreach (var healBullet in healBullets)
        {
            state.healBulletStates.TryAdd(healBullet.immutable.id, healBullet);
            // TODO maybe later
            //effectEvents.shotsFired.Add(new ShotFired(shooter.immutable.side, shooter.position, bullet.immutable.id));
        }
    }



    private static void Shoot(SimulationState state, int tick, AntState shooter, Vector128<double> target, EffectEvents effectEvents, ShotIndexTracker shotIndexTracker, double addScatter)
    {
        var bullets = GunState.Shoot(shooter.position, target, shooter, tick, shotIndexTracker, addScatter);

        foreach (var bullet in bullets)
        {
            state.bullets.TryAdd(bullet.immutable.id, bullet);
            effectEvents.shotsFired.Add(new ShotFired(shooter.immutable.side, shooter.position, bullet.immutable.id));
        }
    }

    private static void ArcShoot(SimulationState state, int tick, AntState shooter, Vector128<double> direction, EffectEvents effectEvents, ShotIndexTracker shotIndexTracker, double addScatter)
    {
        var arcBullets = ArcGunState.Shoot(shooter.position, direction, shooter, tick, shotIndexTracker, addScatter);

        foreach (var arcBullet in arcBullets)
        {
            state.arcBulletStates.TryAdd(arcBullet.immutable.id, arcBullet);
            // TODO
            //effectEvents.shotsFired.Add(new ShotFired(shooter.immutable.side, shooter.position, bullet.immutable.id));
        }
    }

    private static HashSet<AntState> SupplyLines(AntState[] ants)
    {
        var antsBySide = ants
            .GroupBy(ant => ant.immutable.side);

        // have a unsupplied list
        // and to process list

        // add flags to the to process list 

        // while there is anything in the to process list
        // take the first item
        // compare them to the unsupplied list
        // update or whatever

        throw new NotImplementedException();

    }

    public static IReadOnlyDictionary<int, HashSet<HealBulletState.Immutable.Id>> SideSeesHealBullet(SimulationState state, IReadOnlyList<(Vector128<double> position, double radius)> avoid)
    {
        var sideSeesHealBullets = new ConcurrentDictionary<int, HashSet<HealBulletState.Immutable.Id>>();

        var unitsBySide = state.ants.Values
            .GroupBy(x => x.immutable.side)
            .ToList();

        foreach (var unitsOfSide in unitsBySide)
        {
            var seen = new HashSet<HealBulletState.Immutable.Id>();
            sideSeesHealBullets[unitsOfSide.Key] = seen;

            foreach (var spottable in state.healBulletStates.Values)
            {
                foreach (var spotter in unitsOfSide)
                {
                    foreach (var seeFrom in spotter.SeesFrom())
                    {
                        var distanceTo = Avx.Subtract(seeFrom, spottable.position).Length();

                        if (distanceTo < spotter.immutable.lineOfSight)
                        {
                            if (PathTo2(seeFrom, spottable.position, avoid))
                            {
                                seen.Add(spottable.immutable.id);
                                goto seen;
                            }
                        }
                    }
                }
            seen:;
            }
        }

        return sideSeesHealBullets;
    }

    public static double OddsToPassCover(IEnumerable<double> distances) {
        var res = 1.0;
        foreach (var distance in distances)
        {
            res*= OddsToPassCover( distance);
        }
        return res;
    }

    public static double OddsToPassCover(double distanceTravelled) {
        // at 1000 distance 80% odds of passing cover
        return 1 - (Math.Min(1000, distanceTravelled) * .2 / 1000);
    }

    public static bool Dies(double distanceTravelled, params object[] parms) {
        var oddsToPass = OddsToPassCover(distanceTravelled);
        var roll = SimulationSafeRandom.GetDouble(0, 1, parms);

        return roll > oddsToPass;
    }

    public static IReadOnlyDictionary<int, HashSet<ArcBulletState.Immutable.Id>> SideSeesArcBullet(SimulationState state, IReadOnlyList<(Vector128<double> position, double radius)> avoid)
    {
        var sideSeesHealBullets = new ConcurrentDictionary<int, HashSet<ArcBulletState.Immutable.Id>>();

        var unitsBySide = state.ants.Values
            .GroupBy(x => x.immutable.side)
            .ToList();

        foreach (var unitsOfSide in unitsBySide)
        {
            var seen = new HashSet<ArcBulletState.Immutable.Id>();
            sideSeesHealBullets[unitsOfSide.Key] = seen;

            foreach (var spottable in state.arcBulletStates.Values)
            {
                foreach (var spotter in unitsOfSide)
                {
                    foreach (var seeFrom in spotter.SeesFrom())
                    {
                        var distanceTo = Avx.Subtract(seeFrom, spottable.position).Length();

                        if (distanceTo < spotter.immutable.lineOfSight)
                        {
                            if (PathTo2(seeFrom, spottable.position, avoid))
                            {
                                seen.Add(spottable.immutable.id);
                                goto seen;
                            }
                        }
                    }
                }
            seen:;
            }
        }

        return sideSeesHealBullets;
    }

    public static IReadOnlyDictionary<int, HashSet<ResourceState.Immutable.Id>> SideSeesResources(SimulationState state, IReadOnlyList<(Vector128<double> position, double radius)> avoid)
    {
        var sideSeesHealResources = new ConcurrentDictionary<int, HashSet<ResourceState.Immutable.Id>>();

        // TODO
        // I calculate this over and over!
        var unitsBySide = state.ants.Values
            .GroupBy(x => x.immutable.side)
            .ToList();

        foreach (var unitsOfSide in unitsBySide)
        {
            var seen = new HashSet<ResourceState.Immutable.Id>();
            sideSeesHealResources[unitsOfSide.Key] = seen;

            foreach (var spottable in state.resources)
            {
                foreach (var spotter in unitsOfSide)
                {
                    foreach (var seeFrom in spotter.SeesFrom())
                    {
                        var distanceTo = Avx.Subtract(seeFrom, spottable.immutable.position).Length();

                        if (distanceTo < spotter.immutable.lineOfSightFar)
                        {
                            if (PathTo2(seeFrom, spottable.immutable.position, avoid))
                            {
                                seen.Add(spottable.immutable.id);
                                goto seen;
                            }
                        }
                    }
                }
            seen:;
            }
        }

        return sideSeesHealResources;
    }

    public static bool SideSeesGather(int side,  SimulationState state, Vector128<double> at, IReadOnlyList<(Vector128<double> position, double radius)> avoid)
    {
        var sideSeesHealResources = new ConcurrentDictionary<int, HashSet<ResourceState.Immutable.Id>>();

        var unitsOfSide = state.ants.Values
            .Where(x => x.immutable.side == side)
            .ToList();

        foreach (var spotter in unitsOfSide)
        {
            foreach (var seeFrom in spotter.SeesFrom())
            {
                var distanceTo = Avx.Subtract(seeFrom, at).Length();

                if (distanceTo < spotter.immutable.lineOfSightFar)
                {
                    if (PathTo2(seeFrom, at, avoid))
                    {
                        return true;
                    }
                }
            }
        }

        return false;
    }


    public static IReadOnlyDictionary<int, HashSet<Guid>> SideSeesSmoke(SimulationState state, 
        IReadOnlyList<(Vector128<double> position, double radius)> rocks,
        IEnumerable<SmokeState> smokes)
    {
        var sideSeesSmoke = new ConcurrentDictionary<int, HashSet<Guid>>();

        var unitsBySide = state.ants.Values
            .GroupBy(x => x.immutable.side)
            .ToList();

        foreach (var unitsOfSide in unitsBySide)
        {
            var seen = new HashSet<Guid>();
            sideSeesSmoke[unitsOfSide.Key] = seen;

            foreach (var (key, spottable) in state.smokes)
            {
                foreach (var point in AntState.PointsToSee(spottable.position, spottable.radius))
                {
                    foreach (var spotter in unitsOfSide)
                    {
                        foreach (var seeFrom in spotter.SeesFrom())
                        {
                            var distanceTo = Avx.Subtract(seeFrom, point).Length();

                            if (distanceTo < spotter.immutable.lineOfSightFar)
                            {
                                if (PathTo2(seeFrom, point, rocks.Union(smokes.Except(new[] { spottable}).Select(x => (x.position, x.radius))).ToArray()))
                                {
                                    seen.Add(key);
                                    goto seen;
                                }
                            }
                        }
                    }
                }
            seen:;
            }
        }

        return sideSeesSmoke;
    }

    public static IReadOnlyDictionary<int, HashSet<ArtyBulletState.Immutable.Id>> SideSeesArtyBullet(SimulationState state, IReadOnlyList<(Vector128<double> position, double radius)> avoid)
    {
        var sideSeesArcBullets = new ConcurrentDictionary<int, HashSet<ArtyBulletState.Immutable.Id>>();

        var unitsBySide = state.ants.Values
            .GroupBy(x => x.immutable.side)
            .ToList();

        foreach (var unitsOfSide in unitsBySide)
        {
            var seen = new HashSet<ArtyBulletState.Immutable.Id>();
            sideSeesArcBullets[unitsOfSide.Key] = seen;

            foreach (var spottable in state.artyBulletStates.Values)
            {
                foreach (var spotter in unitsOfSide)
                {
                    foreach (var seeFrom in spotter.SeesFrom())
                    {
                        var distanceTo = Avx.Subtract(seeFrom, spottable.position).Length();

                        if (distanceTo < spotter.immutable.lineOfSight)
                        {
                            if (PathTo2(seeFrom, spottable.position, avoid))
                            {
                                seen.Add(spottable.immutable.id);
                                goto seen;
                            }
                        }
                    }
                }
            seen:;
            }
        }

        return sideSeesArcBullets;
    }


    public static IReadOnlyDictionary<int, HashSet<BulletState.Immutable.Id>> SideSeesBullet(SimulationState state, IReadOnlyList<(Vector128<double> position, double radius)> avoid)
    {
        var sideSeesBullets = new ConcurrentDictionary<int, HashSet<BulletState.Immutable.Id>>();

        var unitsBySide = state.ants.Values
            .GroupBy(x => x.immutable.side)
            .ToList();

        foreach (var unitsOfSide in unitsBySide)
        {
            var seen = new HashSet<BulletState.Immutable.Id>();
            sideSeesBullets[unitsOfSide.Key] = seen;

            foreach (var spottable in state.bullets.Values)
            {
                foreach (var spotter in unitsOfSide)
                {
                    foreach (var seeFrom in spotter.SeesFrom())
                    {
                        var distanceTo = Avx.Subtract(seeFrom, spottable.position).Length();

                        if (distanceTo < spotter.immutable.lineOfSight)
                        {
                            if (PathTo2(seeFrom, spottable.position, avoid))
                            {
                                seen.Add(spottable.immutable.id);
                                goto seen;
                            }
                        }
                    }
                }
            seen:;
            }
        }

        return sideSeesBullets;
    }

    public const int PingLastsFor = FramesPerSecond.framesPerSecond * 5;

    private class SeeFromAndDistance
    {
        public readonly AntState spotter;
        public readonly Vector128<double> seeFrom;
        public readonly double part;
        public readonly Vector128<double> seeTo;

        public SeeFromAndDistance(AntState spotter, Vector128<double> seeFrom, double part, Vector128<double> seeTo)
        {
            this.spotter = spotter;
            this.seeFrom = seeFrom;
            this.part = part;
            this.seeTo = seeTo;
        }
    }

    private enum SpotType
    {
        close,
        mid,
        far,
        not,
    }

    // could probably optimize a little by doing pairs together
    // starting with units with large lines of sight
    // maybe I could build off what happened last time
    public static (ConcurrentDictionary<int, HashSet<Guid>> sideSees, ConcurrentDictionary<int, HashSet<Guid>> sideSeesMid, ConcurrentDictionary<int, HashSet<Guid>> sideSeesFar) SideSeesUnits(SimulationState state, IReadOnlyList<(Vector128<double> position, double radius)> avoid, int tick)
    {

        // who can see who
        var sideSeesUnits = new ConcurrentDictionary<int, HashSet<Guid>>();
        var sideSeesMid = new ConcurrentDictionary<int, HashSet<Guid>>();
        var sideSeesUnitsFar = new ConcurrentDictionary<int, HashSet<Guid>>();

        var unitsBySide = state.ants
            .Select(ant => (ant: ant.Value, ant.Value.PointsToSee()))
            .GroupBy(x => x.ant.immutable.side)
            .ToList();

        foreach (var unitsOfSide in unitsBySide)
        {
            var sideCanSeeFar = new HashSet<Guid>();
            var sideCanSeeMid = new HashSet<Guid>();
            var sideCanSee = new HashSet<Guid>();

            foreach (var spottable in unitsBySide.Where(x => x.Key != unitsOfSide.Key).SelectMany(x => x))
            {
                var potentailSpots = spottable.Item2
                    .SelectMany(seeTo =>
                        unitsOfSide
                        .SelectMany(unitOfSide => unitOfSide.ant.SeesFrom()
                            .Select(seeFrom => (spotter: unitOfSide.ant, seeFrom: seeFrom, seeTo: seeTo, distanceTo: Avx.Subtract(seeFrom, seeTo).Length()))))
                    .GroupBy(x => x.distanceTo < x.spotter.immutable.lineOfSightDetection ? SpotType.close :
                                  x.distanceTo < x.spotter.immutable.lineOfSight ? SpotType.mid :
                                  x.distanceTo < x.spotter.immutable.lineOfSightFar ? SpotType.far : SpotType.not
                                )
                    .ToDictionary(x => x.Key, x => x.ToArray());

                if (potentailSpots.TryGetValue(SpotType.close, out var cloeses))
                {
                    foreach (var (spotter, seeFrom, seeTo, distanceTo) in cloeses)
                    {
                        if (PathTo2(seeFrom, seeTo, avoid))
                        {
                            sideCanSee.Add(spottable.ant.immutable.id);
                            goto seen;
                        }
                    }
                }
                if (potentailSpots.TryGetValue(SpotType.mid, out var mids))
                {
                    foreach (var (spotter, seeFrom, seeTo, distanceTo) in mids)
                    {
                        if (PathTo2(seeFrom, seeTo, avoid))
                        {
                            sideCanSeeMid.Add(spottable.ant.immutable.id);
                            goto seen;
                        }
                    }
                }

                if (potentailSpots.TryGetValue(SpotType.far, out var fars))
                {
                    var best = fars
                    .Select(pair =>
                    {
                        return new SeeFromAndDistance(pair.spotter, pair.seeFrom, (pair.distanceTo - pair.spotter.immutable.lineOfSight) / (pair.spotter.immutable.lineOfSightFar - pair.spotter.immutable.lineOfSight), pair.seeTo);
                    })
                    .OrderBy(x => x.part)
                    .Where(x =>
                    {
                        if (!PathTo2(x.seeFrom, x.seeTo, avoid))
                        {
                            return false;
                        }
                        return true;
                    })
                    .FirstOrDefault();

                    if (best != null)
                    {
                        var pingsPerCycle = 1 + (4 * (1 - best.part));

                        if (tick % ((int)(PingLastsFor / pingsPerCycle)) == 0)
                        {
                            sideCanSeeFar.Add(spottable.ant.immutable.id);
                        }
                    }
                }

            seen:;
            }

            foreach (var onSide in unitsOfSide.Select(x => x.ant))
            {
                sideCanSee.Add(onSide.immutable.id);
            }

            sideSeesUnits.TryAdd(unitsOfSide.Key, sideCanSee);
            sideSeesMid.TryAdd(unitsOfSide.Key, sideCanSeeMid);
            sideSeesUnitsFar.TryAdd(unitsOfSide.Key, sideCanSeeFar);
        }

        return (sideSeesUnits, sideSeesMid, sideSeesUnitsFar);
    }

    public static (
        ConcurrentDictionary<int, HashSet<ZapperSetupState.Immutable>> sideSees, 
        ConcurrentDictionary<int, HashSet<ZapperSetupState.Immutable>> sideSeesMid, 
        ConcurrentDictionary<int, HashSet<ZapperSetupState.Immutable>> sideSeesFar) 
        SideSeesZapperSetup(SimulationState state, IReadOnlyList<(Vector128<double> position, double radius)> avoid, int tick)
    {

        // who can see who
        var sideSeesUnits = new ConcurrentDictionary<int, HashSet<ZapperSetupState.Immutable>>();
        var sideSeesMid = new ConcurrentDictionary<int, HashSet<ZapperSetupState.Immutable>>();
        var sideSeesUnitsFar = new ConcurrentDictionary<int, HashSet<ZapperSetupState.Immutable>>();

        var unitsBySide = state.ants
            .Select(ant => (ant: ant.Value, ant.Value.PointsToSee()))
            .GroupBy(x => x.ant.immutable.side)
            .ToList();

        var zappersBySide = state.zapperSetupStates.Where(x=>state.ants.ContainsKey(x.immutable.createdBy)).GroupBy(x => state.ants[x.immutable.createdBy].immutable.side).ToList();

        foreach (var unitsOfSide in unitsBySide)
        {
            var sideCanSeeFar = new HashSet<ZapperSetupState.Immutable>();
            var sideCanSeeMid = new HashSet<ZapperSetupState.Immutable>();
            var sideCanSee = new HashSet<ZapperSetupState.Immutable>();

            foreach (var spottable in zappersBySide.Where(x => x.Key != unitsOfSide.Key).SelectMany(x => x))
            {
                var potentailSpots = unitsOfSide
                    .SelectMany(unitOfSide => unitOfSide.ant.SeesFrom()
                        .Select(seeFrom => (spotter: unitOfSide.ant, seeFrom: seeFrom, seeTo: spottable.position, distanceTo: Avx.Subtract(seeFrom, spottable.position).Length())))
                    .GroupBy(x => x.distanceTo < x.spotter.immutable.lineOfSightDetection ? SpotType.close :
                                  x.distanceTo < x.spotter.immutable.lineOfSight ? SpotType.mid :
                                  x.distanceTo < x.spotter.immutable.lineOfSightFar ? SpotType.far : SpotType.not
                                )
                    .ToDictionary(x => x.Key, x => x.ToArray());

                if (potentailSpots.TryGetValue(SpotType.close, out var cloeses))
                {
                    foreach (var (spotter, seeFrom, seeTo, distanceTo) in cloeses)
                    {
                        if (PathTo2(seeFrom, seeTo, avoid))
                        {
                            sideCanSee.Add(spottable.immutable);
                            goto seen;
                        }
                    }
                }
                if (potentailSpots.TryGetValue(SpotType.mid, out var mids))
                {
                    foreach (var (spotter, seeFrom, seeTo, distanceTo) in mids)
                    {
                        if (PathTo2(seeFrom, seeTo, avoid))
                        {
                            sideCanSeeMid.Add(spottable.immutable);
                            goto seen;
                        }
                    }
                }

                if (potentailSpots.TryGetValue(SpotType.far, out var fars))
                {
                    var best = fars
                    .Select(pair =>
                    {
                        return new SeeFromAndDistance(pair.spotter, pair.seeFrom, (pair.distanceTo - pair.spotter.immutable.lineOfSight) / (pair.spotter.immutable.lineOfSightFar - pair.spotter.immutable.lineOfSight), pair.seeTo);
                    })
                    .OrderBy(x => x.part)
                    .Where(x =>
                    {
                        if (!PathTo2(x.seeFrom, x.seeTo, avoid))
                        {
                            return false;
                        }
                        return true;
                    })
                    .FirstOrDefault();

                    if (best != null)
                    {
                        var pingsPerCycle = 1 + (4 * (1 - best.part));

                        if (tick % ((int)(PingLastsFor / pingsPerCycle)) == 0)
                        {
                            sideCanSeeFar.Add(spottable.immutable);
                        }
                    }
                }

            seen:;
            }

            foreach (var onSide in zappersBySide.Where(x=>x.Key == unitsOfSide.Key).SelectMany(x => x))
            {
                sideCanSee.Add(onSide.immutable);
            }

            sideSeesUnits.TryAdd(unitsOfSide.Key, sideCanSee);
            sideSeesMid.TryAdd(unitsOfSide.Key, sideCanSeeMid);
            sideSeesUnitsFar.TryAdd(unitsOfSide.Key, sideCanSeeFar);
        }

        return (sideSeesUnits, sideSeesMid, sideSeesUnitsFar);
    }


    //public static bool PathTo(Map map, Vector128<double> seeFrom, Vector128<double> seeTo)
    //{
    //    foreach (var (hit, _) in MapExtensions.RayCast(map, seeFrom, seeTo))
    //    {
    //        if (hit.Is3(out RockState _))
    //        {
    //            return false;
    //        }
    //    }
    //    return true;
    //}

    public static bool PathTo2(Vector128<double> seeFrom, Vector128<double> seeTo, IReadOnlyList<(Vector128<double> position, double radius)> avoid)
    {
        return Pathing.IsAPathBetween(seeFrom, seeTo, avoid, 0);
    }
}