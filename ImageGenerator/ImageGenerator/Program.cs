﻿// See https://aka.ms/new-console-template for more information
using System.Drawing;
using System.Drawing.Imaging;
using System.Net.Http.Headers;

Console.WriteLine("Hello, World!");





byte[][] lookup = new byte[256][];
lookup[255] = new byte[] { 255, 255, 255, 255 };

var r = new Random();
for (int i = 0; i < lookup.Length; i++)
{
    while (true)
    {
        var sub1 = r.Next((int)Math.Max(i - 30, 0), (int)Math.Min(i + 30, 256));
        var sub2 = r.Next((int)Math.Max(i - 30, 0), (int)Math.Min(i + 30, 256));
        var sub3 = r.Next((int)Math.Max(i - 30, 0), (int)Math.Min(i + 30, 256));
        var sub4 = r.Next((int)Math.Max(i - 30, 0), (int)Math.Min(i + 30, 256));

        var index = (sub1 + sub2 + sub3 + sub4) / 4;

        if (index == i) {
            lookup[index] = new byte[] { (byte)sub1, (byte)sub2, (byte)sub3, (byte)sub4 };
            Console.WriteLine($"found one {i}");
            break;
        }
    }
}

Console.WriteLine("got here");
//var pass0r = new[] { new[] { (byte)0x88 } };  // 1
//var pass1r = Duplicate(pass0r, lookup); // 2
//var pass2r = Duplicate(pass1r, lookup); // 4 
//var pass3r = Duplicate(pass2r, lookup); // 8
//var pass4r = Duplicate(pass3r, lookup); // 16
var pass4r = GetByteGrid(16, 16);
var pass5r = Duplicate(pass4r, lookup); // 32
var pass6r = Duplicate(pass5r, lookup); // 64
var pass7r = Duplicate(pass6r, lookup); // 128
var pass8r = Duplicate(pass7r, lookup); // 256
var pass9r = Duplicate(pass8r, lookup); // 512
var pass10r = Duplicate(pass9r, lookup); // 1024
var pass11r = Duplicate(pass10r, lookup); // 2049

var pass0g = new[] { new[] { (byte)0x87 } };  // 1
var pass1g = Duplicate(pass0g, lookup); // 2
var pass2g = Duplicate(pass1g, lookup); // 4 
var pass3g = Duplicate(pass2g, lookup); // 8
var pass4g = Duplicate(pass3g, lookup); // 16
//var pass4g = GetByteGrid(16, 16);
var pass5g = Duplicate(pass4g, lookup); // 32
var pass6g = Duplicate(pass5g, lookup); // 64
var pass7g = Duplicate(pass6g, lookup); // 128
var pass8g = Duplicate(pass7g, lookup); // 256
var pass9g = Duplicate(pass8g, lookup); // 512
var pass10g = Duplicate(pass9g, lookup); // 1024
var pass11g = Duplicate(pass10g, lookup); // 2049

int width = 4096;
int height = 4096;


//byte[] imageData = pass10r.SelectMany(x => x.SelectMany(y => new[] { y, y, y })).ToArray();
//byte[] imageData = pass11r.Zip(pass11g).SelectMany(linePair => linePair.First.Zip(linePair.Second).SelectMany(pair => new byte[] { pair.First, pair.Second, 0x88 })).ToArray();

byte[] imageData = FullRandom(width, height);

unsafe
{
    fixed (byte* ptr = imageData)
    {
        using (Bitmap image = new Bitmap(width, height, width * 3, PixelFormat.Format24bppRgb, new IntPtr(ptr)))
        {
            image.Save("test.bmp");
        }
    }
}


byte[][] Duplicate(byte[][] input, byte[][] lookup) 
{
    return input.SelectMany(row => new byte[][] 
    {
        row.SelectMany(entry => new []{ lookup[entry][0] ,lookup[entry][1] }).ToArray(),
        row.SelectMany(entry => new []{ lookup[entry][2] ,lookup[entry][3] }).ToArray(),
    }).ToArray();
}

// use normals
// build a "lookup" texture x,y => the four colors that make that pixel

// top level, use the main texture
// zoom in enought, look up once
// zoon in a lot look up twice?


// really I should generate hight
// and them I can do different mappings off that
// higher points catch more light
// hight map
// normal map


// ideas:
// noise
// generate a maze
// generate 4 random numbers and then have them make squares sort them and assign each index to a position in the square



byte[] FullRandom(int width, int height) { 

    var list = new List<byte>();

    var r = new Random();

    for (int y = 0; y < height; y++)
    {
        for (int x = 0; x < width; x++) {
            var color = new byte[3];
            r.NextBytes(color);
            list.AddRange(color);
        }
    }
    return list.ToArray();

}

byte[][] GetByteGrid(int width, int height)
{
    var res = new List<byte[]>();

    var r = new Random();

    for (int y = 0; y < height; y++)
    {
        var line = new List<byte>();
        for (int x = 0; x < width; x++)
        {
            line.Add((byte)r.Next(100, 150));
        }
        res.Add(line.ToArray());
    }
    return res.ToArray();

}