using Godot;
using System;

public partial class RemoveWhenDone : GpuParticles3D
{
	[Export]
	public float power = 1;

	//// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		OneShot = true;
		Amount = Math.Max(1,(int)(Amount * power));
		Lifetime = Lifetime * Mathf.Sqrt(power);

		var material = (ParticleProcessMaterial)ProcessMaterial.Duplicate();
		material.InitialVelocityMax = Mathf.Sqrt(power) * material.InitialVelocityMax;
		material.InitialVelocityMin = Mathf.Sqrt(power) * material.InitialVelocityMin;
		material.ScaleMin = power * material.ScaleMin;
		material.ScaleMax = power * material.ScaleMax;
		ProcessMaterial = material;

		Finished += () => { QueueFree(); };


	}

	//// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(double delta)
	{
		// this doens't seem to work
		//if (!Emitting) QueueFree(); 
	}

}
