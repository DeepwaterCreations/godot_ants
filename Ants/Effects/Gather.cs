using Godot;
using System;
using System.Diagnostics;

public partial class Gather : Node3D
{
	private const int MySpeed = 600;
	private const double LiveFor = .15;
	private static PackedScene gatherBitPacked = GD.Load<PackedScene>("res://Effects/GatherBit.tscn");

	private Vector3 velocity;

	public double age = 0;

	/// <param name="velocity">should be normalized</param>
	public void Init(Vector3 velocity){
		Debug.Assert(velocity.IsNormalized());
		this.velocity = velocity;
	} 

	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(double delta)
	{
		GlobalPosition += velocity*((float)delta * MySpeed);

		if ((age <= LiveFor * .0 && (age + delta) > LiveFor * .0) ||
			(age <= LiveFor *.25 && (age+ delta) > LiveFor * .25) ||
			(age <= LiveFor * .5 && (age + delta) > LiveFor *.5) ||
			(age <= LiveFor * .75 && (age + delta) > LiveFor * .75) ||
			(age <= LiveFor * 1.0 && (age + delta) > LiveFor * 1.0))
		{
			var bit = gatherBitPacked.Instantiate<GatherBit>();

			GetParent().AddChild(bit);
			bit.GlobalPosition = this.GlobalPosition;
		}

		age += delta;

		if (age > LiveFor) {
			QueueFree();
		}
	}

	public static double HowFarITravel() {
		return LiveFor * MySpeed;
	}
}
