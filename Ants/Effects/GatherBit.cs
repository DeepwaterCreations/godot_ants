using Godot;
using System;
using System.Runtime.Intrinsics;

public partial class GatherBit : MeshInstance3D
{
	private double age = 0;
	private const double livesFor = .2;
	//private const double max = -(livesFor / 2) * ((livesFor / 2) - livesFor);

	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(double delta)
	{
		age += delta;
		var v = .5*livesFor -Math.Abs(age - (.5*livesFor));
		var part = (float)Math.Clamp(v / (livesFor*.5), 0, 1);
		this.Scale = new Vector3(3*part, 3*part, 3*part);
		this.GlobalPosition = new Vector3(this.GlobalPosition.X, this.GlobalPosition.Y, 50*part);
		
		if (age >= livesFor)
		{
			QueueFree();
		}
	}
	// 
	// 
}
