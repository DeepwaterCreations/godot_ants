using Godot;
using System;

public partial class MatchPosition : Camera3D
{
	[Export]
	public Camera3D follows;
	
	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
	}

	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(double delta)
	{
		Transform = follows.Transform;
		Fov = follows.Fov;
  		Size = follows.Size;
		

		// for some reason if I set this >=1
		// it breaks 
		//Near = GlobalPosition.Z -110;
		//Far = GlobalPosition.Z+ 110;
		//Log.WriteLine($"Near: {Near}, Far: {Far} ");
	}
}
