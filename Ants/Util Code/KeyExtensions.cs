﻿using Ants.Domain;

public static class KeyExtensions {
	public static Key ToDomainKey(this Godot.Key key) {
		return new Key((long)key);
	}
    public static Godot.Key ToGodotKey(this Key key)
    {
        return (Godot.Key)key.keyCode;
    }
}