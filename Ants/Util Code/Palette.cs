using Godot;

public class Palette
{

    public static Color[] playerColors = new[] {
        new Color(7.0f,0.4f,0.4f,1),
        new Color(0.4f,0.4f,0.7f,1),
        new Color(0.4f,0.7f,0.4f,1),
        new Color(0.6f,0.6f,0.3f,1),
        new Color(0.3f,0.6f,0.6f,1),
        new Color(0.6f,0.3f,0.6f,1),
    };

    public static Color shieldHint = new Color(.6f, .2f, .2f);
    // not scouted is set on the los camera in outline camera
    public static Color losFar = new Color(.6f, .6f, .6f);
    public static Color los = new Color(.84f, .84f, .84f);
    public static Color losDetection = new Color(1f, 1f, 1f);

    public static Color pathDrawer = new Color(1, 1, 1);
}