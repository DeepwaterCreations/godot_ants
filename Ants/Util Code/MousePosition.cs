﻿using Godot;
using System.Diagnostics;

public static class MousePosition {
    public static Vector2 CastPosition2d(Node node)
    {

        var vp = node.GetViewport();
        var camera = vp.GetCamera3D();

        // othoginal 
        var rayOrigin = camera.ProjectRayOrigin(vp.GetMousePosition());
        return new Vector2(rayOrigin.X, rayOrigin.Y);

        // prospective
        var awayFromCamera = camera.ProjectRayNormal(vp.GetMousePosition());
        awayFromCamera *= -(camera.GlobalPosition.Z / awayFromCamera.Z);
        var res = new Vector2(camera.GlobalPosition.X + awayFromCamera.X, camera.GlobalPosition.Y + awayFromCamera.Y);
        return res;
    }

    //public static (Vector2 start, Vector2 end) VisibleRect(Node node)
    //{
    //    var vp = node.GetViewport();
    //    var camera = vp.GetCamera3D();

    //    var position = camera.ProjectLocalRayNormal(vp.GetVisibleRect().Position);
    //    position *= (camera.GlobalPosition.Z / position.Z);
    //    var end = camera.ProjectLocalRayNormal(vp.GetVisibleRect().End);
    //    end *= (camera.GlobalPosition.Z / end.Z);


    //    return (
    //        new Vector2(
    //            camera.GlobalPosition.X + Mathf.Min(position.X, end.X), 
    //            camera.GlobalPosition.Y + Mathf.Min(position.Y, end.Y)), 
    //        new Vector2(
    //            camera.GlobalPosition.X + Mathf.Max(position.X, end.X), 
    //            camera.GlobalPosition.Y + Mathf.Max(position.Y, end.Y)));
    //}

    public static Vector3 CastPosition3d(Node node)
    {
        var vp = node.GetViewport();
        var camera = vp.GetCamera3D();
        var awayFromCamera = camera.ProjectRayNormal(vp.GetMousePosition());
        awayFromCamera *= -(camera.GlobalPosition.Z / awayFromCamera.Z);

        return new Vector3(camera.GlobalPosition.X + awayFromCamera.X, camera.GlobalPosition.Y + awayFromCamera.Y, 0);
    }
}