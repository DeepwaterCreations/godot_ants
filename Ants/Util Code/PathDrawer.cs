using Ants.AppState;
using Godot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Intrinsics;
using System.Threading.Tasks;

public partial class PathDrawer : Node2D
{
	Dictionary<Vector128<double>, Pathing.Reachable> pointsDict;
	Vector128<double>[] pathPoints;

	public override void _Process(double delta)
	{
		var sizeToDraw = Pathing.unitSize_Medium;

        if (pathPoints == null && pointsDict == null && StaticAppState.TryGetState(out IHaveGame appState)) {
			var avoidables = appState.Game.immutableSimulationState.rocks.Select(x=>(x.position, x.physicsImmunatbleInner.radius)).ToArray();
			pathPoints = avoidables.SelectMany(x => Pathing.GeneratePoints(x, avoidables, sizeToDraw)).ToArray();

			pointsDict = Pathing.DirectPaths(
				avoidables, 
				FildMud(GetTree().Root).Select(x => (x.GlobalPosition.ToNumericsVector(), (double)x.radius)).ToArray(),
				appState.Game.immutableSimulationState.mudMap,
				Pathing.Mode.SlowMud,
                sizeToDraw).pointsDict;
			QueueRedraw();
		}

		base._Process(delta);
	}

	// this doesn't feel great since I find the mud in other places 
	public IEnumerable<Mud> FildMud(Node node) {
		if (node is Mud mud)
		{
			yield return mud;	
		}

		foreach (var child in node.GetChildren())
		{
			foreach (var mudChild in FildMud(child))
			{
				yield return mudChild;
			}
		}
	}

	public override void _Draw()
	{
		base._Draw();
		if (pointsDict == null)
		{
			return;
		}

		foreach (var reachable in pointsDict.Values)
		{
			foreach (var path in reachable.distanceTo.Values)
			{
				var points = path.ToPoints();
				for (int i = 0; i < points.Count - 1; i++)
				{
					DrawLine(points[i].ToVector2(), points[i + 1].ToVector2(), Palette.pathDrawer);
				}
			}
		}
		foreach (var pathPoint in pathPoints)
		{
			DrawCircle(pathPoint.ToVector2(), 20, Palette.pathDrawer);
		}
	}
}

