﻿using Godot;

namespace Ants.V2
{
    internal class GdLogger : ILogger
    {

        public GdLogger() { 
        
        }
        public void WriteLine(string v)
        {
            GD.Print(v);
        }
    }
}