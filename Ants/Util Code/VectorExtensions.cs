﻿using Godot;
using System.Runtime.Intrinsics;
using Ants.Domain.Infrastructure;

public static class VectorExtensions
{
	public static Vector2 ToVector2(this Vector128<double> self) {
		return new Vector2((float)self.X(), (float)self.Y());
	}

    public static Vector3 ToVector3(this Vector128<double> self, float z)
    {
        return new Vector3((float)self.X(), (float)self.Y(), z);
    }

    public static Vector2 ToVector2(this GlobalPosionInt self)
    {
        return new Vector2((float)(self.X()/ GlobalPosionInt.scale), (float)(self.Y() / GlobalPosionInt.scale));
    }

    public static Vector128<double> ToNumericsVector(this Vector2 self)
    {
        return Vector128.Create((double)self.X, (double)self.Y);
    }

    public static Vector128<double> ToNumericsVector(this Vector3 self)
    {
        return Vector128.Create((double)self.X, (double)self.Y);
    }
}
