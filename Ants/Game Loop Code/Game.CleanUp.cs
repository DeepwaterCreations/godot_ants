﻿using Ants.Domain.State;
using Ants.Network.Messages;
using Godot;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

public partial class Game
{
    internal SemaphoreSlim readyToCleanUp;

    public void ReleaseCleanUp() {
        if (readyToCleanUp.CurrentCount == 0)
        {
            readyToCleanUp.Release();
        }
    }

    public async Task CleanUp() {
        try
        {
            while (true)
            {
                await Task.Yield(); // this is how I'm doing low priority, not sure if it works

                await readyToCleanUp.WaitAsync(cancellationTokenSource.Token).ConfigureAwait(false);
                //readyToCleanUp = new TaskCompletionSource(cancellationTokenSource.Token);

                if (cancellationTokenSource.Token.IsCancellationRequested)
                {
                    return;
                }


                UpdateHashesFromStates();
                CleanUpOldOrders();
                CleanUpOldHashes();
            }
        }
        catch (Exception e)
        {
            Log.WriteLine(e.ToString());
        }
    }

    // shouldn't be called more than once at once
    // this isn't threadsafe {E00BE453-F554-46F6-97F0-F243A7AAAD4D}
    internal void UpdateHashesFromStates()
    {
        var pressure = 0;
        while (true)
        {
            pressure++;
            if (0 == pressure % 1000)
            {
                Log.WriteLine("stuck in UpdateStateHashes");
            }

            var myFrame = frameHashes[index].Keys.Any() ? frameHashes[index].Keys.Max() + 1 : 0;
            var actualRunWith = GetInputsSubmittedOnFrame(myFrame - 1);

            if (!TryGetRunWith(myFrame, out var runWith))
            {
                // no new state hashes 
                Log.WriteLine("no run with");
                break;
            }
            if (!TryGetState(myFrame, out var state))
            {
                //Log.WriteLine($"we don't have state {myFrame} yet");
                // no new state hashes 
                //Log.WriteLine("no state");
                break;
            }
            if (!TryGetTargetState(myFrame, out var targetState))
            {
                //Log.WriteLine($"we don't have state {myFrame} yet");
                // no new state hashes 
                Log.WriteLine("no targetState");
                break;
            }

            if (!targetState.IsCompleted) {
                Debugger.Launch();
            }

            Debug.Assert(targetState.IsCompleted, "if we have a state, it's target state should be complete");

            //var waitingFor = runWith.WaitingForPlayers();
            if (actualRunWith.WaitingForPlayers().Any())
            {
                // Log.WriteLine("waiting for someone");
                //Log.WriteLine($"{myFrame} isn't final, waiting for players {string.Join(", ", waitingFor)}");
                break;
            }
            if (!runWith.Equals(actualRunWith))
            {
                Log.WriteLine("run width doesn't match");
                //Log.WriteLine($"what we ran with doesn't match what we should have run with for frame: {myFrame}");
                // the state isn't final
                break;
            }

            var myHash = new CompositSimulationState(state, targetState.Result).Hash();
            //Log.WriteLine($"myHash:{myHash} myFrame:{myFrame}");
            // you update others frameHashes here:
            // {46CB6561-8355-4999-991D-F7E42F03D280}
            var innerPressure = 0;
            while (true)
            {
                innerPressure++;
                if (0 == innerPressure % 1000)
                {
                    Log.WriteLine("stuck in the inner loop in UpdateHashesFromStates");
                }

                var current = frameHashesIndexes[index];
                if (current.value != myFrame - 1)
                {
                    break;
                }

                if (Interlocked.CompareExchange(ref frameHashesIndexes[index], new RefInt(myFrame), current) == current)
                {

                    if (frameHashes[index].TryAdd(myFrame, myHash))
                    {
                        Log.bugLogger?.WriteLine($"added framehash (UpdateHashesFromStates) {myFrame}");
                        Log.bugLogger2?.WriteLine($"added framehash (UpdateHashesFromStates) frame: {myFrame}, hash: {myHash} ");
                        Debug.Assert(hashAcks[index] < myFrame);
                        hashAcks[index] = myFrame;

                        if (effectsAwaitingConfirmation.TryRemove(myFrame, out var effectDict))
                        {
                            if (effectDict.TryGetValue(myHash, out var effectSet))
                            {
                                confirmedEffects.Enqueue(effectSet);
                            }
                        }
                        break;
                    }
                    else
                    {
                        Log.WriteLine($"couldn't add my own stateHash! frame: {myFrame}, playerId: {index}");
                        Log.bugLogger2?.WriteLine($"couldn't add framehash (UpdateHashesFromStates) frame: {myFrame}, hash: {myHash} ");
                    }
                }
            }
        }
    }

    internal void CleanUpOldOrders()
    {
        // we need to hold on to orders until we are done with their hashes
        // we might need to print them out if the hashes don't match
        // the orders for a frame are on the frame before it 
        // thus the - 1
        // here: {650B0574-04BF-4D94-AFAF-ACA8740ECF28}
        var endAt = frameHashes[index].Keys.Any() ? frameHashes[index].Keys.Min() - 1 : 0;

        for (int i = 0; i < orders.Length; i++)
        // < because orders are logged on the frame they are enterd not on the framet hey are applied
        // so if we have a hash for frame 5 we can delete the orders associated with frame 4
        {
            if (orders[i].Keys.Any())
            {
                // don't remove the max order for any player
                // it's used here: {71252FD7-FE46-4603-99C8-5A54503FE405}
                // and here: {A5824BA4-050C-43DD-B12A-DEE0E79F0A0C}
                // or maybe they should take hashes into account?
                var max = orders[i].Keys.Max();
                var toRemoves = orders[i].Keys.Where(x => x < endAt && x < max).ToArray();

                foreach (var toRemove in toRemoves)
                {
                    orders[i].TryRemove(toRemove, out var _);
                    //Log.WriteLine($"removed order {toRemove} {i}");
                }


                if (orders[i].Count > frameHashes.Count() * 3600)
                {
                    Log.WriteLine($"you have collected {orders.Sum(x => x.Count)} orders for player {i}, endAt: {endAt}, max: {max}, just removed: {toRemoves.Length}");
                }
            }
        }
    }

    // delete items where we have the full set
    // and all the other players have acked it
    internal void CleanUpOldHashes()
    {

        if (frameHashes.Any(x => !x.Keys.Any()))
        {
            return;
        }

        // don't delete any hashes that haven't been ack'ed
        var dontDeletePast = hashAcks
            .Min();

        // start at the smallest hash we have
        var startAt = frameHashes.Select(x => x.Keys.Aggregate(int.MaxValue, Math.Min)).Min();
        
        // while we have that hash for all players
        // and we have at least one more hash for all players
        // we don't want to delete the last hash for anybody
        // it's not a huge deal if we do
        // it'll just stop us from re-acking it
        for (var at = startAt; frameHashes.All(x => x.ContainsKey(at + 1)) && at < dontDeletePast; at++)
        {
            var ourHash = frameHashes[index][at];
            for (int i = 0; i < frameHashes.Length; i++)
            {
                if (i != index)
                {
                    //Log.WriteLine($"comparing hashes theirs: {frameHashes[i][at]}, ours: {ourHash}, i; {i}, at: {at}");
                    Log.bugLogger2.WriteLine($"comparing hashes theirs: {frameHashes[i][at]}, ours: {ourHash}, i; {i}, at: {at}");
                    if (frameHashes[i][at] != ourHash)
                    {
                        WriteState(at, i);
                    }
                }
            }

            for (int i = 0; i < frameHashes.Length; i++)
            {
                // {6C35072C-F482-4F33-9004-384A8F5EA8A6}
                if (!frameHashes[i].TryRemove(at, out var _))
                {
                    Log.WriteLine($"failed to delete frameHashes {at}");
                }
                //else
                //{
                //    maxDeleted = at;
                //}
            }
        }
        //if (maxDeleted != -1)
        //{
        //    Log.WriteLine($"deleted up to: {maxDeleted}");
        //}
    }
}

