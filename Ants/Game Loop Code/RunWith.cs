﻿using Ants.Domain.Infrastructure;
using Ants.Network.Messages;
using Prototypist.TaskChain;
using System;
using System.Collections.Generic;
using System.Linq;

public class RunWith: IStateHash
{
    private ConcurrentLinkedList<FrameData.OrderData>[] ordersList;


    public RunWith(ConcurrentLinkedList<FrameData.OrderData>[] ordersList)
    {
        this.ordersList = ordersList;
    }

    public bool HasOrdersForAllPlayers() {
        return ordersList.All(x => x != null);
    }

    public int[] WaitingForPlayers() {
        var res = new List<int>();
        for (int i = 0; i < ordersList.Length; i++)
        {
            if (ordersList[i] == null) {
                res.Add(i);
            }
        }
        return res.ToArray();
    }

    internal List<(Guid id, IPendingOrderState order, bool queue)> Flatten()
    {
        var res = new List<(Guid id, IPendingOrderState order, bool queue)>();
        foreach (var ordersForPlayer in ordersList)
        {
            if (ordersForPlayer != null) {
                foreach (var order in ordersForPlayer)
                {
                    res.Add((order.id, order.order, order.queue));
                }
            }
        }
        return res;
    }

    /// <summary>
    /// two run with are the same if they contain the same set of orders
    /// in other words, RunWiths are equal when the flatten list of orders are the same
    /// this means null entries are equivent to empty lists entries in ordersList
    /// {CA4AB3E1-BF8F-47A0-8D51-1647C6A11D7A}
    /// </summary>
    public override bool Equals(object obj)
    {
        return obj is RunWith with &&
               this.Flatten().SequenceEqual(with.Flatten());
    }

    /// <summary>
    /// {CA4AB3E1-BF8F-47A0-8D51-1647C6A11D7A}
    /// </summary>
    public override int GetHashCode()
    {
        return Flatten().UncheckedSum(x => x.GetHashCode());
    }

    public IEnumerable<(string, object)> HashData()
    {
        yield return (nameof(ordersList), ordersList);
    }
}
