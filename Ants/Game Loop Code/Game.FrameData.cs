﻿using Ants.Network.Messages;
using Godot;
using Prototypist.TaskChain;
using Prototypist.Toolbox.IEnumerable;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Channels;
using System.Threading.Tasks;
using static Ants.Network.Messages.FrameData;



// handle frame data
public partial class Game
{

    // player, largest frame number recived from me 
    public readonly int[] acks;

    // player, largest frame hash recived from me 
    public readonly int[] hashAcks;

    Channel<FrameData> framesChannel = Channel.CreateUnbounded<FrameData>();

    public void Handle(FrameData frameData)
    {
        framesChannel.Writer.WriteAsync(frameData);
    }

    public async Task PrcocessFrames()
    {
        try
        {
            while (true)
            {
                await framesChannel.Reader.WaitToReadAsync(cancellationTokenSource.Token).ConfigureAwait(false);

                int? rollbackTo = null;
                var frames = new List<FrameData>();
                while (framesChannel.Reader.TryRead(out var frame))
                {
                    rollbackTo = UpdateOrders(frame, rollbackTo);
                    frames.Add(frame);

                    UpdateDoNotPassOthers();
                }

                if (rollbackTo != null)
                {
                    RunFromFrame(rollbackTo.Value);
                }

                foreach (var frame in frames)
                {
                    UpdateAcks(frame);
                    UpdateFrameHashes(frame);
                }
            }
        }
        catch (Exception e) {
            Log.WriteLine(e.ToString());
        }
    }

    internal int? UpdateOrders(FrameData frameData, int? rollbackTo = null)
    {
        var playerOrders = orders[frameData.playerId];


        for (int i = 0; i < frameData.pendingOrdersByFrame.Length; i++)
        {
            var ordersForFrame = frameData.pendingOrdersByFrame[i];
            var frame = frameData.firstOrderFrame + i;

            // {A5824BA4-050C-43DD-B12A-DEE0E79F0A0C}
            // only add if this is the frist frame or you have the frame before
            // this is going to be a concern for deleting
            // never delete everything
            // Why? when I ack I just say the largest frame we have, I don't check all the frames
            // maybe that is wrong...
            if (playerOrders.ContainsKey(frame - 1) || frame == 0)
            {
                var list = new ConcurrentLinkedList<OrderData>();

                foreach (var item in ordersForFrame)
                {
                    list.Add(item);
                }

                var lockedIn = playerOrders.GetOrAdd(frame, list);

                if (!lockedIn.SetEqual(list))
                {
                    Log.WriteLine(@$"how could we get set two sets of orders for one frame? player: {i}, frame: {frame} 
lockedIn:
{DebugPrint.DebugList(lockedIn.ToArray(), 0, new Dictionary<object, int>())}
list:
{DebugPrint.DebugList(list.ToArray(), 0, new Dictionary<object, int>())}
");
                }

                if (lockedIn == list && list.Any()) {
                    if (rollbackTo == null)
                    {
                        rollbackTo = frame;
                    }
                    else
                    {
                        rollbackTo = Math.Min(frame, rollbackTo.Value);
                    }
                }
            }
            else if (playerOrders.Keys.Any() && playerOrders.Keys.Max() < frame)
            {
                Log.WriteLine($"frame {frame} rejected, out of order");
            }
        }

        return rollbackTo;
    }

    public void UpdateAcks(FrameData frameData)
    {
        var ackedFrame = frameData.greatestReceivedFrameIndexByPlayer[index];
        while (true)
        {
            var current = acks[frameData.playerId];
            if (current > ackedFrame)
            {
                break;
            }

            if (Interlocked.CompareExchange(ref acks[frameData.playerId], ackedFrame, current) == current)
            {
                break;
            }
        }
 
        var ackedHash = frameData.greatestReceivedHashIndexByPlayer[index];
        while (true)
        {
            var current = hashAcks[frameData.playerId];
            if (current > ackedHash)
            {
                break;
            }

            if (Interlocked.CompareExchange(ref hashAcks[frameData.playerId], ackedHash, current) == current)
            {
                break;
            }
        }
    }

    internal void UpdateFrameHashes(FrameData frameData)
    {
        for (var i = 0; i < frameData.stateHashes.Length; i++)
        {
            var frame = frameData.firstHashFrame + i;

            // {46CB6561-8355-4999-991D-F7E42F03D280}
            while (true)
            {
                var current = frameHashesIndexes[frameData.playerId];
                if (current.value != frame - 1)
                {
                    break;
                }

                if (Interlocked.CompareExchange(ref frameHashesIndexes[frameData.playerId], new RefInt(frame), current) == current)
                {
                    if (!frameHashes[frameData.playerId].TryAdd(frame, frameData.stateHashes[i]))
                    {
                        Log.bugLogger2?.WriteLine($"couldn't add framehash (UpdateFrameHashes) {frame}, hash: {frameData.stateHashes[i]}, i: {i}");
                        Log.WriteLine($"couldn't add stateHash! frame: {frame}, playerId: {frameData.playerId}");

                    }
                    else {
                        Log.bugLogger2?.WriteLine($"added framehash (UpdateFrameHashes) {frame}, hash: {frameData.stateHashes[i]}, i: {i}");
                        Log.bugLogger?.WriteLine($"added framehash (UpdateFrameHashes) {frame}"); 
                    } 
                    break;
                }
            }
        }
    }


    public FrameData GetFrameData(int frame)
    {
        var acksToSend = new int[orders.Length];

        for (int i = 0; i < orders.Length; i++)
        {
            
            if (!orders[i].Keys.Any()) {
                acksToSend[i] = -1;
            }
            else {
                var max = orders[i].Keys.Max();
                acksToSend[i] = max;

                // for debugging
                // check the that orders we have are continuous
#if DEBUG
                var lookingAt = max;

                while (orders[i].ContainsKey(lookingAt - 1))
                {
                    lookingAt--;
                }
                // less than because orders could be deleted
                if (lookingAt > orders[i].Keys.Min()) {
                    Log.WriteLine($"that aint right girl, lookingAt: {lookingAt}, orders[{i}].Keys.Min(): {orders[i].Keys.Min()}");
                }
#endif
            }
        }
        var start = acks.Min() + 1;// +1 becuase we they have already received the min
        var ordersForFrames = new OrderData[frame - start + 1][]; //+1 because we want the number of elements including start and frame
        for (int i = 0; i < ordersForFrames.Length; i++)
        {
            ordersForFrames[i] = GetOrdersForFrame(index, i + start);
        }

        var hashStart = hashAcks
            .Min() + 1;

        var hashes = new List<int>();

        var at = hashStart;
        while (frameHashes[index].TryGetValue(at, out var hash))
        {
            hashes.Add(hash);
            at++;
        }

        var hashAcksToSend = new int[frameHashes.Length];
        for (int i = 0; i < frameHashes.Length; i++)
        {
            //Sequence contains no elements
            //at System.Linq.ThrowHelper.ThrowNoElementsException()
            //at System.Linq.Enumerable.MinMaxInteger[T, TMinMax](IEnumerable`1 source)
            //at Game.GetFrameData(Int32 frame) in C: \Users\Colin_000\source\repos\Ants\godot_ants\Ants\Game Loop Code\Game.FrameData.cs:line 233
            //at Ants.AppState.InGameState.SendFrameDataAsync(Int32 frame) in C: \Users\Colin_000\source\repos\Ants\godot_ants\Ants\App State Code\InGameState.cs:line 40
            //at SendData.Send(WaitingFrame waitingFrame) in C: \Users\Colin_000\source\repos\Ants\godot_ants\Ants\Game Loop Code\Game.MainLoop.cs:line 38
            //at Game.MainLoop(Int64 startAtUtc, ISendData sendData) in C: \Users\Colin_000\source\repos\Ants\godot_ants\Ants\Game Loop Code\Game.MainLoop.cs:line 128
            //
            // something is removing frame hashes before they have been sent?
            // when do I delete them?
            // here: {6C35072C-F482-4F33-9004-384A8F5EA8A6}
            // < hashAcks.min
            //
            // and now another error
            // Sequence contains no elements
            //      at System.Linq.ThrowHelper.ThrowNoElementsException()
            //      at System.Linq.Enumerable.MinMaxInteger[T, TMinMax](IEnumerable`1 source)
            //      at Game.GetFrameData(Int32 frame) in C: \Users\Colin_000\source\repos\Ants\godot_ants\Ants\Game Loop Code\Game.FrameData.cs:line 258
            //      at Ants.AppState.InGameState.SendFrameDataAsync(Int32 frame) in C: \Users\Colin_000\source\repos\Ants\godot_ants\Ants\App State Code\InGameState.cs:line 40
            //      at SendData.Send(WaitingFrame waitingFrame) in C: \Users\Colin_000\source\repos\Ants\godot_ants\Ants\Game Loop Code\Game.MainLoop.cs:line 38
            //      at Game.MainLoop(Int64 startAtUtc, ISendData sendData) in C: \Users\Colin_000\source\repos\Ants\godot_ants\Ants\Game Loop Code\Game.MainLoop.cs:line 126

            hashAcksToSend[i] = frameHashes[i].Keys.Aggregate(-1, Math.Max);
        }

        return new FrameData(
            index,
            start,
            ordersForFrames,
            acksToSend,
            hashStart,
            hashes.ToArray(),
            hashAcksToSend
            );
    }

    internal OrderData[] GetOrdersForFrame(int player, int frame)
    {
        return orders[player]
            .GetOrAdd(frame, new ConcurrentLinkedList<OrderData>())
            .ToArray();
    }
}
