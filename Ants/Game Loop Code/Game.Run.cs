﻿using Ants.Domain;
using Ants.Domain.State;
using Godot;
using Prototypist.TaskChain;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Intrinsics;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using static Game;
using static Godot.WebSocketPeer;

public partial class Game
{
    public Run run;

    // this should have a constructor
    // some of this stuff is required
    public class Run
    {
        public readonly ConcurrentDictionary<int, RunWith> runWiths = new ConcurrentDictionary<int, RunWith>();
        public readonly ConcurrentDictionary<int, AdvanceResult> advanceResults =new ConcurrentDictionary<int, AdvanceResult>();

        public readonly ConcurrentDictionary<int, Task<TargetState2>> targetStates = new ConcurrentDictionary<int, Task<TargetState2>>();

        // I'm not sure at should really be public
        // or exist
        // check state.Max instead
        // I can't write both at once so there could be a little drift
        //public int at;
        //public CancellationTokenSource cancellationTokenSource;
        //public Map map;
        // could be null when the run is created but before it has started
        public Task Task;

        public readonly Guid id = Guid.NewGuid();


    }

    // this should have a constructor
    // some of this stuff is required
    public class AdvanceResult
    {
        public readonly SimulationState state;
        public readonly ConcurrentDictionary<int, HashSet<Guid>> sideSeesUnit;
        public readonly ConcurrentDictionary<int, HashSet<Guid>> sideSeesUnitFar;
        public readonly ConcurrentDictionary<int, HashSet<Guid>> sideSeesUnitMid;
        public readonly IReadOnlyDictionary<int, HashSet<BulletState.Immutable.Id>> sideSeesBullets;
        public readonly IReadOnlyDictionary<int, HashSet<HealBulletState.Immutable.Id>> sideSeesHealBullets;
        public readonly IReadOnlyDictionary<int, HashSet<ArtyBulletState.Immutable.Id>> sideSeesArtyBullets;
        public readonly IReadOnlyDictionary<int, HashSet<ZapperSetupState.Immutable>> sideSeesZapperFull;
        public readonly IReadOnlyDictionary<int, HashSet<ZapperSetupState.Immutable>> sideSeesZapperMid;
        public readonly IReadOnlyDictionary<int, HashSet<ZapperSetupState.Immutable>> sideSeesZapperFar;
        public readonly IReadOnlyDictionary<int, HashSet<ArcBulletState.Immutable.Id>> sideSeesArcBullets;
        public readonly IReadOnlyDictionary<int, HashSet<ResourceState.Immutable.Id>> sideSeesResources;
        public readonly IReadOnlyDictionary<int, HashSet<Guid>> sideSeesSmoke;

        public AdvanceResult(
            SimulationState state,
            //int frame, 
            ConcurrentDictionary<int, HashSet<Guid>> sideSeesUnit,
            ConcurrentDictionary<int, HashSet<Guid>> sideSeesUnitFar,
            ConcurrentDictionary<int, HashSet<Guid>> sideSeesUnitMid,
            IReadOnlyDictionary<int, HashSet<BulletState.Immutable.Id>> sideSeesBullets,
            IReadOnlyDictionary<int, HashSet<HealBulletState.Immutable.Id>> sideSeesHealBullets,
            IReadOnlyDictionary<int, HashSet<ArtyBulletState.Immutable.Id>> sideSeesArtyBullets,
            IReadOnlyDictionary<int, HashSet<ZapperSetupState.Immutable>> sideSeesZapperFull,
            IReadOnlyDictionary<int, HashSet<ZapperSetupState.Immutable>> sideSeesZapperMid,
            IReadOnlyDictionary<int, HashSet<ZapperSetupState.Immutable>> sideSeesZapperFar,
            IReadOnlyDictionary<int, HashSet<ArcBulletState.Immutable.Id>> sideSeesArcBullets,
            IReadOnlyDictionary<int, HashSet<ResourceState.Immutable.Id>> sideSeesResources,
            IReadOnlyDictionary<int, HashSet<Guid>> sideSeesSmoke)
        {
            this.state = state ?? throw new ArgumentNullException(nameof(state));
            //this.frame = frame;
            this.sideSeesUnit = sideSeesUnit ?? throw new ArgumentNullException(nameof(sideSeesUnit));
            this.sideSeesUnitFar = sideSeesUnitFar ?? throw new ArgumentNullException(nameof(sideSeesUnitFar));
            this.sideSeesUnitMid = sideSeesUnitMid ?? throw new ArgumentNullException(nameof(sideSeesUnitMid));
            this.sideSeesBullets = sideSeesBullets ?? throw new ArgumentNullException(nameof(sideSeesBullets));
            this.sideSeesHealBullets = sideSeesHealBullets ?? throw new ArgumentNullException(nameof(sideSeesHealBullets));
            this.sideSeesArtyBullets = sideSeesArtyBullets ?? throw new ArgumentNullException(nameof(sideSeesArtyBullets));
            this.sideSeesZapperFull = sideSeesZapperFull ?? throw new ArgumentNullException(nameof(sideSeesZapperFull));
            this.sideSeesZapperMid = sideSeesZapperMid ?? throw new ArgumentNullException(nameof(sideSeesZapperMid));
            this.sideSeesZapperFar = sideSeesZapperFar ?? throw new ArgumentNullException(nameof(sideSeesZapperFar));
            this.sideSeesArcBullets = sideSeesArcBullets ?? throw new ArgumentNullException(nameof(sideSeesArcBullets));
            this.sideSeesResources = sideSeesResources ?? throw new ArgumentNullException(nameof(sideSeesResources));
            this.sideSeesSmoke = sideSeesSmoke ?? throw new ArgumentNullException(nameof(sideSeesSmoke));
        }
    }

    public class DisplayState {
        public readonly AdvanceResult advanceResult;
        public readonly int frame;
        public readonly TargetState2 targetState;

        public DisplayState(AdvanceResult advanceResult, int frame, TargetState2 targetState)
        {
            this.advanceResult = advanceResult ?? throw new ArgumentNullException(nameof(advanceResult));
            this.frame = frame;
            this.targetState = targetState ?? throw new ArgumentNullException(nameof(targetState));
        }
    }

    public DisplayState displayState;
    public ConcurrentQueue<EffectEvents> effectEvents = new ConcurrentQueue<EffectEvents>();

    public bool TryGetTargetState(int i, out Task<TargetState2> state) => run.targetStates.TryGetValue(i / TargetState2.runEvery, out state);
    public bool TryGetState(int i, out SimulationState state) { 
        if (run.advanceResults.TryGetValue(i, out var display)) {
            state = display.state;
            return true;
        }
        state = default;
        return false;
    }
    public bool TryGetRunWith(int i, out RunWith runWith) => run.runWiths.TryGetValue(i, out runWith);

    internal void RunFromFrame(int startAtFrame)
    {
        //Log.WriteLine($"RunFromFrame({startAtFrame})");
        Run myRun;

        while (true)
        {
            var local = run;

            // we don't need to roll back, if we are already behind the frame they are asking for

            
            if (local != null)
            {
                // if the current run doesn't yet have a RunWith for the frame we are proposing to start on
                // drop the run
                // this used to use runWiths
                // but that didn't play nice if the runwith for `startAtFrame + 1`
                // was partily calcuated, calcuated enought that they aren't going to match
                // but not actually set yet
                if (!local.advanceResults.TryGetValue(startAtFrame, out var _))
                {
                    //Log.WriteLine($"rollback from frame {startAtFrame} dropped, frame not yet reached");
                    return;
                }
                var proposedRollback = GetInputsSubmittedOnFrame(startAtFrame);
                if (local.runWiths.TryGetValue(startAtFrame + 1, out var localRunWith) && localRunWith.Equals(proposedRollback)) {
                    //Log.WriteLine($"rollback from frame {startAtFrame} dropped, runWith matches");
                    return;
                }
            }


            myRun = new Run();


            //var maxHashed = frameHashes[index].Keys.Any() ? frameHashes[index].Keys.Max() : 0; // Math.Min(startAtFrame, frameHashes[index].Keys.Max());

            // we need to copy everything over before we CompareExchange
            // that way we don't end up with a run with no states
            foreach (var (key, value) in local.advanceResults)
            {
                // I might not need key >= maxHashed
                // and state clean up
                // {33D32D6A-4B16-41F0-A6EF-8FAA176EB5FB}
                // this is probably a littl efficient
                // ...
                // I think I need to remove this
                // or I should atleast do the min instead of the max
                // we might still need the states for frame comparision
                if (key <= startAtFrame /*&& key >= maxHashed*/)
                {
                    myRun.advanceResults[key] = value;
                }
            }

            foreach (var (key, value) in local.runWiths)
            {
                if (key <= startAtFrame)
                {
                    myRun.runWiths[key] = value;
                }
            }

            // {8CF8548A-BE07-4F36-AEC0-849C478458BB}
            // target states are added before advance results
            // so we know if we have a advance results we also have target state
            //
            // generated from frame 0 is used from frame 10 to 20
            // keep anything generated from a frame <= startAtFrame
            foreach (var (key, value) in local.targetStates)
            {
                // inverse of {237E696F-D35C-44CE-816E-1C99827D325B}
                var generatedFrom = (key * TargetState.runEvery) - TargetState.runEvery;
                if (generatedFrom <= startAtFrame)
                {
                    myRun.targetStates[key] = value;
                }
            }

            //foreach (var (key, value) in local.sideSeeUnits)
            //{
            //    if (key <= startAtFrame)
            //    {
            //        myRun.sideSeeUnits[key] = value;
            //    }
            //}

            //foreach (var (key, value) in local.sideSeeUnitsFar)
            //{
            //    if (key <= startAtFrame)
            //    {
            //        myRun.sideSeeUnitsFar[key] = value;
            //    }
            //}


            //foreach (var (key, value) in local.sideSeeUnitsMid)
            //{
            //    if (key <= startAtFrame)
            //    {
            //        myRun.sideSeeUnitsMid[key] = value;
            //    }
            //}

            //foreach (var (key, value) in local.sideSeesBullets)
            //{
            //    if (key <= startAtFrame)
            //    {
            //        myRun.sideSeesBullets[key] = value;
            //    }
            //}

            //foreach (var (key, value) in local.sideSeesHealBullets)
            //{
            //    if (key <= startAtFrame)
            //    {
            //        myRun.sideSeesHealBullets[key] = value;
            //    }
            //}

            //foreach (var (key, value) in local.sideSeesArtyBullets)
            //{
            //    if (key <= startAtFrame)
            //    {
            //        myRun.sideSeesArtyBullets[key] = value;
            //    }
            //}

            if (local == Interlocked.CompareExchange(ref run, myRun, local))
            {
                break;
            }
        }


        Log.WriteLine($"max frame {(myRun.advanceResults.Any() ? myRun.advanceResults.Keys.Max(): 0)}");

        var state = myRun.advanceResults[startAtFrame];
        //myRun.map = Map.FromState(state);
        myRun.Task = StartAsync(myRun, startAtFrame);
    }

    private const int localRollAhead = FramesPerSecond.framesPerSecond / 10;

    private async Task StartAsync(Run myRun, int at)
    {
        try
        {
            while (true)
            {
                while (true)
                {
                    var local = waitingLocalOrders;
                    if (local.frame + localRollAhead > at)
                    {
                        break;
                    }
                    await local.isReady.WaitAsync(cancellationTokenSource.Token).ConfigureAwait(false);
                }

                if (myRun != run)
                {
                    break;
                }
                if (cancellationTokenSource.Token.IsCancellationRequested)
                {
                    return;
                }

                if (at % TargetState.runEvery == 0) {
                    var localAt = at;
                    var localAdvanceResult = myRun.advanceResults[localAt];

                    

                    Log.bugLogger?.WriteLine($"calculating target states {localAt}");
                    // {237E696F-D35C-44CE-816E-1C99827D325B}
                    if (!myRun.targetStates.TryAdd((at + TargetState.runEvery) / TargetState.runEvery, Task.Run(() =>
                    {

                        Stopwatch sw = Stopwatch.StartNew();
                        sw.Start();
                        var antsBySide = localAdvanceResult.state.ants.Values.GroupBy(x => x.immutable.side).ToDictionary(x => x.Key, x => x.ToArray());

                        // this once through this stack trace, I don't know why..

                        // given key '4610' was not present in the dictionary.
                        //  at System.Collections.Concurrent.ConcurrentDictionary`2.ThrowKeyNotFoundException(TKey key)
                        //  at System.Collections.Concurrent.ConcurrentDictionary`2.get_Item(TKey key)
                        //  at Game.<> c__DisplayClass56_1.< StartAsync > b__0() in C: \Users\Colin_000\source\repos\Ants\godot_ants\Ants\Game Loop Code\Game.Run.cs:line 192
                        //  at System.Threading.Tasks.Task`1.InnerInvoke()
                        //  at System.Threading.ExecutionContext.RunFromThreadPoolDispatchLoop(Thread threadPoolThread, ExecutionContext executionContext, ContextCallback callback, Object state)
                        //   - End of stack trace from previous location ---
                        //  at System.Threading.ExecutionContext.RunFromThreadPoolDispatchLoop(Thread threadPoolThread, ExecutionContext executionContext, ContextCallback callback, Object state)
                        //  at System.Threading.Tasks.Task.ExecuteWithThreadLocal(Task & currentTaskSlot, Thread threadPoolThread)
                        //   - End of stack trace from previous location ---
                        //  at Game.StartAsync(Run myRun, Int32 at) in C: \Users\Colin_000\source\repos\Ants\godot_ants\Ants\Game Loop Code\Game.Run.cs:line 223
                        var res = TargetStateFromState.FromState(
                            localAdvanceResult.state, 
                            localAdvanceResult.sideSeesUnit, 
                            localAdvanceResult.sideSeesUnitMid, 
                            immutableSimulationState, 
                            localAt, 
                            antsBySide,
                            localAdvanceResult.sideSeesZapperFull,
                            localAdvanceResult.sideSeesZapperMid);
                        //Log.WriteLine($"generated targeting state for {localAt} - hash: {res.Hash()}");

                        sw.Stop();
                        DebugRunTimes.Add("TargetStateFromState.FromState", sw.ElapsedTicks, frame: localAt, runId: myRun.id);

                        return res;
                    }))) {
                        Log.WriteLine($"{(at + TargetState.runEvery) / TargetState.runEvery} already running");
                    }
                }
                var lastResult = myRun.advanceResults[at];
                var lastState = lastResult.state;
                var runWith = GetInputsSubmittedOnFrame(at);
                at++;
                myRun.runWiths[at] = runWith;

                Stopwatch stopwatchStateCopy = new Stopwatch();
                stopwatchStateCopy.Start();
                var state = lastState.Copy();
                stopwatchStateCopy.Stop(); 
                DebugRunTimes.Add("state.Copy", stopwatchStateCopy.ElapsedTicks, frame: at);

                // run side sees on the frame behind
                var locatlAt = at; // not sure I need this.. don't think I do... if the assert below never throws, that I don't
                var sideSeesTask = Task.Run(() => {
                    Debug.Assert(locatlAt == at);
                    return GetSideSees(lastState, locatlAt);
                    });
                var dontWait = sideSeesTask.ContinueWith(x =>
                {
                    if (x.IsFaulted)
                    {
                        Log.WriteLine($"sideSees failed {x.Exception}");
                    }
                });


                var gotState = TryGetTargetState(at, out var targetStateTask);
                Debug.Assert(gotState);

                // {8CF8548A-BE07-4F36-AEC0-849C478458BB}
                // target states are added before advance results
                // so we know if we have a advance results we also have target state
                if (!targetStateTask.IsCompleted)
                {
                    var sw = Stopwatch.StartNew();
                    sw.Start();
                    await targetStateTask.ConfigureAwait(false);
                    sw.Stop();
                    Log.WriteLine($"waiting on target state {sw.ElapsedMilliseconds}");
                }

                await RunFrame(myRun, at, await targetStateTask.ConfigureAwait(false), runWith, state, sideSeesTask, numberOfPlayers, lastResult.sideSeesUnit, lastResult.sideSeesUnitMid, lastResult.sideSeesResources).ConfigureAwait(false);

                // if we're the active run
                // update the train paths
                // TODO
                // commenting out because it doesn't work :'(
                //if (myRun == run) {
                //    foreach (var item in trainPathingHolder.Values)
                //    {
                //        item.Update(myRun.states[at].Stations.Select(x => x.position).ToHashSet());
                //    }
                //}
            }
        }
        catch (Exception e)
        {
            Log.WriteLine(e.Message);
            Log.WriteLine(e.StackTrace);
        }
    }

    private class SideSees 
    {
        public readonly ConcurrentDictionary<int, HashSet<Guid>> sideSeesUnit;
        public readonly ConcurrentDictionary<int, HashSet<Guid>> sideSeesMid;
        public readonly ConcurrentDictionary<int, HashSet<Guid>> sideSeesUnitFar;
        public readonly IReadOnlyDictionary<int, HashSet<BulletState.Immutable.Id>> sideSeesBullets;
        public readonly IReadOnlyDictionary<int, HashSet<HealBulletState.Immutable.Id>> sideSeesHealBullets;
        public readonly IReadOnlyDictionary<int, HashSet<ArtyBulletState.Immutable.Id>> sideSeesArtyBullets;
        public readonly IReadOnlyDictionary<int, HashSet<ZapperSetupState.Immutable>> sideSeesZapperFar;
        public readonly IReadOnlyDictionary<int, HashSet<ZapperSetupState.Immutable>> sideSeesZapperMid;
        public readonly IReadOnlyDictionary<int, HashSet<ZapperSetupState.Immutable>> sideSeesZapperFull;
        public readonly IReadOnlyDictionary<int, HashSet<ArcBulletState.Immutable.Id>> sideSeesArcBullets;
        public readonly IReadOnlyDictionary<int, HashSet<ResourceState.Immutable.Id>> sideSeesResources;
        public readonly IReadOnlyDictionary<int, HashSet<Guid>> sideSeesSmoke;

        public SideSees(
            ConcurrentDictionary<int, HashSet<Guid>> sideSeesUnit, 
            ConcurrentDictionary<int, HashSet<Guid>> sideSeesMid,
            ConcurrentDictionary<int, HashSet<Guid>> sideSeesUnitFar,
            IReadOnlyDictionary<int, HashSet<BulletState.Immutable.Id>> sideSeesBullets,
            IReadOnlyDictionary<int, HashSet<HealBulletState.Immutable.Id>> sideSeesHealBullets,
            IReadOnlyDictionary<int, HashSet<ArtyBulletState.Immutable.Id>> sideSeesArtyBullets,
            IReadOnlyDictionary<int, HashSet<ZapperSetupState.Immutable>> sideSeesZapperFull,
            IReadOnlyDictionary<int, HashSet<ZapperSetupState.Immutable>> sideSeesZapperMid,
            IReadOnlyDictionary<int, HashSet<ZapperSetupState.Immutable>> sideSeesZapperFar,
            IReadOnlyDictionary<int, HashSet<ArcBulletState.Immutable.Id>> sideSeesArcBullets,
            IReadOnlyDictionary<int, HashSet<ResourceState.Immutable.Id>> sideSeesResources,
            IReadOnlyDictionary<int, HashSet<Guid>> sideSeesSmoke)
        {
            this.sideSeesUnit = sideSeesUnit ?? throw new ArgumentNullException(nameof(sideSeesUnit));
            this.sideSeesMid = sideSeesMid ?? throw new ArgumentNullException(nameof(sideSeesMid));
            this.sideSeesUnitFar = sideSeesUnitFar ?? throw new ArgumentNullException(nameof(sideSeesUnitFar));
            this.sideSeesBullets = sideSeesBullets ?? throw new ArgumentNullException(nameof(sideSeesBullets));
            this.sideSeesHealBullets = sideSeesHealBullets ?? throw new ArgumentNullException(nameof(sideSeesHealBullets));
            this.sideSeesArtyBullets = sideSeesArtyBullets ?? throw new ArgumentNullException(nameof(sideSeesArtyBullets));
            this.sideSeesZapperFull = sideSeesZapperFull ?? throw new ArgumentNullException(nameof(sideSeesZapperFull));
            this.sideSeesZapperMid = sideSeesZapperMid ?? throw new ArgumentNullException(nameof(sideSeesZapperMid));
            this.sideSeesZapperFar = sideSeesZapperFar ?? throw new ArgumentNullException(nameof(sideSeesZapperFar));
            this.sideSeesArcBullets = sideSeesArcBullets ?? throw new ArgumentNullException(nameof(sideSeesArcBullets));
            this.sideSeesResources = sideSeesResources ?? throw new ArgumentNullException(nameof(sideSeesResources));
            this.sideSeesSmoke = sideSeesSmoke ?? throw new ArgumentNullException(nameof(sideSeesSmoke));
        }
    }

    private SideSees GetSideSees(SimulationState state, int tick)
    {
        var avoid = Avoid(immutableSimulationState, state);

        Stopwatch stopwatchSideSeesUnits = new Stopwatch();
        stopwatchSideSeesUnits.Start();
        var (sideSeesUnits, sideSessUnitsMid, sideSeesUnitsFar) = StateAdvancer.SideSeesUnits(state, avoid, tick);
        stopwatchSideSeesUnits.Stop();
        DebugRunTimes.Add("SideSeesUnits",  stopwatchSideSeesUnits.ElapsedTicks, frame:tick);

        Stopwatch stopwatchSideSeesBullet = new Stopwatch();
        stopwatchSideSeesBullet.Start();
        var sideSeesBullets = StateAdvancer.SideSeesBullet(state, avoid);
        stopwatchSideSeesBullet.Stop();
        DebugRunTimes.Add("SideSeesBullets", stopwatchSideSeesBullet.ElapsedTicks, frame: tick);

        Stopwatch stopwatchSideSeesHealBullet = new Stopwatch();
        stopwatchSideSeesHealBullet.Start();
        var sideSeesHealBullets = StateAdvancer.SideSeesHealBullet(state, avoid);
        stopwatchSideSeesHealBullet.Stop();
        DebugRunTimes.Add("SideSeesHealBullets", stopwatchSideSeesHealBullet.ElapsedTicks, frame: tick);

        Stopwatch stopwatchSideArtyHealBullet = new Stopwatch();
        stopwatchSideArtyHealBullet.Start();
        var sideSeesArtyBullets = StateAdvancer.SideSeesArtyBullet(state, avoid);
        stopwatchSideArtyHealBullet.Stop();
        DebugRunTimes.Add("SideSeesArtyBullet", stopwatchSideArtyHealBullet.ElapsedTicks, frame: tick);

        Stopwatch stopwatchSideSeesZapperSetup = new Stopwatch();
        stopwatchSideSeesZapperSetup.Start();
        var (sideSeesZapperSetUp, sideSessZapperSetUpMid, sideSeesZapperSetUpFar) = StateAdvancer.SideSeesZapperSetup(state, avoid, tick);
        stopwatchSideSeesZapperSetup.Stop();
        DebugRunTimes.Add("stopwatchSideSeesZapperSetup", stopwatchSideSeesZapperSetup.ElapsedTicks, frame: tick);

        Stopwatch stopwatchSideSeesArcBullet = new Stopwatch();
        stopwatchSideSeesArcBullet.Start();
        var sideSeeArcBullet = StateAdvancer.SideSeesArcBullet(state, avoid);
        stopwatchSideSeesArcBullet.Stop();
        DebugRunTimes.Add("stopwatchSideSeesArcBullet", stopwatchSideSeesArcBullet.ElapsedTicks, frame: tick);

        Stopwatch stopwatchSideSeesResources = new Stopwatch();
        stopwatchSideSeesResources.Start();
        var sideSeesResources = StateAdvancer.SideSeesResources(state, Avoid(immutableSimulationState, state));
        stopwatchSideSeesResources.Stop();
        DebugRunTimes.Add("stopwatchSideSeesResources", stopwatchSideSeesResources.ElapsedTicks, frame: tick);

        Stopwatch stopwatchSideSeesSmoke = new Stopwatch();
        stopwatchSideSeesSmoke.Start();
        var sideSeesSmoke = StateAdvancer.SideSeesSmoke(state, immutableSimulationState.rocksTuple, state.smokes.Values);
        stopwatchSideSeesSmoke.Stop();
        DebugRunTimes.Add("stopwatchSideSeesSmoke", stopwatchSideSeesSmoke.ElapsedTicks, frame: tick);

        return new SideSees(sideSeesUnits, sideSessUnitsMid, sideSeesUnitsFar, sideSeesBullets, sideSeesHealBullets, sideSeesArtyBullets, sideSeesZapperSetUp, sideSessZapperSetUpMid, sideSeesZapperSetUpFar, sideSeeArcBullet, sideSeesResources, sideSeesSmoke);
    }

    public static (Vector128<double> position, double radius)[] Avoid(ImmutableSimulationState immutableSimulationState, SimulationState state)
    {
        return immutableSimulationState.rocksTuple.Union(
                    state.smokes.Values.Select(x => (x.position, x.radius))).ToArray();
    }

    private async Task RunFrame(
        Run myRun, 
        int at, 
        TargetState2 targetState, 
        RunWith runWith, 
        SimulationState state, 
        Task<SideSees> sideSeesTask, 
        int players, 
        ConcurrentDictionary<int, HashSet<Guid>> sideSeesUnitFull,
        ConcurrentDictionary<int, HashSet<Guid>> sideSeesUnitMid,
        IReadOnlyDictionary<int, HashSet<ResourceState.Immutable.Id>> sideSeesResources)
    {
        Stopwatch stopwatch = new Stopwatch();
        stopwatch.Start();

        var nextState = new StateAdvancer().Advance(state, at, runWith.Flatten(), targetState, immutableSimulationState, index, Order, sideSeesUnitFull, sideSeesUnitMid, sideSeesResources);

        UpdateMaxFrameSimulated(at);

        //if (nextState == null)
        //{
        //    Log.WriteLine("nextState is null!");
        //}


        if (at % 3600 == 0)
        {
            Log.WriteLine($"reached frame {at}");
        }

        var sideSees = await sideSeesTask.ConfigureAwait(false);

        for (int i = 0; i < players; i++)
        {
            foreach (var ant in nextState.pings)
            {
                if (ant.immutable.side == i) {
                    continue;
                } 
                if (sideSees.sideSeesUnit.TryGetValue(i, out var seen1) && seen1.Contains(ant.immutable.id)) 
                {
                    continue;
                }
                if (sideSees.sideSeesMid.TryGetValue(i, out var seen2) && seen2.Contains(ant.immutable.id))
                {
                    continue;
                }
                if (sideSees.sideSeesUnitFar.TryGetValue(i, out var seen3) && seen3.Contains(ant.immutable.id))
                {
                    continue;
                }

                var set = sideSees.sideSeesUnitFar.GetOrAdd(i, new HashSet<Guid>());
                set.Add(ant.immutable.id);
            }
        }
        
        var myResult = new AdvanceResult(
            nextState.state, 
            sideSees.sideSeesUnit, 
            sideSees.sideSeesUnitFar, 
            sideSees.sideSeesMid, 
            sideSees.sideSeesBullets,
            sideSees.sideSeesHealBullets, 
            sideSees.sideSeesArtyBullets,
            sideSees.sideSeesZapperFull,
            sideSees.sideSeesZapperMid,
            sideSees.sideSeesZapperFar,
            sideSees.sideSeesArcBullets,
            sideSees.sideSeesResources,
            sideSees.sideSeesSmoke);
        myRun.advanceResults[at] = myResult;

        // TODO shouldn't this be in clean up? it's not becuase it on the run level
        CleanUpOldState(myRun);

        var bag = effectsAwaitingConfirmation.GetOrAdd(at, new ConcurrentDictionary<int, EffectEventRequireVerification>());
        if (!bag.TryAdd(nextState.state.Hash(), nextState.eventsRequiringVerification))
        {
            Log.WriteLine("that probably shouldn't happen");
            // I guess hashes could collide...
        }

        // this isn't perfect
        // a run could be the run when you check it is
        // and not be by the time it writes
        // but since we get a copy of displayState
        // it won't write over anyone who between 
        // between it's check and it's write
        // in other words it won't write over the real run
        var toAdd = new DisplayState(myResult, at, targetState);
        while (true)
        {
            var local = displayState;

            if (displayState.frame > toAdd.frame || run != myRun)
            {
                break;
            }
            
            if (local == Interlocked.CompareExchange(ref displayState, toAdd, local))
            {
                effectEvents.Enqueue(nextState.events);
                break;
            }
        }


        stopwatch.Stop();

        DebugRunTimes.Add("RunFrame", stopwatch.ElapsedTicks, frame: at, runId: myRun.id);
    }

    // this is really only needed in local games
    // or if we never have to rollback
    // like if you are play vs a AFK player
    // {33D32D6A-4B16-41F0-A6EF-8FAA176EB5FB}
    internal void CleanUpOldState(Run myrun)
    {

        // we need to hold on to states until we are done with their hashes
        // we might need to print them out if the hashes don't match
        // it prints the frame that didn't match and the frame before it
        // thus the - 1
        // here: {650B0574-04BF-4D94-AFAF-ACA8740ECF28}
        var line = frameHashes[index].Keys.Any() ? frameHashes[index].Keys.Min() -1 : 0;

        // < because we might need the state at line
        //var toRemoves = myrun.states.Keys.Where(x => x < line).ToArray();

        //foreach (var toRemove in toRemoves)
        //{
        //    if (!myrun.states.TryRemove(toRemove, out var _))
        //    {
        //        Log.bugLogger?.WriteLine($"failed to remove state {toRemove}");
        //    }
        //    else {
        //        Log.bugLogger?.WriteLine($"delete state {string.Join(", ",toRemove)}, line: {line}");
        //    }

        //    if (!myrun.sideSeeUnits.TryRemove(toRemove, out var _))
        //    {
        //        Log.bugLogger?.WriteLine($"failed to remove sideSeeUnits {toRemove}");
        //    }
        //    else
        //    {
        //        Log.bugLogger?.WriteLine($"delete sideSeeUnits {string.Join(", ", toRemove)}, line:  {line}");
        //    }

        //    if (!myrun.sideSeeUnitsMid.TryRemove(toRemove, out var _))
        //    {
        //        Log.bugLogger?.WriteLine($"failed to remove sideSeeUnitsMid {toRemove}");
        //    }
        //    else
        //    {
        //        Log.bugLogger?.WriteLine($"delete sideSeeUnitsMid {string.Join(", ", toRemove)}, line:  {line}");
        //    }

        //    if (!myrun.sideSeesBullets.TryRemove(toRemove, out var _))
        //    {
        //        Log.bugLogger?.WriteLine($"failed to remove sideSeesBullets {toRemove}");
        //    }
        //    else
        //    {
        //        Log.bugLogger?.WriteLine($"delete sideSeesBullets {string.Join(", ", toRemove)}, line:  {line}");
        //    }

        //    if (!myrun.sideSeesHealBullets.TryRemove(toRemove, out var _))
        //    {
        //        Log.bugLogger?.WriteLine($"failed to remove sideSeesHealBullets {toRemove}");
        //    }
        //    else
        //    {
        //        Log.bugLogger?.WriteLine($"delete sideSeesHealBullets {string.Join(", ", toRemove)}, line:  {line}");
        //    }


        //    if (!myrun.sideSeesArtyBullets.TryRemove(toRemove, out var _))
        //    {
        //        Log.bugLogger?.WriteLine($"failed to remove sideSeesArtyBullets {toRemove}");
        //    }
        //    else
        //    {
        //        Log.bugLogger?.WriteLine($"delete sideSeesArtyBullets {string.Join(", ", toRemove)}, line:  {line}");
        //    }
        //}

        foreach (var toRemove in myrun.runWiths.Keys.Where(x=>x < line))
        {
            if (!myrun.runWiths.TryRemove(toRemove, out var _))
            {
                Log.bugLogger?.WriteLine($"failed to remove runWiths {toRemove}");
            }
            else
            {
                Log.bugLogger?.WriteLine($"delete runWiths {string.Join(", ", toRemove)}, line:  {line}");
            }
        }

        var toRemovesFar = myrun.advanceResults.Keys.Where(x => x < line - StateAdvancer.PingLastsFor).ToArray();

        foreach (var toRemove in toRemovesFar)
        {

            if (!myrun.advanceResults.TryRemove(toRemove, out var _))
            {
                Log.bugLogger?.WriteLine($"failed to remove sideSeeUnitsFar {toRemove}");
            }
            else
            {
                Log.bugLogger?.WriteLine($"delete sideSeeUnitsFar {string.Join(", ", toRemove)}, line:  {line}");
            }
        }

        // {237E696F-D35C-44CE-816E-1C99827D325B}
        var toRemovesTargetStates = myrun.targetStates.Keys.Where(x => x < (line - TargetState.runEvery) / TargetState.runEvery).ToArray();

        foreach (var toRemove in toRemovesTargetStates)
        {

            if (!myrun.targetStates.TryRemove(toRemove, out var _))
            {
                Log.bugLogger?.WriteLine($"failed to remove targetStates {toRemove}");
            }
            else
            {
                Log.bugLogger?.WriteLine($"delete targetStates {string.Join(", ", toRemove)}, line:  {line}");
            }
        }

        if (myrun.advanceResults.Count > 3600) 
        {
            Log.WriteLine($"you have collected {myrun.advanceResults.Count} states, line: {line}, just removed: {toRemovesFar.Length}, frame hash mins {string.Join(", ", frameHashes.Select(x=> x.Keys.Any() ? x.Keys.Min() : -1))}");
        }
    }
}