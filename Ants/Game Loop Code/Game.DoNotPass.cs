﻿using Ants.Domain;
using Prototypist.TaskChain;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

// don't get too far ahead of other players
// but also don't get too far ahead of your simulation
public partial class Game
{
    public class DoNotPass
    {
        public int frame;
        public SemaphoreSlim taskSource;
    }

    public DoNotPass doNotPassOtherInputs;


    public const int MaxAheadOfOthers = FramesPerSecond.framesPerSecond/2;

    private void UpdateDoNotPassOthers()
    {
        while (true)
        {
            var local = doNotPassOtherInputs;

            // {71252FD7-FE46-4603-99C8-5A54503FE405}
            var maxFrameToSend = MaxAheadOfOthers +
                orders.Select(x => x.Keys.Any() ? x.Keys.Max() : 0).Min();

            if (local.frame < maxFrameToSend)
            {
                if (Interlocked.CompareExchange(ref doNotPassOtherInputs, new DoNotPass
                {
                    taskSource = new SemaphoreSlim(0),
                    frame = maxFrameToSend
                }, local) == local)
                {
                    //Log.WriteLine($"updated wiating frame to {maxFrameToSend}");
                    local.taskSource.Release(int.MaxValue);
                    return;
                }
            }
            else
            {
                return;
            }
        }
    }

    public const int MaxAheadOfSimulation = FramesPerSecond.framesPerSecond/2;
    private class DoNotPassLink
    {
        public readonly SemaphoreSlim taskSource = new SemaphoreSlim(0);
        public readonly int frame;
        public DoNotPassLink next;

        public DoNotPassLink(int frame)
        {
            this.frame = frame;
        }
    }

    private DoNotPassLink doNotPassSimulation = new DoNotPassLink(-1);

    private async Task WaitForSimulation(int at) {
        var local = doNotPassSimulation;
        //var ticks = 0;
        //var sw = new Stopwatch();
        //sw.Start();
        while (local.frame + MaxAheadOfSimulation < at)
        {
          //  ticks++;
            await local.taskSource.WaitAsync(cancellationTokenSource.Token).ConfigureAwait(false);
            Interlocked.CompareExchange(ref doNotPassSimulation, local.next, local);
            local = local.next;
        }
        //Log.WriteLine($"WaitForSimulation, local.frame: {local.frame}, at {at}, ticks {ticks}, elapsed {sw.Elapsed}");
    }

    private void UpdateMaxFrameSimulated(int justSimulated) {

        var local = doNotPassSimulation;
        //var ticks = 0;
        //var actualUpdates = 0;
        while (justSimulated > local.frame)
        {
            //ticks++;
            if (Interlocked.CompareExchange(ref local.next, new DoNotPassLink(local.frame + 1), null) == null)
            {
                //actualUpdates++;
                local.taskSource.Release(int.MaxValue);
            }
            local = local.next;
        }

        //Log.WriteLine($"UpdateMaxFrameSimulated, justSimulated: {justSimulated}, ticks {ticks}, actualUpdates: {actualUpdates}");
    }
}

