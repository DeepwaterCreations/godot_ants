﻿using Ants.Domain.State;
using Ants.Network.Messages;
using Prototypist.TaskChain;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using static Ants.Network.Messages.FrameData;

// this is such a maga class we multiple responsibilities
// 1. it advances the target frame and sends out updates
// 2. it process incoming frames
// 3. it runs the game loop and does rollbacks
//
// these are all tied together
// (1-2) the target frame depends on incoming frames
// (1-3) the game loop waits for the target frame to update
// (2-3) incoming frames generate rollbacks

// I used partial classes to orginiaze a bit


// shared stuff used by all the partial classes
public partial class Game : ITrainPathing
{

    // mabye a readonlylist instead of ConcurrentLinkedList
    /// <summary>
    /// player index to frame number to ordres 
    /// orders collected on frame 0 will be applied in frame 1
    /// the index here is when they were collected
    /// </summary>
    internal readonly ConcurrentDictionary<int, ConcurrentLinkedList<OrderData>>[] orders;

    private readonly int numberOfPlayers;

    /// <summary>
    /// hash = frameHashes[player index][frame]
    /// </summary>
    internal readonly ConcurrentDictionary<int, int>[] frameHashes;
    /// <summary>
    /// ensures a frame hash is written exactly once
    /// we don't want to get a frame hash, delete it and then have it come in again
    /// the other might not come it, so we will never clean up
    /// </summary>
    private readonly RefInt[] frameHashesIndexes;
    private class RefInt {
        public readonly int value;

        public RefInt(int value)
        {
            this.value = value;
        }
    }

    // what player are we
    private readonly int index;
    private readonly ISendData sendData;
    public CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();

    public readonly System.Collections.Generic.Dictionary<(Pathing.Mode, double), TrainPathingHolder> trainPathingHolder;

    public readonly ImmutableSimulationState immutableSimulationState;

    public Game(
        SimulationState startingState, 
        int numberOfPlayers,
        int index, 
        ISendData sendData,
        System.Collections.Generic.Dictionary<(Pathing.Mode, double), TrainPaths> trainPaths,
        ImmutableSimulationState immutableSimulationState)
    {
        this.numberOfPlayers = numberOfPlayers;
        this.acks = new int[numberOfPlayers];
        Array.Fill(acks, -1);
        this.hashAcks = new int[numberOfPlayers];
        Array.Fill(hashAcks, -1);

        if (startingState is null)
        {
            throw new ArgumentNullException(nameof(startingState));
        }
        //this.frameAdvancer = frameAdvancer ?? throw new ArgumentNullException(nameof(frameAdvancer));
        //this.frameProcessor = frameProcessor ?? throw new ArgumentNullException(nameof(frameProcessor));
        this.immutableSimulationState = immutableSimulationState;
        this.index = index;
        this.sendData = sendData;
        this.trainPathingHolder = trainPaths.ToDictionary(x=>x.Key, x=> new TrainPathingHolder(x.Value, startingState.Stations.Select(x => x.position).ToHashSet()));
        orders = new ConcurrentDictionary<int, ConcurrentLinkedList<OrderData>>[this.numberOfPlayers];
        frameHashes = new ConcurrentDictionary<int, int>[this.numberOfPlayers];
        var startingSideSees = new ConcurrentDictionary<int, HashSet<Guid>>();
        var sideSeesFar = new ConcurrentDictionary<int, HashSet<Guid>>();
        var sideSeesMid = new ConcurrentDictionary<int, HashSet<Guid>>();
        frameHashesIndexes = new int[this.numberOfPlayers].Select(x=>new RefInt(-1)).ToArray();
        var startingSideSessBullets = new ConcurrentDictionary<int, HashSet<BulletState.Immutable.Id>>();
        var startingSideSeesHealBullets = new ConcurrentDictionary<int, HashSet<HealBulletState.Immutable.Id>>();
        var startingSideSeesArtyBullets = new ConcurrentDictionary<int, HashSet<ArtyBulletState.Immutable.Id>>();

        var startingsideSeesZapperFull = new Dictionary<int, HashSet<ZapperSetupState.Immutable>>();
        var startingsideSeesZapperMid = new Dictionary<int, HashSet<ZapperSetupState.Immutable>>();
        var startingsideSeesZapperFar = new Dictionary<int, HashSet<ZapperSetupState.Immutable>>();
        var startingsideArcBullets = new Dictionary<int, HashSet<ArcBulletState.Immutable.Id>>();
        var startingSideSeeResources = new Dictionary<int, HashSet<ResourceState.Immutable.Id>>();
        var startingSideSeeSmoke = new Dictionary<int, HashSet<Guid>>();

        var startingTargetState = TargetState2.NothingTargeted(0);

        for (int i = 0; i < this.numberOfPlayers; i++)
        {
            orders[i] = new ConcurrentDictionary<int, ConcurrentLinkedList<OrderData>>();
            frameHashes[i] = new ConcurrentDictionary<int, int>();
            startingSideSees[i] = new HashSet<Guid>();
            sideSeesFar[i] = new HashSet<Guid>();
        }

        var inputs = new ConcurrentLinkedList<FrameData.OrderData>[this.numberOfPlayers];
        for (int i = 0; i < this.numberOfPlayers; i++)
        {
            inputs = new ConcurrentLinkedList<OrderData>[i];
        }
        var startingRunWith = new RunWith(inputs);

        var advanceReuslt = new AdvanceResult(
            startingState, 
            startingSideSees, 
            sideSeesFar, 
            sideSeesMid, 
            startingSideSessBullets, 
            startingSideSeesHealBullets, 
            startingSideSeesArtyBullets, 
            startingsideSeesZapperFull, 
            startingsideSeesZapperMid, 
            startingsideSeesZapperFar,
            startingsideArcBullets,
            startingSideSeeResources,
            startingSideSeeSmoke);

        displayState = new DisplayState(advanceReuslt, 0, startingTargetState);

        Log.WriteLine($"running");

        run = new Run();
        run.advanceResults.TryAdd(0, advanceReuslt);
        run.targetStates.TryAdd(0, Task.FromResult(startingTargetState));
        run.runWiths.TryAdd(0, startingRunWith);
    }

    public (Task frameProcessor, Task frameAdvancer, Task cleanUp, Task losWorker, Task monitor) Start(long startingAtTicksUtc, LosWorkerGroup losWorkerGroup)
    {
        Debug.Assert(run.Task == null);

        waitingLocalOrders = new WaitingFrame(0, new SemaphoreSlim(0));
        doNotPassOtherInputs = new DoNotPass { frame = MaxAheadOfOthers, taskSource = new SemaphoreSlim(0)};
        readyToCleanUp = new SemaphoreSlim(0,1);

        // I would like these to run at a hight priority
        // but... I have a lot of async
        // 
        //new Thread(() =>
        //{
        //})
        //{
        //    Priority = ThreadPriority.Highest
        //}.Start();

        // shouldn't some of these have 
        var res = (
            Task.Run(async () => await PrcocessFrames().ConfigureAwait(false)),
            Task.Run(async () => await MainLoop(startingAtTicksUtc, sendData).ConfigureAwait(false)),
            Task.Run(async () => await CleanUp().ConfigureAwait(false)),
            Task.Run(async () => await losWorkerGroup.Process().ContinueWith(x=> {
                if (x.IsFaulted) {
                    Log.WriteLine(x.Exception.ToString());
                }
            }).ConfigureAwait(false)),
            Task.Run(async () => await Monitor().ConfigureAwait(false))
        );
        run.Task = Task.Run(async () => await StartAsync(run, 0).ConfigureAwait(false));
        Log.WriteLine($"Start {Thread.CurrentThread.ManagedThreadId}");
        return res;
    }

    /// <summary>
    /// remember inputs are submitted a frame before they are processed
    /// that code is here: {5C28EBB2-C7F6-41D3-A252-1C126AB29A93}
    /// if you want to see what inputs frame 4 ran with call GetInputsSubmittedOnFrame(3)
    /// </summary>
    internal RunWith GetInputsSubmittedOnFrame(int at)
    {
        var ordersList = new ConcurrentLinkedList<OrderData>[orders.Length];

        for (int i = 0; i < orders.Length; i++)
        {
            // no orders submitted on frame -1
            // it's reasonable for this to get called with at == -1
            // they just want the order used from frame 0
            if (at == -1)
            {
                ordersList[i] = new ConcurrentLinkedList<OrderData>();
            }
            else  if (orders[i].TryGetValue(at, out var ordersAt))
            {
                ordersList[i] = ordersAt;
            }
            else
            {
                ordersList[i] = null;
            }
        }

        return new RunWith(ordersList);
    }

    public TrainPaths TrainPathingHolder(Pathing.Mode mode, double radius)
    {
        return trainPathingHolder[(mode, radius)].value.value;
    }
}