﻿using Ants.AppState;
using Ants.Domain;
using Ants.Domain.State;
using Ants.Network.Messages;
using Godot;
using Prototypist.TaskChain;
using Prototypist.Toolbox.Object;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Channels;
using System.Threading.Tasks;
using static Ants.Network.Messages.FrameData;
using static Game;

public interface ISendData
{
    Task Send(WaitingFrame waitingFrame);
}

public class SendData : ISendData
{
    public async Task Send(WaitingFrame waitingFrame)
    {
        if (StaticAppState.TryGetState(out InGameState inGameState))
        {
            // this is not awaited, but the thread might go in there anyway... 
            // maybe I should force seding onto it's own task
            // and channel stuf over

            // Log.WriteLine($"seding data for frame {waitingFrame.frame}");
            // {5C28EBB2-C7F6-41D3-A252-1C126AB29A93}
            await inGameState.SendFrameDataAsync(waitingFrame.frame - 1);

            // your inputs can get ahead of your simulation...
            // probably that's not good for you
            // but you don't slow everyone else down
            // you will see you're own leg
            // you inputs willn't get applied until the simulation reaches the time they were entered
            //}
        }
        else
        {
            Log.WriteLine($"not in in game state {Thread.CurrentThread.ManagedThreadId}");
        }
    }
}


public partial class Game
{
    public class WaitingFrame
    {
        public readonly int frame;
        public readonly SemaphoreSlim isReady;

        public WaitingFrame(int frame, SemaphoreSlim isReady)
        {
            this.frame = frame;
            this.isReady = isReady ?? throw new ArgumentNullException(nameof(isReady));
        }
    }

    public WaitingFrame waitingLocalOrders;

    ConcurrentLinkedList<OrderData> orderQueue = new ConcurrentLinkedList<OrderData>();


    public async Task Monitor() {
        try
        {
            var lastWaitingLocalOrders = waitingLocalOrders;
            var lastDoNotPassOtherInputs = doNotPassOtherInputs;
            var lastDoNotPassSimulation = doNotPassSimulation;
            while (!cancellationTokenSource.Token.IsCancellationRequested)
            {
                await Task.Delay(TimeSpan.FromSeconds(10));

                var nextWaitingLocalOrders = waitingLocalOrders;
                var nextDoNotPassOtherInputs = doNotPassOtherInputs;
                var nextCurrent = doNotPassSimulation;

                if (lastWaitingLocalOrders == nextWaitingLocalOrders)
                {
                    Log.WriteLine($"{nameof(waitingLocalOrders)} hasn't chagned in 10 seconds. frame:{lastWaitingLocalOrders.frame}");
                }

                if (lastDoNotPassOtherInputs == nextDoNotPassOtherInputs)
                {
                    Log.WriteLine($"{nameof(doNotPassOtherInputs)} hasn't chagned in 10 seconds. frame:{lastDoNotPassOtherInputs.frame}, order indexs: {string.Join(", ", orders.Select(x => (x.Keys.Any() ? x.Keys.Max() : -1)))}");
                }

                if (lastDoNotPassSimulation == nextCurrent)
                {
                    Log.WriteLine($"{nameof(doNotPassSimulation)} hasn't chagned in 10 seconds. frame:{lastDoNotPassSimulation.frame}");
                }

                lastWaitingLocalOrders = waitingLocalOrders;
                lastDoNotPassOtherInputs = doNotPassOtherInputs;
                lastDoNotPassSimulation = doNotPassSimulation;
            }
        }
        catch (Exception ex) {

            Log.WriteLine(ex.ToString());
        }
     } 

    // this is probably it's own class really
    // or maybe it's part of game state
    // that would make sense
    public async Task MainLoop(long startAtUtc, ISendData sendData)
    {
        try
        {
            var lastSend = DateTime.UtcNow;
            var startAt = new DateTime(startAtUtc);

            while (!cancellationTokenSource.Token.IsCancellationRequested)
            {
                var state = StaticAppState.GetAppState();
                if (state.SafeIs(out InGameState _))
                {
                    break;
                }
                await state.StateDisposed;
            }

            var i = 0;

            while (!cancellationTokenSource.Token.IsCancellationRequested)
            {
                i++;

                var frameCompleteIn = (int)((startAt + TimeSpan.FromSeconds(waitingLocalOrders.frame / (double)FramesPerSecond.framesPerSecond)) - DateTime.UtcNow).TotalMilliseconds;

                if (frameCompleteIn < -1000 && i % FramesPerSecond.framesPerSecond * 5 == 0)
                {
                    Log.WriteLine($"running {-frameCompleteIn} ms behind");
                }

                // past due && not too far ahead of the inputs from other players && not too far ahead of the simulation
                await Task.WhenAll(new[] {
                    Task.WhenAny(new []{
                        // TODO when you have any input - let's send right away when we have orders {86E2A23C-08F0-4979-9CE3-6D37B7743C8E}
                        frameCompleteIn > 0 ? Task.Delay(frameCompleteIn, cancellationTokenSource.Token) : Task.CompletedTask,
                    }),
                    WaitForInputs(waitingLocalOrders.frame), // I think this never hits
                    WaitForSimulation(waitingLocalOrders.frame)
                }).ConfigureAwait(false);


                var last = waitingLocalOrders;
                LockInOrders(last.frame);
                waitingLocalOrders = new WaitingFrame(last.frame + 1, new SemaphoreSlim(0));
                last.isReady.Release(int.MaxValue);

                lastSend = DateTime.UtcNow;

                // TODO you are here
                // we don't actually send because we aren't in the correct state
                // and then because we didn't send everyone waits 
                // we shouldn't be in here if we are in the wrong state
                // check if the state is correct, and if it's not await something  
                await sendData.Send(waitingLocalOrders);

                ReleaseCleanUp();
            }
        }
        catch (Exception e)
        {
            Log.WriteLine(e.Message);
            Log.WriteLine(e.StackTrace);
        }
    }

    public async Task WaitForInputs(int frame)
    {
        while (true)
        {
            var local = doNotPassOtherInputs;
            //Log.WriteLine($"WaitForInputs. frame: {frame}, local.frame: {local.frame}");
            if (frame < local.frame)
            {
                return;
            }
            await local.taskSource.WaitAsync(cancellationTokenSource.Token).ConfigureAwait(false);
        }
    }

    // this shouldn't be called multiple times at once
    // they would both try to pull all the orders
    // then both run short
    // plus this isn't threadsafe
    // {61072579-FC5C-42FE-A8E2-9476B6B9C69A}
    public void LockInOrders(int frame)
    {
        //Log.WriteLine("LockInOrders");
        var toTake = orderQueue.Count;
        var list = new ConcurrentLinkedList<OrderData>();

        for (int i = 0; i < toTake; i++)
        {
            if (!orderQueue.RemoveStart(out var order))
            {
                Log.WriteLine("I should have been able to remove the start");
            }
            list.Add(order);
        }

        if (!orders[index].TryAdd(frame, list))
        {
            Log.WriteLine("I should have been able to add that");
        }

        RunFromFrame(frame);

        // ack yourself
        // {61072579-FC5C-42FE-A8E2-9476B6B9C69A}
        Debug.Assert(acks[index] < frame);
        acks[index] = frame;

        UpdateDoNotPassOthers();
    }

    internal void QueueOrder(Guid id, IPendingOrderState order)
    {
        orderQueue.Add(new OrderData(id, order, true));
    }

    internal void Order(Guid id, IPendingOrderState order)
    {

        orderQueue.Add(new OrderData(id, order, false));
    }

    internal void Order(Guid id, IPendingOrderState order, bool enqueue)
    {

        orderQueue.Add(new OrderData(id, order, enqueue));
    }

    private bool printed = false;
    // effectsAwaitingConfirmation[frame][hash]
    private readonly ConcurrentDictionary<int, ConcurrentDictionary<int, EffectEventRequireVerification>> effectsAwaitingConfirmation = new ConcurrentDictionary<int, ConcurrentDictionary<int, EffectEventRequireVerification>>();
    public readonly ConcurrentQueue<EffectEventRequireVerification> confirmedEffects = new ConcurrentQueue<EffectEventRequireVerification>();

    private void WriteState(int myFrame, int player)
    {
        //Debugger.Launch();
        Log.WriteLine($"state hashes don't match for frame {myFrame} with player {player}");

        try
        {
            if (StaticAppState.TryGetState(out NetworkedAppState networkedAppState))
            {

                // {650B0574-04BF-4D94-AFAF-ACA8740ECF28}
                // it is useful to have the frame that leed up to the mismatching frames
                var gotPreState = TryGetState(myFrame - 1, out var preState);
                Debug.Assert(gotPreState);
                var gotState = TryGetState(myFrame, out var state);
                Debug.Assert(gotState);
                var gotPreTargetState = TryGetTargetState(myFrame - 1, out var preTargetState);
                Debug.Assert(gotPreTargetState);
                var gotTargetState = TryGetTargetState(myFrame, out var targetState);
                Debug.Assert(gotTargetState);

                var preComp = new CompositSimulationState(preState, preTargetState.Result);
                var comp = new CompositSimulationState(state, targetState.Result);

                Log.bugLogger2.WriteLine($"writing hash, lastState: {preComp.Hash()}, state {comp.Hash()}");
                if (!printed)
                {
                    // the orders used to get there
                    TryGetRunWith(myFrame, out var runWith);

                    File.WriteAllLines($"../{Log.Logs}/frame-{networkedAppState.networker.port}.state", new[] {
                    "last State------------",
                    preComp.ToDebugString(),
                    "runWith--------------",
                    runWith?.ToDebugString() ?? string.Empty,
                    "state----------------",
                    comp.ToDebugString() });

                    printed = true;
                }
            }
            else
            {
                Log.WriteLine($"but I couldn't print it out");
            }
        }
        catch (Exception e)
        {
            Log.WriteLine(e.ToString());
        }
    }
}