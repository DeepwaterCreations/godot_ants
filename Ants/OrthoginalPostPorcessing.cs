using Godot;
using System;

public partial class OrthoginalPostPorcessing : MeshInstance3D
{
	[Export]
	public Camera3D follows;
	
	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
	}

	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(double delta)
	{
		
		
		var windowSize = DisplayServer.WindowGetSize();
		if (follows.KeepAspect == Camera3D.KeepAspectEnum.Height)
		{
			Scale = new Vector3(follows.Size * windowSize.X / windowSize.Y, follows.Size, Scale.Z);
		}
		else
		{
			Scale = new Vector3(follows.Size, follows.Size * windowSize.Y / windowSize.X, Scale.Z);
		}
		// you don't need this
		// if you think you do, the scene is probably be rendered by a different camera than you think 
		//GlobalPosition = new Vector3(follows.GlobalPosition.X,follows.GlobalPosition.Y,follows.GlobalPosition.Z -20);
		
		
		//(MaterialOverlay as ShaderMaterial).SetShaderParameter("size", follows.Size);
		//(MaterialOverride as ShaderMaterial).SetShaderParameter("size", follows.Size);
		//(GetActiveMaterial(0) as ShaderMaterial).SetShaderParameter("size", .3);//follows.Size);
		//(GetActiveMaterial(1) as ShaderMaterial).SetShaderParameter("size", .7);//follows.Size);


	}
}
