using Ants.AppState;
using Ants.Network.Messages;
using Godot;
using System;
using System.Threading;
using System.Threading.Tasks;

public partial class Match : Node
{
	public override void _Process(double delta)
	{
		StaticAppState.ProcessOrNaviage(GetTree(), delta);
	}
}
