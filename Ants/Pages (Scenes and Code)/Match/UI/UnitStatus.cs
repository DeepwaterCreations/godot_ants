using Ants.AppState;
using Godot;
using System.Linq;

public partial class UnitStatus : Label
{
	[Export]
	public StateToScene StateToScene { get; set; }

	public override void _Process(double delta)
	{
		if (StaticAppState.TryGetState(out InGameState state))
		{
			var selectedAnts = StateToScene.GetSelectedAnts(state.Game.displayState.advanceResult.state)
				.OrderBy(x => x.immutable.id)
				.Select(ant =>
$@"
type --------------- {ant.immutable.type}
hp                   {ant.hp}
sprint energy ------ {ant.sprint?.energy:f2}
shockwave energy     {ant.shockwaverState?.energy:f2}
grenade reload ----- {ant.grenadeLauncher?.reloadDue:f2}
zapper energy        {ant.zapperState?.energy:f2}");

			this.Text = string.Join("", selectedAnts);
		}
	}
}
