using Godot;
using System;
using System.Threading;

public partial class FpsCounter : Label
{
	public override void _Process(double delta)
	{
		ThreadPool.GetMaxThreads(out var maxThreads, out var _);
		ThreadPool.GetAvailableThreads(out var availableThreads, out var _);
		this.Text = @$"fps {Engine.GetFramesPerSecond()}
active threads: {maxThreads - availableThreads}
last advance: {StateAdvancer.lastAdvanceSeconds}
last target: {TargetStateFromState.lastAdvanceSeconds}";
	}
}
