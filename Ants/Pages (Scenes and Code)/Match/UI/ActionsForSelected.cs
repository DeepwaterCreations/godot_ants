using Ants.AppState;
using Godot;
using Prototypist.Toolbox.Object;
using System.Linq;

public partial class ActionsForSelected : Label
{
	[Export]
	public StateToScene StateToScene { get; set; }
	
	public override void _Process(double delta)
	{
		if (StaticAppState.TryGetState(out InGameState state))
		{
			//			if (StateToScene.menu == Menu.commands)
			//			{
			//				this.Text =
			//	@$"Actions Menu (1):
			//Q - {GeActionsForKey(state, Godot.Key.Q)}
			//W - {GeActionsForKey(state, Godot.Key.W)}
			//E - {GeActionsForKey(state, Godot.Key.E)}
			//R - {GeActionsForKey(state, Godot.Key.R)}
			//T - {GeActionsForKey(state, Godot.Key.T)}
			//A - {GeActionsForKey(state, Godot.Key.A)}
			//S - {GeActionsForKey(state, Godot.Key.S)}
			//D - {GeActionsForKey(state, Godot.Key.D)}
			//F - {GeActionsForKey(state, Godot.Key.F)}
			//G - {GeActionsForKey(state, Godot.Key.G)}
			//Z - {GeActionsForKey(state, Godot.Key.Z)}
			//X - {GeActionsForKey(state, Godot.Key.X)}
			//C - {GeActionsForKey(state, Godot.Key.C)}
			//V - {GeActionsForKey(state, Godot.Key.V)}
			//B - {GeActionsForKey(state, Godot.Key.B)}";
			//			}
			//			else if (StateToScene.menu == Menu.build)
			//			{
			//				var buildActions = StateToScene.BuildActions();

			//				this.Text = $"Build Menu (2):\n";
			//				foreach (var key in StateToScene.orderedKeys) {
			//					this.Text += $"{key} - {(buildActions.TryGetValue(key, out var value) ? value.ToString() : "")}\n";
			//				}
			//			}
			//			else if (StateToScene.menu == Menu.select)
			//			{
			this.Text = StateToScene.KeyActionTest(state);

//        @$"
//Q - {NameForKey(state, Godot.Key.Q)}
//W - {NameForKey(state, Godot.Key.W)}
//E - {NameForKey(state, Godot.Key.E)}
//R - {NameForKey(state, Godot.Key.R)}
//T - {NameForKey(state, Godot.Key.T)}
//A - {NameForKey(state, Godot.Key.A)}
//S - {NameForKey(state, Godot.Key.S)}
//D - {NameForKey(state, Godot.Key.D)}
//F - {NameForKey(state, Godot.Key.F)}
//G - {NameForKey(state, Godot.Key.G)}
//Z - {NameForKey(state, Godot.Key.Z)}
//X - {NameForKey(state, Godot.Key.X)}
//C - {NameForKey(state, Godot.Key.C)}
//V - {NameForKey(state, Godot.Key.V)}
//B - {NameForKey(state, Godot.Key.B)}";
			//}
			//else {
			//	Log.WriteLine($"unexpect menu: {StateToScene.menu}");
			//}
		}
	}

	//private string GeActionsForKey(InGameState state, Godot.Key key)
	//{
	//	return StateToScene.ConglomeratedGetKeyActions(state.Game.displayState.advanceResult.state, key).SwitchReturns(
	//		clickActions => string.Join(", ", clickActions.Select(x => x.Item1.ToString()).Distinct()),
	//		actions => string.Join(", ", actions.Select(x => x.Item1.ToString()).Distinct()));
	//}

	//private string NameForKey(InGameState state, Godot.Key key)
	//{
	//	if (StateToScene.TryGetUnitForKey(key, out var tracker) && tracker.Tracks().Assign(out var tracked) != null ) {
	//		return tracked.immutable.type.ToString();
	//	}
	//	return null;
	//}
}
