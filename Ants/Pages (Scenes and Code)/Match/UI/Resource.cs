using Ants.AppState;
using Godot;
using System;
using System.Threading;

public partial class Resource : Label
{
	public override void _Process(double delta)
	{
		if (StaticAppState.TryGetState(out InGameState state))
		{
			this.Text = @$"
p0 money: {state.Game.displayState.advanceResult.state.money.GetMainMoney(0):f1} / {StateAdvancer.MaxMoney(state.Game.displayState.advanceResult.state.ants.Values, 0):f1}
p1 money: {state.Game.displayState.advanceResult.state.money.GetMainMoney(1):f1} / {StateAdvancer.MaxMoney(state.Game.displayState.advanceResult.state.ants.Values, 1):f1}
p0 gatherer money: {state.Game.displayState.advanceResult.state.money.GetGathererMoney(0):f1}
p1 gatherer money: {state.Game.displayState.advanceResult.state.money.GetGathererMoney(1):f1}
population: {state.Game.displayState.advanceResult.state.CompletAndPendingPopulation(state.index):f1} of {state.Game.displayState.advanceResult.state.populationState.allowedPopulation:f1}";
		}
	}
}
