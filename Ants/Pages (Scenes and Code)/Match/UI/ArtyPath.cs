using Godot;
using System;
using System.Diagnostics;
using System.Linq;

public partial class ArtyPath : Node3D
{
	private (Guid antId, Godot.Vector3 from, Godot.Vector3 to)? id;

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
	}

	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(double delta)
	{
	}

	public void Init(Guid antId, Vector3[] path) {
		Debug.Assert(id == null);

		id = (antId, path.First(), path.Last());

		foreach (var (a,b) in path.SkipLast(1).Zip(path.Skip(1), (a, b) => (a,b)))
		{
			var packedScene = GD.Load<PackedScene>("res://Pages (Scenes and Code)/Match/SceneToState/Ant/DebugLine.tscn");
			var line = packedScene.Instantiate<DebugLine>();
			AddChild(line);
			line.UpdateLine(a, b);
		}
	}
}
