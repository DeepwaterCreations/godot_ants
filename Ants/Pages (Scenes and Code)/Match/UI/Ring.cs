using Godot;
using System;
using System.Collections.Generic;
using System.Linq;

public partial class Ring : Node3D
{
	// TODO needs velocity

	private const int sides = 12;
	PackedScene debugLinePacked = GD.Load<PackedScene>("res://Pages (Scenes and Code)/Match/SceneToState/Ant/DebugLine.tscn");
	public void Init(float radius) {

		var points = new List<Vector3>();

		for (int i = 0; i <= sides; i++) {
			points.Add(new Vector3(radius, 0 ,0).Rotated(new Vector3(0,0,1) ,Mathf.Tau* (i/ (float)sides)));
		}

		foreach (var (from, to) in points.SkipLast(1).Zip(points.Skip(1), (x,y)=>(x,y)))
		{
			var debugLine = debugLinePacked.Instantiate<DebugLine>();
			AddChild(debugLine);
			debugLine.UpdateLine(from, to);
		}
	}
}
