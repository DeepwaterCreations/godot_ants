using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Godot;

public partial class Camera2 : Camera3D
{

	bool focus = true;
	private bool dragging = false;
	private Vector2 lastMousePosition = new Vector2();
	private Vector2 velocity = new Vector2();

	public override void _Ready()
	{
		base._Ready();
	}

	public override void _Process(double delta)
	{
		//Log.WriteLine($"at: {GlobalPosition.Z}");
		//Near = 1;// GlobalPosition.Z -110;
		//Far = 1000;//GlobalPosition.Z + 110;
		//Near = Position.Z - 30.0f;
		//Far = Position.Z;


		var mousePosition = MousePosition.CastPosition2d(this) - new Vector2(GlobalPosition.X, GlobalPosition.Y);
		if (dragging)
		{
			velocity = lastMousePosition - mousePosition;
		}
		else
		{
			velocity =new Vector2(0,0);//*= .9f;
		}
		GlobalPosition += new Vector3(velocity.X, velocity.Y, 0);
		lastMousePosition = mousePosition;


		//      if (focus)
		//{

		//	// var mousePosition = GetLocalMousePosition();
		//	// var visibleRect = GetViewport().GetVisibleRect();

		//	// 	// DO MATH

		//	// 	// position is bottom left
		//	// 	// end of top right
		//	// 	// 
		//	// 	// |----------------------end
		//	// 	// |                        |
		//	// 	// start -------------------|

		//	// IF the mouse position is near the top of the rectangle THEN move the camera up 
		//	// Log.WriteLine("words")

		//	// if (mousePosition.y >= visibleRect.End.y - 100/* we want a little buffer, scroll if it is near the top*/ ) {
		//	// 	Position = new Vector2(Position.x, Position.y + 100 * delta);
		//	// 	Log.WriteLine("I moved up!");
		//	// 	// Position.y = Position.y + 100;
		//	// }

		//	// GetMousePosition();
		//	// Position = new Vector2(Position.x, Position.y + 100 * delta);

		//	// don't scroll for now, I keep loosing my stuff
		//	//if (false){

		//	var rec = GetViewport().GetVisibleRect();

		//	var stupidMouse = GetViewport().GetMousePosition();// "stupid" because is top right
		//													   //var mouse = new Vector2(stupidMouse.x - (rec.Size.x/2f), - (stupidMouse.y - (rec.Size.y / 2f)) );
		//													   //var cameraSpeed = 1000;
		//	var cameraMargin = 100;

		//	var moveMouseX = 0f;
		//	var moveMouseY = 0f;

		//	if (stupidMouse.x < cameraMargin)
		//	{
		//		moveMouseX = (cameraMargin - stupidMouse.x);
		//	}
		//	else if (stupidMouse.x > rec.Size.x - cameraMargin)
		//	{
		//		// stupidMouse.x + moveMouseX = (rec.Size.x - cameraMargin)
		//		moveMouseX = ((rec.Size.x - cameraMargin) - stupidMouse.x);
		//	}


		//	if (stupidMouse.y < cameraMargin)
		//	{
		//		moveMouseY = ((cameraMargin - stupidMouse.y));
		//	}
		//	else if (stupidMouse.y > rec.Size.y - cameraMargin)
		//	{
		//		// stupidMouse.y + moveMouseY = (rec.Size.y - cameraMargin)
		//		moveMouseY = ((rec.Size.y - cameraMargin) - stupidMouse.y);
		//	}

		//	//moveMouseX *= .1f;
		//	//moveMouseY *= .1f;

		//	GetViewport().WarpMouse(new Vector2(stupidMouse.x + (moveMouseX), stupidMouse.y + (moveMouseY)));
		//	Position = new Vector2(Position.x - (moveMouseX * Zoom.x), Position.y - (moveMouseY * Zoom.y));

		//	//if (mouse.Length() > 1000) {
		//	//	var moveMouse =  ((mouse.Normalized() * 1000) - mouse);
		//	//	GetViewport().WarpMouse(new Vector2(moveMouse.x + stupidMouse.x , stupidMouse.y - moveMouse.y));
		//	//	//GetViewport().WarpMouse(new Vector2(stupidMouse.x, stupidMouse.y));
		//	//	Position = new Vector2(Position.x - moveMouse.x, Position.y + moveMouse.y);
		//	//}

		//	//if (rec.Size.x - mouse.x <= cameraMargin)
		//	//{
		//	//    var moveX = cameraMargin - (rec.Size.x - mouse.x);
		//	//    Position = new Vector2(Position.x + cameraSpeed * delta, Position.y);
		//	//    GetViewport().WarpMouse(new Vector2(mouse.x + moveX, mouse.y));
		//	//}

		//	//if (mouse.x <= cameraMargin)
		//	//{
		//	//    //Position.x -= cameraSpeed * delta;
		//	//    Position = new Vector2(Position.x + cameraSpeed * delta * -1, Position.y);
		//	//}

		//	//if (rec.Size.y - mouse.y <= cameraMargin)
		//	//{
		//	//    //Position.y += cameraSpeed * delta;
		//	//    Position = new Vector2(Position.x, Position.y + cameraSpeed * delta);
		//	//}

		//	//if (mouse.y <= cameraMargin)
		//	//{
		//	//    //Position.y -= cameraSpeed * delta;
		//	//    Position = new Vector2(Position.x, Position.y + cameraSpeed * delta * -1);
		//	//}
		//	//}
		//}

		base._Process(delta);
	}


	public override void _UnhandledInput(InputEvent @event)
	{
		if (@event is InputEventMouseButton inputEventMouseButton)
		{
			InputEventMouseButton emb = (InputEventMouseButton)@event;

			if (emb.IsPressed())
			{
				if (emb.ButtonIndex == MouseButton.WheelUp)
				{

					Size /= 1.1f;

					// perspective
					//var proposedZ = GlobalPosition.Z / Mathf.Pow(1.1f, emb.Factor);
					//proposedZ = Mathf.Max(proposedZ, Near + 100);
					//var deltaZ = proposedZ - GlobalPosition.Z;

					//var awayFromCamera = ProjectRayNormal(GetViewport().GetMousePosition());
					//var move = awayFromCamera * deltaZ / awayFromCamera.Z;
					//GlobalPosition += move;

				}
				if (emb.ButtonIndex == MouseButton.WheelDown)
				{

					Size *= 1.1f;

					//Fov = Math.Max(5, Fov - emb.Factor*5);

					// perspective
	 //               var proposedZ = GlobalPosition.Z * Mathf.Pow(1.1f, emb.Factor);
					//proposedZ = Mathf.Min(proposedZ, Far - 100);
					//var deltaZ = proposedZ - GlobalPosition.Z;

					//var awayFromCamera = ProjectRayNormal(GetViewport().GetMousePosition());
					//var move = awayFromCamera * deltaZ / awayFromCamera.Z;
					//GlobalPosition += move;
				}

				if ((emb.ButtonIndex & MouseButton.Middle) == MouseButton.Middle)
				{
					dragging = true;
				}
				else
				{
					dragging = false;
				}
			}
			else
			{
				dragging = false;
			}

		}
	}
}
