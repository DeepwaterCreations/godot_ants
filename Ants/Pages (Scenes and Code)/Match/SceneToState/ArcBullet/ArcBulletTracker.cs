using Ants.Domain;
using Ants.Domain.Infrastructure;
using Godot;
using System;
using System.Runtime.Intrinsics.X86;

public partial class ArcBulletTracker : Node3D
{
	internal void Update(ArcBulletState arcBulletState, StateToScene manager)
	{
		var fromCenter = Avx2.Subtract(arcBulletState.position, arcBulletState.immutable.center);

		var rads = arcBulletState.immutable.speed * ((float)FramesPerSecond.framesPerSecond / 60) * manager.frameOnState / fromCenter.Length();

		var realPosition = Avx2.Add( fromCenter.Rotated(rads), arcBulletState.immutable.center);

		var position2d = realPosition.ToVector2();
		this.GlobalPosition = new Vector3(position2d.X, position2d.Y, GlobalPosition.Z);
	}
}
