using Godot;
using System;
using System.Runtime.Intrinsics;

public partial class StationTracker : MeshInstance3D
{
	private Vector128<double> tracks;

	internal void Track(Vector128<double> stationsToMake)
	{
		tracks = stationsToMake;
	}

	internal Vector128<double> Tracks()
	{
		return tracks;
	}
}
