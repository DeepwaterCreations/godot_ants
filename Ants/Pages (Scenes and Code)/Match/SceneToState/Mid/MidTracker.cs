using Ants.Domain;
using Ants.Domain.Infrastructure;
using Godot;
using System;
using System.Runtime.Intrinsics;
using System.Runtime.Intrinsics.X86;

public partial class MidTracker : Node3D
{
	internal void Update(Vector128<double> position, Vector128<double> velocity, StateToScene stateToScene)
	{
		var at =  Avx.Add(position, Avx.Multiply(velocity, (stateToScene.frameOnState * (float)FramesPerSecond.framesPerSecond / 60.0).AsVector())).ToVector2();
		this.GlobalPosition = new Vector3(at.X, at.Y, 0);
	}
}
