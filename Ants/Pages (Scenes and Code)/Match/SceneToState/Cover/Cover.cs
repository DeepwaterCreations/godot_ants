using Godot;
using System;

public partial class Cover : Node3D
{
	[Export]
	public float radius;

	public override void _Ready()
	{
		var r = new Random();
		this.RotateX(r.Next() * Mathf.Tau);
		this.RotateY(r.Next() * Mathf.Tau);
		this.RotateZ(r.Next() * Mathf.Tau);

		base._Ready();
	}
}

