using Ants.Domain.State;
using Godot;
using System;

public partial class SmokeTracker : Node3D
{
	private SmokeState bakcing;
	internal void Track(SmokeState smokeState,
		bool visible)
	{
		Visible= visible;
		bakcing = smokeState;
		Position = smokeState.position.ToVector3(0);
	}

	internal SmokeState Tracks()
	{
		return bakcing; 
	}
}
