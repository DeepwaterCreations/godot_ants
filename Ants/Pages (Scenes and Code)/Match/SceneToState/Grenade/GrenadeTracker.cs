using Ants.Domain;
using Godot;

public partial class GrenadeTracker : Node3D, ITrackState<GrenadeState>
{
	private GrenadeState grenadeState;
	private StateToScene manager;
	private int tick;

	public GrenadeState Tracks() => grenadeState;

	public void Track(StateToScene manager, GrenadeState grenadeState)
	{
		this.manager = manager;
		this.grenadeState = grenadeState;
		var fractionalTick = manager.tick + (manager.frameOnState * FramesPerSecond.framesPerSecond / 60.0);
		var position2d = grenadeState.GetPosition(fractionalTick).ToVector2();
		var z = grenadeState.GetZ(fractionalTick);
		this.GlobalPosition = new Vector3(position2d.X, position2d.Y,(float) z);
	}

	public override void _Process(double delta)
	{
		base._Process(delta);
		if (grenadeState != null)
		{
			var fractionalTick = manager.tick + (manager.frameOnState * FramesPerSecond.framesPerSecond / 60.0);
			var position2d = grenadeState.GetPosition(fractionalTick).ToVector2();
			var z = grenadeState.GetZ(fractionalTick);
			this.GlobalPosition = new Vector3(position2d.X, position2d.Y, (float)z);
		}
	}
}

