using Ants.Domain.State;
using Godot;
using System;

public partial class ResourceTracker : Node3D, ITrackState<ResourceState>
{
	private ResourceState tracks;
	public ResourceState Tracks()
	{
		return tracks;
	}

	internal void Track(ResourceState x, bool seen)
	{
		var pos = x.immutable.position.ToVector2();
		Scale = new Vector3((float)x.available*.5f, (float)x.available * .5f, Scale.Z);
		GlobalPosition = new Vector3(pos.X, pos.Y,0);
		Visible = seen;
		tracks = x;
	}
}
