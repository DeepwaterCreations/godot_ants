﻿using Godot;
using System;
using System.Collections.Concurrent;
using System.IO;
using System.Linq;
using System.Runtime.Intrinsics;
using System.Threading;
using System.Threading.Tasks;

public class ProjectedPathWorker {

	private class Entry {
        public Vector3[] path;
        public Task task;
    }

    // null when never calculated


    private readonly ConcurrentDictionary<Guid, Entry> dict = new ConcurrentDictionary<Guid, Entry>();

	public Vector3[] Request(ImmunatblePhysicsState immunatble, Vector128<double> position, Vector128<double> velocity, Map2<RockState> rockMap, int steps, Guid antId) 
	{
		var entry = dict.GetOrAdd(antId, new Entry());

        var task = new Task(() =>
        {
            entry.path = ArtyShotSimulator.ProjectPath(immunatble, position, velocity, rockMap, steps).Select(x => x.ToVector3(50)).ToArray();
            entry.task = null;
        });
        
        task.ContinueWith(x =>
        {
            if (x.IsFaulted)
            {
                Log.WriteLine(x.Exception.ToString());
            }
        });

        if (Interlocked.CompareExchange(ref entry.task, task, null) == null)
		{
            Task.Run(task.Start);
		}
        return entry.path;
	}
}
