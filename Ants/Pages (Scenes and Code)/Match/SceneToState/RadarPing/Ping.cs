using Ants.Domain;
using Ants.Domain.State;
using Godot;
using System;

public partial class Ping : MeshInstance3D
{


	public void Update(PingState pingState, int tick) 
	{
		var part = Math.Clamp(1 - ((tick - pingState.tick) / (float)StateAdvancer.PingLastsFor), 0, 1);
		this.Scale = new Vector3(10*part, 10*part, 10*part);

		var position = pingState.position.ToVector2();
		this.GlobalPosition = new Vector3(position.X, position.Y, 0);
	}
}
