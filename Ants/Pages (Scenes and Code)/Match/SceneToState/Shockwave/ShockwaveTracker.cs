using Ants.Domain;
using Ants.Domain.State;
using Godot;

public partial class ShockwaveTracker : Node3D, ITrackState<ShockwaveState>
{
	private ShockwaveState shockwaveState;
	private StateToScene manager;
	private int tick;

	public ShockwaveState Tracks() => shockwaveState;

	public void Track(StateToScene manager, ShockwaveState shockwaveState)
	{
		this.manager = manager;
		this.shockwaveState = shockwaveState;
		var fractionalTick = manager.tick + (manager.frameOnState * FramesPerSecond.framesPerSecond / 60.0);
		var position2d = shockwaveState.CurrentPosition(fractionalTick).ToVector2();
		this.GlobalPosition = new Vector3(position2d.X, position2d.Y, 10);
	}

	public override void _Process(double delta)
	{
		base._Process(delta);
		if (shockwaveState != null)
		{
			var fractionalTick = manager.tick + (manager.frameOnState * FramesPerSecond.framesPerSecond / 60.0);
			var position2d = shockwaveState.CurrentPosition(fractionalTick).ToVector2();
			this.GlobalPosition = new Vector3(position2d.X, position2d.Y, 10);
		}
	}
}

