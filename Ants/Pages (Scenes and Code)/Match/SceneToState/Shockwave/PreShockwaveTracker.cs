using Ants.Domain;
using Ants.Domain.State;
using Godot;

public partial class PreShockwaveTracker : Node3D, ITrackState<PreShockwaveState>
{
	private PreShockwaveState preShockwaveState;
	private StateToScene manager;
	private int tick;

	public PreShockwaveState Tracks() => preShockwaveState;

	public void Track(StateToScene manager, PreShockwaveState preShockwaveState)
	{
		this.manager = manager;
		this.preShockwaveState = preShockwaveState;
		var fractionalTick = manager.tick + (manager.frameOnState * FramesPerSecond.framesPerSecond / 60.0);
		var position2d = preShockwaveState.CurrentPosition(fractionalTick).ToVector2();
		this.GlobalPosition = new Vector3(position2d.X, position2d.Y, 10);
	}

	public override void _Process(double delta)
	{
		base._Process(delta);
		if (preShockwaveState != null)
		{
			var fractionalTick = manager.tick + (manager.frameOnState * FramesPerSecond.framesPerSecond / 60.0);
			var position2d = preShockwaveState.CurrentPosition(fractionalTick).ToVector2();
			this.GlobalPosition = new Vector3(position2d.X, position2d.Y, 10);
		}
	}
}

