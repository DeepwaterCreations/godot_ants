using Ants.Domain.Infrastructure;
using Ants.Domain.State;
using Godot;
using System;
using System.Collections.Generic;
using System.Linq;
using static Godot.WebSocketPeer;
using System.Security.Cryptography;

public class EffectEventHandler
{
	public Dictionary<object, int> handled = new Dictionary<object, int>();
	private readonly PackedScene effect = GD.Load<PackedScene>("res://Effects/Hit.tscn");
	private readonly PackedScene gather = GD.Load<PackedScene>("res://Effects/Gather.tscn");
	public int maxFrameSeen = -1;

	internal void Handle(EffectEvents events, int frame, Node node, int playerSide, SimulationState state, ImmutableSimulationState immutableSimulationState)
	{
		foreach (var shotHit in events.shotsHit.Where(x => !handled.ContainsKey(x)))
		{
			if (handled.TryAdd(shotHit, frame))
			{
				var atVector2 = shotHit.at.ToVector2();

				var effectNode = effect.Instantiate<RemoveWhenDone>();
				effectNode.power = (float)shotHit.damage/5f;

				node.AddChild(effectNode);
				effectNode.GlobalPosition = new Vector3(atVector2.X, atVector2.Y, 20);
				//effectNode.Scale = new Vector3((float)shotHit.damage, (float)shotHit.damage, (float)shotHit.damage);
				var player = new AudioStreamPlayer2D();
				node.AddChild(player);
				player.VolumeDb = player.VolumeDb / 2;
				//player.VolumeDb = (float)Math.Pow((float)shotHit.damage, 1.5);
				player.Stream = (AudioStream)GD.Load("res://sounds/enemyhit.mp3");
				player.Finished += () => { player.QueueFree(); };
				player.Play();
			}
		}

		foreach (var resourceGathered in events.resourceGathereds)
		{
			// just handle them if it's a frame we haven't seen before
			if (frame < maxFrameSeen) {
				break;
			}

			if (resourceGathered.amount * 2 > GD.Randf()) {
				var effectNode = gather.Instantiate<Gather>();
				var from = resourceGathered.from.ToVector3(50);
				var to = resourceGathered.to.ToVector3(50);
				var distance = to - from;
				var longestAllowed = Math.Max(0, distance.Length() - Gather.HowFarITravel());
				var at = from + (distance.Normalized() * GD.Randf()* (float)longestAllowed);
				if (resourceGathered.side == playerSide || StateAdvancer.SideSeesGather(playerSide, state, at.ToNumericsVector(),Game.Avoid(immutableSimulationState, state))) {
					node.AddChild(effectNode);
					effectNode.GlobalPosition = at;
					effectNode.Init((to - from).Normalized());
				}
			}
		}

		foreach (var shotFired in events.shotsFired.Where(x => !handled.ContainsKey(x)))
		{
			if (handled.TryAdd(shotFired, frame))
			{
				var player = new AudioStreamPlayer2D();
				node.AddChild(player);
				player.GlobalPosition = shotFired.at.ToVector2();
				player.Stream = (AudioStream)GD.Load("res://sounds/musket.ogg");
				player.VolumeDb = player.VolumeDb / 2;
				player.Finished += () => { player.QueueFree(); };
				player.Play();
			}
		}

		//foreach (var shotFizzled in events.ShotsFizzled)
		//{
		//	if (handled.TryAdd(shotFizzled, frame))
		//	{

		//		var player = new AudioStreamPlayer2D();
		//		node.AddChild(player);
  //              player.GlobalPosition = shotFizzled.at.ToVector2();
  //              player.Stream = (AudioStream)GD.Load("res://sounds/shot_missed.mp3");
  //              player.VolumeDb = player.VolumeDb / 2;
  //              player.Finished += () => { player.QueueFree(); };
		//		player.Play();
			   
		//	}

		//}

		//foreach (var shotHitFriendly in events.shotsHitFriendly)
		//{
		//	if (handled.TryAdd(shotHitFriendly, frame))
		//	{

		//		var player = new AudioStreamPlayer2D();
		//		node.AddChild(player);
  //              player.GlobalPosition = shotHitFriendly.at.ToVector2();
  //              player.Stream = (AudioStream)GD.Load("res://sounds/alert.mp3");
  //              player.VolumeDb = player.VolumeDb / 2;
  //              player.Finished += () => { player.QueueFree(); };
		//		player.Play();
		//	}
		//}

		foreach (var shotHitRock in events.shotsHitRock)
		{
			if (handled.TryAdd(shotHitRock, frame))
			{

				var player = new AudioStreamPlayer2D();
				node.AddChild(player);
				player.GlobalPosition = shotHitRock.at.ToVector2();
				player.Stream = (AudioStream)GD.Load("res://sounds/prht.ogg");
				player.VolumeDb = player.VolumeDb / 2;
				player.Finished += () => { player.QueueFree(); };
				player.Play();
			}
		}

		maxFrameSeen = frame;
	}

	public void Clean(int frame)
	{
		handled = handled.Where(x => x.Value > frame - 100).ToDictionary(x => x.Key, x => x.Value);
	}

	internal void HandleConfiredEvent(EffectEventRequireVerification events, Node node, int frame, int playerSide)
	{
		foreach (var antCreated in events.antsCreated)
		{
			if (handled.TryAdd(antCreated, frame))
			{
				if (antCreated.side != playerSide) {
					continue;
				}
				var player = new AudioStreamPlayer2D();
				node.AddChild(player);
				player.GlobalPosition = antCreated.at.ToVector2();
				player.Stream = (AudioStream)GD.Load("res://sounds/unit_created.mp3");
				player.VolumeDb = player.VolumeDb / 2;
				player.Finished += () => { player.QueueFree(); };
				player.Play();
			}
		}

		foreach (var antKilled in events.antsKilled)
		{
			if (handled.TryAdd(antKilled, frame))
			{
				var atVector2 = antKilled.at.ToVector2();

				var effect = GD.Load<PackedScene>("res://Effects/Killed.tscn");
				var effectNode = effect.Instantiate<GpuParticles3D>();
				node.AddChild(effectNode);
				effectNode.GlobalPosition = new Vector3(atVector2.X, atVector2.Y, 100);

				var player = new AudioStreamPlayer2D();
				node.AddChild(player);

				player.GlobalPosition = antKilled.at.ToVector2();
				player.Stream = (AudioStream)GD.Load("res://sounds/unit_death2.mp3");
				player.VolumeDb = player.VolumeDb / 2;
				player.Finished += () => { player.QueueFree(); };
				player.Play();
			}
		}
	}
}
