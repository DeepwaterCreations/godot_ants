using Ants.AppState;
using Ants.Domain;
using Ants.Domain.Infrastructure;
using Ants.Domain.State;
using Ants.Domain.State.Orders._12___Arty;
using Godot;
using Prototypist.TaskChain;
using Prototypist.Toolbox;
using Prototypist.Toolbox.Object;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.NetworkInformation;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.Intrinsics;
using System.Runtime.Intrinsics.X86;
using System.Threading.Tasks;
using static Game;
using static Godot.WebSocketPeer;
using static Pathing;
using static RockState;
using static StateToScene;
using static System.Collections.Specialized.BitVector32;

//public enum Menu
//{
//	commands,
//	build,
//	select,
//}

public partial class StateToScene : Node3D
{

	[Export]
	public MeshInstance3D selection;


	public readonly EffectEventHandler eventHandler = new EffectEventHandler();
	public PackedScene antScene;
	public PackedScene bulletScene;
	public PackedScene healBulletScene;
	public PackedScene stationScene;
	public PackedScene controlPointScene;
	public PackedScene grenadeScene;
	public PackedScene resourceScene;
	public PackedScene smokeScene;
	public PackedScene preShockwaveScene;
	public PackedScene shockwaveScene;
	public PackedScene pingScene;
	public PackedScene artyBulletScene;
	public PackedScene zapperSetupScene;
	public PackedScene midScene;
	public PackedScene arcBulletScene;

	public PackedScene artyPathScene;
	//public PackedScene ringScene;
	private RingMaster ringMaster;

	private List<Bullet2> bulletTrackers = new();
	private List<HealBulletTracker> healBulletTrackers = new();
	private List<AntTracker> antTrackers = new();
	private List<StationTracker> stationTrackers = new();
	private List<ControlPoint2> controlPointTrackers = new();
	private List<GrenadeTracker> grenadeTrackers = new();
	private List<ResourceTracker> resourceTrackers = new();
	private List<SmokeTracker> smokeTrackers = new();
	private List<PreShockwaveTracker> preShockwaveTrackers = new();
	private List<ShockwaveTracker> shockwaveTrackers = new();
	private List<ArtyBulletTracker> artyBulletTrackers = new();
	private List<Ping> pingsTrackers = new();
	private List<MidTracker> midTrackers = new();
	private List<ArcBulletTracker> arcBulletTrackers = new();
	//private List<Ring> rings = new();

	private List<ZapperSetupTracker> zapperSetupTrackers = new();

	private Dictionary<(Guid antId, Godot.Vector3 from, Godot.Vector3 to), ArtyPath> artyPaths = new();

	private readonly ProjectedPathWorker projectPath = new ProjectedPathWorker();

	//public Menu menu = Menu.select;


	//public List<Dictionary<Godot.Key, Assignment>> pages = new List<Dictionary<Godot.Key, Assignment>>() {
	//    new Dictionary<Godot.Key, Assignment>()
	//};

	public Dictionary<(Godot.Key key, int pageIndex), Assignment> assigned = new Dictionary<(Godot.Key key, int pageIndex), Assignment>();

	//private List<Godot.Key> onTab = new List<Godot.Key>();

	private List<UnitPath> paths = new List<UnitPath>();

	public class Assignment
	{
		public class Select { }
		public readonly AntTracker assignedTo;
		public readonly IOrType<AntAction, Assignment.Select> action;

		public Assignment(AntTracker assignedTo, IOrType<AntAction, Assignment.Select> action)
		{
			this.assignedTo = assignedTo ?? throw new ArgumentNullException(nameof(assignedTo));
			this.action = action ?? throw new ArgumentNullException(nameof(action));
		}
	}



	// TODO you are here
	// I don't really like having fuction pointers here
	// it's force at a distance
	// more UI state
	// the UI more aware of the actions
	//
	// is it too complex to do multiple actions at once?
	// probably it is
	public StateToScene()
	{
		//assigned = pages.First();
		//onFrame = DefaultOnFrame;
	}

	private static Dictionary<Godot.Key, Godot.Key[]> lines = new Dictionary<Godot.Key, Godot.Key[]>
	{
		[Godot.Key.Key1] = new Godot.Key[] { Godot.Key.Key2, Godot.Key.Key3, Godot.Key.Key4, Godot.Key.Key5, Godot.Key.Key6, Godot.Key.Key7, Godot.Key.Key8, Godot.Key.Key9, Godot.Key.Key0 },
		[Godot.Key.Q] = new Godot.Key[] { Godot.Key.W, Godot.Key.E, Godot.Key.R, Godot.Key.T, Godot.Key.Y, Godot.Key.U, Godot.Key.I, Godot.Key.O, Godot.Key.P },
		[Godot.Key.A] = new Godot.Key[] { Godot.Key.S, Godot.Key.D, Godot.Key.F, Godot.Key.G , Godot.Key.H, Godot.Key.J, Godot.Key.K, Godot.Key.L, Godot.Key.Semicolon},
		[Godot.Key.Z] = new Godot.Key[] { Godot.Key.X, Godot.Key.C, Godot.Key.V, Godot.Key.B ,Godot.Key.N, Godot.Key.M, Godot.Key.Less, Godot.Key.Greater, Godot.Key.Question },
	};

	public void AddHotKey(AntTracker ant)
	{
		var (requestedKey, pageIndex) = AntFactory.antKeys[ant.antType];

		var line = lines[requestedKey.ToGodotKey()];

		foreach (var item in line)
		{
			if (!assigned.ContainsKey((item, pageIndex))) {
				assigned.Add((item, pageIndex), new Assignment(ant, OrType.Make<AntAction, Assignment.Select>(new Assignment.Select())));
				return;
			}
		}
	}

	public void Enact(Godot.Key keycode, bool shiftPressed, bool controlPressed, bool altPressed, InGameState inGameState)
	{
		pendingClickOrders = OrType.Make<(IPendingClickOrderState, Guid id)[], EggKeyAction>(Array.Empty<(IPendingClickOrderState, Guid id)>());

		var pageIndex = altPressed ? 1:
						controlPressed? 2:
						shiftPressed? 3:
						0;

		if (assigned.TryGetValue((keycode, pageIndex), out var target))
		{
			//if (controlPressed)
			//{
			//	target.assignedTo.selected = false;
			//}
			//else if (shiftPressed)
			//{
			//	target.assignedTo.selected = true;
			//}
			//else
			//{
				foreach (var ant in antTrackers)
				{
					ant.selected = false;
				}
				target.assignedTo.selected = true;
			//}

			target.action.Switch(
				antAction =>
				{
					antAction.Switch(
						pendingOrder =>
						{
							pendingClickOrders = OrType.Make<(IPendingClickOrderState, Guid id)[], EggKeyAction>(new[] { (pendingOrder, target.assignedTo.Tracks().immutable.id) });
						},
						keyAction =>
						{
							inGameState.Game.Order(target.assignedTo.Tracks().immutable.id, keyAction.GetPendingOrder(), shiftPressed);
						});
				},
				_ =>
				{
					// 
				});
		}
		else
		{
			var selected = GetPrimaryAnt(inGameState);
			
			if (selected != null)
			{
				if (selected.immutable.actions.TryGetValue((keycode.ToDomainKey(), pageIndex), out var action))
				{
					action.Switch(pendingOrder =>
					{
						pendingClickOrders = OrType.Make<(IPendingClickOrderState, Guid id)[], EggKeyAction>(new[] { (pendingOrder, selected.immutable.id) });
					},
					keyAction =>
					{
						inGameState.Game.Order(selected.immutable.id, keyAction.GetPendingOrder(), shiftPressed);
					});
				}
			}
		}
	}

	private AntState GetPrimaryAnt(InGameState inGameState)
	{
		return GetSelectedAnts(inGameState.Game.displayState.advanceResult.state).OrderBy(x => AntFactory.ActionPriory(x.immutable.type)).ThenBy(x => x.immutable.id).FirstOrDefault();
	}

	//public static readonly List<Godot.Key> orderedKeys = new List<Godot.Key> {
	//    Godot.Key.Key1,Godot.Key.Key2,Godot.Key.Key3,Godot.Key.Key4,Godot.Key.Key5,
	//    Godot.Key.Q, Godot.Key.W, Godot.Key.E, Godot.Key.R, Godot.Key.T, 
	//    Godot.Key.A, Godot.Key.S, Godot.Key.D, Godot.Key.F, Godot.Key.G, 
	//    Godot.Key.Z, Godot.Key.X, Godot.Key.C, Godot.Key.V, Godot.Key.B, 
	//};
	//private static readonly IReadOnlySet<Godot.Key> commandKeys = orderedKeys.ToHashSet();

	//internal static bool IsCommandKey(Godot.Key keycode)
	//{
	//    return commandKeys.Contains(keycode);
	//}

	internal string KeyActionTest(InGameState inGameState)
	{
		var res = "";

		var selected = GetPrimaryAnt(inGameState);

		if (selected != null)
		{
			foreach (var action in selected.immutable.actions)
			{
				string keyCombo = GetKeyCombo(action.Key);

				action.Value.Switch(
					antAction => res += $"{keyCombo} - {action.Value.description}\n",
					select => res += $"{keyCombo} - {action.Value.description}\n");
			}
		}

		return res;
	}

	private static string GetKeyCombo((Ants.Domain.Key key, int pageIndex) entry)
	{
		var keyCombo = entry.key.ToGodotKey().ToString();

		if (entry.pageIndex == 0)
		{
			keyCombo = "      " + keyCombo;
		}
		else if (entry.pageIndex == 1)
		{
			keyCombo = "alt + " + keyCombo;
		}
		else if (entry.pageIndex == 2)
		{
			keyCombo = "ctrl + " + keyCombo;
		}
		else if (entry.pageIndex == 3)
		{
			keyCombo = "shift + " + keyCombo;
		}

		return keyCombo;
	}

	internal string GetHotKeys(AntTracker ant)
	{

		foreach (var item in assigned)
		{
			if (item.Value.assignedTo == ant) {
				return GetKeyCombo((item.Key.key.ToDomainKey(), item.Key.pageIndex));
			}
		}

		return string.Empty;
	}

	public string DenotActive(string s, bool active)
	{
		if (active)
		{
			return s.ToUpper();
		}
		return s.ToLower();
	}

	//internal bool TryGetUnitForKey(Godot.Key key, out AntTracker ant)
	//{
	//	if (assigned.TryGetValue(key, out var assignment)){
	//		ant = assignment.assignedTo;
	//		return true;
	//	}
	//	ant = null;
	//	return false;
	//}


	public override void _Ready()
	{
		base._Ready();
		antScene = GD.Load<PackedScene>("res://Pages (Scenes and Code)/Match/SceneToState/Ant/Ant.tscn");
		bulletScene = GD.Load<PackedScene>("res://Pages (Scenes and Code)/Match/SceneToState/Bullet/Bullet2.tscn");
		healBulletScene = GD.Load<PackedScene>("res://Pages (Scenes and Code)/Match/SceneToState/HealBullet/HealBullet.tscn");
		stationScene = GD.Load<PackedScene>("res://Pages (Scenes and Code)/Match/SceneToState/Station/Station.tscn");
		controlPointScene = GD.Load<PackedScene>("res://Pages (Scenes and Code)/Match/SceneToState/ControlPoint/ControlPoint2.tscn");
		grenadeScene = GD.Load<PackedScene>("res://Pages (Scenes and Code)/Match/SceneToState/Grenade/Grenade.tscn");
		resourceScene = GD.Load<PackedScene>("res://Pages (Scenes and Code)/Match/SceneToState/ResourceSpawner/Resource.tscn");
		smokeScene = GD.Load<PackedScene>("res://Pages (Scenes and Code)/Match/SceneToState/Smoke/Smoke.tscn");
		preShockwaveScene = GD.Load<PackedScene>("res://Pages (Scenes and Code)/Match/SceneToState/Shockwave/PreShockwave.tscn");
		shockwaveScene = GD.Load<PackedScene>("res://Pages (Scenes and Code)/Match/SceneToState/Shockwave/Shockwave.tscn");
		pingScene = GD.Load<PackedScene>("res://Pages (Scenes and Code)/Match/SceneToState/RadarPing/Ping.tscn");
		artyBulletScene = GD.Load<PackedScene>("res://Pages (Scenes and Code)/Match/SceneToState/ArtyBullet/ArtyBullet.tscn");
		zapperSetupScene = GD.Load<PackedScene>("res://Pages (Scenes and Code)/Match/SceneToState/ZapperSetup/ZapperSetup.tscn");
		midScene = GD.Load<PackedScene>("res://Pages (Scenes and Code)/Match/SceneToState/Mid/Mid.tscn");
		arcBulletScene = GD.Load<PackedScene>("res://Pages (Scenes and Code)/Match/SceneToState/ArcBullet/ArcBullet.tscn");

		artyPathScene = GD.Load<PackedScene>("res://Pages (Scenes and Code)/Match/UI/ArtyPath.tscn");
		ringMaster = new RingMaster(this);
		//ringScene = GD.Load<PackedScene>("res://Pages (Scenes and Code)/Match/UI/Ring.tscn");
	}

	public override void _Process(double delta)
	{
		if (StaticAppState.TryGetState(out LoadingGameAppState loading) && loading.paths == null)
		{
			var ants = new List<AntState>();
			foreach (var antTracker in GetChildren().OfType<AntTracker>())
			{
				var antState = AntFactory.Make(StringToGuid(antTracker.GetPath()), antTracker.antType, antTracker.side, antTracker.GlobalPosition.ToNumericsVector());
				antTracker.Track(antState, loading.index);
				ants.Add(antState);
				antTrackers.Add(antTracker);
				if (loading.index == antState.immutable.side)
				{
					AddHotKey(antTracker);
				}
			}

			var avoidRocks = new List<(Vector128<double> globalPosition, double radius)>();
			var rocks = new List<RockState>();
			foreach (var rockTracker in GetChildren().OfType<Rock2>())
			{
				var rockState = new RockState(new ImmunatblePhysicsStateInner(rockTracker.Radius), rockTracker.GlobalPosition.ToNumericsVector());
				rockTracker.Track(this, rockState);
				rocks.Add(rockState);
				avoidRocks.Add((rockState.position, rockState.physicsImmunatbleInner.radius));

			}

			var controlPoints = new List<ControlPointState>();
			foreach (var controlPointTracker in GetChildren().OfType<ControlPoint2>())
			{
				var controlPointState = new ControlPointState(0, controlPointTracker.GlobalPosition.ToNumericsVector());
				controlPointTracker.Track(this, controlPointState);
				controlPoints.Add(controlPointState);
				controlPointTrackers.Add(controlPointTracker);

			}

			var mudDict = new Dictionary<(int x, int y), Vector128<double>>();
			var muds = new List<(Vector128<double> globalPosition, double radius)>();
			foreach (var mud in GetChildren().OfType<Mud>())
			{
				muds.Add((mud.GlobalPosition.ToNumericsVector(), mud.radius));

				var mapPosition = mud.GlobalPosition.ToNumericsVector().ToMapPosition();

				foreach (var softSpot in new Mappable(mud.radius)
					.ReletivePositions()
					.Select(x => x.Shift(mapPosition.x, mapPosition.y)))
				{
					var center = mud.GlobalPosition.ToNumericsVector();
					if (!mudDict.TryAdd(softSpot, center))
					{
						var globalLoc = softSpot.ToGlobalPosition();
						var currentCenter = mudDict[softSpot];
						mudDict[softSpot] = Avx.Subtract(globalLoc, center).Length() < Avx.Subtract(globalLoc, currentCenter).Length() ? center : currentCenter;
					}
				}
			}

			var coverSet = new HashSet<(int x, int y)>();
			var coverList = new List<CoverState>();
			foreach (var cover in GetChildren().OfType<Cover>())
			{
				var mapPosition = cover.GlobalPosition.ToNumericsVector().ToMapPosition();

				foreach (var coverSpot in new Mappable(cover.radius)
					.ReletivePositions()
					.Select(x => x.Shift(mapPosition.x, mapPosition.y)))
				{
					coverSet.Add(coverSpot);
				}
				coverList.Add(new CoverState(new ImmunatblePhysicsStateInner(cover.radius), cover.GlobalPosition.ToNumericsVector()));
			}


			var resourceSpawners = new List<ResourceSpawnerState>();
			foreach (var resourceSpawnerTracker in GetChildren().OfType<ResourceSpawnerTracker>())
			{
				resourceSpawners.Add(new ResourceSpawnerState(resourceSpawnerTracker.GlobalPosition.ToNumericsVector(), StringToGuid(resourceSpawnerTracker.GetPath())));
			}

			var smokes = new List<SmokeState>();
			foreach (var smokesTracker in GetChildren().OfType<SmokeTracker>())
			{
				var smokeState = new SmokeState(smokesTracker.GlobalPosition.ToNumericsVector(), StringToGuid(smokesTracker.GetPath()));
				smokesTracker.Track(smokeState, false);
				smokes.Add(smokeState);
				smokeTrackers.Add(smokesTracker);
			}

			// TODO build stations
			var stations = new List<StationState>();

			var terrainMap = new TerrainMap(mudDict, coverSet);

			var antsDict = new ConcurrentDictionary<Guid, AntState>();

			foreach (var ant in ants)
			{
				antsDict[ant.immutable.id] = ant;
			}

			var numberOfPlayers = 4;// why not
			var mudStates = muds.Select(x => new MudState(x.radius, x.globalPosition)).ToArray();

			loading.TrySetStatePendingPaths(
				new ImmutableSimulationStatePendingPaths(
					rocks.ToArray(),
					muds.Select(x => new MudState(x.radius, x.globalPosition)).ToArray(),
					coverList.ToArray()),
				new SimulationState(
					antsDict,
					new ConcurrentDictionary<BulletState.Immutable.Id, BulletState>(),
					new MoneyState(new double[numberOfPlayers].Select(x => 1500.0).ToArray(), new double[numberOfPlayers].Select(x => 1000.0).ToArray()),
					new ControlState(100.0),
					controlPoints.ToArray(),
					new List<StationState>(),
					new PopulationState(8, new double[numberOfPlayers]),
					new List<GrenadeState>(),
					resourceSpawners.ToArray(),
					new List<ResourceState>(),
					smokes.ToDictionary(x => x.id, x => x),
					new List<ShockwaveState>(),
					//new List<PreShockwaveState>(),
					new ConcurrentDictionary<HealBulletState.Immutable.Id, HealBulletState>(),
					new ConcurrentDictionary<ArtyBulletState.Immutable.Id, ArtyBulletState>(),
					new List<ZapperSetupState>(),
					new ConcurrentDictionary<ArcBulletState.Immutable.Id, ArcBulletState>()
					));
			var mudMap = Map2<MudState>.Build(mudStates);

			loading.TryBuildPaths(avoidRocks, muds);
			loading.TryBuildTrainPaths(avoidRocks, stations.Select(x => x.position).ToHashSet());
		}

		if (StaticAppState.TryGetState(out IHaveGame state) && StaticAppState.TryGetState(out HasPlayersAppState networkedApp))
		{
			var stopwatch = new Stopwatch();
			stopwatch.Start();

			var displayState = state.Game.displayState;


			Dictionary<(Guid antId, Godot.Vector3 from, Godot.Vector3 to), Godot.Vector3[]> artyTargetingUi = pendingClickOrders.SwitchReturns(orders =>
			{
				var mousePosition = MousePosition.CastPosition2d(this);
				var list = new List<(Guid id, Vector3[])>();
				foreach (var (order, id) in orders)
				{
					if (order.SafeIs(out ArtyNeedsClick _))
					{
						if (displayState.advanceResult.state.ants.TryGetValue(id, out var ant))
						{
							var current = projectPath.Request(new ImmunatblePhysicsState(1),
								ant.position, Avx.Subtract(mousePosition.ToNumericsVector(), ant.position).ToLength(ant.artyState.immutable.speed), state.Game.immutableSimulationState.rockMap, ant.artyState.immutable.runForTicks, id);
							if (current != null)
							{
								list.Add((id, current));
							}
						}
					}
				}

				return list.ToDictionary(x => (x.id, x.Item2.First(), x.Item2.Last()), x => x.Item2);
			}, _ => new Dictionary<(Guid antId, Godot.Vector3 from, Godot.Vector3 to), Godot.Vector3[]>());

			var nextArtyPaths = new Dictionary<(Guid antId, Godot.Vector3 from, Godot.Vector3 to), ArtyPath>();

			foreach (var (key, value) in artyTargetingUi)
			{
				if (artyPaths.TryGetValue(key, out var artyPath))
				{
					nextArtyPaths.Add(key, artyPath);
					continue;
				}

				artyPath = artyPathScene.Instantiate<ArtyPath>();
				AddChild(artyPath);
				artyPath.Init(key.antId, value);
				nextArtyPaths.Add(key, artyPath);
			}
			foreach (var toDelete in artyPaths.Values.Except(nextArtyPaths.Values))
			{
				toDelete.QueueFree();
			}
			artyPaths = nextArtyPaths;




			var pings = new HashSet<PingState>();

			foreach (var (frame, antsResult) in state.Game.run.advanceResults)
			{
				if (frame > tick - StateAdvancer.PingLastsFor)
				{
					if (state.Game.run.advanceResults.TryGetValue(frame + 1, out var seeResult))
					{
						if (seeResult.sideSeesUnitFar.TryGetValue(networkedApp.index, out var seesUnitFar))
						{
							foreach (var sees in seesUnitFar)
							{
								if (antsResult.state.ants.TryGetValue(sees, out var ant))
								{
									pings.Add(new PingState(frame, ant.position, OrType.Make<Guid, ZapperSetupState.Immutable>(ant.immutable.id)));

								}
							}
						}
						if (seeResult.sideSeesZapperFar.TryGetValue(networkedApp.index, out var seesZapperFar))
						{
							foreach (var sees in seesZapperFar)
							{
								var zapperOrNull = antsResult.state.zapperSetupStates.Where(x => x.immutable.Equals(sees)).SingleOrDefault();
								if (zapperOrNull != null)
								{
									pings.Add(new PingState(frame, zapperOrNull.position, OrType.Make<Guid, ZapperSetupState.Immutable>(zapperOrNull.immutable)));

								}
							}
						}
					}
				}
			}

			//state.Game.run.advanceResults.SelectMany(x => x.Value.sideSeesUnitFar.TryGetValue(networkedApp.index, out var pings) ? pings.Select(y => new PingState(x.Key, y.position, y.immutable.id)) : Array.Empty<PingState>()).ToHashSet();

			// what goes in here vs what doesn't is a little random
			// probably the code above should be in here
			UpdateFromState(
				displayState.advanceResult.state,
				networkedApp.index,
				displayState.advanceResult.sideSeesUnit,
				pings,
				displayState.targetState,
				state.LosWorkerGroup,
				displayState.frame,
				state.Game.immutableSimulationState,
				displayState.advanceResult.sideSeesBullets,
				displayState.advanceResult.sideSeesHealBullets,
				displayState.advanceResult.sideSeesArtyBullets,
				displayState.advanceResult.sideSeesZapperFull,
				displayState.advanceResult.sideSeesUnitMid,
				displayState.advanceResult.sideSeesZapperMid,
				displayState.advanceResult.sideSeesArcBullets,
				displayState.advanceResult.sideSeesResources,
				displayState.advanceResult.sideSeesSmoke
				);

			while (state.Game.effectEvents.TryDequeue(out var effects))
			{
				eventHandler.Handle(effects, displayState.frame, this, networkedApp.index, displayState.advanceResult.state, state.Game.immutableSimulationState);
			}

			while (state.Game.confirmedEffects.TryDequeue(out var confirmedEffects))
			{
				eventHandler.HandleConfiredEvent(confirmedEffects, this, displayState.frame, networkedApp.index);
			}

			eventHandler.Clean(displayState.frame);


			stopwatch.Stop();
			DebugRunTimes.Add($"{nameof(StateToScene)}.{nameof(StateToScene._Process)}", stopwatch.ElapsedTicks, frame: displayState.frame);

		}

		selection.Visible = selectionStart != null;
		if (selectionStart != null)
		{
			var selectionEnd = MousePosition.CastPosition2d(this);
			var center = (selectionStart.Value + selectionEnd) / 2f;
			selection.GlobalPosition = new Vector3(center.X, center.Y, selection.GlobalPosition.Z);
			selection.Scale = new Vector3(Mathf.Abs(selectionStart.Value.X - selectionEnd.X), Mathf.Abs(selectionStart.Value.Y - selectionEnd.Y), 1);
		}


	}

	public static Guid StringToGuid(string name)
	{
		var stringBytes = name.ToAsciiBuffer();
		var guidBytes = new byte[16];

		for (int i = 0; i < stringBytes.Length; i++)
		{
			guidBytes[i % 16] += stringBytes[i];
		}
		return new Guid(guidBytes);
	}

	//private readonly ConcurrentDictionary<int, ShaderMaterial> shaders = new ConcurrentDictionary<int, ShaderMaterial>(); 


	public SimulationState lastState;
	public int frameOnState = 0;
	public int tick = 0;

	public void UpdateFromState(
		SimulationState state,
		int index,
		ConcurrentDictionary<int, HashSet<Guid>> sideSeesUnit,
		HashSet<PingState> pings,
		TargetState2 targetState,
		LosWorkerGroup losWorkerGroup,
		int tick,
		ImmutableSimulationState immutableSimulationState,
		IReadOnlyDictionary<int, HashSet<BulletState.Immutable.Id>> sideSeesBullets,
		IReadOnlyDictionary<int, HashSet<HealBulletState.Immutable.Id>> sideSeesHealBullets,
		IReadOnlyDictionary<int, HashSet<ArtyBulletState.Immutable.Id>> sideSeesArtyBullets,
		IReadOnlyDictionary<int, HashSet<ZapperSetupState.Immutable>> sideSeesZapper,
		ConcurrentDictionary<int, HashSet<Guid>> sideSeesUnitMid,
		IReadOnlyDictionary<int, HashSet<ZapperSetupState.Immutable>> sideSeesZapperMid,
		IReadOnlyDictionary<int, HashSet<ArcBulletState.Immutable.Id>> sideSeesArcBullets,
		IReadOnlyDictionary<int, HashSet<ResourceState.Immutable.Id>> sideSeesResources,
		IReadOnlyDictionary<int, HashSet<Guid>> sideSeesSmoke
		)
	{

		var weSeeMid = new HashSet<(Vector128<double> position, Vector128<double> velocity)>();
		if (sideSeesUnitMid.TryGetValue(index, out var weSeeUnitsMid))
		{
			foreach (var unitId in weSeeUnitsMid)
			{
				if (state.ants.TryGetValue(unitId, out var ant) && ant.immutable.side != index)
				{
					weSeeMid.Add((ant.Position, ant.velocity));
				}
			}
		}
		if (sideSeesZapperMid.TryGetValue(index, out var weSeeZapperMid))
		{
			foreach (var zapperId in weSeeZapperMid)
			{
				var zapperOrNull = state.zapperSetupStates.Where(x => x.immutable.Equals(zapperId)).SingleOrDefault();
				if (zapperOrNull != null &&
					state.ants.TryGetValue(zapperOrNull.immutable.createdBy, out var creator) &&
					creator.immutable.side != index)
				{
					weSeeMid.Add((zapperOrNull.Position, Vector128<double>.Zero));
				}
			}
		}

		this.tick = tick;

		if (state == lastState)
		{
			frameOnState++;
		}
		else
		{
			frameOnState = 0;
			lastState = state;
		}

		// rocks are immutable...
		//var rockTrackers = new List<ITrackState<BulletState>>();


		//      foreach (var item in GetChildren())
		//{
		//	if (item is ITrackState<BulletState> bulletTracker)
		//	{
		//		bulletTrackers.Add(bulletTracker);
		//	}
		//	else if (item is AntTracker antTracker)
		//	{
		//		antTrackers.Add(antTracker);
		//	}
		//	else if (item is StationTracker stationTracker)
		//	{
		//		stationTrackers.Add(stationTracker);
		//	}
		//	else if (item is ControlPoint2 controlPoint) {
		//		controlPointTrackers.Add(controlPoint);

		//          }
		//}

		var weSeeUnit = WeSeeUnit(index, sideSeesUnit);

		#region Rings
		{

			var selectAnts = GetSelectedAnts(state).ToDictionary(x => x.immutable.id, x => x);
			foreach (var selectedAnt in selectAnts.Values)
			{
				if (selectedAnt.zapperGun != null && selectedAnt.zapperGun.target != null)
				{
					var ring = ringMaster.GetRing(AntFactory.AmbushPreditorRadius);
					ring.GlobalPosition = selectedAnt.zapperGun.target.Value.ToVector3(50);
				}
			}

			if (pendingClickOrders.Is1(out var orders))
			{
				foreach (var (order, id) in orders)
				{
					if (selectAnts.TryGetValue(id, out var ant))
					{
						if (order.SafeIs(out GrenadeNeedsClick grenade))
						{
							var ring = ringMaster.GetRing((float)ant.grenadeLauncher.immuntable.maxRange);
							ring.GlobalPosition = ant.position.ToVector3(50);
						}

						if (order.SafeIs(out ZapperNeedsClick zap))
						{
							var ring = ringMaster.GetRing((float)ant.zapperState.immutable.range);
							ring.GlobalPosition = ant.position.ToVector3(50);
						}
						if (order.SafeIs(out SmokeNeedsClick smoke))
						{
							var ring = ringMaster.GetRing((float)SmokePlacerState.range);
							ring.GlobalPosition = ant.position.ToVector3(50);
						}
						// maybe sprint
					}
				}
			}

			//int i = 0;
			//var nextRings = new List<Ring>();
			//for (; i < ringsPositions.Length; i++)
			//{
			//	var ring = GetRing(i);
			//	ring.GlobalPosition = ringsPositions[i].ToVector3(50);
			//	nextRings.Add(ring);
			//}
			//// maybe just hide them 
			//// or it there are a lot of extras,
			//// ok then we remove
			//for (; i < rings.Count; i++)
			//{
			//	rings[i].QueueFree();
			//}
			//rings = nextRings;
		}

		ringMaster.Clean();

		#endregion Rings

		#region Pings
		{

			var stopwatch = new Stopwatch();
			stopwatch.Start();

			var pingStates = pings.ToArray();
			int i = 0;
			var nextPingTrackers = new List<Ping>();
			for (; i < pingStates.Length; i++)
			{
				var pingState = pingStates[i];
				var tracker = PingGetTracker(i);
				tracker.Update(pingState, tick);
				nextPingTrackers.Add(tracker);
			}
			// maybe just hide them 
			// or it there are a lot of extras,
			// ok then we remove
			for (; i < pingsTrackers.Count; i++)
			{
				pingsTrackers[i].QueueFree();
			}
			pingsTrackers = nextPingTrackers;
			stopwatch.Stop();
			DebugRunTimes.Add($"{nameof(StateToScene)}.{nameof(StateToScene.UpdateFromState)} - update pings", stopwatch.ElapsedTicks);
		}
		#endregion

		#region Mids
		{

			var stopwatch = new Stopwatch();
			stopwatch.Start();

			var midStates = weSeeMid.ToArray();
			int i = 0;
			var nextMidTrackers = new List<MidTracker>();
			for (; i < midStates.Length; i++)
			{
				var midState = midStates[i];
				var tracker = GetMidTracker(i);
				tracker.Update(midState.position, midState.velocity, this);
				nextMidTrackers.Add(tracker);
			}
			// maybe just hide them 
			// or it there are a lot of extras,
			// ok then we remove
			for (; i < midTrackers.Count; i++)
			{
				midTrackers[i].QueueFree();
			}
			midTrackers = nextMidTrackers;
			stopwatch.Stop();
			DebugRunTimes.Add($"{nameof(StateToScene)}.{nameof(StateToScene.UpdateFromState)} - update mids", stopwatch.ElapsedTicks);
		}
		#endregion

		#region ZapperSetups
		{
			var stopwatch = new Stopwatch();
			stopwatch.Start();

			var weSee = sideSeesZapper.TryGetValue(index, out var inline2) ? inline2 : new HashSet<ZapperSetupState.Immutable>();

			var zapperSetupStates = state.zapperSetupStates.Where(x => weSee.Contains(x.immutable)).ToArray();
			int i = 0;
			var nextZapperSetupTrackerTrackers = new List<ZapperSetupTracker>();
			for (; i < zapperSetupStates.Length; i++)
			{
				var zapperSetupState = zapperSetupStates[i];
				var tracker = GetZapperSetupTracker(i);
				tracker.Update(zapperSetupState, tick, state);
				nextZapperSetupTrackerTrackers.Add(tracker);
			}
			// maybe just hide them 
			// of it there are a lot of extras,
			// ok then we remove
			for (; i < zapperSetupTrackers.Count; i++)
			{
				zapperSetupTrackers[i].QueueFree();
			}
			zapperSetupTrackers = nextZapperSetupTrackerTrackers;
			stopwatch.Stop();
			DebugRunTimes.Add($"{nameof(StateToScene)}.{nameof(StateToScene.UpdateFromState)} - update zapper setups", stopwatch.ElapsedTicks);
		}
		#endregion


		#region Ants
		{

			var antDict = state.ants.ToDictionary(x => x.Key, x => x.Value);

			var nextAntTrackers = new List<AntTracker>();

			var stopwatch = new Stopwatch();
			stopwatch.Start();

			foreach (var tracker in antTrackers)
			{
				if (tracker.Tracks() != null)
				{
					if (antDict.TryGetValue(tracker.Tracks().immutable.id, out var antState))
					{
						tracker.TrackAndUpdate(antState, state, index,
							weSeeUnit.Contains(antState.immutable.id),
							targetState,
							frameOnState);
						antDict.Remove(antState.immutable.id);
						nextAntTrackers.Add(tracker);
					}
					else
					{
						losWorkerGroup.CleanUp(tracker.Tracks().immutable.id);
						foreach (var pair in assigned)
						{
							if (pair.Value.assignedTo == tracker)
							{
								assigned.Remove(pair.Key);
							}
						}
					   
						tracker.CastTo<Node>().QueueFree();
					}
				}
				else
				{
					Log.WriteLine("tracker was tracking nothing");
				}
			}

			antTrackers = nextAntTrackers;

			stopwatch.Stop();
			DebugRunTimes.Add($"{nameof(StateToScene)}.{nameof(StateToScene.UpdateFromState)} - update remove ants", stopwatch.ElapsedTicks);

			stopwatch = new Stopwatch();
			stopwatch.Start();

			foreach (var (_, antToMake) in antDict)
			{
				var instance = antScene.Instantiate<AntTracker>();
				antTrackers.Add(instance);

				var playerIndex = antToMake.immutable.side;

				AddChild(instance);

				instance.TrackAndUpdate(antToMake, state, index,
					weSeeUnit.Contains(antToMake.immutable.id),
					targetState,
					frameOnState);

				if (index == antToMake.immutable.side)
				{
					AddHotKey(instance);
				}

				//var x = instance.GetChild<MeshInstance3D>(0).GetSurfaceOverrideMaterial(0).Duplicate(true) as ShaderMaterial;
				//var x = instance.unitModel.GetSurfaceOverrideMaterial(0).Duplicate(true) as ShaderMaterial;
				//x.SetShaderParameter("input_player", playerIndex);
				//instance.unitModel.SetSurfaceOverrideMaterial(0, x);

			}


			stopwatch.Stop();
			DebugRunTimes.Add($"{nameof(StateToScene)}.{nameof(StateToScene.UpdateFromState)} - add ants", stopwatch.ElapsedTicks);

			// this is a weird way to pass in rocks...
			var cantSeeThrought = immutableSimulationState.rocks.Select(rock => (rock.position, rock.physicsImmunatbleInner.radius))
							.Union(state.smokes.Values.Select(smoke => (smoke.position, smoke.radius))).ToArray();

			losWorkerGroup.UpdateWork(
				state.ants.Values
					.Where(x => x.immutable.side == index)
					.SelectMany(ant => ant.SeesFrom().Select((eye, eyeIndex) => (ant, eye, eyeIndex)))
					.Select(pair => (
						pair.ant.immutable.id,
						pair.eye.ToVector2(),
						cantSeeThrought,
						pair.ant.immutable.lineOfSightFar,
						pair.ant.immutable.lineOfSight,
						pair.ant.immutable.lineOfSightDetection,
						tick,
						pair.eyeIndex
						)));

			foreach (var tracker in antTrackers)
			{
				var eyes = tracker.Tracks().SeesFrom().Count();
				for (int i = 0; i < eyes; i++)
				{
					if (losWorkerGroup.lastUpdate.TryGetValue((tracker.Tracks().immutable.id, i), out var meshes))
					{
						tracker.UpdateMeshes(meshes);
					}
				}
			}
		}
		#endregion

		#region Bullets
		{
			var bulletDict = state.bullets.ToDictionary(x => x.Key, x => x.Value);

			var nextBulletTrackers = new List<Bullet2>();

			var stopwatch = new Stopwatch();
			stopwatch.Start();

			foreach (var tracker in bulletTrackers)
			{
				var tracks = tracker.Tracks();
				var key = tracks.immutable.id;
				if (bulletDict.TryGetValue(key, out var x))
				{
					tracker.Track(this, x, sideSeesBullets.TryGetValue(index, out var sideSees) ? sideSees : new HashSet<BulletState.Immutable.Id>());
					bulletDict.Remove(key);
					nextBulletTrackers.Add(tracker);
				}
				else
				{
					var node = tracker.CastTo<Node>();
					node.QueueFree();
				}
			}

			bulletTrackers = nextBulletTrackers;

			stopwatch.Stop();
			DebugRunTimes.Add($"{nameof(StateToScene)}.{nameof(StateToScene.UpdateFromState)} - update remove bullets", stopwatch.ElapsedTicks);

			stopwatch = new Stopwatch();
			stopwatch.Start();


			foreach (var (_, bulletToMake) in bulletDict)
			{
				var instance = bulletScene.Instantiate<Bullet2>();

				instance.RotationOrder = EulerOrder.Zyx;
				instance.Rotation = new Vector3(0, 0, bulletToMake.velocity.ToVector2().Angle());


				AddChild(instance);
				instance.Track(this, bulletToMake, sideSeesBullets.TryGetValue(index, out var sideSees) ? sideSees : new HashSet<BulletState.Immutable.Id>());
				bulletTrackers.Add(instance);
			}


			stopwatch.Stop();
			DebugRunTimes.Add($"{nameof(StateToScene)}.{nameof(StateToScene.UpdateFromState)} - add bullets", stopwatch.ElapsedTicks);
		}
		#endregion


		#region ArcBullets
		{
			var stopwatch = new Stopwatch();
			stopwatch.Start();

			if (!sideSeesArcBullets.TryGetValue(index, out var weSee))
			{
				weSee = new HashSet<ArcBulletState.Immutable.Id>();
			}

			var arcBulletStates = state.arcBulletStates.Values.Where(x => weSee.Contains(x.immutable.id)).ToArray();
			int i = 0;
			var nextArcBulletTrackers = new List<ArcBulletTracker>();
			for (; i < arcBulletStates.Length; i++)
			{

				var arcBulletState = arcBulletStates[i];
				var tracker = GetArcBulletTracker(i);
				tracker.Update(arcBulletState, this);
				nextArcBulletTrackers.Add(tracker);
			}
			// maybe just hide them 
			// or it there are a lot of extras,
			// ok then we remove
			for (; i < arcBulletTrackers.Count; i++)
			{
				arcBulletTrackers[i].QueueFree();
			}
			arcBulletTrackers = nextArcBulletTrackers;
			stopwatch.Stop();
			DebugRunTimes.Add($"{nameof(StateToScene)}.{nameof(StateToScene.UpdateFromState)} - update arc bullets", stopwatch.ElapsedTicks);
		}
		#endregion

		#region Heal Bullets
		{
			var bulletDict = state.healBulletStates.ToDictionary(x => x.Key, x => x.Value);

			var nextHealBulletTrackers = new List<HealBulletTracker>();

			var stopwatch = new Stopwatch();
			stopwatch.Start();

			foreach (var tracker in healBulletTrackers)
			{
				var tracks = tracker.Tracks();
				var key = tracks.immutable.id;
				if (bulletDict.TryGetValue(key, out var x))
				{
					tracker.Track(this, x, sideSeesHealBullets.TryGetValue(index, out var sideSees) ? sideSees : new HashSet<HealBulletState.Immutable.Id>());
					bulletDict.Remove(key);
					nextHealBulletTrackers.Add(tracker);
				}
				else
				{
					tracker.CastTo<Node>().QueueFree();
				}
			}

			healBulletTrackers = nextHealBulletTrackers;

			stopwatch.Stop();
			DebugRunTimes.Add($"{nameof(StateToScene)}.{nameof(StateToScene.UpdateFromState)} - update remove heal bullets", stopwatch.ElapsedTicks);

			stopwatch = new Stopwatch();
			stopwatch.Start();


			foreach (var (_, bulletToMake) in bulletDict)
			{
				var instance = healBulletScene.Instantiate<HealBulletTracker>();

				instance.RotationOrder = EulerOrder.Zyx;
				instance.Rotation = new Vector3(0, 0, bulletToMake.velocity.ToVector2().Angle());


				AddChild(instance);
				instance.Track(this, bulletToMake, sideSeesHealBullets.TryGetValue(index, out var sideSees) ? sideSees : new HashSet<HealBulletState.Immutable.Id>());
				healBulletTrackers.Add(instance);
			}

			stopwatch.Stop();
			DebugRunTimes.Add($"{nameof(StateToScene)}.{nameof(StateToScene.UpdateFromState)} - add heal bullets", stopwatch.ElapsedTicks);
		}
		#endregion

		#region Arty Bullets
		{
			var bulletDict = state.artyBulletStates.ToDictionary(x => x.Key, x => x.Value);

			var nextArtyBulletTrackers = new List<ArtyBulletTracker>();

			var stopwatch = new Stopwatch();
			stopwatch.Start();

			foreach (var tracker in artyBulletTrackers)
			{
				var tracks = tracker.Tracks();
				var key = tracks.immutable.id;
				if (bulletDict.TryGetValue(key, out var x))
				{
					tracker.Track(this, x, sideSeesArtyBullets.TryGetValue(index, out var sideSees) ? sideSees : new HashSet<ArtyBulletState.Immutable.Id>());
					bulletDict.Remove(key);
					nextArtyBulletTrackers.Add(tracker);
				}
				else
				{
					tracker.CastTo<Node>().QueueFree();
				}
			}

			artyBulletTrackers = nextArtyBulletTrackers;

			stopwatch.Stop();
			DebugRunTimes.Add($"{nameof(StateToScene)}.{nameof(StateToScene.UpdateFromState)} - update remove arty bullets", stopwatch.ElapsedTicks);

			stopwatch = new Stopwatch();
			stopwatch.Start();


			foreach (var (_, bulletToMake) in bulletDict)
			{
				var instance = artyBulletScene.Instantiate<ArtyBulletTracker>();

				instance.RotationOrder = EulerOrder.Zyx;
				instance.Rotation = new Vector3(0, 0, bulletToMake.velocity.ToVector2().Angle());


				AddChild(instance);
				instance.Track(this, bulletToMake, sideSeesArtyBullets.TryGetValue(index, out var sideSees) ? sideSees : new HashSet<ArtyBulletState.Immutable.Id>());
				artyBulletTrackers.Add(instance);
			}

			stopwatch.Stop();
			DebugRunTimes.Add($"{nameof(StateToScene)}.{nameof(StateToScene.UpdateFromState)} - add arty bullets", stopwatch.ElapsedTicks);
		}
		#endregion

		#region Grenades
		{
			var grenadeDict = state.grenadeStates.ToDictionary(x => x.id, x => x);

			var nextGrenadeTrackers = new List<GrenadeTracker>();

			var stopwatch = new Stopwatch();
			stopwatch.Start();

			foreach (var tracker in grenadeTrackers)
			{
				var tracks = tracker.Tracks();
				var key = tracks.id;
				if (grenadeDict.TryGetValue(key, out var x))
				{
					tracker.Track(this, x);
					grenadeDict.Remove(key);
					nextGrenadeTrackers.Add(tracker);
				}
				else
				{
					tracker.CastTo<Node>().QueueFree();
				}
			}

			grenadeTrackers = nextGrenadeTrackers;

			stopwatch.Stop();
			DebugRunTimes.Add($"{nameof(StateToScene)}.{nameof(StateToScene.UpdateFromState)} - update remove grenades", stopwatch.ElapsedTicks);

			stopwatch = new Stopwatch();
			stopwatch.Start();


			foreach (var (_, grenadeToMake) in grenadeDict)
			{
				var instance = grenadeScene.Instantiate<GrenadeTracker>();

				instance.RotationOrder = EulerOrder.Zyx;
				instance.Rotation = new Vector3(0, 0, grenadeToMake.velocity.ToVector2().Angle());

				AddChild(instance);
				instance.Track(this, grenadeToMake);
				grenadeTrackers.Add(instance);
			}


			stopwatch.Stop();
			DebugRunTimes.Add($"{nameof(StateToScene)}.{nameof(StateToScene.UpdateFromState)} - add grenades", stopwatch.ElapsedTicks);
		}
		#endregion

		//#region Pre-Shockwaves
		//{
		//	var preShockwaveDict = state.preShockwaves.ToDictionary(x => x.id, x => x);

		//	var nextPreShockwaveTrackers = new List<PreShockwaveTracker>();

		//	var stopwatch = new Stopwatch();
		//	stopwatch.Start();

		//	foreach (var tracker in preShockwaveTrackers)
		//	{
		//		var tracks = tracker.Tracks();
		//		var key = tracks.id;
		//		if (preShockwaveDict.TryGetValue(key, out var x))
		//		{
		//			tracker.Track(this, x);
		//			preShockwaveDict.Remove(key);
		//			nextPreShockwaveTrackers.Add(tracker);
		//		}
		//		else
		//		{
		//			tracker.CastTo<Node>().QueueFree();
		//		}
		//	}

		//	preShockwaveTrackers = nextPreShockwaveTrackers;

		//	stopwatch.Stop();
		//	DebugRunTimes.Add($"{nameof(StateToScene)}.{nameof(StateToScene.UpdateFromState)} - update remove pre-shockwaves", stopwatch.ElapsedTicks);

		//	stopwatch = new Stopwatch();
		//	stopwatch.Start();


		//	foreach (var (_, preShockwaveToMake) in preShockwaveDict)
		//	{
		//		var instance = preShockwaveScene.Instantiate<PreShockwaveTracker>();

		//		//instance.RotationOrder = EulerOrder.Zyx;
		//		//instance.Rotation = new Vector3(0, 0, preShockwaveToMake.velocity.ToVector2().Angle());

		//		AddChild(instance);
		//		instance.Track(this, preShockwaveToMake);
		//		preShockwaveTrackers.Add(instance);
		//	}


		//	stopwatch.Stop();
		//	DebugRunTimes.Add($"{nameof(StateToScene)}.{nameof(StateToScene.UpdateFromState)} - add pre-shockwaves", stopwatch.ElapsedTicks);
		//}
		//#endregion

		#region Shockwaves
		{
			var shockwaveDict = state.shockwaves.ToDictionary(x => x.id, x => x);

			var nextShockwaveTrackers = new List<ShockwaveTracker>();

			var stopwatch = new Stopwatch();
			stopwatch.Start();

			foreach (var tracker in shockwaveTrackers)
			{
				var tracks = tracker.Tracks();
				var key = tracks.id;
				if (shockwaveDict.TryGetValue(key, out var x))
				{
					tracker.Track(this, x);
					shockwaveDict.Remove(key);
					nextShockwaveTrackers.Add(tracker);
				}
				else
				{
					tracker.CastTo<Node>().QueueFree();
				}
			}

			shockwaveTrackers = nextShockwaveTrackers;

			stopwatch.Stop();
			DebugRunTimes.Add($"{nameof(StateToScene)}.{nameof(StateToScene.UpdateFromState)} - update remove shockwaves", stopwatch.ElapsedTicks);

			stopwatch = new Stopwatch();
			stopwatch.Start();


			foreach (var (_, shockwaveToMake) in shockwaveDict)
			{
				var instance = shockwaveScene.Instantiate<ShockwaveTracker>();

				//instance.RotationOrder = EulerOrder.Zyx;
				//instance.Rotation = new Vector3(0, 0, shockwaveToMake.velocity.ToVector2().Angle());

				AddChild(instance);
				instance.Track(this, shockwaveToMake);
				shockwaveTrackers.Add(instance);
			}


			stopwatch.Stop();
			DebugRunTimes.Add($"{nameof(StateToScene)}.{nameof(StateToScene.UpdateFromState)} - add shockwaves", stopwatch.ElapsedTicks);
		}
		#endregion

		#region Resources
		{
			var resourceDict = state.resources.ToDictionary(x => x.immutable.id, x => x);

			var nextResourceTrackers = new List<ResourceTracker>();

			var stopwatch = new Stopwatch();
			stopwatch.Start();

			var seenResources = sideSeesResources.TryGetValue(index, out var sees) ? sees : new HashSet<ResourceState.Immutable.Id>();

			foreach (var tracker in resourceTrackers)
			{
				var tracks = tracker.Tracks();
				var key = tracks.immutable.id;
				if (resourceDict.TryGetValue(key, out var x))
				{
					tracker.Track(x, seenResources.Contains(x.immutable.id));
					resourceDict.Remove(key);
					nextResourceTrackers.Add(tracker);
				}
				else
				{
					tracker.CastTo<Node>().QueueFree();
				}
			}

			resourceTrackers = nextResourceTrackers;

			stopwatch.Stop();
			DebugRunTimes.Add($"{nameof(StateToScene)}.{nameof(StateToScene.UpdateFromState)} - update remove resources", stopwatch.ElapsedTicks);

			stopwatch = new Stopwatch();
			stopwatch.Start();


			foreach (var (_, resourceToMake) in resourceDict)
			{
				var instance = resourceScene.Instantiate<ResourceTracker>();

				AddChild(instance);
				instance.Track(resourceToMake, seenResources.Contains(resourceToMake.immutable.id));
				resourceTrackers.Add(instance);
			}


			stopwatch.Stop();
			DebugRunTimes.Add($"{nameof(StateToScene)}.{nameof(StateToScene.UpdateFromState)} - add resources", stopwatch.ElapsedTicks);
		}
		#endregion

		#region Smokes
		{
			var smokeDict = state.smokes.ToDictionary(x => x.Key, x => x.Value);

			var nextSmokeTrackers = new List<SmokeTracker>();

			var stopwatch = new Stopwatch();
			stopwatch.Start();

			foreach (var tracker in smokeTrackers)
			{
				var tracks = tracker.Tracks();
				var key = tracks.id;
				if (smokeDict.TryGetValue(key, out var x))
				{
					tracker.Track(x, sideSeesSmoke.TryGetValue(index, out var sideSees) ? sideSees.Contains(x.id) : false);
					smokeDict.Remove(key);
					nextSmokeTrackers.Add(tracker);
				}
				else
				{
					tracker.CastTo<Node>().QueueFree();
				}
			}

			smokeTrackers = nextSmokeTrackers;

			stopwatch.Stop();
			DebugRunTimes.Add($"{nameof(StateToScene)}.{nameof(StateToScene.UpdateFromState)} - update remove smokes", stopwatch.ElapsedTicks);

			stopwatch = new Stopwatch();
			stopwatch.Start();


			foreach (var (_, smokeToMake) in smokeDict)
			{
				var instance = smokeScene.Instantiate<SmokeTracker>();

				AddChild(instance);
				instance.Track(smokeToMake, sideSeesSmoke.TryGetValue(index, out var sideSees) ? sideSees.Contains(smokeToMake.id) : false);
				smokeTrackers.Add(instance);
			}


			stopwatch.Stop();
			DebugRunTimes.Add($"{nameof(StateToScene)}.{nameof(StateToScene.UpdateFromState)} - add smokes", stopwatch.ElapsedTicks);
		}
		#endregion

		#region Stations
		{
			var stationDict = state.Stations.Select(x => x.position).ToHashSet();

			var nextStationTrackers = new List<StationTracker>();

			foreach (var tracker in stationTrackers)
			{
				if (stationDict.TryGetValue(tracker.Tracks(), out var position))
				{
					stationDict.Remove(position);
					nextStationTrackers.Add(tracker);
				}
				else
				{
					tracker.CastTo<Node>().QueueFree();
				}
			}

			stationTrackers = nextStationTrackers;

			foreach (var stationsToMake in stationDict)
			{
				var instance = stationScene.Instantiate<StationTracker>();
				AddChild(instance);

				instance.GlobalPosition = new Vector3((float)stationsToMake.X(), (float)stationsToMake.Y(), 0);
				instance.Track(stationsToMake);
				stationTrackers.Add(instance);
			}
		}
		#endregion

		#region Control Points
		{
			var controlPointDict = state.controlPoints.ToDictionary(x => x.position, x => x);


			var nextControlPointsTrackers = new List<ControlPoint2>();

			foreach (var tracker in controlPointTrackers)
			{
				if (controlPointDict.TryGetValue(tracker.Tracks().position, out var x))
				{
					tracker.Update(x);
					controlPointDict.Remove(x.position);
					nextControlPointsTrackers.Add(tracker);
				}
				else
				{
					tracker.CastTo<Node>().QueueFree();

				}
			}

			controlPointTrackers = nextControlPointsTrackers;

			foreach (var (_, controlPointToMake) in controlPointDict)
			{
				var instance = controlPointScene.Instantiate<ControlPoint2>();

				AddChild(instance);
				instance.Track(this, controlPointToMake);

				controlPointTrackers.Add(instance);
			}
		}
		#endregion

		#region Paths
		{
			var stopwatch = new Stopwatch();
			stopwatch.Start();
			foreach (var path in paths)
			{
				path.QueueFree();
			}
			paths = new List<UnitPath>();

			foreach (var item in GetSelectedAnts(state))
			{
				if (item.activeOrder.SafeIs(out MoveOrderState move))
				{
					var path = new UnitPath();
					AddChild(path);
					var destinations = new List<Vector3>();
					destinations.Add(item.position.ToVector3(50));
					foreach (var destination in move.destinations.Select(x => x.ToVector3(50)))
					{
						destinations.Add(destination);
					}

					path.Init(destinations.ToArray());
					paths.Add(path);

				}
			}

			stopwatch.Stop();
			DebugRunTimes.Add($"{nameof(StateToScene)}.{nameof(StateToScene.UpdateFromState)} - add paths", stopwatch.ElapsedTicks);
		}
		#endregion
	}

	private static HashSet<Guid> WeSeeUnitMid(int index, ConcurrentDictionary<int, HashSet<Guid>> sideSeesUnitMid)
	{
		if (!sideSeesUnitMid.TryGetValue(index, out var weSeeUnitMid))
		{
			weSeeUnitMid = new HashSet<Guid>();
		}
		return weSeeUnitMid;
	}

	private static HashSet<Guid> WeSeeUnit(int index, ConcurrentDictionary<int, HashSet<Guid>> sideSeesUnit)
	{
		if (!sideSeesUnit.TryGetValue(index, out var weSeeUnit))
		{
			weSeeUnit = new HashSet<Guid>();
		}
		return weSeeUnit;
	}

	private Ping PingGetTracker(int i)
	{
		if (i < pingsTrackers.Count)
		{
			return pingsTrackers[i];
		}
		else
		{
			var ping = pingScene.Instantiate<Ping>();
			AddChild(ping);
			return ping;
		}
	}

	//private Ring GetRing(int i)
	//{
	//	if (i < rings.Count)
	//	{
	//		return rings[i];
	//	}
	//	else
	//	{
	//		var ring = ringScene.Instantiate<Ring>();

	//		AddChild(ring);
	//		// this is actually bad code
	//		ring.Init(AntFactory.AmbushPreditorRadius);

	//		return ring;
	//	}
	//}

	private ArcBulletTracker GetArcBulletTracker(int i)
	{
		if (i < arcBulletTrackers.Count)
		{
			return arcBulletTrackers[i];
		}
		else
		{
			var arcBulletTracker = arcBulletScene.Instantiate<ArcBulletTracker>();
			AddChild(arcBulletTracker);
			return arcBulletTracker;
		}
	}

	private MidTracker GetMidTracker(int i)
	{
		if (i < midTrackers.Count)
		{
			return midTrackers[i];
		}
		else
		{
			var mid = midScene.Instantiate<MidTracker>(); ;
			AddChild(mid);
			return mid;
		}
	}

	private ZapperSetupTracker GetZapperSetupTracker(int i)
	{
		if (i < zapperSetupTrackers.Count)
		{
			return zapperSetupTrackers[i];
		}
		else
		{
			var scene = zapperSetupScene.Instantiate<ZapperSetupTracker>(); ;
			AddChild(scene);
			return scene;
		}
	}

	private Vector2? selectionStart = null;
	public override void _UnhandledInput(InputEvent @event)
	{
		if (StaticAppState.TryGetState(out InGameState inGameState))
		{

			var ants = GetChildren().OfType<AntTracker>();

			if (@event is InputEventMouseButton mouseEvent)
			{
				if (mouseEvent.ButtonIndex == MouseButton.Left)
				{
					if (mouseEvent.Pressed)
					{
						if (!mouseEvent.ShiftPressed)
						{
							ClearSelection();
						}
						selectionStart = MousePosition.CastPosition2d(this);
					}
					else
					{
						if (selectionStart != null)
						{
							var mouse = MousePosition.CastPosition2d(this);

							// select everyone in a box

							var selectAny = false;

							foreach (var ant in ants)
							{
								var tracks = ant.Tracks();
								if (tracks != null)
								{
									// {2950D6BA-82A8-444D-BF4D-EA7B7A4CD342}
									// allow selecting any ant for testing
									//if (tracks.immutable.side == inGameState.index )
									{

										// this isn't quiet right
										// it treats ants like the are boxes
										// it can select ants that are near the coners of the box
										// ants whose circle doesn't intersect the box

										var radius = tracks.physicsImmunatbleInner.radius;
										if ((
											(selectionStart.Value.X >= ant.GlobalPosition.X - radius && ant.GlobalPosition.X + radius >= mouse.X) ||
											(selectionStart.Value.X <= ant.GlobalPosition.X + radius && ant.GlobalPosition.X - radius <= mouse.X)
											) && (
											(selectionStart.Value.Y >= ant.GlobalPosition.Y - ant.Tracks().physicsImmunatbleInner.radius && ant.GlobalPosition.Y + radius >= mouse.Y) ||
											(selectionStart.Value.Y <= ant.GlobalPosition.Y + ant.Tracks().physicsImmunatbleInner.radius && ant.GlobalPosition.Y - radius <= mouse.Y)))
										{
											selectAny = true;

											ant.selected = true;
										}
									}
								}
							}

							if (!selectAny)
							{
								var closest = ants
									// {2950D6BA-82A8-444D-BF4D-EA7B7A4CD342}
									// real code:
									.Where(x => x.Tracks() != null && x.Tracks().immutable.side == inGameState.index)
									.OrderBy(x => (new Vector2(x.GlobalPosition.X, x.GlobalPosition.Y) - mouse).LengthSquared())
									// test code:
									//.Where(x => x.Tracks() != null)
									//.OrderBy(x => (new Vector2(x.GlobalPosition.X, x.GlobalPosition.Y) - mouse).LengthSquared() * (x.Tracks().immutable.side == inGameState.index ? 1 : 10))
									.FirstOrDefault();

								if (closest != null)
								{
									closest.selected = true;
								}
							}

							selectionStart = null;
						}
					}
				}

			}
		}

		// TODO
		//{
		//    if (@event is InputEventKey keyEvent &&
		//        keyEvent.Pressed &&
		//        AutoHotKeyManager.TryGet(Ground.playerSide, keyEvent.Keycode, out var ant))
		//    {
		//        SelectionManager.ClearSelection();
		//        ant.Select();
		//    }
		//}

		if (@event is InputEventKey keyEvent &&
			   keyEvent.Pressed
			   )
		{
			//if (keyEvent.Keycode == Godot.Key.Key1)
			//{
			//	menu = Menu.commands;
			//	pendingClickOrders = OrType.Make<(IPendingClickOrderState, Guid id)[], EggKeyAction>(Array.Empty<(IPendingClickOrderState, Guid id)>());
			//}
			//else if (keyEvent.Keycode == Godot.Key.Key2)
			//{
			//	menu = Menu.build;
			//	pendingClickOrders = OrType.Make<(IPendingClickOrderState, Guid id)[], EggKeyAction>(Array.Empty<(IPendingClickOrderState, Guid id)>());
			//}
			//else if (keyEvent.Keycode == Godot.Key.Key3)
			//{
			//	menu = Menu.select;
			//	pendingClickOrders = OrType.Make<(IPendingClickOrderState, Guid id)[], EggKeyAction>(Array.Empty<(IPendingClickOrderState, Guid id)>());
			//}
			//else
			//{
			//if (IsCommandKey(keyEvent.Keycode))
			//{
				//if (menu == Menu.commands)
				//{
				//	if (StaticAppState.TryGetState(out InGameState state))
				//	{
				//		var key = keyEvent.Keycode;
				//		ConglomeratedGetKeyActions(state.Game.displayState.advanceResult.state, key)
				//			.Switch(keyClickActions =>
				//		{
				//			pendingClickOrders = OrType.Make<(IPendingClickOrderState, Guid id)[], EggKeyAction>(keyClickActions);
				//		}, keyActions =>
				//		{
				//			if (keyActions.Any())
				//			{
				//				var enque = Input.IsKeyPressed(Godot.Key.Shift);
				//				foreach (var (keyAction, id) in keyActions)
				//				{
				//					state.Game.Order(id, keyAction.GetPendingOrder(), enque);
				//				}
				//			}
				//		});
				//	}
				//}
				//else if (menu == Menu.build)
				//{
				//	// 49 minutes, act like you care
				//	var buildActions = BuildActions();

				//	if (buildActions.TryGetValue(keyEvent.Keycode, out var order)) 
				//	{
				//		var eggKeyAction = order.CastTo<EggKeyAction>();
				//		pendingClickOrders = OrType.Make<(IPendingClickOrderState, Guid id)[], EggKeyAction>(eggKeyAction);

				//	}
				//}
				//else if (menu == Menu.select)
				//{

				//if (keyEvent.ShiftPressed)
				//{
				Enact(keyEvent.Keycode, keyEvent.ShiftPressed, keyEvent.CtrlPressed, keyEvent.AltPressed, inGameState);
				//}
				//else if (keyEvent.CtrlPressed)
				//{
				//	RemoveFromSelection(keyEvent.Keycode);
				//}
				//else
				//{
				//	Select(keyEvent.Keycode, inGameState);
				//}
				//}
			//}
			//}

		}

		base._Input(@event);
	}


	private class FromAndPath
	{
		public readonly AntState ant;
		public readonly double pathLength;

		public FromAndPath(AntState ant, double pathLength)
		{
			this.ant = ant ?? throw new ArgumentNullException(nameof(ant));
			this.pathLength = pathLength;
		}
	}

	// TODO this should probably be an extension
	// so there is some hope of finding it
	private double Length(IEnumerable<Vector128<double>> res)
	{
		return res.SkipLast(1).Zip(res.Skip(1)).Select(x => Avx.Subtract(x.First, x.Second).Length()).Sum();
	}

	private delegate Task OnRightClick(bool enque, Vector128<double> clicked);

	IOrType<(IPendingClickOrderState, Guid id)[], EggKeyAction> pendingClickOrders = OrType.Make<(IPendingClickOrderState, Guid id)[], EggKeyAction>(Array.Empty<(IPendingClickOrderState, Guid id)>());
	//    OnRightClick onRightClick;
	//Action<Vector128<double>> onFrame;

	public override void _Input(InputEvent @event)
	{
		if (StaticAppState.TryGetState(out InGameState state))
		{
			var myTrackedAnts = GetChildren().OfType<AntTracker>()
						.Where(x =>
							x.Tracks() != null
							// {2950D6BA-82A8-444D-BF4D-EA7B7A4CD342}
							// allow selecting any ant for testing
							//&&
							//x.Tracks().immutable.side == state.index
							)
						.ToArray();

			if (@event is InputEventMouseButton mouseEvent && mouseEvent.Pressed)
			{
				if (mouseEvent.ButtonIndex == MouseButton.Left)
				{
					var toSelect = myTrackedAnts
						.Where(x =>
							Avx.Subtract(MousePosition.CastPosition2d(this).ToNumericsVector(), x.Tracks().position).Length() < x.Tracks().physicsImmunatbleInner.radius)
						.OrderBy(x => Avx.Subtract(MousePosition.CastPosition2d(this).ToNumericsVector(), x.Tracks().position).Length())
						.FirstOrDefault();

					if (toSelect != null)
					{
						if (!mouseEvent.ShiftPressed)
						{
							ClearSelection();
						}
						toSelect.selected = true;
					}
				}
				else if (mouseEvent.ButtonIndex == MouseButton.Right)
				{
					var enque = Input.IsKeyPressed(Godot.Key.Shift);

					var position = MousePosition.CastPosition2d(this).ToNumericsVector();

					pendingClickOrders.Switch(orders =>
					{
						if (orders.Any())
						{
							foreach (var (action, id) in orders)
							{
								if (state.Game.displayState.advanceResult.state.ants.TryGetValue(id, out var ant))
								{
									Order(enque, action, position, ant, state.Game, state.Game.displayState.advanceResult.state, state.Game.immutableSimulationState, new[] { ant });
								}
							}
						}
						else
						{
							Move(state, enque, position);
						}

					}, eggKeyAction =>
					{
						var size = AntFactory.AntSize(eggKeyAction.unpackInto);
						var mudMode = AntFactory.MudMode(eggKeyAction.unpackInto);

						var builders = antTrackers
							.Select(x => x.Tracks())
							.Where(x => x != null)
							.Where(x => x.immutable.side == state.index)
							.Where(x => new[] { AntType.Flag }.Contains(x.immutable.type))
							.ToArray();

						var tasks = builders.Select(builder =>
						{
							return Task.Run(() =>
							{
								if (Pathing.GetPathWithFristPoint(state.Game.immutableSimulationState.paths.knownPaths[(mudMode, size)], builder.position, position, out var res))
								{
									return new FromAndPath(builder, Length(res));
								}
								return null;
							});
						}).ToArray();
						Task.WhenAll(tasks).ContinueWith(result =>
						{
							if (result.IsFaulted)
							{
								Log.WriteLine(result.Exception.ToString());
								return;
							}

							var list = result.Result.Where(x => x != null).ToArray();
							if (!list.Any())
							{
								Log.WriteLine("I tired to make a unit, but there was no path, or all the builders were killed or something");
								return;
							}

							var ant = list.OrderBy(x => x.pathLength).First().ant;

							Order(enque, eggKeyAction, position, ant, state.Game, state.Game.displayState.advanceResult.state, state.Game.immutableSimulationState, new[] { ant });
						});
					});

					pendingClickOrders = OrType.Make<(IPendingClickOrderState, Guid id)[], EggKeyAction>(Array.Empty<(IPendingClickOrderState, Guid id)>());
				}
			}
		}
	}

	private void Move(InGameState state, bool enque, Vector128<double> position)
	{
		var hasDefaultActions = antTrackers
													.Where(x => x.selected)
													.Select(x => x.Tracks())
													.ToArray();

		if (hasDefaultActions.Any())
		{
			var avoidables = state.Game.immutableSimulationState.rocks.Select(x => (x.position, x.physicsImmunatbleInner.radius)).ToList();

			foreach (var orderedUnit in hasDefaultActions)
			{
				var myAvoidables = orderedUnit.immutable.mudMode == Mode.ImpassableMud ? avoidables.Union(state.Game.immutableSimulationState.mud.Select(x => (x.position, x.physicsImmunatbleInner.radius - orderedUnit.physicsImmunatbleInner.radius))).ToList()
																						: avoidables;

				if (Pathing.TryAdjustTarget(position, myAvoidables, orderedUnit.physicsImmunatbleInner.radius, out var toAt))
				{
					Order(enque, new MoveAction(), toAt, orderedUnit, state.Game, state.Game.displayState.advanceResult.state, state.Game.immutableSimulationState, hasDefaultActions);
				}
			}
		}
	}

	//private void DefaultOnFrame(Vector128<double> mousePosition) {
	//	if (StaticAppState.TryGetState(out InGameState innerState))
	//	{
	//		innerState.Game.displayState.artyTargetingUi = new Dictionary<Guid, Vector3[]>();
	//	}
	//}

	static Vector128<double> FindNextAt(Vector128<double> perpendicular, Vector128<double> back, List<(Vector128<double> GlobalPosition, double Radius)> myAvoidables, Vector128<double> middleAt, Vector128<double> at, List<Vector128<double>> placed, AntState unit)
	{
		if (Pathing.TryAdjustTarget(Avx.Add(at, perpendicular), myAvoidables, unit.physicsImmunatbleInner.radius, out at))
		{
			return at;
		}

		foreach (var placedAt in Enumerable.Reverse(placed))
		{
			if (Pathing.TryAdjustTarget(Avx.Add(placedAt, back), myAvoidables, unit.physicsImmunatbleInner.radius, out at))
			{
				return at;
			}
		}
		return middleAt;

	}

	private static Task Order(bool enque, IPendingClickOrderState pendingOrder, Vector128<double> at, AntState unit, Game game, SimulationState state, ImmutableSimulationState immutableSimulationState, AntState[] alsoOrdered)
	{
		return Task.Run(() =>
		{
			if (pendingOrder.TryOrder(at, state, unit, game, immutableSimulationState, alsoOrdered, out var order))
			{
				game.Order(unit.immutable.id, order, enque);
			}
			else
			{
				Log.WriteLine("cant execute order!");
			}
		}).ContinueWith(x =>
		{
			if (x.IsFaulted)
			{
				Log.WriteLine($"order failed {x.Exception}");
			}
		});
	}

	//public Dictionary<Godot.Key, IPendingClickOrderState> BuildActions() {
	//	if (!StaticAppState.TryGetState(out HasPlayersAppState networkedApp)) {
	//		return new Dictionary<Godot.Key, IPendingClickOrderState>();
	//	}

	//	var builders = antTrackers.Select(x => x.Tracks()).Where(x => x != null).Where(x => x.immutable.side == networkedApp.index).Where(x => new[] { AntType.Flag }.Contains(x.immutable.type)).ToArray();

	//	if (builders.Length == 0)
	//	{
	//		return new Dictionary<Godot.Key, IPendingClickOrderState>();
	//	}

	//	Debug.Assert(builders.Select(x => x.immutable.type).Distinct().Count() < 2);

	//	var rep = builders.First();

	//	return rep.immutable.keyClickActions.ToDictionary(x=>(Godot.Key)x.Key.keyCode,x=>x.Value);
	//}

	//public OrType<(IPendingClickOrderState, Guid id)[], (IKeyAction action, Guid id)[]> ConglomeratedGetKeyActions(SimulationState state, Godot.Key key)
	//{
	//	var keyClickActions = GetKeyClickActions(state, key);

	//	if (keyClickActions.Any())
	//	{
	//		return OrType.Make<(IPendingClickOrderState, Guid id)[], (IKeyAction action, Guid id)[]>(keyClickActions);
	//	}
	//	else
	//	{
	//		return OrType.Make<(IPendingClickOrderState, Guid id)[], (IKeyAction action, Guid id)[]>(GetKeyActions(state, key));
	//	}
	//}

	//public (IKeyAction action, Guid id)[] GetKeyActions(SimulationState state, Godot.Key key)
	//{

	//	var selectedAnts = GetSelectedAnts(state);

	//	var keyActions = selectedAnts
	//				  .Select(x => (x.immutable.actions.TryGetValue(key.ToDomainKey(), out var res), res, x.immutable.id))
	//				  .Where(x => x.Item1)
	//				  .Select(x => (x.res, x.id))
	//				  .ToArray();

	//	// TODO pick a winner
	//	if (keyActions.Select(x => x.res.GetType()).Distinct().Count() > 1)
	//	{
	//		return Array.Empty<(IKeyAction, Guid)>();
	//	}

	//	return keyActions;

	//}

	public AntState[] GetSelectedAnts(SimulationState state)
	{
		return antTrackers.Where(x => x.selected).Select(x => x.Tracks()).ToArray();
		//return state.ants.Values.Where(x => NonSimulationState.state.TryGetState(x.immutable.id, out var state) && state.selected).ToArray();
	}

	//public (IPendingClickOrderState, Guid id)[] GetKeyClickActions(/*AntTracker[] myTrackedAnts,*/ SimulationState state, Godot.Key key)
	//{
	//	var selectedAnts = GetSelectedAnts(state);

	//	if (!selectedAnts.Any(x => x.immutable.keyClickActions.TryGetValue(key.ToDomainKey(), out var res)))
	//	{
	//		return Array.Empty<(IPendingClickOrderState, Guid id)>();
	//	}

	//	var actions = selectedAnts
	//			.Select(x => {
	//					var hasAction = x.immutable.keyClickActions.TryGetValue(key.ToDomainKey(), out var res);
	//					return (res, x.immutable.id, hasAction);
	//				})
	//			.Where(x=>x.hasAction)
	//			.Select(x=>(x.res, x.id))
	//			.ToArray();

	//	if (actions.Select(x => x.Item1.GetType()).Distinct().Count() > 1)
	//	{
	//		return Array.Empty<(IPendingClickOrderState, Guid id)>();
	//	}

	//	return actions;



	// //      var list = new List<(IPendingClickOrderState, Guid id)>();
	//	//foreach (var ant in selectedAnts)
	//	//{
	//	//	if (ant.immutable.keyClickActions.TryGetValue(key.ToDomainKey(), out var res) && GetActionGroupIndex(res.GetType()) == winningActionGroupIndex)
	//	//	{
	//	//		list.Add((res, ant.immutable.id));
	//	//	}
	//	//	else if (ant.immutable.defultAction != null && GetActionGroupIndex(ant.immutable.defultAction.GetType()) == winningActionGroupIndex)
	//	//	{
	//	//		list.Add((ant.immutable.defultAction, ant.immutable.id));
	//	//	}
	//	//}
	//	//return list.ToArray();
	//}

	// I don't really enjoy this
	// ActionType very much
	// maybe a priority thing
	// and some actions mover around

	// we assign number that express priority
	// if a group has an ant with move on A and and ant with shoot on A
	// we move 
	//private enum ActionType
	//{
	//	move = 0,
	//	shoot = 1,
	//	place = 2,
	//}

	//private static Dictionary<Type, ActionType> ActionGroups = new Dictionary<Type, ActionType> {
	//	{typeof(MoveAction),ActionType.move},
	//	{typeof(MoveWithShiledUpNeedsClick),ActionType.move},
	//	{typeof(SprintNeedsClick),ActionType.move},
	//	{typeof(ShootNeedsClick),ActionType.shoot },
	//	{typeof(GrenadeNeedsClick),ActionType.shoot },
	//	{typeof(SmokeNeedsClick),ActionType.shoot },
	//	{typeof(EggKeyAction),ActionType.place },
	//	{typeof(ShockwaveNeedsClick),ActionType.shoot },
	//	{typeof(ArtyNeedsClick),ActionType.shoot }
	//};

	//private static int GetActionGroupIndex(Type type)
	//{
	//	return (int)ActionGroups[type];
	//}

	//private bool NotMove(IEnumerable<IPendingClickOrderState> actions)
	//{
	//	if (actions.Any(x => GetActionGroupIndex(x.GetType()) == 0))
	//	{
	//		if (!actions.All(x => GetActionGroupIndex(x.GetType()) == 0))
	//		{
	//			throw new Exception("if any are move actions they all should be");
	//		}
	//		return false;
	//	}
	//	return true;
	//}

	public Vector128<double> PredictedPosition(AntState ant, bool enqueue)
	{
		if (!enqueue)
		{
			return ant.position;
		}

		var orderedTo = ant.pendingOrders.SelectMany(x =>
		{
			if (x.SafeIs(out MovePendingImmutableState move))
			{
				return new[] { (x, move.path.Last()) };
			}
			if (x.SafeIs(out PendingMoveWithShiledUpOrderImmutableState moveWithShieldUp))
			{
				return new[] { (x, moveWithShieldUp.inner.path.Last()) };
			}
			if (x.SafeIs(out SprintPendingOrder sprint))
			{
				return new[] { (x, sprint.inner.path.Last()) };
			}
			return Array.Empty<(IPendingOrderState, Vector128<double>)>();
		}).ToArray();

		if (orderedTo.Any())
		{
			return orderedTo.Last().Item2;
		}
		return ant.position;
	}

	public AntState FindMiddle(AntState[] orderedUnits)
	{

		var totalWidth = orderedUnits.Sum(x => x.physicsImmunatbleInner.radius * 2);
		var at = 0.0;
		foreach (var item in orderedUnits)
		{
			at += item.physicsImmunatbleInner.radius * 2.0;
			if (at > totalWidth / 2.0)
			{
				return item;
			}
		}
		throw new Exception("did find the middle...");
	}

	public static ConcurrentLinkedList<(Vector2, float, Color)> DebugToDraw = new ConcurrentLinkedList<(Vector2, float, Color)>();

	internal void ClearSelection()
	{
		foreach (var ant in GetChildren().OfType<AntTracker>())
		{
			ant.selected = false;
		}
		pendingClickOrders = OrType.Make<(IPendingClickOrderState, Guid id)[], EggKeyAction>(Array.Empty<(IPendingClickOrderState, Guid id)>());
	}


	private class RingMaster
	{
		private readonly PackedScene ringScene = GD.Load<PackedScene>("res://Pages (Scenes and Code)/Match/UI/Ring.tscn");
		private readonly Node addTo;
		private readonly Dictionary<float, RingsOfSize> backing = new Dictionary<float, RingsOfSize>();

		public RingMaster(Node addTo)
		{
			this.addTo = addTo ?? throw new ArgumentNullException(nameof(addTo));
		}

		public Ring GetRing(float radius)
		{
			if (backing.TryGetValue(radius, out var ringsOfSize))
			{
				return ringsOfSize.GetRing();
			}
			else
			{
				ringsOfSize = new RingsOfSize(radius, ringScene, addTo);
				backing.Add(radius, ringsOfSize);
				return ringsOfSize.GetRing();
			}
		}

		public void Clean()
		{
			foreach (var value in backing.Values)
			{
				value.Clean();
			}
		}

		private class RingsOfSize
		{
			public readonly float raduis;
			private List<Ring> rings = new List<Ring>();
			private List<Ring> nextRings = new List<Ring>();
			private int at = 0;
			private readonly PackedScene ringScene;
			private readonly Node addTo;

			public RingsOfSize(float raduis, PackedScene ringScene, Node addTo)
			{
				this.raduis = raduis;
				this.ringScene = ringScene ?? throw new ArgumentNullException(nameof(ringScene));
				this.addTo = addTo ?? throw new ArgumentNullException(nameof(addTo));
			}

			public Ring GetRing()
			{
				if (at < rings.Count)
				{
					var ring = rings[at];
					at++;
					nextRings.Add(ring);
					return ring;
				}
				else
				{
					at++;
					var ring = ringScene.Instantiate<Ring>();
					addTo.AddChild(ring);
					ring.Init(raduis);
					nextRings.Add(ring);
					return ring;
				}
			}

			public void Clean()
			{
				for (; at < rings.Count; at++)
				{
					rings[at].QueueFree();
				}
				rings = nextRings;
				nextRings = new List<Ring>();
				at = 0;
			}
		}
	}
}


public class PingState
{
	public readonly int tick;
	public readonly Vector128<double> position;
	public readonly IOrType<Guid, ZapperSetupState.Immutable> id;

	public PingState(int tick, Vector128<double> position, IOrType<Guid, ZapperSetupState.Immutable> id)
	{
		this.tick = tick;
		this.position = position;
		this.id = id;
	}

	public override bool Equals(object obj)
	{
		return obj is PingState state &&
			   tick == state.tick &&
			   position.Equals(state.position) &&
			   id.Equals(state.id);
	}

	public override int GetHashCode()
	{
		return HashCode.Combine(tick, position, id);
	}
}

// this is a dumb interface
public interface ITrackState<T>
{
	public T Tracks();
}
