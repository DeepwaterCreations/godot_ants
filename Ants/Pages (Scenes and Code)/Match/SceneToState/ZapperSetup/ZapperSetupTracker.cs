using Ants.Domain.State;
using Godot;
using System;

internal partial class ZapperSetupTracker : Node3D
{
	internal void Update(ZapperSetupState zapperSetupState, int tick,SimulationState state)
	{
		var part = state.ants.TryGetValue(zapperSetupState.immutable.createdBy, out var ant) ? Math.Clamp(1 - ((tick - zapperSetupState.immutable.createdOnTick) / (float)ant.zapperState.immutable.setUpTime), 0, 1)
																							 : 0;
		this.Scale = new Vector3(15 * part, 15 * part, 15 * part);

		var position = zapperSetupState.position.ToVector2();
		this.GlobalPosition = new Vector3(position.X, position.Y, 0);
	}
}
