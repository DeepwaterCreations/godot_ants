using Ants.Domain.State;
using Godot;
using System;
using System.Collections.Concurrent;

public partial class ControlPoint2 : MeshInstance3D, ITrackState<ControlPointState>
{
	private static readonly ConcurrentDictionary<int, ShaderMaterial> shaders = new ConcurrentDictionary<int, ShaderMaterial>();

	private ControlPointState controlPoint;
	public ControlPointState Tracks() => controlPoint;

	public void Track(StateToScene manager, ControlPointState controlPoint)
	{
		if (this.controlPoint != null)
		{
			throw new Exception("it's not going to cause any trouble as far as I konw, but seems werid for this to be called more than once");
		}

		Update(controlPoint);
	}

	public void Update(ControlPointState controlPoint) {
		this.controlPoint = controlPoint;
		var position2d = controlPoint.position.ToVector2();
		this.GlobalPosition = new Vector3(position2d.X, position2d.Y, GlobalPosition.Z);

		//var myShader = shaders.GetOrAdd(controlPoint.side, _ =>
		//{
		//	var x = this.GetSurfaceOverrideMaterial(0).Duplicate(true) as ShaderMaterial;
		//	x.SetShaderParameter("input_player", controlPoint.side);
		//	return x;
		//});
		//SetSurfaceOverrideMaterial(0, myShader);
	}
}

