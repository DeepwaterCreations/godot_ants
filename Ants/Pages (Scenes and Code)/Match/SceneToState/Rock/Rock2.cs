using Godot;
using System;

public partial class Rock2 : Node3D, ITrackState<RockState>
{
	private RockState rockState;

	public RockState Tracks() => rockState;

	public void Track(StateToScene manager, RockState rockState)
	{

		this.rockState = rockState;
		var position2d = rockState.position.ToVector2();
		this.GlobalPosition = new Vector3(position2d.X, position2d.Y, GlobalPosition.Z);
		var r = new Random();
		this.RotateX(r.Next() * Mathf.Tau);
		this.RotateY(r.Next() * Mathf.Tau);
		this.RotateZ(r.Next() * Mathf.Tau);
	}


	public float Radius => Transform[0][0]/2f;

}
