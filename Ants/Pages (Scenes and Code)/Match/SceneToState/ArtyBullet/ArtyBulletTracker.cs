using Ants.Domain;
using Godot;
using System.Collections.Generic;
using System.Linq;

public partial class ArtyBulletTracker : Node3D, ITrackState<ArtyBulletState>
{
	private ArtyBulletState bulletState;
	private StateToScene manager;

	public ArtyBulletState Tracks() => bulletState;

	public void Track(StateToScene manager, ArtyBulletState bulletState,
		HashSet<ArtyBulletState.Immutable.Id> sideSeesBullets)
	{
		this.manager = manager;
		this.bulletState = bulletState;
		var position2d = bulletState.position.ToVector2() + (bulletState.velocity.ToVector2() * (manager.frameOnState * (float)FramesPerSecond.framesPerSecond / 60));
		this.GlobalPosition = new Vector3(position2d.X, position2d.Y, GlobalPosition.Z);
		// would be nice if this was a dictionary
		this.Visible = sideSeesBullets.Contains(bulletState.immutable.id);
		this.RotationOrder = EulerOrder.Zyx;
		this.Rotation = new Vector3(0, 0, bulletState.velocity.ToVector2().Angle());

	}

	public override void _Process(double delta)
	{
		base._Process(delta);
		if (bulletState != null)
		{
			var position2d = bulletState.position.ToVector2() + (bulletState.velocity.ToVector2() * (manager.frameOnState * (float)FramesPerSecond.framesPerSecond / 60));
			this.GlobalPosition = new Vector3(position2d.X, position2d.Y, GlobalPosition.Z);
		}
	}
}

