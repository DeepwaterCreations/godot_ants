using Ants.Domain;
using Godot;
using System;
using System.Collections.Generic;
using System.Linq;

public partial class Bullet2 : Node3D, ITrackState<BulletState>
{
	private BulletState bulletState;
	private StateToScene manager;

	public BulletState Tracks() => bulletState;

	public void Track(StateToScene manager, BulletState bulletState,
		HashSet<BulletState.Immutable.Id> sideSeesBullets)
	{
		this.manager = manager;
		this.bulletState = bulletState;
		var position2d = bulletState.position.ToVector2() + (bulletState.velocity.ToVector2() * (manager.frameOnState* (float)FramesPerSecond.framesPerSecond/60));
		this.GlobalPosition = new Vector3(position2d.X, position2d.Y, GlobalPosition.Z);
		// would be nice if this was a dictionary
		this.Visible = sideSeesBullets.Contains(bulletState.immutable.id);

	}

	public override void _Process(double delta)
	{
		base._Process(delta);
		if (bulletState != null)
		{

			var position2d = bulletState.position.ToVector2() + (bulletState.velocity.ToVector2() * (manager.frameOnState * (float)FramesPerSecond.framesPerSecond / 60));
			this.GlobalPosition = new Vector3(position2d.X, position2d.Y, GlobalPosition.Z);
		}
	}
}
