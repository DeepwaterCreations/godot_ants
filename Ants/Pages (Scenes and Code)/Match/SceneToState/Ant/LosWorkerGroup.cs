﻿using Ants.Domain;
using Godot;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Intrinsics;
using System.Threading.Tasks;
using static AntTracker;

public class LosWorkerGroup {
	private LosWorker[] workers;
	ShaderMaterial shaderMaterialFar;
	ShaderMaterial shaderMaterialLos;
	ShaderMaterial shaderMaterialDetection;
    StandardMaterial3D mat;

    private ConcurrentQueue<(Guid id, Vector2 myPosition, (Vector128<double> position, double radius)[] rocks, double lineOfSightFar, double lineOfSight, double lineOfSightDetection, int tick, int index)> requests = new();

	public ConcurrentDictionary<(Guid,int), (ArrayMesh far, ArrayMesh los, ArrayMesh detection, Vector2 myPosition, int lastUpdateTime, int index)> lastUpdate = new();
	public LosWorkerGroup(int count) {
		workers = new LosWorker[count];
		for (int i = 0; i < count; i++)
		{
			workers[i] = new LosWorker(this);
		}
		// might need 3 copies of the shader, not sure
		var shader = GD.Load<Shader>("res://Shaders/simple.gdshader");

        mat = new StandardMaterial3D();
        mat.VertexColorUseAsAlbedo = true;


        shaderMaterialFar = new ShaderMaterial()
		{
			Shader = shader
		};
		shaderMaterialFar.SetShaderParameter("input_color", Palette.losFar);
		shaderMaterialLos = new ShaderMaterial()
		{
			Shader = shader
		};
		shaderMaterialLos.SetShaderParameter("input_color", Palette.los);
		shaderMaterialDetection = new ShaderMaterial()
		{
			Shader = shader
		};
		shaderMaterialDetection.SetShaderParameter("input_color", Palette.losDetection);
	}

	// TODO cancellationTokenSource?
	public Task Process()
	{
		return Task.WhenAll(workers.Select(x => x.Process()));
	}

	public void UpdateWork(IEnumerable<(Guid id, Vector2 myPosition, (Vector128<double> position, double radius)[] rocks, double lineOfSightFar, double lineOfSight, double lineOfSightDetection, int tick, int index)> values)
	{
		requests = new ConcurrentQueue<(Guid id, Vector2 myPosition, (Vector128<double> position, double radius)[] rocks, double lineOfSightFar, double lineOfSight, double lineOfSightDetection, int tick, int index)>(values
			.Where(x => !lastUpdate.TryGetValue((x.id,x.index), out var current) || (current.myPosition - x.myPosition).LengthSquared() > 50*50 || x.tick - current.lastUpdateTime > FramesPerSecond.framesPerSecond)
			.OrderByDescending(x => lastUpdate.TryGetValue((x.id,x.index), out var current) ? (current.myPosition - x.myPosition).LengthSquared() : float.MaxValue)
            .ThenBy(x => lastUpdate.TryGetValue((x.id, x.index), out var current) ? current.lastUpdateTime : int.MinValue)
            .Take(1/*turn it up and down*/ * workers.Length)
            );
		foreach (var losWorker in workers)
		{
			losWorker.UpdateWork();
		}
	}

	public void CleanUp(Guid id) 
	{
        foreach (var key in lastUpdate.Keys.Where(x => x.Item1 == id).ToArray())
        {
            lastUpdate.TryRemove(key, out var _);
        } 
	}


    public class SegmentUpdater
    {

        //(ArrayMesh far, ArrayMesh los, ArrayMesh detection, Vector2 myPosition)? lastRes = null;
        //Task running = null;

        //// probably should kick these off as soon as we have the state
        //// so they have some hope of finishing on frame
        //internal (ArrayMesh far, ArrayMesh los, ArrayMesh detection, Vector2 myPosition) UpdateSegments(Vector2 myPosition, (Vector128<double> position, double radius)[] rocks, double lineOfSightFar, double lineOfSight, double lineOfSightDetection,
        //    ShaderMaterial shaderMaterialFar,
        //    ShaderMaterial shaderMaterialLos,
        //    ShaderMaterial shaderMaterialDetection)
        //{
        //    if (lastRes == null)
        //    {
        //        lastRes = UpdateRes(myPosition, rocks, lineOfSightFar, lineOfSight, lineOfSightDetection, shaderMaterialFar, shaderMaterialLos, shaderMaterialDetection);
        //        return (lastRes.Value.far, lastRes.Value.los, lastRes.Value.detection, lastRes.Value.myPosition);
        //    }

        //    if (myPosition != lastRes.Value.myPosition)
        //    {
        //        if (running == null || running.IsCompleted)
        //        {
        //            running = Task.Run(() =>
        //            {
        //                try
        //                {
        //                    lastRes = UpdateRes(myPosition, rocks, lineOfSightFar, lineOfSight, lineOfSightDetection, shaderMaterialFar, shaderMaterialLos, shaderMaterialDetection);
        //                }
        //                catch
        //                {
        //                    Log.WriteLine("the segments thing blew up!");
        //                }
        //            });
        //        }
        //    }

        //    return (lastRes.Value.far, lastRes.Value.los, lastRes.Value.detection, lastRes.Value.myPosition);
        //}

        public static (ArrayMesh far, ArrayMesh los, ArrayMesh detection, Vector2 myPosition, int lastUpdateTime, int index) UpdateRes(
            Vector2 myPosition, 
            (Vector128<double> position, double radius)[] rocks, 
            double lineOfSightFar, 
            double lineOfSight, 
            double lineOfSightDetection,
            ShaderMaterial shaderMaterialFar,
            ShaderMaterial shaderMaterialLos,
            ShaderMaterial shaderMaterialDetection,
            Material mat,
            int tick, 
            int index)
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();

            var segmentsDetection = new SegmentsRewrite();
            var segments = new SegmentsRewrite();
            var segmentsFar = new SegmentsRewrite();

            foreach (var rock in rocks)
            {

                var vector = rock.position.ToVector2() - myPosition;
                if (vector.Length() < lineOfSightFar)
                {
                    // dup code
                    // {0EFDD1AF-64B2-4850-A4A1-EFFF6533272C}
                    var hypotinose = vector.Length();
                    var oppisite = (float)rock.radius;

                    if (hypotinose <= oppisite) {
                        // you're in the rock 
                        var res = (
    ArrayMesh( Segment.Make(0, Math.Tau, 0), (float)lineOfSightFar, shaderMaterialFar),
    ArrayMesh(Segment.Make(0, Math.Tau, 0) , (float)lineOfSight, shaderMaterialLos),
    ArrayMesh(Segment.Make(0, Math.Tau, 0) , (float)lineOfSightDetection, shaderMaterialDetection),
    myPosition,
    tick,
    index
    );

                        stopwatch.Stop();
                        DebugRunTimes.Add("SegmentUpdater.UpdateRes", stopwatch.ElapsedTicks);

                        return res;
                    }

                    var angleNearThis = Mathf.Asin(oppisite / hypotinose);
                    var angleNearItem = (Mathf.Pi / 2f) - angleNearThis;


                    var oneSide = vector + ((-vector).Normalized().Rotated(angleNearItem) * oppisite);
                    var otherSide = vector + ((-vector).Normalized().Rotated(-angleNearItem) * oppisite);

                    var newSegments = Segment.Make(oneSide.Angle(), otherSide.Angle(), oneSide.Length()).ToList();

                    segmentsFar.AddRange(newSegments);

                    if (vector.Length() < lineOfSight)
                    {
                        segments.AddRange(newSegments);

                        if (vector.Length() < lineOfSightDetection)
                        {
                            segmentsDetection.AddRange(newSegments);
                        }
                    }
                }
            }
            {
                var res = (
                    ArrayMesh(SplitBigSegments(FillInGaps(segmentsFar.AsEnumerable(), (float)lineOfSightFar)).ToArray(), (float)lineOfSightFar, shaderMaterialFar),
                    ArrayMesh(SplitBigSegments(FillInGaps(segments.AsEnumerable(), (float)lineOfSight)).ToArray(), (float)lineOfSight, shaderMaterialLos),
                    ArrayMesh(SplitBigSegments(FillInGaps(segmentsDetection.AsEnumerable(), (float)lineOfSightDetection)).ToArray(), (float)lineOfSightDetection, shaderMaterialDetection),
                    myPosition,
                    tick,
                    index);

                stopwatch.Stop();
                DebugRunTimes.Add("SegmentUpdater.UpdateRes", stopwatch.ElapsedTicks);

                return res;
            }
        }

        public static ArrayMesh ArrayMesh(IEnumerable<Segment> segments, float maxLos, Material shaderMaterial)
        {
            var surfaceTool = new SurfaceTool();
            surfaceTool.Begin(Mesh.PrimitiveType.Triangles);
            var up = new Vector3(0, 0, 1);
            var tan = new Plane(new Vector3(1, 0, 0));

            var r = new Random();

            foreach (var segment in segments)
            {
                var left = new Vector3(
                    (float)(Math.Cos(segment.angle1) * segment.distance),
                    (float)(Math.Sin(segment.angle1) * segment.distance),
                    0);
                var right = new Vector3(
                    (float)(Math.Cos(segment.angle2) * segment.distance),
                    (float)(Math.Sin(segment.angle2) * segment.distance),
                    0);
                var center = new Vector3(0, 0, 0);

                surfaceTool.SetNormal(up);
                surfaceTool.SetUV(ToUv(right));
                surfaceTool.SetTangent(tan);
                surfaceTool.SetColor(new Color((float)r.NextDouble(), (float)r.NextDouble(), (float)r.NextDouble()));
                surfaceTool.AddVertex(right);

                surfaceTool.SetNormal(up);
                surfaceTool.SetUV(ToUv(left));
                surfaceTool.SetTangent(tan);
                surfaceTool.SetColor(new Color((float)r.NextDouble(), (float)r.NextDouble(), (float)r.NextDouble()));
                surfaceTool.AddVertex(left);

                surfaceTool.SetNormal(up);
                surfaceTool.SetUV(ToUv(center));
                surfaceTool.SetTangent(tan);
                surfaceTool.SetColor(new Color((float)r.NextDouble(), (float)r.NextDouble(), (float)r.NextDouble()));
                surfaceTool.AddVertex(center);

            }

            surfaceTool.SetMaterial(shaderMaterial);

            //surfaceTool.GenerateTangents(); // TODO slow! - was like 9%

            var res = surfaceTool.Commit();
            return res;

            Vector2 ToUv(Vector3 vector)
            {
                return new Vector2(
                    .5f + (.5f * (vector.X / maxLos)),
                    .5f + (.5f * (vector.Y / maxLos))
                    );
            }
        }


        public static IEnumerable<Segment> FillInGaps(IEnumerable<Segment> segments, float los)
        {
            var at = 0.0;

            foreach (var segment in segments)
            {
                if (segment.angle1 != at)
                {
                    yield return Segment.MakeOrThrow(at, segment.angle1, los);
                }
                yield return segment;
                at = segment.angle2;
            }

            if (at < Math.Tau)
            {
                yield return Segment.MakeOrThrow(at, Math.Tau, los);
            }
        }

        public static IEnumerable<Segment> SplitBigSegments(IEnumerable<Segment> segments)
        {
            var maxArc = 100;
            var maxAngle = Mathf.Tau / 3f;
            foreach (var segment in segments)
            {
                var angleDiff = segment.angle2 - segment.angle1;
                var splitInTo = Mathf.Ceil(
                    Mathf.Max(
                        (segment.distance * angleDiff) / maxArc,
                        (segment.angle2 - segment.angle1) / maxAngle));
                if (splitInTo < 2)
                {
                    yield return segment;
                }


                for (int i = 0; i < splitInTo; i++)
                {
                    yield return Segment.MakeOrThrow(
                        Mathf.Max(0, segment.angle1 + (angleDiff * (i / splitInTo))), // 
                        Mathf.Min(Mathf.Tau, segment.angle1 + (angleDiff * ((i + 1) / splitInTo))), // sometimes this end up a little over Tau, i assume something something computer math
                        segment.distance
                        );
                }
            }
        }
    }


    public class LosWorker
	{
		private TaskCompletionSource hasInputs = new();

		private readonly LosWorkerGroup boss;

		public LosWorker(LosWorkerGroup boss)
		{
			this.boss = boss ?? throw new ArgumentNullException(nameof(boss));
		}

		public async Task Process()
		{
			try
			{
				while (true)
				{
					await hasInputs.Task;
					hasInputs = new TaskCompletionSource();

					while (boss.requests.TryDequeue(out var input))
					{
						boss.lastUpdate[(input.id,input.index)] = SegmentUpdater.UpdateRes(input.myPosition, input.rocks, input.lineOfSightFar, input.lineOfSight, input.lineOfSightDetection, boss.shaderMaterialFar, boss.shaderMaterialLos, boss.shaderMaterialDetection, boss.mat, input.tick, input.index);

						// not really sure if this is what I want
						// maybe I should check how many threads are available and if it is zero break out of here
						// maybe I want not limit myself to a fix number of processors
						await Task.Yield();
					}
				}
			}catch(Exception ex) {
				Log.WriteLine(ex.ToString());
			}
		}


		public void UpdateWork()
		{
			hasInputs.TrySetResult();
		}

	}
}
