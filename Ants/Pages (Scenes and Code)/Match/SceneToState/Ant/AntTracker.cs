using Ants.AppState;
using Ants.Domain;
using Ants.Domain.Infrastructure;
using Ants.Domain.State;
using Ants.Network;
using Godot;
using Prototypist.Toolbox.Object;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Intrinsics;
using System.Threading;
using System.Threading.Tasks;
using static AntTracker;




//public class NonSimulationState
//{

//	public static NonSimulationState state = new NonSimulationState();

//	private NonSimulationState() { }




//	public void Add(Guid id, NonSimulationAnt ant, bool playerControls)
//	{
//		ants.Add(id, ant);

//	}

//	internal bool TryGetState(Guid id, out NonSimulationAnt state) => ants.TryGetValue(id, out state);

//}



//public class NonSimulationAnt
//{

//	// error related to this, not sure why
//	//E 0:06:42:0327   void System.ThrowHelper.ThrowAddingDuplicateWithKeyArgumentException<T>(T): System.ArgumentException: An item with the same key has already been added. Key: f9b16ebb-4f83-4aa3-acf8-74b9e7052531
//	//<C++ Error>    System.ArgumentException
//	//<C++ Source>   :0 @ void System.ThrowHelper.ThrowAddingDuplicateWithKeyArgumentException<T>(T)
//	//<Stack Trace>  :0 @ void System.ThrowHelper.ThrowAddingDuplicateWithKeyArgumentException<T>(T)
//	//               :0 @ bool System.Collections.Generic.Dictionary`2.TryInsert(TKey, TValue, System.Collections.Generic.InsertionBehavior)
//	//               :0 @ void System.Collections.Generic.Dictionary`2.Add(TKey, TValue)
//	//               AntTracker.cs:31 @ void NonSimulationState.Add(System.Guid, NonSimulationAnt)
//	//               AntTracker.cs:44 @ NonSimulationAnt NonSimulationAnt.CreateAndAdd(System.Guid, NonSimulationState)
//	//               AntTracker.cs:182 @ void AntTracker.TrackAndUpdate(AntState, Ants.Domain.State.SimulationState, int, AntTracker+Visibility, Ants.Domain.State.TargetState2, int)
//	//               StateToScene.cs:384 @ void StateToScene.UpdateFromState(Ants.Domain.State.SimulationState, int, System.Collections.Concurrent.ConcurrentDictionary`2[System.Int32, AntState[]], System.Collections.Generic.HashSet`1[PingState], Ants.Domain.State.TargetState2, LosWorkerGroup, int, Ants.Domain.State.ImmutableSimulationState, System.Collections.Generic.IReadOnlyDictionary`2[System.Int32, System.Collections.Generic.IReadOnlyList`1[BulletState]])
//	//               StateToScene.cs:203 @ void StateToScene._Process(double)
//	//               Node.cs:2131 @ bool Godot.Node.InvokeGodotClassMethod(Godot.NativeInterop.godot_string_name&, Godot.NativeInterop.NativeVariantPtrArgs, Godot.NativeInterop.godot_variant&)
//	//               Node3D.cs:1036 @ bool Godot.Node3D.InvokeGodotClassMethod(Godot.NativeInterop.godot_string_name&, Godot.NativeInterop.NativeVariantPtrArgs, Godot.NativeInterop.godot_variant&)
//	//               StateToScene_ScriptMethods.generated.cs:78 @ bool StateToScene.InvokeGodotClassMethod(Godot.NativeInterop.godot_string_name&, Godot.NativeInterop.NativeVariantPtrArgs, Godot.NativeInterop.godot_variant&)
//	//               CSharpInstanceBridge.cs:24 @ Godot.NativeInterop.godot_bool Godot.Bridge.CSharpInstanceBridge.Call(nint, Godot.NativeInterop.godot_string_name*, Godot.NativeInterop.godot_variant**, int, Godot.NativeInterop.godot_variant_call_error*, Godot.NativeInterop.godot_variant*)

//	//when you create a NonSimulationAnt, the NonSimulationAnt should track it
//	public static NonSimulationAnt CreateAndAdd(Guid id, NonSimulationState state, bool playerControls)
//	{
//		var res = new NonSimulationAnt();
//		state.Add(id, res, playerControls);
//		return res;
//	}
//	private NonSimulationAnt() { }
//	public bool selected = false;
//}

public partial class AntTracker : Node3D
{

	// maybe I should have a antTracker and a EditorAnt
	// EditorAnt is destoried by StateToScene and    with a antTracker
	// it just seems like this has two personalities
	// some fields here are for the editor side
	// and some are for the ongoing tracking side
	[Export]
	public AntType antType;
	// this should only be used by scene to state
	// I think these should be a different class like AntToBe
	// and they are destoryed once they are loaded in to state
	// and then the state makes the ant trackers 
	[Export]
	public int side;

	[Export]
	public MeshInstance3D hp;

	[Export]
	public MeshInstance3D SelectedIndicator;

	// nullable
	//public NonSimulationAnt nonSimulationAnt;

	private AntState antState;
	private MeshInstance3D[] meshFar;
	private MeshInstance3D[] mesh;
	private MeshInstance3D[] meshDetection;
	private MeshInstance3D shieldMesh;
	//private ShaderMaterial shaderMaterialLos;
	//private ShaderMaterial shaderMaterialFar;
	//private ShaderMaterial shaderMaterialDetection;
	public Node3D unitModel;// TODO AntModelController
	public MeshInstance3D shieldModel;
	public MeshInstance3D gunModel;

	//public DebugLine gathering;


	public bool selected = false;


	public override void _Ready()
	{
		base._Ready();
	}

	//private readonly SegmentUpdater segmentUpdater = new SegmentUpdater();


	public AntState Tracks() => antState;


	public void InitMeshesIfNeeded() {

		int points = antState.SeesFrom().Count();

		if (this.meshFar == null)
		{
			var list = new List<MeshInstance3D>();
			for (int i = 0; i < points; i++)
			{
				var mesh = new MeshInstance3D();
                mesh.Layers = 4096; //los
                list.Add(mesh);
				AddChild(mesh);
				this.meshFar = list.ToArray();
			}
		}
		else {
			Debug.Assert(this.meshFar.Length == points);
		}

		if (this.mesh == null)
		{
			var list = new List<MeshInstance3D>();
			for (int i = 0; i < points; i++)
			{
				var mesh = new MeshInstance3D();
                mesh.Layers = 4096; //los
                list.Add(mesh);
				AddChild(mesh);
				this.mesh = list.ToArray();
			}
		}
		else
		{
			Debug.Assert(this.mesh.Length == points);
		}

		if (this.meshDetection == null)
		{
			var list = new List<MeshInstance3D>();
			for (int i = 0; i < points; i++)
			{
				var mesh = new MeshInstance3D();
				mesh.Layers = 4096; //los
                list.Add(mesh);
				AddChild(mesh);
				this.meshDetection = list.ToArray();
			}
		}
		else
		{
			Debug.Assert(this.meshDetection.Length == points);
		}
	}

	public void Track(AntState antState, int playerSide)
	{
		this.antState = antState;
		this.antType = antState.immutable.type;
		//AddNonSimulationStateIfNeeded(this.antState.immutable.side == playerSide);
		InitMeshesIfNeeded();
	}

	private void AddModelIfNeeded()
	{
		if (this.unitModel == null)
		{
			{
				var toMake = AntFactory.Model(antState.immutable.type, antState.physicsImmunatbleInner.radius);
				var packedScene = GD.Load<PackedScene>(toMake);
				unitModel = packedScene.Instantiate<Node3D>();
				AddChild(unitModel);
			}
			float selectorSize = ((float)antState.physicsImmunatbleInner.radius * 2) + 20;
			SelectedIndicator.Transform = new Transform3D(
				new Vector3(selectorSize, 0, 0),
				new Vector3(0, 0, 1),
				new Vector3(0, -selectorSize, 0),
				SelectedIndicator.Transform.Origin);
			hp.Position = new Vector3(0, (float)antState.physicsImmunatbleInner.radius + 20, hp.Position.Z);

			var material = new StandardMaterial3D() {
				AlbedoColor = Palette.playerColors[antState.immutable.side],
			};
			//var shader = GD.Load<Shader>("res://Shaders/antDefault.gdshader");
			//var shaderMaterial = new ShaderMaterial()
			//{
			//	Shader = shader
			//};
			//shaderMaterial.SetShaderParameter("input_player", antState.immutable.side);
			//shaderMaterial.SetShaderParameter("normal_map", "res://.godot/imported/R.jpg-d2aeb37e7dd5f62d172decb3ef49759f.s3tc.ctex");
			hp.MaterialOverride = material;

			var shieldModelPath = AntFactory.SheildModel(antState.immutable.type);
			if (shieldModelPath != null) {
				var packedScene = GD.Load<PackedScene>(shieldModelPath);
				shieldModel = packedScene.Instantiate<MeshInstance3D>();

				AddChild(shieldModel);
			}

			var gunModelPath = AntFactory.GunModel(antState.immutable.type);
			if (gunModelPath != null)
			{
				var packedScene = GD.Load<PackedScene>(gunModelPath);
				gunModel = packedScene.Instantiate<MeshInstance3D>();

				AddChild(gunModel);
			}
		}
	}



	public void TrackAndUpdate(AntState antState, SimulationState state, int playerSide, bool visibility, TargetState2 targetState, int frameOnState)
	{
		Track(antState, playerSide);

		//var lastMeshPositionFar = meshFar?.GlobalPosition ?? new Vector3(0,0,0);
		//var lastMeshPosition = mesh?.GlobalPosition ?? new Vector3(0, 0, 0); 
		//var lastMeshPositionDetection = meshDetection?.GlobalPosition ?? new Vector3(0, 0, 0); ;

		var position2d = antState.position.ToVector2() + (antState.velocity.ToVector2() * (frameOnState * (float)FramesPerSecond.framesPerSecond) / 60);
		GlobalPosition = new Vector3(position2d.X, position2d.Y, GlobalPosition.Z);
		AddModelIfNeeded();

  //      if (antState.resouceGatherer != null)
		//{
		//	if (gathering == null)
		//	{
		//		var packedScene = GD.Load<PackedScene>("res://Pages (Scenes and Code)/Match/SceneToState/Ant/DebugLine.tscn");
		//		gathering = packedScene.Instantiate<DebugLine>();
		//		AddChild(gathering);
		//	}

		//	if (antState.resouceGatherer.collectFrom.HasValue && antState.immutable.side == playerSide)
		//	{
		//		gathering.Visible = true;
		//		var to = antState.resouceGatherer.collectFrom.Value.ToVector2();

		//		gathering.UpdateLine(GlobalPosition + new Vector3(0, 0, 15), new Vector3(to.X, to.Y, 0));
		//	}
		//	else
		//	{
		//		gathering.Visible = false;
		//	}
		//}
		//if (meshFar != null)
		//{
		//	meshFar.GlobalPosition = lastMeshPositionFar;
		//}
		//if (mesh != null)
		//{
		//	mesh.GlobalPosition = lastMeshPosition;
		//}
		//if (meshDetection != null)
		//{
		//	meshDetection.GlobalPosition = lastMeshPositionDetection;
		//}

		hp.Scale = new Vector3((float)antState.hp, hp.Scale.Y, hp.Scale.Z);
		SelectedIndicator.Visible = selected;

		//if (antState.immutable.side == side)
		//{

		//if (this.meshFar == null)
		//{
		//	this.meshFar = new MeshInstance3D();
		//	AddChild(this.meshFar);


		//	var shader = GD.Load<Shader>("res://Shaders/simple.gdshader");
		//	shaderMaterialFar = new ShaderMaterial()
		//	{
		//		Shader = shader
		//	};
		//	shaderMaterialFar.SetShaderParameter("input_color", Palette.losFar);
		//}

		//if (mesh == null)
		//{
		//	mesh = new MeshInstance3D();
		//	AddChild(mesh);

		//	var shader = GD.Load<Shader>("res://Shaders/simple.gdshader");
		//	shaderMaterialLos = new ShaderMaterial()
		//	{
		//		Shader = shader
		//	};
		//	shaderMaterialLos.SetShaderParameter("input_color", Palette.los);

		//}

		//if (meshDetection == null)
		//{
		//	meshDetection = new MeshInstance3D();
		//	AddChild(meshDetection);

		//	var shader = GD.Load<Shader>("res://Shaders/simple.gdshader");
		//	shaderMaterialDetection = new ShaderMaterial()
		//	{
		//		Shader = shader
		//	};
		//	shaderMaterialDetection.SetShaderParameter("input_color", Palette.losDetection);
		//}

		//var (arrayMeshFar, arrayMeshLos, arrayMeshDetection, position) = segmentUpdater.UpdateSegments(
		//	new Vector2(GlobalPosition.X, GlobalPosition.Y),
		//	state.rocks,
		//	antState.immutable.lineOfSightFar,
		//	antState.immutable.lineOfSight,
		//	antState.immutable.lineOfSightDetection,
		//	shaderMaterialFar,
		//	shaderMaterialLos,
		//	shaderMaterialDetection);

		//meshFar.Mesh = arrayMeshFar;
		//meshFar.GlobalPosition = new Vector3(position.X, position.Y, -65);
		//mesh.Mesh = arrayMeshLos;
		//mesh.GlobalPosition = new Vector3(position.X, position.Y, -60);
		//meshDetection.Mesh = arrayMeshDetection;
		//meshDetection.GlobalPosition = new Vector3(position.X, position.Y, -55);

		//}

		// shieldModel == null &&
		if (shieldMesh == null && antState.shield != null)
		{
			shieldMesh = new MeshInstance3D();
			AddChild(shieldMesh);
			shieldMesh.Position = new Vector3(0, 0, 0);
			var shader = GD.Load<Shader>("res://Shaders/simple.gdshader");
			var shaderMaterialShield = new ShaderMaterial()
			{
				Shader = shader
			};
			shaderMaterialShield.SetShaderParameter("input_color", Palette.shieldHint);
			var surfaceTool = new SurfaceTool();
			surfaceTool.Begin(Mesh.PrimitiveType.Triangles);


			// this isn't really the correct normal
			var up = new Vector3(0, 0, 1);

			float rad = (float)(antState.physicsImmunatbleInner.radius * 1.5);

			var left = new Vector3(
				(float)(Mathf.Cos(antState.shield.baseImmutable.armorAngleRads / 2f) * rad),
				(float)(Mathf.Sin(antState.shield.baseImmutable.armorAngleRads / 2f) * rad),
				0);
			var right = new Vector3(
				(float)(Mathf.Cos(antState.shield.baseImmutable.armorAngleRads / 2f) * rad),
				(float)(-Mathf.Sin(antState.shield.baseImmutable.armorAngleRads / 2f) * rad),
				0);
			var center = new Vector3(0, 0, 0);

			surfaceTool.SetNormal(up);
			surfaceTool.SetUV(new Vector2((left.X + rad) / (2 * rad), (left.Y + rad) / (2 * rad)));
			surfaceTool.AddVertex(left);

			surfaceTool.SetNormal(up);
			surfaceTool.SetUV(new Vector2((right.X + rad) / (2 * rad), (right.Y + rad) / (2 * rad)));
			surfaceTool.AddVertex(right);

			surfaceTool.SetNormal(up);
			surfaceTool.SetUV(new Vector2((center.X + rad) / (2 * rad), (center.Y + rad) / (2 * rad)));
			surfaceTool.AddVertex(center);



			surfaceTool.SetMaterial(shaderMaterialShield);
			//surfaceTool.GenerateNormals();
			surfaceTool.GenerateTangents();

			shieldMesh.Mesh = surfaceTool.Commit();

		}

		if (shieldMesh != null)
		{
			var anlge = antState.shield.facingUnit.Angle();
			shieldMesh.Rotation = new Vector3(0, 0, (float)anlge);

			if (antState.shield is ShieldState shieldState)
			{
				var progress = (float)shieldState.ShieldEnbaledPercent();
				shieldMesh.Scale = new Vector3(progress, progress, progress);
			}
		}

		if (shieldModel != null)
		{
			var anlge = antState.shield.facingUnit.Angle();
			shieldModel.Rotation = new Vector3(0, 0, (float)anlge);
		}

		if (gunModel != null &&
			targetState.targeting.TryGetValue(antState.immutable.id, out var at))
		{
			var toTarget = at.position.ToVector2() - antState.position.ToVector2();
			var anlge = toTarget.Angle();
			gunModel.Rotation = new Vector3(0, 0, (float)anlge);
		}

		// TODO rotate the main model to point in the direction of movement
		var veccy = antState.velocity.ToVector2();
		//unitModel.RotateZ(veccy.Angle());
		//float moo = MathF.PI/12; // TODO: fiddle with threshold.

		//if (Math.Abs(veccy.Angle() % (2*MathF.PI) - unitModel.Rotation.Z % (2*MathF.PI)) > moo)
		//{
		if (antState.velocity.LengthSquared() > 0)
		{
			if (unitModel is AntModelController amc)
			{
				amc.UpdateTarget(antState.Position.ToVector3(15), new Vector3(0, 0, veccy.Angle()));

			}
			else
			{
				var oldy = unitModel.Rotation;
				// TODO: what are the magic orientation angles in X-axis and Y-axis? it's not oldy's, it's not 0 and 0.
				unitModel.RotationOrder = EulerOrder.Xyz;
				unitModel.Rotation = new Vector3(0, 0, veccy.Angle());
			}
		}
		//};


		if (visibility)
		{

			this.hp.Visible = true;
			this.unitModel.Visible = true;
			if (shieldMesh != null)
			{
				var anlge = antState.shield.facingUnit.Angle();
				shieldMesh.Rotation = new Vector3(0, 0, (float)anlge);

				if (antState.shield is ShieldState shieldState)
				{
					var progress = (float)shieldState.ShieldEnbaledPercent();
					shieldMesh.Visible = true;
					shieldMesh.Scale = new Vector3(progress, progress, progress);
				}
				else
				{
					shieldMesh.Visible = antState.shield.isShielded;
				}
			}
			if (shieldModel != null)
			{
				shieldModel.Visible = antState.shield.isShielded;
			}
			if (gunModel != null)
			{
				gunModel.Visible = true;
			}
		}
		else 
		{
			this.hp.Visible = false;
			this.unitModel.Visible = false;
			if (shieldMesh != null)
			{
				shieldMesh.Visible = false;
			}
			if (shieldModel != null)
			{
				shieldModel.Visible = false;
			}
			if (gunModel != null)
			{
				gunModel.Visible = false;
			}
		}
	}

	//private void AddNonSimulationStateIfNeeded(bool playerControls)
	//{
	//	if (nonSimulationAnt == null)
	//	{
	//		nonSimulationAnt = NonSimulationAnt.CreateAndAdd(antState.immutable.id, NonSimulationState.state, playerControls);
	//	}
	//}

	internal void UpdateMeshes((ArrayMesh far, ArrayMesh los, ArrayMesh detection, Vector2 position, int lastUpdateTime, int index) meshes)
	{
		//if (meshFar.Length > 1)
		//{
		//	Log.WriteLine($"updating {meshes.index} position {meshes.position}");
		//}

		meshFar[meshes.index].Mesh = meshes.far;
		meshFar[meshes.index].GlobalPosition = new Vector3(meshes.position.X, meshes.position.Y, -65);
		mesh[meshes.index].Mesh = meshes.los;
		mesh[meshes.index].GlobalPosition = new Vector3(meshes.position.X, meshes.position.Y, -60);
		meshDetection[meshes.index].Mesh = meshes.detection;
		meshDetection[meshes.index].GlobalPosition = new Vector3(meshes.position.X, meshes.position.Y, -55);
	}
}
