using Godot;
using System.Collections.Generic;

public partial class BodySegment : Node3D
{
	//public float startingOffset;
	//public Vector3 startingRotation;

	//public Vector3 targetPosition;

	private List<ModelPoint> modelPoints = new List<ModelPoint>();

	public override void _Ready()
	{

		var parent = GetParent<AntModelController>();
		parent.Register(this);

	}

	public override void _Process(double delta)
	{
		var sum = new Vector3();


		foreach (var modelPointMetaData in modelPoints)
		{
			sum += modelPointMetaData.GetPrivateGlobalPosition();
		}
		var center = sum / modelPoints.Count;


		GlobalPosition = center;

		var sumDeltaRotation = new Vector3();
		var deltaRotation = sumDeltaRotation / modelPoints.Count;

		foreach (var modelPointMetaData in modelPoints)
		{
			sumDeltaRotation += modelPointMetaData.DeltaRotation(center);
		}
		Rotation = deltaRotation;
		base._Process(delta);	
	}

	public void UpdateTarget(Vector3 parentTargetPosition, Vector3 targetRotation) {
		foreach (var modelPoint in modelPoints)
		{
			modelPoint.UpdateTarget(parentTargetPosition, targetRotation);
		}
	}


	public static Vector3 GetRotations(Vector3 position)
	{
		var rotationZ = Mathf.Atan2(position.X, position.Y);
		var current = position.Rotated(new Vector3(0, 0, 1), rotationZ);

		var rotationY = Mathf.Atan2(current.Z, current.X);
		//current = current.Rotated(new Vector3(0, 1, 0), rotationY);

		//var rotationX = Mathf.Atan2(current.Y, current.Z);

		return new Vector3(0, rotationY, rotationZ);
	}

	public IEnumerable<ModelPoint> Points() => modelPoints;

	internal void Register(ModelPoint modelPoint)
	{
		modelPoints.Add(modelPoint);
	}

}
