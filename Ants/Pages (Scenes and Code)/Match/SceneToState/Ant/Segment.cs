﻿//using Godot;
//using System;
//using System.Collections.Generic;
//using System.Linq;

//internal class Segment
//{

//    public readonly float distance;
//    // we assume angle 1 < angle 2
//    // if angle 
//    public readonly float angle1;
//    public readonly float angle2;

//    private Segment(float distance, float angle1, float angle2)
//    {
//        this.distance = distance;
//        this.angle1 = angle1;
//        this.angle2 = angle2;
//    }

//    public static IEnumerable<Segment> Make(float angle1, float angle2, float distance)
//    {
//        if (float.IsNaN(angle1))
//        {
//            throw new Exception($"angle {angle1}");
//        }
//        if (float.IsNaN(angle2))
//        {
//            throw new Exception($"angle {angle2}");
//        }

//        while (angle1 > Mathf.Tau)
//        {
//            angle1 -= Mathf.Tau;
//        }
//        while (angle2 > Mathf.Tau)
//        {
//            angle2 -= Mathf.Tau;
//        }
//        while (angle1 < 0)
//        {
//            angle1 += Mathf.Tau;
//        }
//        while (angle2 < 0)
//        {
//            angle2 += Mathf.Tau;
//        }
//        if (angle1 == angle2)
//        {

//        }
//        else if (angle1 < angle2)
//        {
//            yield return new Segment(distance, angle1, angle2);
//        }
//        else
//        {
//            yield return new Segment(distance, angle1, Mathf.Tau);
//            yield return new Segment(distance, 0, angle2);
//        }
//    }



//    public static Segment MakeOrThrow(float angle1, float angle2, float distance)
//    {
//        if (angle2 > Mathf.Tau)
//        {
//            throw new Exception($"the agnle2 is too big {angle2}");
//        }
//        if (angle1 > Mathf.Tau)
//        {
//            throw new Exception($"the agnle1 is too big {angle1}");
//        }
//        if (angle2 < 0)
//        {
//            throw new Exception($"the agnle2 is too small {angle2}");
//        }
//        if (angle1 < 0)
//        {
//            throw new Exception($"the agnle1 is too small {angle1}");
//        }
//        if (angle1 == angle2)
//        {
//            throw new Exception($"the agnles are the same {angle2}");
//        }
//        if (angle1 > angle2)
//        {
//            throw new Exception($"the agnles1 ({angle1}) is bigger tyhan angle2 ({angle2})");
//        }
//        return new Segment(distance, angle1, angle2);
//    }

//    public bool Contains(float angle)
//    {
//        return angle1 <= angle && angle < angle2;
//    }

//    public bool Combine(Segment other, out List<Segment> res)
//    {
//        var (leading, following) = angle1 < other.angle1 ? (this, other) : (other, this);

//        // no overlap, we are done
//        if (!leading.Contains(following.angle1))
//        {
//            res = null;
//            return false;
//        }

//        var result = new List<Segment>(); // allocation on a hot path

//        if (following.distance == leading.distance)
//        {
//            AddIfNotEmpty(following.distance, Mathf.Min(leading.angle1, following.angle1), Mathf.Max(leading.angle2, following.angle2));
//            res = result;
//            return true;
//        }
//        else if (following.distance > leading.distance)
//        {
//            result.Add(leading);
//            if (!leading.Contains(following.angle2))
//            {
//                AddIfNotEmpty(following.distance, leading.angle2, following.angle2);
//            }
//            res = result;
//            return true;
//        }
//        else
//        {
//            AddIfNotEmpty(leading.distance, leading.angle1, following.angle1);
//            AddIfNotEmpty(following.distance, following.angle1, following.angle2);
//            if (leading.Contains(following.angle2))
//            {
//                AddIfNotEmpty(leading.distance, following.angle2, leading.angle2);
//            }
//            res = result;
//            return true;
//        }

//        void AddIfNotEmpty(float distance, float angle1, float angle2)
//        {
//            if (angle1 != angle2)
//            {
//                result.Add(new Segment(distance, angle1, angle2));
//            }
//        }
//    }
//}
