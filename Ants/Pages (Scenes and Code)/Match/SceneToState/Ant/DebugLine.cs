using Godot;
using System;
using static System.Formats.Asn1.AsnWriter;
using System.Diagnostics;

public partial class DebugLine : MeshInstance3D
{
	internal void UpdateLine(Vector3 from, Vector3 to)
	{
	// error related to this...
	// not sure why
	//E 0:02:55:0772   NativeCalls.cs:6354 @ void Godot.NativeCalls.godot_icall_3_706(nint, nint, Godot.Vector3 *, Godot.Vector3 *, Godot.NativeInterop.godot_bool): Node origin and target are in the same position, look_at() failed.
	//< C++ Error > Condition "p_pos.is_equal_approx(p_target)" is true.
	//< C++ Source > scene / 3d / node_3d.cpp:935 @ look_at_from_position()
	//< Stack Trace > NativeCalls.cs:6354 @ void Godot.NativeCalls.godot_icall_3_706(nint, nint, Godot.Vector3 *, Godot.Vector3 *, Godot.NativeInterop.godot_bool)
	//               Node3D.cs:975 @ void Godot.Node3D.LookAt(Godot.Vector3, System.Nullable`1[Godot.Vector3], bool)
	//               DebugLine.cs:18 @ void DebugLine.UpdateLine(Godot.Vector3, Godot.Vector3)
	//               Foot.cs:240 @ void Foot._Process(double)
	//               Node.cs:2131 @ bool Godot.Node.InvokeGodotClassMethod(Godot.NativeInterop.godot_string_name &, Godot.NativeInterop.NativeVariantPtrArgs, Godot.NativeInterop.godot_variant &)
	//               Node3D.cs:1036 @ bool Godot.Node3D.InvokeGodotClassMethod(Godot.NativeInterop.godot_string_name &, Godot.NativeInterop.NativeVariantPtrArgs, Godot.NativeInterop.godot_variant &)
	//               Foot_ScriptMethods.generated.cs:98 @ bool Foot.InvokeGodotClassMethod(Godot.NativeInterop.godot_string_name &, Godot.NativeInterop.NativeVariantPtrArgs, Godot.NativeInterop.godot_variant &)
	//               CSharpInstanceBridge.cs:24 @ Godot.NativeInterop.godot_bool Godot.Bridge.CSharpInstanceBridge.Call(nint, Godot.NativeInterop.godot_string_name *, Godot.NativeInterop.godot_variant * *, int, Godot.NativeInterop.godot_variant_call_error *, Godot.NativeInterop.godot_variant *)

	// angle: NaN, currentLenght: 0 offsetXyLength: 0, footInitailState.startingLegLength: 34.9196



		if (to.Equals(from))
		{
			GlobalPosition = to;
			Scale = new Vector3(1, 1, 0);
		}
		else
		{
			GlobalPosition = (from + to) / 2f;
			var length = (to - from).Length();
			Scale = new Vector3(1, 1, length);
			LookAt(to);
		}
	}
}
