using Godot;

public partial class ModelPoint : Node3D
{

	public float startingOffset;
	public Vector3 startingRotation;
	public Vector3 targetPosition;
	private Vector3 privateGlobalPostion;

	public Vector3 DeltaRotation(Vector3 around)
	{
		var currentRotation = BodySegment.GetRotations(privateGlobalPostion - around);
		return currentRotation - startingRotation;
	}

	public void UpdateTarget(Vector3 parentTargetPosition, Vector3 targetRotation)
	{
		var offset = new Vector3(0, startingOffset, 0);

		offset = offset.Rotated(new Vector3(1, 0, 0), startingRotation.X);
		offset = offset.Rotated(new Vector3(0, 1, 0), startingRotation.Y);
		offset = offset.Rotated(new Vector3(0, 0, 1), startingRotation.Z);

		offset = offset.Rotated(new Vector3(1, 0, 0), targetRotation.X);
		offset = offset.Rotated(new Vector3(0, 1, 0), targetRotation.Y);
		offset = offset.Rotated(new Vector3(0, 0, 1), targetRotation.Z);

		//Log.WriteLine($"parentTargetPosition: {parentTargetPosition}, offset: {offset}, startingOffset: {startingOffset}");

		targetPosition = parentTargetPosition + offset;
	}

	public Vector3 GetPrivateGlobalPosition() => privateGlobalPostion;


	internal void ApplyFoce(Vector3 force)
	{
		privateGlobalPostion += force;
	}

	//[Export] public bool lookAt;

	private BodySegment modelMesh;

	private float avialableFroce = 1;
	private float friction = .9f;
	private Vector3 velocity = new Vector3();

	public override void _Process(double delta)
	{
		var toTravel = targetPosition - (privateGlobalPostion + velocity);

		if (toTravel.LengthSquared() > avialableFroce*avialableFroce) {
			toTravel =toTravel.Normalized() * avialableFroce;
		}
		velocity += toTravel;
		privateGlobalPostion += velocity;
		velocity *= friction;

		base._Process(delta);
	}

	public override void _Ready()
	{
		modelMesh = GetParent<BodySegment>();
		modelMesh.Register(this);
		base._Ready();

		var modelController = modelMesh.GetParent<AntModelController>();

		var position = GlobalPosition - modelController.GlobalPosition;

		this.startingOffset = position.Length();


		// assume XYZ rotation
		startingRotation = BodySegment.GetRotations(position);

		privateGlobalPostion = GlobalPosition;
	}
}
