public partial class KneeToBody : DebugLine {

	public override void _Ready()
	{
		base._Ready();
		GetParent<Foot>().SetKneeToBody(this);
	}
}
