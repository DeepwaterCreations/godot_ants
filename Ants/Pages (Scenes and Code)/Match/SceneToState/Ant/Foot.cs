using Ants.Domain.Infrastructure;
using Godot;
using System;
using System.Collections.Generic;
using System.Linq;


public class FootManager
{
	const int candenceLength = 8;
	Dictionary<int, List<Step>> cadence = new Dictionary<int, List<Step>>
	{
		//[0] = new List<Step> { new Step(0, 1 / 3f), new Step(4, 1 / 2f), new Step(2, 1f) , new Step(5, 1 / 3f), new Step(1, 1 / 2f), new Step(3, 1f) },
		[0] = new List<Step> { new Step(0, 1 / 3f), new Step(4, 1 / 2f), new Step(2, 1f) },
		[4] = new List<Step> { new Step(5, 1 / 3f), new Step(1, 1 / 2f), new Step(3, 1f) },
	};

	int at = 0;

	public List<Foot> feet = new List<Foot>();
	public void Register(Foot f)
	{
		feet.Add(f);
	}

	private class Step
	{
		public readonly int footIndex;
		public readonly float percent;

		public Step(int footIndex, float percent)
		{
			this.footIndex = footIndex;
			this.percent = percent;
		}
	}

	//public Dictionary<Foot, Vector2> shouldMove = new Dictionary<Foot, Vector2>();

	Vector2 lastDirection = new Vector2();
	bool move = false;

	public void Update(Vector3 targetPosition, Vector3 targetRotation)
	{

		foreach (var foot in feet)
		{
			foot.Update(/*targetPosition, */targetRotation);
		}

		//      move = tracks.velocity.LengthSquared() != 0;


		//if (tracks.velocity.LengthSquared() != 0)
		//{
		//	lastDirection = tracks.velocity.ToVector2();
		//}

		//if (lastDirection.LengthSquared() != 0)
		//{

		at++;
		at %= candenceLength;

		//	//var sum = new Vector3();
		//	//foreach (var foot in feet)
		//	//{
		//	//	sum += foot.Position;
		//	//}
		//	//var wrongBy = new Vector2(sum.X, sum.Y);



		//	//shouldMove = feet
		//	//	.Select(x => (foot: x, propsed: x.PropsoedLocalPosition(lastDirection, wrongBy)))
		//	//	.OrderByDescending(x => (new Vector2(x.foot.Position.X, x.foot.Position.Y) - x.propsed).Dot(wrongBy))
		//	//	.Take(cadence[at])
		//	//	.ToDictionary(x => x.foot, x => x.propsed);
		//}
	}

	public bool ShouldMove(Foot f, out Vector2 localPosition)
	{

		//if (!move) {
		//	localPosition = default;
		//	return false;
		//}



		if (cadence.TryGetValue(at, out var steps) && steps.Any(x => x.footIndex == feet.IndexOf(f)))
		{

			var sum = new Vector3();
			foreach (var foot in feet)
			{
				sum += foot.Position;
			}
			var wrongBy = new Vector2(sum.X, sum.Y);

			localPosition = f.PropsoedLocalPosition(/*lastDirection,*/ -wrongBy * (1 / steps.Count));
			return true;
		}
		localPosition = default;
		return false;
	}
}


/// <summary>
/// it's called Foot it's actual position is the hip
/// </summary>
public partial class Foot : Node3D
{
	private InitailState footInitailState;

	private class InitailState
	{
		public float startingLegLength;
		public float startingXYLength;
		public float startingOffset;
		public Vector3 startingRotation;
	}

	public float startingOffset;
	public Vector3 startingRotation;

	private const int stepLength = 3;
	Vector3 plantedGlobal;
	private AntModelController antModelController;


	public Vector3 targetLocalPosition;

	private KneeToBody kneeToBody;
	public KneeToFloor kneeToFloor;


	public void SetKneeToBody(KneeToBody kneeToBody)
	{
		if (this.kneeToBody != null)
		{
			throw new Exception("kneeToBody shouldn't be set two times");
		}

		this.kneeToBody = kneeToBody;
	}

	public void SetKneeToFloor(KneeToFloor kneeToFloor)
	{
		if (this.kneeToFloor != null)
		{
			throw new Exception("kneeToBody shouldn't be set two times");
		}

		this.kneeToFloor = kneeToFloor;

		var relatedPosition = kneeToFloor.GlobalPosition - GlobalPosition;

		footInitailState = new InitailState
		{
			startingLegLength = relatedPosition.Length() + stepLength + 10,
			startingXYLength = GetXYLength(relatedPosition, relatedPosition.Length()),
			// TODO - this probably shouldn't really be 3d
			startingRotation = BodySegment.GetRotations(relatedPosition),
			startingOffset = relatedPosition.Length()
		};

		plantedGlobal = kneeToFloor.GlobalPosition;
	}

	public override void _Ready()
	{
		var parent = GetParent<BodySegment>();
		antModelController = parent.GetParent<AntModelController>();
		antModelController.footManager.Register(this);

		var relatedPosition = GlobalPosition - parent.GlobalPosition;
		startingRotation = BodySegment.GetRotations(relatedPosition);
		startingOffset = relatedPosition.Length();

	}

	public override void _Process(double delta)
	{
		// SEE LegMath.png

		if (antModelController.footManager.ShouldMove(this, out var to))
		{
			plantedGlobal = new Vector3(to.X + GlobalPosition.X, to.Y + GlobalPosition.Y, 0);
		}

		// if we are two far from the body we snap into range
		var currentOffset = (plantedGlobal - GlobalPosition);
		var currentLenght = currentOffset.Length();
		if (currentLenght > footInitailState.startingLegLength)
		{
			var resetTo = GlobalPosition + (currentOffset * (footInitailState.startingLegLength / currentLenght));

			if (!float.IsFinite(resetTo.X) || !float.IsFinite(resetTo.Y))
			{
				Log.WriteLine($"resetTo: {resetTo}, from: {GlobalPosition} ");
			}
			plantedGlobal = resetTo;

			currentOffset = (plantedGlobal - GlobalPosition);
			currentLenght = currentOffset.Length();
		}

		var kneeUpByInner = (footInitailState.startingLegLength * footInitailState.startingLegLength) - (currentLenght * currentLenght);
		var kneeUpBy = kneeUpByInner < 0 ? 0 : Mathf.Sqrt(kneeUpByInner) / 2f;

		if (!float.IsFinite(kneeUpBy))
		{
			Log.WriteLine($"kneeUpBy: {kneeUpBy}, footInitailState.startingLegLength: {footInitailState.startingLegLength}, currentLenght: {currentLenght}");
		}

		var offsetXY = new Vector2(currentOffset.X, currentOffset.Y);
		var offsetXyLength = offsetXY.Length();
		var angle = Mathf.Asin(currentOffset.Z / currentLenght);

		if (!float.IsFinite(angle))
		{
            Log.WriteLine($"angle: {angle}, currentLenght: {currentLenght} offsetXyLength:{offsetXyLength}, footInitailState.startingLegLength: {footInitailState.startingLegLength}");
            angle = Mathf.Tau / 4f;
		}

		var center = (plantedGlobal + GlobalPosition) / 2f;
		var d2Normal = GetNormal2D(offsetXY);

		var knee = center + new Vector3(kneeUpBy * d2Normal.X * Mathf.Sin(angle), kneeUpBy * d2Normal.Y * Mathf.Sin(angle), kneeUpBy * Mathf.Cos(angle));

		if (!float.IsFinite(knee.X) || !float.IsFinite(knee.Y))
		{
			Log.WriteLine($"kneeUpBy:{kneeUpBy}, angle: {angle}, knee: {knee} ");
		}

		kneeToFloor.UpdateLine(knee, plantedGlobal);
		kneeToBody.UpdateLine(GlobalPosition, knee);

	}

	private Vector2 GetNormal2D(Vector2 offsetXY)
	{
		if (offsetXY.LengthSquared() != 0) {
			return offsetXY.Normalized();
		}
		var compareToController = (GlobalPosition - antModelController.GlobalPosition);
		var compareToController2D = new Vector2(compareToController.X, compareToController.Y);


		if (compareToController2D.LengthSquared() != 0){
			return compareToController2D.Normalized();
		}

		throw new Exception("how do we get the normal 2d?");
	}

	private static float GetXYLength(Vector3 currentOffset, float currentLenght)
	{
		var currentLenghtXYSquared = (currentLenght * currentLenght) - (currentOffset.Z * currentOffset.Z);
		var currentLenghtXY = currentLenghtXYSquared < 0 ? 0 : Mathf.Sqrt(currentLenghtXYSquared);
		return currentLenghtXY;
	}

	public Vector2 PropsoedLocalPosition(Vector2 wrongBy)
	{
		if (wrongBy.LengthSquared() > stepLength * stepLength)
		{
			wrongBy = wrongBy.Normalized() * stepLength;
		}
		var ideally = new Vector2(targetLocalPosition.X + wrongBy.X, targetLocalPosition.Y + wrongBy.Y);
		return ideally;
	}

	internal void Update(Vector3 targetRotation)
	{
		{
			var offset = new Vector3(0, footInitailState.startingOffset, 0);

			offset = offset.Rotated(new Vector3(1, 0, 0), footInitailState.startingRotation.X);
			offset = offset.Rotated(new Vector3(0, 1, 0), footInitailState.startingRotation.Y);
			offset = offset.Rotated(new Vector3(0, 0, 1), footInitailState.startingRotation.Z);

			offset = offset.Rotated(new Vector3(1, 0, 0), targetRotation.X);
			offset = offset.Rotated(new Vector3(0, 1, 0), targetRotation.Y);
			offset = offset.Rotated(new Vector3(0, 0, 1), targetRotation.Z);

			targetLocalPosition = offset;
		}

		{
			var offset = new Vector3(0, startingOffset, 0);

			offset = offset.Rotated(new Vector3(1, 0, 0), startingRotation.X);
			offset = offset.Rotated(new Vector3(0, 1, 0), startingRotation.Y);
			offset = offset.Rotated(new Vector3(0, 0, 1), startingRotation.Z);

			offset = offset.Rotated(new Vector3(1, 0, 0), targetRotation.X);
			offset = offset.Rotated(new Vector3(0, 1, 0), targetRotation.Y);
			offset = offset.Rotated(new Vector3(0, 0, 1), targetRotation.Z);

			GlobalPosition = GetParent<BodySegment>().GlobalPosition + offset;
		}
	}
}
