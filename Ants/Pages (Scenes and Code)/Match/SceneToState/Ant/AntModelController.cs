using Godot;
using System;
using System.Collections.Generic;
using System.Linq;

public partial class AntModelController : Node3D
{

	public readonly FootManager footManager = new FootManager();
	private AntTracker antTracker;
	//public float startingOffset;
	//public Vector3 startingRotation;

	public List<BodySegment> segments = new List<BodySegment>();

	public override void _Ready()
	{

		antTracker = GetParent<AntTracker>();
		base._Ready();
		//this.startingOffset = Position.Length();

		// assume XYZ rotation
		//startingRotation = BodySegment.GetRotations(Position);
	}

	public override void _Process(double delta)
	{
		base._Process(delta);
	}

	// we apply force on the lowest level
	// and the it aggregates up
	public void ApplyForce(Vector3 force, Vector3 at) 
	{
		var hit = segments
			.SelectMany(x => x.Points())
			.OrderBy(x => (x.Position - at).LengthSquared())
			.First();
		hit.ApplyFoce(force);
	}

	public void UpdateTarget(Vector3 targetPosition, Vector3 targetRotation)
	{
		footManager.Update(targetPosition, targetRotation);
		foreach (var segment in segments)
		{
			segment.UpdateTarget(targetPosition, targetRotation);
		}
	}

	internal void Register(BodySegment bodySegment)
	{
		segments.Add(bodySegment);
	}
}
