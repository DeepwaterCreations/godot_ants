using Ants.AppState;
using Godot;

public partial class LoadingLobby : Node
{
	public override void _Process(double delta)
	{
		StaticAppState.ProcessOrNaviage(GetTree(), delta);
    }
}

