using Ants.AppState;
using Godot;
using System;
using System.Threading;

public partial class CountDown : Label
{
	TimeSpan? lastTimeTilStart;

	public override void _Process(double delta)
	{
		if (StaticAppState.TryGetState(out StartingGameAppState state))
		{
			var timeTillStart = (new DateTime(state.startingAtTicksUtc) - DateTime.UtcNow);

			if (timeTillStart > new TimeSpan())
			{
				this.Text = $"GAME STARTING IN: {(new DateTime(state.startingAtTicksUtc) - DateTime.UtcNow).Seconds:D2}";

			}
			else
			{
				this.Text = $"GAME STARTING IN: {0:D2}";
				this.QueueFree();
				Log.WriteLine($"CountDown is complete {Thread.CurrentThread.ManagedThreadId}");
			}

			if (Math.Round(timeTillStart.TotalSeconds) != Math.Round(lastTimeTilStart?.TotalSeconds ?? 1_000_000) && timeTillStart.TotalSeconds >= 0) {
				// TODO scott put in a real sound

	//            var player = new AudioStreamPlayer();
	//            AddChild(player);
	//            player.Stream = (AudioStream)GD.Load("res://sounds/enemyhit.mp3");
	//            player.Finished += () => { player.QueueFree(); };
				//player.VolumeDb *= .01f;
	//            player.Play();

			}

			lastTimeTilStart = timeTillStart;

		}
		else {
			this.QueueFree();
			Log.WriteLine($"CountDown is complete - not in starting game state {Thread.CurrentThread.ManagedThreadId}");
		}
	}
}
