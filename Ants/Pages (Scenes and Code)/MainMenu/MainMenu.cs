using Ants.AppState;
using Ants.V2;
using Godot;
using System;
using System.Diagnostics;
using System.Net;

public partial class MainMenu : Node
{

	private string ipAndPort;


	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		//Debugger.Launch();
	}

	private void OnIpChanged(string new_text)
	{
		ipAndPort = new_text;
	}

	public override void _Process(double delta)
	{
		StaticAppState.ProcessOrNaviage(GetTree(), delta);
	}
	private void OnJoin()
	{
		if (StaticAppState.TryGetStateOrNaviage(GetTree(), out MainMenuAppState state))
		{
			state.TryToJoin(ipAndPort)
				.ContinueWith(x => {
					if (x.Exception != null)
					{
						Log.WriteLine(x.Exception.ToString());
					}
				});
		}
		else {
			Log.WriteLine($"not in the right app state to join");
		}
	}

	private void OnHost()
	{
		if (StaticAppState.TryGetStateOrNaviage(GetTree(), out MainMenuAppState state))
		{
			state.Host();
		}
		else
		{
			Log.WriteLine($"not in the right app state to host");
		}
	}
}
