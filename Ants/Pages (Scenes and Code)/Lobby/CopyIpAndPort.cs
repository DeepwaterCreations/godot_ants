using Ants.AppState;
using Godot;
using System;
using System.Net;

public partial class CopyIpAndPort : Button
{

	public override void _Ready()
	{
		base._Ready();
		if (StaticAppState.TryGetStateOrNaviage(GetTree(), out NetworkedAppState state)) {
			Text = $"Copy Ip and Port \"{IpAndPort(state)}\"";
		}
	}

	// Called when the node enters the scene tree for the first time.
	public override void _Process(double delta)
	{
	}

	private void OnPressed()
	{
		if (StaticAppState.TryGetStateOrNaviage(GetTree(), out NetworkedAppState state))
		{
			DisplayServer.ClipboardSet(IpAndPort(state));
		}
	}

	private static string IpAndPort(NetworkedAppState state)
	{
		return new IPEndPoint(state.networker.myIp, state.networker.port).ToString();
	}
}
