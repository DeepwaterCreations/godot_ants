using Ants.AppState;
using Godot;
using System;
using System.Linq;
using System.Net;

public partial class PlayersInLobby : Godot.RichTextLabel
{
	public override void _Process(double delta)
	{
		if (StaticAppState.TryGetStateOrNaviage(GetTree(), out HasPlayersAppState state))
		{
			this.Text = String.Join("\n", state.GetPlayersList()
			.Select((x,i) => $"{i:D2} - {new IPEndPoint(new IPAddress(x.ip), x.port)}{(i == state.index ? " (you)" : "")}"));
		}
	}
}
