using Ants.AppState;
using Ants.Network.Messages;
using Godot;
using Prototypist.TaskChain;
using System;
using System.Linq;
using System.Reflection;

public partial class Lobby2 : Node
{
	public override void _Process(double delta)
	{
		StaticAppState.ProcessOrNaviage(GetTree(), delta);
	}

	private void StartGamePressed()
	{
		if (StaticAppState.TryGetStateOrNaviage(GetTree(), out LobbyAppState state))
		{
			state.StartGame();
		}
	}
}
