using Ants.AppState;
using Ants.Network.Messages;
using Godot;
using System;

public partial class LoadingGame : Node {

	public override void _Process(double delta)
	{
        StaticAppState.ProcessOrNaviage(GetTree(), delta);
    }
}
