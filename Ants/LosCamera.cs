using Godot;
using System;

public partial class LosCamera : Camera3D
{
	[Export]
	public Camera3D follows;
	
	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
	}

	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(double delta)
	{
		GlobalPosition = new Vector3(follows.GlobalPosition.X, follows.GlobalPosition.Y, 0);
		Size = follows.Size;
	}
}
