﻿using Ants.Domain.State;
using Ants.Network;
using Ants.Network.Messages;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Intrinsics;
using System.Threading;
using System.Threading.Tasks;
using static Pathing;

namespace Ants.AppState
{

    public class ReadyToStartAppState : StartingGameAppState, IHaveGame
    {

        public Game Game { get; }
        // should this be part of the game?
        public LosWorkerGroup LosWorkerGroup { get; } = new LosWorkerGroup(1);

        public ReadyToStartAppState(LoadingGameAppState loadingGameAppState, SimulationState state, ImmutableSimulationState immutableSimulationState, Dictionary<(Mode,double), TrainPaths> trainPaths) : base(loadingGameAppState.GetPlayersList(), loadingGameAppState.networker, loadingGameAppState.startingAtTicksUtc, loadingGameAppState.actualStartTimeTickstUtc, loadingGameAppState.index)
        {
            if (loadingGameAppState is null)
            {
                throw new ArgumentNullException(nameof(loadingGameAppState));
            }

            if (state is null)
            {
                throw new ArgumentNullException(nameof(state));
            }

            lastSentStartGameUtc = loadingGameAppState.lastSentStartGameUtc;
            this.Game = new Game(state, players.Count, index, new SendData(), trainPaths, immutableSimulationState);

            var (process, advance, _, _, _) = Game.Start(startingAtTicksUtc, LosWorkerGroup);
            process.ContinueWith(x => {
                if (x.Exception != null)
                {
                    Log.WriteLine(x.Exception.ToString());
                }
            });
            advance.ContinueWith(x => {
                if (x.Exception != null)
                {
                    Log.WriteLine(x.Exception.ToString());
                }
            });
        }

        public override void Process(double delta)
        {
            if (index == 0)
            {
                // {BCE71D36-FD4F-45B9-8C2D-A9F46DD7E268}
                if (lastSentStartGameUtc + TimeSpan.FromSeconds(1) < DateTime.UtcNow)
                {
                    networker.sender.SendAsync(
                        new StartingGame(actualStartTimeTickstUtc),
                        OtherPlayers().Select(x => x.ToIPEndPoint()).ToArray()).ContinueWith(x => {
                            if (x.IsFaulted)
                            {
                                Log.WriteLine(x.Exception.ToString());
                            }
                        }); ;

                    lastSentStartGameUtc = DateTime.UtcNow;
                }
            }

            if (new DateTime(startingAtTicksUtc) < DateTime.UtcNow /*&& NextState != null*/)
            {
                Log.WriteLine("setting appState to in game");
                StaticAppState.SetState(new InGameState(Game, players, networker, index, LosWorkerGroup));
            }
        }
    }
}
