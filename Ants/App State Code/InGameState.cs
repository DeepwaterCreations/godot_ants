﻿using System;
using System.Collections.Generic;
using System.Linq;
using Prototypist.Toolbox.IEnumerable;
using System.Collections.Concurrent;
using Prototypist.TaskChain;
using static Ants.Network.Messages.FrameData;
using Ants.Network.Messages;
using System.Diagnostics.CodeAnalysis;
using static Game;
using System.Threading;
using System.IO;
using System.Text.Json;
using Ants.V2;
using System.Threading.Tasks;
using Ants.Network;
using System.Diagnostics;

namespace Ants.AppState
{
    // I feel like I could merge this with ready to start state
    // maybe

    public class InGameState : HasPlayersAppState, IHaveGame
    {
        public readonly IReadOnlyList<PlayerData> players;
        public Game Game { get; }
        public LosWorkerGroup LosWorkerGroup { get; init; }

        public InGameState(Game game, IReadOnlyList<PlayerData> players, Networker networker, int index, LosWorkerGroup losWorkerGroup) : base(networker, index)
        {
            this.Game = game ?? throw new ArgumentNullException(nameof(game));
            this.players = players ?? throw new ArgumentNullException(nameof(players));
            LosWorkerGroup = losWorkerGroup ?? throw new ArgumentNullException(nameof(losWorkerGroup));
        }


        internal Task SendFrameDataAsync(int frame)
        {
            return networker.sender.SendAsync(Game.GetFrameData(frame), OtherPlayers().Select(x=>x.ToIPEndPoint()).ToArray());
        }


        // dups
        // {80244C61-714A-47ED-8A7D-46C82C40B343}
        public override IEnumerable<PlayerData> OtherPlayers()
        {
            for (int i = 0; i < players.Count; i++)
            {
                if (i != index) { 
                    yield return players[i];
                }
            }

        }


        // {80244C61-714A-47ED-8A7D-46C82C40B343}
        public override bool TryGetPlayer(int playerIndex, [MaybeNullWhen(false)] out PlayerData playerData)
        {
            if (playerIndex >= players.Count)
            {
                throw new Exception($"player index {playerIndex} is larger than the number of players in the game {players.Count}");
            }

            playerData = players[playerIndex];
            return true;
        }

        // {80244C61-714A-47ED-8A7D-46C82C40B343}
        public override IReadOnlyList<PlayerData> GetPlayersList() => players;

        // maybe I should do something 
        // for when this is called while it is running
        // I dont' want to kick off two runs at once 
        // do I even want to rollback before I have the all the inputs?
        //internal void HandleFrameData(FrameData frameData)
        //{
        //    int? rollbackTo = UpdateOrders(frameData);

        //    if (rollbackTo != null)
        //    {
        //        Game.RunFromFrame(rollbackTo.Value);
        //    }

        //    UpldateAcks(frameData);

        //    UpdateStateHashes();

        //    CheckStateHashes(frameData);
        //}

        public override void Process(double delta)
        {
        }
    }
}
