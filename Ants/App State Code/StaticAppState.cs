using System;
using System.Text;
using System.Reflection;
using System.Runtime.Serialization.Json;
using static Godot.Projection;
using static Godot.HttpRequest;
using System.Numerics;
using Prototypist.Toolbox.Dictionary;
using Ants.V2;
using System.Diagnostics.CodeAnalysis;
using Godot;
using System.Threading.Tasks;
using Godot.Collections;

namespace Ants.AppState
{
	// TODO we should probably not switch the state on the UI mid frame
	// and for match, we shouldn't update the game state

	public class StaticAppState
	{
		private static IAppState appState = new MainMenuAppState(new Networker());

		public StaticAppState()
		{
		}

		public static void ProcessOrNaviage(SceneTree tree, double delta) {
			var shouldBeAt = TypeToScene[appState.GetType()];
			if (shouldBeAt != currentScene)
			{
				Log.WriteLine($"going to {shouldBeAt}");
				tree.ChangeSceneToFile(shouldBeAt);
				currentScene = shouldBeAt;
			}
			else
			{
				appState.Process(delta);
			}
		}

		private static string currentScene = "res://Pages (Scenes and Code)/MainMenu/MainMenu.tscn";

		private static readonly System.Collections.Generic.Dictionary<Type, string> TypeToScene = new System.Collections.Generic.Dictionary<Type, string> {
			{ typeof(MainMenuAppState), "res://Pages (Scenes and Code)/MainMenu/MainMenu.tscn" },
			{ typeof(LoadingLobbyAppState), "res://Pages (Scenes and Code)/LoadingLobby/LoadingLobby.tscn" },
			{ typeof(LobbyGuestAppState), "res://Pages (Scenes and Code)/Lobby/Lobby.tscn" },
			{ typeof(LobbyHostAppState), "res://Pages (Scenes and Code)/Lobby/Lobby.tscn" },
			{ typeof(LoadingGameAppState), "res://Pages (Scenes and Code)/Match/MatchMultiplayer.tscn" },
			{ typeof(ReadyToStartAppState), "res://Pages (Scenes and Code)/Match/MatchMultiplayer.tscn" },
			{ typeof(InGameState), "res://Pages (Scenes and Code)/Match/MatchMultiplayer.tscn" },
		};

		public static bool TryGetStateOrNaviage<T>(SceneTree tree ,[MaybeNullWhen(false)] out T state)
			where T : class, IAppState
		{
			var local = appState; 
			var shouldBeAt = TypeToScene[local.GetType()];
			if (shouldBeAt != currentScene)
			{
				Log.WriteLine($"going to {shouldBeAt}");
				currentScene = shouldBeAt;
				tree.ChangeSceneToFile(shouldBeAt);
				state = null;
				return false;
			}
			else if (local is T t)
			{
				state = t;
				return true;
			}
			else 
			{
				state = default;
				return false;
			}
		}

		public static IAppState GetAppState() => appState;

        public static bool TryGetState<T>([MaybeNullWhen(false)] out T state)
			where T : class, IAppState 
		{
			if (appState.GetType().IsAssignableTo(typeof(T))) {
				state = (T)appState;
				return true;
			}
			state = null;
			return false;
		}

		public static T TryGetStateOrThrow<T>()
			where T : class, IAppState
		{
			if (appState.GetType().IsAssignableTo(typeof(T)))
			{
				return (T)appState;
			}
			throw new Exception("");
		}

		internal static void SetState(IAppState newState)
		{
			appState.Dispose();

            Log.WriteLine($"state is now: {newState}");
			appState = newState;


			//.. TODO might as well naviage right?
		}
	}

	public interface IAppState : IDisposable
	{
		Task StateDisposed { get; }
		void Process(double delta);
	}

	public interface IHaveGame : IAppState
	{
		Game Game { get; }
        LosWorkerGroup LosWorkerGroup { get; }
    }
}
