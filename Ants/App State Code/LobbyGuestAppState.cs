using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics.CodeAnalysis;
using Ants.Network.Messages;
using Godot;

namespace Ants.AppState
{
	public class LobbyGuestAppState : LobbyAppState
	{
		public IReadOnlyList<PlayerData> players;

		public LobbyGuestAppState(Networker networker, IReadOnlyList<PlayerData> players, int index) : base(networker, index)
		{
			this.players = players ?? throw new ArgumentNullException(nameof(players));
        }

        // {80244C61-714A-47ED-8A7D-46C82C40B343}
        public override IEnumerable<PlayerData> OtherPlayers() => players
			.Where((player, i) => i != index);

		// {80244C61-714A-47ED-8A7D-46C82C40B343}
		public override bool TryGetPlayer(int playerIndex, [MaybeNullWhen(false)] out PlayerData playerData)
		{
			if (playerIndex >= players.Count)
			{
				throw new Exception($"player index {playerIndex} is larger than the number of players in the game {players.Count}");
			}

			playerData = players[playerIndex];
			return true;
		}
		// {80244C61-714A-47ED-8A7D-46C82C40B343}
		public override IReadOnlyList<PlayerData> GetPlayersList() => players;

        internal void HandleLobbyState(LobbyState lobbyState)
        {
            players = lobbyState.players;
        }
    }
}
