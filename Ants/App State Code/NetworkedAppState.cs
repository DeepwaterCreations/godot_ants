﻿using Ants.Network.Messages;
using Prototypist.TaskChain;
using Prototypist.Toolbox.Object;
using System;
using System.Net;
using System.Threading.Tasks;

namespace Ants.AppState
{
    public abstract class NetworkedAppState : IAppState
    {
        
        public readonly Networker networker;

        public NetworkedAppState(Networker networker)
        {
            this.networker = networker ?? throw new ArgumentNullException(nameof(networker));
        }

        private TaskCompletionSource<bool> stateDisposed = new TaskCompletionSource<bool>();
        public Task StateDisposed => stateDisposed.Task;

        public void Dispose()
        {
            stateDisposed.SetResult(true);
        }

        public abstract void Process(double delta);

        internal Task HandleLegTest(LagTest lagTest, IPEndPoint from, DateTime at)
        {

            if (this.SafeIs(out HasPlayersAppState hasPlayer) && !hasPlayer.TryGetPlayer(lagTest.playerIndex, out var _))
            {
                Log.WriteLine($"received {nameof(LagTest)} from a player not in our lobby");
                return Task.CompletedTask;
            }

            return networker.sender.SendAsync(new LagTestAck(lagTest.sentAt, at.Ticks), from);
        }
    }
}
