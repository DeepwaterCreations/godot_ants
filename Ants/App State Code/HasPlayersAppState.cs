﻿using Prototypist.TaskChain;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace Ants.AppState
{

    public abstract class HasPlayersAppState : NetworkedAppState
    {
        public readonly int index;

        public HasPlayersAppState(Networker networker, int index) : base(networker)
        {
            this.index = index;
        }

        public abstract IEnumerable<PlayerData> OtherPlayers();
        public abstract bool TryGetPlayer(int playerIndex, [MaybeNullWhen(false)] out PlayerData playerData);

        public abstract IReadOnlyList<PlayerData> GetPlayersList();

    }
}
