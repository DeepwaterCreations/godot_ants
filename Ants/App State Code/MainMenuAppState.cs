﻿using Ants.Network.Messages;
using Ants.V2;
using System;
using System.Diagnostics;
using System.Net;
using System.Threading.Tasks;

namespace Ants.AppState
{
    public class MainMenuAppState : NetworkedAppState
    {
        private class TryJoin
        {
            public DateTime startedTryingToJoinUtc;
            public IPEndPoint ipEndPoint;
        }

        private TryJoin tryingToJoin;

        public MainMenuAppState(Networker networker) : base(networker)
        {
            Debugger.Launch();
            //Log.loggers.Add(new ConsoleLogger());
            var fileLogger = new FileLogger();
            fileLogger.Start($"log-{networker.port}");
            Log.StartBugLogger(networker.port);
            Log.StartBugLogger2(networker.port);
            Log.loggers.Add(fileLogger);
            new RunTimesLogger().Start(networker.port);
            Log.WriteLine($"starting... {DateTime.Now}");
        }

        public override void Process(double delta)
        {
            if (tryingToJoin != null)
            {
                // try to join for 30 seconds
                if (tryingToJoin.startedTryingToJoinUtc + TimeSpan.FromSeconds(30) < DateTime.UtcNow)
                {
                    var random = new Random();
                    if (random.NextDouble() < delta / 3.0)
                    {   
                        networker.sender.SendAsync(
                            new Ants.Network.Messages.JoinGame(),
                            tryingToJoin.ipEndPoint).ContinueWith(x => {
                                if (x.IsFaulted)
                                {
                                    Log.WriteLine(x.Exception.ToString());
                                }
                            });
                    }
                }

                // wait for a response for 10 more sends and then give up
                if (tryingToJoin.startedTryingToJoinUtc + TimeSpan.FromSeconds(40) < DateTime.UtcNow)
                {
                    tryingToJoin = null;
                }
            }
        }

        internal void HandleJoinGameAck(JoinGameAck ack)
        {
            StaticAppState.SetState(new LoadingLobbyAppState(networker, ack.index));
        }

        internal Task TryToJoin(string ipAndPort)
        {
            if (IPEndPoint.TryParse(ipAndPort.Trim(), out var ipEndPoint))
            {
                Log.WriteLine($"I should be sending...");
                tryingToJoin = new TryJoin
                {
                    startedTryingToJoinUtc = DateTime.UtcNow,
                    ipEndPoint = ipEndPoint
                };

                return networker.sender.SendAsync(
                    new Ants.Network.Messages.JoinGame(),
                    tryingToJoin.ipEndPoint);
            }

            Log.WriteLine($"failed to parse {ipAndPort}");
            return Task.CompletedTask;
        }

        internal void Host()
        {
            StaticAppState.SetState(new LobbyHostAppState(networker));
        }
    }
}
