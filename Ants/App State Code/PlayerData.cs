﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Prototypist.Toolbox.IEnumerable;
using System.Text.RegularExpressions;
using Ants.Domain.State;
using System.Diagnostics.CodeAnalysis;

namespace Ants.AppState
{
    public class PlayerData
    {
        public readonly byte[] ip;
        public readonly int port;

        public PlayerData(byte[] ipv6, int port)
        {
            this.ip = ipv6 ?? throw new ArgumentNullException(nameof(ipv6));
            this.port = port;
        }

        public override bool Equals(object obj)
        {
            return obj is PlayerData data &&
                   ip.NullSafeSequenceEqual(data.ip) &&
                   port == data.port;
        }


        // string will look like "[fe80::6d57:b0c9:78f9:b226%9]:51979" (ipv6)
        // or  98.249.85.242:52005 (ipv4)
        //public static bool TryParseData(string parse, [MaybeNullWhen(false)] out PlayerData res)
        //{
        //    if (IPEndPoint.TryParse(parse.Trim(), out var IpEndPoint)) { 
        //        res = new PlayerData(IpEndPoint.Address.GetAddressBytes(), IpEndPoint.Port);
        //        return true;
        //    }
        //    res = null;
        //    return false;

        //    //string addressPattern = @"\[(.*?)\]";
        //    //var ipAddress = IPAddress.Parse(Regex.Matches(parse, addressPattern).Single().Groups[1].Value);
        //    //string portPattern = @"\]:(\d+?)$";
        //    //var port = int.Parse(Regex.Matches(parse, portPattern).Single().Groups[1].Value);
        //    //res = new PlayerData(ipAddress.GetAddressBytes(), port);
        //    //return true;
        //}

        public override int GetHashCode()
        {
            return HashCode.Combine(ip.Sum(x => x.GetHashCode()), port);
        }

        public override string ToString()
        {
            return $"{new IPEndPoint(new IPAddress(ip), port)}";
        }

        public IPEndPoint ToIPEndPoint() => new IPEndPoint(new IPAddress(this.ip), this.port);
        

        public byte[] Serialize()
        {
            var res = new List<byte>();
            res.AddRange(BitConverter.GetBytes(ip.Length));
            res.AddRange(ip);
            res.AddRange(BitConverter.GetBytes(port));
            return res.ToArray();
        }

        public static PlayerData Deserialize(byte[] bytes, ref int at)
        {
            var addressBytesLength = BitConverter.ToInt32(bytes, at);
            at += 4;
            var addressBytes = new byte[addressBytesLength];
            for (int i = 0; i < addressBytesLength; i++)
            {
                addressBytes[i] = bytes[at];
                at++;
            }
            var port = BitConverter.ToInt32(bytes, at);
            at += 4;
            return new PlayerData(addressBytes, port);
        }
    }
}
