﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections.Concurrent;
using System.Diagnostics.CodeAnalysis;
using System.Threading;
using System.Net;
using Ants.Network.Messages;
using System.Threading.Tasks;
using Godot;

namespace Ants.AppState
{
    public class LobbyHostAppState : LobbyAppState
    {

        // this isn't a good way to store this
        // more than one player could have the same index
        // {6368DF46-C34E-47B7-BD1A-EC29A17CFC97}
        public readonly ConcurrentDictionary<PlayerData, int> players;
        public int playerIndexInc = 0;

        public LobbyHostAppState(Networker networker) : base(networker, 0)
        {
            players = new ConcurrentDictionary<PlayerData, int>();
            players.TryAdd(new PlayerData(networker.myIp.GetAddressBytes(), networker.port), 0);

        }


        public override IEnumerable<PlayerData> OtherPlayers() => players
            .Where(pair  => pair.Value != index)
            .Select(pair => pair.Key);

        internal int AddPlayer(PlayerData player)
        {
            // this isn't even good,
            // if a player gets added twice the add fails,
            // but we still increment
            var index = Interlocked.Increment(ref playerIndexInc);
            players.TryAdd(player, index);
            return index;
        }

        public override IReadOnlyList<PlayerData> GetPlayersList()
        {
            return players.OrderBy(x => x.Value).Select(x => x.Key).ToArray();
        }

        public override bool TryGetPlayer(int playerIndex, [MaybeNullWhen(false)] out PlayerData playerData)
        {
            var options = players.Where(x => x.Value == playerIndex).ToArray();
            if (options.Length == 0)
            {
                playerData = null;
                return false;
            }
            // {6368DF46-C34E-47B7-BD1A-EC29A17CFC97}
            if (options.Length > 1)
            {
                throw new Exception($"there should only be one player with the index {playerIndex}");
            }

            playerData = options[0].Key;
            return true;
        }

        internal Task HandleJoinGame(Network.Messages.JoinGame playerJoined, IPEndPoint from)
        {
            var player = new PlayerData(from.Address.GetAddressBytes(), from.Port);
            var index = AddPlayer(player);
            return networker.sender.SendAsync(new JoinGameAck(index), from);
        }

        private DateTime lastSentLobbyStateUtc;

        public override void Process(double delta)
        {
            // you're the host
            // send the lobby state every second
            if (lastSentLobbyStateUtc + TimeSpan.FromSeconds(1) < DateTime.UtcNow)
            {
                networker.sender.SendAsync(
                    new LobbyState(GetPlayersList()
                        .ToArray()),
                    OtherPlayers().Select(x => x.ToIPEndPoint()).ToArray()).ContinueWith(x => {
                        if (x.IsFaulted)
                        {
                            Log.WriteLine(x.Exception.ToString());
                        }
                    });

                lastSentLobbyStateUtc = DateTime.UtcNow;
            }
        }
    }
}
