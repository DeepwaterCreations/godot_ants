﻿using Ants.Network.Messages;
using System;
using System.Threading.Tasks;

namespace Ants.AppState
{
    public class LoadingLobbyAppState : NetworkedAppState
    {
        public LoadingLobbyAppState(Networker networker, int index) : base(networker)
        {
            this.index = index;
        }

        public readonly int index;

        internal void HandleLobbyState(LobbyState lobbyState)
        {
            AppState.StaticAppState.SetState(new LobbyGuestAppState(networker, lobbyState.players, index));
        }

        public override void Process(double delta)
        {
        }
    }
}
