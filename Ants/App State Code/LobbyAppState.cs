﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ants.Network.Messages;
using Godot;
using Prototypist.TaskChain;

namespace Ants.AppState
{

    public abstract class LobbyAppState : HasPlayersAppState
    {
        public readonly ConcurrentLinkedList<(int index, long sentAt, long? arrivalTime)> lagTestResults = new ConcurrentLinkedList<(int index, long sentAt, long? arrivalTime)>();

        public LobbyAppState(Networker networker, int index) : base(networker, index)
        {

        }

        public long TimeToAveragePlayerTicks()
        {
            var relatedResults = lagTestResults.Where(x => x.index != index)
                .Where(x => x.arrivalTime != null)
                .Select(x => x.arrivalTime - x.sentAt)
                .OrderBy(x => x)
                .ToArray();

            var toAverage = relatedResults
                .Skip(Mathf.FloorToInt(relatedResults.Length / 4f))
                .Take(relatedResults.Length - 2 * Mathf.FloorToInt(relatedResults.Length / 4f))
                .ToArray();

            return (long)(toAverage.Any() ? toAverage.Average() : 0.0);
        }

        // maybe I do need this --  naw I don't think so
        public long TimeToHostPlayerTicks()
        {
            var relatedResults = lagTestResults.Where(x => x.index == 0)
                .Where(x => x.arrivalTime != null)
                .Select(x => x.arrivalTime - x.sentAt)
                .OrderBy(x => x)
                .ToArray();

            var toAverage = relatedResults
                .Skip(Mathf.FloorToInt(relatedResults.Length / 4f))
                .Take(relatedResults.Length - 2 * Mathf.FloorToInt(relatedResults.Length / 4f))
                .ToArray();

            return (long)(toAverage.Any() ? toAverage.Average() : 0.0);
        }

        internal void HandleLagTestAck(LagTestAck lagTestAck)
        {

            var matches = lagTestResults.Where(x => x.sentAt == lagTestAck.replyToSentAt).ToArray();

            if (matches.Length == 0)
            {
                throw new Exception("there should be one");
            }
            else if (matches.Length == 1)
            {
                matches[0].arrivalTime = lagTestAck.arrivalTime;
            }
            else
            {
                // we sent out two LagTests in the same tick
                // weird, I guess we'll just ingore the data
            }
        }

        internal void HandleStartingGame(StartingGame startingGame)
        {
            AppState.StaticAppState.SetState(new LoadingGameAppState(startingGame.startingAtTicksUTC, startingGame.startingAtTicksUTC - TimeToAveragePlayerTicks(), this));
        }

        // seconds
        private const int startCountDownStartsAt = 0;//120;//30;//5;

        internal void StartGame()
        {

            var actualStartTime = DateTime.UtcNow.AddSeconds(startCountDownStartsAt).Ticks;

            networker.sender.SendAsync(
                new StartingGame(actualStartTime),
                OtherPlayers().Select(x => x.ToIPEndPoint()).ToArray());

            StaticAppState.SetState(new LoadingGameAppState(actualStartTime, actualStartTime - TimeToAveragePlayerTicks(), this)
            {
                lastSentStartGameUtc = DateTime.UtcNow
            });
        }

        public override void Process(double delta)
        {
            var ourIndex = index;

            var random = new Random();


            var players = GetPlayersList();
            for (int i = 0; i < players.Count; i++)
            {
                if (random.NextDouble() < delta / 5.0 && i != index)
                {
                    // probably I should move some of this stuff into the state
                    var now = DateTime.UtcNow.Ticks;
                    networker.sender.SendAsync(new LagTest(ourIndex, now), players[i].ToIPEndPoint())
                        .ContinueWith(x => {
                            if (x.IsFaulted)
                            {
                                Log.WriteLine(x.Exception.ToString());
                            }
                        }); ;
                    lagTestResults.Add((i, now, null));
                }
            }
        }
    }
}
