﻿using System;
using System.Threading.Tasks;
using static BulletState.Immutable;
using System.Collections.Concurrent;
using Ants.Domain.State;
using Ants.Network.Messages;
using Godot;
using System.Runtime.Intrinsics;
using System.Collections.Generic;
using static Pathing;
using System.Diagnostics;
using System.Linq;

namespace Ants.AppState
{
    public class ImmutableSimulationStatePendingPaths {
        public readonly RockState[] rocks;
        public readonly MudState[] mud;
        public readonly CoverState[] cover;

        public ImmutableSimulationStatePendingPaths(
            RockState[] rocks,
            MudState[] mud,
            CoverState[] cover)
        {
            this.rocks = rocks ?? throw new ArgumentNullException(nameof(rocks));;
            this.mud = mud ?? throw new ArgumentNullException(nameof(mud));
            this.cover = cover ?? throw new ArgumentNullException(nameof(cover));
        }
    }

    public class LoadingGameAppState : StartingGameAppState
    {
        // nullable
        internal Task<Paths[]> paths;
        internal Task<Dictionary<(Mode, double), TrainPaths>> trainPaths;
        // nullable
        private SimulationState startingState;
        // nullable
        private ImmutableSimulationStatePendingPaths immutableStatePendingPaths;
       

        public LoadingGameAppState(long actualStartTimeTickstUtc,long startingAtTicksUTC, LobbyAppState lobby) : base(lobby.GetPlayersList(), lobby.networker, startingAtTicksUTC, actualStartTimeTickstUtc, lobby.index)
        {
        }

        public override void Process(double delta)
        {
            if (index == 0)
            {
                // {BCE71D36-FD4F-45B9-8C2D-A9F46DD7E268}
                // maybe this is just a task I kick off..
                // instead of have to be spread around a bunch of _Process methods
                // let the players know
                if (lastSentStartGameUtc + TimeSpan.FromSeconds(1) < DateTime.UtcNow)
                {
                    networker.sender.SendAsync(
                        new StartingGame(actualStartTimeTickstUtc),
                        OtherPlayers().Select(x => x.ToIPEndPoint()).ToArray())
                        .ContinueWith(x=> { 
                            if (x.IsFaulted)
                            {
                                Log.WriteLine(x.Exception.ToString());
                            }
                        });

                    lastSentStartGameUtc = DateTime.UtcNow;
                }
            }

            if (paths !=null && paths.IsCompleted && 
                trainPaths != null && trainPaths.IsCompleted &&
                startingState != null)
            {
                var immutableSimulationState = new ImmutableSimulationState(
                    immutableStatePendingPaths.rocks,
                    immutableStatePendingPaths.mud,
                    immutableStatePendingPaths.cover,
                    new KnownPathsImmutableState(paths.Result)
                    );

                Log.WriteLine("loaded paths");
                StaticAppState.SetState(new ReadyToStartAppState(this, startingState, immutableSimulationState, trainPaths.Result));
            }
            //else
            //{
            //    Log.WriteLine("game isn't ready...");
            //}
        }

        public bool TryBuildPaths(
            IReadOnlyList<(Vector128<double> globalPosition, double radius)> avoidables,
            IReadOnlyList<(Vector128<double> globalPosition, double radius)> mud) {
            var mudMap = Map2<MudState>.Build(immutableStatePendingPaths.mud);
            if (paths == null)
            {
                paths = BuildKnownPaths(avoidables, mud, mudMap);
                return true;
            }
            return false;
        }

        public bool TryBuildTrainPaths(
            IReadOnlyList<(Vector128<double> globalPosition, double radius)> avoidables,
            HashSet<Vector128<double>> stations)
        {
            var mudMap = Map2<MudState>.Build(immutableStatePendingPaths.mud);
            if (trainPaths == null)
            {
                trainPaths = TrainPathing.BuildKnownPaths(stations, avoidables, mudMap);
                return true;
            }
            return false;
        }

        public bool TrySetStatePendingPaths(ImmutableSimulationStatePendingPaths immutableStatePendingPaths, SimulationState startingState)
        {
            if (this.startingState == null)
            {

                this.startingState = startingState;
                this.immutableStatePendingPaths = immutableStatePendingPaths;
                return true;
            }
            return false;
        }
    }
}
