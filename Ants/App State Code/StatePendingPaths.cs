﻿using Ants.Domain.State;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace Ants.AppState
{
    public class StatePendingPaths {
        public readonly ConcurrentDictionary<Guid, AntState> ants;
        public readonly RockState[] rocks;
        // maybe keep these ordered?
        public readonly ConcurrentDictionary<BulletState.Immutable.Id, BulletState> bullets;
        public readonly MoneyState money;
        public readonly ControlState control;
        public readonly ControlPointState[] controlPoints;
        public readonly MudState[] mud;
        public readonly PopulationState populationState;
        public readonly CoverState[] cover;
        public readonly ResourceSpawnerState[] resourceSpawnerStates;

        public StatePendingPaths(ConcurrentDictionary<Guid, AntState> ants, RockState[] rocks, ConcurrentDictionary<BulletState.Immutable.Id, BulletState> bullets, MoneyState money, ControlState control, ControlPointState[] controlPoints, MudState[] mud, PopulationState populationState, CoverState[] cover, ResourceSpawnerState[] resourceSpawnerStates)
        {
            this.ants = ants ?? throw new ArgumentNullException(nameof(ants));
            this.rocks = rocks ?? throw new ArgumentNullException(nameof(rocks));
            this.bullets = bullets ?? throw new ArgumentNullException(nameof(bullets));
            this.money = money ?? throw new ArgumentNullException(nameof(money));
            this.control = control ?? throw new ArgumentNullException(nameof(control));
            this.controlPoints = controlPoints ?? throw new ArgumentNullException(nameof(controlPoints));
            this.mud = mud ?? throw new ArgumentNullException(nameof(mud));
            this.populationState = populationState ?? throw new ArgumentNullException(nameof(populationState));
            this.cover = cover ?? throw new ArgumentNullException(nameof(cover));
            this.resourceSpawnerStates = resourceSpawnerStates ?? throw new ArgumentNullException(nameof(resourceSpawnerStates));
        }
    }
}
