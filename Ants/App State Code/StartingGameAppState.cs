﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics.CodeAnalysis;

namespace Ants.AppState
{
    public abstract class StartingGameAppState : HasPlayersAppState
    {

        public readonly IReadOnlyList<PlayerData> players;
        /// <summary>
        /// the time we should start running the game locally
        /// </summary>
        public long startingAtTicksUtc;
        /// <summary>
        /// the time the first message should arraive
        /// if the first message was sent exactly then the game started locally
        /// </summary>
        public long actualStartTimeTickstUtc;

        public DateTime lastSentStartGameUtc;


        protected StartingGameAppState(IReadOnlyList<PlayerData> players, Networker networker, long startingAtTicksUtc, long actualStartTimeTickstUtc, int index) : base(networker, index)
        {
            this.players = players ?? throw new ArgumentNullException(nameof(players));
            this.startingAtTicksUtc = startingAtTicksUtc;
            this.actualStartTimeTickstUtc = actualStartTimeTickstUtc;
        }

        // {80244C61-714A-47ED-8A7D-46C82C40B343}
        public override IEnumerable<PlayerData> OtherPlayers() => players
            .Where((player, i) => i != index);

        // {80244C61-714A-47ED-8A7D-46C82C40B343}
        public override bool TryGetPlayer(int playerIndex, [MaybeNullWhen(false)] out PlayerData playerData)
        {
            if (playerIndex >= players.Count)
            {
                throw new Exception($"player index {playerIndex} is larger than the number of players in the game {players.Count}");
            }

            playerData = players[playerIndex];
            return true;
        }
        // {80244C61-714A-47ED-8A7D-46C82C40B343}
        public override IReadOnlyList<PlayerData> GetPlayersList() => players;
    }
}
