using Godot;
using System;

public partial class Effect : MeshInstance3D
{ 
	[Export]
	public Camera3D follows;
	
	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
	}

	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(double delta)
	{
		GlobalPosition = new Vector3(follows.GlobalPosition.X, follows.GlobalPosition.Y, 100);
		var windowSize = DisplayServer.WindowGetSize();
		if (follows.KeepAspect == Camera3D.KeepAspectEnum.Height)
		{
			Scale = new Vector3(follows.Size * windowSize.X / windowSize.Y, follows.Size, Scale.Z);
		}
		else
		{
			Scale = new Vector3(follows.Size, follows.Size * windowSize.Y / windowSize.X, Scale.Z);
		}

		((Mesh as PrimitiveMesh).Material as ShaderMaterial).SetShaderParameter("size", follows.Size);

		// TODO
		// figure out the correct scale...
		// currently I just set this to a big number

		//Scale = new Vector3(follows.GlobalPosition.Z, follows.GlobalPosition.Z, 1);
	}
}
