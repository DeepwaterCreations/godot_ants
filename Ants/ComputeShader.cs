﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using Godot;

//namespace Ants
//{


//    public partial class MyComputeShader : Shader
//    {
//        public Texture DepthTexture { get; set; }

//        protected override unsafe void _Process(RenderingServer.CommandList cmd, Material.Pass pass)
//        {
//            // Execute the compute shader.
//            cmd.DrawCompute(GD.GetMainViewport().Size, shader_id, DepthTexture, null, 0);
//        }
//    }

//    public partial class MyNode : Node
//    {
//        private MyComputeShader shader;

//        public override void _Ready()
//        {
//            // Create a local rendering device.
//            var rd = RenderingServer.CreateLocalRenderingDevice();

//            // Create a texture from the hint_depth_texture.
//            var depth_texture = rd.Tex  .TextureCreateFromResource(hint_depth_texture);

//            // Create a compute shader instance.
//            shader = new MyComputeShader();

//            // Set the depth_texture as a uniform.
//            shader.DepthTexture = depth_texture;

//            // Execute the compute shader.
//            shader.Process(rd);
//        }
//    }

//}
