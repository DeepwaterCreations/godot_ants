using Godot;
using System;

public partial class MatchAspect : MeshInstance3D
{
	[Export]
	public Camera3D follows;
	
	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
	}

	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(double delta)
	{
		var windowSize = DisplayServer.WindowGetSize();
		Scale = new Vector3((float)windowSize.X/(float)windowSize.Y, 1 , 1);
		
		((Mesh as PrimitiveMesh).Material as ShaderMaterial).SetShaderParameter("size", follows.Size);
	}
}
