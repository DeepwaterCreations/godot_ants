﻿using System;
using System.Threading.Tasks;
using System.Text.Json;
using Ants.Network.Messages;
using Ants.AppState;
using System.Net;

namespace Ants.Network
{
    // parses messages and determines where they should go
    public class PostOffice {
		public Task Deliver(byte[] data, IPEndPoint from, DateTime at) {

            //var options = new JsonSerializerOptions { IncludeFields = true, WriteIndented = true };

            if (data.Length == 0)
            {
                Log.WriteLine($"got message from {from}");
            }

            if (data[0] == LagTest.TypeByte)
            {

                if (!StaticAppState.TryGetState(out NetworkedAppState netwrokedState))
                {
                    Log.WriteLine($"received {nameof(LagTest)} but we're not in a {nameof(NetworkedAppState)}");
                    return Task.CompletedTask;
                }
                var lagTest = LagTest.Deserialize(data);
                //Log.WriteLine($"received {nameof(LagTest)} {JsonSerializer.Serialize(lagTest, options)} {string.Join("", data)}");
                return netwrokedState.HandleLegTest(lagTest, from, at);
            }
            
            if (data[0] == Messages.JoinGame.TypeByte)
            {
                if (!StaticAppState.TryGetState(out LobbyHostAppState lobbyHostAppState))
                {
                    Log.WriteLine($"received {nameof(Messages.JoinGame)} but we're not hosting a {nameof(LobbyHostAppState)}");
                    return Task.CompletedTask;
                }
                var playerJoined = Messages.JoinGame.Deserialize(data);
                //Log.WriteLine($"received {nameof(JoinGame)} {JsonSerializer.Serialize(playerJoined, options)} {string.Join("", data)}");
                return lobbyHostAppState.HandleJoinGame(playerJoined, from);
            }
            
            if (data[0] == StartingGame.TypeByte)
            {
                if (!StaticAppState.TryGetState(out LobbyAppState lobbyAppState))
                {
                    Log.WriteLine($"received {nameof(StartingGame)} but we're not in a {nameof(LobbyAppState)}");
                    return Task.CompletedTask;
                }
                var startingGame = StartingGame.Deserialize(data);
                //Log.WriteLine($"received {nameof(StartingGame)} {JsonSerializer.Serialize(startingGame, options)} {string.Join("", data)}");
                lobbyAppState.HandleStartingGame(startingGame);
                return Task.CompletedTask;
            }
            
            if (data[0] == FrameData.TypeByte)
            {
                var frameData = FrameData.Deserialize(data);

                if (StaticAppState.TryGetState(out IHaveGame inGameAppState))
                {
                    inGameAppState.Game.Handle(frameData);
                    return Task.CompletedTask;
                }

                //this next stat thing is totally werid, I don't feel I have it right
                //if (StaticAppState.TryGetState(out ReadyToStartAppState readyToStartApp) && readyToStartApp.NextState != null)
                //{
                //    readyToStartApp.NextState.Game.Handle(frameData);
                //    return Task.CompletedTask;
                //}

                //Log.WriteLine($"received {nameof(FrameData)} {JsonSerializer.Serialize(frameData, options)} {string.Join("", data)}");
                //inGameAppState.HandleFrameData(frameData);

                Log.WriteLine($"received {nameof(FrameData)} but we're not in {nameof(IHaveGame)}");
                return Task.CompletedTask;
            }
            
            if (data[0] == JoinGameAck.TypeByte)
            {
                // probably trying to join should live in the app state
                // so I can check if got this from who we think it should come from....
                if (!StaticAppState.TryGetState(out MainMenuAppState mainMenuAppState))
                {
                    Log.WriteLine($"received {nameof(LobbyState)} but we're not in  {nameof(MainMenuAppState)}");
                    return Task.CompletedTask;
                }

                var ack = JoinGameAck.Deserialize(data);
                //Log.WriteLine($"received {nameof(JoinGameAck)} {JsonSerializer.Serialize(ack, options)} {string.Join("", data)}");
                mainMenuAppState.HandleJoinGameAck(ack);
                return Task.CompletedTask;
            }
            
            if (data[0] == LobbyState.TypeByte)
            {

                var lobbyState = LobbyState.Deserialize(data);
                //Log.WriteLine($"received {nameof(LobbyState)} {JsonSerializer.Serialize(lobbyState, options)} {string.Join("", data)}");
                if (StaticAppState.TryGetState(out LobbyGuestAppState lobbyGuestAppState))
                {
                    lobbyGuestAppState.HandleLobbyState(lobbyState);
                    return Task.CompletedTask;
                }

                if (StaticAppState.TryGetState(out LoadingLobbyAppState loadingLobby))
                {
                    loadingLobby.HandleLobbyState(lobbyState);
                    return Task.CompletedTask;
                }

                Log.WriteLine($"received {nameof(LobbyState)} but we're not in {nameof(LobbyGuestAppState)} or {nameof(LoadingLobbyAppState)}");
                return Task.CompletedTask;
            }
            
            if (data[0] == LagTestAck.TypeByte)
            {
                if (!StaticAppState.TryGetState(out LobbyAppState lobbyAppState))
                {
                    Log.WriteLine($"received {nameof(LagTestAck)} but we're not in a {nameof(LobbyAppState)}");
                    return Task.CompletedTask;
                }

                var lagTestAck = LagTestAck.Deserialize(data);
                //Log.WriteLine($"received {nameof(LagTestAck)} {JsonSerializer.Serialize(lagTestAck, options)} {string.Join("", data)}");
                lobbyAppState.HandleLagTestAck(lagTestAck);
                return Task.CompletedTask;
            }
                
            throw new NotImplementedException($"unexpected message type byte {data[0]}");
        }
	}
}
