using System;
using System.Linq;
using System.Net.Sockets;
using System.Threading.Tasks;
using Prototypist.Toolbox.Object;
using System.Threading.Channels;

namespace Ants.Network
{
	public class UDPListener
	{
		public readonly UdpClient listener;
		private readonly PostOffice postOffice;


		public UDPListener(UdpClient listener, PostOffice postOffice)
		{
			this.listener = listener ?? throw new ArgumentNullException(nameof(listener));
			this.postOffice = postOffice ?? throw new ArgumentNullException(nameof(postOffice));
		}

		public async Task Listen()
		{
			try
			{
				var task = listener.ReceiveAsync();


				while (true)
				{
					var got = await task.ConfigureAwait(false);
					task = listener.ReceiveAsync();

					var dontWait = postOffice.Deliver(got.Buffer, got.RemoteEndPoint, DateTime.UtcNow).ContinueWith(x => {
						if (x.Exception != null) {
							Log.WriteLine(x.Exception.ToString());
						}
					});
				}
			}
			catch (Exception e) {
				Log.WriteLine(e.ToString());
			}
			finally
			{
				listener.Close();
			}
		}
	}
}
