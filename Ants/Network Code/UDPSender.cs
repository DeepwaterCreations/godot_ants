﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Net;
using System.Text.Json;
using Ants.Network.Messages;
using Ants.AppState;
using System.Threading.Tasks;
using System.Linq;
using System.Threading;
using System.Threading.Channels;
using static Godot.Projection;
using System.Diagnostics;
using Ants.Domain;

namespace Ants.Network
{
    // TODO
    // I should probably time this
    // and then remove it
    // 
    public class UDPSenderJob : IDisposable, IUDPSender
    {
        Channel<(IMessage message, IPEndPoint[] to)> channel;
        UDPSender uDPSender;

        public UDPSenderJob(UDPSender uDPSender)
        {
            this.channel = Channel.CreateUnbounded<(IMessage message, IPEndPoint[] to)>();
            this.uDPSender = uDPSender ?? throw new ArgumentNullException(nameof(uDPSender));
        }

        public void Dispose()
        {
            ((IDisposable)uDPSender).Dispose();
        }

        public async Task SendAsync(IMessage message, IPEndPoint[] players)
        {
            await channel.Writer.WriteAsync((message, players)).ConfigureAwait(false);
        }

        public async Task SendAsync(IMessage message, IPEndPoint endpoint)
        {
            await channel.Writer.WriteAsync((message, new[] { endpoint })).ConfigureAwait(false);
        }

        public async Task Start(CancellationToken token) {
            await foreach(var item in channel.Reader.ReadAllAsync(token).ConfigureAwait(false))
            {
                foreach (var to in item.to)
                {
                    var dontwait = uDPSender.SendAsync(item.message, to)
                        .ContinueWith(x=> {
                            if (x.IsFaulted) {
                                Log.WriteLine($"could not send message: {item.message}, to: {to}, {x.Exception} ");
                            }
                        });
                }
            } 
        }
    }

    public class UDPSender : IDisposable, IUDPSender
    {
        private UdpClient udpClient;

        public UDPSender(UdpClient udpClient)
        {
            this.udpClient = udpClient;
        }

        //public static (int actualPort, UDPSender listener) Create(int tryPort, AddressFamily addressFamily)
        //      {
        //          var p = tryPort;
        //          while (true)
        //          {
        //              try
        //              {
        //                  return (p, new UDPSender(new UdpClient(p, addressFamily)));
        //              }
        //              catch (SocketException)
        //              {
        //                  if (p - tryPort > 50)
        //                  {
        //                      throw;
        //                  }
        //                  p++;
        //              }
        //          }
        //      }

        public Task SendAsync(IMessage message, IPEndPoint[] players)
        {
            byte[] bytes = message.Serialize();

            //Log.WriteLine($"sending {message.GetType().Name} {JsonSerializer.Serialize(message, message.GetType(), new JsonSerializerOptions { IncludeFields = true, WriteIndented = true })}, to {player}, bytes: {string.Join("", bytes)}");//

            var tasks = players.Select(player => udpClient.SendAsync(bytes, bytes.Length, player)).ToArray();
            return Task.WhenAll(tasks);

        }

        public async Task SendAsync(IMessage message, IPEndPoint player)
        {
            // currently I put this in a queue and pass
            // and do the sending on it's own thread
            // but do I need to do that? how slow is the sending?
            var sw = new Stopwatch();
            sw.Start();
            byte[] bytes = message.Serialize();

            //Log.WriteLine($"sending {message.GetType().Name} {JsonSerializer.Serialize(message, message.GetType(), new JsonSerializerOptions { IncludeFields = true, WriteIndented = true })}, to {player}, bytes: {string.Join("", bytes)}"); //
            var res = await udpClient.SendAsync(bytes, bytes.Length, player);
            DebugRunTimes.Add($"{nameof(UDPSender)}.{nameof(UDPSender.SendAsync)}", sw.ElapsedTicks);
        }

        //public Task SendAsync(IMessage message, IPEndPoint endpoint)
        //{
        //    byte[] bytes = message.Serialize();

        //    //Log.WriteLine($"sending {message.GetType().Name} {JsonSerializer.Serialize(message, message.GetType(), new JsonSerializerOptions { IncludeFields = true, WriteIndented = true })}, to {endpoint}, bytes: {string.Join("", bytes)}"); //
        //    return udpClient.SendAsync(bytes, bytes.Length, endpoint);
        //}

        public void Dispose()
        {
            udpClient.Close();
        }
    }
}
