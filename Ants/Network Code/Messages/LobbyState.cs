﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ants.AppState;
using Ants.Domain.State;
using Prototypist.Toolbox.IEnumerable;

namespace Ants.Network.Messages
{
    public class LobbyState : IMessage
    {
        /// <summary>
        /// ipv6 length 16
        /// </summary>
        public readonly PlayerData[] players;

        public LobbyState(PlayerData[] players)
        {
            this.players = players;
        }

        public byte[] Serialize()
        {
            var res = new List<byte>();
            res.Add(TypeByte);
            res.AddRange(BitConverter.GetBytes(players.Length));
            foreach (var player in players)
            {
                res.AddRange(player.Serialize());
            }
            return res.ToArray();
        }

        public static LobbyState Deserialize(byte[] bytes)
        {
            if (bytes[0] != TypeByte)
            {
                throw new Exception($"unexpected type byte got: {bytes[0]}, expected: {bytes[0]}");
            }

            var at = 1;
            var playersLength = BitConverter.ToInt32(bytes, at);
            at += 4;

            var players = new PlayerData[playersLength];

            for (int i = 0; i < playersLength; i++)
            {
                players[i] = PlayerData.Deserialize(bytes, ref at);
            }

            return new LobbyState(players);
        }

        public override bool Equals(object obj)
        {
            return obj is LobbyState state &&
                   players.NullSafeSequenceEqual(state.players);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(players.Sum(x => x.GetHashCode()));
        }

        public static byte TypeByte => 0x04;
    }
}
