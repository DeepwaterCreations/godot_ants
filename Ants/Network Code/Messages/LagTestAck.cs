﻿using System;
using System.Collections.Generic;

namespace Ants.Network.Messages
{
    public class LagTestAck : IMessage
    {
        public long replyToSentAt;
        public long arrivalTime;
        //public int playerIndex;


        public LagTestAck(long replyToSentAt, long arrivalTime/*, int playerIndex*/)
        {
            this.replyToSentAt = replyToSentAt;
            this.arrivalTime = arrivalTime;
            //this.playerIndex = playerIndex;
        }

        public static byte TypeByte => 0x07;

        public byte[] Serialize()
        {
            var list = new List<byte>();
            list.Add(TypeByte);
            list.AddRange(BitConverter.GetBytes(replyToSentAt));
            list.AddRange(BitConverter.GetBytes(arrivalTime));
            //list.AddRange(BitConverter.GetBytes(playerIndex));
            return list.ToArray();
        }

        public static LagTestAck Deserialize(byte[] bytes)
        {
            if (bytes[0] != TypeByte)
            {
                throw new Exception($"unexpected type byte got: {bytes[0]}, expected: {bytes[0]}");
            }

            var at = 1;
            var replyToIntendedArrivalTime = BitConverter.ToInt64(bytes, at);
            at += 8;
            var arrivalTime = BitConverter.ToInt64(bytes, at);
            at += 8;
            //var playerIndex = BitConverter.ToInt32(bytes, at);
            //at += 4;
            return new LagTestAck(replyToIntendedArrivalTime, arrivalTime/*, playerIndex*/);
        }

        public override bool Equals(object obj)
        {
            return obj is LagTestAck test &&
                   replyToSentAt == test.replyToSentAt &&
                   arrivalTime == test.arrivalTime
                   //&& playerIndex == test.playerIndex
                   ;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(replyToSentAt, arrivalTime /*, playerIndex*/);
        }
    }
}
