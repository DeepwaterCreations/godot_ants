using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Ants.Domain.Infrastructure;
using Ants.Domain.State;
using Ants.Domain.State.Orders._12___Arty;
using Ants.Domain.State.Orders._13___Zapper;
using Godot;
using Prototypist.Toolbox.IEnumerable;

namespace Ants.Network.Messages
{
	public class FrameData : IMessage, IStateHash
	{
		public class OrderData: IStateHash
		{
			public Guid id;
			public IPendingOrderState order;
			public bool queue;

			public OrderData(Guid id, IPendingOrderState order, bool queue)
			{
				this.id = id;
				this.order = order ?? throw new ArgumentNullException(nameof(order));
				this.queue = queue;
			}

			public override bool Equals(object obj)
			{
				return obj is OrderData data &&
					   id.Equals(data.id) &&
					   EqualityComparer<IPendingOrderState>.Default.Equals(order, data.order) &&
					   queue == data.queue;
			}

			public override int GetHashCode()
			{
				return HashCode.Combine(id, order, queue);
			}

			public IEnumerable<(string, object)> HashData() {
				yield return (nameof(id), id);
				yield return (nameof(order), order);
				yield return (nameof(queue), queue);
			}
		}

		public readonly int playerId;

		public readonly int firstOrderFrame; 
		public readonly OrderData[][] pendingOrdersByFrame;
		public readonly int[] greatestReceivedFrameIndexByPlayer;

		public readonly int firstHashFrame;
		public readonly int[] stateHashes;
		public readonly int[] greatestReceivedHashIndexByPlayer;

		public FrameData(
			int playerId, 
			int firstOrderFrame, 
			OrderData[][] pendingOrdersByFrame, 
			int[] greatestReceivedFrameIndexByPlayer, 
			int firstHashFrame, 
			int[] stateHashes, 
			int[] greatestReceivedHashIndexByPlayer)
		{
			this.playerId = playerId;
			this.firstOrderFrame = firstOrderFrame;
			this.pendingOrdersByFrame = pendingOrdersByFrame ?? throw new ArgumentNullException(nameof(pendingOrdersByFrame));
			this.greatestReceivedFrameIndexByPlayer = greatestReceivedFrameIndexByPlayer ?? throw new ArgumentNullException(nameof(greatestReceivedFrameIndexByPlayer));
			this.firstHashFrame = firstHashFrame;
			this.stateHashes = stateHashes ?? throw new ArgumentNullException(nameof(stateHashes));
			this.greatestReceivedHashIndexByPlayer = greatestReceivedHashIndexByPlayer ?? throw new ArgumentNullException(nameof(greatestReceivedHashIndexByPlayer));
		}

		public static byte TypeByte => 0x03;

		public byte[] Serialize()
		{
			var res = new List<byte>();
			res.Add(TypeByte);
			res.AddRange(BitConverter.GetBytes(playerId));
			res.AddRange(BitConverter.GetBytes(firstOrderFrame));

			res.AddRange(BitConverter.GetBytes(pendingOrdersByFrame.Length));
			foreach (var ordersForFrame in pendingOrdersByFrame)
			{
				res.AddRange(BitConverter.GetBytes(ordersForFrame.Length));
				foreach (var order in ordersForFrame)
				{
					res.AddRange(order.id.ToByteArray());
					var ordersBlock = order.order.Serialize();
					res.AddRange(BitConverter.GetBytes(ordersBlock.Length));
					res.AddRange(ordersBlock);
					res.AddRange(BitConverter.GetBytes(order.queue));
				}
			}

			res.AddRange(BitConverter.GetBytes(greatestReceivedFrameIndexByPlayer.Length));
			foreach (var frame in greatestReceivedFrameIndexByPlayer)
			{
				res.AddRange(BitConverter.GetBytes(frame));
			}

			res.AddRange(BitConverter.GetBytes(firstHashFrame));

			res.AddRange(BitConverter.GetBytes(stateHashes.Length));
			foreach (var hash in stateHashes) {
				res.AddRange(BitConverter.GetBytes(hash));
			}

			res.AddRange(BitConverter.GetBytes(greatestReceivedHashIndexByPlayer.Length));
			foreach (var frame in greatestReceivedHashIndexByPlayer)
			{
				res.AddRange(BitConverter.GetBytes(frame));
			}

			return res.ToArray();
		}

		public static FrameData Deserialize(byte[] data)
		{
			if (data[0] != TypeByte)
			{
				throw new Exception($"unexpected type byte got: {data[0]}, expected: {data[0]}");
			}

			var at = 1;
			var playerId = BitConverter.ToInt32(data, at);
			at += 4;
			var frame = BitConverter.ToInt32(data, at);
			at += 4;
			var pendingOrdersByFrameBytesCount = BitConverter.ToInt32(data, at);
			at += 4;

			var pendingOrdersByFrameList = new List<OrderData[]>();

			for (int i = 0; i < pendingOrdersByFrameBytesCount; i++)
			{
				var ordersLenght = BitConverter.ToInt32(data, at);
				at += 4;

				var orderList = new List<OrderData>();
				for (int j = 0; j < ordersLenght; j++)
				{
					var id = new Guid(data.AsSpan(at, 16));
					at += 16;

					var orderLength = BitConverter.ToInt32(data, at);
					at += 4;

					IPendingOrderState order = DeserializeOrder(data.Skip(at).Take(orderLength).ToArray());
					at += orderLength;

					var queue = BitConverter.ToBoolean(data, at);
					at += 1;

					orderList.Add(new OrderData(id, order, queue));
				}

				pendingOrdersByFrameList.Add(orderList.ToArray());
			}

			var greatestreceivedFrameIndexByPlayerList = new List<int>();

			var playerCount = BitConverter.ToInt32(data, at);
			at += 4;

			for (int i = 0; i < playerCount; i++)
			{
				greatestreceivedFrameIndexByPlayerList.Add(BitConverter.ToInt32(data, at));
				at += 4;
			}

			var hashFrame = BitConverter.ToInt32(data, at);
			at += 4;

			var stateHashesLength = BitConverter.ToInt32(data, at);
			at += 4;
			
			var stateHashes = new int[stateHashesLength];
			for (int i = 0; i < stateHashesLength; i++)
			{
				stateHashes[i] = BitConverter.ToInt32(data, at);
				at += 4;
			}

			var hashAckCount = BitConverter.ToInt32(data, at);
			at += 4;

			var greatestreceivedHashByPlayer = new List<int>();
			for (int i = 0; i < hashAckCount; i++)
			{
				greatestreceivedHashByPlayer.Add(BitConverter.ToInt32(data, at));
				at += 4;
			}

			return new FrameData
			(
				playerId,
				frame,
				pendingOrdersByFrameList.ToArray(),
				greatestreceivedFrameIndexByPlayerList.ToArray(),
				hashFrame,
				stateHashes,
				greatestreceivedHashByPlayer.ToArray()
			);
		}

		public static IPendingOrderState DeserializeOrder(byte[] data)
		{
			if (data[0] == EggPendingOrder.typeByte)
			{
				return EggPendingOrder.Deserialize(data);
			}
			else if (data[0] == ShootPendingOrder.typeByte)
			{
				return ShootPendingOrder.Deserialize(data);
			}
			else if (data[0] == SprintPendingOrder.typeByte)
			{
				return SprintPendingOrder.Deserialize(data);
			}
			else if (data[0] == PendingMoveWithShiledUpOrderImmutableState.typeByte)
			{
				return PendingMoveWithShiledUpOrderImmutableState.Deserialize(data);
			}
			else if (data[0] == MovePendingImmutableState.typeByte)
			{
				return MovePendingImmutableState.Deserialize(data);
			}
			else if (data[0] == MakeUnitOrderImmutableState.typeByte)
			{
				return MakeUnitOrderImmutableState.Deserialize(data);
			}
            else if (data[0] == GrenadePendingOrder.typeByte)
            {
                return GrenadePendingOrder.Deserialize(data);
            }
            else if (data[0] == SmokeOrder.typeByte)
            {
                return SmokeOrder.Deserialize(data);
            }
            else if (data[0] == ShockwaveOrder.typeByte)
            {
                return ShockwaveOrder.Deserialize(data);
            }
            else if (data[0] == ArtyOrder.typeByte)
            {
                return ArtyOrder.Deserialize(data);
            }
            else if (data[0] == ZapperOrder.typeByte)
            {
                return ZapperOrder.Deserialize(data);
            }
            throw new NotImplementedException($"unexpected order type byte {data[0]}");
		}

		public override bool Equals(object obj)
		{
			if (obj is FrameData data)
			{
				return
				   playerId == data.playerId &&
				   firstOrderFrame == data.firstOrderFrame &&
				   pendingOrdersByFrame.SequenceEqual(data.pendingOrdersByFrame, new MyComparer()) &&
				   greatestReceivedFrameIndexByPlayer.NullSafeSequenceEqual(data.greatestReceivedFrameIndexByPlayer) &&
				   firstHashFrame == data.firstHashFrame &&
				   stateHashes.NullSafeSequenceEqual(data.stateHashes) &&
				   greatestReceivedHashIndexByPlayer.NullSafeSequenceEqual(data.greatestReceivedHashIndexByPlayer);
			}
			return false;
		}

		private class MyComparer : IEqualityComparer<OrderData[]>
		{
			public bool Equals(OrderData[] x, OrderData[] y)
			{
				return x.NullSafeSequenceEqual(y);
			}

			public int GetHashCode([DisallowNull] OrderData[] obj)
			{
				return obj.UncheckedSum(x => x.GetHashCode());
			}
		}

		public override int GetHashCode()
		{
			return HashCode.Combine(
				playerId,
				firstOrderFrame,
				pendingOrdersByFrame.UncheckedSum(x => x.UncheckedSum(y=>y.GetHashCode())), 
				greatestReceivedFrameIndexByPlayer.UncheckedSum(x => x.GetHashCode()),
				firstHashFrame,
				stateHashes.UncheckedSum(x=>x.GetHashCode()),
				greatestReceivedHashIndexByPlayer.UncheckedSum(x => x.GetHashCode()));
		}

		public IEnumerable<(string, object)> HashData()
		{
			yield return (nameof(playerId), playerId);
			yield return (nameof(firstOrderFrame), firstOrderFrame);
			yield return (nameof(pendingOrdersByFrame), pendingOrdersByFrame);
			yield return (nameof(greatestReceivedFrameIndexByPlayer), greatestReceivedFrameIndexByPlayer);
			yield return (nameof(firstHashFrame), firstHashFrame);
			yield return (nameof(stateHashes), stateHashes);
			yield return (nameof(greatestReceivedHashIndexByPlayer), greatestReceivedHashIndexByPlayer);

		}
	}
}
