﻿using System;
using System.Collections.Generic;

namespace Ants.Network.Messages
{
    public class StartingGame : IMessage
    {
        public readonly long startingAtTicksUTC;

        public StartingGame(long startingAtTicksUTC)
        {
            this.startingAtTicksUTC = startingAtTicksUTC;
        }

        // this will probably hold more data about map and stuff

        public static byte TypeByte => 0x02;

        public byte[] Serialize()
        {
            var res = new List<byte>();
            res.Add(TypeByte);
            res.AddRange(BitConverter.GetBytes(startingAtTicksUTC));
            return res.ToArray();
        }


        public static StartingGame Deserialize(byte[] data)
        {
            if (data[0] != TypeByte)
            {
                throw new Exception($"unexpected type byte got: {data[0]}, expected: {data[0]}");
            }

            return new StartingGame
            (
                startingAtTicksUTC: BitConverter.ToInt64(data, 1)
            );
        }

        public override bool Equals(object obj)
        {
            return obj is StartingGame game &&
                   startingAtTicksUTC == game.startingAtTicksUTC;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(startingAtTicksUTC);
        }
    }
}
