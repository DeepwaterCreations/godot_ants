namespace Ants.Network.Messages
{
	public interface IMessage
	{
		byte[] Serialize();

		// TODO C# 11
		//static abstract IMessage Deserialize(byte[] data);
	}

	// TODO C# 11
	//public interface IMessage<T> {
	//    static abstract byte TypeByte { get; }
	//    static abstract T Deserialize(byte[] data);
	//}
}
