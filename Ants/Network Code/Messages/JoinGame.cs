using System;
using System.Collections.Generic;
using System.Linq;
using Ants.Domain.State;
using Prototypist.Toolbox.IEnumerable;

namespace Ants.Network.Messages
{

	public class JoinGameAck : IMessage {
		public static byte TypeByte = 0x08;
		public readonly int index;

		public JoinGameAck(int index)
		{
			this.index = index;
		}

		public byte[] Serialize()
		{
			var res = new List<byte>();
			res.Add(TypeByte);
			res.AddRange(BitConverter.GetBytes(index));
			return res.ToArray();
		}

		public static JoinGameAck Deserialize(byte[] data)
		{

			if (data[0] != TypeByte)
			{
				throw new Exception($"unexpected type byte got: {data[0]}, expected: {data[0]}");
			}

			var at = 1;
			var index = BitConverter.ToInt32(data, at);


			return new JoinGameAck(index);
		}

		public override bool Equals(object obj)
		{
			return obj is JoinGameAck ack &&
				   index == ack.index;
		}

		public override int GetHashCode()
		{
			return HashCode.Combine(index);
		}
	}

	public class JoinGame : IMessage
	{
		public JoinGame(/*Guid id*/)
		{
			//this.id = id;
		}

		public static byte TypeByte = 0x00;
		//public readonly Guid id;

		public byte[] Serialize()
		{
			var res = new List<byte>();
			res.Add(TypeByte);
			//res.AddRange(id.ToByteArray());
			return res.ToArray();
		}
		public static JoinGame Deserialize(byte[] data)
		{

			if (data[0] != TypeByte)
			{
				throw new Exception($"unexpected type byte got: {data[0]}, expected: {data[0]}");
			}


			return new JoinGame(/*id*/);
		}

		public override bool Equals(object obj)
		{
			return obj is JoinGame game /*&& id.Equals(game.id)*/;
		}

		public override int GetHashCode()
		{
			return nameof(JoinGame).GetHashCode() /*id.GetHashCode()*/;
		}
	}
}
