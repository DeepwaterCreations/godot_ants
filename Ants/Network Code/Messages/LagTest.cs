﻿using System;
using System.Collections.Generic;

namespace Ants.Network.Messages
{
    public class LagTest : IMessage
    {
        public int playerIndex;
        public long sentAt;

        public LagTest(int playerIndex, long sentAt)
        {
            this.playerIndex = playerIndex;
            this.sentAt = sentAt;
        }

        public static byte TypeByte => 0x06;

        public byte[] Serialize()
        {
            var list = new List<byte>();
            list.Add(TypeByte);
            list.AddRange(BitConverter.GetBytes(playerIndex));
            list.AddRange(BitConverter.GetBytes(sentAt));
            return list.ToArray();
        }

        public static LagTest Deserialize(byte[] bytes)
        {

            if (bytes[0] != TypeByte)
            {
                throw new Exception($"unexpected type byte got: {bytes[0]}, expected: {bytes[0]}");
            }

            var at = 1;
            var playerIndex = BitConverter.ToInt32(bytes, at);
            at += 4;
            var sentAt = BitConverter.ToInt64(bytes, at);
            at += 8;
            return new LagTest(playerIndex, sentAt);
        }

        public override bool Equals(object obj)
        {
            return obj is LagTest test &&
                   playerIndex == test.playerIndex &&
                   sentAt == test.sentAt;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(playerIndex, sentAt);
        }
    }
}
