﻿using Ants.AppState;
using Ants.Network.Messages;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace Ants.Network
{
    public interface IUDPSender
    {
        Task SendAsync(IMessage message, IPEndPoint[] players);
        Task SendAsync(IMessage message, IPEndPoint endpoint);
    }
}