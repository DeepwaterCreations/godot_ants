using System;
using System.Linq;
using System.Net.Sockets;
using System.Net;
using Ants.Network;
using System.Threading.Tasks;
using System.Net.Http;
using System.Diagnostics;
using System.Threading;

namespace Ants.AppState
{
	public class Networker : IDisposable
	{
		public readonly IPAddress myIp;
		public readonly int port;

		public IUDPSender sender;

		public UDPListener listener;

		public CancellationToken CancellationToken;


		public Networker()
		{
            //string strHostName = Dns.GetHostName();
            //IPHostEntry ipEntry = Dns.GetHostEntry(strHostName);
            //myIp = ipEntry.AddressList.Where(x => x.AddressFamily == AddressFamily.InterNetworkV6 && IsPublic(x)).First();
            // TODO hardcode your IP
            // "73.242.227.238"
            // "73.228.28.27"
            myIp = IPAddress.Parse("127.0.0.1");//GetExternalIpAddress();//IPAddress.Parse("2601:8c1:817f:8a80:97db:1a0:84d2:2fd0");//

            var pair = Create(21980, myIp.AddressFamily);
			port = pair.actualPort;
			listener = new UDPListener(pair.client, new PostOffice());
			Task.Run(async () =>
			{
				try
				{
					await listener.Listen().ConfigureAwait(false);
				}
				catch (Exception ex)
				{
					Log.WriteLine(ex.ToString());
				}
			});
			var sendingJob = new UDPSenderJob(new UDPSender(pair.client));
            sendingJob.Start(CancellationToken).ContinueWith(x => {
				if (x.IsFaulted) {
					Log.WriteLine($"UDPSenderJob crashed {x.Exception}");
				}
			});
			sender = sendingJob;
            

            Log.WriteLine($"port {port}");
		}

	public static (int actualPort, UdpClient client) Create(int tryPort, AddressFamily addressFamily)
	{
		var p = tryPort;
		while (true)
		{
			try
			{
				return (p, new UdpClient(p, addressFamily));
			}
			catch (SocketException)
			{
				if (p - tryPort > 50)
				{
					throw;
				}
				p++;
			}
		}
	}

	public void Dispose()
	{
		((IDisposable)sender).Dispose();
		((IDisposable)listener).Dispose();
	}

	public static bool IsPublic(IPAddress ipAddress)
	{
		var ipAddressBytes = ipAddress.GetAddressBytes();
		switch (ipAddress.AddressFamily)
		{
			case AddressFamily.InterNetwork:
				return !(
					ipAddressBytes[0] == 10 ||
					(ipAddressBytes[0] == 172 && ipAddressBytes[1] >= 16 && ipAddressBytes[1] <= 31) ||
					(ipAddressBytes[0] == 192 && ipAddressBytes[1] == 168)
				);
			case AddressFamily.InterNetworkV6:
				return !ipAddress.IsIPv6LinkLocal && !ipAddress.IsIPv6SiteLocal;
			default:
				throw new ArgumentException("Only IPv4 and IPv6 addresses are supported");
		}
	}

	public static IPAddress GetExternalIpAddress()
	{
		//Debugger.Launch();

		try
		{
			//var response = new HttpClient().Send(new HttpRequestMessage(HttpMethod.Get, "https://icanhazip.com"));
			//response.Content.Rea();

			//var externalIpString = (new HttpClient().GetStringAsync("https://icanhazip.com"))// this gives ip v6
			var externalIpString = (new HttpClient().GetStringAsync("https://ipinfo.io/ip"))// this gives ip v4
				.Result
				.Replace("\\r\\n", "").Replace("\\n", "").Trim();
			if (!IPAddress.TryParse(externalIpString, out var ipAddress))
			{
				throw new Exception("failed us to get ip");
			}
			return ipAddress;
		}
		catch (Exception ex)
		{
			Log.WriteLine(ex.ToString());
			throw;
		}
	}
}
}
