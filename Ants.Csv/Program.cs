﻿using System.Collections.Generic;
using System.Runtime.Intrinsics;



using (StreamWriter writer = new StreamWriter(@"..\..\..\Units.csv"))
{
    var header1 = new List<string>
    {
        "type",
        "maxHp",
        "mudMode",
        "lineOfSightFar",
        "lineOfSight",
        "lineOfSightDetection",
        "radius",
        "Speed",
        "endingPierce",
        "pierceAt200",
        "damage",
        "shotSpeed",
        "scatterAngleRad",
        "relaodTimeTick",
        "canMoveAndShoot",
        "canMoveAndRelaod",
        "range",
        "rangeScatter",
        "setUpTimeTicks",
        "shots",
        "ignoreCoverFor",
        "ignoreCoverForScatter",
        "autoShoot",
        "frontArmor",
        "backArmor",
        "armorAngleRads",
        "maxEnergy",
        "multiplySpeed"
    };

    writer.WriteLine(String.Join(", ", header1.ToArray()));

    var header2 = new List<string>
    {
        "type",
        "ant.immutable.maxHp",
        "ant.immutable.mudMode",
        "ant.immutable.lineOfSightFar",
        "ant.immutable.lineOfSight",
        "ant.immutable.lineOfSightDetection",
        "ant.immutable.mappable.radius",
        "ant.legs?.Speed",
        "ant.Gun?.immutable.endingPierce",
        "ant.Gun?.immutable.pierceAt200",
        "ant.Gun?.immutable.damage",
        "ant.Gun?.immutable.shotSpeed",
        "ant.Gun?.immutable.scatterAngleRad",
        "ant.Gun?.immutable.relaodTimeTick",
        "ant.Gun?.immutable.canMoveAndShoot",
        "ant.Gun?.immutable.canMoveAndRelaod",
        "ant.Gun?.immutable.range",
        "ant.Gun?.immutable.rangeScatter",
        "ant.Gun?.immutable.setUpTimeTicks",
        "ant.Gun?.immutable.shots",
        "ant.Gun?.immutable.ignoreCoverFor",
        "ant.Gun?.immutable.ignoreCoverForScatter",
        "ant.Gun?.immutable.autoShoot",
        "ant.shield?.baseImmutable.frontArmor",
        "ant.shield?.baseImmutable.backArmor",
        "ant.shield?.baseImmutable.armorAngleRads",
        "ant.sprint.immutable.maxEnergy",
        "ant.sprint.immutable.multiplySpeed"
};

    writer.WriteLine(String.Join(", ", header2.ToArray()));

    foreach (AntType value in Enum.GetValues(typeof(AntType)))
    {
        //if (value == AntType.Unpacker_Old) continue;
        //if (value == AntType.Decoy_Old) continue;

        var list = new List<string>();
        var ant = AntFactory.Make(Guid.NewGuid(), value, 0, Vector128<double>.Zero);

        list.Add(value + "");
        list.Add(ant.immutable.maxHp + "");
        list.Add(ant.immutable.mudMode + "");
        list.Add(ant.immutable.lineOfSightFar + "");
        list.Add(ant.immutable.lineOfSight + "");
        list.Add(ant.immutable.lineOfSightDetection + "");
        //list.Add(ant.immutable.mappable.radius + "");
        list.Add(ant.legs?.Speed + "");
        list.Add(ant.gun?.immutable.pierce + "");
        list.Add(ant.gun?.immutable.damage + "");
        list.Add(ant.gun?.immutable.shotSpeed + "");
        list.Add(ant.gun?.immutable.scatterAngleRad + "");
        list.Add(ant.gun?.immutable.relaodTimeTick + "");
        list.Add(ant.gun?.immutable.canMoveAndShoot + "");
        list.Add(ant.gun?.immutable.canMoveAndRelaod + "");
        list.Add(ant.gun?.immutable.range + "");
        list.Add(ant.gun?.immutable.rangeScatter + "");
        list.Add(ant.gun?.immutable.setUpTimeTicks + "");
        list.Add(ant.gun?.immutable.shots + "");
        list.Add(ant.gun?.immutable.ignoreCoverFor + "");
        list.Add(ant.gun?.immutable.ignoreCoverForScatter + "");
        list.Add(ant.gun?.immutable.autoShoot + "");
        list.Add(ant.shield?.baseImmutable.frontArmor + "");
        list.Add(ant.shield?.baseImmutable.backArmor + "");
        list.Add(ant.shield?.baseImmutable.armorAngleRads + "");
        list.Add(ant.sprint?.immutable.maxEnergy + "");
        list.Add(ant.sprint?.immutable.multiplySpeed + "");

        writer.WriteLine(String.Join(", ", list.ToArray()));
    }
}

