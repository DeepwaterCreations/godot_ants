﻿MakeLineGame count=1 frames=10_000  (DEBUG) --------------------------------------------------

RUN		TIME (s)					NOTES
1		7.6182 +- 0.3452			orginal						 		
2		5.4299 +- 0.4324			moved more stuff into immutable, reduced the amount of times Mappible is constructed
3		4.4378 +- 0.2352			replaced Math.Exp(Math.E, x) with x*x*x in Sigmoid
4		4.3400 +- 0.2554			replaced some calls to ToList and ToDictionay
5		4.2543 +- 0.2120			small change to ApplyArmor to reduce the number of angles we have to calculate
6		1.5568 +- 0.1712			cut the number of allowed shield angles in half - but it had too much effect
7		0.5348 +- 0.3682			rewrote how facing unit is calcuated to call .Angle() less
8		0.9642 +- 0.2980			fix a few bugs cause by "Math.Exp(Math.E, x) with x*x*x" and "rewrote how facing unit is calcuated"
9		0.8781 +- 0.2157			Math.Pow(Math.E, -value) -> Mathf.Pow(Mathf.E, -value)
10		0.7150 +- 0.2433			replaced sigmoid with something else
15		0.2948 +- 0.0798			""
16		0.2713 +- 0.0981			replaced state dicts with conccurent version
32		1.3956 +- 0.0435			running all 3 tests (no profiler)
33		0.2911 +- 0.0527			fixed a bug with the test  where all the ants were being stack on top of each other, parallel off (no profiler)
38      1.2390 +- 0.5895			""
39		1.6774 +- 0.1858			""
46		0.2201 +- 0.0530			a very long time passed during which I could not run these tests (no profiler)
57		1.9488 +- 0.3026			""
58		2.1334 +- 0.7302			post project weekend (no profiler)

MakeLineGame count=10 frames=1000  (DEBUG) ------------------------------

RUN		TIME (s)					NOTES
11		 7.4339 +- 0.1901			get raw ids out of the map instead of States
12		 6.6155 +- 0.0883			replaced a call to "new List" to "Array.Empty" this result seems unreasonably low
13		 6.2312 +- 0.1976			replaced a list with a LinkedList, the list is quesiton was only added and iterated and stayed small, I suspect that allocation the backing array was trouble. I also repalced LengthBetween with LengthSquaredBetween.
14		 6.4425 +- 0.1101			replaced a few more Lists with LinkedLists
15		 6.1159 +- 0.2311			took map out of state
16		 6.1530 +- 0.2161			""
17		 3.9752 +- 0.2959			parallized picking who to shoot at
18		 3.0501 +- 0.2346			filter before we enter the Paralle.ForEach
19		 3.7128 +- 0.1561			Made map a concurrent Dict (no profiler)
20		 3.2148 +- 0.2442			no changes but with the profilers on
21		14.8639 +- 0.5762			map to array, full of thread safty (no profiler)
22		 3.9023 +- 0.2561			map still array, full of thread unsafe (no profiler) 
23		 3.4712 +- 0.3434			no changes but with the profilers on. It seems are we collecting more garbage with the big array 
24		 3.7012 +- 0.2630			reverted to 19 (no profiler)
25		 3.7137 +- 0.3162			LargestOrFallback instead of order by (no profiler)
32		 1.1368 +- 0.1575			""
34		 3.4602 +- 0.2145			fixed a bug where HittableSpots2 got stuck in a loop (no profilers)
38		 0.7144 +- 0.1597			""
46		 2.1953 +- 0.3561			""
57		 0.5269 +- 0.1846			""
58		 0.3239	+- 0.1712			""
59		 0.2193 +- 0.2133			map refactor (no profiler)

MakeLineGame count=100 frames=100  (DEBUG) ------------------------------

RUN		TIME (s)					NOTES
26		37.0027 +- 3.8262			baseline (probably the same as 25 but I don't know) (no profiler)
27		33.6974	+- 3.6920			tried to add an early exit for targeting
28		12.1334 +- 1.2233			I noticed it was spending a lot of time in GC, HittableSpots uses a dict and a lot of allocations to halve the number of spots it has to check. I added HittableSpots2 which is much less fancy. (no profiler)
29		 7.5673 +- 0.2126			Replaced GetRaw with TryGetRaw I think this saves on the allocation of an enumerator (no profiler)
30		 6.0887	+- 0.2024			a simpler way to get the next square a bullet will enter (no profiler)
31		 6.1270 +- 0.1412			tried to early exit again, no luck. I improved my damage estimate (no profiler)
32		 5.9938 +- 0.1459			""
34		17.8065 +- 0.4355			""
35		 7.0174 +- 0.3233			parallel back on (no profiler)
36		 6.9422 +- 0.3687			
37		 2.5932 +- 0.3189			fixed a bug with expected damaged where targets that are far away were consider first
38		 1.2300 +- 0.1170			improve moving bullets, had a lot of pointless sorting (no profiler)
39		 1.5673 +- 0.2971			new fancy algirth to see where to shoot (no profiler)
40		 1.1432 +- 0.2406			fixed by in fancy algorithm from 39 (no profiler)
41		 1.0726 +- 0.1778           different counting pattern, more runs (no profiler)
42       0.8698 +- 0.1646			when one targeting ray hits someting check if any of the other rays hit it (no profiler)
46		 2.8480 +- 0.3858			""
47		 2.9910 +- 0.3371			saved a call to create a vector (no profiler)
48		 1.6956 +- 0.1835			saved a call to BaseDamage (no profiler)
49		 1.7403 +- 0.2524			removed an call to order by that is apparently so but also helps (no profiler)
50		 1.8641 +- 0.2222			reverted 49, tired to get cleaver and half sort the list (no profiler)
51		 1.6918 +- 0.1945			reverted 50 (no profiler)
52	   29?.???? +- ?.????			I didn't let it finishes, taking too long. after I added different levels of LOS. 3 runs finished 297, 302 and 295 (no profiler) 
53	   29?.???? +- ?.????			I didn't let it finishes, taking too long. I tried to switch how it decide who can see who. 1 run finished 297 (no profiler) 
54     28?.???? +- ?.????			I didn't let it finishes, taking too long. I moved a call to subtract around. 1 run finished 280 (no profiler)
55		 3.3117 +- 0.2432			""
56		 1.8685 +- 0.2928			More avoiding of hasing (no profiler) 
55		 0.6148 +- 0.2029			better loop (no profiler)
56       8.6524 +- 0.4457			chanaged damage by range and pierce by range, so that it gets a lot fewer early exits (no profiler)
57		 0.2626 +- 0.1043			moved targeting to it's own thread, update testing to use run (no profiler) - huh this speed up is way bigger than I'd expect, I'm aiming every 5 frames, best case it should be 1/5th the work, but it's like 40x better

MakeLineGame count=100 frames=10  (DEBUG) -------------------------------

RUN		TIME (s)					NOTES
54		32.3499 +- 1.2558			""
55		 0.4908 +- 0.1834			wow hashing is expensive, I avoided using a hashset. maybe I should think about cheaper hashing. (no profiler)
58		 0.1401 +- 0.2943			"" - think I make it calcuate who to shoot less often

Physics count=100 (10x10) frames=1_000 (DEBUG) --------------------------

RUN		TIME (s)					NOTES
42		70.8161 +- 1.8120			""
43		11.9959 +- ?.????			don't totally remove and readd the ants from the map (no profiler)
44		12.0082 +- 0.2012			removed a debugging contains (no profiler)
45		 2.8653 +- 0.1939			keep the list of ant moves sorted (no profiler)


ideas  ------------------------------------------------------------------
- take map out of state, when you roll back regenerate it from state ✅
- parallel? ✅
- make map an array
- stucts or atleast stack allocated?
- only duplicate state on change
- use MonsterIndexBackedIndex?
- numeric's vector2, I think it is SIMD
- how to spend less time drawing lines for selecting a target?
	- blockers tend to be thicker than 1 square so I can probably skip squares
	- if I find a blocker a can find it's center and draw lines back to the target to see what other points it blocks
	- I can stop drawing lines if my current outcome becomes worst than the best outcome I've found
	- targeting should be given many frames
- bullets don't actually have to move to the edge before moving