﻿
using Ants.Domain.State;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Numerics;
using System.Runtime.Intrinsics;


//ValueTuple<Func<(SimulationState, Map)>, int> pair = (MakeSimpleGame, 10_000);
ValueTuple<Func<SimulationState>, int> pair = (() => MakeLineGame(10), 1000);
//ValueTuple<Func<(SimulationState, Map)>, int> pair = (() => MakeLineGame(100), 100);
//ValueTuple<Func<(SimulationState, Map)>, int> pair = (() => MakeLineGame(100), 10);
//ValueTuple<Func<(SimulationState, Map)>, int> pair = (() => PhysicsGame(10), 1000);

var times = new List<double>();
for (int i = 0; i < 10; i++)
{
    //    var adv = new StateAdvancer();
    var terrainMap = new TerrainMap(new Dictionary<(int x, int y), Vector128<double>>(), new HashSet<(int x, int y)>());
    var stopwatch = new Stopwatch();
    var state = pair.Item1();
    var game = new Game(state, 1, 0, new MockSendData(), new Dictionary<(Pathing.Mode, double), TrainPaths>(), new ImmutableSimulationState(new RockState[0], new MudState[0], new CoverState[0], new KnownPathsImmutableState(new Paths[0])));

    // this feels like a bit of a hack, lock in all the orders before the game has started
    //for (int j = 0; j <= pair.Item2; j++
    //{
    //    game.LockInOrders(j);
    //}

    stopwatch.Start();
    var (frameProcessor, frameAdvancer, _, _, _) = game.Start(0/*hack, 0 ticks runing way behind!*/, new LosWorkerGroup(2));
    while (game.doNotPassOtherInputs.frame < pair.Item2)
    {
        await game.doNotPassOtherInputs.taskSource.WaitAsync().ConfigureAwait(false);
    }
    game.cancellationTokenSource.Cancel();
    stopwatch.Start();
    times.Add(stopwatch.Elapsed.TotalSeconds);
    Console.WriteLine($"run time: {stopwatch.Elapsed.TotalSeconds:F4}");
}

Console.WriteLine($"average run time: {times.Average():F4} ± {StandardDeviation(times):F4}");

//Console.WriteLine($"{WhoToShoot.earlyExit} vs {WhoToShoot.noEarlyExit}");


#pragma warning disable CS8321 // Local function is declared but never used
static SimulationState MakeSimpleGame()
{
    return MakeLineGame(1);
}

static SimulationState MakeLineGame(int count)
{
    var ants = new ConcurrentDictionary<Guid, AntState>();
    var paths = new KnownPathsImmutableState(new Paths[] { });

    for (int i = 0; i < count; i++)
    {
        AddAnt(ants, 0, Vector128.Create(-75.0, 100.0 *i));
        AddAnt(ants, 1, Vector128.Create(75.0, 100.0 * i));
    }
    var state = new SimulationState(
        ants,
        new ConcurrentDictionary<BulletState.Immutable.Id, BulletState>(),
        new MoneyState(new[] { 0.0, 0.0 }, new[] { 0.0, 0.0} ),
        new ControlState(100.0),
        Array.Empty<ControlPointState>(),
        new List<StationState>(),
        new PopulationState(5, new double[2]),
        new List<GrenadeState>(),
        new ResourceSpawnerState[0],
        new List<ResourceState>(),
        new Dictionary<Guid, SmokeState>(),
        new List<ShockwaveState>(),
        new ConcurrentDictionary<HealBulletState.Immutable.Id, HealBulletState>(),
        new ConcurrentDictionary<ArtyBulletState.Immutable.Id, ArtyBulletState>(),
        new List<ZapperSetupState>(),
        new ConcurrentDictionary<ArcBulletState.Immutable.Id, ArcBulletState>());
    return state;
}

static Game PhysicsGame(int sqrtCount)
{
    var ants = new ConcurrentDictionary<Guid, AntState>();
    //var map = new Map();
    var paths = new KnownPathsImmutableState(new Paths[] { });

    throw new NotImplementedException();

    //var orders = new List<(Guid,IPendingOrderState)>();

    //for (int x = 0; x < sqrtCount; x++)
    //{
    //    for (int y = 0; y < sqrtCount; y++)
    //    {
    //        var ant = AddAnt(ants, map, 0, new GlobalPosionInt(GlobalPosionInt.scale * 100 * x, GlobalPosionInt.scale * 100 * y));
    //        orders.Add((ant.immutable.id, new MovePendingImmutableState(new Godot.Vector2(0, 0))));
    //    }
    //}

    //var game = new Game(
    //    new State(
    //        ants,
    //        Array.Empty<RockState>(),
    //        paths,
    //        new ConcurrentDictionary<BulletState.Immutable.Id, BulletState>(),
    //        new MoneyState(new[] { 0.0, 0.0 }),
    //        new ControlState(new[] { 0.0, 0.0 }),
    //        Array.Empty<ControlPointImmutableState>()));

    //foreach (var (id, order) in orders)
    //{
    //    game.Order(id, order);
    //}

    //return game;
}
#pragma warning restore CS8321 // Local function is declared but never used

static double StandardDeviation(List<double> values)
{
    double avg = values.Average();
    double sum = values.Sum(d => Math.Pow(d - avg, 2));
    return Math.Sqrt((sum) / (values.Count() - 1));
}

static AntState AddAnt(ConcurrentDictionary<Guid, AntState> ants, int side, Vector128<double> position)
{
    var ant = AntFactory.Make(Guid.NewGuid(), AntType.Basic, side, position);
    ants.TryAdd(ant.immutable.id, ant);
    return ant;
}

class MockSendData : ISendData
{
    public Task Send(Game.WaitingFrame waitingFrame)
    {
        return Task.CompletedTask;
    }
}