﻿// See https://aka.ms/new-console-template for more information

using System;
using System.Drawing;
using System.Drawing.Imaging;

{
    TestMazeImage();
}

void RandomImage()
{
    int width = 900;
    int height = 900;

    WriteImage(Normals(RandomHeights(width, height)), "random-normals");
}



void TestImage()
{
    int width = 900;
    int height = 900;

    WriteImage(TestNormals(RandomHeights(width, height)), "stupid-normals");
}

void TestMazeImage()
{
    int width = 900;
    int height = 900;

    var maze0 = Maze(width / 9, height / 9);

    var strechedMaze0 = Stretch(maze0, 9);

    WriteImage(Normals(strechedMaze0), "test-maze-normals");
}

void MazeImage()
{
    int width = 900;
    int height = 900;

    var maze0 = Maze(width / 9, height / 9);
    var maze1 = Maze(width / 3, height / 3);
    var maze2 = Maze(width, height);

    var strechedMaze0 = Stretch(maze0, 3);

    var combinedMaze01 = Combine(strechedMaze0, maze1, .5);
    var strechedMaze01 = Stretch(combinedMaze01, 3);
    var combinedMaze012 = Combine(strechedMaze01, maze2, .5);
    var normals = Normals(combinedMaze012);

    WriteImage(normals, "maze-normals");
}

// returns x,y, rgb
Color[][] Normals(double[][] heights/*between 0 and 1*/)
{
    var res = new Color[heights.Length][];
    for (int y = 0; y < heights.Length; y++)
    {
        res[y] = new Color[heights[y].Length];
        for (int x = 0; x < heights[y].Length; x++)
        {
            var topLeft = heights[x % heights[y].Length][y % heights.Length];
            var topRight = heights[(x + 1) % heights[y].Length][y % heights.Length];
            var bottomLeft = heights[x % heights[y].Length][(y + 1) % heights.Length];
            var bottomRight = heights[(x + 1) % heights[y].Length][(y + 1) % heights.Length];

            //var dx = .5 + (((topLeft - topRight) + (bottomLeft - bottomRight)) / 4.0);
            //var dy = .5 + (((topLeft - bottomLeft) + (topRight - bottomRight)) / 4.0);

            var dx = .5;
            var dy = .5;

            var averageHeight = (topLeft + topRight + bottomLeft + bottomRight) / 4.0;

            // I have an issue 
            // idk why 
            // I'd think this would end up RGB
            // but the first byte ends up in blue...
            // also godot can't up my images
            // so... I think I'm not doing it correctly
            res[y][x] = Color.FromArgb( (int)(0xff * dx), (int)(0xff * dy), (int)(0xff * averageHeight));
        }
    }
    return res;
}

// seems low values make it less effected by light
// which is cool
// I can use that
// naw that was a bug
// b is not used
// ...so shade it myself?
// ...use... hmmm

Color[][] TestNormals(double[][] heights/*between 0 and 1*/)
{
    var res = new Color[heights.Length][];
    for (int y = 0; y < heights.Length; y++)
    {
        res[y] = new Color[heights[y].Length];
        for (int x = 0; x < heights[y].Length; x++)
        {
            res[y][x] = Color.FromArgb((int)(0xff * .5), (int)(0xff * .5), (int)(0xff * heights[y][x]));
        }
    }
    return res;
}

double[][] TopLeftHigh(int width, int height)
{
    var passOne = RandomHeights(width, height);

    for (int y = 0; y < height / 2; y++)
    {
        for (int x = 0; x < width / 2; x++)
        {
            var p00 = passOne[(2 * y)][(2 * x)];
            var p01 = passOne[(2 * y)][(2 * x) + 1];
            var p10 = passOne[(2 * y) + 1][(2 * x)];
            var p11 = passOne[(2 * y) + 1][(2 * x) + 1];

            var sorted = new[] { p00, p01, p10, p11 }.OrderBy(x => x).ToArray();

            passOne[(2 * y)][(2 * x)] = sorted[0];
            passOne[(2 * y)][(2 * x) + 1] = sorted[1];
            passOne[(2 * y) + 1][(2 * x)] = sorted[2];
            passOne[(2 * y) + 1][(2 * x) + 1] = sorted[3];
        }
    }
    return passOne;
}

double[][] Maze(int width, int height)
{
    var passOne = RandomHeights(width, height);

    for (int y = 0; y < height; y++)
    {
        for (int x = 0; x < width; x++)
        {
            if (x % 2 == 0 && y % 2 == 0)
            {
                passOne[y][x] = (passOne[y][x] * .4) + .6;
            }
            else if (x % 2 == 1 && y % 2 == 1)
            {
                passOne[y][x] = (passOne[y][x] * .4);
            }
            else
            {
                if (passOne[y][x] < .5)
                {
                    // .5 -> .4
                    passOne[y][x] = passOne[y][x] * (.4 / .5);
                }
                else
                {
                    // .5 -> .6
                    passOne[y][x] = ((passOne[y][x] - .5) * (.4 / .5)) + .6;
                }
            }
        }
    }
    return passOne;
}

void WriteImage(Color[][] data, string name)
{
    //var imageData = data.SelectMany(row => row.SelectMany(rgb => rgb)).ToArray();
    var width = data[0].Length;
    var height = data.Length;
    unsafe
    {
        //fixed (byte* ptr = imageData)
        //{
        using (Bitmap image = new Bitmap(width, height))
        {

            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    image.SetPixel(x, y, data[y][x]);
                }
            }

            // from C:\Users\Colin\source\Ants\godot_ants\ImageGen2\bin\Debug\net6.0
            // to   C:\Users\Colin\source\Ants\godot_ants\Ants\images
            image.Save(@$"..\..\..\..\Ants\images\{name}.bmp");
        }
        //}
    }
}

double[][] RandomHeights(int width, int height)
{
    var random = new Random();
    return Range(height).Select(_ => Range(width).Select(_ => random.NextDouble()).ToArray()).ToArray();
}

IEnumerable<int> Range(int range)
{
    for (int i = 0; i < range; i++)
    {
        yield return i;
    }
}

double[][] Stretch(double[][] heights, int by)
{
    var res = new double[heights.Length * by][];
    for (int y = 0; y < heights.Length; y++)
    {
        for (int iy = 0; iy < by; iy++)
        {
            res[(y * by) + iy] = new double[heights[y].Length * by];
            for (int x = 0; x < heights[y].Length; x++)
            {
                for (int ix = 0; ix < by; ix++)
                {
                    res[(y * by) + iy][(x * by) + ix] = heights[y][x];
                }
            }
        }
    }
    return res;
}

double[][] Combine(double[][] heights1, double[][] heights2, double part1)
{
    var res = new double[heights1.Length][];

    for (int y = 0; y < heights1.Length; y++)
    {
        res[y] = new double[heights1[y].Length];
        for (int x = 0; x < heights1[y].Length; x++)
        {

            res[y][x] = (heights1[y][x] * part1) + (heights2[y][x] * (1 - part1));
        }
    }
    return res;
}
