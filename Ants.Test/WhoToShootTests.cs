﻿using Ants.Domain.Infrastructure;
using Ants.Domain.State;
using Prototypist.Toolbox;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Intrinsics;
using System.Text;
using System.Threading.Tasks;

namespace Ants.Test
{
    public class WhoToShootTests
    {

        //[Fact]
        //public void HittableSpots_ClearShot()
        //{
        //    var target = new AntState(
        //        100,
        //        new AntState.Immutable(Guid.NewGuid(), 0, 100, 600, new MoveAction(), new Dictionary<Key, IKeyAction>(), new Dictionary<Key, IPendingClickOrderState>(), new Mappable(Pathing.unitSize_Medium), Pathing.Mode.SlowMud),
        //         new GlobalPosionInt(100_000, 100_000));
        //    var shooter = new AntState(
        //        100,
        //        new AntState.Immutable(Guid.NewGuid(), 0, 100, 600, new MoveAction(), new Dictionary<Key, IKeyAction>(), new Dictionary<Key, IPendingClickOrderState>(), new Mappable(Pathing.unitSize_Medium), Pathing.Mode.SlowMud),
        //        new GlobalPosionInt(0, 0));

        //    var ants = new[] { target, shooter }.ToDictionary(x => x.immutable.id, x => x);

        //    var map = new Map();
        //    map.AddToMap(target, target.GlobalPositionInt);
        //    map.AddToMap(shooter, shooter.GlobalPositionInt);

        //    var couldHit = WhoToShoot.CouldHit(shooter.GlobalPosition, target.immutable.mappable.radius, target.GlobalPosition);

        //    var res = WhoToShoot.HittableSpots(shooter, shooter.GlobalPosition, target, map, target.GlobalPosition);

        //    Assert.Equal(couldHit.Count, res.Length);

        //}



        //[Fact]
        //public void HittableSpots3_ClearShot()
        //{
        //    var target = new AntState(
        //        100,
        //        new AntState.Immutable(Guid.NewGuid(), 0, 100, 900, 600, 300, new MoveAction(), new Dictionary<Key, IKeyAction>(), new Dictionary<Key, IPendingClickOrderState>(), new Mappable(Pathing.unitSize_Medium), Pathing.Mode.SlowMud, AntType.Heavy/* TODO probably I should just use the ant factory*/),
        //         new GlobalPosionInt(100_000, 100_000));
        //    var shooter = new AntState(
        //        100,
        //        new AntState.Immutable(Guid.NewGuid(), 0, 100, 900, 600, 300, new MoveAction(), new Dictionary<Key, IKeyAction>(), new Dictionary<Key, IPendingClickOrderState>(), new Mappable(Pathing.unitSize_Medium), Pathing.Mode.SlowMud, AntType.Heavy),
        //        new GlobalPosionInt(0, 0));


        //    var ants = new ConcurrentDictionary<Guid, AntState>();
        //    ants[target.immutable.id] = target;
        //    ants[shooter.immutable.id] = shooter;

        //    var map = new Map();
        //    map.AddToMap(target, target.GlobalPositionInt);
        //    map.AddToMap(shooter, shooter.GlobalPositionInt);

        //    var couldHit = WhoToShoot.CouldHit(shooter.GlobalPosition, target.immutable.mappable.radius, target.GlobalPosition);

        //    var terrainMap = new TerrainMap(new(), new HashSet<(int x, int y)>());

        //    var res = WhoToShoot.HittableSpots3(shooter, shooter.GlobalPosition, target, map, target.GlobalPosition, ants, terrainMap);

        //    Assert.Equal(couldHit.Count, res.Length);

        //}

        //[Fact]
        //public void HittableSpots()
        //{
        //    var target = new AntState(
        //        100,
        //        new AntState.Immutable(Guid.NewGuid(), 0, 100, 600, new MoveAction(), new Dictionary<Key, IKeyAction>(), new Dictionary<Key, IPendingClickOrderState>(), new Mappable(Pathing.unitSize_Medium), Pathing.Mode.SlowMud),
        //        new GlobalPosionInt(100_000, 0));
        //    var shooter = new AntState(
        //        100,
        //        new AntState.Immutable(Guid.NewGuid(), 0, 100, 600, new MoveAction(), new Dictionary<Key, IKeyAction>(), new Dictionary<Key, IPendingClickOrderState>(), new Mappable(Pathing.unitSize_Medium), Pathing.Mode.SlowMud),
        //        new GlobalPosionInt(0, 0));
        //    var rock = new RockState(new RockState.RockStateImmutable(new Mappable(50)), new GlobalPosionInt(50_000, 50_000));

        //    var ants = new[] { target, shooter }.ToDictionary(x => x.immutable.id, x => x);

        //    var map = new Map();
        //    map.AddToMap(target, target.GlobalPositionInt);
        //    map.AddToMap(shooter, shooter.GlobalPositionInt);
        //    map.AddToMap(rock, rock.GlobalPositionInt);

        //    var couldHit = WhoToShoot.CouldHit(shooter.GlobalPosition, target.immutable.mappable.radius, target.GlobalPosition);

        //    var res = WhoToShoot.HittableSpots(shooter, shooter.GlobalPosition, target, map, target.GlobalPosition);

        //    Assert.Equal(Math.Floor(couldHit.Count / 2.0), res.Length);
        //}


        //[Fact]
        //public void HittableSpots3()
        //{
        //    var target = new AntState(
        //        100,
        //        new AntState.Immutable(Guid.NewGuid(), 0, 100, 900, 600,300, new MoveAction(), new Dictionary<Key, IKeyAction>(), new Dictionary<Key, IPendingClickOrderState>(), new Mappable(Pathing.unitSize_Medium), Pathing.Mode.SlowMud, AntType.Heavy),
        //        new GlobalPosionInt(100_000, 0));
        //    var shooter = new AntState(
        //        100,
        //        new AntState.Immutable(Guid.NewGuid(), 0, 100, 900, 600, 300, new MoveAction(), new Dictionary<Key, IKeyAction>(), new Dictionary<Key, IPendingClickOrderState>(), new Mappable(Pathing.unitSize_Medium), Pathing.Mode.SlowMud, AntType.Heavy),
        //        new GlobalPosionInt(0, 0));
        //    var rock = new RockState(new RockState.RockStateImmutable(new Mappable(50)), new GlobalPosionInt(50_000, 50_000));


        //    var ants = new ConcurrentDictionary<Guid, AntState>();
        //    ants[target.immutable.id] = target;
        //    ants[shooter.immutable.id] = shooter;

        //    var map = new Map();
        //    map.AddToMap(target, target.GlobalPositionInt);
        //    map.AddToMap(shooter, shooter.GlobalPositionInt);
        //    map.AddToMap(rock, rock.GlobalPositionInt);

        //    var couldHit = WhoToShoot.CouldHit(shooter.GlobalPosition, target.immutable.mappable.radius, target.GlobalPosition);

        //    var terrainMap = new TerrainMap(new(), new HashSet<(int x, int y)>());

        //    var res = WhoToShoot.HittableSpots3(shooter, shooter.GlobalPosition, target, map, target.GlobalPosition, ants, terrainMap);

        //    Assert.Equal(Math.Floor(couldHit.Count / 2.0), res.Length);
        //}

        //[Fact]
        //public void HittableSpots3_FindsCover()
        //{
        //    var target = new AntState(
        //        100,
        //        new AntState.Immutable(Guid.NewGuid(), 0, 100, 900, 600, 300, new MoveAction(), new Dictionary<Key, IKeyAction>(), new Dictionary<Key, IPendingClickOrderState>(), new Mappable(Pathing.unitSize_Medium), Pathing.Mode.SlowMud, AntType.Heavy),
        //        new GlobalPosionInt(100_000, 0));
        //    var shooter = new AntState(
        //        100,
        //        new AntState.Immutable(Guid.NewGuid(), 0, 100, 900, 600, 300, new MoveAction(), new Dictionary<Key, IKeyAction>(), new Dictionary<Key, IPendingClickOrderState>(), new Mappable(Pathing.unitSize_Medium), Pathing.Mode.SlowMud, AntType.Heavy),
        //        new GlobalPosionInt(0, 0));


        //    var ants = new ConcurrentDictionary<Guid, AntState>();
        //    ants[target.immutable.id] = target;
        //    ants[shooter.immutable.id] = shooter;

        //    var map = new Map();
        //    map.AddToMap(target, target.GlobalPositionInt);
        //    map.AddToMap(shooter, shooter.GlobalPositionInt);

        //    var couldHit = WhoToShoot.CouldHit(shooter.GlobalPosition, target.immutable.mappable.radius, target.GlobalPosition);

        //    var cover = new HashSet<(int x, int y)>();
        //    for (double y = -50; y <= 50; y++)
        //    {
        //        cover.Add(Vector128.Create(50.0, y).ToMapPosition());
        //    }

        //    var terrainMap = new TerrainMap(new (), cover);

        //    var res = WhoToShoot.HittableSpots3(shooter, shooter.GlobalPosition, target, map, target.GlobalPosition, ants, terrainMap);

        //    Assert.Equal(50.0, res.Select(x => x.furthestCover).Min());
        //    Assert.True(res.All(x => x.furthestCover != 0));
        //}

        [Fact]
        public void HealShootAt()
        {
            var shootAt = WhoToShoot.HealAt(
                targets: new Target[] {
                    new Target(OrType.Make<AntState, ZapperSetupState>(new AntState(
                        hp: 100,
                        new AntState.Immutable(
                            id: Guid.NewGuid(),
                            side: 1,
                            maxHp: 100,
                            lineOfSightFar: 300,
                            lineOfSight: 200,
                            lineOfSightDetection: 100,
                            actions: new Dictionary<(Ants.Domain.Key key, int pageIndex), AntAction>(),
                            mudMode: Pathing.Mode.SlowMud,
                            AntType.Basic
                            ),
                        position: Vector128.Create(100.0, 100.0),
                        radius: 100)),
                        projectedPosition: Vector128.Create(100.0, 100.0),
                        fullyVisible: true)
                },
                rocks: new IBlocksShots[0],
                cover: new CoverState[0],
                shoortFrom: Vector128.Create(0.0, 0.0),
                healGunState: new HealGunState(new HealGunState.Immutable(
                    healing: 1.0,
                    shotSpeed: 10,
                    scatterAngleRad: (30 * Math.Tau) / 360,
                    relaodTimeTick: 10,
                    canMoveAndShoot: true,
                    canMoveAndRelaod: true,
                    range: 1000,
                    rangeScatter: 100,
                    setUpTimeTicks: 0,
                    shots: 1,
                    ignoreCoverFor: 1000,
                    ignoreCoverForScatter: 100
                    )),
                tick: 0,
                shooterRadius: Pathing.unitSize_Medium);

            Assert.NotNull(shootAt);

            var x = Math.Atan(shootAt.Value.position.X() / shootAt.Value.position.Y()) * 360 / Math.Tau;

            Assert.Equal(90, x, 5);
        }

        [Fact]
        public void ArcShootAt()
        {
            var dis = 800*Math.Sqrt(2) / 2.0;
            var shootAt = WhoToShoot.ArcShootAt(
                targets: new Target[] {
                    new Target(OrType.Make<AntState, ZapperSetupState>(new AntState(
                        hp: 100,
                        new AntState.Immutable(
                            id: Guid.NewGuid(),
                            side: 1,
                            maxHp: 100,
                            lineOfSightFar: 300,
                            lineOfSight: 200,
                            lineOfSightDetection: 100,
                            actions: new Dictionary<(Ants.Domain.Key key, int pageIndex), AntAction>(),
                            mudMode: Pathing.Mode.SlowMud,
                            AntType.Basic
                            ),
                        position: Vector128.Create(dis, -dis),
                        radius: 100)),
                        projectedPosition: Vector128.Create(dis, -dis),
                        fullyVisible: true)
                },
                rocks: new IBlocksShots[0],
                cover: new CoverState[0],
                shoortFrom: Vector128.Create(0.0, 0.0),
                gunState: new ArcGunState(new ArcGunState.Immutable(
                    pierce: 1.0,
                    damage: 1.0,
                    shotSpeed: 10,
                    scatterAngleRad: (30 * Math.Tau) / 360,
                    relaodTimeTick: 10,
                    canMoveAndShoot: true,
                    canMoveAndRelaod: true,
                    range: 1000,
                    rangeScatter: 100,
                    setUpTimeTicks: 0,
                    shots: 1,
                    ignoreCoverFor: 1000,
                    ignoreCoverForScatter: 100,
                    autoShoot: true,
                    stun: 0,
                    knockBack: 0,
                    radius: 500
                    )),
                tick: 0,
                shooterId: Guid.NewGuid());

            Assert.NotNull(shootAt);

            var x = Math.Atan(shootAt.Value.point.X() / shootAt.Value.point.Y()) * 360 / Math.Tau;

            Assert.Equal(-49.942175135647609, x, 5);
        }

        [Fact]
        public void ShootAt() {
            var shootAt = WhoToShoot.ShootAt(
                targets: new Target[] {
                    new Target(OrType.Make<AntState, ZapperSetupState>(new AntState(
                        hp: 100,
                        new AntState.Immutable(
                            id: Guid.NewGuid(),
                            side: 1,
                            maxHp: 100,
                            lineOfSightFar: 300,
                            lineOfSight: 200,
                            lineOfSightDetection: 100,
                            actions: new Dictionary<(Ants.Domain.Key key, int pageIndex), AntAction>(),
                            mudMode: Pathing.Mode.SlowMud,
                            AntType.Basic
                            ),
                        position: Vector128.Create(100.0,100.0),
                        100)),
                        projectedPosition: Vector128.Create(100.0, 100.0),
                        fullyVisible: true)
                }, 
                rocks: new IBlocksShots[0], 
                cover: new CoverState[0], 
                shoortFrom: Vector128.Create(0.0, 0.0), 
                gunState: new GunState(new GunState.Immutable(
                    pierce: 1.0,
                    damage: 1.0,
                    shotSpeed: 10,
                    scatterAngleRad: (30 * Math.Tau) / 360,
                    relaodTimeTick: 10,
                    canMoveAndShoot: true,
                    canMoveAndRelaod: true,
                    range: 1000,
                    rangeScatter: 100,
                    setUpTimeTicks: 0,
                    shots: 1,
                    ignoreCoverFor: 1000,
                    ignoreCoverForScatter: 100,
                    autoShoot: true,
                    stun: 0,
                    knockBack: 0
                    )),
                tick:0,
                shooterId: Guid.NewGuid(),
                shooterRaidus: Pathing.unitSize_Medium);

            Assert.NotNull(shootAt);

            var x = Math.Atan(shootAt.Value.point.X() / shootAt.Value.point.Y())* 360 / Math.Tau;

            Assert.Equal(90,x, 5);

        }

        [Fact]
        public void BestSpotToShootAtAndOddsToHit_ClearShot() {
            var hittableSpots = new (Vector128<double> shootAt, double furthestCover)[] {
                (Vector128.Create(1000.0,-30.0),0),
                (Vector128.Create(1000.0,-20.0),0),
                (Vector128.Create(1000.0,-10.0),0),
                (Vector128.Create(1000.0,0.0),0),
                (Vector128.Create(1000.0,10.0),0),
                (Vector128.Create(1000.0,20.0),0),
                (Vector128.Create(1000.0,30.0),0),
            };

            var res = WhoToShoot.BestSpotToShootAtAndOddsToHit(hittableSpots, Vector128.Create(0.0,0.0),Math.PI/12, 1000, 1);

            Assert.Equal(Vector128.Create(1000.0, 0.0), res.shootAt);
        }

        [Fact]
        public void BestSpotToShootAtAndOddsToHit_Cover()
        {
            var hittableSpotsNoCover = new (Vector128<double> shootAt, double furthestCover)[] {
                (Vector128.Create(1000.0,-30.0),0),
                (Vector128.Create(1000.0,-20.0),0),
                (Vector128.Create(1000.0,-10.0),0),
                (Vector128.Create(1000.0,0.0),0),
                (Vector128.Create(1000.0,10.0),0),
                (Vector128.Create(1000.0,20.0),0),
                (Vector128.Create(1000.0,30.0),0),
            };

            var noCover = WhoToShoot.BestSpotToShootAtAndOddsToHit(hittableSpotsNoCover, Vector128.Create(0.0, 0.0), Math.PI / 12, 1000, 100);

            var hittableSpotsCover = new (Vector128<double> shootAt, double furthestCover)[] {
                (Vector128.Create(1000.0,-30.0),1000),
                (Vector128.Create(1000.0,-20.0),1000),
                (Vector128.Create(1000.0,-10.0),1000),
                (Vector128.Create(1000.0,0.0),1000),
                (Vector128.Create(1000.0,10.0),1000),
                (Vector128.Create(1000.0,20.0),1000),
                (Vector128.Create(1000.0,30.0),1000),
            };

            var cover = WhoToShoot.BestSpotToShootAtAndOddsToHit(hittableSpotsCover, Vector128.Create(0.0, 0.0), Math.PI / 12, 1000, 100);

            Assert.True(noCover.oddsToHit > cover.oddsToHit);
        }


        [Fact]
        public void BestSpotToShootAtAndOddsToHit_MoreHittables()
        {
            var hittableSpots = new (Vector128<double> shootAt, double furthestCover)[] {
                (Vector128.Create(1000.0,-30.0),0),
                (Vector128.Create(1000.0,-20.0),0),
                (Vector128.Create(1000.0,-10.0),0),
                (Vector128.Create(1000.0,0.0),0)
            };

            var less = WhoToShoot.BestSpotToShootAtAndOddsToHit(hittableSpots, Vector128.Create(0.0, 0.0), Math.PI / 12, 1000, 100);

            var hittableSpotsMore = new (Vector128<double> shootAt, double furthestCover)[] {
                (Vector128.Create(1000.0,-30.0), 0),
                (Vector128.Create(1000.0,-20.0), 0),
                (Vector128.Create(1000.0,-10.0), 0),
                (Vector128.Create(1000.0,0.0), 0),
                (Vector128.Create(1000.0,10.0), 0),
                (Vector128.Create(1000.0,20.0), 0),
                (Vector128.Create(1000.0,30.0), 0),
            };

            var more = WhoToShoot.BestSpotToShootAtAndOddsToHit(hittableSpotsMore, Vector128.Create(0.0, 0.0), Math.PI / 12, 1000, 100);

            Assert.True(less.oddsToHit < more.oddsToHit);
        }


        [Fact]
        public void BestSpotToShootAtAndOddsToHit_Range()
        {
            var closeSpots = new (Vector128<double> shootAt, double furthestCover)[] {
                (Vector128.Create(1000.0,-30.0),0),
                (Vector128.Create(1000.0,-20.0),0),
                (Vector128.Create(1000.0,-10.0),0),
                (Vector128.Create(1000.0,0.0),0),
                (Vector128.Create(1000.0,10.0),0),
                (Vector128.Create(1000.0,20.0),0),
                (Vector128.Create(1000.0,30.0),0),
            };

            var close = WhoToShoot.BestSpotToShootAtAndOddsToHit(closeSpots, Vector128.Create(0.0, 0.0), Math.PI / 12, 1000, 100);

            var farSpots = new (Vector128<double> shootAt, double furthestCover)[] {
                (Vector128.Create(2000.0,-30.0), 0),
                (Vector128.Create(2000.0,-20.0), 0),
                (Vector128.Create(2000.0,-10.0), 0),
                (Vector128.Create(2000.0,0.0), 0),
                (Vector128.Create(2000.0,10.0), 0),
                (Vector128.Create(2000.0,20.0), 0),
                (Vector128.Create(2000.0,30.0), 0),
            };

            var far = WhoToShoot.BestSpotToShootAtAndOddsToHit(farSpots, Vector128.Create(0.0, 0.0), Math.PI / 12, 1000, 100);

            Assert.True(close.oddsToHit > far.oddsToHit);
        }
    }
}
