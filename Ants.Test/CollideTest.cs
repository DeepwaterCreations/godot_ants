﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Intrinsics;
using System.Text;
using System.Threading.Tasks;

namespace Ants.Test
{
    
    public class CollideTest
    {
        // these actually aren't even moving toward each other 
        //[Fact]
        //public void Test() {
        //    MapExtensions.Collide(
        //        Vector128.Create(-0.089, -0.047),
        //        Vector128.Create(-0.079, -0.033),
        //        Vector128.Create(-180.0, 5.0),
        //        Vector128.Create(-110.0, -45.0),
        //        1,
        //        1
        //        );
        //}

        [Fact]
        public void MovingTowardEachOther() {
            Assert.False(MapExtensions.MovingTowardsEachOther(
                Vector128.Create(-0.089, -0.047),
                Vector128.Create(-0.079, -0.033),
                Vector128.Create(-180.0, 5.0),
                Vector128.Create(-110.0, -45.0)
                ));
        }
    }
}
