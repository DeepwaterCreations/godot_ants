﻿using Ants.AppState;
using Ants.Network.Messages;
using static Ants.Network.Messages.FrameData;

namespace Ants.Test
{
    public class SerializeMessageTests
    {
        [Fact]
        public void JoinGameSerialize() {
            Network.Messages.JoinGame original = new Ants.Network.Messages.JoinGame();
            Network.Messages.JoinGame looped = Network.Messages.JoinGame.Deserialize(original.Serialize());
            Assert.Equal(original, looped);
        }

        [Fact]
        public void JoinGameAckSerialize()
        {
            var original = new Ants.Network.Messages.JoinGameAck(5);
            var looped = Network.Messages.JoinGameAck.Deserialize(original.Serialize());
            Assert.Equal(original, looped);
        }

        [Fact]
        public void StartingGameSerialize()
        {
            StartingGame original = new StartingGame(1231289712223342343L);
            StartingGame looped = StartingGame.Deserialize(original.Serialize());
            Assert.Equal(original, looped);
        }

        [Fact]
        public void LagTestSerialize()
        {
            var original = new LagTest(234,1231289712223342343L);
            var looped = LagTest.Deserialize(original.Serialize());
            Assert.Equal(original, looped);
        }

        [Fact]
        public void LagTestAckSerialize()
        {
            var original = new LagTestAck(1231234712343342343L, 1231289712223342343L);
            var looped = LagTestAck.Deserialize(original.Serialize());
            Assert.Equal(original, looped);
        }

        [Fact]
        public void LobbyStateSerialize()
        {
            LobbyState original = new LobbyState(new PlayerData[] {
                new PlayerData(new byte[16]{ 0x83, 0x34, 0x12, 0x34, 0xff, 0x2f, 0x14,0x12,0x34, 0x34,0x34, 0xaa, 0xdd, 0xcc,0xbb, 0x00}, 10), 
                new PlayerData(new byte[16]{ 0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66,0x77,0x88, 0x99,0xaa, 0xbb, 0xcc, 0xdd,0xee, 0xff}, 10)
            });
            LobbyState looped = LobbyState.Deserialize(original.Serialize());
            Assert.Equal(original, looped);
        }

        [Fact]
        public void PlayerDataSerialize() {
            var original = new PlayerData(new byte[16] { 0x83, 0x34, 0x12, 0x34, 0xff, 0x2f, 0x14, 0x12, 0x34, 0x34, 0x34, 0xaa, 0xdd, 0xcc, 0xbb, 0x00 }, 10);
            var at = 0;
            var looped = PlayerData.Deserialize(original.Serialize(), ref at);
            Assert.Equal(original, looped);
        }

        [Fact]
        public void FrameDataSerialize() 
        {
            var original = new FrameData(10, 14, new OrderData[][]
            { 
                new OrderData[]{
                    new OrderData(
                        id: Guid.NewGuid(), 
                        order:new EggPendingOrder(AntType.Basic, Guid.NewGuid(), /*Guid.NewGuid(),*/ new Godot.Vector2(123f,123f).ToNumericsVector(), new Godot.Vector2(12f,12f).ToNumericsVector()) ,
                        queue: true
                    ),
                    new OrderData(   
                        id:Guid.NewGuid(),
                        order:new ShootPendingOrder(new Godot.Vector2(123.2f,98234.324f).ToNumericsVector()),
                        queue: false
                    )
                },
                new OrderData[]{
                    new OrderData(
                        id:Guid.NewGuid(),
                        order:new SprintPendingOrder(new MovePendingImmutableState(new []{ new Godot.Vector2(234f,234.234f).ToNumericsVector() }, 10)),
                        queue: false
                    ),
                    new OrderData(
                        id:Guid.NewGuid(),
                        order:new PendingMoveWithShiledUpOrderImmutableState(new MovePendingImmutableState(new []{new Godot.Vector2(234f, 234.234f).ToNumericsVector() }, 10)),
                        queue: false
                    ),
                    new OrderData(
                        id:Guid.NewGuid(),
                        order:new MovePendingImmutableState(new []{new Godot.Vector2(234f, 234.234f).ToNumericsVector() }, 10),
                        queue: true
                    ),
                    new OrderData(
                        id:Guid.NewGuid(),
                        order:new MakeUnitOrderImmutableState(Guid.NewGuid(), AntType.Basic, /*Guid.NewGuid(),*/  new Godot.Vector2(123f,123f).ToNumericsVector(),  new Godot.Vector2(1234f,1234f).ToNumericsVector()),
                        queue: false
                    )
                }
            }, 
            new int[] { 1,2,3,8},
            12,
            new int[] { 234123, 234123},
            new int[] { 12,23,34,3});
            byte[] bytes = original.Serialize();
            FrameData looped = FrameData.Deserialize(bytes);
            Assert.Equal(original, looped);
        }
    }

    public class SerializeOrderTests
    {
        [Fact]
        public void EggPendingOrderSerialize()
        {
            EggPendingOrder original = new EggPendingOrder(AntType.Basic, Guid.NewGuid(), /*Guid.NewGuid(),*/ new Godot.Vector2(123f, 123f).ToNumericsVector(), new Godot.Vector2(12f, 12f).ToNumericsVector());
            EggPendingOrder looped = EggPendingOrder.Deserialize(original.Serialize());
            Assert.Equal(original, looped);
        }
        [Fact]
        public void ShootPendingOrderSerialize()
        {
            ShootPendingOrder original = new ShootPendingOrder(new Godot.Vector2(123.2f,98234.324f).ToNumericsVector());
            ShootPendingOrder looped = ShootPendingOrder.Deserialize(original.Serialize());
            Assert.Equal(original, looped);
        }
        [Fact]
        public void SprintPendingOrderSerialize()
        {
            SprintPendingOrder original = new SprintPendingOrder(new MovePendingImmutableState(new[] { new Godot.Vector2(234f, 234.234f).ToNumericsVector() }, 10));
            SprintPendingOrder looped = SprintPendingOrder.Deserialize(original.Serialize());
            Assert.Equal(original, looped);
        }
        [Fact]
        public void PendingMoveWithShiledUpOrderImmutableStateSerialize()
        {
            PendingMoveWithShiledUpOrderImmutableState original = new PendingMoveWithShiledUpOrderImmutableState(new MovePendingImmutableState(new[] { new Godot.Vector2(234f, 234.234f).ToNumericsVector() }, 10));
            PendingMoveWithShiledUpOrderImmutableState looped = PendingMoveWithShiledUpOrderImmutableState.Deserialize(original.Serialize());
            Assert.Equal(original, looped);
        }

        [Fact]
        public void MovePendingImmutableStateSerialize()
        {
            MovePendingImmutableState original = new MovePendingImmutableState(new[] { new Godot.Vector2(234f, 234.234f).ToNumericsVector() }, 10);
            MovePendingImmutableState looped = MovePendingImmutableState.Deserialize(original.Serialize());
            Assert.Equal(original, looped);
        }
        [Fact]
        public void MakeUnitOrderImmutableStateSerialize()
        {
            MakeUnitOrderImmutableState original = new MakeUnitOrderImmutableState(Guid.NewGuid(), AntType.Basic, /*Guid.NewGuid(),*/ new Godot.Vector2(123f, 123f).ToNumericsVector(), new Godot.Vector2(1234f, 1234f).ToNumericsVector());
            MakeUnitOrderImmutableState looped = MakeUnitOrderImmutableState.Deserialize(original.Serialize());
            Assert.Equal(original, looped);
        }
    }
}
