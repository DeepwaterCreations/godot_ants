﻿using Ants.Domain.State;
using Ants.V2;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Intrinsics;
using System.Text;
using System.Threading.Tasks;
using static BulletState.Immutable;
using static Pathing;

namespace Ants.Test
{
    public class StateHash
    {
        [Fact]
        public void Simple() {
            var ants1 = new ConcurrentDictionary<Guid, AntState>();

            var target1 = AntFactory.Make(Guid.Parse("{51334B1F-3EC2-49E7-B9E9-EE45A4616171}"), AntType.Basic, 1, Vector128.Create(0.0, 0.0));

            ants1.TryAdd(target1.immutable.id, target1);

            var state1 = new SimulationState(
                        ants1,
                        new ConcurrentDictionary<Id, BulletState>(),
                        new MoneyState(new[] { 0.0, 0.0 }, new[] { 0.0, 0.0 }),
                        new ControlState(100.0),
                        Array.Empty<ControlPointState>(),
                        new List<StationState>(),
                        new PopulationState(5, new[] { 0.0, 0.0 }),
                        new List<GrenadeState>(),
                        Array.Empty<ResourceSpawnerState>(),
                        new List<ResourceState>(),
                        new Dictionary<Guid, SmokeState>(),
                        new List<ShockwaveState>(),
                        new ConcurrentDictionary<HealBulletState.Immutable.Id, HealBulletState>(),
                        new ConcurrentDictionary<ArtyBulletState.Immutable.Id, ArtyBulletState>(),
                        new List<ZapperSetupState>(),
                        new ConcurrentDictionary<ArcBulletState.Immutable.Id, ArcBulletState>());

            var ants2 = new ConcurrentDictionary<Guid, AntState>();

            var target2 = AntFactory.Make(Guid.Parse("{51334B1F-3EC2-49E7-B9E9-EE45A4616171}"), AntType.Basic, 1, Vector128.Create(0.0, 0.0));

            ants2.TryAdd(target2.immutable.id, target2);

            var state2 = new SimulationState(
                        ants1,
                        new ConcurrentDictionary<Id, BulletState>(),
                        new MoneyState(new[] { 0.0, 0.0 }, new[] { 0.0, 0.0 }),
                        new ControlState(100.0),
                        Array.Empty<ControlPointState>(),
                        new List<StationState>(),
                        new PopulationState(5, new[] { 0.0, 0.0 }),
                        new List<GrenadeState>(),
                        Array.Empty<ResourceSpawnerState>(),
                        new List<ResourceState>(),
                        new Dictionary<Guid, SmokeState>(),
                        new List<ShockwaveState>(),
                        new ConcurrentDictionary<HealBulletState.Immutable.Id, HealBulletState>(),
                        new ConcurrentDictionary<ArtyBulletState.Immutable.Id, ArtyBulletState>(),
                        new List<ZapperSetupState>(),
                        new ConcurrentDictionary<ArcBulletState.Immutable.Id, ArcBulletState>());

            var x = TypedSerializer.Serialize(state1);
            var y = TypedSerializer.Serialize(state2);

            Assert.Equal(state1.GetHashCode(), state2.GetHashCode());

        }
    }
}
