using System;
using Godot;
using Xunit;
using Ants;

namespace Ants.Test;

public class UnitTest1
{
    [Fact]
    public void Intersect1()
    {
        var res = Pathing.Intersect(new Vector2(-50, 0).ToNumericsVector(), new Vector2(50, 0).ToNumericsVector(), new Vector2(0, -50).ToNumericsVector(), new Vector2(0, 50).ToNumericsVector());
        Assert.NotNull(res);
        Assert.Equal(new Vector2(0,0).ToNumericsVector(), res!.Value.Item1);

    }
    [Fact]
    public void Intersect2()
    {
        var res = Pathing.Intersect(new Vector2(50, 0).ToNumericsVector(), new Vector2(-50, 0).ToNumericsVector(), new Vector2(0, 50).ToNumericsVector(), new Vector2(0, -50).ToNumericsVector());
        Assert.NotNull(res);
        Assert.Equal(new Vector2(0, 0).ToNumericsVector(), res!.Value.Item1);

    }
    [Fact]
    public void Intersect3()
    {
        var res = Pathing.Intersect(new Vector2(0, -50).ToNumericsVector(), new Vector2(0, 50).ToNumericsVector(), new Vector2(-50, 0).ToNumericsVector(), new Vector2(50, 0).ToNumericsVector());
        Assert.NotNull(res);
        Assert.Equal(new Vector2(0, 0).ToNumericsVector(), res!.Value.Item1);
    }
    [Fact]
    public void Intersect4()
    {
        var res = Pathing.Intersect(new Vector2(-50, -50).ToNumericsVector(), new Vector2(50, 50).ToNumericsVector(), new Vector2(-50, 50).ToNumericsVector(), new Vector2(50, -50).ToNumericsVector());
        Assert.NotNull(res);
        Assert.Equal(new Vector2(0, 0).ToNumericsVector(), res!.Value.Item1);
    }
    [Fact]
    public void Intersect5()
    {
        var res = Pathing.Intersect(new Vector2(-50, 0).ToNumericsVector(), new Vector2(50, 0).ToNumericsVector(), new Vector2(-100, 0).ToNumericsVector(), new Vector2(100, 0).ToNumericsVector());
        Assert.Null(res);
    }
    [Fact]
    public void Intersect6ParallelHorizontal()
    {
        var res = Pathing.Intersect(new Vector2(-50, 50).ToNumericsVector(), new Vector2(50, 50).ToNumericsVector(), new Vector2(-50, -50).ToNumericsVector(), new Vector2(50, -50).ToNumericsVector());
        Assert.Null(res);
    }
    [Fact]
    public void Intersect6ParallelVerical()
    {
        var res = Pathing.Intersect(new Vector2(50, -50).ToNumericsVector(), new Vector2(50, 50).ToNumericsVector(), new Vector2(-50, -50).ToNumericsVector(), new Vector2(-50, 50).ToNumericsVector());
        Assert.Null(res);
    }
    [Fact]
    public void T1AndT2NeedToBeBetween0And1()
    {
        var res = Pathing.Intersect(new Vector2(2667, 494).ToNumericsVector(), new Vector2(2663, 619).ToNumericsVector(), new Vector2(2686, 903).ToNumericsVector(), new Vector2(2643, 1021).ToNumericsVector());
        Assert.Null(res);
    }
    [Fact]
    public void Grr()
    {
        var res = Pathing.Intersect(new Vector2(53, -156).ToNumericsVector(), new Vector2(399, 318).ToNumericsVector(), new Vector2(379, 579).ToNumericsVector(), new Vector2(379, -8).ToNumericsVector());
        Assert.NotNull(res);
    }
    [Fact]
    public void Grr2()
    {
        var res = Pathing.Intersect(
            new Vector2(-505.4457f, 612.5622f).ToNumericsVector(),
            new Vector2(-505.4457f, 24.77695f).ToNumericsVector(),
            new Vector2(-696.1677f, -415.0131f).ToNumericsVector(),
            new Vector2(-350.6762f, 60.51516f).ToNumericsVector());

        // 135, 956
        // 135, 358
        // 0, 0
        // 345, 464
        //
        // ??
        Assert.Null(res);
    }

    //[Fact]
    //public void Test() {
    //    var a1= new Vector2(1, 0).AngleTo(new Vector2(0, 1));
    //    var a2 = new Vector2(0, 1).AngleTo(new Vector2(1, 0));
    //}

    [Fact]
    public void InShape() {
        Assert.True(MapExtensions.IsInShape(new[] {
            new Vector2(-1,1).ToNumericsVector(),
            new Vector2(1,1).ToNumericsVector(),
            new Vector2(1,-1).ToNumericsVector(),
            new Vector2(-1,-1).ToNumericsVector(),
        },
        new Vector2(0, 0).ToNumericsVector()));
    }

    [Fact]
    public void InShape_Over()
    {
        Assert.False(MapExtensions.IsInShape(new[] {
            new Vector2(-1,1).ToNumericsVector(),
            new Vector2(1,1).ToNumericsVector(),
            new Vector2(1,-1).ToNumericsVector(),
            new Vector2(-1,-1).ToNumericsVector(),
        },
        new Vector2(0, 2).ToNumericsVector()));
    }

    [Fact]
    public void InShape_Under()
    {
        Assert.False(MapExtensions.IsInShape(new[] {
            new Vector2(-1,1).ToNumericsVector(),
            new Vector2(1,1).ToNumericsVector(),
            new Vector2(1,-1).ToNumericsVector(),
            new Vector2(-1,-1).ToNumericsVector(),
        },
        new Vector2(0, -2).ToNumericsVector()));
    }

    [Fact]
    public void InShape_Left()
    {
        Assert.False(MapExtensions.IsInShape(new[] {
            new Vector2(-1,1).ToNumericsVector(),
            new Vector2(1,1).ToNumericsVector(),
            new Vector2(1,-1).ToNumericsVector(),
            new Vector2(-1,-1).ToNumericsVector(),
        },
        new Vector2(-2, 0).ToNumericsVector()));
    }

    [Fact]
    public void InShape_Right()
    {
        Assert.False(MapExtensions.IsInShape(new[] {
            new Vector2(-1,1).ToNumericsVector(),
            new Vector2(1,1).ToNumericsVector(),
            new Vector2(1,-1).ToNumericsVector(),
            new Vector2(-1,-1).ToNumericsVector(),
        },
        new Vector2(2, 0).ToNumericsVector()));
    }
}