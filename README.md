# Ants

World of tanks the RTS

## Game Play Goals

1. positional strategy - it's about fighting in the right place not with the right guys
   1. range matters
   1. flanking matters
   1. cover matters
1. stable state combat - An even fight should stalemate and then expand on the flanks. It should grow in complexity until one side has an advantage.
1. information warfare - bluffing is cool
1. no mono comps - you can't win with a massed single unit. Units depend on each other. No mass blink stalker. Paladins do not go brrrr.
1. no snowballs - small early game advantages shouldn't translate to large late game advantages

## Game Play Mechanics

1. shields
   1. What -- units take little damage from the direction their shield is facing
   1. Why -- encourages flanking
1. units are inaccurate
   1. What -- units miss a lot; misses deal full damage if they hit something else
   1. Why -- This discourages grouping units up / range matters
1. real LOS
   1. What -- you can't see through rocks
   1. Why -- cool/positional/information
1. partially visible
   1. What -- you get partial information about units in LOS but far away
   1. Why -- information
1. low APM units
   1. what -- most units are slow and don't require a lot of micro
   1. why -- I think we need this for 'stable state combat'
1. mud
   1. what -- some units can't go through mud, it slow some down, some are un effected by it
   1. why -- positional, units good in mud like to fight near it
1. cover
   1. what -- blocks shots fired from far away, it's not something you stand in (like DoW), but rather something you stand behind so you can be flanked.
   1. why -- positional

