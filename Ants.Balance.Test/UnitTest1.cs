using Ants.Domain.State;
using System.Collections.Concurrent;
using System.Runtime.Intrinsics;

namespace Ants.Balance.Test
{

    public class UnitTests
    {

        [Fact(Skip = "out of date")]
        public async Task Heavy_Beats_TankDestoryer_200()
        {
            var game = MakeGame(new List<AntState> {
                MakeAnt(AntType.Basic, 0, new Godot.Vector2(200,0)),
                MakeAnt(AntType.BasicNarrow, 1, new Godot.Vector2(0,0)),
            });

            while (game.AntCount() == 2) {
                await Task.Delay(100);
            }

            Assert.True(game.HasNoSurviving(AntType.Basic));

            game.cancellationTokenSource.Cancel();
        }

        //[Fact(Skip = "out of date")]
        //public async Task Heavy_Beats_Medium_200()
        //{
        //    var game = MakeGame(new List<AntState> {
        //        MakeAnt(AntType.Heavy_Old, 0, new Godot.Vector2(200,0)),
        //        MakeAnt(AntType.Medium_Old, 1, new Godot.Vector2(0,0)),
        //    });

        //    while (game.AntCount() == 2)
        //    {
        //        await Task.Delay(100);
        //    }

        //    Assert.True(game.HasNoSurviving(AntType.Medium_Old));

        //    game.cancellationTokenSource.Cancel();
        //}

        //[Fact(Skip = "out of date")]
        //public async Task Heavy_Beats_Medium_100()
        //{
        //    var game = MakeGame(new List<AntState> {
        //        MakeAnt(AntType.Heavy_Old, 0, new Godot.Vector2(100,0)),
        //        MakeAnt(AntType.Medium_Old, 1, new Godot.Vector2(0,0)),
        //    });

        //    while (game.AntCount() == 2)
        //    {
        //        await Task.Delay(100);
        //    }

        //    Assert.True(game.HasNoSurviving(AntType.Medium_Old));

        //    game.cancellationTokenSource.Cancel();
        //}

        //[Fact(Skip = "out of date")]
        //public async Task Medium_Beats_Light_200()
        //{
        //    var game = MakeGame(new List<AntState> {
        //        MakeAnt(AntType.Medium_Old, 0, new Godot.Vector2(200,0)),
        //        MakeAnt(AntType.LookOut_Old, 1, new Godot.Vector2(0,0)),
        //    });

        //    while (game.AntCount() == 2)
        //    {
        //        await Task.Delay(100);
        //    }

        //    Assert.True(game.HasNoSurviving(AntType.LookOut_Old));

        //    game.cancellationTokenSource.Cancel();
        //}

        //[Fact(Skip = "out of date")]
        //public async Task Medium_Beats_Light_400()
        //{
        //    var game = MakeGame(new List<AntState> {
        //        MakeAnt(AntType.Medium_Old, 0, new Godot.Vector2(400,0)),
        //        MakeAnt(AntType.LookOut_Old, 1, new Godot.Vector2(0,0)),
        //    });

        //    while (game.AntCount() == 2)
        //    {
        //        await Task.Delay(100);
        //    }

        //    Assert.True(game.HasNoSurviving(AntType.LookOut_Old));

        //    game.cancellationTokenSource.Cancel();
        //}

        //[Fact(Skip = "out of date")]
        //public async Task Medium_Beats_Light_600()
        //{
        //    var game = MakeGame(new List<AntState> {
        //        MakeAnt(AntType.Medium_Old, 0, new Godot.Vector2(600,0)),
        //        MakeAnt(AntType.LookOut_Old, 1, new Godot.Vector2(0,0)),
        //    });

        //    while (game.AntCount() == 2)
        //    {
        //        await Task.Delay(100);
        //    }

        //    Assert.True(game.HasNoSurviving(AntType.LookOut_Old));

        //    game.cancellationTokenSource.Cancel();
        //}

        //[Fact(Skip = "out of date")]
        //public async Task Light_Beats_Medium_800()
        //{
        //    var game = MakeGame(new List<AntState> {
        //        MakeAnt(AntType.Medium_Old, 0, new Godot.Vector2(800,0)),
        //        MakeAnt(AntType.LookOut_Old, 1, new Godot.Vector2(0,0)),
        //    });

        //    while (game.AntCount() == 2)
        //    {
        //        await Task.Delay(100);
        //    }

        //    Assert.True(game.HasNoSurviving(AntType.LookOut_Old));

        //    game.cancellationTokenSource.Cancel();
        //}

        //[Fact(Skip = "out of date")]
        //public async Task Light_Beats_Medium_1000()
        //{
        //    var game = MakeGame(new List<AntState> {
        //        MakeAnt(AntType.Medium_Old, 0, new Godot.Vector2(1000,0)),
        //        MakeAnt(AntType.LookOut_Old, 1, new Godot.Vector2(0,0)),
        //    });

        //    while (game.AntCount() == 2)
        //    {
        //        await Task.Delay(100);
        //    }

        //    Assert.True(game.HasNoSurviving(AntType.LookOut_Old));

        //    game.cancellationTokenSource.Cancel();
        //}


        public static Game MakeGame(List<AntState> ants)
        {
            throw new NotImplementedException();
            //var antsDict = new ConcurrentDictionary<Guid, AntState>();

            //foreach (var ant in ants)
            //{
            //    antsDict[ant.immutable.id] = ant;
            //}

            //var paths = new KnownPathsImmutableState(new Paths[] { });

            //var state = new SimulationState(
            //    antsDict,
            //    Array.Empty<RockState>(),
            //    paths,
            //    new ConcurrentDictionary<BulletState.Immutable.Id, BulletState>(),
            //    new MoneyState(new[] { 0.0, 0.0 }),
            //    new ControlState(100.0),
            //    Array.Empty<ControlPointState>(),
            //    Array.Empty<MudState>(),
            //    new List<StationState>(),
            //    new PopulationState(5, new[] { 0.0, 0.0}),
            //    new CoverState[0]);

            //var game = new Game(state, 1, 0, new TerrainMap(new Dictionary<(int x, int y), Vector128<double>>(), new HashSet<(int x, int y)>()), new MockSendData(), new Dictionary<(Pathing.Mode, double), TrainPaths>());
            //game.Start(0, new LosWorkerGroup(2));

            //return game;
        }

        public static AntState MakeAnt(AntType antType, int side, Godot.Vector2 vector2)
        {
            return AntFactory.Make(Guid.NewGuid(), antType, side, vector2.ToNumericsVector());
        }
    }
}

public static class GameExtensions
{
    public static bool HasNoSurviving(this Game game, AntType antType) { 
        return !game.displayState.advanceResult.state.ants.Where(x=>x.Value.immutable.type == antType).Any();
    }

    public static int AntCount(this Game game)
    {
        return game.displayState.advanceResult.state.ants.Count();
    }
}