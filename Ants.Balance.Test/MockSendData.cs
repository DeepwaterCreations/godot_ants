﻿namespace Ants.Balance.Test
{
    class MockSendData : ISendData
    {
        public Task Send(Game.WaitingFrame waitingFrame)
        {
            return Task.CompletedTask;
        }
    }
}